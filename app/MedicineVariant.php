<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MedicineVariant extends Model
{
	protected $table = 'medicine_variants';

	protected $guarded = [];	
}

