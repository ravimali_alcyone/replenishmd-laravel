<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ForumDetail extends Model {
	protected $table = 'forum_details';

	protected $guarded = [];
}

