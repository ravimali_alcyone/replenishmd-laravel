<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SocialPostCommentHub extends Model {
	protected $table = 'social_post_comment_hub';

	protected $guarded = [];
}

