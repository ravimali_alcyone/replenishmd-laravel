<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Section extends Model {
    # This Method Created for Insert & Update Sections.
    public function insertAndUpdateSection($data) {
		$sectionCheck = DB::table('sections')->where(array(
			'section_name' 	=> $data['section_name'],
			'language' 		=> 'en',
			'page_name' 	=> $data['page_name']
		))->get();

		if(count($sectionCheck) <= 0) {
			return DB::table('sections')->insert($data);
		} else {
			return DB::table('sections')->where(array('section_name' => $data['section_name'], 'page_name' => $data['page_name'], 'language' => 'en'))->update($data);
		}
	}

}
