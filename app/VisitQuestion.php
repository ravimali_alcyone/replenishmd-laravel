<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VisitQuestion extends Model
{
	protected $table = 'visit_questions';

	protected $guarded = [];	
}

