<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OnlineVisitProviderAssign extends Model {
	protected $table = 'online_visit_provider_assign';

	protected $guarded = [];
}

