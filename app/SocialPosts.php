<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SocialPosts extends Model {
	protected $table = 'social_posts';

	protected $guarded = [];
}

