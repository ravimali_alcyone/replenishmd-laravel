<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ForumPaymentDetail extends Model {
	protected $table = 'forum_payment_detail';

	protected $guarded = [];
}

