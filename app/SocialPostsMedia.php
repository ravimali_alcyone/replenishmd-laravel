<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SocialPostsMedia extends Model {
	protected $table = 'social_posts_media';

	protected $guarded = [];
}

