<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SocialUserFollowConnection extends Model {
	protected $table = 'social_user_follow_connection';

	protected $guarded = [];
}

