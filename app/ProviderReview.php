<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProviderReview extends Model
{
	protected $table = 'provider_reviews';

	protected $guarded = [];	
}

