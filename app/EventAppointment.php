<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventAppointment extends Model
{
	protected $table = 'events';

	protected $guarded = [];	
}

