<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SupporterRequest extends Model
{
    protected $table = 'supporter_requests';

	protected $guarded = [];
}
