<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OnlineVisitPaywallFeature extends Model {
	protected $table = 'online_visit_paywall_features';

	protected $guarded = [];
}

