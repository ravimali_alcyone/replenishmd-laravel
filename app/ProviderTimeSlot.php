<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProviderTimeSlot extends Model
{
	protected $table = 'provider_timeslots';

	protected $guarded = [];	
}

