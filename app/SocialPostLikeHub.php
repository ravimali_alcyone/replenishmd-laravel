<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SocialPostLikeHub extends Model {
	protected $table = 'social_post_like_hub';

	protected $guarded = [];
}

