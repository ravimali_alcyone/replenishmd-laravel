<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Cashier\Billable;

class User extends Authenticatable
{
    use Notifiable, Billable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'is_admin', 'menu_permissions', 'user_role', 'category', 'status','first_name', 'last_name', 'image', 'id_card_image', 'phone', 'address', 'gender', 'dob', 'age', 'household_income', 'dmm_fund', 'monthly_budget',' category', 'diagnosis', 'insurance','allergies', 'smokers', 'google_adsense_id', 'amazon_id', 'rmd_ad_id', 'provider_title', 'license_number', 'provider_licenses', 'npi', 'dea', 'practice_address', 'appointment_time_interval'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}
