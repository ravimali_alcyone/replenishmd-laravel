<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ForumTopicLike extends Model
{
	protected $table = 'forum_topic_likes';

	protected $guarded = [];	
}

