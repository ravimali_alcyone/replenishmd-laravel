<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentCardInfo extends Model
{
     /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'payment_card_info';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];
        
}

