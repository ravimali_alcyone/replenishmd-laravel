<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SocialPostMediaCommentHub extends Model {
	protected $table = 'social_post_media_comment_hub';

	protected $guarded = [];
}

