<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BlogComment extends Model {
	protected $table = 'blog_comments';

	protected $guarded = [];
}

