<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VisitAnswer extends Model
{
	protected $table = 'visit_answers';

	protected $guarded = [];	
}

