<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FacebookAuth extends Model
{
	protected $table = 'facebook_auth';

	protected $guarded = [];	
}

