<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Laravel\Cashier\Exceptions\IncompletePayment;
use App\Library\Services\CommonService;
use App\TreatmentMedicine;
use App\MedicineVariant;
use App\Order;
use App\OnlineVisit;
use App\OnlineVisitProviderAssign;
use App\OnlineVisitStatusTimeline;
use App\OnlineVisitPaywallFeature;
use App\Service;
use App\Subscription;
use Session;

class BillingController extends Controller
{
    public function index (Request $request, CommonService $common) {
		
		$req = $request->all();
		
		//_token
		
        try {
			
			$sh_add = session('shipping_address');
			
 			$user_info = [
				'email' => auth()->user()->email, 
				'name' => auth()->user()->name, 
				'address' => [
					'line1' => $sh_add['shipping_address1'],
					'postal_code' => $sh_add['shipping_zipcode'],
					'city' => $sh_add['shipping_city'],
					'state' => $sh_add['shipping_state'],
					'country' => $sh_add['shipping_country'],
				]			
			];
			$medicine_id = 0;
			$medicine_variant_id = 0;
			$sub_type = 'other';
			$plan_variant_stripe_id = '';
			$myVariantId = '';
			$medicine_qty = 1;
			$MedicineVariant = array();
			$remarks = 'Online Visit medicine prescription payment';
			

			// if Medicine is also being purchased.
			if(Session::has('medicine_id') && session('medicine_id') != 0){ 
				$medicine_id = session('medicine_id');
				$sub_type = 'medicine';
				$Medicine = TreatmentMedicine::find($medicine_id);
			
				if(Session::has('medicine_qty') && session('medicine_qty') != 0){ 
					$medicine_qty = session('medicine_qty');
				}
				
				if(Session::has('medicine_variant_id') && session('medicine_variant_id') != 0){ 
					$medicine_variant_id = session('medicine_variant_id');
					$sub_type = 'medicine'; 
					$MedicineVariant = MedicineVariant::find($medicine_variant_id);
					$plan_variant_stripe_id = $MedicineVariant->plan_variant_stripe_id;
				}			
				
				$s_data = array(
					'sub_type'=>'medicine',
					'medicine_id'=>$medicine_id,
					'medicine_variant_id'=>$medicine_variant_id,
				);
				
				//Subscription[Recurring]
				if(Session::has('medicine_variant_payment_type') && session('medicine_variant_payment_type') == 2){ 
					
					$myVariantId = $plan_variant_stripe_id;
						
					if (is_null(auth()->user()->stripe_id)) {
						$stripeCustomer = auth()->user()->createOrGetStripeCustomer($user_info);
						auth()->user()->updateDefaultPaymentMethod($request->payment_method);
						auth()->user()->save();
					}
					
					$newSubscription = auth()->user()->newSubscription('main', $myVariantId)->quantity($medicine_qty)->create($request->payment_method, $user_info);
										
					Subscription::where('id',$newSubscription->id)->update($s_data);
					
				}
				else{

					//Invoice[One Time]
					
					if(is_null(auth()->user()->stripe_id)) {
						$stripeCustomer = auth()->user()->createOrGetStripeCustomer($user_info);
						auth()->user()->updateDefaultPaymentMethod($request->payment_method);
						auth()->user()->save();
					}
					
					
					$title = $Medicine->name.' - '.$MedicineVariant->variant_name.', Qty:'.$medicine_qty;
				
					$newInvoiceItem = auth()->user()->invoiceFor($title, session('final_amount')*100);		// multiply by 100

					
					$s_data['user_id'] = auth()->user()->id;
					$s_data['name'] = 'one_time';
					$s_data['stripe_id'] = $newInvoiceItem->id;
					$s_data['stripe_status'] = 'active';
					$s_data['stripe_plan'] = $newInvoiceItem->id;
					$s_data['quantity'] = $medicine_qty;
									
					$newSubscription = Subscription::create($s_data);
					$remarks = 'Only visit payment';
				}
			}else{
				$title = 'Visit Charge Only';
				
				//Invoice[One Time]
				
                if (is_null(auth()->user()->stripe_id)) {
                    $stripeCustomer = auth()->user()->createOrGetStripeCustomer($user_info);
                    auth()->user()->updateDefaultPaymentMethod($request->payment_method);
					auth()->user()->save();
                }
				
				$newInvoiceItem = auth()->user()->invoiceFor($title, session('final_amount')*100);		// multiply by 100

				//echo '<Pre>';print_r($newInvoiceItem);die;
				
				$s_data['user_id'] = auth()->user()->id;
				$s_data['name'] = 'one_time';
				$s_data['stripe_id'] = $newInvoiceItem->id;
				$s_data['stripe_status'] = 'active';
				$s_data['stripe_plan'] = $newInvoiceItem->id;
				$s_data['quantity'] = 1;
				
				$newSubscription = Subscription::create($s_data);			
			}
							
			$service_detail = Service::where('id',session('online_visit_service_id'))->where('status',1)->first();
			
			if($service_detail && $service_detail->count() > 0){
					
				if(Session::has('online_visit_id') && session('online_visit_id') != ''){

					$online_visit = OnlineVisit::where('id', session('online_visit_id'))->where('is_submit',0)->first();

					if($online_visit && $online_visit->count() > 0){
						$online_visit_data = array(
							'visit_amount' => 0, // services table
							'payment_status' => 'done',
							'is_submit' => 1,
						);
						
						OnlineVisit::where('id',$online_visit->id)->update($online_visit_data); //Update Online Visit 

						$order_data = array(
							'user_id' => auth()->user()->id,
							'online_visit_id' => $online_visit->id,					
							'sub_id' => $newSubscription->id,
							'remarks' => $remarks,
							'amount' => session('final_amount'),
							'status' => 1 // Processing
						);
						
						Order::create($order_data); //Order Create
						
						//If Visit type is Asynchronous
						if($service_detail->visit_type == '1'){
					
							//Check providers to assign
							$provider = $common->getProviderForOnlineVisit();
							
							if($provider){
								
								//Provider Assign to Online Visit
								
								$i_data3 = array(
									'online_visit_id' => $online_visit->id,
									'provider_id' => $provider,
									'status' => 1
								);
							
								OnlineVisitProviderAssign::create($i_data3);
							
								//Online Visit Status Timeline
								
								$i_data4 = array(
									'online_visit_id' => $online_visit->id,
									'old_visit_status' => NULL,
									'new_visit_status' => 'PENDING ACCEPTANCE',
								);
								
								OnlineVisitStatusTimeline::create($i_data4);
							}
							
						}						
					}					
				}	
			}
						
			session()->put('is_being_payment', 2); //1 for yes, 0 for No, 2 for finished
			session()->forget('final_amount');
			session()->forget('shipping_address');			
			
        } catch ( IncompletePayment $exception ){
            return redirect()->route(
                'cashier.payment',
                [$exception->payment->id, 'redirect' => route('payment_page')]
            );

            //route('cashier.payment', $subscription->latestPayment()->id)
        }
        

        return redirect('success_page');
    }

    public function reprocess(Request $request){
        return $this->newSubscription($request->payment_method);
    }

    public function newSubscription($paymentMethod){
        $user = auth()->user();
		
        try {
			
 			$user_info = [
				'email' => auth()->user()->email, 
				'name' => auth()->user()->name, 
				'address' => [
					'line1' => 'Newark',
					'postal_code' => '75501',
					'city' => 'Newark',
					'state' => 'Texas',
					'country' => 'US',
				]			
			];
			
			if (is_null(auth()->user()->stripe_id)) {
				$stripeCustomer = auth()->user()->createOrGetStripeCustomer($user_info);
				auth()->user()->updateDefaultPaymentMethod($request->payment_method);
				auth()->user()->save();
			}
				
            $newSubscription = auth()->user()->newSubscription('main', 'price_1I3hG4HPfT4MeGIh7TJti5DC')->quantity(1)->withCoupon('uMs1ROXf')->create($paymentMethod, $user_info);
        } catch ( IncompletePayment $exception ){
            return redirect()->route(
                'cashier.payment',
                [$exception->payment->id, 'redirect' => route('home')]
            );

            //route('cashier.payment', $subscription->latestPayment()->id)
        }
        

        return redirect()->back();
    }

}
