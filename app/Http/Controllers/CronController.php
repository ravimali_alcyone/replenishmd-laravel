<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Session;
use App\Library\Services\CommonService;
use App\User;
use DB;
use App\Notifications;
use App\SocialStory;
use Auth;



class CronController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
		ini_set('max_execution_time', 0);
        //$this->middleware('auth:students');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
	 
    
	  public function index(Request $request) {
        /**
		 * Here notification staus will change active to expired after 7 days.
		 * 
		 */
		    $notification = DB::select( DB::raw("Update notifications set status=2 WHERE DATEDIFF(NOW(),`created_at`) > 7"));
    }

    public function send_post_detail_to_subscriber(Request $request)
    {
        #Here system will send new blog post notification to each subscriber emmail-id
        $user =  BlogSubscription::select('*')->where('subscribe_flag',1)->get();
    }

    public function disable_story_time(Request $request)
    {
        #Here system will disable the shared story after 24 hour
        $story = DB::select( DB::raw("Update social_stories set display_status=0 WHERE TIMESTAMPDIFF(HOUR, `created_at`, NOW()) >= 24"));
    }
	
	
	
	
	
	
	
    

}
