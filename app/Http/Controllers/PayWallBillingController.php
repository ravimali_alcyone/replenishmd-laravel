<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Laravel\Cashier\Exceptions\IncompletePayment;
use App\Library\Services\CommonService;
use App\TreatmentMedicine;
use App\MedicineVariant;
use App\Order;
use App\OnlineVisit;
use App\OnlineVisitProviderAssign;
use App\OnlineVisitStatusTimeline;
use App\OnlineVisitPaywallFeature;
use App\Service;
use App\Subscription;
use App\ShippingAddress;
use Session;
use Auth;

class PayWallBillingController extends Controller
{
    public function index (Request $request, CommonService $common) {

		if(!Session::has('paywall_feature_online_visit_id')){ 
			redirect('user.treatments')->withErrors('Payment failed');
		}
		
		if(!Session::has('paywall_feature_amount')){ 
			redirect('user.treatments')->withErrors('Payment failed');
		}
				
		try {
			
			$sh_add = ShippingAddress::where('user_id',$user->id)->where('status',1)->first();
			
			if($sh_add && $sh_add->count() > 0){
				
				$user_info = [
					'email' => auth()->user()->email, 
					'name' => auth()->user()->name, 
					'address' => [
						'line1' => $sh_add->address_line1,
						'postal_code' => $sh_add->postal_code,
						'city' => $sh_add->city,
						'state' => $sh_add->state,
						'country' => $sh_add->country,
					]			
				];

				$remarks = 'Video Call Feature Payment';
				
				$title = 'Video Call Feature Charge Only';
				
				//Invoice[One Time]				
				if (is_null(auth()->user()->stripe_id)) {
					$stripeCustomer = auth()->user()->createOrGetStripeCustomer($user_info);
					auth()->user()->updateDefaultPaymentMethod($request->payment_method);
					auth()->user()->save();
				}
					
				$newInvoiceItem = auth()->user()->invoiceFor($title, session('paywall_feature_amount')*100);	// multiply by 100
				
				$s_data['user_id'] = auth()->user()->id;
				$s_data['name'] = 'one_time';
				$s_data['stripe_id'] = $newInvoiceItem->id;
				$s_data['stripe_status'] = 'active';
				$s_data['stripe_plan'] = $newInvoiceItem->id;
				$s_data['quantity'] = 1;
		
				$newSubscription = Subscription::create($s_data);				
							
				session()->put('is_being_payment', 2); //1 for yes, 0 for No, 2 for finished	

				$input_data = array(
					'online_visit_id' => session('paywall_feature_online_visit_id'),
					'patient_id' => auth()->user()->id,
					'provider_id' => session('paywall_feature_provider_id'),
					'feature_type' => 'video_call',
					'status' => 1,
					'patient_id' => auth()->user()->id,
				);
				
				OnlineVisitPaywallFeature::create($input_data);
					
				
			}else{
				return redirect()->route('user.treatments');
			}
			
			
        } catch ( IncompletePayment $exception ){
            return redirect()->route(
                'cashier.payment',
                [$exception->payment->id, 'redirect' => route('user.paywall_payment_page')]
            );
        }
        
		return redirect()->route('user.paywall_success_page');
    }

    public function reprocess(Request $request){
        return $this->newSubscription($request->payment_method);
    }

    public function newSubscription($paymentMethod){
        auth()->user() = auth()->user();
		
        try {
			
 			$user_info = [
				'email' => auth()->user()->email, 
				'name' => auth()->user()->name, 
				'address' => [
					'line1' => 'Newark',
					'postal_code' => '75501',
					'city' => 'Newark',
					'state' => 'Texas',
					'country' => 'US',
				]			
			];
			
			//Invoice[One Time]
			
			if (is_null(auth()->user()->stripe_id)) {
				$stripeCustomer = auth()->user()->createOrGetStripeCustomer($user_info);
				auth()->user()->updateDefaultPaymentMethod($request->payment_method);
				auth()->user()->save();
			}			
			
            $newSubscription = auth()->user()->newSubscription('main', 'price_1I3hG4HPfT4MeGIh7TJti5DC')->quantity(1)->withCoupon('uMs1ROXf')->create($paymentMethod, $user_info);
        } catch ( IncompletePayment $exception ){
            return redirect()->route(
                'cashier.payment',
                [$exception->payment->id, 'redirect' => route('home')]
            );

            //route('cashier.payment', $subscription->latestPayment()->id)
        }
        

        return redirect()->back();
    }

}
