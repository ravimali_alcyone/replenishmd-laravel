<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\Library\Services\CommonService;
use DataTables;

class AdminController extends Controller
{

    public function __construct(){
        $this->middleware('auth');
    }

    public function index(Request $request, CommonService $common){
		
		if ($request->ajax()) {
			
			$where = array('is_admin' => 1,	'user_role' => 2);
			
			if($request->filter_status != ''){
				$where['status'] = $request->filter_status;
			}			
			
			$data = $common->getTableListing($request, $table='users', $where, array(), $isLatest=true);
			
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
						$btn = '<div class="flex items-center">';
						$edit_url = env('APP_URL').'/admin/subadmins/add/'.$row->id;
                        $btn = $btn.'<a href="'.$edit_url.'" class="button button--sm w-16 shadow-md mr-1 mb-2 btn_blue text-white editUser" data-id="'.$row->id.'" title="Edit"> Edit </a>';
                        $btn = $btn.' <a class="button button--sm w-16 shadow-md mr-1 mb-2 btn_red text-white deleteUser" href="javascript:void(0)" onclick="deleteRow('.$row->id.')" data-id="'.$row->id.'" title="Delete">Delete</a>';
						$btn = $btn.'</div>';
                        return $btn;
                    })
					->addColumn('status', function($row){
						if($row->status == 1){
							// $status = '<span class="chip green lighten-5"><span class="green-text">Active</span></span>';
							$status = '<input type="checkbox" name="status" class="input input--switch border" title="Status" checked onchange="changeStatus('.$row->id.')">';
						}else{
							// $status = '<span class="chip red lighten-5"><span class="red-text">Blocked</span></span>';
							$status = '<input type="checkbox" name="status" class="input input--switch border" title="Status" onchange="changeStatus('.$row->id.')">';
						}
						return $status;
					})
                    ->addColumn('privileges', function($row){
						   $privileges = '';
                           if($row->menu_permissions != ''){
							   $menu_permissions = json_decode($row->menu_permissions,true);
							   if($menu_permissions){
								   $menu_permissions = $this->getActualValueArray($menu_permissions);

									$check = '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-check w-4 h-4 mr-2"><polyline points="20 6 9 17 4 12"></polyline></svg>';

									$item = '<a href="javascript:void(0)" class="flex items-center block p-1 transition duration-300 ease-in-out bg-white dark:bg-dark-1 hover:bg-gray-200 dark:hover:bg-dark-2 rounded-md">'.$check;

								   $menus = $item.implode('</a>'.$item,$menu_permissions).'</a>';
								   $privileges = '<div class="dropdown relative"> <button class="dropdown-toggle button button--sm w-20 shadow-md mr-1 mb-2 btn_gray text-white">View All</button>
														<div class="dropdown-box mt-10 absolute w-48 top-0 left-0 z-20">
															<div class="dropdown-box__content box dark:bg-dark-1 p-2">'.$menus.'</div>
														</div>
													</div>';


							   }
						   }
                            return $privileges;
                    })
                    ->rawColumns(['status','privileges','action'])
                    ->make(true);
        }
		$data['page'] = 'sub_admins';
        return view('admin.sub_admin.index', $data);
    }

	public function add(){
		$data['user'] = array();
		$data['page'] = 'sub_admins';
		return view('admin.sub_admin.add', $data);
	}

    public function store(Request $request){
		$req = $request->all();
        $this->validate($request, [
			'name' => 'required|string|max:255',
            'email' => 'required|email|max:255|unique:users,email,'.$req['id'],
        ]);

		$menu_permissions = json_encode($req['privileges'],true);
		$password = Hash::make('123456');

		if(isset($req['status']) && $req['status'] == 'on'){
			$status = 1;
		}else{
			$status = 0;
		}

		$where = ['id' => $req['id']];
		$input_data = [
			'name' => $req['name'],
			'email' => $req['email'],
			'user_role' => 2,
			'status'=> $status,
			'menu_permissions'=> $menu_permissions,
			'password' => $password,
			'is_admin' => 1
		];
		if($req['id'] != ''){
			unset($input_data['password']);
		}
        User::updateOrCreate($where, $input_data);
        return response()->json(['status'=>'success', 'message'=>'Saved successfully.']);
    }

    public function edit($id){
        $user = User::find($id);
		if($user && $user->count() > 0){
			$data['user'] = $user;
			$data['page'] = 'sub_admins';
			return view('admin.sub_admin.add',$data);
		}else{
			return redirect()->route('admin.subadmins');
		}
    }

    public function destroy($id){

        User::find($id)->delete();

        return response()->json(['status'=>'success', 'message'=>'Deleted successfully.']);
    }

	public function change_status(Request $request) {
		$user = User::where('id', $request->id)->first();
		if ($user->status) {
			$user = User::where('id', $request->id)->update(array('status' => 0));
		} else {
			$user = User::where('id', $request->id)->update(array('status' => 1));
		}
	}
	
	public function getActualValueArray($stringArray){

		foreach($stringArray as $key => $value){

			$newValue = $value;

			if($value == 'manage_online_visits') { $newValue = 'Online Visits';	}
			if($value == 'visit_pool') { $newValue = 'Visit Pool';	}
			if($value == 'manage_orders') { $newValue = 'Orders';	}
			if($value == 'manage_products') { $newValue = 'Products';	}
			if($value == 'manage_providers') { $newValue = 'Providers';	}
			if($value == 'manage_patients') { $newValue = 'Patients';	}
			if($value == 'manage_contact_us') { $newValue = 'Contact Us';	}
			if($value == 'finances') { $newValue = 'Finances';	}
			if($value == 'manage_website_content') { $newValue = 'Website Content';	}
			if($value == 'manage_sub_admin') { $newValue = 'Sub Admins';	}
			if($value == 'manage_questions') { $newValue = 'Questions';	}
			if($value == 'manage_faq') { $newValue = 'FAQ';	}

			$stringArray[$key] = $newValue;
		}
		return $stringArray;
	}
}
