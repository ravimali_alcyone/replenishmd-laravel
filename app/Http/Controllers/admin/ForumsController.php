<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\ForumTopic;
use App\ForumCategory;
use App\ForumDetail;
use App\MasterSetting;
use App\ForumPaymentDetail;
use App\User;
use DataTables;
use DB;

class ForumsController extends Controller {

    public function __construct(){
        $this->middleware('auth');
    }

/** *** Forum Topics Module  *** **/

    public function index(Request $request)
	{
		
		if ($request->ajax()) 
		{
			$data = ForumTopic::select('forum_topics.*','users.name','forum_details.forum_subject')
				->join('forum_details', function ($join) {
					$join->on('forum_details.id', '=', 'forum_topics.forum_category_id');
				})
				->join('users', function ($join) {
					$join->on('users.id', '=', 'forum_topics.user_id');
				});

			if($request->filter_category != ''){
				$data = $data->where('forum_topics.forum_category_id',$request->filter_category);
			}

			if($request->author != ''){
				$data = $data->where('forum_topics.user_id',$request->author);
			}

			if($request->publish_status != ''){
				$data = $data->where('forum_topics.status',$request->publish_status);
			}

			if($request->comment_status != ''){
				$data = $data->where('forum_topics.comment_on',$request->comment_status);
			}
			
			$data = $data->latest()->get();
            return Datatables::of($data)
			->addIndexColumn()
			->addColumn('action', function($row){
				
				

				$edit_url = env('APP_URL').'/admin/forums/post/edit/'.$row->id;
				$view_url = '';
				$btn = '';
				$btn = $btn.'<div class="flex items-center"><a href="'.$view_url.'" data-id="'.$row->id.'" title="View" class="button button--sm w-16 shadow-md mr-1 mb-2 btn_info viewUser">View</a>';
				if($row->user_id == auth()->user()->id)
				{
					$btn = $btn.'<a href="'.$edit_url.'"   data-id="'.$row->id.'" title="Edit" class="button button--sm w-16 shadow-md mr-1 mb-2 btn_blue viewUser">Edit</a>';
				}
				$btn = $btn.' <a href="javascript:void(0)" onclick="deleteRow('.$row->id.')" data-id="'.$row->id.'" title="Delete" class="button button--sm w-16 shadow-md mr-1 mb-2 btn_red deleteUser">Delete</a>';
				$btn = $btn.' </div>';
				return $btn;
			})
			
			->addColumn('status', function($row){
				if($row->status == 1){
					$status = '<input type="checkbox" name="status" class="input input--switch border"  title="Status" checked onchange="changeStatus('.$row->id.',1,2)">';
				}else{
					$status = '<input type="checkbox" name="status" class="input input--switch border"  title="Status" onchange="changeStatus('.$row->id.',0,2)">';
				}
				return $status;
			})
			->addColumn('comment_on', function($row){
				if($row->comment_on == 1){
					$comment_on = '<input type="checkbox" name="comment_on" class="input input--switch border"  title="Allow Comment" checked onchange="changeCommentStatus('.$row->id.',1,3)">';
				}else{
					$comment_on = '<input type="checkbox" name="comment_on" class="input input--switch border"  title="Allow Comment" onchange="changeCommentStatus('.$row->id.',0,3)">';
				}
				return $comment_on;
			})
			
			->addColumn('title', function($row){
				$res = substr($row->title, 0, 50);
				return $res.'...';
			})
			->addColumn('forum_subject', function($row){
				$res = substr($row->forum_subject, 0, 50);
				return $res.'...';
			})
			->addColumn('author', function($row){
				$data = '<h4 style="font-size: 14px;" class="mt-1">'.$row->name.'</h4>';
				return $data;
			})				
			
			

			->rawColumns(['forum_subject','title','author','status','comment_on','action'])
			->make(true);
        }

		$data['parent_page'] = 'forums';
		$data['page'] = 'forum_topics';
		$data['title'] = 'Forum Topics';
		$data['categories'] = ForumDetail::select('*')->get();
		$data['users'] = User::where('status', 1)->get();
        return view('admin.forums.topics.index', $data);
    }

	#Add topic 
	public function addPost(Request $request)
	{
		$id = "";
		$data['forum'] = array();
		$data['parent_page'] = 'forums';
		$data['page'] = 'add_post';
		$data['title'] = 'Forum Post';
		$data['funcType'] = 'add';
		$data['id'] = $id;
		$data['forum_topic'] = array();
		$data['categories'] = ForumDetail::select('*')->get();
		
		return view('admin.forums.topics.add', $data);
	}
	
	#Edit topic 
	public function editPost(Request $request)
	{	
		$id = $request->id;		
		$data['parent_page'] = 'forums';
		$data['page'] = 'edit_topic';
		$data['title'] = 'Forum Post';
		$data['funcType'] = 'edit';
		$data['id'] = $id;
		$data['forum_topic'] = ForumTopic::where('id', $id)->first();
		$data['categories'] = ForumDetail::select('*')->get();
		
		return view('admin.forums.topics.add', $data);
	}
	
	public function storePost(Request $request)
	{
		$status = 'error';
		$message = 'Error occured';

		if ($request->get('page') == 'edit') {
			$id = $request->get('id');
		}

		$input_data = [
			'user_id' => auth()->user()->id,
			'forum_category_id' => $request->category,
			'slug' => $request->slug,
			'title' => $request->title,
			'description' => $request->description,			
			'status' => $request->status,
			'comment_on' => $request->comment_on
		];
		
		if ($request->get('page') == 'add') {
			
			$forum_topic = ForumTopic::create($input_data);
			if ($forum_topic->save()) {
				return response()->json(['status'=>'success', 'message'=>'Saved successfully.']);
			}
		} 
		elseif ($request->get('page') == 'edit') {
			$forum_topic = ForumTopic::where('id', $request->id)->update($input_data);
			return response()->json(['status'=>'success', 'message'=>'Saved successfully.']);
		}
	}
	
    public function storeForum(Request $request)
	{
		$status = 'error';
		$message = 'Error occured';

		if ($request->get('page') == 'edit') {
			$id = $request->get('id');
		}


		$input_data = [
			'forum_subject' => $request->forum_subject,
			'short_info' => $request->short_info,
			'slug' => $request->slug,
			'created_by' => auth()->user()->id,
			'status' => $request->status,
			'is_paid' => $request->status,
			'access_amount' => $request->access_amount,
			'access_payment_type' => $request->access_payment_type,
			'is_posting_paid' => $request->is_posting,
			'posting_amount' => $request->posting_amount,
			'posting_payment_type' => $request->posting_payment_type,
			'is_approved' => 1
			
		];
		
		if ($request->get('page') == 'add') {
			
			$forum_topic = ForumDetail::create($input_data);
			if ($forum_topic->save()) {
				return response()->json(['status'=>'success', 'message'=>'Saved successfully.']);
			}
		} 
		elseif ($request->get('page') == 'edit') {
			$forum_topic = ForumDetail::where('id', $request->id)->update($input_data);
			return response()->json(['status'=>'success', 'message'=>'Saved successfully.']);
		}
	}

	public function topicStatus(Request $request) 
	{
		$row = ForumTopic::where('id', $request->id)->first();
		
		if($row){
			if ($row->status == 1) {
				$row = ForumTopic::where('id', $request->id)->update(array('status' => 0));
			} else {
				$row = ForumTopic::where('id', $request->id)->update(array('status' => 1));
			}
			return response()->json(['status'=>'success', 'message'=>'Updated successfully.']);
		}else{
			return response()->json(['status'=>'error', 'message'=>'Error Occured.']);
		}

	}

	public function comment_on(Request $request) 
	{
		$row = ForumTopic::where('id', $request->id)->first();
		
		if($row){
			if ($row->comment_on == 1) {
				$row = ForumTopic::where('id', $request->id)->update(array('comment_on' => 0));
			} else {
				$row = ForumTopic::where('id', $request->id)->update(array('comment_on' => 1));
			}
			return response()->json(['status'=>'success', 'message'=>'Updated successfully.']);
		}else{
			return response()->json(['status'=>'error', 'message'=>'Error Occured.']);
		}

	}

	public function topic_paid_status(Request $request) 
	{
		$row = ForumTopic::where('id', $request->id)->first();
		
		if($row){
			if ($row->is_paid == 1) {
				$row = ForumTopic::where('id', $request->id)->update(array('is_paid' => 0));
			} else {
				$row = ForumTopic::where('id', $request->id)->update(array('is_paid' => 1));
			}
			return response()->json(['status'=>'success', 'message'=>'Updated successfully.']);
		}else{
			return response()->json(['status'=>'error', 'message'=>'Error Occured.']);
		}

	}
	
	#Remove Category
	public function destroy($id)
	{
        ForumTopic::find($id)->delete();
        return response()->json(['status'=>'success', 'message'=>'Deleted successfully.']);
	}

	#verify category
	public function verify_topic($id)
	{
		$forum = ForumTopic::where('id', $id)->update(array('is_verified'=>1));
        return response()->json(['status'=>'success', 'message'=>'Verify successfully.']);
	}
	
/** *** Forum Topics Module  *** **/	
	
	
/** *** Forum Module  *** **/	

	public function addForum(Request $request)
	{
		$id = $request->id;
		
		if($id==0)
		{
			$data['forum'] = array();
			$data['parent_page'] = 'forums';
			$data['page'] = 'forums';
			$data['title'] = 'Create Forum';
			$data['funcType'] = 'add';
			$data['id'] = $id;
			$data['forum_topic'] =array();
		}
		else
		{
			$data['forum'] = array();
			$data['parent_page'] = 'forums';
			$data['page'] = 'forums';
			$data['title'] = 'Edit Create Forum';
			$data['funcType'] = 'edit';
			$data['id'] = $id;
			$data['forum_topic'] = ForumDetail::where('id', $request->id)->first();
		}
		
		$data['categories'] = ForumDetail::where('status', 1)->get();
		return view('admin.forums.add_forum', $data);
	}

    

    #Remove Category
	public function destroy_category($id)
	{
        ForumDetail::find($id)->delete();
        return response()->json(['status'=>'success', 'message'=>'Deleted successfully.']);
	}

	#verify category
	public function verify_category($id)
	{
		$forum = ForumDetail::where('id', $id)->update(array('is_approved'=>1));
        return response()->json(['status'=>'success', 'message'=>'Verify successfully.']);
	}

	public function change_status(Request $request) 
	{
		$row = ForumCategory::where('id', $request->id)->first();
		
		if($row){
			if ($row->status == 1) {
				$row = ForumCategory::where('id', $request->id)->update(array('status' => 0));
			} else {
				$row = ForumCategory::where('id', $request->id)->update(array('status' => 1));
			}
			return response()->json(['status'=>'success', 'message'=>'Updated successfully.']);
		}else{
			return response()->json(['status'=>'error', 'message'=>'Error Occured.']);
		}

	}
	
	public function categories_list(Request $request)
	{
		if ($request->ajax()) 
		{
			$data = ForumDetail::select('forum_details.*','users.name',DB::raw('COUNT(forum_topics.forum_category_id) as topic_count'))
							->leftJoin('forum_topics', function ($join) {
									$join->on('forum_topics.forum_category_id', '=', 'forum_details.id');
							})
							->leftJoin('users', function ($join) {
									$join->on('users.id', '=', 'forum_details.created_by');
							})
							->groupBy('forum_details.id')->ORDERBY('forum_subject','ASC');
			

			if($request->author != ''){
				$data = $data->where('created_by',$request->author);
			}

			if($request->vstatus != ''){
				$data = $data->where('is_approved',$request->vstatus);
			}

			$data = $data->latest()->get();
            return Datatables::of($data)
				->addIndexColumn()
				->addColumn('action', function($row){
					
					$edit_url = env('APP_URL').'/admin/forums/addForum/'.$row->id;
					$btn = '';
					$btn = ($row->created_by==auth()->user()->id) ? $btn.'<a href="'.$edit_url.'"   data-id="'.$row->id.'" title="Edit" class="button button--sm w-16 shadow-md mr-1 mb-2 btn_blue viewUser">Edit</a>' : '';
					$btn = $btn.' <a href="javascript:void(0)" onclick="deleteRow('.$row->id.')" data-id="'.$row->id.'" title="Delete" class="button button--sm w-16 shadow-md mr-1 mb-2 btn_red deleteUser">Delete</a>';
					if($row->is_approved == 0){
					$btn = $btn.' <a href="javascript:void(0)" onclick="verifyRow('.$row->id.')" data-id="'.$row->id.'" title="Verify" class="button button--sm w-16 shadow-md mr-1 mb-2 btn_info">Verify</a>';
					}
					$btn = $btn.' </div>';
					return $btn;
				})
				
				
				
				->addColumn('title', function($row){
					$res = substr($row->forum_subject, 0, 50);
					return $res.'...';
				})
				->addColumn('no_of_post', function($row){
					$data = '<h4 style="font-size: 14px;" class="mt-1">'.$row->topic_count.'</h4>';
					return $data;
				})				
				->addColumn('moderator', function($row){
					$data = '<h4 style="font-size: 14px;" class="mt-1">'.$row->name.'</h4>';
					return $data;
				})
				->rawColumns(['title','moderator','no_of_post','action'])
				->make(true);
        }

		$data['parent_page'] = 'forums';
		$data['page'] = 'forum_categories';	
		$data['title'] = 'Forum Category';	
		$data['categories'] = ForumCategory::where('status', 1)->orderby('id','DESC')->get();
		$data['users'] = User::where('status', 1)->get();
        return view('admin.forums.categories.index', $data);

    }


	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	#Change category active and inactive status
	public function change_category_status(Request $request){
		$user = ForumCategory::where('id', $request->id)->first();
		if ($user->status) {
			$user = ForumCategory::where('id', $request->id)->update(array('status' => 0));
		} else {
			$user = ForumCategory::where('id', $request->id)->update(array('status' => 1));
		}
	}
	

	

	#Store data in category
	public function store_category(Request $request){
		$status = 'error';
		$message = 'Error occured';

		if ($request->get('page') == 'edit') {
			$id = $request->get('id');
		}

		if (!empty($request->file('file_input'))) {
            $file = $request->file('file_input');
			if(strtolower($file->getClientOriginalExtension()) == 'jpg' || strtolower($file->getClientOriginalExtension()) == 'png' || strtolower($file->getClientOriginalExtension()) == 'jpeg'){
				$name= time().'-'.rand(11111111,99999999).'.'.strtolower($file->getClientOriginalExtension());
				$destinationPath = 'uploads/category/';
				
				if($file->move($destinationPath, $name)){
					$image = 'uploads/category'.'/'.$name;
				}				
			}
			else
			{
				$status = 'error';
				$message = 'Only jpg, jpeg and png files are allowable';					
			}
        }

		if($request->get('page') == 'edit')
		{
			$filename = (!empty($request->file('file_input'))) ? $image : $request->old_file;
		}
		else
		{
			$filename = $image;
		}

		$input_data = [
			'name' => $request->category_name,
			'parent_id' => 0,
			'status' => 1,
			'category_img' => $filename
		];
		
		if ($request->get('page') == 'add') {
			$forum = ForumCategory::create($input_data);
			if ($forum->save()) {
				return response()->json(['status'=>'success', 'message'=>'Saved successfully.']);
			}
		} elseif ($request->get('page') == 'edit') {
			$forum = ForumCategory::where('id', $request->id)->update($input_data);
			return response()->json(['status'=>'success', 'message'=>'Saved successfully.']);
		}
	}


	
	public function verify_forum_topic($id){
        $forum = ForumTopic::where('id', $id)->update(array('is_verify'=>1));
        return response()->json(['status'=>'success', 'message'=>'Verify successfully.']);
	}

	public function getSlug($slug, $id){
		
		if($id != NULL){
			$check = ForumTopic::where('id','!=',$id)->where('slug','=',$slug)->get();		
		}else{
			$check = ForumTopic::where('slug','=',$slug)->get();
		}		
		
		if($check && $check->count() > 0){
			//slug is already exists
			$slug = $slug.'-'.time().rand(1111,9999);
		}
		
		return $slug;
	}
	
}
