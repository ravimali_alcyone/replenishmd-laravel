<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\Library\Services\CommonService;
use App\User;
use App\Service;
use App\VisitForm;
use App\VisitFormFieldType;
use App\VisitQuestion;
use App\QuestionOption;
use DataTables;

class VisitFormController extends Controller {

    public function __construct(){
        $this->middleware('auth');
	}

    public function index(Request $request){

		if ($request->ajax()) {
			$data = VisitForm::select('visit_forms.id','visit_forms.name', 'services.name AS service','visit_forms.status')
			->join('services','services.id','=','visit_forms.service_id')
			->orderBy('visit_forms.created_at','ASC');

			$data = $data->get();

            return Datatables::of($data)->addIndexColumn()
				->addColumn('action', function($row){
					$edit_url = env('APP_URL').'/admin/visit_forms/add/'.$row->id;

					$btn = '<div class="flex items-center"><a href="'.$edit_url.'" class="button button--sm w-16 shadow-md mr-1 mb-2 btn_blue text-white editUser" data-id="23" title="Edit"> Edit </a>';
					$btn = $btn.'<div class="flex items-center"><a href="javascript:void(0)" onclick="deleteRow('.$row->id.')" class="button button--sm w-16 shadow-md mr-1 mb-2 btn_red text-white deleteUser" data-id="23" title="Edit"> Delete </a>';
					return $btn;
				})
				->addColumn('service', function($row){
					$service = $row->service;
					return $service;
				})
				->addColumn('status', function($row){
					if($row->status == 1){
						$status = '<input type="checkbox" name="status" class="input input--switch border"  title="Status" checked onchange="changeStatus('.$row->id.')">';
					}else{
						$status = '<input type="checkbox" name="status" class="input input--switch border"  title="Status" onchange="changeStatus('.$row->id.')">';
					}
					return $status;
				})				
				->rawColumns(['service','name','status','action'])
				->make(true);
        }
		$data['page'] = 'visit_forms';
        return view('admin.visit_form.index', $data);
    }

	public function add(){
		$data['services'] = Service::select('services.*')
								->leftJoin('visit_forms','services.id','=','visit_forms.service_id')
								->where('services.status', 1)
								->whereNull('visit_forms.service_id')
								->groupBy('services.id')->get();
		$data['visit_form'] = array();
		$data['page'] = 'visit_forms';
		return view('admin.visit_form.add', $data);
	}
	
	public function addQuestion($formid){
        $visit_form = VisitForm::find($formid);
		$data['visit_questions '] = array();
		
		if($visit_form && $visit_form->count() > 0){
			$data['form_field_types'] = VisitFormFieldType::select('*')->where('status',1)->orderBy('s_order','ASC')->get();
			$data['visit_form'] = $visit_form;
			
			$parent_where = array(
				'visit_form_id' => $visit_form->id,
				'parent_id' => 0,
				'status' => 1,
			);
						
			$data['visit_questions'] = VisitQuestion::select('id','visit_form_id','question_order','question_type','question_text','parent_id')->where($parent_where)->orderBy('question_order','ASC')->get();
			$data['page'] = 'visit_forms';
			return view('admin.visit_form.addQuestion', $data);			
		} else {
			return redirect()->route('admin.visit_forms');
		}	
	}

 	public static function get_child_question($visit_form_id,$parent_id){
		$output = '';
		$child_where = array(
			'visit_form_id' => $visit_form_id,
			'parent_id' => $parent_id,
			'status' => 1,
		);									
		$data['child_questions'] = VisitQuestion::select('id','visit_form_id','question_order','question_type','question_text','parent_id')->where($child_where)->get();
				
		if($data['child_questions'] && $data['child_questions']->count() > 0){		
			$output = view('admin.visit_form.get_child_question',$data)->render();
		}
		
		return $output;
	}
			
    public function store(Request $request, CommonService $common){
		
		/* $messages = [
			'name.regex' => 'Form title field is required.',
			'service_id.regex' => 'Please select the type of service.',
		];
		
		$validator = Validator::make($request->all(), [
			'name' => 'required|string|regex:/^[A-Za-z. -]+$/|max:120',
			'service_id' => 'required|string'
		], $messages)->validate(); */
		
		$req = $request->all();
			
		$visit_form_id = $common->add_form($request); //get visit form id from add/update				
			
		return response()->json(['status'=>'success', 'message'=>'Saved successfully.', 'id' => $visit_form_id]);
		
    }

    public function storeQuestion(Request $request, CommonService $common){
		$req = $request->all();
		
		//echo '<pre>';print_R($req);die;
		
		if(isset($req['visit_form_data']) && count($req['visit_form_data']) > 0){
			
			$visit_form_data = $req['visit_form_data'];	
			$q_ids = array();
			$visitQArray = array();
			
			$visit_form_id = $request->formid; //get visit form id from add/update				
					
			//echo '<pre>'; print_r($visit_form_data); die;
			$order  = 1;
			foreach($visit_form_data as $key => $value){
				
				if(!empty($value['q_label'])){	
					$q_id = $common->add_question($visit_form_id, $value, 0, $order); //parent_id is 0 for parent question and question order
						
					if($value['type'] == 'yes_no' || $value['type'] == 'single' || $value['type'] == 'multiple'){
						
						//for Options
						if(isset($value['options']) && count($value['options']) > 0){
							$options = $value['options'];
							
							foreach($options as $k => $v){
								
								$op_id = $common->add_option($visit_form_id, $q_id, $v);

								//for child questions
								if(isset($v['child_array']) && count($v['child_array']) > 0){
									$child_array = $v['child_array'];
									
									foreach($child_array as $c => $val){
										
										$order = $this->add_child_question($common, $visit_form_id, $val, $op_id, $order); //op_id is now parent id for other question
									}
								}
							}
						}
					}
				}
			$order++;
			}
			
			//echo '<pre>'; print_r($visit_form_data); die();
			
			return response()->json(['status'=>'success', 'message'=>'Saved successfully.']);
		}else{
			return response()->json(['status'=>'error', 'message'=>'Please add questions.']);
		}
    }

	function add_child_question($common, $visit_form_id, $value, $parent_id, $order){ //recursive function
		$order++;
		
		$q_id = $common->add_question($visit_form_id, $value, $parent_id, $order); //order is 0 for child questions
			
		if($value['type'] == 'yes_no' || $value['type'] == 'single' || $value['type'] == 'multiple'){
			
			//for Options
			if(isset($value['options']) && count($value['options']) > 0){
				$options = $value['options'];
				
				foreach($options as $k => $v){
					
					$op_id = $common->add_option($visit_form_id, $q_id, $v);

					//for child questions
					if(isset($v['child_array']) && count($v['child_array']) > 0){
						$child_array = $v['child_array'];
						
						foreach($child_array as $c => $val){
							$order = $this->add_child_question($common, $visit_form_id, $val, $op_id, $order); //op_id is now parent id for other question
						}
					}
				}
			}
		}
		
		return $order;
	}
	
    public function edit($id){
        $visit_form = VisitForm::find($id);
		if($visit_form && $visit_form->count() > 0){
			$data['services'] = Service::where('id', $visit_form->service_id)->get();
			$data['visit_form'] = $visit_form;
			$data['form_field_types'] = VisitFormFieldType::select('*')->where('status',1)->get();
			$data['page'] = 'visit_forms';
			return view('admin.visit_form.add', $data);
		} else {
			return redirect()->route('admin.visit_forms');
		}
    }
	
    public static function getOptions($vid,$qid){
        $options = QuestionOption::where('visit_form_id', $vid)->where('question_id', $qid)->where('status',1)->get();
        return $options;
    }
		
    public function destroy($id){
        VisitForm::find($id)->delete();
        return response()->json(['status'=>'success', 'message'=>'Deleted successfully.']);
    }
	
    public function destroy_field($id){
        VisitFormField::find($id)->delete();
        return response()->json(['status'=>'success', 'message'=>'Deleted successfully.']);
    }

    public function destroy_question($id){
        VisitQuestion::find($id)->delete();
        return response()->json(['status'=>'success', 'message'=>'Deleted successfully.']);
    }

    public function destroy_option($id){
        QuestionOption::find($id)->delete();
        return response()->json(['status'=>'success', 'message'=>'Deleted successfully.']);
    }
	
}
