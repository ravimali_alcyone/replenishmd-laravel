<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\AllPlanFeatures;
use DataTables;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

class PlanFeaturesController extends Controller {

    public function __construct(){
        $this->middleware('auth');
	}

    public function index(Request $request){

		if ($request->ajax()) {
			$data = AllPlanFeatures::select('*');

			$data = $data->latest()->get();

            return Datatables::of($data)->addIndexColumn()
				->addColumn('action', function($row){
					$edit_url = env('APP_URL').'/admin/plan_features/add/'.$row->id;

					$btn = '<div class="flex items-center"><a href="'.$edit_url.'" class="button button--sm w-16 shadow-md mr-1 mb-2 btn_blue text-white editUser" data-id="23" title="Edit"> Edit </a>';
					$btn = $btn.'<div class="flex items-center"><a href="javascript:void(0)" onclick="deleteRow('.$row->id.')" class="button button--sm w-16 shadow-md mr-1 mb-2 btn_red text-white deleteUser" data-id="23" title="Edit"> Delete </a>';
					return $btn;
				})
				->addColumn('detail', function($row){
					$detail = '';
					
					if($row->detail != ''){
						$detail = strlen($row->detail) > 50 ? substr($row->detail,0,50)."..." : $row->detail;
					}
					return $detail;
				})
				->addColumn('status', function($row){
					if($row->status == 1){
						$status = '<input type="checkbox" name="status" class="input input--switch border"  title="Status" checked onchange="changeStatus('.$row->id.')">';
					}else{
						$status = '<input type="checkbox" name="status" class="input input--switch border"  title="Status" onchange="changeStatus('.$row->id.')">';
					}
					return $status;
				})				
				->rawColumns(['status','detail','action'])
				->make(true);
        }
		$data['page'] = 'plan_features';
        return view('admin.plan_features.index', $data);
    }

	public function add(){
		$data['page'] = 'plan_features';
		$data['plan_features'] = array();
		return view('admin.plan_features.add', $data);
	}

    public function store(Request $request){
		$req = $request->all();
		
		if ($request->status == 'on') {
			$status = 1;
		} else {
			$status = 0;
		}
		
		$where = ['id' => $req['id']];
		$input_data = [
			'name' => $req['name'],
			'price' => $req['price'],
			'detail' => $req['detail'],
			'status' => $status
		];

		AllPlanFeatures::updateOrCreate($where, $input_data);
		
		
        return response()->json(['status'=>'success', 'message'=>'Saved successfully.']);
    }

    public function edit($id){
        $plan_features = AllPlanFeatures::find($id);
		
		if($plan_features && $plan_features->count() > 0){
			$data['plan_features'] = $plan_features;
			$data['page'] = 'plan_features';
			return view('admin.plan_features.add', $data);
		} else {
			return redirect()->route('admin.plan_features');
		}
    }

    public function destroy($id){
        AllPlanFeatures::find($id)->delete();
        return response()->json(['status'=>'success', 'message'=>'Deleted successfully.']);
    }
	
	public function change_status(Request $request) {
		$user = AllPlanFeatures::where('id', $request->id)->first();
		if ($user->status) {
			$user = AllPlanFeatures::where('id', $request->id)->update(array('status' => 0));
		} else {
			$user = AllPlanFeatures::where('id', $request->id)->update(array('status' => 1));
		}
	}	
}
