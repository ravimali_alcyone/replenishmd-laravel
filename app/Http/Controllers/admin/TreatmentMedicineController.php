<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Library\Services\StripeService;
use App\TreatmentMedicine;
use App\MedicineFaq;
use App\MedicineVariant;
use App\Service;
use DataTables;
use Stripe;

class TreatmentMedicineController extends Controller {

    public function __construct(){
        $this->middleware('auth');
    }

    public function index(Request $request){
		
		if ($request->ajax()) {

			$data = TreatmentMedicine::select('*');

			if($request->filter_service != ''){
				$data = $data->where('service_id',$request->filter_service);
			}

			if($request->filter_status != ''){
				$data = $data->where('status',$request->filter_status);
			}

			 $data = $data->latest()->get();

            return Datatables::of($data)
				->addIndexColumn()
				->addColumn('action', function($row){
					$view_url = env('APP_URL').'/admin/treatment_medicines/view/'.$row->id;
					$edit_url = env('APP_URL').'/admin/treatment_medicines/edit/'.$row->id;
					$btn = '<div class="flex items-center"><a href="'.$view_url.'" data-id="'.$row->id.'" title="View" class="button button--sm w-16 shadow-md mr-1 mb-2 btn_gray viewUser">View</a>';
					$btn = $btn.'<a href="'.$edit_url.'"   data-id="'.$row->id.'" title="Edit" class="button button--sm w-16 shadow-md mr-1 mb-2 btn_blue viewUser">Edit</a>';
					$btn = $btn.' <a href="javascript:void(0)" onclick="deleteRow('.$row->id.')" data-id="'.$row->id.'" title="Delete" class="button button--sm w-16 shadow-md mr-1 mb-2 btn_red deleteUser">Delete</a></div>';
					return $btn;
				})
				->addColumn('service', function($row){

					$service = Service::where('id', $row->service_id)->pluck('name')->first();

					$data = $service;
					return $data;
				})

				->addColumn('image', function($row){
					$images_arr = json_decode($row->images);
					$images = '<div class="flex">';
					if ($images_arr != null) {
						foreach ($images_arr as $key => $value) {
							$images .= '<a href="/'.$value.'" data-lightbox="lightbox-set-'.$row->id.'">
							<div class="w-10 h-10 image-fit zoom-in -ml-5">
									<img class="tooltip rounded-full tooltipstered" src="/'.$value.'">
							</div>
							</a>';
						}
					}
					$images	.= '</div>';
					return $images;
				})
				->addColumn('start_price', function($row){
					$price = '$'.$row->start_price;
					return $price;
				})
				->addColumn('status', function($row){
					if($row->status == 1){
						$status = '<input type="checkbox" name="status" class="input input--switch border"  title="Status" checked onchange="changeStatus('.$row->id.')">';
					}else{
						$status = '<input type="checkbox" name="status" class="input input--switch border"  title="Status" onchange="changeStatus('.$row->id.')">';
					}
					return $status;
				})
				->rawColumns(['name','image','service_id','start_price','status','action'])
				->make(true);
        }

		$data['page'] = 'treatment_medicines';
		$data['services'] = Service::where('status', 1)->get();
        return view('admin.treatment_medicine.index', $data);
    }

	public function add(){
		$data['treatment_medicine'] = array();
		$data['page'] = 'treatment_medicines';
		$data['services'] = Service::where('status', 1)->get();
		$data['variant_rows'] = array();
		$data['faq_rows'] = array();
		return view('admin.treatment_medicine.add', $data);
	}

    public function store(Request $request, StripeService $ss){

		$req = $request->all();
		
		$medicine_id = '';
		$service_id = '';
		$product_stripe_id = '';
		$variant_rows = array();
		
		if ($request->get('page') == 'edit') {
			if (!empty($request->input('old_image'))) {
				foreach($request->input('old_image') as $key => $value) {
					$images[] = $value;
				}
			}
		}

		if (!empty($request->file('images'))) {
            foreach($request->file('images') as $key => $file) {
				$name='medicine-'.time().'-'.rand(11111111,99999999).'.'.strtolower($file->getClientOriginalExtension());
                $destinationPath = 'uploads/treatment_medicines/';
                $file->move($destinationPath, $name);
                $images[$key] = 'uploads/treatment_medicines'.'/'.$name;
            }
        }

		if($request->status == 'on') { $status = 1; } else { $status = 0; }

		if($request->service_id == ''){ $service_id = 0; } else { $service_id = $request->service_id; }
		
		$input_data = [
			'name' => $request->name,
			'other_name' => $request->other_name,
			'chemical_name' => $request->chemical_name,
			'service_id' => $service_id,
			'start_price' => $request->start_price,
			'description' => $request->description,
			'status' => $status,
			'images' => isset($images) ? json_encode($images) : ''
		];
		
		$faq_rows = array();
		
		if(isset($req['faq_data']) && count($req['faq_data']) > 0){
			foreach($req['faq_data'] as $type => $value){
				if($type == 'question'){
					$i=0;
					foreach($value as $row){
						$faq_rows[$i]['question_data'] = $row;
						$i++;
					}
				}elseif($type == 'answer'){
					$j=0;
					foreach($value as $row){
						$faq_rows[$j]['answer_data'] = $row;
						$j++;
					}
				}
			}
		}
		
		if(isset($req['variants']) && count($req['variants']) > 0){
			
			//echo '<pre>';print_r($req['variants']);die;
 			//$v=0;
			foreach($req['variants'] as $type => $val){
				$j=0;
				  foreach($val as $row){
					$variant_rows[$j][$type] = $row;
					$j++;
				}  				
					 				
			} 
		}
		
		//echo '<pre>';print_R($variant_rows);die;
		if ($request->get('page') == 'add') {
										
			//Medicine(Product) Add
			$product_data['name'] = $input_data['name']; //string
			$product_data['active'] = $input_data['status'] == 1 ? true : false; //boolean
			$product_data['images'] = $images ? [env('APP_URL').'/'.$images[0]] : array(); //array
			
			$sProduct = $ss->createProduct($product_data);
			
			//echo '<Pre>';print_r($sProduct); die;
			
			if($sProduct){
				$product_stripe_id = $input_data['product_stripe_id'] = $sProduct->id;
			}
			
			$treatment_medicine = TreatmentMedicine::create($input_data);
			
			if ($treatment_medicine->save()) {
				$medicine_id = $treatment_medicine->id;
												
				//Medicine FAQ Data
				if($faq_rows){
					foreach($faq_rows as $key => $value){
						$faq_data = array(
							'medicine_id' => $medicine_id,
							'question_data' => $value['question_data'],
							'answer_data' => $value['answer_data'],
						);
						MedicineFaq::create($faq_data);
					}
				}else{
					MedicineFaq::where('medicine_id',$medicine_id)->delete();			
				}
				
			}
			
		} elseif ($request->get('page') == 'edit') {
			
			$treatment_medicine = TreatmentMedicine::where('id', $request->id)->first();
						
			//Medicine(Product) Add
			$product_data['name'] = $input_data['name']; //string
			$product_data['active'] = $input_data['status'] == 1 ? true : false; //boolean
			$product_data['images'] = $images ? [env('APP_URL').'/'.$images[0]] : array(); //array
			
			if($treatment_medicine->product_stripe_id != ''){
				$sProduct = $ss->updateProduct($treatment_medicine->product_stripe_id,$product_data);
			}else{
				$sProduct = $ss->createProduct($product_data);
			}
			
			//echo '<Pre>';print_r($sProduct); die;
			
			if($sProduct){
				$product_stripe_id = $input_data['product_stripe_id'] = $sProduct->id;
			}
			
			TreatmentMedicine::where('id', $request->id)->update($input_data);
			$medicine_id = $request->id;
									
			//Medicine FAQ Data
			MedicineFaq::where('medicine_id',$medicine_id)->delete();
			
			if($faq_rows){
								
				foreach($faq_rows as $key => $value){
					$faq_data = array(
						'medicine_id' => $medicine_id,
						'question_data' => $value['question_data'],
						'answer_data' => $value['answer_data'],
					);
					MedicineFaq::create($faq_data);
				}
			}
			
		}
		
		//Medicine Variant Data
		//echo '<Pre>';print_r($variant_rows); die;
		if($variant_rows && $medicine_id != ''){

			foreach($variant_rows as $k => $v){
				
 				$is_recurring = $v['is_recurring'];
				$plan_interval = isset($v['plan_interval']) ? $v['plan_interval'] : NULL;
				$custom_plan_interval = !empty($v['custom_plan_interval']) ? $v['custom_plan_interval'] : '';
				$interval_count = isset($v['interval_count']) ? $v['interval_count'] : NULL;

				$v_input_data = $v;
				$v_input_data['product_stripe_id'] = $product_stripe_id;				
				$v_input_data['plan_interval'] = $plan_interval;				
				$v_input_data['custom_plan_interval'] = $custom_plan_interval;				
				$v_input_data['interval_count'] = $interval_count;				
				
				$v_data = array(
					'medicine_id' => $medicine_id,
					'service_id' => $service_id,
					'variant_name' => $v['name'],
					'price' => $v['price'],
					'pill_qty' => $v['qty'],
					'is_popular' => isset($v['is_popular']) && $v['is_popular'] == 'on' ? 1 : 0,
					'details' => $v['detail'],
					'is_recurring' => $is_recurring,
					'plan_interval' => ($is_recurring == 1 || $is_recurring == 2) ? $plan_interval : NULL,
					'custom_plan_interval' => (($is_recurring == 1 || $is_recurring == 2) && $plan_interval == 'custom') ? $custom_plan_interval : '',
					'interval_count' => (($is_recurring == 1 || $is_recurring == 2) && $plan_interval == 'custom') ? $interval_count : 1,
				);
				
 				if($product_stripe_id != ''){
					
					if($v['id'] != ''){
 						$myVariant = MedicineVariant::where('id',$v['id'])->first();
												
						//WILL BE COMMENTED
						if(($is_recurring == 1 || $is_recurring == 2) && $myVariant->plan_variant_stripe_id == NULL){
							$planVariant = $ss->createStripePlan($v_input_data);
							$v_data['plan_variant_stripe_id'] = $planVariant->id;
						}
						
					}else{
					
						if($is_recurring == 1 || $is_recurring == 2){
							$planVariant = $ss->createStripePlan($v_input_data);
							$v_data['plan_variant_stripe_id'] = $planVariant->id;
						}
												
					}
				} 
			
				//echo '<pre>';print_r($v_data);echo '</pre>';	
 				if($v['id'] == ''){
					MedicineVariant::create($v_data);
				}else{
					MedicineVariant::where('id',$v['id'])->update($v_data);
				} 
				
			}

			return response()->json(['status'=>'success', 'message'=>'Saved Successfully.']);
		}else{
			return response()->json(['status'=>'error', 'message'=>'Please add atleast one variant for this medicine.']);
		}		
			
    }

    public function edit($id){
				
        $treatment_medicine = TreatmentMedicine::find($id);
		$data['faq_rows'] = array();
		$data['variant_rows'] = array();
		
		if($treatment_medicine && $treatment_medicine->count() > 0){
			$data['page'] = 'treatment_medicines';
			$data['services'] = Service::where('status', 1)->get();
			$data['treatment_medicine'] = $treatment_medicine;
			$data['faq_rows'] = MedicineFaq::where('medicine_id',$treatment_medicine->id)->get();
			$data['variant_rows'] = MedicineVariant::where('medicine_id',$treatment_medicine->id)->get();
						
			return view('admin.treatment_medicine.add', $data);
		} else {
			return redirect()->route('admin.treatment_medicines');
		}
    }

    public function destroy($id){
        TreatmentMedicine::find($id)->delete();
        MedicineFaq::where('medicine_id',$id)->delete();
        MedicineVariant::where('medicine_id',$id)->delete();
        return response()->json(['status'=>'success', 'message'=>'Deleted successfully.']);
	}
	
    public function variant_destroy($id, StripeService $ss){
		
        $medicineVariant = MedicineVariant::where('id',$id)->first();
		
		if($medicineVariant && $medicineVariant->count() > 0){
			
			
			if($medicineVariant->is_recurring == 1 || $medicineVariant->is_recurring == 2){
				$ss->deleteStripePlan($medicineVariant->plan_variant_stripe_id);
			}
			
			MedicineVariant::find($id)->delete();
			
			return response()->json(['status'=>'success', 'message'=>'Deleted successfully.']);
			
		}else{
			return response()->json(['status'=>'error', 'message'=>'Error Occured.']);
		}
		
	}	


	public function change_status(Request $request) {
		$user = TreatmentMedicine::where('id', $request->id)->first();
		if ($user->status) {
			$user = TreatmentMedicine::where('id', $request->id)->update(array('status' => 0));
		} else {
			$user = TreatmentMedicine::where('id', $request->id)->update(array('status' => 1));
		}
	}

	public function view($id) {
		$treatment_medicine = TreatmentMedicine::where('id', $id)->first();
		$service_name = Service::where('id', $treatment_medicine->service_id)->select('name')->first();
		$data['treatment_medicine'] = $treatment_medicine;
		$data['images'] = json_decode($treatment_medicine->images);
		$data['service'] = $service_name->name;
		$data['page'] = 'treatment_medicines';
		return view('admin.treatment_medicine.view', $data);
	}
	
	public function imageUpload(Request $request){
		if($request->hasFile('upload')) {
            $originName = $request->file('upload')->getClientOriginalName();
            $fileName = pathinfo($originName, PATHINFO_FILENAME);
            $extension = $request->file('upload')->getClientOriginalExtension();
            $fileName = $fileName.'_'.time().'.'.$extension;
        
            $request->file('upload')->move(public_path('images'), $fileName);
   
            $CKEditorFuncNum = $request->input('CKEditorFuncNum');
            $url = asset('images/'.$fileName); 
            $msg = 'Image uploaded successfully'; 
            $response = "<script>window.parent.CKEDITOR.tools.callFunction($CKEditorFuncNum, '$url', '$msg')</script>";
               
            @header('Content-type: text/html; charset=utf-8'); 
            echo $response;
        }
	}
}
