<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Service;
use DataTables;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

class ServiceController extends Controller {

    public function __construct(){
        $this->middleware('auth');
	}

    public function index(Request $request){

		if ($request->ajax()) {
			$data = Service::select('*');

			$data = $data->get();

            return Datatables::of($data)->addIndexColumn()
				->addColumn('action', function($row){
					$edit_url = env('APP_URL').'/admin/services/add/'.$row->id;

					$btn = '<div class="flex items-center"><a href="'.$edit_url.'" class="button button--sm w-16 shadow-md mr-1 mb-2 btn_blue text-white editUser" data-id="23" title="Edit"> Edit </a>';
					$btn = $btn.'<div class="flex items-center"><a href="javascript:void(0)" onclick="deleteRow('.$row->id.')" class="button button--sm w-16 shadow-md mr-1 mb-2 btn_red text-white deleteUser" data-id="23" title="Edit"> Delete </a>';
					return $btn;
				})
				->addColumn('visit_type', function($row){
					
					if($row->visit_type == '1'){
						$visit_type = 'Asynchronous Telemedicine';
					}elseif($row->visit_type == '2'){
						$visit_type = 'Synchronous Telemedicine';
					}elseif($row->visit_type == '3'){
						$visit_type = 'Concierge';
					}else{
						$visit_type = '-';
					}
					return $visit_type;
				})	

				->addColumn('available_for', function($row){
					
					if($row->available_for != ''){
						$available_for = ucfirst($row->available_for);
					}else{
						$available_for = '';
					}
					return $available_for;
				})	
				
				->addColumn('status', function($row){
					if($row->status == 1){
						$status = '<input type="checkbox" name="status" class="input input--switch border"  title="Status" checked onchange="changeStatus('.$row->id.')">';
					}else{
						$status = '<input type="checkbox" name="status" class="input input--switch border"  title="Status" onchange="changeStatus('.$row->id.')">';
					}
					return $status;
				})				
				->rawColumns(['available_for','visit_type','status','action'])
				->make(true);
        }
		$data['page'] = 'services';
        return view('admin.services.index', $data);
    }

	public function add(){
		$data['service'] = array();
		$data['page'] = 'services';
		return view('admin.services.add', $data);
	}

    public function store(Request $request){
		$req = $request->all();

		if ($request->status == 'on') {
			$status = 1;
		} else {
			$status = 0;
		}
		
		if ($request->get('page') == 'edit') {
			if (!empty($request->input('old_image'))) {
				$image = $request->input('old_image');
			}
		}
		
		if (!empty($request->file('image'))) {
            $file = $request->file('image');
			$name=time().'.'.$file->getClientOriginalName();
			$destinationPath = 'uploads/services/';
			$file->move($destinationPath, $name);
			$image = 'uploads/services'.'/'.$name;
            
        }
		
		$where = ['id' => $req['id']];
		$input_data = [
			'name' => $req['name'],
			'visit_type' => $req['visit_type'],
			'available_for' => $req['available_for'],
			'short_info' => $req['short_info'],
			'description' => $req['description'],
			'image' => isset($image) ? $image : 'images/default.jpg',
			'status' => $status,
		];

		Service::updateOrCreate($where, $input_data);
        return response()->json(['status'=>'success', 'message'=>'Saved successfully.']);
    }

    public function edit($id){
        $service = Service::find($id);
		
		if($service && $service->count() > 0){
			$data['service'] = $service;
			$data['page'] = 'services';
			return view('admin.services.add', $data);
		} else {
			return redirect()->route('admin.services');
		}
    }

    public function destroy($id){
        Service::find($id)->delete();
        return response()->json(['status'=>'success', 'message'=>'Deleted successfully.']);
    }
	
	public function change_status(Request $request) {
		$user = Service::where('id', $request->id)->first();
		if ($user->status) {
			$user = Service::where('id', $request->id)->update(array('status' => 0));
		} else {
			$user = Service::where('id', $request->id)->update(array('status' => 1));
		}
	}	
}
