<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Section;

class DataStoreController extends Controller {
	
    # This Method Created for Insert & Update Sections Data.
    public function insertAndUpdateSection(Request $request) {
        $data = array(
            'section_name' => $request->input('section_name'),
            'page_name' => $request->input('page_name'),
            'language' => 'en', //$request->input('lang'),
            'section_content' => json_encode($request->input('section_content')),
            'updated_at' => date("Y-m-d")
        );
        $model = new Section;
        $result = $model->insertAndUpdateSection($data);
        return response()->json(['msg' => true]);
    }

    # This Method Created for File Uploading.
    public function FilesUpload(Request $request) {
        $images_data = array();
        if(!empty($request->input('existing_image'))){
            foreach ($request->input('existing_image') as $key => $value) {

                if (!empty($request->file($key))) {

                    # Remove Old Image
                    $old_image = array_values(array_filter($request->input('old_image')));
                    if ($old_image) {
                        foreach ($old_image as $key2 => $value2) {
                            $file_path = public_path($value);
                            if (file_exists($file_path)) {
                                unlink($file_path);
                            }
                        }
                    }

                    $file = $request->file($key);
                    $name = rand(1, 1000);
                    $name = $name.'_'.time().'.'.$file->getClientOriginalExtension();
                    $destinationPath = 'images/uploads/';
                    $file->move($destinationPath, $name);
                    $images_data[$key] = 'images/uploads'.'/'.$name;
                }else{
                    $images_data[$key] = $value;
                }
            }
            return response()->json($images_data);
        }
    }

    # This Method Created For Upload Multiple Images.
    public function multiSectionImageUpload(Request $request) {
        $data = array();

        if (!empty($request->input('old_image'))) {
            $old_images = array_values(array_filter($request->input('old_image')));
        }

        if (!empty($request->file('images'))) {
            if (!empty($request->input('old_image'))) {
                foreach($request->input('old_image') as $old_image) {
                    $data[] = $old_image;
                }
            }

            foreach($request->file('images') as $key => $file) {
                $name = rand(1, 1000);
                $name = $name.'_'.time().'.'.$file->getClientOriginalExtension();
                $destinationPath = 'images/uploads/';
                $file->move($destinationPath, $name);
                $data[$key] = 'images/uploads'.'/'.$name;

                # Remove Old Image
                if (isset($old_images)) {
                    if (isset($old_images[$key]) && !empty($old_images[$key])) {
                        $file_path = realpath($old_images[$key]);
                        if (file_exists($file_path)) {
                            unlink($file_path);
                        }
                    }
                }
            }
        } else {
            if (!empty($request->input('old_image'))) {
                foreach($request->input('old_image') as $old_image) {
                    $data[] = $old_image;
                }
            }
        }
        return response()->json($data);
    }

    # This Method Created For Upload Multiple Images.
    public function MultipalFilesUpload(Request $request) {
        $data = array();
        if (!empty($request->file('images'))) {
            foreach($request->file('images') as $file) {
                $name = rand(1, 1000);
                $name = $name.'_'.time().'.'.$file->getClientOriginalExtension();
                $destinationPath = 'images/uploads/';
                $file->move($destinationPath, $name);
                $data[] = 'images/uploads'.'/'.$name;
            }

            # Remove Old Images
            // $old_images = $request->input('old_image');

            // if (!empty($old_images)) {
            //     foreach ($old_images as $key => $value) {
            //         $file_path = public_path($value);
            //         if (file_exists($file_path)) {
            //             unlink($file_path);
            //         }
            //     }
            // }
        } else {
            if (!empty($request->input('old_image'))) {
                foreach($request->input('old_image') as $old_image) {
                    $data[] = $old_image;
                }
            }
        }
        return response()->json($data);
    }
}
