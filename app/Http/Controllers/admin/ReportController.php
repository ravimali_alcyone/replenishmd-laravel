<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Library\Services\CommonService;
use App\TreatmentMedicine;
use App\MedicineVariant;
use App\Service;
use App\User;
use App\ProviderCategory;
use App\ProviderService;
use App\OnlineVisit;
use DataTables;
use DB;

class ReportController extends Controller {

    public function __construct(){
        $this->middleware('auth');
    }

	public function index(Request $request){
		//nothing
	}
	
    public function prescription_products(Request $request){
		
		if ($request->ajax()) {

			$data = MedicineVariant::select('medicine_variants.variant_name AS variant_name', 'treatment_medicines.name AS medicine_name', 'services.name AS service', 'medicine_variants.price AS variant_price', 'medicine_variants.pill_qty AS variant_qty')
				->join('treatment_medicines', function ($join) {
						$join->on('treatment_medicines.id', '=', 'medicine_variants.medicine_id');
					})
				->join('services', function ($join) {
						$join->on('services.id', '=', 'medicine_variants.service_id');
					});
					
			if($request->filter1 != ''){
				$data = $data->where('medicine_variants.service_id',$request->filter1);
			}

			if($request->filter2 != ''){
				$data = $data->where('medicine_variants.medicine_id',$request->filter2);
			}

			$data = $data->get();

            return Datatables::of($data)
				->addIndexColumn()
				->rawColumns(['variant_name','medicine_name','service','variant_price','variant_qty'])
				->make(true);
        }

		$data['parent_page'] = 'reports';
		$data['parent_page_heading'] = 'Reports';
		$data['page'] = 'Prescription_products';
		$data['page_heading'] = 'Product Data';
		$data['services'] = Service::where('status', 1)->get();
		$data['medicines'] = TreatmentMedicine::where('status', 1)->get();
		$data['heading'] =  "Reports - Prescription Product Data";
		$data['doc_heading'] =  "ReplenishMD Reports";
		$data['doc_heading2'] =  "Prescription Product Variants";
        return view('admin.reports.prescription_products', $data);
    }
	
    public function providers(Request $request, CommonService $common){
		
		if ($request->ajax()) {

			$where = array('is_admin' => 0,	'user_role' => 3);

			if($request->filter1 != ''){
				$whereRaw['column'] = 'users.category';
				$whereRaw['value'] = $request->filter1;
			}else{
				$whereRaw = array();
			}
			
			if($request->filter2 != ''){
				$where['users.status'] = $request->filter2;
			}

			if($request->filter3 != ''){
				$where['provider_services.service_id'] = $request->filter3;
			}			

			$whereDate1 = $whereDate2 = array();
			
			if($request->filter4 != ''){
				$whereDate1['column'] = 'users.created_at';
				$whereDate1['cond'] = '>=';
				$whereDate1['value'] = $request->filter4;
			}
			
			if($request->filter5 != ''){
				$whereDate2['column'] = 'users.created_at';
				$whereDate2['cond'] = '<';
				$whereDate2['value'] = $request->filter5;
			}
			
			$data = $common->getProviderReportData($request, $table='users', $join_table='provider_services', $where, $whereRaw, $whereDate1, $whereDate2, $isLatest=false);

            return Datatables::of($data)
				->addIndexColumn()
				->addColumn('gender', function($row){

					return ucfirst($row->gender);
				})	
				->addColumn('category', function($row){

					$categories = explode(',', $row->category);
					$category_names = DB::table('provider_categories')->whereIn('id', $categories)->select('name')->get();
					$categories_arr = array();
					
					foreach ($category_names as $key => $value) {
						array_push($categories_arr, $value->name);
					}
					
					if($categories_arr){
						$categories_arr = array_values(array_unique($categories_arr));
						$categoryString = implode(', ', $categories_arr);
					}else{
						$categoryString = '';
					}
					return $categoryString;
				})
				->addColumn('service', function($row){

					$services = ProviderService::select('services.name')
									->join('services', function ($join) {
											$join->on('services.id', '=', 'provider_services.service_id');
										})					
									->where('provider_services.provider_id', $row->id)
									->where('provider_services.status', 1)
									->orderBy('provider_services.provider_id','ASC')
									->get();
					$services_arr = array();
					
					if($services && $services->count() > 0){
						foreach ($services as $key => $value) {
							array_push($services_arr, $value->name);
						}
					}
					
					if($services_arr){
						$services_arr = array_values(array_unique($services_arr));
						$serviceString = implode(', ', $services_arr);
					}else{
						$serviceString = '';
					}
					
					return $serviceString;
				})	
				->addColumn('created_at', function($row){
					return $row->created_at !='' ? date(env('DATE_FORMAT_PHP'), strtotime($row->created_at)) : '';
				})					
				->rawColumns(['name','gender','phone','category','service','created_at'])
				->make(true);
        }

		$data['parent_page'] = 'reports';
		$data['parent_page_heading'] = 'Reports';
		$data['page'] = 'report_providers';
		$data['page_heading'] = 'Provider Data';
		$data['categories'] = ProviderCategory::where('parent_id', 0)->where('status', 1)->get();
		$data['services'] = Service::where('status', 1)->get();
		$data['heading'] =  "Reports - Provider Data";
		$data['doc_heading'] =  "ReplenishMD Reports";
		$data['doc_heading2'] =  "Providers";
        return view('admin.reports.providers', $data);
    }	
	
    public function patients(Request $request, CommonService $common){
		
		if ($request->ajax()) {

			$where = array('is_admin' => 0,	'user_role' => 4);
			
			if($request->filter1 != ''){
				$where['online_visits.service_id'] = $request->filter1;
			}			
			
			if($request->filter2 != ''){
				$where['users.status'] = $request->filter2;
			}			

			$whereDate1 = $whereDate2 = array();
			
			if($request->filter3 != ''){
				$whereDate1['column'] = 'users.created_at';
				$whereDate1['cond'] = '>=';
				$whereDate1['value'] = $request->filter3;
			}
			
			if($request->filter4 != ''){
				$whereDate2['column'] = 'users.created_at';
				$whereDate2['cond'] = '<';
				$whereDate2['value'] = $request->filter4;
			}
			
			$data = $common->getPatientReportData($request, $table='users', $join_table='online_visits', $where, $whereDate1, $whereDate2, $isLatest=false);

            return datatables::of($data)
				->addindexcolumn()
				->addColumn('gender', function($row){
					return ucfirst($row->gender);
				})	
				->addColumn('service', function($row){

					$services = OnlineVisit::select('services.name')
									->join('services', function ($join) {
											$join->on('services.id', '=', 'online_visits.service_id');
										})					
									->where('online_visits.patient_id', $row->id)
									->orderBy('online_visits.service_id','ASC')
									->get();
					$services_arr = array();
					
					if($services && $services->count() > 0){
						foreach ($services as $key => $value) {
							array_push($services_arr, $value->name);
						}
					}
					
					if($services_arr){
						$services_arr = array_values(array_unique($services_arr));
						$serviceString = implode(', ', $services_arr);
					}else{
						$serviceString = '';
					}
					
					return $serviceString;
				})	
				->addColumn('created_at', function($row){
					return $row->created_at !='' ? date(env('DATE_FORMAT_PHP'), strtotime($row->created_at)) : '';
				})					
				->rawColumns(['name','gender','phone','service','created_at'])
				->make(true);
        }

		$data['parent_page'] = 'reports';
		$data['parent_page_heading'] = 'Reports';
		$data['page'] = 'report_patients';
		$data['page_heading'] = 'Patient Data';
		$data['services'] = Service::where('status', 1)->get();
		$data['heading'] =  "Reports - Patient Data";
		$data['doc_heading'] =  "ReplenishMD Reports";
		$data['doc_heading2'] =  "Patients";
        return view('admin.reports.patients', $data);
    }	

}
