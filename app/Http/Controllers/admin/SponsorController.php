<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Sponsor;
use DataTables;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

class SponsorController extends Controller {

    public function __construct(){
        $this->middleware('auth');
	}

    public function index(Request $request){

		if ($request->ajax()) {
			$data = Sponsor::select('*');

			$data = $data->get();

            return Datatables::of($data)->addIndexColumn()
				->addColumn('action', function($row){
					$edit_url = env('APP_URL').'/admin/sponsors/add/'.$row->id;

					$btn = '<div class="flex items-center"><a href="'.$edit_url.'" class="button button--sm w-16 shadow-md mr-1 mb-2 btn_blue text-white editUser" data-id="23" title="Edit"> Edit </a>';
					$btn = $btn.'<div class="flex items-center"><a href="javascript:void(0)" onclick="deleteRow('.$row->id.')" class="button button--sm w-16 shadow-md mr-1 mb-2 btn_red text-white deleteUser" data-id="23" title="Edit"> Delete </a>';
					return $btn;
				})
				->addColumn('image', function($row){
					$image = '<div class="flex">';
					
					if ($row->image != null) {
						$image .= '<a href="/'.$row->image.'" data-lightbox="lightbox-set-'.$row->id.'"><div class="w-20 h-20 image-fit zoom-in">
							<img class="tooltip rounded-full tooltipstered" src="/'.$row->image.'">
						</div></a>';
						
					}
					$image	.= '</div>';
					return $image;
				})			
				->addColumn('status', function($row){
					if($row->status == 1){
						$status = '<input type="checkbox" name="status" class="input input--switch border"  title="Status" checked onchange="changeStatus('.$row->id.')">';
					}else{
						$status = '<input type="checkbox" name="status" class="input input--switch border"  title="Status" onchange="changeStatus('.$row->id.')">';
					}
					return $status;
				})				
				->rawColumns(['image','status','action'])
				->make(true);
        }
		$data['parent_page'] = 'website_content';
		$data['page'] = 'sponsors';
        return view('admin.sponsors.index', $data);
    }

	public function add(){
		$data['sponsor'] = array();
		$data['parent_page'] = 'website_content';
		$data['page'] = 'sponsors';
		return view('admin.sponsors.add', $data);
	}

    public function store(Request $request){
		$req = $request->all();

		if ($request->get('page') == 'edit') {
			if (!empty($request->input('old_image'))) {
				$image = $request->input('old_image');
			}
		}

		if (!empty($request->file('image'))) {
				$file = $request->file('image');
                $name = time().'.'.$file->getClientOriginalName();
                $destinationPath = 'uploads/sponsors/';
                $file->move($destinationPath, $name);
                $image = 'uploads/sponsors'.'/'.$name;
        }
		
		if ($request->status == 'on') {
			$status = 1;
		} else {
			$status = 0;
		}
		
		$where = ['id' => $req['id']];
		$input_data = [
			'name' => $req['name'],
			'image' => isset($image) ? $image : 'images/default.jpg',
			'status' => $status,
		];

		Sponsor::updateOrCreate($where, $input_data);
        return response()->json(['status'=>'success', 'message'=>'Saved successfully.']);
    }

    public function edit($id){
        $sponsor = Sponsor::find($id);
		
		if($sponsor && $sponsor->count() > 0){
			$data['sponsor'] = $sponsor;
			$data['parent_page'] = 'website_content';
			$data['page'] = 'sponsors';
			return view('admin.sponsors.add', $data);
		} else {
			return redirect()->route('admin.sponsors');
		}
    }

    public function destroy($id){
        Sponsor::find($id)->delete();
        return response()->json(['status'=>'success', 'message'=>'Deleted successfully.']);
    }
	
	public function change_status(Request $request) {
		$user = Sponsor::where('id', $request->id)->first();
		if ($user->status) {
			$user = Sponsor::where('id', $request->id)->update(array('status' => 0));
		} else {
			$user = Sponsor::where('id', $request->id)->update(array('status' => 1));
		}
	}	
}
