<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\TreatmentMedicine;
use App\MedicineVariant;
use App\MasterSetting;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index() {
        $data['page'] = 'dashboard';
		$data['total_patients'] = User::select('id')
						->where('user_role', 4)
						->get();
		$data['total_products'] = TreatmentMedicine::select('id')
						->where('status', 1)
						->get();
		$data['total_sales'] = MedicineVariant::select('id')
						->where('status', 1)
						->get();
        return view('admin.home', $data);
    }
	
    public function profile() {
        $data['page'] = 'profile';
		$data['user'] = auth()->user();
		$data['setting'] = MasterSetting::select('*')->where('status',1)->get();
        return view('admin.profile', $data);
    }

    public function change_password(Request $request){
		$req = $request->all();
        $this->validate($request, [
			'old_password' => ['required'],
			'password' => ['required', 'string', 'min:6', 'confirmed','different:old_password'],
			
        ]);
		
		if (!Hash::check($req['old_password'], auth()->user()->password)) {
			return response()->json(['status'=>'error', 'message'=>'Incorrect Old Password']);
		}
		
		
		$password = Hash::make($req['password']);

		$input_data = [
			'password' => $password,
			'updated_at' => date('Y-m-d H:i:s')
		];

        User::where('id',auth()->user()->id)->update($input_data);
		
        return response()->json(['status'=>'success', 'message'=>'Password Changed Successfully.']);
    }	
	
    public function contact_details(Request $request){
		$req = $request->all();
        $this->validate($request, [
			'email' => ['required','string','email'],			
			'phone' => ['max:20'],
			'address' => ['max:150'],
        ]);
		
		$input_data = [
			'email' => $req['email'],
			'phone' => $req['phone'],
			'address' => $req['address'],
			'updated_at' => date('Y-m-d H:i:s')
		];

        User::where('id',auth()->user()->id)->update($input_data);
		
        return response()->json(['status'=>'success', 'message'=>'Details Updated Successfully.']);
    }

	public function profile_image(Request $request){
		
		if (!empty($request->file('image'))) {
            $file = $request->file('image');
			$name=time().'.'.$file->getClientOriginalName();
			$destinationPath = 'uploads/users/';
			$file->move($destinationPath, $name);
			$image = 'uploads/users'.'/'.$name;
            
        }

		$input_data = [
			'image' => isset($image) ? $image : 'images/default.jpg',
			'updated_at' => date('Y-m-d H:i:s')
		];

        User::where('id',auth()->user()->id)->update($input_data);
		
        return response()->json(['status'=>'success', 'message'=>'Profile Image Changed Successfully.']);		
	}

	public function setting_update(Request $request)
	{
		$old_data = MasterSetting::select('*')->where('function_name','forum')->where('status',1)->first();
		$payable = (json_decode($old_data->value)->payable==1) ? 0 : 1 ;
		$newdata = array('payable'=>$payable,'package'=>json_decode($old_data->value)->package,'price'=>json_decode($old_data->value)->price);
		$input_data = [
			'value' => json_encode($newdata)
		];
		
		MasterSetting::where('id',1)->update($input_data);
        return response()->json(['status'=>'success', 'message'=>'Changed Successfully.']);
	}

	public function update_forum_setting(Request $request)
	{
		$id = $request->id;
		$inputData = [
			'payable' => $request->payable,
			'package' => $request->package,
			'price'   => $request->price
		];
		$input_data = [
			'value' => json_encode($inputData)
		];
		MasterSetting::where('id',$id)->update($input_data);
        return response()->json(['status'=>'success', 'message'=>'Changed Successfully.']);

	}

	public function update_chat_paywall_setting(Request $request)
	{
		$id = $request->id;
		$inputData = [
			'price'   => $request->price
		];
		$input_data = [
			'value' => json_encode($inputData)
		];
		if($id==0)
		{  
			MasterSetting::create($input_data);
		}
		else
		{  
			MasterSetting::where('id',$id)->update($input_data);
		}
		
        return response()->json(['status'=>'success', 'message'=>'Changed Successfully.']);

	}

}
