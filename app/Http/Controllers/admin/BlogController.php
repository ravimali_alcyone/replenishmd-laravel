<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Article;
use App\BlogCategory;
use App\User;
use App\CategoryTopic;
use App\PostType;
use DataTables;
use DB;

class BlogController extends Controller {

    public function __construct(){
        $this->middleware('auth');
    }

    public function index(Request $request)
	{
		if ($request->ajax()) {

			$data = Article::select('*');

			if($request->filter_category != ''){
				$data = $data->where('blog_category_id',$request->filter_category);
			}

			if($request->visibility_status != ''){
				$data = $data->where('post_visibility',$request->visibility_status);
			}

			if($request->publish_status != ''){
				$data = $data->where('post_publish',$request->publish_status);
			}

			if($request->comment_status != ''){
				$data = $data->where('post_comment',$request->comment_status);
			}

			if($request->name != ''){
				$data = $data->where('user_id',$request->name);
			}
			
			 $data = $data->latest()->get();

            return Datatables::of($data)
				->addIndexColumn()
				->addColumn('action', function($row){
					$view_url = env('APP_URL').'/admin/blogs/view/'.$row->id;
					$edit_url = env('APP_URL').'/admin/blogs/edit/'.$row->id;
					$btn = '<div class="flex items-center"><a href="'.$view_url.'" data-id="'.$row->id.'" title="View" class="button button--sm w-16 shadow-md mr-1 mb-2 btn_gray viewUser">View</a>';
					$btn = $btn.'<a href="'.$edit_url.'"   data-id="'.$row->id.'" title="Edit" class="button button--sm w-16 shadow-md mr-1 mb-2 btn_blue viewUser">Edit</a>';
					$btn = $btn.' <a href="javascript:void(0)" onclick="deleteRow('.$row->id.')" data-id="'.$row->id.'" title="Delete" class="button button--sm w-16 shadow-md mr-1 mb-2 btn_red deleteUser">Delete</a>';
					if($row->is_verify == 0){
					$btn = $btn.' <a href="javascript:void(0)" onclick="verifyRow('.$row->id.')" data-id="'.$row->id.'" title="Verify" class="button button--sm w-16 shadow-md mr-1 mb-2 btn_info">Verify</a>';
					}
					$btn = $btn.' </div>';
					return $btn;
				})
				
				->addColumn('post_visibility', function($row){
					if($row->post_visibility == 1){
						$post_visibility = '<input type="checkbox" name="status" class="input input--switch border"  title="Status" checked onchange="changeStatus('.$row->id.',1,1)">';
					}else{
						$post_visibility = '<input type="checkbox" name="status" class="input input--switch border"  title="Status" onchange="changeStatus('.$row->id.',0,1)">';
					}
					return $post_visibility;
				})
				->addColumn('post_publish', function($row){
					if($row->post_publish == 1 || ( $row->post_publish == 2 && strtotime($row->scheduled_at) <= time())){
						$post_publish = '<input type="checkbox" name="status" class="input input--switch border"  title="Status" checked onchange="changeStatus('.$row->id.',1,2)">';
					}else{
						$post_publish = '<input type="checkbox" name="status" class="input input--switch border"  title="Status" onchange="changeStatus('.$row->id.',0,2)">';
					}
					return $post_publish;
				})
				->addColumn('post_comment', function($row){
					if($row->post_comment == 1){
						$post_comment = '<input type="checkbox" name="status" class="input input--switch border"  title="Status" checked onchange="changeStatus('.$row->id.',1,3)">';
					}else{
						$post_comment = '<input type="checkbox" name="status" class="input input--switch border"  title="Status" onchange="changeStatus('.$row->id.',0,3)">';
					}
					return $post_comment;
				})
				->addColumn('title', function($row){
					$res = substr($row->title, 0, 25);
					return $res.'...';
				})				
				->addColumn('name', function($row){
					$name = \DB::table('users')->where('id', $row->user_id)->pluck('name')->first();
					$data = '<h4 style="font-size: 14px;" class="mt-1">'.$name.'</h4>';
					return $data;
				})
				->addColumn('blog_category_id', function($row){
					$category = \DB::table('blog_categories')->where('id', $row->blog_category_id)->pluck('name')->first();
					$data = '<h4 style="font-size: 14px;" class="mt-1">'.$category.'</h4>';
					return $data;
				})

				
				->rawColumns(['name','blog_category_id','post_visibility','post_publish','post_comment','title','action'])
				->make(true);
        }

		$data['page'] = 'blogs';
		$data['categories'] = BlogCategory::where('parent_id', 0)->where('status', 1)->get();
		$data['users'] = User::where('status', 1)->get();
        return view('admin.blog.index', $data);
    }

	public function add(){
		$data['blog'] = array();
		$data['page'] = 'blogs';
		$data['categories'] = \DB::table('blog_categories')->where('parent_id', 0)->where('status', 1)->get();
		$data['sub_categories'] = array();
		$data['topics'] = array();
		return view('admin.blog.add', $data);
	}

    public function store(Request $request){
		
		//featured image
		$image = '';
		//header image
		$header_image = '';		
		
		//for existing images
		if ($request->get('page') == 'edit') {
			if (!empty($request->input('old_image'))) {
				$image = $request->input('old_image');
			}
			
			if (!empty($request->input('old_header_image'))) {
				$header_image = $request->input('old_header_image');
			}			
		}

		//for new images
		if (!empty($request->input('crop_cover_image'))) {
			
			$image_array_1 = explode(";", $request->input('crop_cover_image'));
			$image_array_2 = explode(",", $image_array_1[1]);
			$file = base64_decode($image_array_2[1]);
			$name = 'IMG-'.uniqid().'-'.time().'.png';
			$destinationPath = 'uploads/blogs/';
			$image = 'uploads/blogs'.'/'.$name;

			file_put_contents($image, $file);
			
		}else{
			if (!empty($request->file('image'))) {
				$file = $request->file('image');
				$name = 'IMG-'.uniqid().'-'.time().'.'. $file->getClientOriginalExtension();
				$destinationPath = 'uploads/blogs/';
				$file->move($destinationPath, $name);
				$image = 'uploads/blogs'.'/'.$name;
			}			
		}

		if (!empty($request->file('header_image'))) {
            $file = $request->file('header_image');
			$name = 'IMG-'.uniqid().'-'.time().'.'. $file->getClientOriginalExtension();
			$destinationPath = 'uploads/blogs/';
			$file->move($destinationPath, $name);
			$header_image = 'uploads/blogs'.'/'.$name;
        }		
		
		if($request->topics && count($request->topics) > 0){
			$topics = implode(',',$request->topics);
		}else{
			$topics = '';
		}		

		if ($request->get('page') == 'add') {
			$blog = new Article;
			$blog->title = $request->name;
			$blog->user_id = auth()->user()->id;
			$blog->blog_category_id = $request->category != '' ? $request->category : null;
			$blog->category_topic_ids = $topics;
			$blog->blog_content = $request->description;
			$blog->post_tags = $request->tags;
			$blog->image = $image;
			$blog->header_image = $header_image;
			$blog->slug = $this->getSlug($request->slug,null);
			$blog->paramlink = env('APP_URL').'/blogs/post/'.$request->slug;
			$blog->post_visibility = ($request->visibility=="public") ? 1 : 0;
			if($request->publish == 'immediate'){
				$post_publish = 1;
				$scheduled_at = NULL;
			}elseif($request->publish == 'draft'){
				$post_publish = 0;
				$scheduled_at = NULL;
			}else{
				//If Schedule
				$post_publish = 2;
				$scheduled_at = date('Y-m-d H:i:s',strtotime(str_replace('/','-',$request->scheduled_at))); // DD/MM/YYYY to Y-M-D
			}
			$blog->is_verify = 1;                            //admin post will be verified by defalut.
			$blog->post_comment = (isset($_POST['alw_comment'])) ? 1 : 0;
			$blog->lb_content = $request->description;
			
			if($blog->save()) {
				return response()->json(['status'=>'success', 'message'=>'Saved successfully.']);
			}
		} elseif ($request->get('page') == 'edit') {

			$blog = Article::find($request->id);
			$blog->title = $request->name;
			//$blog->user_id = auth()->user()->id;
			$blog->blog_category_id = $request->category != '' ? $request->category : null;
			$blog->category_topic_ids = $topics;
			$blog->blog_content = $request->description;
			$blog->post_tags = $request->tags;
			$blog->image = $image;
			$blog->header_image = $header_image;
			$blog->slug = $this->getSlug($request->slug,$blog->id);
			$blog->paramlink = env('APP_URL').'/blogs/post/'.$request->slug;
			$blog->post_visibility = ($request->visibility=="public") ? 1 : 0;
			if($request->publish == 'immediate'){
				$post_publish = 1;
				$scheduled_at = NULL;
			}elseif($request->publish == 'draft'){
				$post_publish = 0;
				$scheduled_at = NULL;
			}else{
				//If Schedule
				$post_publish = 2;
				$scheduled_at = date('Y-m-d H:i:s',strtotime(str_replace('/','-',$request->scheduled_at))); // DD/MM/YYYY to Y-M-D
			}
			$blog->post_comment = (isset($_POST['alw_comment'])) ? 1 : 0;
			$blog->lb_content = $request->description;
			
			if($blog->save()) {
				return response()->json(['status'=>'success', 'message'=>'Saved successfully.']);
			}
		}
    }

	function getSlug($slug, $id){
		
		if($id != NULL){
			$check = Article::where('id','!=',$id)->where('slug','=',$slug)->get();		
		}else{
			$check = Article::where('slug','=',$slug)->get();
		}		
		
		if($check && $check->count() > 0){
			//slug is already exists
			$slug = $slug.'-'.time().rand(111,999);
		}
		
		return $slug;
	}
	
    public function edit($id){
		$data['topics'] = array();
        $blog = Article::find($id);
		
		if($blog && $blog->count() > 0){
			$data['page'] = 'blogs';
			$data['categories'] = \DB::table('blog_categories')->where('parent_id', 0)->where('status', 1)->get();
			$data['topics'] = \DB::table('category_topic')->where('category_id', $blog->blog_category_id)->where('status', 1)->get();
			$data['blog'] = $blog;
			return view('admin.blog.add', $data);
		} else {
			return redirect()->route('admin.blogs');
		}
    }

    public function destroy($id){
        Article::find($id)->delete();
        return response()->json(['status'=>'success', 'message'=>'Deleted successfully.']);
	}

	public function get_topics(Request $request){
		$req = $request->all();
		$output = '';
		$category_id = $req['category_id'];

		if($category_id != ''){
			$topics = \DB::table('category_topic')->where('category_id',$category_id)->get();

			if($topics && $topics->count() > 0){
				foreach($topics as $topic){
					$output .= '<option value="'.$topic->id.'">'.$topic->topic.'</option>';
				}
			}
		}
		echo $output;
	}

	public function get_sub_sub_category(Request $request){
		$req = $request->all();
		$output = '<option value="">--Select--</option>';
		$category_id = $req['category_id'];

		if($category_id != ''){
			$sub_categories = \DB::table('blog_categories')->where('parent_id', $category_id)->get();

			if($sub_categories && $sub_categories->count() > 0){
				foreach($sub_categories as $sub_category){
					$output .= '<option value="'.$sub_category->id.'">'.$sub_category->name.'</option>';
				}
			}
		}
		echo $output;
    }

	public function change_status(Request $request) {
		$data = Article::where('id', $request->id)->first();
		$column = ($request->flag==1) ? "post_visibility" : (($request->flag==2) ? "post_publish" : "post_comment");
		
		if($data->$column==1)
		{
			$user = Article::where('id', $request->id)->update(array($column => 0));
		}
		else
		{
			$user = Article::where('id', $request->id)->update(array($column => 1));	
		}
		return response()->json(['status'=>'success', 'message'=>'Update successfully.']);

	}

	public function view($id) {
		$blog = Article::where('id', $id)->first();			
		$category_name = BlogCategory::where('id', $blog->blog_category_id)->first();
		$author_name = User::where('id', $blog->user_id)->first();
		$data['blog'] = $blog;
		$data['images'] = json_decode($blog->images);
		$data['category'] = $category_name->name;
		$data['author_name'] = $author_name->name;
		$data['page'] = 'blogs';
		return view('admin.blog.view', $data);
	}

	# Get blogs category master list
	public function blog_category(Request $request)
	{
		if ($request->ajax()) 
		{	
			$data = BlogCategory::where('parent_id', 0);

			if($request->filter_category != ''){
				$data = $data->where('id',$request->filter_category);
			}

			if($request->publish_status != ''){
				$data = $data->where('status',$request->publish_status);
			}

			$data = $data->latest()->get();

            return Datatables::of($data)
				->addIndexColumn()
				->addColumn('action', function($row){
					$edit_url = env('APP_URL').'/admin/blogs/add_category/'.$row->id;
					$btn = '';
					$btn = $btn.'<a href="'.$edit_url.'"   data-id="'.$row->id.'" title="Edit" class="button button--sm w-16 shadow-md mr-1 mb-2 btn_blue viewUser">Edit</a>';
					$btn = $btn.' <a href="javascript:void(0)" onclick="deleteRow('.$row->id.')" data-id="'.$row->id.'" title="Delete" class="button button--sm w-16 shadow-md mr-1 mb-2 btn_red deleteUser">Delete</a>';
					if($row->is_verify == 0){
					$btn = $btn.' <a href="javascript:void(0)" onclick="verifyRow('.$row->id.')" data-id="'.$row->id.'" title="Verify" class="button button--sm w-16 shadow-md mr-1 mb-2 btn_info">Verify</a>';
					}
					$btn = $btn.' </div>';
					return $btn;
				})
				
				->addColumn('status', function($row){
					if($row->status == 1 || ( $row->status == 2 && strtotime($row->scheduled_at) <= time())){
						$status = '<input type="checkbox" name="status" class="input input--switch border"  title="Status" checked onchange="changeStatus('.$row->id.',1,2)">';
					}else{
						$status = '<input type="checkbox" name="status" class="input input--switch border"  title="Status" onchange="changeStatus('.$row->id.',0,2)">';
					}
					return $status;
				})
				
				->addColumn('forum_category', function($row){
					$data = '<h4 style="font-size: 14px;" class="mt-1">'.$row->name.'</h4>';
					return $data;
				})
				->addColumn('image', function($row){
					$data = '<img src="'.env('APP_URL').'/'.$row->category_img.'" style="width:80px !important; height:40px !important;">';
					return $data;
				})
				
				->rawColumns(['forum_category','image','status','action'])
				->make(true);
        }

		$data['parent_page'] = 'blogs';
		$data['page'] = 'blog_category';
		$data['title'] = 'Blog Categories';
		$data['categories'] = BlogCategory::where('status', 1)->get();		
        return view('admin.blog.category', $data);
	}

	#Change category active and inactive status
	public function change_category_status(Request $request)
	{
		$user = BlogCategory::where('id', $request->id)->first();
		if ($user->status) {
			$user = BlogCategory::where('id', $request->id)->update(array('status' => 0));
		} else {
			$user = BlogCategory::where('id', $request->id)->update(array('status' => 1));
		}
	}

	#Add category name
	public function add_category(Request $request)
	{
		$id = $request->id;
		
		if($id==0)
		{
			$data['page'] = 'Add category';
			$data['funcType'] = 'add';
			$data['id'] = $id;
		}
		else
		{
			$data['page'] = 'Edit category';
			$data['funcType'] = 'edit';
			$data['id'] = $id;
			$data['catData'] = BlogCategory::where('id', $request->id)->first();
		}
		return view('admin.blog.add_category',$data);
	}

	#Store data in category
	public function store_category(Request $request)
	{
		$status = 'error';
		$message = 'Error occured';

		if ($request->get('page') == 'edit') {
			$id = $request->get('id');
		}

		if (!empty($request->file('file_input'))) {
            $file = $request->file('file_input');
			if(strtolower($file->getClientOriginalExtension()) == 'jpg' || strtolower($file->getClientOriginalExtension()) == 'png' || strtolower($file->getClientOriginalExtension()) == 'jpeg'){
				$name= time().'-'.rand(11111111,99999999).'.'.strtolower($file->getClientOriginalExtension());
				$destinationPath = 'uploads/category/';
				
				if($file->move($destinationPath, $name)){
					$image = 'uploads/category'.'/'.$name;
				}				
			}
			else
			{
				$status = 'error';
				$message = 'Only jpg, jpeg and png files are allowable';					
			}
        }

		if($request->get('page') == 'edit')
		{
			$filename = (!empty($request->file('file_input'))) ? $image : $request->old_file;
		}
		else
		{
			$filename = $image;
		}

		$input_data = [
			'name' => $request->category_name,
			'parent_id' => 0,
			'status' => 1,
			'category_img' => $filename
		];
		
		if ($request->get('page') == 'add') {
			$blog = BlogCategory::create($input_data);
			if ($blog->save()) {
				return response()->json(['status'=>'success', 'message'=>'Saved successfully.']);
			}
		} elseif ($request->get('page') == 'edit') {
			$blog = BlogCategory::where('id', $request->id)->update($input_data);
			return response()->json(['status'=>'success', 'message'=>'Saved successfully.']);
		}
	}

	#Remove Category
	
	public function destroy_category($id){
		BlogCategory::find($id)->delete();
        return response()->json(['status'=>'success', 'message'=>'Deleted successfully.']);
	}

	#Remove Topic
	public function destroy_topic($id){
		CategoryTopic::find($id)->delete();
        return response()->json(['status'=>'success', 'message'=>'Deleted successfully.']);
	}

	public function verify_category($id){
        $blog = BlogCategory::where('id', $id)->update(array('is_verify'=>1));
        return response()->json(['status'=>'success', 'message'=>'Verify successfully.']);
	}

	public function category_topic(Request $request){
		if ($request->ajax()) 
		{	
			$data = CategoryTopic::select('category_topic.*','blog_categories.name AS cat_name')
					->leftJoin('blog_categories','blog_categories.id','=','category_topic.category_id')
					->where('category_topic.status',1);

			if($request->filter_category != ''){
				$data = $data->where('category_id',$request->filter_category);
			}

			if($request->publish_status != ''){
				$data = $data->where('category_topic.status',$request->publish_status);
			}

			$data = $data->latest()->get();

            return Datatables::of($data)
				->addIndexColumn()
				->addColumn('action', function($row){
					$edit_url = env('APP_URL').'/admin/blogs/add_category_topic/'.$row->id;
					$btn = '';
					$btn = $btn.'<a href="'.$edit_url.'"   data-id="'.$row->id.'" title="Edit" class="button button--sm w-16 shadow-md mr-1 mb-2 btn_blue viewUser">Edit</a>';
					$btn = $btn.' <a href="javascript:void(0)" onclick="deleteRow('.$row->id.')" data-id="'.$row->id.'" title="Delete" class="button button--sm w-16 shadow-md mr-1 mb-2 btn_red deleteUser">Delete</a>';
					if($row->is_verify == 0){
					$btn = $btn.' <a href="javascript:void(0)" onclick="verify_category_topic('.$row->id.')" data-id="'.$row->id.'" title="Verify" class="button button--sm w-16 shadow-md mr-1 mb-2 btn_info">Verify</a>';
					}
					$btn = $btn.' </div>';
					return $btn;
				})
				
				->addColumn('status', function($row){
					if($row->status == 1 || ( $row->status == 2 && strtotime($row->scheduled_at) <= time())){
						$status = '<input type="checkbox" name="status" class="input input--switch border"  title="Status" checked onchange="changeStatus('.$row->id.')">';
					}else{
						$status = '<input type="checkbox" name="status" class="input input--switch border"  title="Status" onchange="changeStatus('.$row->id.')">';
					}
					return $status;
				})
				
				->addColumn('category', function($row){
					$data = '<h4 style="font-size: 14px;" class="mt-1">'.$row->cat_name.'</h4>';
					return $data;
				})
				->addColumn('topic', function($row){
					$data = '<h4 style="font-size: 14px;" class="mt-1">'.$row->topic.'</h4>';
					return $data;
				})

				->rawColumns(['category','topic','status','action'])
				->make(true);
        }

		$data['parent_page'] = 'blogs';
		$data['page'] = 'category_topic';
		$data['title'] = 'Topic List';
		$data['categories'] = BlogCategory::where('status', 1)->get();		
        return view('admin.blog.category_topic', $data);

	}

	public function verify_category_topic($id)
	{
        $blog = CategoryTopic::where('id', $id)->update(array('is_verify'=>1));
        return response()->json(['status'=>'success', 'message'=>'Verify successfully.']);
	}


	public function blog_post_type(Request $request){
		//$data['page'] = 'blog_post_type';
		//$data['categories'] = PostType::select()->get();
		//return view('admin.blog.blog_post_type',$data);
		
		

		if ($request->ajax()) 
		{	
			$data = PostType::select('*');

			$data = $data->latest()->get();

            return Datatables::of($data)
				->addIndexColumn()
				->addColumn('action', function($row){
					$edit_url = env('APP_URL').'/admin/blogs/add_post_type/'.$row->id;
					$btn = '';
					$btn = $btn.'<a href="'.$edit_url.'"   data-id="'.$row->id.'" title="Edit" class="button button--sm w-16 shadow-md mr-1 mb-2 btn_blue viewUser">Edit</a>';
					$btn = $btn.' <a href="javascript:void(0)" onclick="deleteRow('.$row->id.')" data-id="'.$row->id.'" title="Delete" class="button button--sm w-16 shadow-md mr-1 mb-2 btn_red deleteUser">Delete</a>';
					
					$btn = $btn.' </div>';
					return $btn;
				})
				
				->addColumn('status', function($row){
					if($row->status == 1){
						$status = '<input type="checkbox" name="status" class="input input--switch border"  title="Status" checked onchange="changeStatus('.$row->id.')">';
					}else{
						$status = '<input type="checkbox" name="status" class="input input--switch border"  title="Status" onchange="changeStatus('.$row->id.')">';
					}
					return $status;
				})
				
				->addColumn('type', function($row){
					$data = '<h4 style="font-size: 14px;" class="mt-1">'.$row->type_name.'</h4>';
					return $data;
				})
				

				->rawColumns(['type','status','action'])
				->make(true);
        }

		$data['parent_page'] = 'blogs';
		$data['page'] = 'blog_post_type';
		$data['title'] = 'Category List';
        return view('admin.blog.blog_post_type', $data);
	}

	public function add_category_topic(Request $request){
		$id = $request->id;
		if($id==0)
		{
			$data['page'] = 'add_category_topic';
			$data['funcType'] = 'add';
			$data['id'] = $id;
		}
		else
		{
			$data['page'] = 'add_category_topic';
			$data['funcType'] = 'edit';
			$data['id'] = $id;
			$data['catData'] = CategoryTopic::where('id', $request->id)->first();
		}
		$data['category'] = BlogCategory::select()->get();
		return view('admin.blog.add_category_topic',$data);
	}

	public function store_category_topic(Request $request)
	{
		if ($request->ajax()) {
			
			$messages = [
				'category_id.regex' => 'Category name is required filed.',
				'topic.regex' => 'Category topic is required filed.',
			];
			
			$validator = Validator::make($request->all(), [
				'category_id' => 'required',
				'topic' => 'required',
			], $messages)->validate();
			
			$input_data = [
				'category_id' => $request->category_id,
				'topic' =>  $request->topic
			];
			
			if ($request->get('page') == 'add') {
				$blog = CategoryTopic::create($input_data);
				if ($blog->save()) {
					return response()->json(['status'=>'success', 'message'=>'Saved successfully.']);
				}
			} 
			elseif ($request->get('page') == 'edit') {
				$blog = CategoryTopic::where('id', $request->id)->update($input_data);
				return response()->json(['status'=>'success', 'message'=>'Saved successfully.']);
			}
			
		}
		else{
			return response()->json(['status'=> 'error', 'message' => 'This request is not valid.', 'data' => '']);
		}
	}

	public function add_post_type(Request $request){
		$id = $request->id;
		
		if($id==0)
		{
			$data['page'] = 'add_post_type';
			$data['funcType'] = 'add';
			$data['id'] = $id;
		}
		else
		{
			$data['page'] = 'add_post_type';
			$data['funcType'] = 'edit';
			$data['id'] = $id;
			$data['catData'] = PostType::where('id', $request->id)->first();
		}
		return view('admin.blog.add_post_type',$data);
	}

	public function store_post_type(Request $request)
	{
		if ($request->ajax()) {
			
			$messages = [
				'post_type.regex' => 'Post Type is required filed.'
			];
			
			$validator = Validator::make($request->all(), [
				'post_type' => 'required'
			], $messages)->validate();
			
			$input_data = [
				'type_name' => $request->post_type
			];
			
			if ($request->get('page') == 'add') {
				$blog = PostType::create($input_data);
				if ($blog->save()) {
					return response()->json(['status'=>'success', 'message'=>'Saved successfully.']);
				}
			} 
			elseif ($request->get('page') == 'edit') {
				$blog = PostType::where('id', $request->id)->update($input_data);
				return response()->json(['status'=>'success', 'message'=>'Saved successfully.']);
			}
			
		}
		else{
			return response()->json(['status'=> 'error', 'message' => 'This request is not valid.', 'data' => '']);
		}
	}

	public function verify_blog_post($id)
	{
        $blog = Article::where('id', $id)->update(array('is_verify'=>1));
        return response()->json(['status'=>'success', 'message'=>'Verify successfully.']);
	}

}
