<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\ProviderCategory;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\Library\Services\CommonService;
use DataTables;
use DB;

class ProviderController extends Controller {

    public function __construct(){
        $this->middleware('auth');
    }

    public function index(Request $request, CommonService $common, $cat_id=null){


		if ($request->ajax()) {

			$where = array('is_admin' => 0,	'user_role' => 3);

			if($request->filter_status != ''){
				$where['status'] = $request->filter_status;
			}

			if($request->filter_category != ''){
				$whereRaw['column'] = 'category';
				$whereRaw['value'] = $request->filter_category;
			}else{
				$whereRaw = array();
			}

			$data = $common->getTableListing($request, $table='users', $where, $whereRaw, $isLatest=false);

            return Datatables::of($data)
				->addIndexColumn()
				->addColumn('action', function($row){
					$view_url = env('APP_URL').'/admin/providers/view/'.$row->id;
					$edit_url = "#";
					$btn = '<div class="flex items-center"><a href="'.$view_url.'" data-id="'.$row->id.'" title="View" class="button button--sm w-16 shadow-md mr-1 mb-2 btn_gray viewUser">View</a>';
					$btn = $btn.' <a href="javascript:void(0)" onclick="deleteRow('.$row->id.')" data-id="'.$row->id.'" title="Delete" class="button button--sm w-16 shadow-md mr-1 mb-2 btn_red deleteUser">Delete</a></div>';
					return $btn;
				})
				->addColumn('category', function($row){

					$categories = explode(',', $row->category);
					$category_names = DB::table('provider_categories')->whereIn('id', $categories)->select('name')->get();
					$categories_arr = array();
					foreach ($category_names as $key => $value) {
						array_push($categories_arr, $value->name);
					}
					$categoryString = implode(', ', $categories_arr);
					return $categoryString;
				})
				->addColumn('status', function($row){
					if($row->status == 1){
						$status = '<input type="checkbox" name="status" class="input input--switch border"  title="Status" checked onchange="changeStatus('.$row->id.')">';
					}else{
						$status = '<input type="checkbox" name="status" class="input input--switch border"  title="Status" onchange="changeStatus('.$row->id.')">';
					}
					return $status;
				})->addColumn('verified', function($row){
					if($row->provider_verified == 1){
						$verified = '<input type="checkbox" name="verified" class="input input--switch border provider_verified"  data-id="'.$row->id.'" title="verified" checked >';
					}else{
						$verified = '<input type="checkbox" name="verified" class="input input--switch border provider_verified"  data-id="'.$row->id.'" title="Not verified" >';
					}
					return $verified;
				})
				->addColumn('image', function($row){

					if($row->image == ''){
						$url = '/dist/images/user_icon.png';
					}else{
						$url = '/'.$row->image;
					}

					$image = '<a href="'.$url.'" data-lightbox="lightbox-set-'.$row->id.'"> <div class="w-10 h-10 image-fit zoom-in">
								<img class="tooltip rounded-full tooltipstered" src="'.$url.'">
							</div></a>';

					return $image;
				})
				->rawColumns(['name','image','category','status','verified','action'])
				->make(true);
        }
		$data['page'] = 'providers';
		$data['cat_id'] = $cat_id;
		$data['categories'] = ProviderCategory::where('parent_id', 0)->where('status', 1)->get();
		//echo "<pre>"; print_r($data); die();
        return view('admin.providers.index', $data);
    }

	public function add(){
		$data['user'] = array();
		return view('admin.providers.add', $data);
	}

    public function store(Request $request){
		$req = $request->all();
        $this->validate($request, [
			'name' => 'required|string|max:255',
            'email' => 'required|email|max:255|unique:users,email,'.$input['id'],
        ]);

		$password = Hash::make('123456');
		if(isset($input['status']) && $req['status'] == 'on'){
			$status = 1;
		}else{
			$status = 0;
		}

		$where = ['id' => $req['id']];

		$input_data =
		[
			'name' => $req['name'],
			'email' => $req['email'],
			'user_role' => 3,
			'status'=> $status,
			'password' => $password,
			'is_admin' => 0
		];

		if($input['id'] != ''){
			unset($input_data['password']);
		}

        User::updateOrCreate($where,$input_data);

        return response()->json(['status'=>'success', 'message'=>'Saved successfully.']);
    }

    public function edit($id){
        $user = User::find($id);
		if($user && $user->count() > 0){
			$data['user'] = $user;
			return view('admin.doctor.add',$data);
		}else{
			return redirect()->route('admin.doctors');
		}
    }

    public function destroy($id){
        User::find($id)->delete();
        return response()->json(['status'=>'success', 'message'=>'Deleted successfully.']);
    }

	public function change_status(Request $request) {
		if ($request->ajax()) {
			$user = User::where('id', $request->id)->first();
			if ($user->status) {
				$user_update = User::where('id', $request->id)->update(array('status' => 0));
				return response()->json(['status'=> 'success', 'redirect'=> '', 'message' => 'Updated successfully.']);
			} else {
				$user_update = User::where('id', $request->id)->update(array('status' => 1));
			
				return response()->json(['status'=> 'success', 'redirect'=> '', 'message' => 'Updated successfully.']);
			}
		}
	}
	
	public function verified(Request $request, CommonService $common) {
		
		if ($request->ajax()) {
			$user = User::where('id', $request->id)->first();
			if ($user->provider_verified) {
				$user_update = User::where('id', $request->id)->update(array('provider_verified' => 0));
				$msg = 'Location removed successfully.';
				return response()->json(['status'=> 'success', 'redirect'=> '', 'message' => 'Updated successfully.']);
			} else {
				$user_update = User::where('id', $request->id)->update(array('provider_verified' => 1,'status'=>1));
				
                $input['name'] = auth()->user()->name;
				$input['email'] = $user->email;
				$input['subject'] = 'Account verified by Admin';
				$input['template'] = 'front.email_template.provider_verified';					
				$input['provider_name'] = $user->name;
				$input['admin_name'] = auth()->user()->name;
				
			    $result = $common->sendMail($input);
        		$msg = 'Location removed successfully.';
				return response()->json(['status'=> 'success', 'redirect'=> '', 'message' => 'Updated successfully.']);
			}
		}
	}

	public function view($id) {
		$provider = User::where('id', $id)->where('user_role', 3)->first();
		$categories = explode(',', $provider->category);
		$data['categories'] = DB::table('provider_categories')->whereIn('id', $categories)->select('name', 'id')->get();
		// $categories_arr = array();
		// foreach ($category_names as $key => $value) {
		// 	array_push($categories_arr, $value->name);
		// }
		// $categoryString = implode(', ', $categories_arr);

		$data['provider'] = $provider;
		// $data['categories'] = $categoryString;
		$data['page'] = 'providers';
		return view('admin.providers.view', $data);
	}
}
