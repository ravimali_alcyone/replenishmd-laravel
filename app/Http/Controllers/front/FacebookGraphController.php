<?php

namespace App\Http\Controllers\front;

use App\Http\Controllers\Controller;
use Facebook\Exceptions\FacebookSDKException;
use Facebook\Facebook;
use Illuminate\Http\Request;
//use Illuminate\Support\Facades\Auth;
use App\FacebookAuth;
use Auth;
 
class FacebookGraphController extends Controller
{    
    private $api;
    private $page_id;
    private $fb_id;
    private $token;
    
    public function __construct(Facebook $fb)
    {
        
        
        $this->middleware(function ($request, $next) use ($fb) {
            $check_auth = FacebookAuth::where('user_id', auth()->user()->id)->first();
            $this->page_id = $check_auth->page_id;
            $this->fb_id = $check_auth->fb_id;
            $this->token = $check_auth->token;
            
            
            $fb->setDefaultAccessToken($check_auth->token);
            $this->api = $fb;
            return $next($request);
        });
    }
    

    public function retrieveUserProfile(){
        try {

            $params = "first_name,last_name,age_range,gender";

            $user = $this->api->get('/me?fields='.$params)->getGraphUser();

            dd($user);

        } catch (FacebookSDKException $e) {

        }

    }
    
    public function getPageAccessToken($page_id){
        
        try {
             // Get the \Facebook\GraphNodes\GraphUser object for the current user.
             // If you provided a 'default_access_token', the '{access-token}' is optional.
             $response = $this->api->get('/me/accounts', $this->token);
        } catch(FacebookResponseException $e) {
            // When Graph returns an error
            echo 'Graph returned an error: ' . $e->getMessage();
            exit;
        } catch(FacebookSDKException $e) {
            // When validation fails or other local issues
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            exit;
        }
     
        try {
            $pages = $response->getGraphEdge()->asArray();
            foreach ($pages as $key) {
                if ($key['id'] == $page_id) {
                    return $key['access_token'];
                }
            }
        } catch (FacebookSDKException $e) {
            dd($e); // handle exception
        }
    }
    
    public function publishToPage(Request $request){
        
        $page_id = $this->page_id;
        try {
        	if ($request->ajax()) {
                $post = $this->api->post( '/'.$page_id.'/feed', array('message' => $request->message), $this->getPageAccessToken($page_id));
                
                $post = $post->getGraphNode()->asArray();
                if($post){
                    return response()->json(['status'=> 'success', 'message'=>'Post Shared successfully']);
                }else{
                    return response()->json(['status'=> 'error', 'redirect'=> '', 'message' => 'Something went wrong, please try again.']);
                }
        	}else{
        	   return response()->json(['status'=> 'error', 'redirect'=> '', 'message' => 'Something went wrong, please try again.']);
        	}
     
        } catch (FacebookSDKException $e) {
            dd($e); // handle exception
        }
    }
    
     public function publishToPageimg(Request $request){
 
        $page_id = $this->page_id;
        try {
            if ($request->ajax()) {
                $post = $this->api->post( '/'.$page_id.'/photos', array(
                    'caption' => $request->message,
                    'url' => URL($request->post_images)
                ), $this->getPageAccessToken($page_id));
            
                $post = $post->getGraphNode()->asArray();
                if($post){
                        return response()->json(['status'=> 'success', 'message'=>'Post Shared successfully']);
                }else{
                    return response()->json(['status'=> 'error', 'redirect'=> '', 'message' => 'Something went wrong, please try again.']);
                }
            }else{
        	   return response()->json(['status'=> 'error', 'redirect'=> '', 'message' => 'Something went wrong, please try again.']);
        	}
        } catch (FacebookSDKException $e) {
            dd($e); // handle exception
        }
    }
    
    public function publishImages(Request $request){
 
        $page_id = $this->page_id;
     
        try {
            
            if ($request->ajax()) {
                $photoPath= explode(",",$request->post_images);
                $photoIdArray = array();
        		foreach($photoPath as $photoURL) {
        			$params = array(
        				"url" =>URL($photoURL),
        				"published" =>false
        			);
        			try {
        				$postResponse = $this->api->post("/".$page_id."/photos", $params, $this->getPageAccessToken($page_id));
        				$photoId = $postResponse->getDecodedBody();
        				if(!empty($photoId["id"])) {
        					$photoIdArray[] = $photoId["id"];
        				}
        			} catch (FacebookResponseException $e) {
        				// display error message
        				//print $e->getMessage();
        				exit();
        			} catch (FacebookSDKException $e) {
        				//print $e->getMessage();
        				exit();
        			}
        		}
        		
        		$params_up = array( "message" => $request->message);
    			foreach($photoIdArray as $k => $photoId) {
    				$params_up["attached_media"][$k] = '{"media_fbid":"' . $photoId . '"}';
    			}
    			try {
				    $postResponse = $this->api->post("/".$page_id."/feed",     $params_up, $this->getPageAccessToken($page_id));
    			
    			    if($postResponse){
                        return response()->json(['status'=> 'success', 'message'=>'Post Shared successfully']);
                    }else{
                        return response()->json(['status'=> 'error', 'redirect'=> '', 'message' => 'Something went wrong, please try again.']);
                    }
    			} catch (FacebookResponseException $e) {
    				// display error message
    				print $e->getMessage();
    				exit();
    			} catch (FacebookSDKException $e) {
    				print $e->getMessage();
    				exit();
    			}
            }else{
        	   return response()->json(['status'=> 'error', 'redirect'=> '', 'message' => 'Something went wrong, please try again.']);
        	}
        } catch (FacebookSDKException $e) {
            dd($e); // handle exception
        }
    }
    
    
    
}