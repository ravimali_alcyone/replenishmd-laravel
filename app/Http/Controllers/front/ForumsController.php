<?php

namespace App\Http\Controllers\front;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\ForumTopic;
use App\ForumCategory;
use App\ForumDetail;
use App\MasterSetting;
use App\ForumPaymentDetail;
use App\User;
use App\ForumTopicLike;
use DataTables;
use DB;

class ForumsController extends Controller {

    public function __construct(){
        $this->middleware('auth');
    }

/** *** Forum Topics Module  *** **/

	public function index(Request $request)
	{
		if ($request->ajax()) 
		{

			$data = ForumDetail::select('*')->where('created_by', auth()->user()->id)->where('is_deleted', 0);		

			if($request->publish_status != ''){
				$data = $data->where('status',$request->publish_status);
			}
			if($request->pay_status != ''){
				$data = $data->where('is_paid',$request->pay_status);
			}
			if($request->access_payment_type != ''){
				$data = $data->where('access_payment_type',$request->access_payment_type);
			}
			if($request->post_status != ''){
				$data = $data->where('is_posting_paid',$request->post_status);
			}
			if($request->posting_payment_type != ''){
				$data = $data->where('posting_payment_type',$request->posting_payment_type);
			}
			if($request->ver_status != ''){
				$data = $data->where('is_approved',$request->ver_status);
			}

			$data = $data->latest()->get();

            return Datatables::of($data)
				->addIndexColumn()
				->addColumn('action', function($row){
					$edit_url = env('APP_URL').'/user/forums/addForum/'.$row->id;
					//$btn = '<div class="flex items-center"><a href="'.$view_url.'" data-id="'.$row->id.'" title="View" class="btn btn-sm btn-primary d-inline">View</a>';
					$btn = '';
					$btn = $btn.'<a href="'.$edit_url.'"   data-id="'.$row->id.'" title="Edit" class="btn btn-sm btn-info d-inline ml-2">Edit</a>';
					$btn = $btn.' <a href="javascript:void(0)" onclick="deleteRow('.$row->id.')" data-id="'.$row->id.'" title="Delete" class="btn btn-sm btn-danger d-inline ml-2">Delete</a>';
					if($row->is_approved == 0){
						$btn = $btn.' <a href="javascript:void(0)" data-id="'.$row->id.'" title="Forum verification status" class="btn btn-sm btn-warning d-inline ml-2" style="color:#fff;">Pendding...</a>';
					}
					else
					{
						$btn = $btn.' <a href="javascript:void(0)" data-id="'.$row->id.'" title="Forum verification status" class="btn btn-sm btn-success d-inline ml-2">Verified</a>';
					}
					$btn = $btn.' </div>';
					return $btn;
				})
				
				->addColumn('status', function($row){
					if($row->status == 1 || ( $row->status == 2 && strtotime($row->scheduled_at) <= time())){
						$status = '<input type="checkbox" name="status" class="input input--switch border"  title="Status" checked onchange="changeStatus('.$row->id.',1,2)">';
					}else{
						$status = '<input type="checkbox" name="status" class="input input--switch border"  title="Status" onchange="changeStatus('.$row->id.',0,2)">';
					}
					return $status;
				})
				->addColumn('is_paid', function($row){
					if($row->is_paid == 1){
						$is_paid = '<input type="checkbox" name="is_paid" class="input input--switch border"  title="Allow Comment" checked onchange="changeCommentStatus('.$row->id.',1,3)">';
					}else{
						$is_paid = '<input type="checkbox" name="is_paid" class="input input--switch border"  title="Allow Comment" onchange="changeCommentStatus('.$row->id.',0,3)">';
					}
					return $is_paid;
				})
				->addColumn('is_posting_paid', function($row){
					if($row->is_posting_paid == 1){
						$is_posting_paid = '<input type="checkbox" name="is_posting_paid" class="input input--switch border"  title="Allow Comment" checked onchange="changePostingStatus('.$row->id.',1,3)">';
					}else{
						$is_posting_paid = '<input type="checkbox" name="is_posting_paid" class="input input--switch border"  title="Allow Comment" onchange="changePostingStatus('.$row->id.',0,3)">';
					}
					return $is_posting_paid;
				})

				
				->addColumn('forum_subject', function($row){
					$res = substr($row->forum_subject, 0, 50);
					return $res.'...';
				})

				->rawColumns(['forum_subject','status','is_paid','is_posting_paid','action'])
				->make(true);
        }

		$data['parent_page'] = 'forums';
		$data['page'] = 'forum';
		$data['title'] = 'Forum Detail';
		$data['setting'] = MasterSetting::where('id', 1)->get();
		$data['forum_payment_check'] = ForumPaymentDetail::where('user_id',auth()->user()->id)->where('status',1)->orderby('id','DESC')->first();
		$data['forum_payment_flag'] = (!empty($data['forum_payment_check'])) ? $data['forum_payment_check']->count() : 0;
		$masterVal = json_decode($data['setting'][0]->value);
		
		
		/********For render common paywall view page********/
		if(!empty(auth()->user()->id)){
			$user_id=auth()->user()->id;
			$card_info = DB::table('payment_card_info')->select('*')->where(array('user_id' => $user_id))->orderBy('card_id','DESC')->get();
			
			if(!empty($card_info)){
				$intent['card_info']= $card_info;
				
			}
			$card_primary = DB::table('payment_card_info')->select('*')->where(array('user_id' => $user_id,'is_primary'=>1))->orderBy('card_id','DESC')->get();
			if(!empty($card_primary) && !empty($card_primary[0]->card_id)){
				$intent['card_primary']= $card_primary[0]->card_id;
			}else{
				$intent['card_primary']="new";
			}
			$intent['forum_amount'] = $masterVal->price;
			$intent['payment_for'] = 'forum';
		}
		$data['paywall'] = view('front.common_paywall_page', compact('intent'))->render();
		/********End render common paywall view page********/

        return view('front.forum.index', $data);
    }

	

	#Forum Post Details
	public function post_details(Request $request)
	{
		
		if ($request->ajax()) 
		{
			$data = ForumTopic::select('forum_topics.*','users.name','forum_details.forum_subject')
				->join('forum_details', function ($join) {
					$join->on('forum_details.id', '=', 'forum_topics.forum_category_id');
				})
				->join('users', function ($join) {
					$join->on('users.id', '=', 'forum_topics.user_id');
				})
				->where('user_id', auth()->user()->id);


			if($request->filter_category != ''){
				$data = $data->where('forum_details.id',$request->filter_category);
			}

			if($request->publish_status != ''){
				$data = $data->where('status',$request->publish_status);
			}

			if($request->comment_status != ''){
				$data = $data->where('comment_on',$request->comment_status);
			}
			
			$data = $data->latest()->get();
            return Datatables::of($data)
			->addIndexColumn()
			->addColumn('action', function($row){
				$edit_url = env('APP_URL').'/user/forums/add/'.$row->id;
				//$btn = '<div class="flex items-center"><a href="'.$view_url.'" data-id="'.$row->id.'" title="View" class="btn btn-sm btn-primary d-inline">View</a>';
				$btn = '';
				$btn = $btn.'<a href="'.$edit_url.'"   data-id="'.$row->id.'" title="Edit" class="btn btn-sm btn-info d-inline ml-2">Edit</a>';
				$btn = $btn.' <a href="javascript:void(0)" onclick="deleteRow('.$row->id.')" data-id="'.$row->id.'" title="Delete" class="btn btn-sm btn-danger d-inline ml-2">Delete</a>';
				$btn = $btn.' </div>';
				return $btn;
			})
			
			->addColumn('status', function($row){
				if($row->status == 1 || ( $row->status == 2 && strtotime($row->scheduled_at) <= time())){
					$status = '<input type="checkbox" name="status" class="input input--switch border"  title="Status" checked onchange="changeStatus('.$row->id.',1,2)">';
				}else{
					$status = '<input type="checkbox" name="status" class="input input--switch border"  title="Status" onchange="changeStatus('.$row->id.',0,2)">';
				}
				return $status;
			})
			->addColumn('comment_on', function($row){
				if($row->comment_on == 1){
					$comment_on = '<input type="checkbox" name="comment_on" class="input input--switch border"  title="Allow Comment" checked onchange="changeCommentStatus('.$row->id.',1,3)">';
				}else{
					$comment_on = '<input type="checkbox" name="comment_on" class="input input--switch border"  title="Allow Comment" onchange="changeCommentStatus('.$row->id.',0,3)">';
				}
				return $comment_on;
			})
			
			->addColumn('title', function($row){
				$res = substr($row->title, 0, 50);
				return $res.'...';
			})
			->addColumn('forum_subject', function($row){
				$res = substr($row->forum_subject, 0, 50);
				return $res.'...';
			})
			->addColumn('author', function($row){
				$data = '<h4 style="font-size: 14px;" class="mt-1">'.$row->name.'</h4>';
				return $data;
			})				
			
			

			->rawColumns(['forum_subject','title','author','status','comment_on','action'])
			->make(true);
        }

		$data['parent_page'] = 'forums';
		$data['page'] = 'forum_topics';
		$data['title'] = 'Forum Topics';
		$data['categories'] = ForumDetail::where('created_by', auth()->user()->id)->get();
		$data['users'] = User::where('status', 1)->get();
        return view('front.forum.topic_list', $data);
	}

	#Add topic 
	public function addTopic(Request $request)
	{
		$id = $request->id;
		if($id==0)
		{
			$data['forum_topic'] = array();
			$data['parent_page'] = 'forums';
			$data['page'] = 'add';
			$data['title'] = 'Add Forum Topic';
			$data['funcType'] = 'add';
			$data['id'] = $id;
		}
		else
		{
			$data['forum'] = array();
			$data['parent_page'] = 'forums';
			$data['page'] = 'edit';
			$data['title'] = 'Edit Forum Category';
			$data['funcType'] = 'edit';
			$data['id'] = $id;
			$data['forum_topic'] = ForumTopic::where('id', $request->id)->first();
		}
		$data['categories'] = ForumDetail::where('created_by', auth()->user()->id)->get();
		return view('front.forum.add', $data);

	}

	#Add forum page
	public function addForum(Request $request)
	{
		$id = $request->id;
		if($id==0)
		{
			$data['forum_topic'] = array();
			$data['parent_page'] = 'forums';
			$data['page'] = 'add';
			$data['title'] = 'Add Forum';
			$data['funcType'] = 'add';
			$data['id'] = $id;
		}
		else
		{
			$data['forum'] = array();
			$data['parent_page'] = 'forums';
			$data['page'] = 'edit';
			$data['title'] = 'Edit Forum';
			$data['funcType'] = 'edit';
			$data['id'] = $id;
			$data['forum_topic'] = ForumDetail::where('id', $request->id)->first();
		}
		///$data['categories'] = ForumDetail::where('status', 1)->get();
		return view('front.forum.addForum', $data);

	}
	
	#Edit topic 
	public function editTopic($id)
	{			
		$data['forum'] = array();
		$data['parent_page'] = 'forums';
		$data['page'] = 'edit_topic';
		$data['title'] = 'Edit Forum Topic';
		$data['funcType'] = 'edit';
		$data['id'] = $id;
		$data['forum_topic'] = ForumTopic::where('id', $id)->first();		
		$data['categories'] = ForumCategory::where('status', 1)->get();
		
		return view('admin.forums.topics.add', $data);
	}	
	
    public function storeTopic(Request $request)
	{
		$status = 'error';
		$message = 'Error occured';

		if ($request->get('page') == 'edit') {
			$id = $request->get('id');
		}
		if ($request->get('page') == 'add') {

			
			$input_data = [
				'user_id' => auth()->user()->id,
				'title' => $request->title,
				'forum_category_id' => $request->category,
				'description' => $request->description,
				'slug' => $request->slug,
				'status' => $request->status,
				'comment_on' => $request->comment_on ? 1 : 0,
				'is_verified' => 0
			];
			
			$forum_topic = ForumTopic::create($input_data);
			if ($forum_topic->save()) {
				return response()->json(['status'=>'success', 'message'=>'Saved successfully.']);
			}
		} 
		elseif ($request->get('page') == 'edit') {

			$input_data = [
				'title' => $request->title,
				'forum_category_id' => $request->category,
				'description' => $request->description,
				'slug' => $request->slug,
				'status' => $request->status,				
				'comment_on' => $request->comment_on ? 1 : 0,
				'is_verified' => 1
			];

			$forum_topic = ForumTopic::where('id', $request->id)->update($input_data);
			return response()->json(['status'=>'success', 'message'=>'Saved successfully.']);
		}
	}

	public function storeForum(Request $request)
	{
		$messages = [
			'title.regex' => 'Subject field required',
			'slug.regex' => 'Slug field required',
		];
		$validator = Validator::make($request->all(), [
			'title' => 'required|max:100',
			'slug' => 'required'
		], $messages)->validate();
		
		if ($request->get('page') == 'edit') {
			$id = $request->get('id');
		}

		$input_data = [
			'forum_subject' => $request->title,
			'short_info' => $request->short_info,
			'slug' => $request->slug ,
			'created_by' => auth()->user()->id,
			'status' => $request->status,
			'is_paid' => $request->is_paid,
			'access_amount' => $request->access_amount,
			'access_payment_type' => $request->access_payment_type,
			'is_posting_paid' => $request->is_posting,
			'posting_amount' => $request->posting_amount,
			'posting_payment_type' => $request->posting_payment_type,
			'created_at' => date('Y-m-d H:i:s'),
			'updated_at' => date('Y-m-d H:i:s')
		];

		if ($request->get('page') == 'add') {

			$forum_topic = ForumDetail::create($input_data);
			if ($forum_topic->save()) {
				return response()->json(['status'=>'success', 'message'=>'Forum created successfully']);
			}
		} 
		elseif ($request->get('page') == 'edit') {
			$forum_topic = ForumDetail::where('id', $request->id)->update($input_data);
			return response()->json(['status'=>'success', 'message'=>'Saved successfully.']);
		}
	}

	public function topicStatus(Request $request) 
	{
		$row = ForumTopic::where('id', $request->id)->first();
		
		if($row){
			if ($row->status == 1) {
				$row = ForumTopic::where('id', $request->id)->update(array('status' => 0));
			} else {
				$row = ForumTopic::where('id', $request->id)->update(array('status' => 1));
			}
			return response()->json(['status'=>'success', 'message'=>'Updated successfully.']);
		}else{
			return response()->json(['status'=>'error', 'message'=>'Error Occured.']);
		}

	}

	public function comment_on(Request $request) 
	{
		$row = ForumTopic::where('id', $request->id)->first();
		
		if($row){
			if ($row->comment_on == 1) {
				$row = ForumTopic::where('id', $request->id)->update(array('comment_on' => 0));
			} else {
				$row = ForumTopic::where('id', $request->id)->update(array('comment_on' => 1));
			}
			return response()->json(['status'=>'success', 'message'=>'Updated successfully.']);
		}else{
			return response()->json(['status'=>'error', 'message'=>'Error Occured.']);
		}

	}

	public function topic_paid_status(Request $request) 
	{
		$row = ForumTopic::where('id', $request->id)->first();
		
		if($row){
			if ($row->is_paid == 1) {
				$row = ForumTopic::where('id', $request->id)->update(array('is_paid' => 0));
			} else {
				$row = ForumTopic::where('id', $request->id)->update(array('is_paid' => 1));
			}
			return response()->json(['status'=>'success', 'message'=>'Updated successfully.']);
		}else{
			return response()->json(['status'=>'error', 'message'=>'Error Occured.']);
		}

	}
	
	#Remove Category
	public function destroy($id)
	{
        ForumTopic::find($id)->delete();
        return response()->json(['status'=>'success', 'message'=>'Deleted successfully.']);
	}

	#verify category
	public function verify_topic($id)
	{
		$forum = ForumTopic::where('id', $id)->update(array('is_verified'=>1));
        return response()->json(['status'=>'success', 'message'=>'Verify successfully.']);
	}
	
	
	
	



	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

	

	
	
}
