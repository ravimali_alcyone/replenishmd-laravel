<?php

namespace App\Http\Controllers\front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Session;
use Auth;
use DB;
use App\User;
use App\SocialUserFollowConnection;
use App\OnlineVisit;
use App\Article;
use App\SupporterRequest;
use App\ProviderService;
use App\Notifications;
use App\OnlineVisitProviderAssign;
use App\Events\FollowRequest;
use App\Events\AcceptRequest;

class SupporterController extends Controller {
    /**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct() {
		$this->middleware('common_auth');
	}

    public function supporters() {
		$followers_data = [];
		$users_data = [];
		$requests_data = [];
		$followers_count = '';
		$user_id = auth()->user()->id;
					
		if(auth()->user()->user_role==4){
			$data = $this->get_supporters(true);
		}else{
			$data = $this->get_provider_supporters(true);
		}
		
		if ($data) {
			$followers_data = $data['followers_data'];
			$followers_count = $data['followers_count'];

			// New users data
			if(!empty($_GET['search'])){
				$users_data = $this->get_known_persons($data['ids'], 'supporters',$_GET['search']);
			}else{
				$users_data = $this->get_known_persons($data['ids'], 'supporters');
			}
			

			// Get requests data
			$requests_data = SupporterRequest::select('supporter_requests.*', 'users.name', 'users.image', 'users.clinical_expertise')
							->leftJoin('users', 'supporter_requests.request_id', '=', 'users.id')
							->where('supporter_requests.user_id', $user_id)
							->where('supporter_requests.status', 0)
							->where('supporter_requests.is_ignore', 0)
							->get()->toArray();
		}
		
		//Supporting - I am one of the supporter of these users.
		$supporting = $this->get_supporting(auth()->user()->id);
				
		//Blog Stories
		$blogs = $this->get_blogs();

		//Latest Post
		$post = $this->get_latest_posts();
		
		return view('front.supporters.supporters', [
			'users_data' => !empty($users_data['users_data'])? $users_data['users_data']:'',
			'followers_data' => $followers_data,
			'supporting_data' => $supporting ? $supporting['users'] : array(),
			'supporting_data_ids' => $supporting ? $supporting['users_ids'] : array(),
			'requests_data' => $requests_data,
			'followers_count' => $followers_count,
			'persons_count' => !empty($users_data['persons_count'])?$users_data['persons_count']:'',
			'page' => 'supporters',
			'blogs' => $blogs,
			'posts' => $post
		]);
	}

	public function request_for_follow(Request $request) {
		$data = $request->all();
		$result = SupporterRequest::create($data);
		if ($result) {
			$user_data = user::where('id', $result->user_id)->first();
			//pusher notification send
			$follower_id =$request->user_id;
			if(!empty(auth()->user()->image)){
				$img =auth()->user()->image;
			}else{
				$img='dist/images/user_icon.png';
			}
			$noticeData = array(
				'created_id' => auth()->user()->id,
				'created_name' => auth()->user()->name,
				'created_image' => $img,
				'notification_type' => "supporter_requests",
				'notice_type' =>'supporter_request',
				'post_id' => $request->user_id,
				'followers_id' =>$follower_id,
				'status' => 1,
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			);
			
			$notice = Notifications::create($noticeData);
			
			if($notice){
				$data['user_id'] = $result->user_id;
				$data['name'] = auth()->user()->name;
				$data['image'] = $img;
				$data['clinical_expertise'] = $user_data->clinical_expertise;
				
				//get follow_users
				$follow_users='';
				$user_follow = SocialUserFollowConnection::where('user_id', auth()->user()->id)->first();
				$request_user =SocialUserFollowConnection::where('user_id', $request->user_id)->first();
				if(!empty($user_follow) && !empty($request_user)){
					$user_follower_id = explode(",",$user_follow['follower_id']);
					$request_follower_id = explode(",",$request_user['follower_id']);
					$mane_id= array_intersect($request_follower_id,$user_follower_id);
					$name='';
					if(!empty($mane_id)){
						
						foreach($mane_id as $fval){
							$user_name = user::where('id', $fval)->first();
							$name.=$user_name['name'].', ';
						}
						if(count($mane_id)>1){
							$follow_users = '<p>'.$name.' '.count($mane_id).' mutual supporters.</p>';
						}else{
							$follow_users = '<p>'.$name.' mutual supporter.</p>';
						}
					}
				}
				$data['follow_users']= $follow_users;
				$data['date_time']= date(env('DATE_FORMAT_PHP_H'));
				$data['id']= $notice->id;
				
				event(new FollowRequest($data));
			}
			return response(true);
		} else {
			return response(false);
		}
	}

	public function request_accept(Request $request) {
		$data = $request->all();
		SupporterRequest::where(['user_id' => $data['user_id'], 'request_id' => $data['follower_id']])->update(['status' => 1]);

		$follower = SupporterRequest::where(['user_id' => $data['user_id'], 'request_id' => $data['follower_id']])->first();
		$user_data = User::where('id', $follower->user_id)->first();
		$response['user_id'] = "$follower->user_id";
		$response['request_id'] = "$follower->request_id";
		$response['name'] = $user_data->name;
		$response['image'] = $user_data->image ?? '/dist/images/user_icon.png';
		$response['clinical_expertise'] = $user_data->clinical_expertise;
		$response['date_time']= date(env('DATE_FORMAT_PHP_H'));
		$follower_id =$follower->request_id;
		if(!empty(auth()->user()->image)){
				$img =auth()->user()->image;
		}else{
			$img='dist/images/user_icon.png';
		}
		$noticeData = array(
			'created_id' => auth()->user()->id,
			'created_name' => auth()->user()->name,
			'created_image' => $img,
			'notification_type' => "supporter_requests",
			'post_id' => $follower->request_id,
			'followers_id' =>$follower_id,
			'status' => 1,
			'created_at' => date('Y-m-d H:i:s'),
			'updated_at' => date('Y-m-d H:i:s')
		);
				
		$user = SocialUserFollowConnection::where('user_id', $data['user_id'])->first();
		if ($user) {
			$user_data = $user->toArray();
			$follower_ids = explode(',', $user_data['follower_id']);
			$follower_ids[] = $data['follower_id'];
			$follower_ids = implode(',', $follower_ids);

			$result = SocialUserFollowConnection::where('user_id', $data['user_id'])->update(['follower_id' => $follower_ids]);
			if ($result) {
				//pusher notification send
				$noticeData['notice_type']='supporter_accept';
				$notice = Notifications::create($noticeData);
				$response['id']=$notice->id;
				event(new AcceptRequest($response));
				
				return response(true);
			} else {
				return response(false);
			}
		} else {
			$result = SocialUserFollowConnection::create($data);
			if ($result) {
				//pusher notification send
				$noticeData['notice_type']='supporter_accept';
				$notice = Notifications::create($noticeData);
				$response['id']=$notice->id;
				event(new AcceptRequest($response));
				
				return response(true);
			} else {
				return response(false);
			}
		}
	}

	public function request_ignore(Request $request) {
		$data = $request->all();
		$result = SupporterRequest::where(['user_id' => $data['user_id'], 'request_id' => $data['follower_id']])->update(['is_ignore' => 1]);
		if ($result) {
			return response(true);
		} else {
			return response(false);
		}
	}

	public function all_supporters() {
		
		if(auth()->user()->user_role==4){
			$data = $this->get_supporters(false);
		}else{
			$data = $this->get_provider_supporters(false);
		}
						
		//Supporting - I am one of the supporter of these users.
		$supporting = $this->get_supporting(auth()->user()->id);	
		
		//Blog Stories
		$blogs = $this->get_blogs();
				
		//Latest Post
		$post = $this->get_latest_posts();
		
		return view('front.supporters.all_supporters', [
			'followers_data' => $data['followers_data'],
			'page' => 'supporters',
			'blogs' => $blogs,
			'posts' => $post,
			'supporting_data' => $supporting ? $supporting['users'] : array(),
			'supporting_data_ids' => $supporting ? $supporting['users_ids'] : array(),		
		]);
	}

	public function get_supporters($param) {
		$ids = [];
		$followers_data = [];
		$followers_ids = [];
		$user_id = Auth::id();
		$user = OnlineVisit::select()->where('patient_id', $user_id)->latest()->first();
		$followers_count = '';

		if ($user) {
			$service_id = $user->service_id;
			$visit_type = $user->visit_type;
			$patients_data = OnlineVisit::select('patient_id')->where('service_id', $service_id)->orWhere('visit_type', $visit_type)->get()->unique('patient_id');
			$providers_data = ProviderService::select('provider_id')->where('service_id', $service_id)->get()->unique('provider_id');

			foreach ($patients_data as $key => $value) {
				$ids[$value->patient_id] = $value->patient_id;
			}
			foreach ($providers_data as $key => $value) {
				$ids[$value->provider_id] = $value->provider_id;
			}

			$followers = SocialUserFollowConnection::where('user_id', $user_id)->first();
			$followers_ids = explode(',', $followers['follower_id']);
			$followers_count = count($followers_ids);

			foreach ($ids as $key => $value) {
				foreach ($followers_ids as $key2 => $value2) {
					if ($value == $value2) {
						unset($ids[$value]);
					}
				}
			}

			// Followers data
			foreach ($followers_ids as $key => $value) {
				if ($param) {
					if ($key >= 10) {
						break;
					}
				}

				$follower_data = User::select('id', 'name', 'image', 'clinical_expertise')->where('id', $value)->first();
				if ($follower_data !== null) {
					$followers_data[$key] = [
						'id' => $follower_data->id,
						'name' => $follower_data->name,
						'image' => $follower_data->image,
						'clinical_expertise' => $follower_data->clinical_expertise
					];
				}
			}
		}
		//check follower users
		$check_followers = SocialUserFollowConnection::whereIn('follower_id', [$user_id])->get();
		
		if(!empty($check_followers) && $check_followers->count()>0){
			$check_followers_id=array();
			foreach($check_followers as $fkey=>$fvalue){
				$check_id = explode(",",$fvalue->follower_id);
				foreach($check_id as $cval){
					if($cval!=$user_id){
						if(!empty($followers_ids) && count($followers_ids)>0){
							if(!in_array($cval, $followers_ids)){
								$check_followers_id[]=$cval;
							}
						}else{
							$check_followers_id[]=$cval;
						}
						
					}
				}
			}
			$ids = array_unique(array_merge($ids,$check_followers_id));
		}
			
		
		return ['followers_data' => $followers_data, 'ids' => $ids, 'followers_count' => $followers_count,'followers_ids'=>$followers_ids];
	}

	public function get_provider_supporters($param) {
		$ids = [];
		$followers_data = [];
		$followers_ids = [];
		$user_id = Auth::id();
		
		$user = ProviderService::select(DB::raw('group_concat(service_id) as serviceId'))->where('status', 1)->where('provider_id', $user_id)->groupBy('provider_id')->first();
		$followers_count = '';
		if ($user) {
			$service_id = $user->serviceId;
			$patients_data = OnlineVisit::select('patient_id')->whereIn('service_id',[$service_id])->get()->unique('patient_id');
			
			$providers_data = ProviderService::select('provider_id')->whereIn('service_id', [$service_id])->whereNotIn('provider_id', [$user_id])->get()->unique('provider_id');
			if(!empty($patients_data) && $patients_data->count()>0){
				foreach ($patients_data as $key => $value) {
					$ids[$value->patient_id] = $value->patient_id;
				}
			}
			
			if(!empty($providers_data) && $providers_data->count()>0){
				foreach ($providers_data as $key => $value) {
					$ids[$value->provider_id] = $value->provider_id;
				}
			}
			
			$followers = SocialUserFollowConnection::where('user_id', $user_id)->first();
			if(!empty($followers)){
				$followers_ids = explode(',', $followers['follower_id']);
			}else{
				$followers_ids=array();
			}
			
			$followers_count = count($followers_ids);

			foreach ($ids as $key => $value) {
				foreach ($followers_ids as $key2 => $value2) {
					if ($value == $value2) {
						unset($ids[$value]);
					}
				}
			}

			// Followers data
			foreach ($followers_ids as $key => $value) {
				if ($param) {
					if ($key >= 10) {
						break;
					}
				}

				$follower_data = User::select('id', 'name', 'image', 'clinical_expertise')->where('id', $value)->first();
				if ($follower_data !== null) {
					$followers_data[$key] = [
						'id' => $follower_data->id,
						'name' => $follower_data->name,
						'image' => $follower_data->image,
						'clinical_expertise' => $follower_data->clinical_expertise
					];
				}
			}
		}
		
		//check follower users
		$check_followers = SocialUserFollowConnection::whereIn('follower_id', [$user_id])->get();
		
		if(!empty($check_followers) && $check_followers->count()>0){
			$check_followers_id=array();
			foreach($check_followers as $fkey=>$fvalue){
				$check_id = explode(",",$fvalue->follower_id);
				foreach($check_id as $cval){
					if($cval!=$user_id){
						if(!empty($followers_ids) && count($followers_ids)>0){
							if(!in_array($cval, $followers_ids)){
								$check_followers_id[]=$cval;
							}
						}else{
							$check_followers_id[]=$cval;
						}
						
					}
				}
			}
			$ids = array_unique(array_merge($ids,$check_followers_id));
		}
		

		return ['followers_data' => $followers_data, 'ids' => $ids, 'followers_count' => $followers_count,'followers_ids'=>$followers_ids];
	}
	
	public function request_for_unfollow(Request $request) {
		$user_id = Auth::id();
		$follow_id = $request->id;
		$result = SupporterRequest::where(['user_id' => $user_id, 'request_id' => $follow_id])->update(['status' => 0, 'is_ignore' => 1]);
		$follow_user = SocialUserFollowConnection::where('user_id', $user_id)->first();
		$follower_id = [];
		if ($follow_user) {
			$followers_ids = explode(',', $follow_user->follower_id);
			foreach ($followers_ids as $key => $value) {
				if ($value !== $follow_id) {
					$follower_id[] = $value;
				}
			}
			$result = SocialUserFollowConnection::where('user_id', $user_id)->update(['follower_id' => implode(',', $follower_id)]);
			if ($result) {
				return true;
			}
		}
	}

	public function all_known_persons() 
	{
		if(auth()->user()->user_role==4){
			$data = $this->get_supporters(true);
		}else{
			$data = $this->get_provider_supporters(true);
		}
		
		//Supporting - I am one of the supporter of these users.
		$supporting = $this->get_supporting(auth()->user()->id);
		
		$blogs = $this->get_blogs();
		$posts = $this->get_latest_posts();
		
		if(!empty($_GET['search'])){
			$search =$_GET['search'];
			$user_ids = User::select('id', 'name', 'image', 'clinical_expertise')->whereIN('user_role',[3,4])->where('id', '!=', auth()->user()->id)->where('name', 'LIKE', "%{$search}%")->get();
			
			return view('front.supporters.all_known_persons', ['users_data' => $user_ids, 'page' => 'search_result','followers_ids'=>$data['followers_ids'], 'blogs' => $blogs, 'posts' => $posts]);
		}else{
			$persons_data = $this->get_known_persons($data['ids'], 'all_known_persons');
			
			return view('front.supporters.all_known_persons', [
			'users_data' => $persons_data['users_data'],
			'page' => 'supporters',
			'blogs' => $blogs,
			'posts' => $posts,
			'supporting_data' => $supporting ? $supporting['users'] : array(),
			'supporting_data_ids' => $supporting ? $supporting['users_ids'] : array(),			
			]);
		}
	}

	public function get_known_persons($param, $page, $search='') {
		$user_id = Auth::id();
		$users_data = [];
		$persons_count = count($param);

		foreach ($param as $key => $value) {
			$requested_data = SupporterRequest::where(['user_id' => $value, 'request_id' => $user_id, 'status' => 1])->first();
			$requested_pending_data = SupporterRequest::where(['user_id' => $value, 'request_id' => $user_id])->where('status', '!=', 1)->first();
			if(!empty($search)){
				$user_data = User::select('id', 'name', 'image', 'clinical_expertise')->where('id', $value)->where('name', 'LIKE', "%{$search}%")->first();
			}else{
				$user_data = User::select('id', 'name', 'image', 'clinical_expertise')->where('id', $value)->first();
			}
			if($user_data !== null) {
				if($user_data->id !== $user_id) {
					if(!isset($requested_data)) {
						$users_data[$key] = [
							'id' => $user_data->id,
							'name' => $user_data->name,
							'image' => $user_data->image,
							'clinical_expertise' => $user_data->clinical_expertise,
							'request_status' => ''
						];
					}

					if (isset($requested_pending_data)) {
						$users_data[$key] = [
							'id' => $user_data->id,
							'name' => $user_data->name,
							'image' => $user_data->image,
							'clinical_expertise' => $user_data->clinical_expertise,
							'request_status' => 'Pending'
						];
					}
				}
			}

			if ($page == 'supporters') {
				if (count($users_data) >= 10) {
					break;
				}
			}
		}
		return ['users_data' => $users_data, 'persons_count' => $persons_count];
	}
	
	function get_blogs(){
		
		$result = Article::select('*')
					->where('post_publish','=',1)
					->where('post_visibility','=',1)
					->where('is_verify',1)
					->limit(4)
					->latest()
					->get();	
		return $result;
	}

	function get_latest_posts(){
		#Stories detail
		$followerId = SocialUserFollowConnection::where('user_id', auth()->user()->id)->get();
		$followerId = (count($followerId)>0) ? $followerId[0]->follower_id : 0;
		
		if($followerId){
			$data =  DB::select( DB::raw("SELECT media_name from social_posts_media left join social_posts on
		social_posts_media.post_id = social_posts.post_id where social_posts.userid=".auth()->user()->id." OR (social_posts.userid IN (".$followerId.")) ORDER BY `social_posts_media`.media_id DESC LIMIT 16"));
		}else{
			$data = false;
		}
		return $data;	
	}
	
	public function get_supporting($user_id){
		
		$result = array();
		//Supporting - I am one of the supporter of these users
		$supporting_users = SocialUserFollowConnection::select("user_id")->whereRaw("find_in_set(".$user_id.",follower_id)")->pluck('user_id');
		
		if($supporting_users){
			$users = User::select('id', 'name', 'image', 'clinical_expertise')->whereIn('id', $supporting_users->toArray())->get();
			
			if($users){
				$result['users_ids'] = $supporting_users->toArray();
				$result['users'] = $users->toArray();
			}
		}
		
		//echo '<pre>';print_r($result);die;

		return $result;
	}

}
