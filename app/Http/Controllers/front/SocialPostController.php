<?php

namespace App\Http\Controllers\front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Stripe\SetupIntent;
use Illuminate\Support\Facades\Validator;
use Session;
use App\Library\Services\CommonService;
use App\Library\Services\Services_JSON;
use App\User;
use App\SocialPosts;
use App\SocialPostsMedia;
use App\SocialPostCommentHub;
use App\SocialPostLikeHub;
use App\SocialStory;
use App\SocialUserFollowConnection;
use App\SocialPostMediaCommentHub;
use App\OnlineVisit;
use App\SupporterRequest;
use App\ProviderService;
use App\Article;
use DB;
use App\Notifications;
use App\Events\PostComment;
use App\Events\PostCrete;
use App\Events\PostLike;
use Auth;
use Laravel\Socialite\Facades\Socialite;
use App\FacebookAuth;


class SocialPostController extends Controller
{
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('user_auth');
		
	}

	/**
	 * Show the application dashboard.
	 *
	 * @return \Illuminate\Contracts\Support\Renderable
	 */

	public function index(Request $request){
		
		if ($request->ajax()) {
			
			#Stories detail
			$followerId = SocialUserFollowConnection::where('user_id', auth()->user()->id)->get();
			$followerId = (count($followerId)>0) ? $followerId[0]->follower_id : 0;
			
			$story = DB::select( DB::raw("SELECT `social_stories`.*,`users`.name as `storyBy`,`users`.image as `profilePic` from `social_stories`
							INNER JOIN `users` ON `social_stories`.`user_id`=`users`.`id` where `user_id`=".auth()->user()->id." OR (`user_id` IN (".$followerId.")) AND `social_stories`.`display_status` =1 ORDER BY `social_stories`.id DESC"));
			if(!empty($story)){
				return response()->json(['status'=> 'success', 'data'=>$story]);	
			}else{
				return response()->json(['status'=> 'error', 'redirect'=> '', 'message' => '']);
			}
			
		}
		else{
			$data['page'] = 'user_dashboard';
			$data['meta_title'] = 'Dashboard';
			$data['meta_description'] = 'ReplenishMD is a new age integrative, Functional, Anti-Aging, Regenerative Medicine, MedSpa, Mobile Lab & Concierge Medical Clinic at your service.';
			
			#Stories detail
			$followerId = SocialUserFollowConnection::where('user_id', auth()->user()->id)->get();
			$followerId = (count($followerId)>0) ? $followerId[0]->follower_id : 0;
			DB::enableQueryLog();
			
			$storyData = $data['story'] = DB::select( DB::raw("SELECT `social_stories`.*,`users`.name as `storyBy`,`users`.image as `profilePic` from `social_stories`
							INNER JOIN `users` ON `social_stories`.`user_id`=`users`.`id` where `user_id`=".auth()->user()->id." OR (`user_id` IN (".$followerId.")) AND `social_stories`.`display_status` =1 ORDER BY `social_stories`.id DESC"));
							
			$data['storyData'] = array();
			
			if($storyData){
				foreach($storyData as $key=> $value){
					$user_id = $value->user_id;
					$data['storyData'][$user_id]['username'] = $value->storyBy;
					if(!empty($value->profilePic)){
						$data['storyData'][$user_id]['image'] = $value->profilePic;
					}else{
						$data['storyData'][$user_id]['image'] = 'dist/images/user_icon.png';	
					}
					$data['storyData'][$user_id]['user_id'] = $user_id;
					$data['storyData'][$user_id]['link_url'] = '';
					$data['storyData'][$user_id]['timestamp'] = time();
					$data['storyData'][$user_id]['stories'][] = $value;
					
				}
			}
			
			if(!empty(auth()->user()->chat_id)){
				//$this->unread_message();
			}
			
			//Blog Stories
			$data['blogs'] = Article::select('*')->where('post_publish','=',1)->where('post_visibility','=',1)->where('is_verify',1)->limit(4)->latest()->get();
			
			//Latest Post
			$data['posts'] = $this->get_lates_post();
			return view('front.user.index', $data);
		}
	}

	function get_lates_post(){
		#Stories detail
		$followerId = SocialUserFollowConnection::where('user_id', auth()->user()->id)->get();
		$followerId = (count($followerId)>0) ? $followerId[0]->follower_id : 0;
		
		$data =  DB::select( DB::raw("SELECT media_name from social_posts_media left join social_posts on
		social_posts_media.post_id = social_posts.post_id where social_posts.userid=".auth()->user()->id." OR (social_posts.userid IN (".$followerId.")) ORDER BY `social_posts_media`.media_id DESC LIMIT 16"));
		return $data;	
	}
	
	#Load Dashboad data
	public function load_data(Request $request)
	{
	    $id = ($request->user_id !="") ? $request->user_id : auth()->user()->id;
	    $data['flagId'] =$id;
	    
		$followerId = SocialUserFollowConnection::where('user_id', $id)->get();
		
		$followerId = (count($followerId)>0) ? $followerId[0]->follower_id : 0;
		
		$postPerPage = 4;
		$data['offset1'] = 4;
		$data['offset2']=4;
		/*$data['post'] = DB::select( DB::raw("SELECT `social_posts`.*,`users`.name as `postBy`,`users`.image as `postImage` from `social_posts`
						INNER JOIN `users` ON `social_posts`.`userid`=`users`.`id` where `userid`=".$id." OR (`userid` IN (".$followerId.")) ORDER BY `social_posts`.post_id DESC LIMIT 0,".$postPerPage.""));*/
		$data['post'] = DB::select( DB::raw("SELECT `social_posts`.*,`users`.name as `postBy`,`users`.image as `postImage` from `social_posts`
						INNER JOIN `users` ON `social_posts`.`userid`=`users`.`id` where `userid`=".$id." OR (`userid` IN (".$followerId.")) 
						AND CASE WHEN `userid` = ".$id." THEN (`social_posts`.`access_type`=1 OR `social_posts`.`access_type`=2 OR `social_posts`.`access_type`=3)  
						WHEN `userid` IN (".$followerId.") THEN `social_posts`.`access_type`=1
						ELSE 1=1  
						END
						OR CASE WHEN `userid` != ".$id." THEN (`social_posts`.`access_type`=1)  
							WHEN `userid` NOT IN (".$followerId.") THEN `social_posts`.`access_type`=1
							ELSE 1=1  
						END
						ORDER BY `social_posts`.post_id DESC LIMIT 0,".$postPerPage.""));
		
		#load more post count
		$st_offset = 4;
		$en_offset = 4;
		/*$data['load_post'] = DB::select( DB::raw("SELECT `social_posts`.*,`users`.name as `postBy`,`users`.image as `postImage` from `social_posts`
						INNER JOIN `users` ON `social_posts`.`userid`=`users`.`id` where `userid`=".$id." OR (`userid` IN (".$followerId.")) ORDER BY `social_posts`.post_id DESC LIMIT ".$st_offset.",".$en_offset." "));*/
		$data['load_post'] = DB::select( DB::raw("SELECT `social_posts`.*,`users`.name as `postBy`,`users`.image as `postImage` from `social_posts`
						INNER JOIN `users` ON `social_posts`.`userid`=`users`.`id` where `userid`=".$id." OR (`userid` IN (".$followerId.")) 
						AND CASE WHEN `userid` = ".$id." THEN (`social_posts`.`access_type`=1 OR `social_posts`.`access_type`=2 OR `social_posts`.`access_type`=3)  
						WHEN `userid` IN (".$followerId.") THEN `social_posts`.`access_type`=1
						ELSE 1=1  
						END
						OR CASE WHEN `userid` != ".$id." THEN (`social_posts`.`access_type`=1)  
							WHEN `userid` NOT IN (".$followerId.") THEN `social_posts`.`access_type`=1
							ELSE 1=1  
						END
						ORDER BY `social_posts`.post_id DESC LIMIT ".$st_offset.",".$en_offset." "));
		#Post Media detail
		$posts_media = SocialPostsMedia::select('media_id','post_id', 'media_name')->get();
		$data['posts_media'] = [];

		if (!empty($posts_media)) {
			foreach($posts_media as $key => $value) {
				//$data['posts_media'][$value->post_id][] = $value->media_name;
				$data['posts_media'][$value->post_id][] = array('media_name'=>$value->media_name,'media_id'=>$value->media_id);
			}
 		}
		
		#Post Like detail
		$post_likes = SocialPostLikeHub::where('like_by', $id)->get();
		$data['post_likes'] = [];
		$data['post_likes_flagData'] = [];
		if (!empty($post_likes)) {
			foreach($post_likes as $key => $value) {
				$data['post_likes'][$value->post_id] = $value->like_flag;
				$data['post_likes_flagData'][$value->post_id][] = array(
					"like_flag" => $value->like_flag,
					"love_flag"    => $value->love_flag,
					"sad_flag"    => $value->sad_flag,
					"angary_flag"    => $value->angary_flag
				);
				
			}
 		}
		
		#Post Comment detail
		$post_comment = SocialPostCommentHub::select('social_post_comment_hub.comment_id','social_post_comment_hub.post_id','social_post_comment_hub.comments','users.name as commentBy','users.image')
							 ->join('users','social_post_comment_hub.comment_by','=','users.id')
							 ->orderBy('social_post_comment_hub.comment_id')
							 ->get();
		
		$data['post_comment'] = [];
		if(!empty($post_comment))
		{
			foreach($post_comment as $key => $value)
			{
				$data['post_comment'][$value->post_id][] = array(
					"comments" => $value->comments,
					"commentBy"    => $value->commentBy,
					"image"    => $value->image,
					"comment_id"    => $value->comment_id
				);
			}
		}
		
		#Name of person like on post
		$likePostUserData = DB::select( DB::raw("SELECT DISTINCT `post_id`, 
				REPLACE(substring_index(GROUP_CONCAT(users.name), ',', 3), '".auth()->user()->name."', 'You') as `Twoperson` 
				FROM social_post_like_hub inner join users ON social_post_like_hub.`like_by`=users.id 
				WHERE `like_by` != ".$id." OR `like_by` = ".$id." 
				GROUP BY `post_id` ORDER BY post_id DESC"));
		
		$data['likePostUserData'] = [];
		if (!empty($likePostUserData)) {
			foreach($likePostUserData as $key => $value) {
				$data['likePostUserData'][$value->post_id][] = $value->Twoperson;
			}
 		}
		
		
		$output = view('front.user.load_all_data',$data)->render();
		return response()->json(['status'=> 'success', 'message' => 'Success', 'data' => $output]);
	}
	
	#Get image detail for preview
	public function get_post_images(Request $request)
	{
		// get photo info
		$aImageInfo = SocialPostsMedia::select('media_name')->where('media_id',$request->id)->where('post_id',$request->post_id)->first();
		
		$mediaCount = SocialPostsMedia::select('*')->where('post_id',$request->post_id)->get();
		$mediaDate = date('m M.Y',strtotime($mediaCount[0]->created_at));
		$mediaCounts = $mediaCount->count();

		// prepare last 10 comments
		$sCommentsBlock = SocialPostMediaCommentHub::select('social_post_media_comment_hub.*', 'users.name', 'users.image')
				->leftJoin('users', 'social_post_media_comment_hub.comment_by', '=', 'users.id')
				->where('social_post_media_comment_hub.media_id', $request->id)
				->where('social_post_media_comment_hub.post_id', $request->post_id)
				->orderby('created_at','DESC')
				->get();

		

		$commentHtml = '';
		foreach($sCommentsBlock as $val)
		{
			$commentHtml .= '<li><div class="cr_head"><div class="cr_img"><img src="/'.$val->image.'" alt="'.$val->name.'"></div><div class="cr_info"><div><strong>'.$val->name.'</strong><p class="comment_txt">'.$val->comment.'</p></div><small>'.date('m M.Y H:i',strtotime($val->created_at)).'</small></div></div></li>';
		}
		
		// Prev & Next navigation
		$sNext = $sPrev = '';
		$iPrev = SocialPostsMedia::select('media_id')->where('media_id','<',$request->id)->where('post_id',$request->post_id)->orderby('media_id','DESC')->limit(1)->first();
		
		$iPrev = (!empty($iPrev)) ? $iPrev->media_id : '';
		$iNext = SocialPostsMedia::select('media_id')->where('media_id','>',$request->id)->where('post_id',$request->post_id)->orderby('media_id','ASC')->limit(1)->first();
		$iNext = (!empty($iNext)) ? $iNext->media_id : '';
		$sPrevBtn = ($iPrev) ? '<button type="button" id="lg-prev-1" aria-label="Previous slide" class="preview_prev" onclick="getPhotoPreviewAjx('.$iPrev.','.$request->post_id.')"> &#8249;</button>' : '';
		$sNextBtn = ($iNext) ? '<button type="button" id="lg-next-1" aria-label="Next slide" class="preview_next" onclick="getPhotoPreviewAjx('.$iNext.','.$request->post_id.')"> &#8250;</button>' : '';
		
		$red = '<img class="fileUnitSpacer" src="/'. $aImageInfo->media_name .'">' . $sPrevBtn . $sNextBtn;
		
		return response()->json(['status'=> 'success', 'data1' => $red, 'data2' => $commentHtml, 'data3' => $mediaCounts, 'data4' => $mediaDate]);
		
	}
	
	# Store post comment into table

	public function store_media_comment(Request $request)
	{
		if($request->ajax()) {
			$messages = [
				'comment.regex' => 'Comment field is required filed.',
			];
			
			$validator = Validator::make($request->all(), [
				'comment' => 'required',
			], $messages)->validate();	
			
			$input_data = array(
				'post_id' => $request->post_id,
				'media_id' => $request->media_id,
				'comment' => $request->comment,
				'comment_by' => auth()->user()->id,
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s'),
			);

			SocialPostMediaCommentHub::create($input_data);
			$msg = 'Commented successfully.';
			return response()->json(['status'=> 'success', 'redirect'=> '', 'message' => $msg, 'comment'=>$request->comment, 'commentdate'=>date('Y-m-d H:i')]);					
		
		}
		else
		{
			$msg = 'Error occured. Please try again.';
			return response()->json(['status'=> 'error', 'redirect'=> '', 'message' => $msg]);				
		}
	}

	#Add post 
	public function add_post(Request $request)
	{
		if ($request->ajax()) {

			$attachment = (!empty($request->adicionafoto)) ? count($request->adicionafoto) : 0;

			if($attachment==0 && $request->post_content=="")
			{
				$messages = [
					'post_content.regex' => 'Post content required.',
					'adicionafoto.regex' => 'Post attachment is required filed.',
				];
				$validator = Validator::make($request->all(), [
					'post_content' => 'required',
					'adicionafoto' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
				], $messages)->validate();
			}
			elseif($attachment==0 && $request->post_content!="")
			{
				$messages = [
					'post_content.regex' => 'Post content required.',
				];
				$validator = Validator::make($request->all(), [
					'post_content' => 'required',
				], $messages)->validate();
			}
			elseif($attachment>=1 && $request->post_content=="")
			{
				$messages = [
					'adicionafoto.regex' => 'Post attachment is required filed.',
				];
				$validator = Validator::make($request->all(), [
					'adicionafoto' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
				], $messages)->validate();
			}

			#Upload attachment
			if (!empty($request->adicionafoto)) {
				$num = count($request->file('adicionafoto'));
				$cred_images=array();
				for($i=0; $i<$num; $i++)
				{
					$file = $request->file('adicionafoto')[$i];
					if(strtolower($file->getClientOriginalExtension()) == 'jpg' || strtolower($file->getClientOriginalExtension()) == 'png' || strtolower($file->getClientOriginalExtension()) == 'jfif'){
						$name=time().'-'.rand(11111111,99999999).'.'.strtolower($file->getClientOriginalExtension());
						$destinationPath = 'uploads/social_post/';

						if($file->move($destinationPath, $name)){
							$image = 'uploads/social_post'.'/'.$name;
							$status = 'success';
							$message = 'Attachment uploaded successfully.';
							$cred_images[]= $image;
						}

					}
					else{
						$status = 'error';
						$message = 'Only .jpg and .png files are allowable';
					}
				}
				

			}
			$is_media = (!empty($request->adicionafoto)) ? 1 : 0;
			$postData = array(
				'userid' => auth()->user()->id,
				'post_content' => $request->post_content,
				'access_type' => $request->post_access,
				'is_media' => $is_media,
				'param_id' => rand()
			);

			$post = SocialPosts::create($postData);
			$insertedId = $post->id;
			
			#insert notification table
			if(!empty($insertedId)){
			    $followerId = SocialUserFollowConnection::where('user_id', auth()->user()->id)->get();
			
				if(!empty($followerId) && count($followerId)>0){
					$follower_id='';
					if(count($followerId)>0){
						$followers_id = explode(",",$followerId[0]->follower_id);
						$follower_id = json_encode($followers_id);
					
					}
					if(!empty(auth()->user()->image)){
						$img =auth()->user()->image;
					}else{
						$img='dist/images/user_icon.png';
					}
					$noticeData = array(
						'created_id' => auth()->user()->id,
						'created_name' => auth()->user()->name,
						'created_image' => $img,
						'notification_type' => "post_create",
						'post_id' => $insertedId,
						'status' => 1,
						'created_at' => date('Y-m-d H:i:s'),
						'updated_at' => date('Y-m-d H:i:s')
					);
					
					foreach($followers_id as $key=>$value){
						$noticeData['followers_id']=$value;
						$notice = Notifications::create($noticeData);
					}
					//pusher notification send
					$noticeData['followers_id']=$follower_id;
					$noticeData['post_content']=$request->post_content;
					$noticeData['access_type']=$request->post_access;
					$noticeData['is_media']=$request->is_media;
					$noticeData['param_id']=rand();
					event(new PostCrete($noticeData));
					
				}
				
			}
			
			
			#Insert media file into table
			if(!empty($request->adicionafoto)){
				$mediaData = array();
				for($i=0;$i<(count($cred_images));$i++)
				{
					$data= array(
						"post_id"=>$insertedId,
						"media_name"=>$cred_images[$i],
						"created_at" => date('Y-m-d H:i:s')
					);
					$mediaData[] = $data;
				}
				$postMedia = SocialPostsMedia::insert($mediaData);

				if($postMedia)
				{
					$msg = 'Post created successfully.';
				}
			}
			$msg = 'Post created successfully.';
			$redirect = route('user.dashboard');

			return response()->json(['status'=> 'success', 'redirect'=> $redirect, 'message' => $msg]);
		}else{
			$redirect = route('user.dashboard');
			return response()->json(['status'=> 'error', 'redirect'=> $redirect, 'message' => 'This request is not valid.', 'data' => '']);
		}
	}

	public function add_post_comment(Request $request)
	{
		if ($request->ajax()) {

			$messages = [
				'add_post_comment.regex' => 'Comment field required.',
			];
			$validator = Validator::make($request->all(), [
				'add_post_comment' => 'required',
			], $messages)->validate();

			$commentData = array(
				'post_id' => $request->post_id,
				'comment_by' => auth()->user()->id,
				'comments' => $request->add_post_comment
			);
			$comment = SocialPostCommentHub::create($commentData);
			
			#insert notification table
			if(!empty($comment)){
			    $followerId = SocialPosts::where('post_id', $request->post_id)->get();
				//print_r($followerId);
				$follower_id='';
				$viewer_id =array();
				if(!empty($followerId) && ($followerId[0]->userid != auth()->user()->id) ){
					if(count($followerId)>0){
						$follower_id =$followerId[0]->userid;
						
					}
					if(!empty(auth()->user()->image)){
						$img =auth()->user()->image;
					}else{
						$img='dist/images/user_icon.png';
					}
					$noticeData = array(
						'created_id' => auth()->user()->id,
						'created_name' => auth()->user()->name,
						'created_image' => $img,
						'notification_type' => "post_comment",
						'post_id' => $request->post_id,
						'followers_id' => $follower_id,
						'status' => 1,
						'created_at' => date('Y-m-d H:i:s'),
						'updated_at' => date('Y-m-d H:i:s')
					);
					
					$notice = Notifications::create($noticeData);
					$noticeData['comment']=$request->add_post_comment;
					$noticeData['id']=$notice->id;
						event(new PostComment($noticeData));
				}
				
			}
			
			$redirect = route('user.dashboard');
			$msg = "Your comment post successfully.";
			if($comment)
			{
				return response()->json(['status'=> 'success', 'redirect'=> '', 'message' => $msg]);
			}
			else
			{
				return response()->json(['status'=> 'error', 'redirect'=> $redirect, 'message' => 'This request is not valid.', 'data' => '']);
			}
		}
		else{
			$redirect = route('user.dashboard');
			return response()->json(['status'=> 'error', 'redirect'=> $redirect, 'message' => 'This request is not valid.', 'data' => '']);
		}
	}

	public function add_post_like(Request $request) {
		if ($request->ajax()) {
			$likeFlag = $request->likeFlag;
			$cloumn = $request->flagType;
			
			$likeData = array(
				'post_id' => $request->post_id,
				'like_by' => auth()->user()->id,
				$cloumn => $likeFlag
			);
			
			// $socialPostLikes = SocialPosts::select('num_of_post_like')->where('post_id', $request->post_id)->get();
			// echo $likeFlag;
			// die;
			$data = SocialPostLikeHub::where(['post_id' => $request->post_id, 'like_by' => auth()->user()->id])->get();
			
			if (count($data) > 0) {
				$update=array();
				if($cloumn=='like_flag'){
				  $update['like_flag']=1;
				  $update['love_flag']=$data[0]['love_flag'];
				  $update['sad_flag']=$data[0]['sad_flag'];
				  $update['angary_flag']=$data[0]['angary_flag'];
				}
				if($cloumn=='love_flag'){
				  $update['like_flag']=$data[0]['like_flag'];
				  $update['love_flag']=1;
				  $update['sad_flag']=$data[0]['sad_flag'];
				  $update['angary_flag']=$data[0]['angary_flag'];
				}
				if($cloumn=='sad_flag'){
				  $update['like_flag']=$data[0]['like_flag'];
				  $update['love_flag']=$data[0]['love_flag'];
				  $update['sad_flag']=1;
				  $update['angary_flag']=$data[0]['angary_flag'];
				}
				if($cloumn=='angary_flag'){
				  $update['like_flag']=$data[0]['like_flag'];
				  $update['love_flag']=$data[0]['love_flag'];
				  $update['sad_flag']=$data[0]['sad_flag'];
				  $update['angary_flag']=1;
				}
				
				$comment = SocialPostLikeHub::where(['post_id' => $request->post_id, 'like_by' => auth()->user()->id])->update($update);
				if ($likeFlag == 1) {
					$msg = "Thanks for like.";
				} else {
					$msg = "Unliked.";
				}
			} else {
				$comment = SocialPostLikeHub::create($likeData);
				$msg = "Thanks for like.";
				
				#insert notification table
				if(!empty($comment)){
					$followerId = SocialPosts::where('post_id', $request->post_id)->get();
					$follower_id='';
					$viewer_id =array();
					if(!empty($followerId) && ($followerId[0]->userid != auth()->user()->id) ){
						if(count($followerId)>0){
							$follower_id =$followerId[0]->userid;
							 
						}
						if(!empty(auth()->user()->image)){
							$img =auth()->user()->image;
						}else{
							$img='dist/images/user_icon.png';
						}
						$noticeData = array(
							'created_id' => auth()->user()->id,
							'created_name' => auth()->user()->name,
							'created_image' => $img,
							'notification_type' => "post_like",
							'post_id' => $request->post_id,
							'followers_id' => $follower_id,
							'status' => 1,
							'created_at' => date('Y-m-d H:i:s'),
							'updated_at' => date('Y-m-d H:i:s')
						);
						
						$notice = Notifications::create($noticeData);
						$noticeData['id']=$notice->id;
						//pusher notification send
						event(new PostLike($noticeData));
					}
					
				}
			}

			$redirect = route('user.dashboard');

			if($comment) {
				return response()->json(['status'=> 'success', 'redirect'=> $redirect, 'message' => $msg]);
			} else {
				return response()->json(['status'=> 'error', 'redirect'=> $redirect, 'message' => 'This request is not valid.', 'data' => '']);
			}
		} else {
			$redirect = route('user.dashboard');
			return response()->json(['status'=> 'error', 'redirect'=> $redirect, 'message' => 'This request is not valid.', 'data' => '']);
		}
	}
	
	public function get_post_like($post_id)
	{
		if($post_id!=0)
		{
			#Get all type reaction like on post
			$cPost_like = SocialPostLikeHub::select('users.name as likeBy','users.image','social_post_like_hub.like_flag','social_post_like_hub.love_flag','social_post_like_hub.sad_flag','social_post_like_hub.angary_flag')
						 ->join('users','social_post_like_hub.like_by','=','users.id')
						 ->where('social_post_like_hub.post_id',$post_id)
						 ->orderBy('social_post_like_hub.like_id')
						 ->get();
			
			#Get only thumb reaction on post
			$thumbTypeLike = SocialPostLikeHub::select('users.name as likeBy','users.image')
						 ->join('users','social_post_like_hub.like_by','=','users.id')
						 ->where('social_post_like_hub.post_id',$post_id)
						 ->where('social_post_like_hub.like_flag',1)
						 ->orderBy('social_post_like_hub.like_id')
						 ->get();
			
			#Get only sad reaction on post
			$sadTypeLike = SocialPostLikeHub::select('users.name as likeBy','users.image')
						 ->join('users','social_post_like_hub.like_by','=','users.id')
						 ->where('social_post_like_hub.post_id',$post_id)
						 ->where('social_post_like_hub.sad_flag',1)
						 ->orderBy('social_post_like_hub.like_id')
						 ->get();
			
			#Get only love reaction on post
			$loveTypeLike = SocialPostLikeHub::select('users.name as likeBy','users.image')
						 ->join('users','social_post_like_hub.like_by','=','users.id')
						 ->where('social_post_like_hub.post_id',$post_id)
						 ->where('social_post_like_hub.love_flag',1)
						 ->orderBy('social_post_like_hub.like_id')
						 ->get();
			
			#Get only angry reaction on post
			$angryTypeLike = SocialPostLikeHub::select('users.name as likeBy','users.image')
						 ->join('users','social_post_like_hub.like_by','=','users.id')
						 ->where('social_post_like_hub.post_id',$post_id)
						 ->where('social_post_like_hub.angary_flag',1)
						 ->orderBy('social_post_like_hub.like_id')
						 ->get();
			/****************************************************************/			 
			$allTypeLike = [];
			
			foreach($cPost_like as $val)
			{
				if($val['like_flag']==1)
				{
					$allTypeLike[]=array(
						"likeBy"=> $val['likeBy'],
						"image" => $val['image'],
						"flag" => '1'
					);
				}
				if($val['love_flag']==1)
				{
					$allTypeLike[]=array(
						"likeBy"=> $val['likeBy'],
						"image" => $val['image'],
						"flag" => '2'
					);
				}
				if($val['sad_flag']==1)
				{
					$allTypeLike[]=array(
						"likeBy"=> $val['likeBy'],
						"image" => $val['image'],
						"flag" => '3'
					);
				}
				if($val['angary_flag']==1)
				{
					$allTypeLike[]=array(
						"likeBy"=> $val['likeBy'],
						"image" => $val['image'],
						"flag" => '4'
					);
				}
			}
			echo json_encode(['status'=>'success', 'all'=>$allTypeLike, 'thumb'=>$thumbTypeLike, 'sad'=>$sadTypeLike, 'love'=>$loveTypeLike, 'angry'=>$angryTypeLike]); exit();
		}
		else
		{
			echo json_encode(['status'=>'error','message'=>'This request is not valid.']); exit();
		}
	}
	
	public function post_param_link($paramId,$flag)
	{
		$data['page'] = 'user_dashboard';
		$data['meta_title'] = 'Dashboard';
		$data['meta_description'] = 'ReplenishMD is a new age integrative, Functional, Anti-Aging, Regenerative Medicine, MedSpa, Mobile Lab & Concierge Medical Clinic at your service.';
		
		if($flag==0){
			$data['post'] = DB::select( DB::raw("SELECT `social_posts`.*,`users`.name as `postBy`,`users`.image as `postImage` from `social_posts`
						INNER JOIN `users` ON `social_posts`.`userid`=`users`.`id` where `social_posts`.`param_id`=".$paramId.""));
		}else{
			$followerId = SocialUserFollowConnection::where('user_id', auth()->user()->id)->get();
			$followerId = (count($followerId)>0) ? $followerId[0]->follower_id : 0;
			$st_offset = $_REQUEST['offset1'];
			$en_offset = $_REQUEST['offset2'];
			$nextSt_offset = ($st_offset)+($en_offset);
			$data['post'] = DB::select( DB::raw("SELECT `social_posts`.*,`users`.name as `postBy`,`users`.image as `postImage` from `social_posts`
						INNER JOIN `users` ON `social_posts`.`userid`=`users`.`id` where `userid`=".auth()->user()->id." OR (`userid` IN (".$followerId.")) ORDER BY `social_posts`.post_id DESC LIMIT ".$st_offset.",".$en_offset." "));
						
			$data['load_post'] = DB::select( DB::raw("SELECT `social_posts`.*,`users`.name as `postBy`,`users`.image as `postImage` from `social_posts`
						INNER JOIN `users` ON `social_posts`.`userid`=`users`.`id` where `userid`=".auth()->user()->id." OR (`userid` IN (".$followerId.")) ORDER BY `social_posts`.post_id DESC LIMIT ".$nextSt_offset.",".$en_offset." "));
		}
		#Post Media detail
		$posts_media = SocialPostsMedia::select('post_id', 'media_name')->get();
		$data['posts_media'] = [];
		if (!empty($posts_media)) {
			foreach($posts_media as $key => $value) {
				$data['posts_media'][$value->post_id][] = $value->media_name;
			}
 		}
		
		#Post Like detail
		$post_likes = SocialPostLikeHub::where('like_by', auth()->user()->id)->get();
		$data['post_likes'] = [];
		$data['post_likes_flagData'] = [];
		if (!empty($post_likes)) {
			foreach($post_likes as $key => $value) {
				$data['post_likes'][$value->post_id] = $value->like_flag;
				$data['post_likes_flagData'][$value->post_id][] = array(
					"like_flag" => $value->like_flag,
					"love_flag"    => $value->love_flag,
					"sad_flag"    => $value->sad_flag,
					"angary_flag"    => $value->angary_flag
				);
				
			}
 		}
		
		#Post Comment detail
		$post_comment = SocialPostCommentHub::select('social_post_comment_hub.comment_id','social_post_comment_hub.post_id','social_post_comment_hub.comments','users.name as commentBy','users.image')
							 ->join('users','social_post_comment_hub.comment_by','=','users.id')
							 ->orderBy('social_post_comment_hub.comment_id')
							 ->get();
		
		$data['post_comment'] = [];
		if(!empty($post_comment))
		{
			foreach($post_comment as $key => $value)
			{
				$data['post_comment'][$value->post_id][] = array(
					"comments" => $value->comments,
					"commentBy"    => $value->commentBy,
					"image"    => $value->image,
					"comment_id"    => $value->comment_id
				);
			}
		}
		
		#Name of person like on post
		$likePostUserData = DB::select( DB::raw("SELECT DISTINCT `post_id`, 
				REPLACE(substring_index(GROUP_CONCAT(users.name), ',', 3), '".auth()->user()->name."', 'You') as `Twoperson` 
				FROM social_post_like_hub inner join users ON social_post_like_hub.`like_by`=users.id 
				WHERE `like_by` != ".auth()->user()->id." OR `like_by` = ".auth()->user()->id." 
				GROUP BY `post_id` ORDER BY post_id DESC"));
		
		$data['likePostUserData'] = [];
		if (!empty($likePostUserData)) {
			foreach($likePostUserData as $key => $value) {
				$data['likePostUserData'][$value->post_id][] = $value->Twoperson;
			}
 		}
			
		if($flag==0)
		{
			return view('front.user.post_detail', $data);
		}
		else
		{
			$output = view('front.user.load_more_post',$data)->render();
			return response()->json(['status'=> 'success', 'message' => 'Success', 'data' => $output,'nxtOffset'=>$nextSt_offset]);
		}
	}
	
	
	public function create_story() {
		return view('front.user.create_story');
	}

	public function text_story() {
		$data=array();
		$sbc_one = url('uploads/users/story_bg/sbc_one.png');
		$sbc_two =url('uploads/users/story_bg/sbc_two.png');
		$sbc_three =url('uploads/users/story_bg/sbc_three.png');
		$sbc_four =url('uploads/users/story_bg/sbc_four.png');
		$array = array($sbc_one, $sbc_three, $sbc_two, $sbc_four);
		$data['story_bg']= $array[rand(0, count($array) - 1)];
		return view('front.user.text_story', $data);
	}

	public function photo_story() {
		return view('front.user.photo_story');
	}

	public function share_to_story(Request $request) {
		
		if($request->story_type==1)           //Text Story Content
		{
			if (!empty($request->imgBase64)) {
				$file = $request->imgBase64;
				$destinationPath = 'uploads/social_post/';
				$file = str_replace('data:image/png;base64,', '', $file);
				$file = str_replace(' ', '+', $file);
				$data = base64_decode($file);	
				$file = $destinationPath . time().'-'.rand(11111111,99999999) . '.png';
				$success = file_put_contents($file, $data);
				
				if($success){
					$image = $file;
					$status = 'success';
					$message = 'Story added successfully.';
					
					$inputData = array(
						'user_id' => auth()->user()->id,
						'story_bg'=> $file,
						'story_text'=> '',
						'story_type' => 1
					);
					$query = SocialStory::create($inputData);
				}
				else
				{
					$image = $file;
					$status = 'error';
					$message = 'Soory please try again later.';
				}
				return response()->json(['status' => $status, 'message' => 'Feedback Submitted Successfully']);
			}
			else
			{
				return response()->json(['status' => 'error', 'message' => $message]);	
			}	
		}
		else                                  //Media Story Content
		{
			if (!empty($request->imgBase64)) {
				$file = $request->imgBase64;
				$destinationPath = 'uploads/social_post/';
				$file = str_replace('data:image/png;base64,', '', $file);
				$file = str_replace(' ', '+', $file);
				$data = base64_decode($file);	
				$file = $destinationPath . time().'-'.rand(11111111,99999999) . '.png';
				$success = file_put_contents($file, $data);
				
				if($success){
					$image = $file;
					$status = 'success';
					$message = 'Story added successfully.';
					
					$inputData = array(
						'user_id' => auth()->user()->id,
						'story_bg'=> $file,
						'story_text'=> '',
						'story_type' => 2
					);
					$query = SocialStory::create($inputData);
				}
				else
				{
					$image = $file;
					$status = 'error';
					$message = 'Sorry, please try again later.';
				}
				return response()->json(['status' => $status, 'message' => 'Feedback Submitted Successfully']);
			}
			else
			{
				return response()->json(['status' => 'error', 'message' => $message]);	
			}			
		}
		
	}
	
	public function get_social_story()
	{
		$followerId = SocialUserFollowConnection::where('user_id', auth()->user()->id)->get();
		$followerId = (count($followerId)>0) ? $followerId[0]->follower_id : 0;
		$data['story'] = DB::select( DB::raw("SELECT `social_stories`.*,`users`.name as `storyBy`,`users`.image as `profilePic` from `social_stories`
						INNER JOIN `users` ON `social_stories`.`user_id`=`users`.`id` where `user_id`=".auth()->user()->id." OR (`user_id` IN (".$followerId.")) ORDER BY `social_stories`.id DESC"));
	}
		
		
	public function notification_view(Request $request){
		
		if ($request->ajax()) {
			
			#Stories detail
			$viewData = Notifications::where('id', $request->notification_id)->get();
			
			if(!empty($viewData)){
				Notifications::where(['id' => $request->notification_id, 'followers_id'=>auth()->user()->id ])->update(['status'=>0]);
				
				return response()->json(['status'=> 'success', 'data'=>'']);
			}else{
				return response()->json(['status'=> 'error', 'redirect'=> '', 'message' => '']);
			}
		}
		
	}
	
	public function redirectToFacebookLogin(Socialite $socialite)
    {
        
        return $socialite::driver('facebook')->scopes([
            'email','pages_manage_metadata','pages_show_list','pages_manage_posts'])->redirect();
            
    }
 
    /**
     * Obtain the user information from Facebook.
     *
     * @return void
     */
    public function facebookCallback()
    {
        $auth_user = Socialite::driver('facebook')->user();
		//echo "<pre>"; print_r($auth_user); die();
		$check_auth = FacebookAuth::where('user_id', auth()->user()->id)->get();
		if(!empty($check_auth) && count($check_auth)>0 ){
		    $update_data= array(
		                    'updated_at'=>date('Y-m-d H:i:s')
		                    );
		    
		    if(!empty($auth_user->token)){
		        $update_data['token']=$auth_user->token;
		        $token = $auth_user->token;
		        // Generated by curl-to-PHP: http://incarnate.github.io/curl-to-php/
		        $app_id = env('FACEBOOK_APP_ID');
		        $app_secret = env('FACEBOOK_APP_SECRET');
                
                $ch = curl_init();
        
                curl_setopt($ch, CURLOPT_URL, 'https://graph.facebook.com/oauth/access_token?grant_type=fb_exchange_token&client_id='.$app_id.'&client_secret='.$app_secret.'&fb_exchange_token='.$token);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                
                $result = curl_exec($ch);
                if(!empty($result)){
                    
                    $result = json_decode($result);
                    $update_data['token']=$result->access_token;
                    $update_data['refreshToken']=$result->access_token;
                    $update_data['expiresIn']= strtotime("59 day", time());
                    
                    #get facebook page id
                    
                    $chh = curl_init();
        
                    curl_setopt($chh, CURLOPT_URL, 'https://graph.facebook.com/'.$auth_user->id.'/accounts?access_token='.$result->access_token);
                    curl_setopt($chh, CURLOPT_RETURNTRANSFER, 1);
                    
                    $result_page = curl_exec($chh);
                   
                   if(!empty($result_page)){
                        $result_page = json_decode($result_page);
                        $data=$result_page->data;
                        $update_data['page_id'] = $data[0]->id;
                    }
                    if (curl_errno($chh)) {
                        echo 'Error:' . curl_error($chh);
                    }
                    curl_close($chh);
                    
                }
                
                if (curl_errno($ch)) {
                    echo 'Error:' . curl_error($ch);
                }
                
                curl_close($ch);
                
		    }
		    
		    if(!empty($auth_user->refreshToken)){
		        $update_data['refreshToken']=$auth_user->refreshToken;
		    }
		    
		    if(!empty($auth_user->id)){
		        $update_data['fb_id']=$auth_user->id;
		    }
		    
		    $update = FacebookAuth::where('user_id', auth()->user()->id)->update($update_data);
		}else{
		    
		    $update_data= array(
		                    'user_id'   =>auth()->user()->id,
		                    'created_at'=>date('Y-m-d H:i:s'),
		                    'updated_at'=>date('Y-m-d H:i:s'),
		                    );
		    
		    if(!empty($auth_user->token)){
		        $update_data['token']=$auth_user->token;
		        $token = $auth_user->token;
		        // Generated by curl-to-PHP: http://incarnate.github.io/curl-to-php/
		        $app_id = env('FACEBOOK_APP_ID');
		        $app_secret = env('FACEBOOK_APP_SECRET');
                
                $ch = curl_init();
        
                curl_setopt($ch, CURLOPT_URL, 'https://graph.facebook.com/oauth/access_token?grant_type=fb_exchange_token&client_id='.$app_id.'&client_secret='.$app_secret.'&fb_exchange_token='.$token);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                
                $result = curl_exec($ch);
                if(!empty($result)){
                    
                    $result = json_decode($result);
                    $update_data['token']=$result->access_token;
                    $update_data['refreshToken']=$result->access_token;
                    $update_data['expiresIn']= strtotime("59 day", time());
                    
                    #get facebook page id
                    
                    $chh = curl_init();
        
                    curl_setopt($chh, CURLOPT_URL, 'https://graph.facebook.com/'.$auth_user->id.'/accounts?access_token='.$result->access_token);
                    curl_setopt($chh, CURLOPT_RETURNTRANSFER, 1);
                    
                    $result_page = curl_exec($chh);
                   
                   if(!empty($result_page)){
                        $result_page = json_decode($result_page);
                        $data=$result_page->data;
                        $update_data['page_id'] = $data[0]->id;
                    }
                    if (curl_errno($chh)) {
                        echo 'Error:' . curl_error($chh);
                    }
                    curl_close($chh);
                    
                }
                
                if (curl_errno($ch)) {
                    echo 'Error:' . curl_error($ch);
                }
                
                curl_close($ch);
		        
		    }
		    
		    if(!empty($auth_user->refreshToken)){
		        $update_data['refreshToken']=$auth_user->refreshToken;
		    }
		    
		    if(!empty($auth_user->id)){
		        $update_data['fb_id']=$auth_user->id;
		    }
		    
		    $result = FacebookAuth::create($update_data);
		}
       
        return redirect()->to('/user/dashboard'); // Redirect to a secure page
    }
    
    public function publishToPage(){
        
        $result=array();
        $app_id = '220406633283382';
        $app_secret = 'e1400e2f2a133b0f26b8b81f7a20ab25';
        $token = 'EAADIdWsVPzYBAFJPvRxVwsIZADB7W0GcE2VmSwKfLjwa02ZAXZBoJogkNdYGqTLoh3oSZB09xZCg2Y0qMdwyJZCrqzvYcZCk8Y2R7p0zJGdYoZCJRV3DUbAac9XxDZCyFTQ67LEPZB4IT7jMARZALkoi0yGdIf0Enpi6rMgyhuUxJCeuVdZBqwnJFWq4';
        
        // Generated by curl-to-PHP: http://incarnate.github.io/curl-to-php/
        $ch = curl_init();
        
        curl_setopt($ch, CURLOPT_URL, 'https://graph.facebook.com/103824395327093/accounts?access_token=EAADIdWsVPzYBAFJPvRxVwsIZADB7W0GcE2VmSwKfLjwa02ZAXZBoJogkNdYGqTLoh3oSZB09xZCg2Y0qMdwyJZCrqzvYcZCk8Y2R7p0zJGdYoZCJRV3DUbAac9XxDZCyFTQ67LEPZB4IT7jMARZALkoi0yGdIf0Enpi6rMgyhuUxJCeuVdZBqwnJFWq4');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        
        $result = curl_exec($ch);
       // $result = josn_decode($result);
       if(!empty($result)){
            $result = json_decode($result);
            $data=$result->data;
            $data[0]->id;
        }
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close($ch);
        
        
    }
	
	function unread_message(){
		//check unread message
		$userLogin = auth()->user()->email;
		$userPassword =env('QUICKBLOX_PASSWORD');

		$body = [
			'application_id' =>env('QUICKBLOX_APPLICATION_ID'),
			'auth_key' =>env('QUICKBLOX_AUTH_KEY'),
			'nonce' => time(),
			'timestamp' => time(),
			'user' => ['login' => $userLogin, 'password' => $userPassword]
		];
		$built_query = urldecode(http_build_query($body));
		$signature = hash_hmac('sha1', $built_query ,env('QUICKBLOX_AUTH_SECRET'));
		$body['signature'] = $signature;
		$post_body = http_build_query($body);
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, 'https://api.quickblox.com/session.json');
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $post_body);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		$response = curl_exec($curl);
		$response = json_decode($response, true);
		if(!empty($response)){
			$token=$response['session']['token'];
			$userId=$response['session']['user_id'];
			if(!empty($token)){
				$curl = curl_init();

				curl_setopt_array($curl, array(
				  CURLOPT_URL => 'https://api.quickblox.com/chat/Dialog.json?type=3&sort_desc=last_message_date_sent',
				  CURLOPT_RETURNTRANSFER => true,
				  CURLOPT_ENCODING => '',
				  CURLOPT_MAXREDIRS => 10,
				  CURLOPT_TIMEOUT => 0,
				  CURLOPT_FOLLOWLOCATION => true,
				  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				  CURLOPT_CUSTOMREQUEST => 'GET',
				  CURLOPT_HTTPHEADER => array(
					'QB-Token:'.$token
				  ),
				));

				$response_dialog = curl_exec($curl);
				if(!empty($response_dialog)){
					$response_dialog = json_decode($response_dialog, true);
					$total_entries=$response_dialog['total_entries'];
					$items=$response_dialog['items'];
					
					foreach ($items as $val) {
						
						if($val['unread_messages_count']>0){
						    $chat_id =$val['last_message_user_id'];
						    $senderData = User::where('chat_id',$chat_id)->first();
							if(!empty($senderData) && $senderData->count()>0){
							    if(!empty($senderData->image)){
									$img =$senderData->image;
								}else{
									$img='dist/images/user_icon.png';
								}
								$noticeData = array(
									'created_id' => $senderData->id,
									'created_name' => $senderData->name,
									'created_image' => $img,
									'notification_type' => "unread_messages",
									'post_id' => $val['_id'],
									'followers_id' =>auth()->user()->id,
									'notice_type' =>$val['unread_messages_count'],
									'status' => 1,
									'created_at' => date('Y-m-d H:i:s'),
									'updated_at' => date('Y-m-d H:i:s')
								);
								$sender_notice = Notifications::where('followers_id',auth()->user()->id)->where('notification_type','unread_messages')->where('post_id',$val['_id'])->where('created_id',$senderData->id)->first();
								if(!empty($sender_notice) && $sender_notice->count()>0){
									Notifications::where('id',$sender_notice->id)->update($noticeData);
								}else{
									$notice = Notifications::create($noticeData);
								}
							}
						}
					}
				}
			}
		}
		
	}
	

}


