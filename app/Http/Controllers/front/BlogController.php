<?php

namespace App\Http\Controllers\front;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Article;
use App\BlogCategory;
use App\User;
use App\CategoryTopic;
use App\PostType;
use DataTables;
use DB;

class BlogController extends Controller {

    public function __construct(){
        $this->middleware('common_auth');
    }

    public function index(Request $request){
		
		if ($request->ajax()) {

			$data = Article::select('*');

			if($request->filter_category != ''){
				$data = $data->where('blog_category_id',$request->filter_category);
			}

			if($request->visibility_status != ''){
				$data = $data->where('post_visibility',$request->visibility_status);
			}

			if($request->publish_status != ''){
				$data = $data->where('post_publish',$request->publish_status);
			}

			if($request->comment_status != ''){
				$data = $data->where('post_comment',$request->comment_status);
			}
			
			$data = $data->where('articles.user_id', auth()->user()->id)->latest()->get();

            return Datatables::of($data)
				->addIndexColumn()
				->addColumn('action', function($row){
					$btn = '';
					$type = auth()->user()->user_role == 4 ? 'user' : 'provider';
					$view_url = env('APP_URL').'/'.$type.'/blogs/view/'.$row->id;
					$edit_url = env('APP_URL').'/'.$type.'/blogs/edit/'.$row->id;
					$btn = '<div class="action_btn text-center"><a href="'.$view_url.'" data-id="'.$row->id.'" title="View" class="btn btn-sm btn-primary d-inline">View</a>';
					$btn = $btn.'<a href="'.$edit_url.'"   data-id="'.$row->id.'" title="Edit" class="btn btn-sm btn-info d-inline ml-2">Edit</a>';
					$btn = $btn.' <a href="javascript:void(0)" onclick="deleteRow('.$row->id.')" data-id="'.$row->id.'" title="Delete" class="btn btn-sm btn-danger d-inline ml-2">Delete</a></div>';
					return $btn;
				})
				
				->addColumn('post_visibility', function($row){
					if($row->post_visibility == 1){
						$post_visibility = '<input type="checkbox" name="status" class="input input--switch border"  title="Status" checked onchange="changeStatus('.$row->id.',1,1)">';
					}else{
						$post_visibility = '<input type="checkbox" name="status" class="input input--switch border"  title="Status" onchange="changeStatus('.$row->id.',0,1)">';
					}
					return $post_visibility;
				})
				->addColumn('post_publish', function($row){
					if($row->post_publish == 1 || ( $row->post_publish == 2 && strtotime($row->scheduled_at) <= time())){
						$post_publish = '<input type="checkbox" name="status" class="input input--switch border"  title="Status" checked onchange="changeStatus('.$row->id.',1,2)">';
					}else{
						$post_publish = '<input type="checkbox" name="status" class="input input--switch border"  title="Status" onchange="changeStatus('.$row->id.',0,2)">';
					}
					return $post_publish;
				})
				->addColumn('post_comment', function($row){
					if($row->post_comment == 1){
						$post_comment = '<input type="checkbox" name="status" class="input input--switch border"  title="Status" checked onchange="changeStatus('.$row->id.',1,3)">';
					}else{
						$post_comment = '<input type="checkbox" name="status" class="input input--switch border"  title="Status" onchange="changeStatus('.$row->id.',0,3)">';
					}
					return $post_comment;
				})
				->addColumn('title', function($row){
					$res = substr($row->title, 0, 100);
					return $res.'...';
				})
				->addColumn('blog_category_id', function($row){
					$category = \DB::table('blog_categories')->where('id', $row->blog_category_id)->pluck('name')->first();
					$data = '<h4 style="font-size: 14px;" class="mt-1">'.$category.'</h4>';
					return $data;
				})

				
				->rawColumns(['title','blog_category_id','post_visibility','post_publish','post_comment','action'])
				->make(true);
        }

		$data['page'] = 'blogs';
		$data['categories'] = BlogCategory::where('parent_id', 0)->where('status', 1)->get();
        return view('front.blog.index', $data);		
    }

	public function add(){
		$data['blog'] = array();
		$data['page'] = 'blogs';
		$data['categories'] = \DB::table('blog_categories')->where('parent_id', 0)->where('status', 1)->get();
		$data['topics'] = array();
		return view('front.blog.add', $data);
	}

    public function store(Request $request){
		
		//featured image
		$image = '';
		//header image
		$header_image = '';	
		
		#check category come from list or manual
		$cat_check_flag = $request->input('manul_cat_flag');
		
		//for existing images
		if ($request->get('page') == 'edit') {
			if (!empty($request->input('old_image'))) {
				$image = $request->input('old_image');
			}
			
			if (!empty($request->input('old_header_image'))) {
				$header_image = $request->input('old_header_image');
			}			
		}
		
		//for new images
		if (!empty($request->input('crop_cover_image'))) {
			
			$image_array_1 = explode(";", $request->input('crop_cover_image'));
			$image_array_2 = explode(",", $image_array_1[1]);
			$file = base64_decode($image_array_2[1]);
			$name = 'IMG-'.uniqid().'-'.time().'.png';
			$destinationPath = 'uploads/blogs/';
			$image = 'uploads/blogs'.'/'.$name;

			file_put_contents($image, $file);
			
		}else{
			if (!empty($request->file('image'))) {
				$file = $request->file('image');
				$name = 'IMG-'.uniqid().'-'.time().'.'. $file->getClientOriginalExtension();
				$destinationPath = 'uploads/blogs/';
				$file->move($destinationPath, $name);
				$image = 'uploads/blogs'.'/'.$name;
			}			
		}


		if (!empty($request->file('header_image'))) {
            $file = $request->file('header_image');
			$name = 'IMG-'.uniqid().'-'.time().'.'. $file->getClientOriginalExtension();
			$destinationPath = 'uploads/blogs/';
			$file->move($destinationPath, $name);
			$header_image = 'uploads/blogs'.'/'.$name;
        }
		
		if($request->topics && count($request->topics) > 0){
			$topics = implode(',',$request->topics);
		}else{
			$topics = '';
		}
		
		if ($request->get('page') == 'add') {
			
			#Add category first in category master and then add topic in topic master if come from manual
				if($cat_check_flag==1)
				{
					$custom_cat = $request->custom_category;
					$custom_topic = $request->custom_topic;
					$catarray =  [
						'name' => trim($custom_cat),
						'category_img' => 'uploads/category/1636640110-92384610.png',
						'created_at' => date('Y-m-d H:i:s'),
						'updated_at' => date('Y-m-d H:i:s'),
					];
					$catId = BlogCategory::create($catarray);

					$topicarray =  [
						'category_id' => $catId->id,
						'topic' => trim($custom_topic),
						'created_at' => date('Y-m-d H:i:s'),
						'updated_at' => date('Y-m-d H:i:s'),
					];
					$topicId = CategoryTopic::create($topicarray);
				}
			##========== End Here============================##

			$blog = new Article;
			$blog->title = $request->name;
			$blog->user_id = auth()->user()->id;
			$blog->blog_category_id = ($cat_check_flag==0) ? ($request->category != '' ? $request->category : null) : $catId->id;
			$blog->category_topic_ids = ($cat_check_flag==0) ? $topics : $topicId->id;
			$blog->blog_content = $request->description;
			$blog->post_tags = $request->tags;
			$blog->image = $image;
			$blog->header_image = $header_image;
			$blog->slug = $this->getSlug($request->slug,NULL);
			$blog->paramlink = env('APP_URL').'/blogs/post/'.$request->slug;
			$blog->post_visibility = ($request->visibility=="public") ? 1 : 0;

			
			
			if($request->publish == 'immediate'){
				$post_publish = 1;
				$scheduled_at = NULL;
			}
			elseif($request->publish == 'draft'){
				$post_publish = 0;
				$scheduled_at = NULL;
			}
			else{
				//If Schedule
				$post_publish = 2;
				$scheduled_at = date('Y-m-d H:i:s',strtotime(str_replace('/','-',$request->scheduled_at)));        //DD/MM/YYYY to Y-M-D
			}
			
			$blog->post_publish = $post_publish;
			$blog->scheduled_at = $scheduled_at;
			
			$blog->post_comment = (isset($_POST['alw_comment'])) ? 1 : 0;
			$blog->lb_content = $request->description;
			
			if($blog->save()) {
				return response()->json(['status'=>'success', 'message'=>'Saved successfully.']);
			}
		} 
		elseif ($request->get('page') == 'edit') {

			$blog = Article::find($request->id);
			$blog->title = $request->name;
			//$blog->user_id = auth()->user()->id;
			$blog->blog_category_id = $request->category != '' ? $request->category : null;
			$blog->category_topic_ids = $topics;
			$blog->blog_content = $request->description;
			$blog->post_tags = $request->tags;
			$blog->image = $image;
			$blog->header_image = $header_image;
			$blog->slug = $this->getSlug($request->slug,$blog->id);
			$blog->paramlink = env('APP_URL').'/blogs/post/'.$request->slug;
			$blog->post_visibility = ($request->visibility=="public") ? 1 : 0;

			if($request->publish == 'immediate'){
				$post_publish = 1;
				$scheduled_at = NULL;
			}elseif($request->publish == 'draft'){
				$post_publish = 0;
				$scheduled_at = NULL;
			}else{
				//If Schedule
				$post_publish = 2;
				$scheduled_at = date('Y-m-d H:i:s',strtotime(str_replace('/','-',$request->scheduled_at))); // DD/MM/YYYY to Y-M-D
			}
			
			$blog->post_publish = $post_publish;
			$blog->scheduled_at = $scheduled_at;
			
			$blog->post_comment = (isset($_POST['alw_comment'])) ? 1 : 0;
			$blog->lb_content = $request->description;
			
			if($blog->save()) {
				return response()->json(['status'=>'success', 'message'=>'Saved successfully.']);
			}
		}
    }


	function getSlug($slug, $id){
		
		if($id != NULL){
			$check = Article::where('id','!=',$id)->where('slug','=',$slug)->get();		
		}else{
			$check = Article::where('slug','=',$slug)->get();
		}		
		
		if($check && $check->count() > 0){
			//slug is already exists
			$slug = $slug.'-'.time().rand(111,999);
		}
		
		return $slug;
	}
	
    public function edit($id){
		$data['topics'] = array();
		$type = auth()->user()->user_role == 4 ? 'user' : 'provider';
        $blog = Article::find($id);
		if($blog && $blog->count() > 0){
			$data['page'] = 'blogs';
			$data['categories'] = \DB::table('blog_categories')->where('parent_id', 0)->where('status', 1)->get();
			$data['topics'] = \DB::table('category_topic')->where('category_id', $blog->blog_category_id)->where('status', 1)->get();
			$data['blog'] = $blog;
			return view('front.blog.add', $data);
		} else {
			return redirect()->route($type.'.blogs');
		}
    }

    public function destroy($id){
        Article::find($id)->delete();
        return response()->json(['status'=>'success', 'message'=>'Deleted successfully.']);
	}

	public function get_topics(Request $request){
		$req = $request->all();
		$output = '';
		$category_id = $req['category_id'];

		if($category_id != ''){
			$topics = \DB::table('category_topic')->where('category_id',$category_id)->get();

			if($topics && $topics->count() > 0){
				foreach($topics as $topic){
					$output .= '<option value="'.$topic->id.'">'.$topic->topic.'</option>';
				}
			}
		}
		echo $output;
	}

	public function get_sub_sub_category(Request $request){
		$req = $request->all();
		$output = '<option value="">--Select--</option>';
		$category_id = $req['category_id'];

		if($category_id != ''){
			$sub_categories = \DB::table('blog_categories')->where('parent_id', $category_id)->get();

			if($sub_categories && $sub_categories->count() > 0){
				foreach($sub_categories as $sub_category){
					$output .= '<option value="'.$sub_category->id.'">'.$sub_category->name.'</option>';
				}
			}
		}
		echo $output;
    }

	public function change_status(Request $request) {
		$data = Article::where('id', $request->id)->first();
		$column = ($request->flag==1) ? "post_visibility" : (($request->flag==2) ? "post_publish" : "post_comment");
		
		if($data->$column==1)
		{
			$user = Article::where('id', $request->id)->update(array($column => 0));
		}
		else
		{
			$user = Article::where('id', $request->id)->update(array($column => 1));	
		}
		return response()->json(['status'=>'success', 'message'=>'Update successfully.']);

	}

	public function view($id) {
		/*$blog = Article::where('id', $id)->first();			
		$category_name = BlogCategory::where('id', $blog->id)->first();
		$author_name = User::where('id', $blog->user_id)->first();
		$data['blog'] = $blog;
		$data['images'] = json_decode($blog->images);
		$data['category'] = $category_name->name;
		$data['author_name'] = $author_name->name;
		$data['page'] = 'blogs';
		return view('front.blog.view', $data);*/

		$blog = Article::where('id', $id)->first();			
		$category_name = BlogCategory::where('id', $blog->blog_category_id)->first();
		$author_name = User::where('id', $blog->user_id)->first();
		$data['blog'] = $blog;
		$data['images'] = json_decode($blog->images);
		$data['category'] = $category_name->name;
		$data['author_name'] = $author_name->name;
		$data['page'] = 'blogs';
		return view('front.blog.view', $data);
	}
	

}
