<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProviderSetting extends Model
{
	protected $table = 'provider_setting';

	protected $guarded = [];	
}

