<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SocialStory extends Model {
    protected $table = 'social_stories';
    protected $fillable = array(
        'user_id',
        'story_bg',
        'story_text'
    );
}
