<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VisitFormFieldType extends Model
{
	protected $table = 'visit_form_field_types';

	protected $guarded = [];	
}

