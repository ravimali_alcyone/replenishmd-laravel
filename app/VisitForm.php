<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VisitForm extends Model
{
	protected $table = 'visit_forms';

	protected $guarded = [];	
}

