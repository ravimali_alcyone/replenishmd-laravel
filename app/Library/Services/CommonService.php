<?php
namespace App\Library\Services;
use Session;
use Mail;
use Illuminate\Support\Facades\DB;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use App\Library\Services\CommonService;
use App\User;
use App\VisitForm;
use App\VisitFormFieldType;
use App\VisitQuestion;
use App\QuestionOption;
use App\VisitAnswer;
use App\ProviderTimeSlot;


class CommonService
{

	public function removeComma($string){
		$b = str_replace( ',', '', $string );
		if(is_numeric($b)){ $string = $b; }		
		return $string;
	}
	
	public function getActualValueArray($stringArray){
		
		foreach($stringArray as $key => $value){
			
			$newValue = $value;
			
			if($value == 'manage_online_visits') { $newValue = 'Online Visits';	}
			if($value == 'visit_pool') { $newValue = 'Visit Pool';	}
			if($value == 'manage_orders') { $newValue = 'Orders';	}
			if($value == 'manage_products') { $newValue = 'Products';	}
			if($value == 'manage_doctors') { $newValue = 'Doctors';	}
			if($value == 'manage_patients') { $newValue = 'Patients';	}
			if($value == 'manage_contact_us') { $newValue = 'Contact Us';	}
			if($value == 'finances') { $newValue = 'Finances';	}
			if($value == 'manage_website_content') { $newValue = 'Website Content';	}
			if($value == 'manage_sub_admin') { $newValue = 'Sub Admins';	}
			if($value == 'manage_questions') { $newValue = 'Questions';	}
			if($value == 'manage_faq') { $newValue = 'FAQ';	}

			$stringArray[$key] = $newValue;
		}
		return $stringArray;
	}

	public function getTableListing($request, $table, $where, $whereRaw, $isLatest){

			$data = DB::table($table)->where($where);

 			if($whereRaw){
				$column = $whereRaw['column'];
				$value = $whereRaw['value'];
				$data = $data->whereRaw("find_in_set('".$value."', $column)");
			}
			
			if($isLatest == true){
				$data = $data->latest();
			}
			
			$data->get();

			return $data;
	}
	
	public function getProviderReportData($request, $table, $join_table, $where, $whereRaw, $whereDate1, $whereDate2, $isLatest){

			$data = DB::table($table)
						->select($table.'.*')
						->leftJoin($join_table, function ($join) use($table, $join_table) {
							$join->on($table.'.id', '=', $join_table.'.provider_id');
						})						
						->where($where);

 			if($whereRaw){
				$column = $whereRaw['column'];
				$value = $whereRaw['value'];
				$data = $data->whereRaw("find_in_set('".$value."', $column)");
			}
			
			if($whereDate1){
				$data = $data->whereDate($whereDate1['column'], $whereDate1['cond'], $whereDate1['value']);
			}
			
			if($whereDate2){
				$data = $data->whereDate($whereDate2['column'],  $whereDate2['cond'], $whereDate2['value']);
			}
			
			if($isLatest == true){
				$data = $data->latest();
			}
			
			$data = $data->groupBy($table.'.id')->get();
						
			return $data;
	}

	public function getPatientReportData($request, $table, $join_table, $where, $whereDate1, $whereDate2, $isLatest){

			$data = DB::table($table)
						->select($table.'.*')
						->leftJoin($join_table, function ($join) use($table, $join_table) {
							$join->on($table.'.id', '=', $join_table.'.patient_id');
						})						
						->where($where);
			
			if($whereDate1){
				$data = $data->whereDate($whereDate1['column'], $whereDate1['cond'], $whereDate1['value']);
			}
			
			if($whereDate2){
				$data = $data->whereDate($whereDate2['column'],  $whereDate2['cond'], $whereDate2['value']);
			}			
						
			if($isLatest == true){
				$data = $data->latest();
			}
			
			$data = $data->groupBy($table.'.id')->get();
								
			return $data;
	}	
	
	public function add_form($request){
		$req = $request->all();
		
		//Visit Form
		$name = $req['name'];
		$service_id = $req['service_id'];
		$short_info = $req['short_info'];
		$instructions = $req['instructions'];
		$id = $req['id'];	

		if ($request->status == 'on') {
			$status = 1;
		} else {
			$status = 0;
		}
		
		$data = array(
			'service_id' => $service_id,
			'name'=> $name,
			'short_info'=> $short_info,
			'instructions'=> $instructions,
			'status' => $status,
		);
				
		if($id != ''){
			VisitForm::where('id', $id)->update($data);
		}else{
			$id = VisitForm::create($data)->id;
		}
		
		return $id;
	}
	
	public function add_question($visit_form_id, $value, $parent_id, $order){
		
 		//Question
		if(isset($value['q_id'])){
			$q_id = $value['q_id'];
		}else{
			$q_id = '';
		}
				
		$data = array(
			'visit_form_id' =>$visit_form_id,
			'question_order' =>$order,
			'question_type' =>$value['type'],
			'question_text' =>$value['q_label'],
			'parent_id' =>$parent_id,
		);		
		
		if($q_id != ''){
			VisitQuestion::where('id', $q_id)->update($data);
		}else{
			$q_id = VisitQuestion::create($data)->id;
		}
		
		return $q_id; 		
	}

	public function add_option($visit_form_id, $q_id, $v){
					
 		//Option
		if(isset($v['op_id'])){
			$op_id = $v['op_id'];
		}else{
			$op_id = '';
		}														
		
		$data = array(
			'visit_form_id' =>$visit_form_id,
			'question_id' =>$q_id,
			'option_text' =>$v['o_label'],
			'is_conditional' =>$v['is_cond'],
		);
								
		if($op_id != ''){
			QuestionOption::where('id', $op_id)->update($data);
		}else{
			$op_id = QuestionOption::create($data)->id;
		} 
		
		return $op_id; 		
	}

	public function getMedicalQuestions($service_id){
		
		$result = array();
		$questionData = array();
		$user_id = session('online_visit_patient_id');
				
		$visit_form = VisitForm::where('service_id', $service_id)->where('status',1)->first();
		
		if($visit_form && $visit_form->count() > 0){
			$visit_questions = VisitQuestion::select('visit_questions.id AS question_id','visit_questions.question_order','visit_questions.question_type','visit_questions.question_text','question_options.id AS option_id','visit_questions.parent_id','question_options.option_text','question_options.is_conditional','child_questions.question_order AS next_question_id')
				->leftJoin('question_options', function ($join){
					$join->on('question_options.question_id', '=', 'visit_questions.id')->on('question_options.visit_form_id', '=', 'visit_questions.visit_form_id');
				})
				->leftJoin('visit_questions AS child_questions', function ($join){
					$join->on('question_options.id', '=', 'child_questions.parent_id')->on('question_options.visit_form_id', '=', 'child_questions.visit_form_id');
				})			
				->where('visit_questions.visit_form_id',$visit_form->id)
				->where('visit_questions.status',1)
				->where('visit_questions.question_type','!=','header_text')
				//->orderBy('visit_questions.question_order','ASC')
				//->orderBy(\DB::raw('-`visit_questions`.`question_order`'), 'DESC')
				->orderBy('visit_questions.question_order','ASC')
				->orderBy('visit_questions.id','ASC')
				->orderBy('question_options.id','ASC')
				->get();
			
			//echo '<pre>';print_r($visit_questions->toArray());die;
			if($visit_questions && $visit_questions->count() > 0){
				
				$i=0;
				
				foreach($visit_questions->toArray() as $row){
					
					$questionData[$row['question_order']]['question']['question_id'] = $row['question_id'];
					$questionData[$row['question_order']]['question']['question_order'] = $row['question_order'];
					$questionData[$row['question_order']]['question']['question_type'] = $row['question_type'];
					$questionData[$row['question_order']]['question']['question_text'] = $row['question_text'];
					
					if($row['parent_id'] != 0){
						$questionData[$row['question_order']]['question']['prev_id'] = $this->getPrevQuestion($row['parent_id']);
					}else{
						$questionData[$row['question_order']]['question']['prev_id'] = '';
					}					
					
					if($row['question_type'] == 'yes_no' || $row['question_type'] == 'single' || $row['question_type'] == 'multiple'){
						$questionData[$row['question_order']]['options'][$i]['option_id'] = $row['option_id'];
						$questionData[$row['question_order']]['options'][$i]['option_text'] = $row['option_text'];
						$questionData[$row['question_order']]['options'][$i]['is_conditional'] = $row['is_conditional'];						
						$questionData[$row['question_order']]['options'][$i]['next_question_id'] = $row['next_question_id'];	
						
						
												
					}else{
						$questionData[$row['question_order']]['options'] = array();
					}
			
					$i++;
				}
				//die;
				//echo '<pre>';print_R($questionData);die;
			}
		}
		
		$result['questionData'] = $questionData;
		$result['visit_form'] = $visit_form;
		return $result;
	}
	
	public function getPrevQuestion($op_id){
		//echo $q_id.'<br>';
		$q = QuestionOption::select('visit_questions.question_order')
				->join('visit_questions', function ($join){
					$join->on('question_options.question_id', '=', 'visit_questions.id')->on('question_options.visit_form_id', '=', 'visit_questions.visit_form_id');
				})		
			->where('question_options.id',$op_id)->first();
		//echo '<br><br>';
		//echo '<pre>';print_r($q);echo '</pre>';
		if($q && $q->count() > 0){
			$output = $q->question_order;			
		}else{
			$output = '';
		}

		return $output;
	}
	
	public function getProviderForOnlineVisit(){
		
		$providers = DB::select(DB::raw('SELECT users.id FROM users WHERE users.id NOT IN (SELECT online_visit_provider_assign.provider_id FROM online_visit_provider_assign WHERE online_visit_provider_assign.status = 1) AND users.status = 1 AND users.user_role = 3 ORDER BY users.id ASC LIMIT 1'));
				
		if($providers){
			
			$provider = '';
			
				foreach ($providers as $result)
				{
					$provider = $result->id;
				}			
			return $provider;
		}else{
			return false;
		}		
	}
	
	public function sendMail($req) {
		
		if(env('IS_LOCAL') == 'true'){
			return true;
		}
		
		$data = $req;
		$to_name = $req['name'];
		$to_email = $req['email'];
		$subject = $req['subject'];
		$email_template = $req['template'];

		Mail::send($email_template, $data, function($info) use ($to_name, $to_email, $subject) {
		$info->to($to_email, $to_name)->subject($subject);
		});
		
		if(count(Mail::failures()) > 0){			
			return false;
		}else{
			return true;
		}
		
	}

	public function check_weekend_days_timeslots($provider_id,$days){
		
		$result = ProviderTimeSlot::select('*')->where('provider_id',$provider_id);
		
		if($days){
			$result = $result->whereIn(DB::raw("(DATE_FORMAT(event_start,'%a'))"), $days);
		}
		
		$result = $result->delete();
		return true;
	}
	
	public function get_time_ago( $time )
	{   
	    
		$time_difference = time() - $time;
        if($time_difference < 86400){
				return date('d-M-Y',$time);	
		}
		//if( $time_difference < 1 ) { return 'less than 1 second ago'; }
		$condition = array( 12 * 30 * 24 * 60 * 60 =>  'year',
					30 * 24 * 60 * 60       =>  'month',
					24 * 60 * 60            =>  'day',
					60 * 60                 =>  'hour',
					60                      =>  'minute',
					1                       =>  'second'
		);

		foreach( $condition as $secs => $str )
		{
			$d = $time_difference / $secs;
            if( $d >= 1 )
			{
				$t = round( $d );
				return $t . ' ' . $str . ( $t > 1 ? 's' : '' ) . ' ago';
			}
			
		}
	}
	
	
	
}


?>