<?php
namespace App\Library\Services;
use Session;
use Mail;
use Illuminate\Support\Facades\DB;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use App\Library\Services\StripeService;
use App\User;
use Stripe;

class StripeService {
	
	public function createProduct($data){
		$stripe = new \Stripe\StripeClient(env('STRIPE_SECRET'));
		
		$input = [
		  'name' => $data['name'],
		  'active' => $data['active'],
		  'images' => $data['images'],
		];
		
		$result = $stripe->products->create($input);
		
		return $result;
	}

	public function updateProduct($id, $data){
		$stripe = new \Stripe\StripeClient(env('STRIPE_SECRET'));
		
		$result = $stripe->products->update($id,$data);
		
		return $result;
	}
	
	public function createStripePlan($data){
		$stripe = new \Stripe\StripeClient(env('STRIPE_SECRET'));

		$input = [
			'amount' => $data['price'] * 100,
			'currency' => 'usd',
			'interval' => $data['plan_interval'] == 'custom' ? $data['custom_plan_interval'] : $data['plan_interval'],
			'interval_count' => $data['interval_count'],
			'product' => $data['product_stripe_id'],
		];
		//echo '<pre>';print_r($input);die;
		$result = $stripe->plans->create($input);
		
		return $result;
	}	
	
	public function deleteStripePlan($id){
		$stripe = new \Stripe\StripeClient(env('STRIPE_SECRET'));
			
		$result = $stripe->plans->delete($id,[]);
		
		return $result;
	}
	
}


?>