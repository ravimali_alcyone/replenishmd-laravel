<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProviderLocation extends Model
{
	protected $table = 'provider_location';

	protected $guarded = [];	
}

