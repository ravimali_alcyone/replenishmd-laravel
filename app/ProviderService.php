<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProviderService extends Model
{
	protected $table = 'provider_services';

	protected $guarded = [];	
}

