<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MasterSetting extends Model {
	protected $table = 'master_setting';

	protected $guarded = [];
}

