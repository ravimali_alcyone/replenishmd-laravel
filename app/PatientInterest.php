<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PatientInterest extends Model
{
	protected $table = 'patient_interests';

	protected $guarded = [];	
}

