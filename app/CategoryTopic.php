<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoryTopic extends Model {
	protected $table = 'category_topic';

	protected $guarded = [];
}

