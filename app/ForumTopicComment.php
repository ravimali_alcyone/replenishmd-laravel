<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ForumTopicComment extends Model
{
	protected $table = 'forum_topic_comments';

	protected $guarded = [];	
}

