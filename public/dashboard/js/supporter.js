// GLOBAL VARIABLES
var userId = $("#user_id").val();
var baseUrl = window.location.origin;

$(document).ready(function() {
    $('#supporters, #youMayKnow').owlCarousel({
        loop: false,
        autoplay: false,
        // margin: 8,
        nav: false,
        responsiveClass: true,
        dots: false,
        items: 4,
        responsive: {
            0: {
                items: 2
            },
            400: {
                items: 2,
            },
            600: {
                items: 2,
            },
            800: {
                items: 3,
            },
            1300: {
                items: 4,
            }
        }
    });

    // Unfollow via following
    $(".following").on('click', function() {
        let name = $(this).closest('.text_wrapper').find('h6').text();
        let id = $(this).attr('data');
        $("#unFollowModal .username").text(name);
        localStorage.setItem('id', id);
    });
});

// Request for follow any person
function follow(freindID, element) {
    $.ajax({
        url: baseUrl + "/user/request_for_follow",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: 'POST',
        datatype: 'json',
        data: {
            user_id: freindID,
            request_id: userId
        },
        success: function(result) {
            if (result == true) {
                //successAlert('Your request has been sent!<br> Wait for acceptance.', 1000, 'top-right');
				callProgressBar();
                $(element).text('Pending...').removeClass('follow').addClass('following pending').prop('onclick', false);
            }
        }
    });
}

// Request accept
function requestAccept(supporterId) {
    $.ajax({
        url: baseUrl + "/user/request_accept",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: 'POST',
        datatype: 'json',
        data: {
            user_id: userId,
            follower_id: supporterId
        },
        success: function(result) {
            if (result == true) {
                //successAlert('Request accepted!', 1000, 'top-right');
				callProgressBar();
                setTimeout(() => {
                    location.reload();
                }, 1000);
            }
        }
    });
}

// Request ignore
function requestIgnore(supporterId) {
    $.ajax({
        url: baseUrl + "/user/request_ignore",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: 'POST',
        datatype: 'json',
        data: {
            user_id: userId,
            follower_id: supporterId
        },
        success: function(result) {
            if (result == true) {
                //successAlert('Request ignored!', 1000, 'top-right');
				callProgressBar();
                setTimeout(() => {
                    location.reload();
                }, 1000);
            }
        }
    });
}

// Unfollow
function unfollow(element) {
    let id = localStorage.getItem('id');
    $.ajax({
        url: baseUrl + "/user/request_for_unfollow",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: 'POST',
        datatype: 'json',
        data: {
            id: id
        },
        success: function(result) {
            if (result == true) {
                $('#unFollowModal').modal('toggle');
                $('.card_' + id).remove();
                //successAlert('Unfollow successfully.', 1000, 'top-right');
				callProgressBar();
                let supportersCount = parseInt($(".supporters_sec .title_box h6>span").text());
                $(".supporters_sec .title_box h6>span").text(supportersCount - 1);
                if (supportersCount - 1 == 0) {
                    $(".supporters_sec.top").hide();
                    $(".supporters_sec.bottom").addClass('mt-5');
                }
            }
        }
    });
}