// GLOBAL VARIABLES
const authUserId = $("#user_id").val();
const baseUrl = window.location.origin;
const userRole = $("#user_role").val();

$(document).ready(function() {  
	Pusher.logToConsole = true;
	var pusher_api_key = $("#pusher_api_key").val();
	var pusher_channel = $("#pusher_channel").val();
	var pusher_cluster = $("#pusher_cluster").val();
	var pusher = new Pusher(pusher_api_key, {
	cluster:pusher_cluster,
	forceTLS:true,
	encrypted: true
	});
	// Subscribe to the channel we specified in our Laravel Event
	var channel = pusher.subscribe(pusher_channel);
	// post like 
	channel.bind('AdminNotification', function(data) 
	{

		var avatar = Math.floor(Math.random() * (71 - 20 + 1)) + 20;
	
		var user_id = authUserId;
		var message = data.message;
		if(message !='')
		{
			var followers_id = message.followers_id;
			//pusher_notification_row();
			var notificationsCount = parseInt($('#pdNotify_n').find('.notice_count').attr('data-count'));
			notificationsCount +=1;
			
			if(notificationsCount==1){
				$('.n_t').addClass('badge-danger');
			}
			
			$('#pdNotify_n').find('.notice_count').attr('data-count',notificationsCount)
			$('#pdNotify_n').find('.notif-count').text(notificationsCount);
			$('.n_t').text(notificationsCount);
			
			var image =baseUrl+'/'+message.created_image;
			//var rtype="PostLike";
				
			var noticHtml = '<div class="cursor-pointer relative flex items-center ">'+
								'<div class="w-12 h-12 flex-none image-fit mr-1">'+
									'<img alt="'+message.created_name+'" class="rounded-full" src="'+image+'">'+
									'<div class="w-3 h-3 bg-theme-9 absolute right-0 bottom-0 rounded-full border-2 border-white"></div>'+
								'</div>'+
								'<div class="ml-2 overflow-hidden">'+
									'<div class="flex items-center">'+
										'<a href="javascript:;" class="font-medium truncate mr-5">'+message.created_name+'</a>'+
										'<div class="text-xs text-gray-500 ml-auto whitespace-no-wrap">just ago</div>'+
									'</div>'+
									'<div class="w-full truncate text-gray-600">'+message.message_note+'.</div>'+
								'</div>'+
							'</div>';
			$('#pdNotify_n').find('.notif_main_row').prepend(noticHtml);
		}

	});
		
});	


function pusher_notification_row(notice_id='', viewType='', element=''){
		if(notice_id !=''){
			$.ajax({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				data: {'notification_id':notice_id},
				url: baseUrl+"/user/notification_view",
				type: "POST",
				// dataType: 'json',
				success: function (response) {
					
					if(response['status'] == 'success'){
						//$("#passModal").modal('hide');
						callProgressBar();
						if(viewType=='supporter_accept' || viewType=='supporter_requests'){
							var notificationsCount = parseInt($('.notify-drop-title').find('.notice_count_sp').attr('data-count'));
							if(notificationsCount>0){
								notificationsCount -= 1;
								$('.notify-drop-title').find('.notice_count_sp').attr('data-count',notificationsCount);
								$('.notif-count-sp').text(notificationsCount);
								$('.notice_count_su.notice_text').text(notificationsCount);
							}else{
								$('.notify-drop-title').find('.notice_count_sp').attr('data-count',0);
								$('.notice_count_su').removeClass('badge-danger');
								$('.notif-count-sp').text(' ');
								$('.notice_count_su.notice_text').text('');
							}
							setTimeout(function(){
								$(element).removeClass('dot');
								$(element).removeClass('pusher_notification_row');
								$(element).prop("onclick", null).off("click");
								//redirect to user/supporters page
								window.location = baseUrl+'/user/supporters';
							}, 2000);
						}
						else{
							var notificationsCount = parseInt($(element).parent().parent().parent().find('.notify-drop-title').find('.notice_count').attr('data-count'));
							if(notificationsCount>0){
								notificationsCount -= 1;
								$(element).parent().parent().parent().find('.notify-drop-title').find('.notif-count').text(notificationsCount);
								$(element).parent().parent().parent().find('.notify-drop-title').find('.notice_count').attr('data-count',notificationsCount);
								$('.n_t').text(notificationsCount);
							}else{
								$('.n_t').removeClass('badge-danger');
								$('.n_t').text(' ');
								$('.notif-count').text('');
								$(element).parent().parent().parent().find('.notify-drop-title').find('.notice_count').attr('data-count',0);
							}	
							setTimeout(function(){
								$(element).removeClass('dot');
								$(element).removeClass('pusher_notification_row');
								$(element).prop("onclick", null).off("click");
							}, 2000);
						}

					}
				},
				error: function (data) {
				
				}
			});
		}
		
	}


	

