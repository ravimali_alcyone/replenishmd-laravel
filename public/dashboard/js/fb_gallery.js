// close photo preview block
function closePhotoPreview() {
    $('#photo_preview').hide();
    $('#photo_preview .pleft').html('empty');
    $('#photo_preview .pright').html('empty');
};


// display photo preview block


function getPhotoPreviewAjx(id,post_id) {

	$.ajax({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		},
		data: {"id":id, "post_id":post_id},
		url: baseUrl + "/get_post_images",
		type: "POST",
		success: function (response) {
			if(response['status'] == 'success'){
				$('#photo_preview .pleft').html(response['data1']);
				$('#comment_hub').html('');
				$('#comment_hub').html(response['data2']);
				$('#photo_preview').show();	
				$("#mediaId").val('');
				$("#postMediaId").val('');
				$("#mediaId").val(id);
				$("#postMediaId").val(post_id);
			}
			else{
				errorAlert(response['message'],2000,'top-right');
			}
		}
	});
}

// submit comment
function submitComment(id) {
    var sName = $('#name').val();
    var sText = $('#text').val();

    if (sName && sText) {
        $.post('index.php', { action: 'accept_comment', name: sName, text: sText, id: id }, 
            function(data){ 
                if (data != '1') {
                    $('#comments_list').fadeOut(1000, function () { 
                        $(this).html(data);
                        $(this).fadeIn(1000); 
                    }); 
                } else {
                    $('#comments_warning2').fadeIn(1000, function () { 
                        $(this).fadeOut(1000); 
                    }); 
                }
            }
        );
    } else {
        $('#comments_warning1').fadeIn(1000, function () { 
            $(this).fadeOut(1000); 
        }); 
    }
};

