	
// Success Snackbar Toast
function successAlert(message, time, position) {
    $.toast({
        heading: message,
        showHideTransition: 'slide',
		bgColor: "#28a745",
        position: position,
		icon: 'success',
		loader: true,
		loaderBg: '#04f73c',
		hideAfter: time,
    })
    setTimeout(function() {
        $(".jq-toast-single").css('display', 'none');
    }, time);
}


// Error Snackbar Toast
function errorAlert(message, time, position) {
    $.toast({
		heading: message,
		bgColor: "#dc3545",
		position: position,
		icon: 'error',
		loader: true,
		loaderBg: '#fb8581',
		hideAfter: time,		
    });
    setTimeout(function() {
        $(".jq-toast-single").css('display', 'none');
    }, time);
}

// Warning Snackbar Toast
function warningAlert(message, time, position) {
    $.toast({
        heading: message,
		icon: 'warning',
        showHideTransition: 'slide',
        bgColor: "#ff9c07",
        position: position,
		loader: true,
		loaderBg: '#ffc107',
		hideAfter: time,
    });
    setTimeout(function() {
        $(".jq-toast-single").css('display', 'none');
    }, time);
}

