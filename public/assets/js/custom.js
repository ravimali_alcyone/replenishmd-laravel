$(document).ready(function() {

    $('.exp_carousel').owlCarousel({
        loop: true,
        margin: 0,
        responsiveClass: true,
        nav: true,
        autoplay: false,
        responsive: {
            0: {
                items: 1,

            },
            400: {
                items: 1,
            },
            600: {
                items: 1,
            },
            800: {
                items: 3,
            },
            1500: {
                items: 3,
            }
        }
    });
    $('.talks').owlCarousel({
        loop: true,
        margin: 0,
        responsiveClass: true,
        nav: true,
        autoplay: false,
        responsive: {
            0: {
                items: 1,

            },
            400: {
                items: 1,
            },
            600: {
                items: 1,
            },
            800: {
                items: 3,
            },
            1500: {
                items: 4,
            }
        }
    });
    $('.expert_caro').owlCarousel({
        loop: true,
        margin: 15,
        responsiveClass: true,
        nav: false,
        autoplay: false,
        center: true,
        responsive: {
            0: {
                items: 1,

            },
            400: {
                items: 1,
            },
            600: {
                items: 1,
            },
            800: {
                items: 3,
            },
            1500: {
                items: 4,
            }
        }
    });
    $('.sponsers_caro').owlCarousel({
        loop: true,
        margin: 15,
        responsiveClass: true,
        nav: false,
        autoplay: false,
        center: true,
        responsive: {
            0: {
                items: 1,

            },
            400: {
                items: 1,
            },
            600: {
                items: 1,
            },
            812: {
                items: 3,
            },
            1500: {
                items: 5,
            }
        }
    });

    $('.clients').owlCarousel({
        center: false,
        items: 1,
        loop: true,
        margin: 0,
        responsiveClass: true,
        nav: false,
        autoplay: false,
        dots: true
    });

});



	
// Success Snackbar Toast
function successAlert(message, time, position) {
    $.toast({
        heading: message,
        showHideTransition: 'slide',
		bgColor: "#28a745",
        position: position,
		icon: 'success',
		loader: true,
		loaderBg: '#04f73c',
		hideAfter: time,
    })
    setTimeout(function() {
        $(".jq-toast-single").css('display', 'none');
    }, time);
}


// Error Snackbar Toast
function errorAlert(message, time, position) {
    $.toast({
		heading: message,
		bgColor: "#dc3545",
		position: position,
		icon: 'error',
		loader: true,
		loaderBg: '#fb8581',
		hideAfter: time,		
    });
    setTimeout(function() {
        $(".jq-toast-single").css('display', 'none');
    }, time);
}

// Warning Snackbar Toast
function warningAlert(message, time, position) {
    $.toast({
        heading: message,
		icon: 'warning',
        showHideTransition: 'slide',
        bgColor: "#ff9c07",
        position: position,
		loader: true,
		loaderBg: '#ffc107',
		hideAfter: time,
    });
    setTimeout(function() {
        $(".jq-toast-single").css('display', 'none');
    }, time);
}

function callProgressBar(){
		$("#pbar").css('display', 'block');
		var $progress = $('.progress');
		var $progressBar = $('.progress-bar');
		setTimeout(function() {
			$progressBar.css('width', '10%');
			setTimeout(function() {
				$progressBar.css('width', '30%');
					setTimeout(function() {
						$progressBar.css('width', '60%');
						setTimeout(function() {
							$progressBar.css('width', '100%');
							setTimeout(function() {
								$progressBar.css('width', '0');
								$("#pbar").css('display', 'none');
							}, 500); // WAIT 5 milliseconds
						}, 1500); // WAIT 2 seconds
				}, 1500); // WAIT 2 seconds
			}, 1000); // WAIT 1 seconds
		}, 1000); // WAIT 1 second
	}
	

function slidersowl(){
		$('#supporters, #supportingData, #youMayKnow').owlCarousel({
			loop: false,
			autoplay: false,
			// margin: 8,
			nav: false,
			responsiveClass: true,
			dots: false,
			items: 4,
			responsive: {
				0: {
					items: 2
				},
				400: {
					items: 2,
				},
				600: {
					items: 2,
				},
				800: {
					items: 3,
				},
				1300: {
					items: 4,
				}
			}
		});
	}

// Request for follow any person
function follow(freindID='', element='',request_back_id='') {
	//console.log('Here');
	if(freindID !=''){
		if(request_back_id==''){
			request_back_id=0;
		}
		$.ajax({
			url: baseUrl + "/user/request_for_follow",
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			type: 'POST',
			datatype: 'json',
			data: {
				user_id: freindID,
				request_id: authUserId,
				request_back_id: request_back_id,
			},
			success: function(result) {
				if (result == true) {
					callProgressBar();
					//successAlert('Your request has been sent!<br> Wait for acceptance.', 1000, 'top-right');
					$(element).text('Pending...').removeClass('follow').addClass('following pending').prop('onclick', false);
				}
			}
		});
	}
}

// Unfollow
function unfollow(element='') {
    let id = localStorage.getItem('id');
	if(id !=''){
		$.ajax({
			url: baseUrl + "/user/request_for_unfollow",
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			type: 'POST',
			datatype: 'json',
			data: {
				id: id
			},
			success: function(result) {
				if (result == true) {
					$('#unFollowModal').modal('toggle');
					$('.card_' + id).remove();
					callProgressBar();
					//successAlert('Unfollow successfully.', 1000, 'top-right');
					let supportersCount = parseInt($(".supporters_sec .title_box h6>span").text());
					$(".supporters_sec .title_box h6>span").text(supportersCount - 1);
					if (supportersCount - 1 == 0) {
						$(".supporters_sec.top").hide();
						$(".supporters_sec.bottom").addClass('mt-5');
					}
				}
			}
		});
	}
}

		// Request accept
	function requestAccept(supporterId='',element='') {
		//var supporterId =$(this).attr('data-id');
		if(supporterId !=''){
			var ahref='';
			if(userRole==3){
				ahref=baseUrl+"/provider/view/"+supporterId;
			}else{
				ahref=baseUrl+"/user/view/"+supporterId;
			}
			var name = $(element).parent().parent().find('.text_wrapper').find('h6').text();
			var img = $(element).parent().parent().find('.img_wrapper').html();
			var designation = $(element).parent().parent().find('.text_wrapper').find('.designation').text();
			var this_=$(element);
			$.ajax({
				url: baseUrl + "/user/request_accept",
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				type: 'POST',
				datatype: 'json',
				data: {
					user_id: authUserId,
					follower_id: supporterId
				},
				success: function(result) {
					if (result == true) {
						callProgressBar();
						//successAlert('Request accepted!', 1000, 'top-right');
						this_.parent().closest('.request_box').remove();
						if($('.request_box').length==0){
							$('#requests_wrapper').html('<p class="text-center text-secondary mb-0"><small>No requests found.</small></p>');
						}
						if($('.top').length==0){
							var accept_html='<section class="supporters_sec top">'+
							'<div class="title_box"><h6><span>1</span> Supporters</h6>'+
							'<a href="'+baseUrl+'/user/all_supporters">See All <span class="fa fa-angle-right"></span></a></div>'+
							'<div class="cards_wrapper owl-carousel" id="supporters">'+
							'<div class="card card_"'+supporterId+'>'+
							'<div class="img_wrapper">'+
							'<a href="'+ahref+'">'+img+'</a></div>'+
							'<div class="text_wrapper text-center">'+
							'<div class="dropdown sup_drop">'+
								'<button class="unsup_btn" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'+
									'<span class="fa fa-ellipsis-v"></span>'+
								'</button>'+
								'<div class="dropdown-menu  dropdown-menu-right" aria-labelledby="dropdownMenuButton">'+
									'<a class="dropdown-item" href="javascript:void(0)" data-toggle="modal" data-target="#unFollowModal" data="'+supporterId+'">Unsupport</a>'+
								'</div>'+
							'</div>'+
							'<a href="'+ahref+'"><h6>'+name+'</h6></a>'+
							/*'<p class="mb-0"><span>'+designation+'</span></p>'+*/
							'</div>'+
							'<div class="text-center sup_card_btns">'+
							'<a href="javascript:void(0)" class="following"><span class="fa fas fas fa-check"></span> Supporting</a>'+
							'</div>'+
							'</div>'+
							'</div>'+
							'</section>';
							
							$('.supportersAccept').append(accept_html);
						}else{
							if(parseInt($('#supporters').find('.card').length)<4){
								var accept_html= '<div class="card card_"'+supporterId+'>'+
								'<div class="img_wrapper"><a href="'+ahref+'">'+img+'</a></div>'+
								'<div class="text_wrapper text-center">'+
								'<div class="dropdown sup_drop">'+
									'<button class="unsup_btn" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'+
										'<span class="fa fa-ellipsis-v"></span>'+
									'</button>'+
									'<div class="dropdown-menu  dropdown-menu-right" aria-labelledby="dropdownMenuButton">'+
										'<a class="dropdown-item" href="javascript:void(0)" data-toggle="modal" data-target="#unFollowModal" data="'+supporterId+'">Unsupport</a>'+
									'</div>'+
								'</div>'+
								'<a href="'+ahref+'"><h6>'+name+'</h6></a>'+
								/*'<p class="mb-0"><span>'+designation+'</span></p>'+*/
								'</div>'+
								'<div class="text-center sup_card_btns">'+
									'<a href="javascript:void(0)" class="following"><span class="fa fas fas fa-check"></span> Supporting</a>'+
								'</div>'+
								'</div>';
								$('#supporters').append(accept_html);
							}
							var supporter_count = parseInt($('#supporters').find('.card').length);
							$('.top').find('.title_box').find('span').text(supporter_count);
						}
						slidersowl();
						
					}
				}
			});
		}		
	}		

// Request ignore
function requestIgnore(supporterId='',element='') {	
	//var supporterId =$(this).attr('data-id');
	if(supporterId !=''){
		$.ajax({
			url: baseUrl + "/user/request_ignore",
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			type: 'POST',
			datatype: 'json',
			data: {
				user_id: authUserId,
				follower_id: supporterId
			},
			success: function(result) {
				if (result == true) {
					callProgressBar();
					//successAlert('Request ignored!', 1000, 'top-right');
					$(element).parent().parent().remove();
					if($('.request_box').length==0){
						$('#requests_wrapper').html('<p class="text-center text-secondary mb-0"><small>No requests found.</small></p>');
					}
					
				}
			}
		});
	}
}

	
function redirect_post(id){
	document.getElementById(id).scrollIntoView({
	  behavior: 'smooth'
	});
}

$(document).ready(function() {
	slidersowl();
    // Unfollow via following
    $(".following").on('click', function() {
        let name = $(this).closest('.text_wrapper').find('h6').text();
        let id = $(this).attr('data');
        $("#unFollowModal .username").text(name);
        localStorage.setItem('id', id);
    });
		

});

//hide sidebar modal box
$(document).ready(function(){
	$('.cp_close').click(function(flag){
		var hidden = $('.chat_pop');
		hidden.animate({"right":"-100%"}, "slow").removeClass('visible');
	});
});
		
//show sidebar modal box
function modal_box(flag){
	var hidden = $('.chat_pop_'+flag);
	if (hidden.hasClass('visible')){
		hidden.animate({"right":"-100%"}, "slow").removeClass('visible');
	} else {
		hidden.animate({"right":"0"}, "slow").addClass('visible');
	}
}	