@extends('front.online_visit_layout.app')

@section('content')
    <section class="sign_in_sec_wrapper">
        <div class="sign_in_sec shadow">
            <img src="{{asset('assets/images/rmd_logo_red.svg')}}" alt="rmd_logo_red">
            <h1 class="title mt-4">Your account is waiting for our Administration approval.</h1>
            <p>You will receive email notification,Kindly check back later!</p>
        </div>
    </section>	
@endsection