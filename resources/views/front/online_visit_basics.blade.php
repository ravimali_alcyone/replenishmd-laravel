@extends('front.online_visit_layout.app')

@section('content')
     <div class="tabs_wrapper welcome basics">
        <div>
            <a href="javascript:void(0);" class="how_it_works">The Basics</a>
            <div class="steps">
                <span class="active ml-0"></span>
                <span></span>
                <span></span>
                <span></span>
            </div>
        </div>
    </div>

    <div class="main_welcome_wrapper">
        <section class="welcome_content_wrapper the_basics_sec">
            <div>
                <h3 class="heading">The Basics</h3>
                <p class="description">This information helps your doctor determine if you're eligible for treatment.</p>

                <form id="basicsForm">
                    <div class="form-group">
                        <label for="biological_sex">Biological Sex</label>
                        <div class="biological_sex">
                            <span class="male <?php if($user->gender == 'male' && $user->gender !=''){ echo 'active';}else{ echo 'active';}?>" data="male">Male</span>
                            <span class="female <?php if($user->gender == 'female'){ echo 'active';}?>" data="female">Female</span>
                        </div>
						<input type="hidden" name="gender" id="gender" value="<?php if($user->gender){ echo $user->gender; }else{ echo 'male'; } ?>" />
                    </div>
                    <div class="form-group">
                        <label for="birthdate">Birthdate</label>
                        <input type="text" name="birth_date" class="form-control" placeholder="mm/dd/yyyy" id="birth_date" required="required"  max="{{$max_date}}" value="<?php if($user->dob != '0000-00-00'){ echo date(env('DATE_FORMAT_PHP'),strtotime($user->dob)); } ?>">
                    </div>
                    <div class="form-group">
                        <label for="zip_code">Zipcode</label>
                        <input type="text" name="zip_code" class="form-control" maxlength="5" onkeypress="return event.charCode >= 48 && event.charCode <= 57"  placeholder="######" id="zip_code" required="required" maxlength="5" value="@if($user->zip_code){{$user->zip_code}}@endif" data-toggle="tooltip" data-placement="right" title="Only Texas zipcodes are allowable.">
                    </div>
                    <div class="form-group">
                        <label for="phone_number">Phone number</label>
                        <input type="text" name="phone_number" class="form-control" maxlength="10" onkeypress="return event.charCode >= 48 && event.charCode <= 57" placeholder="(###) ###-####" id="phone_number" required="required" value="@if($user->phone){{$user->phone}}@endif">
                    </div>
                    <button type="submit" id="saveBtn" class="btn btn-primary">Next</button>
                </form>
            </div>
        </section>
    </div>
	
	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	<script src="{{asset('assets/js/jquery.mask.js')}}"></script>
	
    <script>
	
		
		$("#phone_number").mask("(999) 999-9999");

        $(document).ready(function() {
			$("#birth_date").datepicker({
				dateFormat: "{{env('DATE_FORMAT_JQ')}}",
				maxDate: -1,
				changeMonth: true,
				changeYear: true,
				yearRange: "-150:+0",
			});

            $('[data-toggle="tooltip"]').tooltip(); 

            $(".the_basics_sec .biological_sex span").click(function() {
                $(".the_basics_sec .biological_sex span").removeClass("active");
                $(this).addClass("active");
				let gender = $(this).attr('data');
				$("#gender").val(gender);
            });
        });
		
		$("#basicsForm").submit(function(e) {
			e.preventDefault();
			$(".loader").css('display', 'flex');
			
			$("#phone_number").unmask();
			
			$.ajax({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				data: $('#basicsForm').serialize(),
				url: "{{ route('basics_submission') }}",
				type: "POST",
				// dataType: 'json',
				success: function (response) {

					if(response['status'] == 'success'){
						setTimeout(function(){
							$(".loader").css('display', 'none');
							window.location = "{{ route('medical_questions') }}";
							
						}, 1000);
					}else{
						$(".loader").css('display', 'none');
						$("#phone_number").mask("(999) 999-9999");
					}
				},
				error: function (data) {
					$(".loader").css('display', 'none');
					let errors = data.responseJSON.errors;
					
					$.each(errors, function(key, value) {
						errorAlert(value[0],5000,'bottom-left');
						$("#phone_number").mask("(999) 999-9999");
					});					
/* 					swal({
						title: 'Error Occured.',
						icon: 'error'
					}) */
					$('#saveBtn').html('Save');
				}
			});
		});			
    </script>
@endsection