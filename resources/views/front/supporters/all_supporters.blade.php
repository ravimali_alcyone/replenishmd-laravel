@extends('front.common_layout.app')

@section('stylesheet')
    <link href="{{asset('dashboard/css/supporters.css')}}" rel="stylesheet" />
@endsection

@section('content')
	@if(auth()->user()->user_role==3)
		<div class="col-xl-10 col-lg-8 col-md-9 col-sm-12">
	@endif
			<div class="row">
				<div class="col-xl-9 col-lg-9 col-md-9 col-sm-12">
					<div class="supperter_mid_wrap">
					
						<div class="supportersAccept">
							<section class="supporters_sec top">
								<div class="title_box">
									<h6><span>{{ count($followers_data) }}</span> Supporters</h6>
									@if(count($followers_data) > 10)
										<a href="{{ auth()->user()->user_role==4  ? url('/user/all_supporters') : url('/provider/all_supporters') }}">See All <span class="fa fa-angle-right"></span></a>
									@endif
								</div>
								<div class="cards_wrapper owl-carousel" id="supporters">
									@isset($followers_data)
										@foreach($followers_data as $key => $value)
											@include('front.supporters.supporter_card')
										@endforeach
									@endisset
								</div>
							</section>
						</div>
					</div>
				</div>
				@include('front.supporters.right_sidebar')
			</div>
	@if(auth()->user()->user_role==3)
		</div>
	@endif

    <!-- Modal -->
    <div class="modal fade" id="unFollowModal" tabindex="-1" role="dialog" aria-labelledby="unFollowModal" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Confirm to unfollow</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>Are you sure you want to unfollow <span class="username font-weight-bold"></span>?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" onclick="javascript: unfollow(this)">Unfollow</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>

    <input type="hidden" id="pusher_api_key" value="{{ env('PUSHER_APP_KEY') }}">
    <input type="hidden" id="pusher_channel" value="{{ env('PUSHER_APP_CHANNEL') }}">
    <input type="hidden" id="pusher_cluster" value="{{ env('PUSHER_APP_CLUSTER') }}">
@endsection

@push("scripts")
    <script src="//js.pusher.com/3.1/pusher.min.js"></script>
    <script src="{{ asset('dashboard/js/supporter.js') }}"></script>
@endpush