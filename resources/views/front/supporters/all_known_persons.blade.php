@extends('front.common_layout.app')

@section('stylesheet')
    <link href="{{asset('dashboard/css/supporters.css')}}" rel="stylesheet" />
@endsection

@section('content')

@php
use App\SupporterRequest;
@endphp
	@if(auth()->user()->user_role==3)
		<div class="col-xl-10 col-lg-8 col-md-9 col-sm-12">
	@endif
			<div class="row">
				<div class="col-xl-9 col-lg-9 col-md-9 col-sm-12">
					<div class="supperter_mid_wrap">
						<section class="supporters_sec top">
							<div class="title_box">
								@if(!empty($_GET['search']))
								<h6>Search Results for "{{$_GET['search']}}"</h6>
								@else
								<h6>You may know?</h6>
								@endif
							</div>
							<div class="cards_wrapper">
								@isset($users_data)
									@foreach($users_data as $key => $value)
										<div class="card card_{{ $value['id'] }}">
											<div class="img_wrapper">
											<a href="@if(auth()->user()->user_role==4){{URL('user/view')}}/{{ $value['id'] }}@else{{URL('provider/view')}}/{{ $value['id'] }}@endif">
											<img src="{{ isset($value['image']) ? '/' : '' }}{{ $value['image'] ?? asset('dashboard/img/default-user-icon.jpg') }}" alt="supporter-1" class="w-100"></a></div>
											
											<div class="text_wrapper text-center">
												<a href="@if(auth()->user()->user_role==4){{URL('user/view')}}/{{ $value['id'] }}@else{{URL('provider/view')}}/{{ $value['id'] }}@endif"" style="color:#000;"><h6>{{ $value['name'] }}</h6></a>
												<!--<p class="mb-0"><span>{{ $value['clinical_expertise'] }}</span></p>-->
											</div>
											<div class="text-center sup_card_btns">
												@if($value['request_status'] == 'Pending')
													<span href="javascript:void(0)" class="following pending">Pending...</span>
												@else
													
													@php
													$follower = SupporterRequest::where(['user_id' => auth()->user()->id, 'status'=>1, 'request_id' => $value['id']])->first();
													$follower_both = SupporterRequest::where(['user_id' => $value['id'], 'status'=>1, 'request_id' => auth()->user()->id])->first();
													
													@endphp
													
													@if((!empty($follower)) && (!empty($follower_both)))
														<a href="javascript:void(0)" class="following" data-toggle="modal" data-target="#unFollowModal" data="{{$value['id']}}"><span class="fa fas fas fa-check"></span> Supporting</a>	
													@else
														@if(!empty($follower)) 
														<!--<a href="javascript:void(0)" class="following" data-toggle="modal" data-target="#unFollowModal" data="{{$value['id']}}"><span class="fa fas fas fa-check"></span> Supporting</a>-->
														@endif
														<a href="javascript:void(0)" class="follow" onclick="javascript: follow({{$value['id']}}, this,1)"><span class="fa fas fa-plus"></span> Support</a>
													@endif
												@endif
											</div>
										</div>
									@endforeach
								@endisset
							</div>
						</section>
					</div>
				</div>
				@include('front.supporters.right_sidebar')
			</div>
	@if(auth()->user()->user_role==3)
		</div>
	@endif

    <!-- Modal -->
    <div class="modal fade" id="unFollowModal" tabindex="-1" role="dialog" aria-labelledby="unFollowModal" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Confirm to unsupporting</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>Are you sure you want to unsupporting <span class="username font-weight-bold"></span>?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" onclick="javascript: unfollow(this)">UnSupporting</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
@endsection
