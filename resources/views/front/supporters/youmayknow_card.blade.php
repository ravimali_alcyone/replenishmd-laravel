<div class="card card_{{ $value['id'] }}">
	<div class="img_wrapper">
		<a href="@if(auth()->user()->user_role==4){{URL('user/view')}}/{{ $value['id'] }}@else{{URL('provider/view')}}/{{ $value['id'] }}@endif">
		<img src="{{ isset($value['image']) ? '/' : '' }}{{ $value['image'] ?? asset('dashboard/img/default-user-icon.jpg') }}" alt="supporter-1" class="w-100">
		</a>
	</div>
	<div class="text_wrapper text-center">
		<a href="@if(auth()->user()->user_role==4){{URL('user/view')}}/{{ $value['id'] }}@else{{URL('provider/view')}}/{{ $value['id'] }}@endif" style="color:#000;"><h6>{{ $value['name'] }}</h6></a>
		<!--<p class="mb-0"><span>{{ $value['clinical_expertise'] }}</span></p>-->
	</div>
	<div class="text-center sup_card_btns">
		@if($value['request_status'] == 'Pending')
			<span href="javascript:void(0)" class="following pending">Pending...</span>
		@else
			<a href="javascript:void(0)" class="follow" onclick="javascript: follow({{ $value['id'] }}, this)"><span class="fa fas fa-plus"></span> Support</a>
		@endif
	</div>
</div>