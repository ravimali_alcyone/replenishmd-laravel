<div class="rigt_sidebar col-xl-3 col-lg-3 col-md-3 col-sm-12">
	<div class="blog-stories sponsored">
		<h4 class="blog-heading">Sponsored</h4>
		<ul class="m-0 p-0">
			<li class="mt-15">
				<a href="#">
					<div class="blog-feature-image">
						<img src="{{ asset('dashboard/img/sponsor-1.png') }}" alt="sponsor-1" />
					</div>
					<div class="blog-extract short-description">
						<p class="mb-0">Diet Plan according to your BMI</p>
						<span href="#">eathealthy.com</span>
					</div>
				</a>
			</li>
			<li class="mt-15">
				<a href="#">
					<div class="blog-feature-image">
						<img src="{{ asset('dashboard/img/sponsor-2.png') }}" alt="sponsor-2" />
					</div>
					<div class="blog-extract short-description">
						<p class="mb-0">Best Hospital for cancer treatment</p>
						<span href="#">eathealthy.com</span>
					</div>
				</a>
			</li>
			<li class="mt-15">
				<a href="#">
					<div class="blog-feature-image">
						<img src="{{ asset('dashboard/img/sponsor-1.png') }}" alt="sponsor-1" />
					</div>
					<div class="blog-extract short-description">
						<p class="mb-0">Diet Plan according to your BMI</p>
						<span href="#">eathealthy.com</span>
					</div>
				</a>
			</li>
			<li class="mt-15">
				<a href="#">
					<div class="blog-feature-image">
						<img src="{{ asset('dashboard/img/sponsor-2.png') }}" alt="sponsor-2" />
					</div>
					<div class="blog-extract short-description">
						<p class="mb-0">Best Hospital for cancer treatment</p>
						<span href="#">eathealthy.com</span>
					</div>
				</a>
			</li>
		</ul>
	</div>
	
@if($blogs && $blogs->count() > 0)
	<div class="blog-stories mt-15">
		<h4 class="blog-heading">Blog stories</h4>
		<ul class="m-0 p-0">
		@foreach($blogs as $key => $value)
			<li class="mt-15">
				<a href="{{ route('front.blogs.post', $value->slug) }}">
					<div class="blog-feature-image">
						<img src="{{ env('APP_URL').'/'.$value->image }}" alt="{{ $value->title}}" />
					</div>
					<div class="blog-extract short-description">
					{{ $value->title }}
					</div>
				</a>
			</li>
		@endforeach	
		</ul>
		<div class="see-all mt-30">
			<a href="{{ route('front.blogs') }}">See all Stories <img src="{{ asset('dashboard/img/arrow-forward.svg') }}" alt="arrow-forward"/></a>
		</div>
	</div>
@endif

@if($posts)
	<div class="blog-stories mt-15">
        <h4 class="blog-heading">Latest Photo</h4>
        <div class="right-latest-post lastest_mem new_gal">
			@foreach($posts as $val)
			<a class="" href="/{{ $val->media_name }}" data-lightbox="lightbox-set-10">
				<img class="avatar-post" src="/{{ $val->media_name }}" alt="...">
			</a>
			<!--<a href="javascript:void(0)"><img class="avatar-post" src="/{{ $val->media_name }}" alt=""></a>-->
			@endforeach
        </div>
    </div>
@endif
	<div class="advertisment mt-15">
		<img src="{{ asset('dashboard/img/ads.svg') }}" alt="ads" class="img-fluid" />
	</div>
</div>