<div class="card card_{{ $value['id'] }}">
	<div class="img_wrapper">
		<a href="@if(auth()->user()->user_role==4){{URL('user/view')}}/{{ $value['id'] }}@else{{URL('provider/view')}}/{{ $value['id'] }}@endif">
			<img src="<?php if(!empty($value['image'])){ echo asset($value['image']); }else{ echo asset('dashboard/img/default-user-icon.jpg'); } ?>" alt="supporter-1" class="w-100">
		</a>
	</div>
	<div class="text_wrapper text-center">
		<div class="dropdown sup_drop">
			<button class="unsup_btn" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				<span class="fa fa-ellipsis-v"></span>
			</button>
			<div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
				<a class="dropdown-item" href="javascript:void(0)" data-toggle="modal" data-target="#unFollowModal" data="{{ $value['id'] }}">Unsupport</a>
			</div>
		</div>
		<a href="@if(auth()->user()->user_role==4){{URL('user/view')}}/{{ $value['id'] }}@else{{URL('provider/view')}}/{{ $value['id'] }}@endif">
			<h6 style="color:#000;">{{ $value['name'] ?? '' }}</h6>
			<!--<p style="color:#999;" class="mb-0"><span>{{ $value['clinical_expertise'] ?? '' }}</span></p>-->
		</a>
	</div>
	<div class="text-center sup_card_btns">
		<!--<a href="javascript:void(0)" class="following" data-toggle="modal" data-target="#unFollowModal" data="{{ $value['id'] }}"><span class="fa fas fa-check text-primary"></span> Supporter</a>-->
		
		@if(isset($supporting_data_ids) && !in_array($value['id'], $supporting_data_ids))
			<a href="javascript:void(0)" class="follow" onclick="javascript: follow({{ $value['id'] }}, this,1)"><span class="fa fas fa-plus"></span> Support Back</a>
		@else
			<a href="javascript:void(0)" class="following"><span class="fa fas fa-check"></span> Supporting</a>
		@endif
	</div>
</div>