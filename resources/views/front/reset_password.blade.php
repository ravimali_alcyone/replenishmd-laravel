@extends('front.online_visit_layout.app')

@section('content')

    <section class="sign_in_sec_wrapper lost_pass">
        <div class="sign_in_sec shadow">
            <img src="{{asset('assets/images/rmd_logo_red.svg')}}" alt="rmd_logo_red">
            <h1 class="title">Enter OTP and update new password.</h1>
            <form id="resetPassForm">
                <div class="form-group">
                    <input name="otp" type="password" class="form-control" placeholder="OTP" id="otp" maxlength="4" onkeypress="return event.charCode >= 48 && event.charCode <= 57" required>				
                </div>
				
                <div class="form-group">
                    <input name="password" type="password" class="form-control" placeholder="New Password" id="pass" required>
                </div>

                <div class="form-group">
                    <input name="password_confirmation" type="password" class="form-control" placeholder="Confirm New Password" id="cpass" required>
                </div>				
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
            <a href="{{ route('sign_in') }}" class="link">Sign In</a>
        </div>
    </section>
	
	<script>
	
		$("#resetPassForm").submit(function(e) {
			e.preventDefault();
			$(".loader").css('display', 'flex');
			
			$.ajax({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				data: $('#resetPassForm').serialize(),
				url: "{{ route('reset_password') }}",
				type: "POST",
				// dataType: 'json',
				success: function (response) {
	
					if(response['status'] == 'success'){
						//successAlert(response['message'],2000,'top-right');
						callProgressBar();
						setTimeout(function(){
							window.location = response['redirect'];
						}, 1500);
					}else{
						errorAlert('Error occured.',3000,'top-right');					
					}
				},
				error: function (data) {
					$(".loader").css('display', 'none');
					let errors = data.responseJSON.errors;
					
					$.each(errors, function(key, value) {
						errorAlert(value[0],3000,'top-right');
					});					
				}
			});
		});		
	</script>	
	
@endsection