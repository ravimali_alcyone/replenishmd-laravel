@extends('front.dashboard_layout.app')

@section('content')
    <style>
        .social_postings {
            display: block !important;
        }
        .left_side_menu {
            display: none !important;
        }
    </style>
    <section class="main-dashboard mt-30 create_story_wrap">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-3">
                    <div class="cstry_sidebar bg-white rounded p-3">
                        <div class="cssb_title d-flex justify-content-between align-items-center">
                            <h4 class="font-weight-bold">Your Story</h4>
                            <a href="{{ url('user/dashboard') }}" class="close_btn"><span class="fa fa-times"></span></a>
                        </div>
                        <div class="cssb_user_info pt-2 pb-3 mt-2 mb-2 border-bottom">
                            <div class="post_by">
                                <div class="poster_img">
                                    @if(auth()->user()->image)
                                        <img src="{{ env('APP_URL')}}/{{auth()->user()->image }}" alt="{{ auth()->user()->name }}"/>
                                    @else
                                        <img src="{{ asset('dist/images/user_icon.png') }}" alt="{{ auth()->user()->name }}"/>
                                    @endif
                                </div>
                                <div class="poster_info">
                                    <div class="poster_name"><b>{{ auth()->user()->name }}</b></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-9">
                    <div class="cstry_preview_area create_story bg-white rounded p-3">
                        <div class="stry_pre_bg rounded">
                            <div class="stry_box sbc_one shadow-lg" id="photoStory">
                                <div class="stry_text_result font-weight-bold"><i class="fa fa-picture-o"></i> <span>Create a Photo Story</span></div>
                            </div>
                            <div class="stry_box sbc_two shadow-lg" id="textStory">
                                <div class="stry_text_result font-weight-bold"><i>Aa</i> Create a Text Story</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@push("scripts")
    <script>
        $(document).ready(function() {
            $("#photoStory").on("click", function() {
                location.href = "{{ url('user/photo_story') }}";
            });
            $("#textStory").on("click", function() {
                location.href = "{{ url('user/text_story') }}";
            });
        });
    </script>
@endpush