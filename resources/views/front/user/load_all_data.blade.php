<?php 
	use App\Library\Services\CommonService;
	$common = new CommonService();
	use App\FacebookAuth;
	$check_auth = FacebookAuth::where('user_id', auth()->user()->id)->first();
?>

<div class="col-md-12" id="allPost">
	@if($post && count($post)>0)
		@foreach($post as $val)
			<div class="post_wrap" id="post_wrap_{{$val->post_id}}">
				<div class="post_head">
					
					<div class="post_by">
						<div class="poster_img">
							@if($val->postImage)
								<img src="{{ env('APP_URL')}}/{{ $val->postImage }}" alt="{{ $val->postBy }}"/>
							@else
								<img src="{{ asset('dist/images/user_icon.png') }}" alt="{{ $val->postBy }}"/>
							@endif
						</div>
						<div class="poster_info">
							<div class="poster_name"><b>{{$val->postBy}}</b></div>
							<div class="post_date"><span><a href="{{env('APP_URL')}}/post/{{$val->param_id}}/0">{{ $common->get_time_ago(strtotime($val->created_at))}}</a></span></div>
						</div>
					</div>

					<div class="dropdown post_setting">
						<button class="dots_btn" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<span>&#8411;</span>
						</button>
						<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
							<a class="dropdown-item" href="#">Hide Post</a>
							<a class="dropdown-item" href="#">Block This Person</a>
							<a class="dropdown-item" href="#">Report</a>
						</div>
					</div>

				</div>
				<div class="post_body">
					<div class="post_text">
						<p class="fb_post_text_{{ $val->post_id }}">@if($val->post_content){{$val->post_content}} @endif</p>
					</div>
					<div class="post_imgs">
					<?php $total_img = '';?>
						@isset($posts_media[$val->post_id])
							@foreach($posts_media[$val->post_id] as $key => $value)
								<?php $total_img .= $value['media_name'].',';?>
								<a class="photo example-image-link " href="">
									<img class="example-image {{ count($posts_media[$val->post_id]) == 1 ? 'mw-100' : '' }}" src="/{{ $value['media_name'] }}" id="{{ $value['media_id'] }}" data-postId="{{$val->post_id}}" />
								</a>
								
							@endforeach
							
							<input type="hidden" class="fb_images_{{ $val->post_id }}" data-fcount="{{ count($posts_media[$val->post_id]) }}" value="{{ rtrim($total_img , ',') }}" name="fb_images_{{ $val->post_id }}">
							@if(count($posts_media[$val->post_id]) > 4)
								<span class="more_imgs_caption">{{ count($posts_media[$val->post_id]) - 4 }} More</span>
							@endif
						@endisset
					</div>
					<div class="post_reactions">
						<div class="post_likes like_user_name_{{$val->post_id}}">
							<div class="top_three_recent">
								<div><img src="{{ asset('dashboard/img/smiles/like.svg') }}" alt=""></div>
								<div><img src="{{ asset('dashboard/img/smiles/in-love.svg') }}" alt=""></div>
								<div><img src="{{ asset('dashboard/img/smiles/angry.svg') }}" alt=""></div>
							</div>
							@if(isset($likePostUserData[$val->post_id]))
								@foreach($likePostUserData[$val->post_id] as $key => $value)
									<span><a href="javascript:void(0)" class="post_like_{{$val->post_id}} <?php if (strpos($value, 'You') !== false) { echo 'like'; }else{ echo 'no_like'; } ?>" id="{{$value}}" onclick="getLikeDetail({{$val->post_id}})">
									<?php 
										if( ($val->num_of_post_like) >3)
										{
											$str ='';
											if (strpos($str, 'You') !== false) {
												echo 'You,'.trim(preg_replace("/,+/", ",", str_replace("You","",$value)), ",").',&'.($val->num_of_post_like - 3).' others';
											}
										}
										else if( ($val->num_of_post_love) >3)
										{
											$str ='';
											if (strpos($str, 'You') !== false) {
												echo 'You,'.trim(preg_replace("/,+/", ",", str_replace("You","",$value)), ",").',&'.($val->num_of_post_love - 3).' others';
											}
										}
										else if( ($val->num_of_post_sad) >3)
										{
											$str ='';
											if (strpos($str, 'You') !== false) {
												echo 'You,'.trim(preg_replace("/,+/", ",", str_replace("You","",$value)), ",").',&'.($val->num_of_post_sad - 3).' others';
											}
										}
										else if( ($val->num_of_post_angary) >3)
										{
											$str ='';
											if (strpos($str, 'You') !== false) {
												echo 'You,'.trim(preg_replace("/,+/", ",", str_replace("You","",$value)), ",").',&'.($val->num_of_post_angary - 3).' others';
											}
										}
										else 
										{
											if($value=='You')
											{
												echo trim('You,'.trim(preg_replace("/,+/", ",", str_replace("You","",$value)), ","), ",");
											} 
											else
											{
												echo trim(preg_replace("/,+/", ",", str_replace("You","",$value)), ",");
											}
										}

										
									?> </a></span>
								@endforeach
							@endisset
						</div>
						<div class="post_status">
							<div class="post_s_type"><span id="{{$val->post_id}}" onclick="getCommentDetail(this.id)">Comments</span><span id="comment_count_{{$val->post_id}}">{{$val->num_of_post_commnet}}</span></div>
							<div class="post_s_type">Share<span>{{$val->num_of_post_share}}</span></div>
						</div>
					</div>
					@if($flagId == auth()->user()->id)
					<div class="posting_btn_grp">
						<div class="rec_btn">
						
							@if(isset($post_likes[$val->post_id]))
								<span class="like-btn {{ $post_likes[$val->post_id] == 1 ? 'reacted' : '' }}">
							@else
								<span class="like-btn">
							@endif
								<span class="like-btn-text" id="{{$val->post_id}}" onclick="doLikePost(this.id, {{ isset($post_likes[$val->post_id]) ? $post_likes[$val->post_id] : 0 }})"><i class="fa fa-thumbs-up"></i> Like</span>
								<ul class="reactions-box">
									@if(isset($post_likes_flagData[$val->post_id]))
									@foreach($post_likes_flagData[$val->post_id] as $key => $value)
										<li class="reaction reaction-like" data-reaction="Like" data-postId="{{$val->post_id}}" data-likeFlag="{{ $value['like_flag'] }}"></li>
										<li class="reaction reaction-love" data-reaction="Love" data-postId="{{$val->post_id}}" data-likeFlag="{{ $value['love_flag'] }}"></li>
										<li class="reaction reaction-sad" data-reaction="Sad" data-postId="{{$val->post_id}}" data-likeFlag="{{ $value['sad_flag'] }}"></li>
										<li class="reaction reaction-angry" data-reaction="Angry" data-postId="{{$val->post_id}}" data-likeFlag="{{ $value['angary_flag'] }}"></li>
									@endforeach
									@else
										<li class="reaction reaction-like" data-reaction="Like" data-postId="{{$val->post_id}}" data-likeFlag="0"></li>
										<li class="reaction reaction-love" data-reaction="Love" data-postId="{{$val->post_id}}" data-likeFlag="0"></li>
										<li class="reaction reaction-sad" data-reaction="Sad" data-postId="{{$val->post_id}}" data-likeFlag="0"></li>
										<li class="reaction reaction-angry" data-reaction="Angry" data-postId="{{$val->post_id}}" data-likeFlag="0"></li>
									@endisset
								</ul>
							</span>
						</div>
						<div class="rec_btn"><a data-toggle="collapse" href="#collapseComment_{{$val->post_id}}" role="button" aria-expanded="false" aria-controls="collapseComment"><i class="fa fa-comment"></i> Comment</a></div>
						<div class="dropdown">
							<div class="rec_btn" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><a href="javascript:void(0)"><i class="fa fa-share"></i> Share</a></div>
							<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
								<a class="dropdown-item" href="javascript:void(0)">Only me</a>
								<div class="fb-like" data-href="https://developers.facebook.com/docs/plugins/" data-layout="standard" data-action="like" data-size="small" data-show-faces="true" data-share="true"></div>
								@if(!empty($check_auth->token))
								<a class="dropdown-item facebookPostShare" data-id="{{$val->post_id}}" href="javascript:void(0)">Via Facebook</a>
								@else
								<a class="dropdown-item" href="{{ URL('/facebook_login') }}">Via Facebook</a>
								@endif
								<a class="dropdown-item" href="javascript:void(0)">Via LinkedIn</a>
							</div>
						</div>
						
					</div>
					@endif
				</div>
				<div class="post_comment_block">
					<form id="form_post_comment_{{$val->post_id}}">
						<div class="collapse" id="collapseComment_{{$val->post_id}}">
							<div class="comment_row">
								@if(isset($post_comment[$val->post_id]))
								@php
								$i=1;
								@endphp
								@foreach($post_comment[$val->post_id] as $key => $value)
								<?php if($i<6) {?>
								<div class="media">
									<div class="poster_img">
									@if($value['image'])
										<img src="{{ env('APP_URL')}}/{{$value['image'] }}" alt="{{ auth()->user()->name }}" />
									@else
										<img src="{{ asset('dist/images/user_icon.png') }}" alt="{{ auth()->user()->name }}" />
									@endif
									</div>
									<div class="media-body">
										<h6 class="mt-0">{{$value['commentBy'] }}</h6>
										<p>{{ $value['comments'] }}</p>
									</div>
								</div>
								<?php } 
								elseif($i>=6) {
									if($i==6){ echo '<span id="show_more" class="show_more_comment show_more'.$val->post_id.'"><a href="javascript:void(0)">Show more</a></span>';
									}		
								?>
								<div class="media show_more_text" style="display:none;">
									<div class="poster_img">
									@if($value['image'])
										<img src="{{ env('APP_URL')}}/{{$value['image'] }}" alt="{{ auth()->user()->name }}" />
									@else
										<img src="{{ asset('dist/images/user_icon.png') }}" alt="{{ auth()->user()->name }}" />
									@endif
									</div>
									<div class="media-body">
										<h6 class="mt-0">{{$value['commentBy'] }}</h6>
										<p>{{ $value['comments'] }}</p>
									</div>
								</div>
								
								<?php
									if($i==count($post_comment[$val->post_id]))	{										
										echo '<span class="show_less s_m_'.$val->post_id.'" id="show_less" style="display:none;"><a href="javascript:void(0)">Show Less</a></span>'; 
									}
								}
								?>
								@php
								$i++;
								@endphp
								@endforeach
								@endisset
							</div>
							<div class="post_comment">
								<div class="comnt_by">
								@if(auth()->user()->image)
									<img class="img-fluid" src="{{ env('APP_URL')}}/{{auth()->user()->image }}" alt="{{ auth()->user()->name }}"/>
								@else
									<img class="img-fluid" src="{{ asset('dist/images/user_icon.png') }}" alt="{{ auth()->user()->name }}"/>
								@endif
								</div>
								<div class="comnt_field">
									<input type="hidden" class="form-control" name="post_id" id="post_id" value="{{$val->post_id}}">
									<input type="text" class="form-control" name="add_post_comment" id="add_post_comment_{{$val->post_id}}" onkeydown="callComment({{$val->post_id}})" value="" placeholder="write a comment...">
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		@endforeach
	@endif
</div>
@if(!empty($load_post) && count($load_post)>0)
<div class="col-md-12">
	<input type="hidden" name="offset1" id="offset1" value="{{$offset1}}">
	<input type="hidden" name="offset2" id="offset2" value="{{$offset2}}">
	<a href="javascript:void(0)" onclick="loadMorePost($('#offset1').val(),$('#offset2').val())">Load more</a>
</div>
@endif