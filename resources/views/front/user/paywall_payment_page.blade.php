@extends('front.online_visit_layout.app')
@section('css')
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
@section('content')

    <div class="flex items-center mt-5">
        <div class="md:w-1/2 md:mx-auto">

            @if (session('status'))
                <div class="text-sm border border-t-8 rounded text-green-700 border-green-600 bg-green-100 px-3 py-4 mb-4" role="alert">
                    {{ session('status') }}
                </div>
            @endif

            <div class="flex flex-col break-words bg-white border border-2 rounded shadow-md">

				<div class="font-semibold bg-gray-200 text-gray-700 py-3 px-6 mb-0 text-right">
					<span class="px-6">{{ session('paywall_feature_title') }} Payment Amount</span>
					<span class="font-bold">${{ session('paywall_feature_amount') }}</span>
				</div>

                <div class="w-full p-6">


						
                        @if(session('error_message'))
                            <div role="alert" class="mb-4">
                                <div class="bg-red-500 text-white font-bold rounded-t px-4 py-2">
                                    Payment Failed
                                </div>
                                <div class="border border-t-0 border-red-400 rounded-b bg-red-100 px-4 py-3 text-red-700">
                                    <p>{{ session('error_message') }}</p>
                                </div>
                            </div>
                        @endif

                        <div class="lg:w-2/3 md:w-1/2 rounded border border-gray-200 mx-auto mt-8 p-6 clearfix">
                            
                            <form id="signup-form" action="{{ route('user.paywall_billing') }}" method="post">
                                @csrf
                                <div class="flex flex-wrap mb-6 mt-8 px-6">
                                        <label for="card-element" class="block text-gray-700 text-sm font-bold mb-2">
                                            Name on Card
                                        </label>
                                        <input type="text" name="name" id="name" class="shadow appearance-none border rounded w-full py-3 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline">
                                </div>
                                <div class="flex flex-wrap mb-6 mt-8 px-6">
                                    <label for="card-element" class="block text-gray-700 text-sm font-bold mb-2">
                                        Credit Card Info
                                    </label>
                                    <!-- Stripe Elements Placeholder -->
                                    <div id="card-element" class="shadow appearance-none border rounded w-full py-3 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"></div>
                                    <div id="card-errors" class="text-red-400 text-bold mt-2 text-sm font-medium"></div>
                                </div>
                                
                                <button type="submit" id="card-button" data-secret="{{ $intent->client_secret }}" class="inline-block align-middle text-center select-none border font-bold whitespace-no-wrap py-2 px-4 rounded text-base leading-normal no-underline text-gray-100 bg-red-500 hover:bg-red-600 float-right mr-6 mt-3">
                                    Submit
                                </button>
                            </form>
                        </div>
                </div>
            </div>
            
        </div>
    </div>

        <script src="https://js.stripe.com/v3/"></script>

        <script>
            const stripe = Stripe('{{ env("STRIPE_KEY") }}');

            const elements = stripe.elements();
            const cardElement = elements.create('card');

            cardElement.mount('#card-element');

            const cardHolderName = document.getElementById('name');
            const cardButton = document.getElementById('card-button');
            const clientSecret = cardButton.dataset.secret;
            const cardError = document.getElementById('card-errors');

            cardElement.addEventListener('change', function(event) {
                if (event.error) {
                    cardError.textContent = event.error.message;
                } else {
                    cardError.textContent = '';
                }
            });

            var form = document.getElementById('signup-form');

            form.addEventListener('submit', async (e) => {
                e.preventDefault();

                const { setupIntent, error } = await stripe.handleCardSetup(
                    clientSecret, cardElement, {
                        payment_method_data: {
                            billing_details: { name: cardHolderName.value }
                        }
                    }
                );

                if (error) {
                    // Display "error.message" to the user...
                    console.log(error);
                } else {
                    // The card has been verified successfully...
                    var hiddenInput = document.createElement('input');
                    hiddenInput.setAttribute('type', 'hidden');
                    hiddenInput.setAttribute('name', 'payment_method');
                    hiddenInput.setAttribute('value', setupIntent.payment_method);
                    form.appendChild(hiddenInput);
                    // Submit the form
					$(".loader").css('display', 'flex');
                    form.submit();
                }
            });
        
        </script>
    

@endsection

