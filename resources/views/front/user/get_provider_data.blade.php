@if($providers)
	@foreach($providers as $provider)
		<div class="cleint-box mt-15 bg-white round-crn pd-20-30 mt-15 hover-effect-box select_provider @if($provider['is_available'] == 1)pro_online @endif" onclick="selectProvider(this, {{$online_visit->id}}, {{$provider['id']}})">
			<div class="pateint_details">
				<div class="provider_left">
					<div class="user_pic">
						<img src="{{ env('APP_URL') }}/{{$provider['image']}}" alt="{{$provider['name']}}">
					</div>
					<div class="user_name">
						<h4 class="mt-15">{{$provider['name']}}</h4>
						<p>{{$provider['provider_categories']}}</p>
					</div>

				</div>
				<div class="review_details mt-15 ">
				@if($provider['rating'] != '' )
					<div class="con_no">
					@php
						$n = round(2*$provider['rating'])/2;
						$int = floor($n);
						$dec = $n - $int;
					@endphp

					@for($i=1; $i<= 5; $i++)
						@if($i <= $int)
							<span class="fa fa-star checked"></span>
						@elseif(($i == $int + 1) && ($dec == 0.5) )
							<span class="fa fa-star-half-o checked"></span>
						@else
							<span class="fa fa-star"></span>
						@endif
					@endfor
					</div>
					<div class="review_cmt">
						<span>{{ $provider['review_count'] }} reviews</span>
					</div>
				@endif
				</div>
			</div>
		</div>
	@endforeach
@endif