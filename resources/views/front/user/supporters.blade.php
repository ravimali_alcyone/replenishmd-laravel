@extends('front.common_layout.app')
@section('stylesheet')
    <link href="{{asset('dashboard/css/supporters.css')}}" rel="stylesheet" />
@endsection
<?php 
use App\socialUserFollowConnection;
use App\SupporterRequest;
$user =socialUserFollowConnection::where('user_id', auth()->user()->id)->first();
use App\User;
//print_r(auth()->user()->user_role); die();
?>
@section('content')
	@if(auth()->user()->user_role==3)
		<div class="col-xl-6 col-lg-6 col-md-12">
	@endif
	<div class="mid_wrap">
        <div class="supperter_mid_wrap">
            <section class="request_sec">
                <div class="title_box">
                    <h6>Supporter’s Requests</h6>
                </div>
                <div class="requests_wrapper" id="requests_wrapper">
                    @if(empty($requests_data))
                        <p class="text-center text-secondary mb-0"><small>No requests found.</small></p>
                    @endif
                    @isset($requests_data)
                        @foreach($requests_data as $key => $value)
                            <div class="request_box">
                                <div class="info_box">
                                    <div class="img_wrapper"><img src="<?php if(!empty($value['image'])){ echo asset($value['image']); }else{ echo asset('dashboard/img/default-user-icon.jpg'); } ?>" alt="request-{{ $key }}" class="mw-100"></div>
                                    <div class="text_wrapper">
                                        <h6>{{ $value['name'] ?? '' }}</h6>
                                        <p class="designation">{{ $value['clinical_expertise'] ?? '' }}</p>
										<?php 
										    $request_user =socialUserFollowConnection::where('user_id', $value['request_id'])->first();
                                                                         
                                            if(!empty($user)){
												$user_follower_id = explode(",",$user['follower_id']);
												if(!empty($request_user)){
													$request_follower_id = explode(",",$request_user['follower_id']);
													$mane_id= array_intersect($request_follower_id,$user_follower_id);
													$name='';
													if(!empty($mane_id)){
														
														foreach($mane_id as $fval){
															$user_name = user::where('id', $fval)->first();
															$name.=$user_name['name'].', ';
														}
													    if(count($mane_id)>1){
															echo '<p>'.$name.' '.count($mane_id).' mutual supporters.</p>';
														}else{
															echo '<p>'.$name.' mutual supporter.</p>';
														}
													}
												}
											}
										?>
                                        
                                    </div>
                                </div>
                                <div class="btns_box">
                                    <button class="btn btn-primary requestAccept" data-id="{{ $value['request_id'] }}" onclick="javascript:requestAccept({{ $value['request_id'] }},this)">Confirm</button>
                                    <button class="btn btn-default requestIgnore" data-id="{{ $value['request_id'] }}" onclick="javascript:requestIgnore({{ $value['request_id'] }},this)">Ignore</button>
                                    <?php if($value['request_back_id']==0) { ?>
                                        <button class="btn btn-primary" onclick="javascript: follow({{ $value['request_id'] }}, this, 1)"><span class="fa fas fa-plus"></span> Support Back</button>
                                    <?php } ?>
                                </div>
                            </div>
                        @endforeach
                    @endisset
                </div>
            </section>
			
			<div class="supportersAccept">
            @if(count($followers_data))
                <section class="supporters_sec top">
                    <div class="title_box">
                        <h6><span>{{ count($followers_data) }}</span> Supporters</h6>
                        @if($followers_count > 10)
                            <a href="@if(auth()->user()->user_role==4){{ url('/user/all_supporters') }}@else{{ url('/provider/all_supporters') }}@endif">See all <span class="fa fa-angle-right"></span></a>
                        @endif
                    </div>
                    <div class="cards_wrapper owl-carousel" id="supporters">
                        @isset($followers_data)
                            @foreach($followers_data as $key => $value)
                                <div class="card card_{{ $value['id'] }}">
                                    <div class="img_wrapper">
										<a href="@if(auth()->user()->user_role==4){{URL('user/view')}}/{{ $value['id'] }}@else{{URL('provider/view')}}/{{ $value['id'] }}@endif">
											<img src="<?php if(!empty($value['image'])){ echo asset($value['image']); }else{ echo asset('dashboard/img/default-user-icon.jpg'); } ?>" alt="supporter-1" class="w-100">
										</a>
									</div>
                                    <div class="text_wrapper">
										<a href="@if(auth()->user()->user_role==4){{URL('user/view')}}/{{ $value['id'] }}@else{{URL('provider/view')}}/{{ $value['id'] }}@endif">
											<h6 style="color:#000;">{{ $value['name'] ?? '' }}</h6>
											<p style="color:#999;" class="mb-0"><span>{{ $value['clinical_expertise'] ?? '' }}</span></p>
										</a>
                                        <div class="text-center">
                                            <a href="javascript:void(0)" class="following" data-toggle="modal" data-target="#unFollowModal" data="{{ $value['id'] }}"><span class="fa fas fas fa-check"></span> Supporting</a>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @endisset
                    </div>
                </section>
            @endif
			</div>

            <section class="supporters_sec bottom {{ count($followers_data) ? 'mt-0' : '' }}">
                <div class="title_box">
                    <h6>You may know?</h6>
                    @if($persons_count > 10)
                        <a href="@if(auth()->user()->user_role==4){{ url('/user/all_known_persons') }}@else{{ url('/provider/all_known_persons') }}@endif">See all <span class="fa fa-angle-right"></span></a>
                    @endif
                </div>
                <div class="cards_wrapper owl-carousel" id="youMayKnow">
                    @if(!empty($users_data))
                        @foreach($users_data as $key => $value)
                            <div class="card card_{{ $value['id'] }}">
                                <div class="img_wrapper">
								<a href="@if(auth()->user()->user_role==4){{URL('user/view')}}/{{ $value['id'] }}@else{{URL('provider/view')}}/{{ $value['id'] }}@endif">
								<img src="{{ isset($value['image']) ? '/' : '' }}{{ $value['image'] ?? asset('dashboard/img/default-user-icon.jpg') }}" alt="supporter-1" class="w-100">
								</a>
								</div>
                                <div class="text_wrapper">
                                    <h6><a href="@if(auth()->user()->user_role==4){{URL('user/view')}}/{{ $value['id'] }}@else{{URL('provider/view')}}/{{ $value['id'] }}@endif" style="color:#000;">{{ $value['name'] }}</a></h6>
                                    <p class="mb-0"><span>{{ $value['clinical_expertise'] }}</span></p>
                                    <div class="text-center">
                                        @if($value['request_status'] == 'Pending')
                                            <span href="javascript:void(0)" class="following pending">Pending...</span>
                                        @else
                                            <a href="javascript:void(0)" class="follow" onclick="javascript: follow({{ $value['id'] }}, this)"><span class="fa fas fa-plus"></span> Support</a>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @endif
                </div>
            </section>
        </div>
    </div>
	@if(auth()->user()->user_role==3)
		</div>
	@endif
	@if(auth()->user()->user_role==4)
		@include('front.user.right_sidebar')
	@else
		@include('front.provider.right_sidebar')
	@endif
    <!-- Modal -->
    <div class="modal fade" id="unFollowModal" tabindex="-1" role="dialog" aria-labelledby="unFollowModal" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Confirm to unsupporting</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>Are you sure you want to unsupporting <span class="username font-weight-bold"></span>?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" onclick="javascript: unfollow(this)">unSupporting</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
@endsection


