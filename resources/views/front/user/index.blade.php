@extends('front.dashboard_layout.app')
@section('content')
	@php
		$name = (strpos(auth()->user()->name, ' ') !== false) ? explode(' ',auth()->user()->name)[0] : auth()->user()->name;
		$post_id;
	@endphp

<style>
	#adicionafoto {
		position: absolute;
		left: 80%;
		opacity: 0;
		cursor: pointer;
	}
	#galeria{
		display: flex;
	}
	#galeria img{
		width: 100px;
		height: 100px;
		border-radius: 10px;
		box-shadow: 0 0 8px rgb(0 0 0 / 20%);
		opacity: 85%;
		margin-right: 10px;
		margin-top: 10px;
	}
	.input_chat_box .emojionearea .emojionearea-editor {
    min-height: 46px;
    text-align: left;
	}
</style>

<link rel="stylesheet" href="{{ asset('dashboard/css/lightbox.min.css') }}">
<link rel="stylesheet" href="{{ asset('dashboard/css/zuck.min.css') }}">
<link rel="stylesheet" href="{{ asset('dashboard/css/snapssenger.css') }}">
<div class="row">	
	<div class="mid_wrap col-xl-9 col-lg-9 col-md-9 col-sm-12">
		<div class="row">
			<div class="col-md-12">
				<div class="story_post">
					<div id="story_board" >
						<div class="story_item">
							<div class="story_item_box">
								<div class="post_box">
									<img src="@if(!empty(auth()->user()->image)){{asset(auth()->user()->image)}}@else{{asset('dist/images/user_icon.png')}}@endif" alt="...">
									<div class="create_story">
										<span>  <a href="{{ url('user/create_story') }}"><i class="fa fa-plus-circle"></i></a></i></span>
										<p>Create</p>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div id="stories" class="storiesWrapper snapssenger owl-carousel"></div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-12">
				<div class="main-center-data">
					<h3 class="display-username">Hi, <?php echo $name;?></h3>
				</div>
				<div class="what_in_mind">
					<button type="button" class="form-control" data-toggle="modal" data-target="#staticBackdrop">
						What's on your mind, <?php echo $name;?>?
					</button>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4 col-sm-6">
				<a href="">
					<div class="dash_red_box show-section-details">
						<div class="first_box">
							<img src="{{ asset('dashboard/img/health_tracker.svg') }}" alt="icon">
							<span>Health Tracker</span>
						</div>
						<span></span>
					</div>
				</a>
			</div>
			<div class="col-md-4 col-sm-6">
				<a href="">
					<div class="dash_red_box show-section-details">
						<div class="first_box">
							<img src="{{ asset('dashboard/img/booking.svg') }}" alt="icon">
							<span>My Schedule</span>
						</div>
						<span></span>
					</div>
				</a>
			</div>
			<div class="col-md-4 col-sm-6">
				<a href="">
					<div class="dash_red_box show-section-details">
						<div class="first_box">
							<img src="{{ asset('dashboard/img/favoratie.png') }}" alt="icon">
							<span>My Favorites</span>
						</div>
						<span></span>
					</div>
				</a>
			</div>
		</div>

		<div class="row" id="all_dashboard_data"></div>
	</div>
	@include('front.supporters.right_sidebar')
</div>	
		

    <!-- What_in_Mind_popup -->

    <div class="modal fade what_in_mind_popup" id="staticBackdrop" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">

        <div class="modal-dialog modal-dialog-centered">

            <div class="modal-content create_post">

                <form id="add_post" enctype="multipart/form-data">

					<div class="crtpost_head">

						<h5>Create Post</h5>

						<button type="button" class="close" data-dismiss="modal" aria-label="Close">

							<span aria-hidden="true">&times;</span>

						</button>

						<div class="post_cb">

							<div class="post_by">

								<div class="poster_img"><img src="@if(!empty(auth()->user()->image)){{asset(auth()->user()->image)}}@else{{asset('dist/images/user_icon.png')}}@endif" alt="..."></div>

								<div class="poster_info">

									<div class="poster_name"><b><?php echo $name;?></b></div>

									<div class="post_visible_to">

										<select class="custom-select custom-select-sm" name="post_access" id="post_access">
											<option value="1" selected>&#127760; Public</option>
											<option value="2">&#128101; Friends</option>
											<option value="3">&#128100; Only Me</option>
										</select>

									</div>

								</div>

							</div>

						</div>

					</div>

					<div class="crtpost_body">

						<div class="post_type">
							<textarea placeholder="Whats on your mind, <?php echo $name;?>?" name="post_content" id="emojionearea"  rows="5"></textarea>
							<!--<input type="text" id="emojionearea1" value=""/>-->
							<!--<textarea placeholder="Whats on your mind, <?php echo $name;?>?" name="post_content" id="emojionearea"  rows="5"></textarea>-->

							<div id="galeria"></div>

							<div class="add_to_post mt-2">

								<span>Add to your Post</span>

								<div class="add_btns_grp">

									<img src="{{ asset('dashboard/img/smiles/atp_img.svg') }}" alt="...">

										<input type="file" id="adicionafoto" multiple onchange="previewMultiple(event)" name="adicionafoto[]">
										<span id="filename"></span>
								</div>

							</div>

							<button type="submit" name="postBtn" id="postBtn" class="btn btn-secondary w-100 mt-2">Post</button>

						</div>

					</div>

				</form>

                <div class="crtpost_footer">

                    <ul>

                        <li><a href=""><i class="fa fa-plus-circle"></i> Create Status</a></li>

                        <li><a href=""><i class="fa fa-comment"></i> Create Blogs</a></li>

                        <li><a href=""><i class="fa fa-heartbeat"></i> Health Goals</a></li>

                        <li><a href=""><i class="fa fa-play-circle"></i> Go Live</a></li>

                    </ul>

                </div>

            </div>

        </div>

    </div>

	<!---Like Detail popups--->

	<div class="modal fade likes_popup" id="likeBackdrop" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="likeBackdropLabel" aria-hidden="true">

		<div class="modal-dialog modal-dialog-centered">

		  <div class="modal-content">

			<div class="modal-header">

				<ul class="nav nav-pills" id="pills-tab" role="tablist">

					<li class="nav-item" role="presentation">

					  <a class="nav-link active" id="pills-all-tab" data-toggle="pill" href="#pills-all" role="tab" aria-controls="pills-all" aria-selected="true">All</a>

					</li>

					<li class="nav-item" role="presentation">

					  <a class="nav-link" id="pills-like-tab" data-toggle="pill" href="#pills-like" role="tab" aria-controls="pills-like" aria-selected="false">

						<img src="{{ asset('dashboard/img/smiles/like.svg') }}" alt=""> Like

					  </a>

					</li>

					<li class="nav-item" role="presentation">

					  <a class="nav-link" id="pills-happy-tab" data-toggle="pill" href="#pills-happy" role="tab" aria-controls="pills-happy" aria-selected="false">

						<img src="{{ asset('dashboard/img/smiles/in-love.svg') }}" alt=""> In Love

					  </a>

					</li>

					<li class="nav-item" role="presentation">

						<a class="nav-link" id="pills-in-love-tab" data-toggle="pill" href="#pills-in-love" role="tab" aria-controls="pills-in-love" aria-selected="false">

							<img src="{{ asset('dashboard/img/smiles/crying.svg') }}" alt=""> Sad

						</a>

					</li>

					<li class="nav-item" role="presentation">

						<a class="nav-link" id="pills-angry-tab" data-toggle="pill" href="#pills-angry" role="tab" aria-controls="pills-angry" aria-selected="false">

							<img src="{{ asset('dashboard/img/smiles/angry.svg') }}" alt=""> Angry

						</a>

					</li>

				</ul>

				<button type="button" class="close" data-dismiss="modal" aria-label="Close">

					<span aria-hidden="true">&times;</span>

				</button>

			</div>

			<div class="modal-body">

				<div class="tab-content" id="pills-tabContent">

					<div class="tab-pane fade show active" id="pills-all" role="tabpanel" aria-labelledby="pills-all-tab">

						<ul class="liker_list" id="all_like_data">

							

						</ul>

					</div>

					<div class="tab-pane fade" id="pills-like" role="tabpanel" aria-labelledby="pills-like-tab">

						<ul class="liker_list" id="thumb_data">

							

						</ul>

					</div>

					<div class="tab-pane fade" id="pills-in-love" role="tabpanel" aria-labelledby="pills-in-love-tab">

						<ul class="liker_list" id="sad_data">

							

						</ul>

					</div>

					<div class="tab-pane fade" id="pills-happy" role="tabpanel" aria-labelledby="pills-happy-tab">

						<ul class="liker_list" id="love_data">

							

						</ul>

					</div>

					<div class="tab-pane fade" id="pills-angry" role="tabpanel" aria-labelledby="pills-angry-tab">

						<ul class="liker_list" id="angry_data">

							

							

						</ul>

					</div>

				</div>

			</div>

		  </div>

		</div>

	  </div>	

	<!----End Here------->


	<!-----Gallery Image Preview strat------->
	<div id="photo_preview" style="display:none">
        <div class="photo_wrp">
            <a href="#" class="close" onclick="closePhotoPreview()"><i class="fa fa-times"></i></a>
			
			<div class="post_lightbox_wrapper">
				<div class="post_photo_slides">
					<div class="pleft"></div>
				</div>
				<div class="post_chat_box">
					<div class="comment_right">
						<div class="cr_head sdw">
							<div class="cr_img"><img src="/{{auth()->user()->image}}" alt=""/></div>
							<div class="cr_info">
								<div><strong>{{auth()->user()->name}}</strong> uploaded <span id="media_count"></span> photos</div>
								<small><span id="media_date"></span></small>
							</div>
						</div>
						<div class="post_chat_comment_area">
                            <ul class="comment_list" id="comment_hub">
                                
                            </ul>
						</div>
						<div class="post_chat_comment_foot">
							<form id="media_comment">
								<div class="input_chat_box">
									<textarea class="input_chat" name="input_chat" id="input_chat"></textarea>
									<input type="hidden" id="mediaId" name="mediaId" value="">
									<input type="hidden" id="postMediaId" name="postMediaId" value="">
									<button type="submit" class="btn_post">Send</button>
								</div>
							</form>
						</div>
						
					</div>
				</div>
			</div>
            
        </div>
    </div>

	<!-------End Here---------->

@endsection



@push("scripts")

	<script src="https://www.jqueryscript.net/demo/Fullscreen-Responsive-Gallery-Lightbox-Plugin-SimpleLightbox/dist/simpleLightbox.js"></script>
    <!--<script src="https://cdnjs.cloudflare.com/ajax/libs/emojionearea/3.4.2/emojionearea.min.js" integrity="sha512-hkvXFLlESjeYENO4CNi69z3A1puvONQV5Uh+G4TUDayZxSLyic5Kba9hhuiNLbHqdnKNMk2PxXKm0v7KDnWkYA==" crossorigin="anonymous"></script>-->
	<script type="text/javascript" src="https://cdn.rawgit.com/mervick/emojionearea/master/dist/emojionearea.min.js"></script>
	<script src="{{asset('assets/js/owl.carousel.js')}}" type="text/javascript"></script>
	<script src="{{ asset('dashboard/js/zuck.min.js') }}"></script>
	<script src="{{ asset('dashboard/js/script.js') }}"></script>
	<script>
		$('.new_gal a').simpleLightbox();
	</script>
	<script type="text/javascript">
	
    $(document).ready(function() {
      	$('#stories').owlCarousel({
			loop: false,
			margin: 0,
			responsiveClass: true,
			nav: true,
			autoplay: false,
				responsive: {
					0: {
						items: 3,
					},
					400: {
						items: 4,
					},
					600: {
						items: 5,
					},
					800: {
						items: 4,
					}
				}
      	});
    });
	  

	  var allStoryData = [

	  

	  <?php foreach($storyData as $key => $userdata){?>

          Zuck.buildTimelineItem(

		  "{{ $userdata['user_id'] }}", 

            "{{ env('APP_URL').'/'.$userdata['image'] }}",

            "{{ $userdata['username'] }}",

            "",

            timestamp(),

            [

			<?php foreach($userdata['stories'] as $k => $s){?>

              ["{{ $s->id }}", "photo", 3, "{{ env('APP_URL').'/'.$s->story_bg }}", "{{ env('APP_URL').'/'.$s->story_bg }}", '', false, false, timestamp()],

			<?php } ?>

            ]

		  ),

		  

	  <?php } ?>

        ];

	  //console.log(timestamp());

	  //console.log('allStoryData',allStoryData);

      var currentSkin = getCurrentSkin();

      var stories = new Zuck('stories', {

        backNative: true,

        previousTap: true,

        skin: currentSkin['name'],

        autoFullScreen: currentSkin['params']['autoFullScreen'],

        avatars: currentSkin['params']['avatars'],

        paginationArrows: currentSkin['params']['paginationArrows'],

        list: currentSkin['params']['list'],

        cubeEffect: currentSkin['params']['cubeEffect'],

        localStorage: true,

        stories: allStoryData

      });

    </script>

	<script>



		$(window).on('load', function() {

			load_page_data();

		})

		$(document).ready(function() {
			$("#emojionearea").emojioneArea({
				pickerPosition: "bottom",
				tonesStyle: "radio"
			});

			$("#input_chat").emojioneArea({
				pickerPosition: "top",
				tonesStyle: "radio"
				
			});

		});
		
		$(document).ready(function() {

			$(document).on('click', '.show_more_comment', function(e) {

				$(this).hide();
				$(this).siblings(".show_more_text").show('slide');
				$(this).siblings(".show_less").show();				

			});

			$(document).on('click', '.show_less', function(e) {
				
				$(this).siblings(".show_more_text").hide('slide');
				$(this).hide();	
				$(this).siblings(".show_more_comment").show();

			});
			
			/******Open gallery preview********/
			$(document).on('click', '.post_imgs .photo img', function(event) {
				if (event.preventDefault) event.preventDefault();
				var post_id = $(this).attr("data-postid");
        		getPhotoPreviewAjx($(this).attr('id'),post_id);

			});

			// close photo preview block
			function closePhotoPreview() {
				$('#photo_preview').hide();
				$('#photo_preview .pleft').html('empty');
				$('#photo_preview .pright').html('empty');
			};

			// display photo preview block
			function getPhotoPreviewAjx(id,post_id) {

				$.ajax({
					headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					},
					data: {"id":id, "post_id":post_id},
					url: "{{ route('get_post_images') }}",
					type: "POST",
					success: function (response) {
						if(response['status'] == 'success'){
							$('#photo_preview .pleft').html(response['data1']);
							$('#media_count').text(response['data3']);
							$('#media_date').text(response['data4']);
							$('#comment_hub').html('');
            				$('#comment_hub').html(response['data2']);
            				$('#photo_preview').show();	
							$("#mediaId").val('');
							$("#postMediaId").val('');
							$("#mediaId").val(id);
							$("#postMediaId").val(post_id);
						}
						else{
							errorAlert(response['message'],2000,'top-right');
						}
					}
				});

			}

			
			
			$("#media_comment").submit(function(e) {
				var profilePic = '<?php echo auth()->user()->image ?>';
				var name = '<?php echo auth()->user()->name ?>';
				e.preventDefault();
					$.ajax({
						headers: {
							'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
						},
						data: {"media_id":$("#mediaId").val(), "post_id":$("#postMediaId").val(), "comment": $("#input_chat").val()},
						url: "{{ route('store_media_comment') }}",
						type: "POST",
						success: function (response) {
							if(response['status'] == 'success'){
								var html = '<li><div class="cr_head">'+
                                        '<div class="cr_img"><img src="/'+profilePic+'" alt="'+name+'"></div>'+
                                        '<div class="cr_info">'+
                                            '<div>'+
                                                '<strong>'+name+'</strong> '+
                                                '<p class="comment_txt">'+response['comment']+'</p>'+
                                            '</div>'+
                                            '<small class="like-btn">'+response['commentdate']+'</small>'+
                                        '</div>'+
                                    '</div></li>';
								$(".post_chat_comment_area ul").append(html);
								$(".emojionearea-editor").html('');	
								
							}
							else{
								errorAlert(response['message'],2000,'top-right');
							}
						}
					});
			});



			/*********End Here**********/
			

        });

		

		/****Upload dashboard data******/

		function load_page_data()
		{
			$.ajax({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				data: {},
				url: "{{ route('load_data') }}",
				type: "GET",
				success: function (response) {
					if(response['status'] == 'success'){
						$("#all_dashboard_data").html(response['data']);
						/********Bind the click event on html response*******/
						$(".reactions-box li").bind("click",function(){
							var flagType = $(this).data("reaction");
							flagType = (flagType=="Like") ? "like_flag" : ((flagType=="Love") ? "love_flag" : ((flagType=="Sad") ? "sad_flag" : "angary_flag" ));
							var postId   = $(this).data("postid");
							var reactionFlag = $(this).data("likeflag");
							reactionFlag = (reactionFlag == 1) ? 0 : 1;  	//1= Post alreday liked,0= post need to unliked
							$.ajax({
								headers: {
									'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
								},
								data: {"post_id":postId, "likeFlag": reactionFlag, "flagType":flagType},
								url: "{{ route('add_post_like') }}",
								type: "POST",
								success: function (response) {
									if(response['status'] == 'success'){
										callProgressBar();
										if($(".post_like_"+postId).hasClass('like')){
										}else if($(".post_like_"+postId).hasClass('no_like')){
											$(".post_like_"+postId).append(",You");
										}else{
											$(".like_user_name_"+postId).append('<span> <a href="javascript:void(0)" class="like post_like_'+postId+'" onclick="getLikeDetail('+postId+')">You</a><span>');
										}
									}

									else{
										errorAlert(response['message'],2000,'top-right');

									}

								},

								error: function (data) {

									//$(".loader").css('display', 'none');

									console.log(data);

									let errors = data.responseJSON.errors;

									$.each(errors, function(key, value) {

										errorAlert(value[0],3000,'top-right');

									});

								}

							});	

						});

						/**************End Here*****************/

					}

					else{

						errorAlert(data.status,2000,'top-right');

					}

				},

				error: function (data) {

					$(".loader").css('display', 'none');

					let errors = data.responseJSON.errors;

					$.each(errors, function(key, value) {

						errorAlert(value[0],3000,'top-right');

					});

				}

			});

		}



		/***Upload media at post***/

		function previewMultiple(event){

			var saida = document.getElementById("adicionafoto");

			var quantos = saida.files.length;

			for(i = 0; i < quantos; i++){

				var urls = URL.createObjectURL(event.target.files[i]);

				document.getElementById("galeria").innerHTML += '<img src="'+urls+'">';

			}

		}



		$("#add_post").submit(function(e) {

			e.preventDefault();
			var formData = new FormData(this);

			$.ajax({

				headers: {

					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

				},

				data: formData,

				url: "{{ route('social_post') }}",

				type: "POST",

				cache:false,

				contentType: false,

				processData: false,

				success: function (response) {

					//$(".loader").css('display', 'none');

					if(response['status'] == 'success'){

						$("#staticBackdrop").modal('hide');
						callProgressBar();
						//successAlert(response['message'],2000,'top-right');

						setTimeout(function(){
							$('#add_post').trigger("reset");
							$('#add_post')[0].reset();
							$('.emojionearea-editor').html('');
							$('#galeria').html('');
							load_page_data();
						}, 2000);

					}

					else{

						errorAlert(response['message'],2000,'top-right');

					}

				},

				error: function (data) {

					//$(".loader").css('display', 'none');

					let errors = data.responseJSON.errors;

					$.each(errors, function(key, value) {

						errorAlert(value[0],3000,'top-right');

					});

				}

			});

		});



		/*****Add Comment******/

		function callComment(postid)

		{
			
			var postid = postid;

			$("#add_post_comment_"+postid).unbind('keypress').bind('keypress', function (e) {
				if(e.which == 13) {
					//$("#form_post_comment_"+postid).submit(function(e) {
						e.preventDefault();

						//$(".loader").css('display', 'flex');
						$.ajax({

							headers: {

								'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

							},

							data: $("#form_post_comment_"+postid).serialize(),

							url: "{{ route('add_post_comment') }}",

							type: "POST",

							success: function (response) {
								//$(".loader").css('display', 'none');
								if(response['status'] == 'success'){
									callProgressBar();
									
									var com_text = $("#add_post_comment_"+postid).val();
									var com_imag ="{{ env('APP_URL')}}/{{auth()->user()->image }}";
									var com_name ="{{ auth()->user()->name }}";
									
									var comment_count = parseInt($("#comment_count_"+postid).text());
									comment_count +=1;
									$("#comment_count_"+postid).text(comment_count);
									
									if($(".s_m_"+postid).hasClass("show_less")){
										
										$('.show_more'+postid).hide();
										$('.show_more'+postid).siblings(".show_more_text").show('slide');
										$('.show_more'+postid).siblings(".show_less").show();
				
										$(".s_m_"+postid).before('<div class="media show_more_text" ><div class="poster_img"><img src="'+com_imag+'" alt="'+com_name+'"></div><div class="media-body"><h6 class="mt-0">'+com_name+'</h6><p>'+com_text+'</p></div></div>');
										
									}else{
										$("#collapseComment_"+postid).find('.comment_row').append('<div class="media"><div class="poster_img"><img src="'+com_imag+'" alt="'+com_name+'"></div><div class="media-body"><h6 class="mt-0">'+com_name+'</h6><p>'+com_text+'</p></div></div>');
										
									}
									$("#add_post_comment_"+postid).val('');	
									//$("#form_post_comment_"+postid).reset();
									
									
								}

								else{

									errorAlert(response['message'],2000,'top-right');

								}

								event.stopPropagation();

							},

							error: function (data) {

								//$(".loader").css('display', 'none');

								let errors = data.responseJSON.errors;

								$.each(errors, function(key, value) {

									errorAlert(value[0],3000,'top-right');

								});

							}

						});

						

				//});

				}

			});

		}



		/****Do like on post*****/

		function doLikePost(id, like_flag)
		{   
		    var this_ = $(this);
            let likeFlag = (like_flag == 1) ? 0 : 1;  //1= Post alreday liked,0= post need to unliked
			$.ajax({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				data: {"post_id":id, "likeFlag": likeFlag, "flagType":"like_flag"},
				url: "{{ route('add_post_like') }}",
				type: "POST",
				success: function (response) {
					if(response['status'] == 'success')
					{
						callProgressBar();
						if($(".post_like_"+id).hasClass('like')){
							
						}
						else if($(".post_like_"+id).hasClass('no_like')){
							$(".post_like_"+id).append(",You");
						}
						else
						{
							$(".like_user_name_"+id).append('<span> <a href="javascript:void(0)" class="like post_like_'+id+'" onclick="getLikeDetail('+id+')">You</a><span>');
						}
					}
					else
					{
						errorAlert(response['message'],2000,'top-right');
					}
				},
				error: function (data) {
					let errors = data.responseJSON.errors;
					$.each(errors, function(key, value) {
						errorAlert(value[0],3000,'top-right');
					});
				}
			});

		}

		

		/*******Get Comment Details**********/

		function getLikeDetail(id)

		{
			
			$("#likeBackdrop").modal('show');

			//$(".loader").css('display', 'block');

			$.ajax({

				headers: {

					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

				},

				data: {},

				url: "{{ env('APP_URL').'/get_post_like' }}"+'/'+id,

				success: function (response) {

					//$(".loader").css('display', 'none');

					var data = jQuery.parseJSON(response); 

					if(data.status == 'success'){
						console.log(data);
						var htmlData='';

						var htmlData1='';

						var htmlData2='';

						var htmlData3='';

						var htmlData4='';

						$("#all_like_data").html('');

						$("#thumb_data").html('');

						$("#sad_data").html('');

						$("#love_data").html('');

						$("#angry_data").html('');

						

						/**append all type reaction in modal body****/
						$.each( data.all, function( key, value ) {
							var cimage = "{{ env('APP_URL').'/' }}"+value.image;
							var imgPath = (value.flag==1) ? "{{ env('APP_URL').'/dashboard/img/smiles/like.svg' }}" : ((value.flag==2) ? "{{ env('APP_URL').'/dashboard/img/smiles/happy.svg' }}" : ((value.flag==3) ? "{{ env('APP_URL').'/dashboard/img/smiles/in-love.svg' }}" : "{{ env('APP_URL').'/dashboard/img/smiles/angry.svg' }}" ));
							htmlData = htmlData + '<li>'+
										'<div class="lkr_info">'+
											'<div class="lkr_img_box">'+
												'<div class="lkr_rection_icon">'+
													'<img src="'+imgPath+'" alt="">'+
												'</div>'+
												'<img src="'+cimage+'" alt="'+value.likeBy+'" class="liker_img">'+
											'</div>'+
											'<span class="liker_name">'+value.likeBy+'</span>'+
										'</div>'
										// <a href="javascript:void(0)" class="btn btn-outline-info"><i class="fa fa-user-plus"></i> Follow</a>
									'</li>';
						});

						/**append all thumb type reaction in modal body****/
						$.each( data.thumb, function( key, value ) {
							var cimage = "{{ env('APP_URL').'/' }}"+value.image;
							var imgPath = "{{ asset('dashboard/img/smiles/like.svg') }}";
							// htmlData1 = htmlData1 + '<li><div class="lkr_info"><div class="lkr_img_box"><div class="lkr_rection_icon"><img src="'+imgPath+'" alt=""></div><img src="'+cimage+'" alt="'+value.likeBy+'" class="liker_img"></div><span class="liker_name">'+value.likeBy+'</span></div><a href="javascript:void(0)" class="btn btn-outline-info"><i class="fa fa-user-plus"></i> Follow</a></li>';
							htmlData1 = '<li>'+
											'<div class="lkr_info">'+
												'<div class="lkr_img_box">'+
													'<div class="lkr_rection_icon">'+
														'<img src="'+imgPath+'" alt="">'+
													'</div>'+
													'<img src="'+cimage+'" alt="'+value.likeBy+'" class="liker_img">'+
												'</div>'+
												'<span class="liker_name">'+value.likeBy+'</span>'+
											'</div>'+
											// '<a href="javascript:void(0)" class="btn btn-outline-info"><i class="fa fa-user-plus"></i> Follow</a>'+
										'</li>';
						});

						/**append all sad type reaction in modal body****/
						$.each( data.sad, function( key, value ) {
							var cimage = "{{ env('APP_URL').'/' }}"+value.image;
							var imgPath = "{{ asset('dashboard/img/smiles/in-love.svg') }}";
							// htmlData2 = htmlData2 + '<li><div class="lkr_info"><div class="lkr_img_box"><div class="lkr_rection_icon"><img src="'+imgPath+'" alt=""></div><img src="'+cimage+'" alt="'+value.likeBy+'" class="liker_img"></div><span class="liker_name">'+value.likeBy+'</span></div><a href="javascript:void(0)" class="btn btn-outline-info"><i class="fa fa-user-plus"></i> Follow</a></li>';
							htmlData2 = '<li>'+
											'<div class="lkr_info">'+
												'<div class="lkr_img_box">'+
													'<div class="lkr_rection_icon">'+
														'<img src="'+imgPath+'" alt="">'+
													'</div>'+
													'<img src="'+cimage+'" alt="'+value.likeBy+'" class="liker_img">'+
												'</div>'+
												'<span class="liker_name">'+value.likeBy+'</span>'+
											'</div>'+
											// '<a href="javascript:void(0)" class="btn btn-outline-info"><i class="fa fa-user-plus"></i> Follow</a>'+
										'</li>';
						});

						/**append all love type reaction in modal body****/
						$.each( data.love, function( key, value ) {
							var cimage = "{{ env('APP_URL').'/' }}"+value.image;
							var imgPath = "{{ asset('dashboard/img/smiles/happy.svg') }}";
							// htmlData3 = htmlData3 + '<li><div class="lkr_info"><div class="lkr_img_box"><div class="lkr_rection_icon"><img src="'+imgPath+'" alt=""></div><img src="'+cimage+'" alt="'+value.likeBy+'" class="liker_img"></div><span class="liker_name">'+value.likeBy+'</span></div><a href="javascript:void(0)" class="btn btn-outline-info"><i class="fa fa-user-plus"></i> Follow</a></li>';
							htmlData3 = '<li>'+
											'<div class="lkr_info">'+
												'<div class="lkr_img_box">'+
													'<div class="lkr_rection_icon">'+
														'<img src="'+imgPath+'" alt="">'+
													'</div>'+
													'<img src="'+cimage+'" alt="'+value.likeBy+'" class="liker_img">'+
												'</div>'+
												'<span class="liker_name">'+value.likeBy+'</span>'+
											'</div>'+
											// '<a href="javascript:void(0)" class="btn btn-outline-info"><i class="fa fa-user-plus"></i> Follow</a>'+
										'</li>';
						});

						/**append all angey type reaction in modal body****/
						$.each( data.angry, function( key, value ) {
							var cimage = "{{ env('APP_URL').'/' }}"+value.image;
							var imgPath = "{{ asset('dashboard/img/smiles/angry.svg') }}";
							// htmlData4 = htmlData4 + '<li><div class="lkr_info"><div class="lkr_img_box"><div class="lkr_rection_icon"><img src="'+imgPath+'" alt=""></div><img src="'+cimage+'" alt="'+value.likeBy+'" class="liker_img"></div><span class="liker_name">'+value.likeBy+'</span></div><a href="javascript:void(0)" class="btn btn-outline-info"><i class="fa fa-user-plus"></i> Follow</a></li>';
							htmlData4 = '<li>'+
											'<div class="lkr_info">'+
												'<div class="lkr_img_box">'+
													'<div class="lkr_rection_icon">'+
														'<img src="'+imgPath+'" alt="">'+
													'</div>'+
													'<img src="'+cimage+'" alt="'+value.likeBy+'" class="liker_img">'+
												'</div>'+
												'<span class="liker_name">'+value.likeBy+'</span>'+
											'</div>'+
											// '<a href="javascript:void(0)" class="btn btn-outline-info"><i class="fa fa-user-plus"></i> Follow</a>'+
										'</li>';
						});

						

						$("#all_like_data").html(htmlData);

						$("#thumb_data").html(htmlData1);

						$("#sad_data").html(htmlData2);

						$("#love_data").html(htmlData3);

						$("#angry_data").html(htmlData4);

					}else{
						//console.log(data);
						errorAlert(data.status,2000,'top-right');

					}

				},

				error: function (data) {

					$(".loader").css('display', 'none');

					let errors = data.responseJSON.errors;

					$.each(errors, function(key, value) {

						errorAlert(value[0],3000,'top-right');

					});

				}

			});

		}

		

		/**********Load more post on dashborad**********/

		function loadMorePost(offset1,offset2)

		{

			//$(".loader").css('display', 'block');

			$.ajax({

				headers: {

					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

				},

				data: {"offset1":offset1,"offset2":offset2},

				url: "{{ env('APP_URL').'/post' }}"+'/0/1',

				type: "GET",

				success: function (response) {

					//$(".loader").css('display', 'none');

					if(response['status'] == 'success'){

						$("#allPost").append(response['data']);

						$("#offset1").val(response['nxtOffset']);

						$("#offset2").val(offset2);

						callProgressBar();

						/********Bind the click event on html response*******/

						$(".reactions-box li").bind("click",function(){

							var flagType = $(this).data("reaction");
							//alert('FDFDFDF');
							flagType = (flagType=="Like") ? "like_flag" : ((flagType=="Love") ? "love_flag" : ((flagType=="Sad") ? "sad_flag" : "angary_flag" ));

							var postId   = $(this).data("postid");

							var reactionFlag = $(this).data("likeflag");

							reactionFlag = (reactionFlag == 1) ? 0 : 1;  //1= Post alreday liked,0= post need to unliked

							$.ajax({

								headers: {

									'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

								},

								data: {"post_id":postId, "likeFlag": reactionFlag, "flagType":flagType},

								url: "{{ route('add_post_like') }}",

								type: "POST",

								success: function (response) {

									//$(".loader").css('display', 'none');

									if(response['status'] == 'success'){
										callProgressBar();
										if($(".post_like_"+postId).hasClass('like')){
											
										}else if($(".post_like_"+postId).hasClass('no_like')){
											$(".post_like_"+postId).append(",You");
										}else{
											$(".like_user_name_"+postId).append('<span> <a href="javascript:void(0)" class="like post_like_'+postId+'" onclick="getLikeDetail('+postId+')">You</a><span>');
											
										}

										/*successAlert(response['message'],2000,'top-right');

										setTimeout(function(){

											window.location = response['redirect'];

										}, 2000);*/

									}

									else{

										errorAlert(response['message'],2000,'top-right');

									}

								},

								error: function (data) {

									//$(".loader").css('display', 'none');

									//console.log(data);

									let errors = data.responseJSON.errors;

									$.each(errors, function(key, value) {

										errorAlert(value[0],3000,'top-right');

									});

								}

							});	

						});

						/**************End Here*****************/

					}

					else{

						errorAlert(data.status,2000,'top-right');

					}

				},

				error: function (data) {

					//$(".loader").css('display', 'none');
					//alert('fdfdf');
					let errors = data.responseJSON.errors;
				
					errorAlert('No Record Found.',2000,'top-right');
					
					$.each(errors, function(key, value) {

						errorAlert(value[0],3000,'top-right');

					});

				}

			});

		}

		

		$(document).ready(function() {

			$(".reactions-box li").on("click",function(){

				//alert("coming here gg");

				var flagType = $(this).data("reaction");

				flagType = (flagType=="Like") ? "like_flag" : ((flagType=="Love") ? "love_flag" : ((flagType=="Sad") ? "sad_flag" : "angary_flag" ));

				var postId   = $(this).data("postid");

				var reactionFlag = $(this).data("likeflag");

				reactionFlag = (reactionFlag == 1) ? 0 : 1;  //1= Post alreday liked,0= post need to unliked

				

				$.ajax({

					headers: {

						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

					},

					data: {"post_id":postId, "likeFlag": reactionFlag, "flagType":flagType},

					url: "{{ route('add_post_like') }}",

					type: "POST",

					success: function (response) {

						//$(".loader").css('display', 'none');

						if(response['status'] == 'success'){
							
							callProgressBar();
							if($(".post_like_"+postId).hasClass('like')){
								
							}else if($(".post_like_"+postId).hasClass('no_like')){
								$(".post_like_"+postId).append(",You");
							}else{
								$(".like_user_name_"+postId).append('<span> <a href="javascript:void(0)" class="like post_like_'+postId+'" onclick="getLikeDetail('+postId+')">You</a><span>');
								
							}

							/*successAlert(response['message'],2000,'top-right');

							setTimeout(function(){

								window.location = response['redirect'];

							}, 2000);*/

						}

						else{

							errorAlert(response['message'],2000,'top-right');

						}

					},

					error: function (data) {

						//$(".loader").css('display', 'none');

						console.log(data);

						let errors = data.responseJSON.errors;

						$.each(errors, function(key, value) {

							errorAlert(value[0],3000,'top-right');

						});

					}

				});	

			});

		});

		

		

		

		function getStories()

		{

			var currentSkin = getCurrentSkin();

			/****Get stories****/

			$.ajax({

				headers: {

					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

				},

				data: {},

				url: "{{ env('APP_URL').'/post' }}"+'/0/1',

				type: "GET",

				success: function (response) {

					//$(".loader").css('display', 'none');

					if(response['status'] == 'success'){

						$("#allPost").append(response['data']);

						$("#offset1").val(response['nxtOffset']);

						$("#offset2").val(offset2);

					}

					else{

						errorAlert(data.status,2000,'top-right');

					}

				},

				error: function (data) {

					$(".loader").css('display', 'none');

					let errors = data.responseJSON.errors;

					$.each(errors, function(key, value) {

						errorAlert(value[0],3000,'top-right');

					});

				}

			});

			/****************************************/

			var stories = new Zuck('stories', {

			backNative: true,

			previousTap: true,

			skin: currentSkin['name'],

			autoFullScreen: currentSkin['params']['autoFullScreen'],

			avatars: currentSkin['params']['avatars'],

			paginationArrows: currentSkin['params']['paginationArrows'],

			list: currentSkin['params']['list'],

			cubeEffect: currentSkin['params']['cubeEffect'],

			localStorage: true,

			stories: [

			  Zuck.buildTimelineItem(

				"ramon", 

				"https://raw.githubusercontent.com/ramon82/assets/master/zuck.js/users/1.jpg",

				"Ramon",

				"https://ramon.codes",

				timestamp(),

				[

				  ["ramon-1", "photo", 3, "https://raw.githubusercontent.com/ramon82/assets/master/zuck.js/stories/1.jpg", "https://raw.githubusercontent.com/ramon82/assets/master/zuck.js/stories/1.jpg", '', false, false, timestamp()],

				  ["ramon-2", "video", 0, "https://raw.githubusercontent.com/ramon82/assets/master/zuck.js/stories/2.mp4", "https://raw.githubusercontent.com/ramon82/assets/master/zuck.js/stories/2.jpg", '', false, false, timestamp()],

				  ["ramon-3", "photo", 3, "https://raw.githubusercontent.com/ramon82/assets/master/zuck.js/stories/3.png", "https://raw.githubusercontent.com/ramon82/assets/master/zuck.js/stories/3.png", 'https://ramon.codes', 'Visit my Portfolio', false, timestamp()]

				]

			  ),

			  Zuck.buildTimelineItem(

				"gorillaz",

				"https://raw.githubusercontent.com/ramon82/assets/master/zuck.js/users/2.jpg",

				"Gorillaz",

				"",

				timestamp(),

				[

				  ["gorillaz-1", "video", 0, "https://raw.githubusercontent.com/ramon82/assets/master/zuck.js/stories/4.mp4", "https://raw.githubusercontent.com/ramon82/assets/master/zuck.js/stories/4.jpg", '', false, false, timestamp()],

				  ["gorillaz-2", "photo", 3, "https://raw.githubusercontent.com/ramon82/assets/master/zuck.js/stories/5.jpg", "https://raw.githubusercontent.com/ramon82/assets/master/zuck.js/stories/5.jpg", '', false, false, timestamp()],

				]

			  ),

			  Zuck.buildTimelineItem(

				"ladygaga",

				"https://raw.githubusercontent.com/ramon82/assets/master/zuck.js/users/3.jpg",

				"Lady Gaga",

				"",

				timestamp(),

				[

				  ["ladygaga-1", "photo", 5, "https://raw.githubusercontent.com/ramon82/assets/master/zuck.js/stories/6.jpg", "https://raw.githubusercontent.com/ramon82/assets/master/zuck.js/stories/6.jpg", '', false, false, timestamp()],

				  ["ladygaga-2", "photo", 3, "https://raw.githubusercontent.com/ramon82/assets/master/zuck.js/stories/7.jpg", "https://raw.githubusercontent.com/ramon82/assets/master/zuck.js/stories/7.jpg", 'http://ladygaga.com', false, false, timestamp()],

				]

			  ),

			  Zuck.buildTimelineItem(

				"starboy",

				"https://raw.githubusercontent.com/ramon82/assets/master/zuck.js/users/4.jpg",

				"The Weeknd",

				"",

				timestamp(),

				[

				  ["starboy-1", "photo", 5, "https://raw.githubusercontent.com/ramon82/assets/master/zuck.js/stories/8.jpg", "https://raw.githubusercontent.com/ramon82/assets/master/zuck.js/stories/8.jpg", '', false, false, timestamp()]

				]

			  ),

			  Zuck.buildTimelineItem(

				"riversquomo",

				"https://raw.githubusercontent.com/ramon82/assets/master/zuck.js/users/5.jpg",

				"Rivers Cuomo",

				"",

				timestamp(),

				[

				  ["riverscuomo", "photo", 10, "https://raw.githubusercontent.com/ramon82/assets/master/zuck.js/stories/9.jpg", "https://raw.githubusercontent.com/ramon82/assets/master/zuck.js/stories/9.jpg", '', false, false, timestamp()]

				]

			  )

			]

		  });	

		}


		
		$(document).on('click', '.facebookPostShare', function(e) {
			e.preventDefault();
			var id =$(this).attr('data-id');
		    var post_url ="{{ route('user_publish_post_text') }}";
		    var post_text = $('.fb_post_text_'+id).text();
		    var post_images = $('.fb_images_'+id).val();
		    var fcount = $('.fb_images_'+id).attr('data-fcount');
		   if(post_images !=null){
    	        if(fcount>1){
    	            var post_url ="{{ route('user_publish_post_images') }}"; 
    	        }else{
    	                var post_url ="{{ route('user_publish_post_img') }}";  
    	        }
		   }else{
		       var post_url ="{{ route('user_publish_post_text') }}";
		   }
		    
		    if(id !=''){
		        $(".loader").css('display', 'flex');
				$.ajax({
					headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					},
					data: {'post_images':post_images,'message':post_text},
					url: post_url,
					type: "POST",
					
					success: function (response) {
						
						if(response['status'] == 'success'){
						    //successAlert(response['message'],2000,'top-right');
							callProgressBar();
						    $(".loader").css('display', 'none');
						}else{
						    $(".loader").css('display', 'none');
						   errorAlert('Something went wrong, please try again.',2000,'top-right'); 
						}
					},
					error: function (data) {
						
						if(data.responseJSON.errors) {
						    $(".loader").css('display', 'none');
						    errorAlert('Something went wrong, please try again.',2000,'top-right');
						}
					
					}
				});
			}
		});
    </script>

	

@endpush

