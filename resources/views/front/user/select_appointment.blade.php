@extends('front.dashboard_layout.app')

@section('content')

	<script>
		var msgs = <?php echo json_encode($location) ?>;
		var visit_type = <?php echo $online_visit->visit_type ?>;
	</script>
	<script src="{{asset('dashboard/js/timeslot.js')}}"></script>
	<!--<div class="loader" style="display: none;">-->
 <!--       <img src="{{asset('assets/images/loading.gif')}}" alt="loading">-->
 <!--   </div>-->
	<div class="col-xl-9 col-lg-9 col-md-12">
		<div class="main-center-data">
			<h3 class="display-username mt-15">Select Appointment of <b>{{ $provider->name }}</b> for {{ $service->name }} treatment</h3>
			
			<div id="picker"></div>
			
			<div class="my-selection">
				<p>Selected date: <span id="selected-date"></span></p>
				<p>Selected time: <span id="selected-time"></span></p>
			</div>
	
		</div>
	</div>
	
    <script type="text/javascript">
	
		var provider_id = {{ $provider->id }};
		var online_visit_id = {{ $online_visit->id }};
		
		//console.log(<?php echo json_encode($timeslots,true);?>);
		$(document).ready(function() {
			$('[data-toggle="tooltip"]').tooltip(); 
		});

		$(document).ready(function() {
			$('.myc-available-time').click(function(e) {  
				var date = $(this).data('date');
            var time = $(this).data('time');
            var tmp = date + ' ' + time;
			//console.log('date time='+tmp);
			});
		});

        (function($) {

			/*$(this).on('click', '.myc-available-time', function() {
            var date = $(this).data('date');
            var time = $(this).data('time');
            var tmp = date + ' ' + time;
			console.log('date time='+tmp);
        });*/

		
            $('#picker').markyourcalendar({                                                                                                                       
                availability: <?php echo json_encode($timeslots,true);?>,
                startDate: new Date("{{ date('Y-m-d') }}"),
                onClick: function(ev, data) {
                    // data is a list of datetimes
                    var d = data[0].split(' ')[0];
                    var t = data[0].split(' ')[1];
					var choseDate = d.split('-');
                    $('#selected-date').html(choseDate[1] + '-' + choseDate[2] + '-' + choseDate[0]);
                    $('#selected-time').html(t);
					addAppointment(d,t);
                },
                 onClickNavigator: function(ev, instance) {
                    var dateArr = [];
					
					var start_date = formatDate(settings.startDate);
					//arr = getTimeSlots(start_date);
					
									
					
					$.ajax({
						headers: {
							'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
						},
						data: {'start_date':start_date, 'provider_id': provider_id,'online_visit_type':{{ $online_visit->visit_type }}},
						url: "{{ route('user.get_time_slots') }}",
						type: "POST",
						// dataType: 'json',
						success: function (response) {
							$(".loader").css('display', 'none');
							if(response['status'] == 'success'){
								dateArr = JSON.parse(response['data']);
								instance.setAvailability(dateArr);								
							}
						},
						error: function (data) {
							$(".loader").css('display', 'none');
						}
					});					
	
                    
                }
            });
        })(jQuery);
		
		
		
		function formatDate(date) {
			var d = new Date(date),
				month = '' + (d.getMonth() + 1),
				day = '' + d.getDate(),
				year = d.getFullYear();

			if (month.length < 2) 
				month = '0' + month;
			if (day.length < 2) 
				day = '0' + day;

			return [year, month, day].join('-');
		}
		
 		function addAppointment(date,time){
			
			swal({
				title: "Are you sure to choose this appointment time?",
				text: "",
				icon: '',
				buttons: {
				cancel: true,
				delete: 'Yes'
				}
			}).then((isConfirm) => {
				if (!isConfirm) {
					return false;
				} else {			
					$(".loader").css('display', 'flex');
					$.ajax({
						headers: {
							'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
						},
						data: {'appointment_date':date, 'appointment_time':time, 'provider_id': provider_id, 'online_visit_id':online_visit_id},
						url: "{{ route('user.addAppointment') }}",
						type: "POST",
						// dataType: 'json',
						success: function (response) {
							$(".loader").css('display', 'none');
							if(response['status'] == 'success'){
								//successAlert(response['message'],2000,'top-right');
								callProgressBar();
								setTimeout(function(){
									window.location = response['redirect'];							
								}, 1500);
							}else{
								errorAlert(response['message'],3000,'top-right');						
							}
						},
						error: function (data) {
							$(".loader").css('display', 'none');
							let errors = data.responseJSON.errors;
							
							$.each(errors, function(key, value) {
								errorAlert(value[0],3000,'top-right');
							});					
						}
					});
					
				}
			});					
				

		} 
    </script>	
@endsection