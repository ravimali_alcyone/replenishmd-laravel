@extends('front.dashboard_layout.app')

@section('stylesheet')
    <link href="{{asset('dashboard/css/supporters.css')}}" rel="stylesheet" />
@endsection

@section('content')
    <div class="mid_wrap">
        <div class="supperter_mid_wrap">
            <section class="supporters_sec top">
                <div class="title_box">
                    <h6><span>{{ count($followers_data) }}</span> Supporters</h6>
                </div>
                <div class="cards_wrapper owl-carousel" id="supporters">
                    @isset($followers_data)
                        @foreach($followers_data as $key => $value)
                            <div class="card card_{{ $value['id'] }}">
								<a href="@if(auth()->user()->user_role==4){{URL('user/view')}}/{{ $value['id'] }}@else{{URL('provider/view')}}/{{ $value['id'] }}@endif">
                                <div class="img_wrapper"><img src="{{ isset($value['image']) ? '/' : '' }}{{ $value['image'] ?? asset('dashboard/img/default-user-icon.jpg') }}" alt="supporter-1" class="w-100"></div>
                                <div class="text_wrapper">
                                    <h6>{{ $value['name'] }}</h6>
                                    <p class="mb-0"><span>{{ $value['clinical_expertise'] }}</span></p>
                                    <div class="text-center">
                                    <a href="javascript:void(0)" class="following" data-toggle="modal" data-target="#unFollowModal" data="{{ $value['id'] }}"><span class="fa fas fas fa-check"></span> Following</a>
                                    </div>
                                </div>
								</a>
                            </div>
                        @endforeach
                    @endisset
                </div>
            </section>
        </div>
    </div>

    <div class="rigt_sidebar">
        <div class="blog-stories sponsored">
            <h4 class="blog-heading">Sponsored</h4>
            <ul class="m-0 p-0">
                <li class="mt-15">
                    <a href="#">
                        <div class="blog-feature-image">
                            <img src="{{ asset('dashboard/img/sponsor-1.png') }}" alt="sponsor-1" />
                        </div>
                        <div class="blog-extract short-description">
                            <p class="mb-0">Diet Plan according to your BMI</p>
                            <span href="#">eathealthy.com</span>
                        </div>
                    </a>
                </li>
                <li class="mt-15">
                    <a href="#">
                        <div class="blog-feature-image">
                            <img src="{{ asset('dashboard/img/sponsor-2.png') }}" alt="sponsor-2" />
                        </div>
                        <div class="blog-extract short-description">
                            <p class="mb-0">Best Hospital for cancer treatment</p>
                            <span href="#">eathealthy.com</span>
                        </div>
                    </a>
                </li>
                <li class="mt-15">
                    <a href="#">
                        <div class="blog-feature-image">
                            <img src="{{ asset('dashboard/img/sponsor-1.png') }}" alt="sponsor-1" />
                        </div>
                        <div class="blog-extract short-description">
                            <p class="mb-0">Diet Plan according to your BMI</p>
                            <span href="#">eathealthy.com</span>
                        </div>
                    </a>
                </li>
                <li class="mt-15">
                    <a href="#">
                        <div class="blog-feature-image">
                            <img src="{{ asset('dashboard/img/sponsor-2.png') }}" alt="sponsor-2" />
                        </div>
                        <div class="blog-extract short-description">
                            <p class="mb-0">Best Hospital for cancer treatment</p>
                            <span href="#">eathealthy.com</span>
                        </div>
                    </a>
                </li>
            </ul>
        </div>

        <div class="blog-stories mt-15">
            <h4 class="blog-heading">Blog stories</h4>
            <ul class="m-0 p-0">
                <li class="mt-15">
                    <a href="#">
                        <div class="blog-feature-image">
                            <img src="{{ asset('dashboard/img/blog-1.svg') }}" alt="blog" />
                        </div>
                        <div class="blog-extract short-description">
                            Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia.
                        </div>
                    </a>
                </li>

                <li class="mt-15">
                    <a href="#">
                        <div class="blog-feature-image">
                            <img src="{{ asset('dashboard/img/blog-2.svg') }}" alt="blog" />
                        </div>
                        <div class="blog-extract short-description">
                            Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia.
                        </div>
                    </a>
                </li>

                <li class="mt-15">
                    <a href="#">
                        <div class="blog-feature-image">
                            <img src="{{ asset('dashboard/img/blog-3.svg') }}" alt="blog" />
                        </div>
                        <div class="blog-extract short-description">
                            Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia.
                        </div>
                    </a>
                </li>

                <li class="mt-15">
                    <a href="#">
                        <div class="blog-feature-image">
                            <img src="{{ asset('dashboard/img/blog-4.svg') }}" alt="blog" />
                        </div>
                        <div class="blog-extract short-description">
                            Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia.
                        </div>
                    </a>
                </li>
            </ul>
            <div class="see-all mt-30">
                <a href="#">See all Stories <img src="{{ asset('dashboard/img/arrow-forward.svg') }}" alt="arrow-forward"/></a>
            </div>
        </div>

        <div class="advertisment mt-15">
            <img src="{{ asset('dashboard/img/ads.svg') }}" alt="ads" class="img-fluid" />
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="unFollowModal" tabindex="-1" role="dialog" aria-labelledby="unFollowModal" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Confirm to unfollow</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>Are you sure you want to unfollow <span class="username font-weight-bold"></span>?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" onclick="javascript: unfollow(this)">Unfollow</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>

    <input type="hidden" id="pusher_api_key" value="{{ env('PUSHER_APP_KEY') }}">
    <input type="hidden" id="pusher_channel" value="{{ env('PUSHER_APP_CHANNEL') }}">
    <input type="hidden" id="pusher_cluster" value="{{ env('PUSHER_APP_CLUSTER') }}">
@endsection

@push("scripts")
    <script src="//js.pusher.com/3.1/pusher.min.js"></script>
    <script src="{{ asset('dashboard/js/supporter.js') }}"></script>
@endpush