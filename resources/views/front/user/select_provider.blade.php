@extends('front.dashboard_layout.app')

@section('content')

	<div class="">
		<div class="main-center-data visit_overview">
			<div class="provider_display">
				<h4 class="display-username">Select Provider For {{ $service->name }} Treatment -  </h4>
				<div class="form_group">
					<i class="fa fa-search" aria-hidden="true"></i>
					<input type="text" class="form-control" id="by_provider_name" placeholder="Search Name.." oninput="getData();">
				</div>		
			</div>
			
			<div class="row mt-15">
				<div class="col-xl-12 col-lg-12 col-md-12">
					<h5>Filter</h5>
					<div class="filters_wrapper">
						<div class="data-list-filters">
							<span class="filter_heading">By Distance</span>
							<select id="by_distance" class="custom-select select2" onchange="getData();">
								<option value="">Select</option>
								<!--option value="1">0-2 KM</option>
								<option value="2">2-10 KM</option>
								<option value="3">10-20 KM</option>
								<option value="4">20 KM +</option-->
							</select>
						</div>
					
						<div class="data-list-filters">
							<span class="filter_heading">By Price ($)</span>
							<select id="by_price" class="custom-select select2" onchange="getData();">
								<option value="">Select</option>
								<!--option value="1">0-20</option>
								<option value="2">20-40</option>
								<option value="3">40-60</option>
								<option value="4">60-80</option>
								<option value="5">80-100</option>
								<option value="6">100 +</option-->
							</select>
						</div>
						
						<!--div class="data-list-filters">
							<span class="filter_heading">By Race</span>
							<select id="by_race" class="custom-select select2" onchange="getData();">
								<option value="">Select</option>
								<option value="1">White(Non-Hispanic)</option>
								<option value="2">Hispanic</option>
								<option value="3">Black</option>
								<option value="4">Asian</option>
								<option value="5">American Indian</option>
								<option value="6">Native Hawaiian</option>
							</select>
						</div-->
						
						<div class="data-list-filters">
							<span class="filter_heading">By Language</span>
							<select id="by_language" class="custom-select select2" onchange="getData();">
								<option value="">Select</option>
								<!--option value="en">English</option>
								<option value="es">Spanish</option>
								<option value="nl">Dutch</option>
								<option value="ru">Russian</option>
								<option value="hi">Hindi</option-->
							</select>
						</div>

						<div class="data-list-filters">
							<span class="filter_heading">By Gender</span>
							<select id="by_gender" class="custom-select select2" onchange="getData();">
								<option value="">Select</option>
								<!--option value="male">Male</option>
								<option value="female">Female</option-->
							</select>																
						</div>	
						
						<div class="data-list-filters">
							<span class="filter_heading">By Reviews</span>
							<select id="by_reviews" class="custom-select select2" onchange="getData();">
								<option value="">Select</option>
								<!--option value="1">0-10</option>
								<option value="2">10-20</option>
								<option value="3">20-30</option>
								<option value="4">30-40</option>
								<option value="5">40-50</option>
								<option value="6">50+ </option-->
							</select>																
						</div>												
					</div>
				</div>
			</div>
			
			<div class="provider_box" id="provider_data">

			</div>

        </div>
	</div>

	<script>
	
		//#by_race,
		$("#by_distance, #by_price,  #by_language, #by_gender, #by_reviews").select2({
			 minimumResultsForSearch: -1
		});
	
	
		$(function() {
			getData();
		});
	
	
		function getData(){
			
			if($("#start_date").val() !='' &&  $("#end_date").val() != ''){
				
				if($("#start_date").val() > $("#end_date").val()){
					errorAlert('Start date must not be greater than End date.',3000,'top-right');
					return false;
				}
			
			}
			
			var req_data = {
				'online_visit_id': '{{ $online_visit->id }}',
				'provider_name': $("#by_provider_name").val(),
				'distance': $("#by_distance").val(),
				'price': $("#by_price").val(),
				'language': $("#by_language").val(),
				'gender': $("#by_gender").val(),
				'review_count': $("#by_reviews").val()
			};
			
			$(".loader").css('display', 'flex');
			$.ajax({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				data: req_data,
				url: "{{ route('user.get_provider_data') }}",
				type: "POST",
				// dataType: 'json',
				success: function (response) {
					$(".loader").css('display', 'none');
					if(response['status'] == 'success'){
						$('#provider_data').html(response['data']);
					}else{
						errorAlert(response['message'],2000,'top-right');
						$('#provider_data').html(response['data']);
					}
				},
				error: function (data) {
					$(".loader").css('display', 'none');
					let errors = data.responseJSON.errors;
					
					$.each(errors, function(key, value) {
						errorAlert('Error Occured.',3000,'top-right');				
					});					
		
				}
			});		
		}
	
		function selectProvider(e, o_id, p_id){
			$(".loader").css('display', 'flex');
			$.ajax({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				data: {'o_id':o_id,'p_id':p_id},
				url: "{{ route('user.selectProviderSubmit') }}",
				type: "POST",
				// dataType: 'json',
				success: function (response) {
					$(".loader").css('display', 'none');
					
					if(response['status'] == 'success'){
						//successAlert(response['message'],2000,'top-right');
						callProgressBar();
						setTimeout(function(){
							window.location = "{{env('APP_URL')}}/user/select_appointment/{{$online_visit->id}}";						
						}, 1500);
						
					}else{
						errorAlert(response['message'],2000,'top-right');
					}
				},
				error: function (data) {
					$(".loader").css('display', 'none');
					let errors = data.responseJSON.errors;
					
					$.each(errors, function(key, value) {
						errorAlert('Error Occured.',3000,'top-right');			
					});					
				}
			});
		}
	</script>
	
	
@endsection