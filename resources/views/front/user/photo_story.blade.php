@extends('front.dashboard_layout.app')
		<link rel="stylesheet" href="https://unpkg.com/dropzone/dist/dropzone.css" />
		<link href="https://unpkg.com/cropperjs/dist/cropper.css" rel="stylesheet"/>
		<script src="https://unpkg.com/dropzone"></script>
		<script src="https://unpkg.com/cropperjs"></script>
@section('content')
    <style>
	
		.crop-preview {
  			overflow: hidden;
  			width: 160px; 
  			height: 160px;
  			margin: 10px;
  			border: 1px solid red;
		}

		.modal-lg{
  			max-width: 1000px !important;
		}

		.overlay {
		  position: absolute;
		  bottom: 10px;
		  left: 0;
		  right: 0;
		  background-color: rgba(255, 255, 255, 0.5);
		  overflow: hidden;
		  height: 0;
		  transition: .5s ease;
		  width: 100%;
		}

		.image_area:hover .overlay {
		  height: 50%;
		  cursor: pointer;
		}
		
        .social_postings {
            display: block !important;
        }
        .left_side_menu {
            display: none !important;
        }
		
		.img-container {
			height: 500px;
			overflow: hidden;
		}
		
		#sample_image{
			width:100%;
		}
    </style>
    <section class="main-dashboard">
        <div class="container-fluid p-0">
            <div class="row">
                <div class="col-md-3">
                    <div class="cstry_sidebar bg-white rounded p-3">
                        <div class="cssb_title d-flex justify-content-between align-items-center">
                            <h4 class="font-weight-bold">Your Story</h4>
                            <div class="dropdown show">
                                <a class="btn btn-light" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-cog"></i></a>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuLink">
                                    <a class="dropdown-item" href="#">Private</a>
                                    <a class="dropdown-item" href="#">Public</a>
                                    <a class="dropdown-item" href="#">Only Me</a>
                                </div>
                            </div>
                        </div>
                        <div class="cssb_user_info pt-2 pb-2 mt-2 mb-2">
                            <div class="post_by">
                                <div class="poster_img">
                                    @if(auth()->user()->image)
                                        <img src="{{ env('APP_URL')}}/{{auth()->user()->image }}" alt="{{ auth()->user()->name }}"/>
                                    @else
                                        <img src="{{ asset('dist/images/user_icon.png') }}" alt="{{ auth()->user()->name }}"/>
                                    @endif
                                </div>
                                <div class="poster_info">
                                    <div class="poster_name"><b>{{ auth()->user()->name }}</b></div>
                                </div>
                            </div>
                        </div>
                        <div class="cssb_story_editor border-top pt-3 pb-2">
                            <form action="" id="storyForm" enctype="multipart/form-data">
                                <div class="custom-file form-group mb-3">
                                    <!--<input type="file" class="custom-file-input" id="validatedCustomFile" required accept="image/*">-->
									<input class="custom-file-input" type="file" id="imageLoader" name="imageLoader"/>
                                    <label class="custom-file-label" for="validatedCustomFile">Choose file...</label>
                                    <div class="invalid-feedback">Example invalid custom file feedback</div>
                                </div>
                                <div class="form-group">
                                    <textarea class="form-control" id="overlay_text_enter" rows="5" placeholder="Overlay Text Here"></textarea>
                                    <small class="story_content_error text-danger"></small>
                                </div>
                                <div class="btns_grp mt-3 text-right">
                                    <button type="button" class="btn btn-secondary" data-toggle="modal" data-target="#discardConfirmation">Discard</button>
                                    <button type="submit" class="btn btn-primary" id="shareStory"><span class="spinner-border spinner-border-sm text-light mr-2 d-none"></span> Share to Story</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="cstry_preview_area photo_story bg-white rounded p-3">
                        <h6>Preview</h6>
                        <div class="stry_pre_bg rounded">
                            <!--<div class="stry_box sbc_one shadow-lg">
                                <div class="stry_text_result"></div>
                            </div>
                            <small class="mt-3 text-muted">Max. 250 Characters</small>-->
							<div id="canvas-wrap">
								<canvas style="display:block" id="imageCanvas" width="400" height="400">
									<canvas id="canvasID"></canvas>
								</canvas> 
							</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Discard confirmation popup -->
	<div class="modal fade what_in_mind_popup" id="discardConfirmation" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content create_post">
                <form id="add_post" enctype="multipart/form-data">
					<div class="crtpost_head">
						<h5>Discard story?</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
                    <div class="modal-body">
                        <p>Are you sure that you want to discard this story? Your story won't be saved.</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Continue Editing</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal" id="discardStory">Discard</button>
                    </div>
				</form>
            </div>
        </div>
    </div>
@endsection

	<div class="modal fade" id="cropImageModal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-body">
					<div class="img-container">
						<div class="row">
							<div class="col-md-8">
								<img src="" id="sample_image" />
							</div>
							<div class="col-md-4">
								<div class="crop-preview"></div>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" id="crop" class="btn btn-primary">Crop</button>
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
				</div>
			</div>
		</div>
	</div>	
	

@push("scripts")
<script type="text/javascript">
$(document).ready(function() {

// Discard story
$("#discardStory").on("click", function() {
	$(".pick_color>li>a").removeClass("active");
	$(".pick_color>li:first-child a").addClass("active");
	$("#storyContent").val('');
	$(".stry_text_result").text("Start Typing");
	$(".stry_pre_bg>.stry_box").removeClass("sbc_one sbc_two sbc_three sbc_four").addClass("sbc_one");
});

	
});
</script>
{{--
<script>
		function callProgressBar()
		{
			$("#pbar").css('display', 'block');
			var $progress = $('.progress');
			var $progressBar = $('.progress-bar');
			setTimeout(function() {
				$progressBar.css('width', '10%');
				setTimeout(function() {
					$progressBar.css('width', '30%');
					setTimeout(function() {
						$progressBar.css('width', '100%');
						setTimeout(function() {
							$("#pbar").css('display', 'none');
						}, 500); // WAIT 5 milliseconds
					}, 2000); // WAIT 2 seconds
				}, 1000); // WAIT 1 seconds
			}, 1000); // WAIT 1 second
		}
	
	
    </script>--}}
	
<script type="text/javascript">

var text_title = "";
var canvas = document.getElementById('imageCanvas');
var ctx = canvas.getContext('2d');
var img = new Image();
img.crossOrigin = "anonymous";
window.addEventListener('load', DrawPlaceholder);

$(document).ready(function(){
	/** Start Image upload, preview, crop and then preview and save **/
	
	var $modal = $('#cropImageModal');
	var image = document.getElementById('sample_image');
	var cropper;
	
	$('#imageLoader').change(function(event){

		var files = event.target.files;
		
 		if(files && files.length > 0)
		{
			var reader = new FileReader();
			reader.onload = function(event)
			{
				done(reader.result);
			};
			reader.readAsDataURL(files[0]);
		}
		
		var done = function(url){		
			var img = "";
			img = new Image();
			img.onload = function () {
			  //canvas.width = img.width;
			  //canvas.height = img.height;
			  canvas.width = 400;
			  canvas.height = 400;
			  ctx.drawImage(img, 0, 0);
			};
			img.src = url; //for main preview
			canvas.classList.add("show");
			DrawOverlay(img);
			DrawText();
			DynamicText(img);

			image.src = url; //for cropper modal
			$modal.modal('show');
		};

	});

	$modal.on('shown.bs.modal', function() {
		cropper = new Cropper(image, {
			aspectRatio: 1,
			viewMode: 3,
			preview:'.crop-preview'
		});
	}).on('hidden.bs.modal', function(){
		cropper.destroy();
   		cropper = null;
	});

	$('#crop').click(function(){
		let crop_canvas = cropper.getCroppedCanvas({
			width:400,
			height:400
		});

		crop_canvas.toBlob(function(blob){
			var new_url = URL.createObjectURL(blob);
			img.src = new_url; //for main preview
			var reader2 = new FileReader();
			reader2.readAsDataURL(blob);
			reader2.onloadend = function(){
				console.log('DONE');
				$modal.modal('hide');
			};
		});
	});
	
});

/** Start text on image **/

function DrawPlaceholder() 
{
  img.onload = function () {
    DrawOverlay(img);
    DrawText();
    DynamicText(img);
  };
  img.src = 'https://unsplash.it/400/400/?random';
}

function DrawOverlay(img) 
{
  ctx.drawImage(img, 0, 0);
  ctx.fillStyle = 'rgba(30, 144, 255, 0.4)';
  ctx.fillRect(0, 0, canvas.width, canvas.height);
}

function DrawText() 
{
  ctx.fillStyle = "white";
  ctx.textBaseline = 'middle';
  ctx.font = "25px 'Montserrat'";
  //ctx.fillText(text_title, 50, 50);
} 

function DynamicText(img) 
{
  document.getElementById('overlay_text_enter').addEventListener('keyup', function () {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    DrawOverlay(img);
    DrawText();
	printAt(ctx, text_title, 10, 20, 30, 380 );
    text_title = this.value;
    //ctx.fillText(text_title, 50, 50);
  });
}

function printAt( context , text, x, y, lineHeight, fitWidth)
	{
		fitWidth = fitWidth || 0;
		
		if (fitWidth <= 0)
		{
			 context.fillText( text, x, y );
			return;
		}
		
		for (var idx = 1; idx <= text.length; idx++)
		{
			var str = text.substr(0, idx);
			//console.log(str, context.measureText(str).width, fitWidth);
			if (context.measureText(str).width > fitWidth)
			{
				context.fillText( text.substr(0, idx-1), x, y );
				printAt(context, text.substr(idx-1), x, y + lineHeight, lineHeight,  fitWidth);
				return;
			}
		}
		context.fillText( text, x, y );
	}

/** End text on image **/

/****Store story into table****/
 $("#storyForm").submit(function(e){
	event.preventDefault();
	var form = document.getElementById("storyForm");
	var dataURL = canvas.toDataURL();
	var formDataToUpload = new FormData(form);
	formDataToUpload.append("imgBase64", dataURL);
	formDataToUpload.append("user_id", parseInt("{{ auth()->user()->id }}"));
	formDataToUpload.append("story_text", "");
	formDataToUpload.append("story_type", 2);


	$.ajax({
		url: "{{ route('share_to_story') }}",
		headers: {
		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		},
		type: "POST",
		datatype: 'json',
		data: formDataToUpload,
		contentType:false,
		processData:false,
		cache:false,
		beforeSend: function() {
			$("#shareStory").prop("disabled", true);
			$("#shareStory .spinner-border").removeClass("d-none");
		},
		success: function(response) {
			if (response.status) {
				callProgressBar();
				setTimeout(function(){
					location.href = "{{ url('user/dashboard') }}";
				}, 3000);
				
			}
		},
		error: function(xhr, textStatus, error) {
			if (textStatus == "error") {
				$(".story_content_error").text(JSON.parse(xhr.responseText).errors.story_text[0]);
				$("#shareStory").prop("disabled", false);
				$("#shareStory .spinner-border").addClass("d-none");
			}
		}
	});
	
});



</script>
@endpush