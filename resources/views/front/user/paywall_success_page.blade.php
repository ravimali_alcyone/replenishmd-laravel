@extends('front.online_visit_layout.app')

@section('content')
	
    <div class="tabs_wrapper welcome basics m_qs">
        <div>
            <a href="javascript:void(0);" class="how_it_works">Success</a>
            <div class="steps">
                <span class="active ml-0"></span>
                <span class="active"></span>
                <span></span>
                <span></span>
            </div>
        </div>
    </div>
    <section>
        <div class="container">
            <div class="thankyou">

            <img class="thankyou_icon" src="{{asset('assets/images/thankyou.svg')}}" alt="">

            <h4>Thank you!</h4>

            <p>Payment Done. You are redirecting to treatments page...</p>

            </div>

        </div>
    </section>	
    
	<script>
		setTimeout(function(){
			window.location = "{{ route('user.treatments') }}";
		}, 3000);
	</script>

@endsection