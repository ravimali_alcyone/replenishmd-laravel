@extends('front.dashboard_layout.app')

@section('content')

	<div class="col-xl-9 col-lg-9 col-md-12">
		<div class="main-center-data">
			<h3 class="display-username">Orders</h3>
			<div class="to-scroll-outer">
				<div class="informative-block bg-white round-crn pd-20-30 mt-15 hover-effect-box my-orders">
					<a href="#" class="theme-btn float-right show-desktop border-radius-100"><img src="{{asset('dashboard/img/btn-arrow-right.svg')}}" alt="btn-arrow-right"/></a>
					<div class="product-details">
						<div class="product-img">
							<img src="{{asset('dashboard/img/product-main.svg')}}" alt="product-main"/>
						</div>
						<div class="product-main-cnt">
							<h4 class="font-weight-bold">Sildenafil</h4>
							<div class="new-look-order-cnt">
								<div class="order-descriptions">
									<h6>Lorem ipsum dolor sit amet</h6>
									<p class="note m-0">Lorem ipsum dolor sit amet, consetetur sadipscing <br/>elitr, sed diam nonumy eirmod tempor invidunt ut <br/>labore et.</p>
								</div>
								<div class="order-product-pricing">
									<table class="table-responsive mt-15">
										<tbody>
											<tr>
												<td class="date-value">Price:</td>
												<td class="date-value">$6/per dose</td>
											</tr>
											<tr>
												<td class="date-value">Dose:</td>
												<td class="date-value">10 Doses</td>
											</tr>
											<tr>
												<td class="p-0" colspan="2">
													<div class="seprator"></div>
												</td>
											</tr>
											<tr>
												<td class="date-value">Overall Price:</td>
												<td class="theme-color">$150.00</td>
											</tr>
										</tbody>
									</table>
									<p class="includes-with-tagname m-0">Included with <span>
									Erectile dysfunction treatment</span></p>
								</div>
							</div>
						</div>
					</div>
					<a href="#" class="theme-btn float-right show-mob border-radius-100"><img src="{{asset('dashboard/img/btn-arrow-right.svg')}}" alt="btn-arrow-right"/></a>
				</div>
				<div class="informative-block bg-white round-crn pd-20-30 mt-15 hover-effect-box my-orders">
					<a href="#" class="theme-btn float-right show-desktop border-radius-100"><img src="{{asset('dashboard/img/btn-arrow-right.svg')}}" alt="btn-arrow-right"/></a>
					<div class="product-details">
						<div class="product-img">
							<img src="{{asset('dashboard/img/product-main.svg')}}" alt="product-main"/>
						</div>
						<div class="product-main-cnt">
							<h4 class="font-weight-bold">Cialis</h4>
							<div class="new-look-order-cnt">
								<div class="order-descriptions">
									<h6>Lorem ipsum dolor sit amet</h6>
									<p class="note m-0">Lorem ipsum dolor sit amet, consetetur sadipscing <br/>elitr, sed diam nonumy eirmod tempor invidunt ut <br/>labore et.</p>
								</div>
								<div class="order-product-pricing">
									<table class="table-responsive mt-15">
										<tbody>
											<tr>
												<td class="date-value">Price:</td>
												<td class="date-value">$45/per dose</td>
											</tr>
											<tr>
												<td class="date-value">Dose:</td>
												<td class="date-value">10 Doses</td>
											</tr>
											<tr>
												<td class="p-0" colspan="2">
													<div class="seprator"></div>
												</td>
											</tr>
											<tr>
												<td class="date-value">Overall Price:</td>
												<td class="theme-color">$450.00</td>
											</tr>
										</tbody>
									</table>
									<p class="includes-with-tagname m-0">Included with <span>
									Erectile dysfunction treatment</span></p>
								</div>
							</div>
						</div>
					</div>
					<a href="#" class="theme-btn float-right show-mob border-radius-100"><img src="{{asset('dashboard/img/btn-arrow-right.svg')}}" alt="btn-arrow-right"/></a>
				</div>
				<div class="informative-block bg-white round-crn pd-20-30 mt-15 hover-effect-box my-orders">
					<a href="#" class="theme-btn float-right show-desktop border-radius-100"><img src="{{asset('dashboard/img/btn-arrow-right.svg')}}" alt="btn-arrow-right"/></a>
					<div class="product-details">
						<div class="product-img">
							<img src="{{asset('dashboard/img/product-main.svg')}}" alt="product-main"/>
						</div>
						<div class="product-main-cnt">
							<h4 class="font-weight-bold">Viagra</h4>
							<div class="new-look-order-cnt">
								<div class="order-descriptions">
									<h6>Lorem ipsum dolor sit amet</h6>
									<p class="note m-0">Lorem ipsum dolor sit amet, consetetur sadipscing <br/>elitr, sed diam nonumy eirmod tempor invidunt ut <br/>labore et.</p>
								</div>
								<div class="order-product-pricing">
									<table class="table-responsive mt-15">
										<tbody>
											<tr>
												<td class="date-value">Price:</td>
												<td class="date-value">$34/per dose</td>
											</tr>
											<tr>
												<td class="date-value">Dose:</td>
												<td class="date-value">10 Doses</td>
											</tr>
											<tr>
												<td class="p-0" colspan="2">
													<div class="seprator"></div>
												</td>
											</tr>
											<tr>
												<td class="date-value">Overall Price:</td>
												<td class="theme-color">$340.00</td>
											</tr>
										</tbody>
									</table>
									<p class="includes-with-tagname m-0">Included with <span>
									Erectile dysfunction treatment</span></p>
								</div>
							</div>
						</div>
					</div>
					<a href="#" class="theme-btn float-right show-mob border-radius-100"><img src="{{asset('dashboard/img/btn-arrow-right.svg')}}" alt="btn-arrow-right"/></a>
				</div>
				<div class="informative-block bg-white round-crn pd-20-30 mt-15 hover-effect-box my-orders">
					<a href="#" class="theme-btn float-right show-desktop border-radius-100"><img src="{{asset('dashboard/img/btn-arrow-right.svg')}}" alt="btn-arrow-right"/></a>
					<div class="product-details">
						<div class="product-img">
							<img src="{{asset('dashboard/img/product-main.svg')}}" alt="product-main"/>
						</div>
						<div class="product-main-cnt">
							<h4 class="font-weight-bold">Sildenafil</h4>
							<div class="new-look-order-cnt">
								<div class="order-descriptions">
									<h6>Lorem ipsum dolor sit amet</h6>
									<p class="note m-0">Lorem ipsum dolor sit amet, consetetur sadipscing <br/>elitr, sed diam nonumy eirmod tempor invidunt ut <br/>labore et.</p>
								</div>
								<div class="order-product-pricing">
									<table class="table-responsive mt-15">
										<tbody>
											<tr>
												<td class="date-value">Price:</td>
												<td class="date-value">$6/per dose</td>
											</tr>
											<tr>
												<td class="date-value">Dose:</td>
												<td class="date-value">10 Doses</td>
											</tr>
											<tr>
												<td class="p-0" colspan="2">
													<div class="seprator"></div>
												</td>
											</tr>
											<tr>
												<td class="date-value">Overall Price:</td>
												<td class="theme-color">$150.00</td>
											</tr>
										</tbody>
									</table>
									<p class="includes-with-tagname m-0">Included with <span>
									Erectile dysfunction treatment</span></p>
								</div>
							</div>
						</div>
					</div>
					<a href="#" class="theme-btn float-right show-mob border-radius-100"><img src="{{asset('dashboard/img/btn-arrow-right.svg')}}" alt="btn-arrow-right"/></a>
				</div>
				<div class="informative-block bg-white round-crn pd-20-30 mt-15 hover-effect-box my-orders">
					<a href="#" class="theme-btn float-right show-desktop border-radius-100"><img src="{{asset('dashboard/img/btn-arrow-right.svg')}}" alt="btn-arrow-right"/></a>
					<div class="product-details">
						<div class="product-img">
							<img src="{{asset('dashboard/img/product-main.svg')}}" alt="product-main"/>
						</div>
						<div class="product-main-cnt">
							<h4 class="font-weight-bold">Cialis</h4>
							<div class="new-look-order-cnt">
								<div class="order-descriptions">
									<h6>Lorem ipsum dolor sit amet</h6>
									<p class="note m-0">Lorem ipsum dolor sit amet, consetetur sadipscing <br/>elitr, sed diam nonumy eirmod tempor invidunt ut <br/>labore et.</p>
								</div>
								<div class="order-product-pricing">
									<table class="table-responsive mt-15">
										<tbody>
											<tr>
												<td class="date-value">Price:</td>
												<td class="date-value">$45/per dose</td>
											</tr>
											<tr>
												<td class="date-value">Dose:</td>
												<td class="date-value">10 Doses</td>
											</tr>
											<tr>
												<td class="p-0" colspan="2">
													<div class="seprator"></div>
												</td>
											</tr>
											<tr>
												<td class="date-value">Overall Price:</td>
												<td class="theme-color">$450.00</td>
											</tr>
										</tbody>
									</table>
									<p class="includes-with-tagname m-0">Included with <span>
									Erectile dysfunction treatment</span></p>
								</div>
							</div>
						</div>
					</div>
					<a href="#" class="theme-btn float-right show-mob border-radius-100"><img src="{{asset('dashboard/img/btn-arrow-right.svg')}}" alt="btn-arrow-right"/></a>
				</div>
				<div class="informative-block bg-white round-crn pd-20-30 mt-15 hover-effect-box my-orders">
					<a href="#" class="theme-btn float-right show-desktop border-radius-100"><img src="{{asset('dashboard/img/btn-arrow-right.svg')}}" alt="btn-arrow-right"/></a>
					<div class="product-details">
						<div class="product-img">
							<img src="{{asset('dashboard/img/product-main.svg')}}" alt="product-main"/>
						</div>
						<div class="product-main-cnt">
							<h4 class="font-weight-bold">Viagra</h4>
							<div class="new-look-order-cnt">
								<div class="order-descriptions">
									<h6>Lorem ipsum dolor sit amet</h6>
									<p class="note m-0">Lorem ipsum dolor sit amet, consetetur sadipscing <br/>elitr, sed diam nonumy eirmod tempor invidunt ut <br/>labore et.</p>
								</div>
								<div class="order-product-pricing">
									<table class="table-responsive mt-15">
										<tbody>
											<tr>
												<td class="date-value">Price:</td>
												<td class="date-value">$34/per dose</td>
											</tr>
											<tr>
												<td class="date-value">Dose:</td>
												<td class="date-value">10 Doses</td>
											</tr>
											<tr>
												<td class="p-0" colspan="2">
													<div class="seprator"></div>
												</td>
											</tr>
											<tr>
												<td class="date-value">Overall Price:</td>
												<td class="theme-color">$340.00</td>
											</tr>
										</tbody>
									</table>
									<p class="includes-with-tagname m-0">Included with <span>
									Erectile dysfunction treatment</span></p>
								</div>
							</div>
						</div>
					</div>
					<a href="#" class="theme-btn float-right show-mob border-radius-100"><img src="{{asset('dashboard/img/btn-arrow-right.svg')}}" alt="btn-arrow-right"/></a>
				</div>
			</div>
			<div class="custom-pagination mt-15">
				<nav aria-label="Page navigation example">
				  <ul class="pagination">
					<li class="page-item"><a class="page-link" href="#">Previous</a></li>
					<li class="page-item"><a class="page-link" href="#">1</a></li>
					<li class="page-item"><a class="page-link" href="#">2</a></li>
					<li class="page-item"><a class="page-link" href="#">3</a></li>
					<li class="page-item"><a class="page-link" href="#">Next</a></li>
				  </ul>
				</nav>
			</div>
		</div>
	</div>
	
@endsection