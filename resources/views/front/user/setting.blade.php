@extends('front.dashboard_layout.app')

@section('content')

@push('header_scripts')
<style>
span.select2.select2-container.select2-container--default {
    width: 100%!important;
}

.select2-container--default .select2-selection--multiple {
    font-size: 12px;
}

.pac-container {
        z-index: 10000 !important;
}
.pac-container:after {
	background-image: none !important;
	height: 0px;
}
</style>

@endpush
	<div class="row">
		<div class="mid_wrap col-xl-9 col-lg-12 col-md-12 col-sm-12">
			<div class="row">
				<div class="col-md-12">
					<h3 class="display-username onpage w-100">User Settings</h3>
					<div class="informative-block bg-white round-crn pd-20-30 mt-15 hover-effect-box w-100">
						<h4 class="font-weight-bold">Reset password</h4>
						<span class="edit-details"  onclick="modal_box('pswrd');"><img src="{{asset('dashboard/img/edit.svg')}}" alt="edit" /></span>
						<table class="table-responsive mt-15">
							<tbody>
								<tr>
									<td class="label-mute">Password</td>
									<td>*********</td>
								</tr>
							</tbody>
						</table>
					</div>
					
					<div class="informative-block bg-white round-crn pd-20-30 mt-15 hover-effect-box w-100">
						<h4 class="font-weight-bold">Set One time Payment Pin</h4>
						<span class="edit-details"  onclick="modal_box('paymentPin');"><img src="{{asset('dashboard/img/edit.svg')}}" alt="edit" /></span>
						<table class="table-responsive mt-15">
							<tbody>
								<tr>
									<td class="label-mute">Payment Pin</td>
									<td>{{auth()->user()->payment_confirmation_pin_view}}</td>
								</tr>
							</tbody>
						</table>
					</div>
					
					<div class="informative-block bg-white round-crn pd-20-30 mt-15 hover-effect-box w-100">
						<h4 class="font-weight-bold">Set Merchant Account Access</h4>
						<span class="edit-details"  onclick="modal_box('access');"><img src="{{asset('dashboard/img/edit.svg')}}" alt="edit" /></span>
						<table class="table-responsive mt-15">
							<tbody>
								<tr>
									<td class="label-mute">Merchant Login Id</td>
									<td>{{auth()->user()->MERCHANT_LOGIN_ID}}</td>
								</tr>
								<tr>
									<td class="label-mute">Merchant Transaction Key</td>
									<td>{{auth()->user()->MERCHANT_TRANSACTION_KEY}}</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>

		<div class="rigt_sidebar col-xl-3 col-lg-12 col-md-12 col-sm-12">
			<div class="informative-block bg-white round-crn pd-20-30 settings hover-effect-box">
				<h4 class="font-weight-bold">Settings</h4>
				<div class="stwich-toggle">
					<div class="custom-control custom-switch">
					  <input type="checkbox" class="custom-control-input" id="customSwitch1">
					  <label class="custom-control-label" for="customSwitch1"></label>
					</div>
				</div>
				<h6 class="label-mute mt-15">SMS Notifications</h6>
				<p class="note m-0">By turning on this toggle, I agree to receive texts from RMD and/or {{auth()->user()->name}} to {{auth()->user()->phone}} that might be considered marketing and that may be sent using an auto dialer. I understand that agreeing is not required to purchase.</p>
			</div>
		</div>
	</div>

	<div class="chat_pop edit_pop petient chat_pop_pswrd">
		<div class="edit_pop_title">
			<h3>Update Password</h3>
			<a class="cp_close normal"><i class="fa fa-times"></i></a>
		</div>
        <div class="chat_block">
			<div class="container pt-3">
				<div class="row">
					<form id="changePassForm" class="col-md-12">
						<div class="form-group col-sm-12">
							<label for="old_password">Old Password <span class="text-danger">*</span></label>
							<input type="password" name="old_password" class="form-control" id="old_password" placeholder="">
							<span class="text-danger">
								<strong class="error" id="old_password-error"></strong>
							</span>
						</div>

						<div class="form-group col-sm-12">
							<label for="new_password">New Password <span class="text-danger">*</span></label>
							<input type="password" name="password" class="form-control" id="new_password" placeholder="">
							<span class="text-danger">
								<strong class="error" id="new_password-error"></strong>
							</span>
						</div>

						<div class="form-group col-sm-12">
							<label for="confirm_password">Confirm New Password <span class="text-danger">*</span></label>
							<input type="password" name="password_confirmation" class="form-control" id="confirm_password" placeholder="">
							<span class="text-danger">
								<strong class="error" id="confirm_password-error"></strong>
							</span>
						</div>
						
						<div class="form-group col-sm-12">
							<button type="submit" id="saveBtn" class="accept_btn btn btn-outline-primary btn-sm" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Processing" >Submit</button>
							<button type="button" class="btn btn-outline-danger btn-sm" onclick="$('.cp_close').trigger('click');">Cancel</button>
						</div>
					</form>
				</div>							
			</div>
    	</div>
	</div>
	
	<div class="chat_pop edit_pop petient chat_pop_paymentPin">
		<div class="edit_pop_title">
			<h3>One time Payment Pin</h3>
			<a class="cp_close normal"><i class="fa fa-times"></i></a>
		</div>
        <div class="chat_block">
			<div class="container pt-3">
				<div class="row">
					<form id="paymentPinForm" class="col-md-12">
						<div class="form-group col-sm-12">
							<label for="old_password">Payment Pin <span class="text-danger">*</span></label>
							<input type="password" name="payment_pin" class="form-control text-center" id="payment_pin" placeholder="" maxlength="4">
							<span class="text-danger">
								<strong class="error" id="payment_pin-error"></strong>
							</span>
						</div>
						
						<div class="form-group col-sm-12">
							<button type="submit" id="saveBtn" class="accept_btn btn btn-outline-primary btn-sm" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Processing" >Submit</button>
							<button type="button" class="btn btn-outline-danger btn-sm" onclick="$('.cp_close').trigger('click');">Cancel</button>
						</div>
					</form>
				</div>							
			</div>
    	</div>
	</div>
	
	<div class="chat_pop edit_pop petient chat_pop_access">
		<div class="edit_pop_title">
			<h3>Merchant Account Access</h3>
			<a class="cp_close normal"><i class="fa fa-times"></i></a>
		</div>
        <div class="chat_block">
			<div class="container pt-3">
				<div class="row">
					<form id="accountAccessForm" class="col-md-12">
						<div class="form-group col-sm-12">
							<label for="old_password">Merchant Login Id <span class="text-danger">*</span></label>
							<input type="text" name="merchant_login_id" class="form-control" id="merchant_login_id" placeholder="">
							<span class="text-danger">
								<strong class="error" id="merchant_login_id-error"></strong>
							</span>
						</div>

						<div class="form-group col-sm-12">
							<label for="new_password">Merchant Transaction Key <span class="text-danger">*</span></label>
							<input type="text" name="merchant_transaction_key" class="form-control" id="merchant_transaction_key" placeholder="">
							<span class="text-danger">
								<strong class="error" id="merchant_transaction_key-error"></strong>
							</span>
						</div>
						
						<div class="form-group col-sm-12">
							<button type="submit" id="saveBtn" class="accept_btn btn btn-outline-primary btn-sm" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Processing" >Submit</button>
							<button type="button" class="btn btn-outline-danger btn-sm" onclick="$('.cp_close').trigger('click');">Cancel</button>
						</div>
					</form>
				</div>							
			</div>
    	</div>
	</div>

	

	
@push('scripts')	
<!--**********--->
	
	<script>

		$("#changePassForm").submit(function(e) {
			e.preventDefault();

			//loader start
			$('#saveBtn').html($('#saveBtn').attr('data-loading-text'));

			$("#changePassForm span.text-danger .error").html('');

			$.ajax({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				data: $('#changePassForm').serialize(),
				url: "{{ route('user.change_password') }}",
				type: "POST",
				success: function (response) {
					$('#saveBtn').html('Submit');
					if(response['status'] == 'success'){
						$("#changePassForm :input[type='password']").val('');
						$(".cp_close").trigger('click');
						callProgressBar();
					}
				},
				error: function (data) {
					$("#con_btn").prop("disabled", false);
					$("#con_btn span").removeClass("d-inline-block").addClass("d-none");
					let errors = data.responseJSON.errors;
					
					$.each(errors, function(key, value) {
						errorAlert(value[0],3000,'top-right');
					});					
					$("#first_name").focus();
				}
			});
		});


		
		$("#paymentPinForm").submit(function(e) {
			e.preventDefault();
			//loader start
			$('#saveBtn2').html($('#saveBtn2').attr('data-loading-text'));
			$("#paymentPinForm span.text-danger .error").html('');
			
			var formData = new FormData(this);

			$.ajax({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				data: formData,
				url: "{{ route('user.set_payment_pin') }}",
				type: "POST",
				cache:false,
				contentType: false,
				processData: false,
				success: function (response) {
					$('#saveBtn2').html('Update');
					callProgressBar();
					if(response['status'] == 'success'){
						$("#paymentPinForm :input[type='text']").val('');
						$(".cp_close").trigger('click');
						
					}
				},
				error: function (data) {
					$("#con_btn").prop("disabled", false);
					$("#con_btn span").removeClass("d-inline-block").addClass("d-none");
					let errors = data.responseJSON.errors;
					
					$.each(errors, function(key, value) {
						errorAlert(value[0],3000,'top-right');
					});					
					$("#payment_pin").focus();
				}
			});
		});


		
		
		$("#accountAccessForm").submit(function(e) {
			e.preventDefault();
			//loader start
			$('#saveBtn2').html($('#saveBtn2').attr('data-loading-text'));
			$("#accountAccessForm span.text-danger .error").html('');
			
			var formData = new FormData(this);

			$.ajax({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				data: formData,
				url: "{{ route('user.set_merchant_access') }}",
				type: "POST",
				cache:false,
				contentType: false,
				processData: false,
				success: function (response) {
					$('#saveBtn2').html('Update');
					callProgressBar();
					if(response['status'] == 'success'){
						$("#accountAccessForm :input[type='text']").val('');
						$(".cp_close").trigger('click');
						
					}
				},
				error: function (data) {
					$("#con_btn").prop("disabled", false);
					$("#con_btn span").removeClass("d-inline-block").addClass("d-none");
					let errors = data.responseJSON.errors;
					
					$.each(errors, function(key, value) {
						errorAlert(value[0],3000,'top-right');
					});					
					$("#merchant_login_id").focus();
				}
			});
		});
		
		


	</script>
@endpush
@endsection