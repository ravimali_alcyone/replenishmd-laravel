@extends('front.dashboard_layout.app')

@section('content')
		
	<div>
		<div class="main-center-data">
			<h3 class="display-username">Appointments</h3>
			
			<div class="row mt-3 block-area">
				
				<div class="col-xl-12 col-lg-12 col-md-12 mt-15">
					<h5>Filter</h5>
					<div class="filters_wrapper">
						<div class="data-list-filters">
							<span class="filter_heading">By Date</span>
							<div class="dates">
								<input type="text" placeholder="Date : From" id="start_date" value="" onchange="getData();">
								<input type="text" placeholder="Date : To" id="end_date" value="" onchange="getData();">
							</div>
							
						</div>
					
						<div class="data-list-filters">
							<span class="filter_heading">By Visit Type</span>
							<select id="by_visit_type" class="custom-select select2" onchange="getData();">
								<option value="">All</option>
								<option value="1">Asynchronous Telemedicine</option>
								<option value="2">Synchronous Telemedicine</option>
								<option value="3">Concierge</option>
							</select>
						</div>

						<div class="data-list-filters">
							<span class="filter_heading">By Service</span>
							<select id="by_service" class="custom-select select2" onchange="getData();">
								<option value="">All</option>
							@if($services && $services->count() > 0)
								@foreach($services as $key => $value)
									<option value="{{ $value->id }}">{{ $value->name }}</option>
								@endforeach
							@endif
							</select>																
						</div>	

						<div class="data-list-filters">
							<span class="filter_heading">By Visit Status</span>
							<select id="by_visit_status" class="custom-select select2" onchange="getData();">
								<option value="">All</option>
								<option value="ACCEPTED">ACCEPTED</option>
								<option value="REVIEWED">REVIEWED</option>
								<option value="SCHEDULED">SCHEDULED</option>
								<option value="COMPLETE">COMPLETE</option>
								<option value="CANCELED">CANCELED</option>
								<option value="REFUND">REFUND</option>
								<option value="RESCHEDULED">RESCHEDULED</option>
							</select>
						</div>					
					</div>
				</div>
				
				<div class="col-xl-12 col-lg-12 col-md-12">
					<div id='calendar'></div>
				</div>
			</div>
			
			
		</div>
	</div>

 <!-- Modal -->
<div id="myModal" class="modal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Appointment Detail</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="details">
			<span class="detail-heading">Appointment at :</span>
			<span class="appointment-time"></span>
			<br>
			<div class="user-location">
				<span class="detail-heading">Location :</span>
				<span class="appointment-location"></span>
			</div>
		</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
   
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>   
<script>
	
	$(function(){
		$("#start_date").datepicker({
			dateFormat: "{{ env('DATE_FORMAT_JQ') }}",
			changeMonth: true,
			changeYear: true,
			yearRange: "-150:+0",
		});
		
		$("#end_date").datepicker({
			dateFormat: "{{ env('DATE_FORMAT_JQ') }}",
			changeMonth: true,
			changeYear: true,
			yearRange: "-150:+0",
		});
		
	});

	$("#by_visit_type, #by_service, #by_visit_status").select2({
		 minimumResultsForSearch: -1
	});
		
	var selectedEvent;
	var calendar;

	$(function() {
	  	getData();
	});
 
	function getData(){
		$(".loader").css('display', 'flex');
		//destroy 
		if(calendar){
			$("#calendar").fullCalendar('destroy');
		}
		var start_date = $("#start_date").val();
		var end_date = $("#end_date").val();
	
		var new_start_date = moment(start_date, "MM/DD/YYYY").format("YYYY-MM-DD");
		var new_end_date = moment(end_date, "MM/DD/YYYY").format("YYYY-MM-DD");
		
		if(start_date==''){
			new_start_date="{{date('Y-m-01')}}";
		}
		if(end_date==''){
			new_end_date="{{date('Y-m-t')}}";
		}
		
		//new
		calendar = $('#calendar').fullCalendar({
			
			events: {
				url: "{{ route('user.appointments') }}",
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				type: 'POST',
				data: {
					start_date: new_start_date,
					end_date: new_end_date,
					visit_type: $("#by_visit_type").val(),
					visit_status: $("#by_visit_status").val(),
					service: $("#by_service").val(),
				},
				error: function() {
					$(".loader").css('display', 'none');
					//alert('There was an error while fetching appointments!');
					errorAlert('No appointment found for this criteria.',2000,'top-right');				
				}
			},			
			displayEventEnd: false,
			timeFormat: 'h:mm a',
			eventOrder: 'title',
			eventBorderColor: '#ddd',
			eventTextColor: '#fff',
			eventBackgroundColor: '#ffc107',
			editable: false,
			locale: 'en',
			
			eventRender: function(event, element) {
				//console.log('event',event);
				$(".loader").css('display', 'none');
					$(element).css("padding","5px");
					$(element).css("font-size","13px");					
				if(event.status == 1){
					$(element).css("background-color","#28a745");
				}
			},
			
			eventClick: function(calEvent, jsEvent, view) {	
				selectedEvent = $(this);
				
				//console.log(calEvent);
				let dt = moment(calEvent.start);
				let date = dt.format('MM/DD/YYYY h:mm A');
				
				let dt2 = moment(calEvent.end);
				let date2 = dt2.format('h:mm A');
					
				$('#myModal .modal-title').html(calEvent.title);
				$('#myModal .modal-body span.appointment-time').html(date+"-"+date2);
				$('#myModal .modal-body span.appointment-detail').html(calEvent.service+' ('+calEvent.visit_type+')');
				if(calEvent.location==''){
					$('#myModal .modal-body div.user-location').hide();
				}else{
					$('#myModal .modal-body div.user-location').show();
					$('#myModal .modal-body span.appointment-location').html(calEvent.location);
				}
				$('#myModal').modal('show');
			}				

		});
	} 
  
</script>

@endsection