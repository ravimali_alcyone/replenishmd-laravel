@extends('front.dashboard_layout.app')

@section('content')
	<!-- Style.css -->
	<style>
		.intro_step_ques_box{text-align:center}
		.ques_box .carousel{height:100vh;display:flex;flex-direction:column;align-items:center;justify-content:center;padding:50px 0}
		.ques_box .carousel-indicators{top:50px;bottom:auto;z-index: 1;}
		.intro_step_ques_box .carousel-indicators li{opacity:1;background-color:#ddd}
		.intro_step_ques_box .carousel-indicators li.active{opacity:1;background-color:#ff6c66}
		.carousel-item .content{height:100%}
		.intro_ques_box{text-align:center;padding-top:10%;margin:0 auto;display:flex;justify-content:flex-start;flex-direction:column;align-items:center;height:100%}
		.intro_ques_box>div{width:100%}
		.qus_wel .intro_stps_btn_grp{justify-content:center}
		.intro_stps_btn_grp{display:flex;justify-content:center;width:100%;max-width:400px;align-items:center;margin:25px auto;flex-wrap:wrap}
		.intro_stps_btn_grp .btn{height:50px;display:flex;justify-content:center;align-items:center;width:100%;max-width:160px;border-radius:4px;margin:10px}
		.intro_stps_btn_grp .btn:hover{box-shadow:1px 0 4px rgba(0,2,4,.055),0 7px 18px rgba(1,1,1,.05)}
		.intro_stps_btn_grp .con_btn{background-color:#2c99cb;color:#fff}
		.intro_stps_btn_grp .con_btn:hover{background-color:#ff6c66;border-color:#eb5752}
		.intro_stps_btn_grp .skip_btn{border-color:#2c99cb;color:#2c99cb}
		.intro_stps_btn_grp .skip_btn:hover{background-color:#fff;border-color:#f1f8f9;color:#333}
		.intro_gra_img{margin-bottom:50px}
		.intro_ques_box h5{margin:15px auto;line-height:1.5;font-weight:700}
		.intro_step_wrap .intro_modal{background-color:rgba(255,255,255,.98)}
		.intro_step_wrap .intro_modal .modal-dialog{height:100vh;margin:0 auto;padding:0;display:flex;justify-content:center;flex-direction:column;max-width:inherit}
		.intro_step_wrap .intro_modal .modal-content{background-color:transparent;border:none;height:100vh;padding:15px;position:relative}
		.intro_step_wrap .intro_modal .intro_model_wrap{max-width:500px;margin:40px auto 15px auto;text-align:left;width:100%}
		.intro_model_wrap .form-control{background-color:#fff;border-color:#dedede;height:50px}
		.intro_model_wrap textarea.form-control{height:auto}
		.intro_model_wrap .form-control:focus{box-shadow:1px 0 4px rgba(0,2,4,.055),0 7px 18px rgba(1,1,1,.05)}
		.intro_model_wrap .form-group{margin-bottom:.75rem}
		.intro_model_wrap label{display:inline-block;margin-bottom:.25rem;font-size:14px;color:#555}
		.intro_model_wrap .modal-title{margin-bottom:1.5rem;line-height:1.5;font-size:1.5rem;font-weight:700}
		.intro_model_wrap .intro_stps_btn_grp{display:flex;justify-content:flex-start;max-width:initial;align-items:center;flex-wrap:wrap;padding:0;margin:0 .5rem;border-top:none}
		.intro_modal button.close{position:absolute;right:15px;top:15px;width:40px;height:40px;border-radius:50%;border:none;background-color:#eee}
		.intro_address_box{box-shadow:0 0 20px 0 rgba(50,56,66,.1);padding:15px 20px;background-color:#fff;border-radius:4px}
		.intro_ques_box .form-control{background-color:#fff;border-color:#dedede;height:50px}
		.intro_ques_box .intro_loc{background-image:url('/assets/images/map_marker.svg');background-repeat:no-repeat;text-indent:25px;background-position:11px}
		.intro_ques_box textarea.form-control{height:auto}
		.intro_ques_box .form-control:focus{box-shadow:1px 0 4px rgba(0,2,4,.055),0 7px 18px rgba(1,1,1,.05)}
		.intro_ques_box .form-group{margin:0 auto .75rem auto;max-width:480px}
		.intro_step_wrap #settingModal .modal-dialog{max-width:inherit;margin:0 auto}
		.intro_step_wrap #settingModal .intro_model_wrap{max-width:670px}
		.days_select{margin:0;padding:0}
		.days_select li{display:flex;width:45px;height:45px;border-radius:4px;background-color:#fff;border:1px solid #dedede;justify-content:center;align-items:center}
		.days_select li:hover{background-color:#ff6c66;border-color:#ff6c66;color:#fff;box-shadow:1px 0 4px rgba(0,2,4,.055),0 7px 18px rgba(1,1,1,.05);cursor:pointer}
		.days_select{display:flex;justify-content:center;flex-wrap:wrap;gap:4px;margin-top:5px}
		.weekend_days{text-align:left;max-width:340px;margin:0 auto}
		.select_weeke_day{background-color: #ff6c66 !important;border-color: #ff6c66 !important;color: #fff;box-shadow: 1px 0 4px rgb(0 2 4 / 6%), 0 7px 18px rgb(1 1 1 / 5%);cursor: pointer;}
		#carouselExampleIndicators{height:100% !important;}
		.page_before_dashboard{width:100% !important;}
        .pd_header{display: none;}
		@media screen and ( max-width: 767px) {
			.intro_ques_box{ margin: 0 15px; justify-content: center;}
		}
		@media screen and ( max-width: 400px) {
			.intro_stps_btn_grp .btn{ margin: 4px;}
			.intro_model_wrap .modal-body{ padding: 0;}
			.intro_model_wrap .intro_stps_btn_grp{ margin: 0 auto;}
		}
		@media screen and ( max-width: 400px) {
			.intro_stps_btn_grp .btn{ margin: 4px 0; max-width: inherit;}
		}
		.btn_red {
			background-color: #ff3401 !important;
			color: #fff !important;
		}
		.modal-backdrop.show {
            opacity: 0.5;
            z-index: 0;
        }
        
        .modal-open .modal{
            z-index: 1;
        }
	</style>
	
	<section class="page_before_dashboard">
        <div class="container-fluid">
            <div class="row">
                <div class="col">
                    <div class="intro_step_wrap">
                        <div class="intro_step_ques_box ">
                            <div class="ques_slider">
                                <div class="ques_box">
                                    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel"
                                        data-interval="false">
                                        <ol class="carousel-indicators">
                                            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                                            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                                            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                                            <!-- <li data-target="#carouselExampleIndicators" data-slide-to="3"></li> -->
                                        </ol>
                                        <div class="carousel-inner">
                                            <div class="carousel-item active">
                                                <div class="content">
                                                    <div class="intro_ques_box">
                                                        <div class="qus_wel">
                                                            <a class="rmd_logo" href="javascript:void(0)">
                                                                <img src="{{asset('assets/images/rmd_logo_red.svg')}}" class="logo logo_red w-auto" alt="ReplenishMD" />
                                                            </a>
                                                            <h5>Welcomes you, {{auth()->user()->name}}.</h5>
                                                            <div class="intro_stps_btn_grp">
                                                                <a class="btn con_btn" href="#carouselExampleIndicators" role="button" data-slide="next">
                                                                <span>Let's Get Started!</span></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="carousel-item">
                                                <div class="content">
                                                    <div class="intro_ques_box">
                                                        <div class="qus_one">
                                                            <div class="intro_gra_img"><img src="{{asset('assets/images/gra1.svg')}}" alt=""></div>
                                                            <h6>ReplenishMD is a new age integrative, Functional, Anti-Aging, Regenerative Medicine, MedSpa, Mobile Lab & Concierge Medical Clinic at your service.</h6>
															<p>&nbsp;</p>
															<h6>
																<strong>Asynchronous Visit, </strong>
																In asynchronous visit a replenisher will able to communicate with provider over chat only and provider also be alot by system automaticaly.
															</h6>
															<h6>
																<strong>Synchronous Visit, </strong>
																In synchronous visit a replenisher will able to communicate with provider over chat and video call and they have also an option for choose provider as per their requirement.
															</h6>
															<h6>
																<strong>Concierge Visit, </strong>
																In concierge visit a replenisher will able to communicate with provider by visit provider location and they have also an option for choose provider as per their requirement.
															</h6>
															
															<!--<div class="position-relative mx-auto" style="max-width: 400px;">
                                                                <table class="mx-auto">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td class="label-mute">Password</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>****</td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                                <div class="edit_icon">
                                                                    <span class="edit-details" data-toggle="modal" data-target="#passModal"><img src="{{asset('dashboard/img/edit.svg')}}" alt="edit" /></span>
                                                                </div>
                                                            </div>-->
															
															
                                                            <div class="intro_stps_btn_grp">
                                                                <!-- <button type="button" class="con_btn btn"  onclick="pass_modal(this)" >Create New!</button> -->
                                                                <a class="btn skip_btn" href="#carouselExampleIndicators" role="button" data-slide="next"><span>Skip</span></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="carousel-item">
                                                <div class="content">
                                                    <div class="intro_ques_box">
                                                        <div class="qus_one">
                                                            <div class="intro_gra_img"><img src="{{asset('assets/images/gra3.svg')}}" alt=""></div>
                                                            <h5 >Set your four digit payment confirmation pin.</h5>
                                                            <div class="position-relative mx-auto" style="max-width: 400px;">
                                                                <table class="mx-auto">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td class="label-mute">Payment Confirmation Pin</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td><?php echo ($payment_pin['payment_confirmation_pin_view']==null) ? '****' : $payment_pin['payment_confirmation_pin_view'];?></td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                                <div class="edit_icon">
                                                                    <span class="edit-details" data-toggle="modal"  onclick="$('#paymentModal').modal({ backdrop: 'static', keyboard: false });"><img src="{{asset('dashboard/img/edit.svg')}}" alt="edit" /></span>
                                                                </div>
                                                            </div>
                                                            <div class="intro_stps_btn_grp">
                                                                <!-- <button type="button" class="con_btn btn" onclick="setting_modal(this);">Add Timings</button> -->
                                                                <a class="btn skip_btn_last skip_btn" href="javascript:void(0)" role="button" data-slide="next">
                                                                    <span>Skip</span>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                           
                                        </div>
                                     </div>
                                </div>
                               
                            </div>
                            <!--Modal-->
                            <div id="passModal" class="intro_modal modal" tabindex="-1" role="dialog">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                       <div class="intro_model_wrap">
                                        <form id="changePassForm">
                                            <div class="modal-body">
                                                <h5 class="modal-title">Change Password</h5>
                                                <div class="form-group has-feedback">
                                                    <label for="old_password">Old Password <span class="text-danger">*</span></label>
                                                    <input type="password" name="old_password" class="form-control" id="old_password" >
                                                    <span class="text-danger">
                                                        <strong class="error" id="old_password-error"></strong>
                                                    </span>
                                                </div>

                                                <div class="form-group has-feedback">
                                                    <label for="new_password">New Password <span class="text-danger">*</span></label>
                                                    <input type="password" name="password" class="form-control" id="new_password" >
                                                    <span class="text-danger">
                                                        <strong class="error" id="new_password-error"></strong>
                                                    </span>
                                                </div>

                                                <div class="form-group has-feedback">
                                                    <label for="confirm_password">Confirm New Password <span class="text-danger">*</span></label>
                                                    <input type="password" name="password_confirmation" class="form-control" id="confirm_password">
                                                    <span class="text-danger">
                                                        <strong class="error" id="confirm_password-error"></strong>
                                                    </span>
                                                </div>

                                            </div>
                                            <div class="modal-footer intro_stps_btn_grp">
                                                <button type="submit" id="saveBtn" class="con_btn btn" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Processing">Save</button>
                                                <button type="button" class="btn skip_btn" data-dismiss="modal">Cancel</button>
                                            </div>
                                        </form>
                                       </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!--Edit Setting-->
                        <div id="settingModal" class="intro_modal modal" tabindex="-1" role="dialog">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                   <div class="intro_model_wrap">

                                    <form id="settingForm">
                                        <div class="modal-body">
                                            <h5 class="modal-title">Set Timings</h5>
											<div class="form-row">
												<div class="form-group col-md-3 has-feedback">
													<label for="first_name">Office starting time <span class="text-danger">*</span></label>
														<div class='input-group date mt-2'>
														<input type='text' class="form-control timepicker"
															name="office_start_time[]" value="{{ date('h:i A') }}" />
														<span class="input-group-text" id="basic-addon1">
															<img src="{{asset('assets/images/clock.svg')}}">
														</span>
													</div>
													<span class="text-danger">
														<strong class="error" id="dob-error"></strong>
													</span>
												</div>
												<div class="form-group col-md-3 has-feedback">
													<label for="last_name">Office closing time <span
															class="text-danger">*</span></label>
													<div class='input-group date mt-2'>
														<input type='text' class="form-control timepicker"
															name="office_close_time[]" value="{{ date('h:i A') }}" />
														<span class="input-group-text" id="basic-addon1">
															<img src="{{asset('assets/images/clock.svg')}}">
														</span>
													</div>
													<span class="text-danger">
														<strong class="error" id="dob-error"></strong>
													</span>
												</div>
												<div class="form-group col-md-4 has-feedback">
													<label for="last_name">Location <span
															class="text-danger">*</span></label>
													<div class='input-group date mt-2'>
														<select name="office_location[]" id="" class="form-control">
														</select>
													</div>
													<span class="text-danger">
														<strong class="error" id="dob-error"></strong>
													</span>
												</div>
												<div class="form-group col-md-1 offset-md-1 has-feedback mt-1">
													<a href="javascript:void(0)" title="Add more" class="add_item"><span
															style="color:#2c99cb;"><i class="fa fa-plus-circle fa-2x"
																aria-hidden="true"></i></span></a>
												</div>
											</div>


                                           <div class="more_items_wrapper"></div>
											<hr>


                                            <div class="form-row">
                                                <div class="form-group col-md-5 has-feedback">
                                                    <label for="gender">Break start time</label>
                                                    <div class='input-group date mt-2'>
                                                        <input type='text' class="form-control timepicker"
                                                            name="break_start_time[]" value="{{ date('h:i A') }}" />
                                                        <span class="input-group-text" id="basic-addon1">
                                                            <img src="{{asset('assets/images/clock.svg')}}">
                                                        </span>
                                                    </div>
                                                    <span class="text-danger">
                                                        <strong class="error" id="dob-error"></strong>
                                                    </span>
                                                </div>
                                                <div class="form-group col-md-5 has-feedback">
                                                    <label for="gender">Break end time</label>
                                                    <div class='input-group date mt-2'>
                                                        <input type='text' class="form-control timepicker"
                                                            name="break_end_time[]" value="{{ date('h:i A') }}" />
                                                        <span class="input-group-text" id="basic-addon1">
                                                            <img src="{{asset('assets/images/clock.svg')}}">
                                                        </span>
                                                    </div>
                                                    <span class="text-danger">
                                                        <strong class="error" id="dob-error"></strong>
                                                    </span>
                                                </div>
                                                <div class="form-group col-md-1 offset-md-1 has-feedback mt-1">
                                                    <a href="javascript:void(0)" title="Add more"
                                                        class="add_break"><span style="color:#2c99cb;"><i
                                                                class="fa fa-plus-circle fa-2x"
                                                                aria-hidden="true"></i></span></a>
                                                </div>
                                            </div>
                                            <div class="more_break_wrapper"></div>

                                        </div>
                                        <div class="modal-footer intro_stps_btn_grp">
                                            <button type="submit" id="saveBtn3"
                                                class="con_btn btn"
                                                data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Processing">Save</button>
                                            <button type="button" class="btn skip_btn"
                                                data-dismiss="modal">Cancel</button>
                                        </div>
                                    </form>
                                   </div>
                                </div>
                            </div>
                        </div>
                        <!--End here-->
						

                        <!---Weekend day setting--->
                        <div id="weekendModal" class="modal" tabindex="-1" role="dialog">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title">Weekend days setting</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <form id="weekendForm_old">
                                        <div class="modal-body">
                                            <div class="form-group has-feedback">
                                                <label for="old_password">Select day <span
                                                        class="text-danger">*</span></label>
                                                <select name="weekend_day[]" class="custom-select select2 form-control"
                                                    id="weekend_day" multiple="multiple">
                                                    <option value="Mon">Mon</option>
                                                    <option value="Tue">Tue</option>
                                                    <option value="Wed">Wed</option>
                                                    <option value="Thu">Thu</option>
                                                    <option value="Fri">Fri</option>
                                                    <option value="Sat">Sat</option>
                                                    <option value="Sun">Sun</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="submit" id="saveBtn"
                                                class="accept_btn btn btn-outline-primary btn-sm"
                                                data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Processing">Submit</button>
                                            <button type="button" class="btn btn btn-outline-danger btn-sm"
                                                data-dismiss="modal">Cancel</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!----End here---->

                        <!---Weekend day setting--->
                        <div id="paymentModal" class="modal" tabindex="-1" role="dialog">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Payment Confirmation Pin</h5>
                                <!--<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button>-->
                            </div>
                            <form id="paymentForm">
                            <div class="modal-body">
                                <div class="form-group has-feedback">
                                    <label for="old_password">Payment Confirmation Pin <span class="text-danger">*</span></label>
                                    <input type="password" name="payment_pin" id="payment_pin" class="form-control" value="" maxlength="4" placeholder="Payment pin">
                                    <span class="text-danger">
                                        <strong class="error" id="payment_pin-error"></strong>
                                    </span>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="submit" id="saveBtn_pin" class="accept_btn btn btn-outline-primary btn-sm" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Processing" >Submit</button>
                                <button type="button" class="btn btn btn-outline-danger btn-sm" data-dismiss="modal">Cancel</button>
                            </div>
                            </form>
                            </div>
                        </div>
                        </div>
                        <!----End here---->


                        <script>
                      

                            /**********Reset Password*********/
                            function pass_modal() {
                                $("#passModal").modal('show');
                                $("#changePassForm").trigger('reset');
                                $("#changePassForm span.text-danger .error").html('');
                            }

                            $("#changePassForm").submit(function(e) {
								e.preventDefault();
								
								//loader start
								$('#saveBtn').html($('#saveBtn').attr('data-loading-text'));
								
								$("#changePassForm span.text-danger .error").html('');
							
								$.ajax({
									headers: {
										'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
									},
									data: $('#changePassForm').serialize(),
									url: "{{ route('user.change_password') }}",
									type: "POST",
									// dataType: 'json',
									success: function (response) {
										$('#saveBtn').html('Submit');
										if(response['status'] == 'success'){
											
											$("#passModal").modal('hide');

											//successAlert(response['message'],2000,'top-right');
											callProgressBar();
											setTimeout(function(){
												$('#changePassForm').trigger("reset");
												//location.reload();
											}, 5000);
										}
									},
									error: function (data) {
										$('#saveBtn').html('Submit');
										
										if(data.responseJSON.errors) {
											let errors = data.responseJSON.errors;
											if(errors.old_password){
												$( '#old_password-error' ).html( errors.old_password[0] );
											}
											
											if(errors.password){
												$( '#new_password-error' ).html( errors.password[0] );
											}
											
											if(errors.password_confirmation){
												$( '#confirm_password-error' ).html( errors.password_confirmation[0] );
											}
											
										}
									
									}
								});
							});

                            /****End Here*****/


                           $("#paymentForm").submit(function(e) {
								e.preventDefault();
								//loader start
								$(".loader").css('display', 'flex');
								$('#saveBtn_pin').html($('#saveBtn_pin').attr('data-loading-text'));
								
								$("#paymentForm span.text-danger .error").html('');
							
								$.ajax({
									headers: {
										'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
									},
									data: $('#paymentForm').serialize(),
									url: "{{ route('user.set_payment_pin') }}",
									type: "POST",
									// dataType: 'json',
									success: function (response) {
										$('#saveBtn_pin').html('Submit');
										$(".loader").css('display', 'none');
										if(response['status'] == 'success'){
											
											$("#paymentModal").modal('hide');

											//successAlert(response['message'],2000,'top-right');
											callProgressBar();
											setTimeout(function(){
												$('#paymentForm').trigger("reset");
												window.location=response['redirect'];
											}, 2000);
										}
									},
									error: function (data) {
										$('#saveBtn_pin').html('Submit');
										$(".loader").css('display', 'none');
										if(data.responseJSON.errors) {
											let errors = data.responseJSON.errors;
											if(errors.payment_pin){
												$( '#payment_pin-error' ).html( errors.payment_pin[0] );
											}
											
										}
									
									}
								});
							});

                            $(".skip_btn_last").click(function (e) {
								var id ="{{ auth()->user()->id }}";
								swal({
									title: "You need to setup PIN before making payment. We are trying to make your experience better and Secured !!",
									text: "",
									icon: 'warning',
									buttons: {
									  cancel: true,
									  delete: {text:'Yes',className:'btn_red'}
									}
								  }).then((isConfirm) => {
									if (!isConfirm) {
										return false;
									} else {
										$(".loader").css('display', 'flex');
										$.ajax({
											headers: {
												'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
											},
											data: {'id':id },
											url: "{{ route('user.set_payment_pin') }}",
											type: "POST",
											success: function (response) {
												if (response['status'] == 'success') {
													$(".loader").css('display', 'none');
													setTimeout(function(){
														$('#paymentForm').trigger("reset");
														window.location=response['redirect'];
													}, 2000);
												}
											}
										});
									}
								});
                            });

							

                            var sliderFlag = 0;
							$(document).on('click', '#con_btn', function() {
                                $("#carouselExampleIndicators .carousel-control-next").trigger('click');
                                sliderFlag++;
                                if (sliderFlag == 4) {
                                    $("#con_btn").attr('disabled', true);
                                }
                                else {
									$(document).on('click', '#carouselExampleIndicators ol li', function() {
                                        var indx = $(this).attr("data-slide-to");
                                        sliderFlag = indx;
                                        $("#con_btn").attr('disabled', false);
                                    });
                                }
                            });

                            $(document).on('click', '#carouselExampleIndicators ol li', function() {
                                var indx = $(this).attr("data-slide-to");
                                if (indx == 4) {
                                    $("#con_btn").attr('disabled', true);
                                }
                            });


                        </script>

                    </div>
                </div>
            </div>
        </div>
        </div>
    </section>
@endsection