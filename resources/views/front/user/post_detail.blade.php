@extends('front.dashboard_layout.param')

@section('content')
	@php
		$name = (strpos(auth()->user()->name, ' ') !== false) ? explode(' ',auth()->user()->name)[0] : auth()->user()->name;
        $post_id;
	@endphp

	<style>
        #adicionafoto {
            position: absolute;
            left: 80%;
            opacity: 0;
            cursor: pointer;
        }
        #galeria{
            display: flex;
        }
        #galeria img{
            width: 100px;
            height: 100px;
            border-radius: 10px;
            box-shadow: 0 0 8px rgb(0 0 0 / 20%);
            opacity: 85%;
            margin-right: 10px;
            margin-top: 10px;
        }
	</style>
    <link rel="stylesheet" href="{{ asset('dashboard/css/lightbox.min.css') }}">

	<div class="mid_wrap">
        
        <div class="row">
            <div class="col-md-12">
				<div id="fb-root"></div>
				@if($post && count($post)>0)
                    @foreach($post as $val)
                        <div class="post_wrap">
                            <div class="post_head">
                                <div class="post_by">
                                    <div class="poster_img">
                                        @if(auth()->user()->image)
                                            <img src="{{ env('APP_URL')}}/{{auth()->user()->image }}" alt="{{ auth()->user()->name }}"/>
                                        @else
                                            <img src="{{ asset('dist/images/user_icon.png') }}" alt="{{ auth()->user()->name }}"/>
                                        @endif
                                    </div>
                                    <div class="poster_info">
                                        <div class="poster_name"><b>{{$val->postBy}}</b></div>
                                        <div class="post_date"><span><a href="{{env('APP_URL')}}/post/{{$val->param_id}}">{{$val->created_at}}</a></span></div>
                                    </div>
                                </div>
                                <div class="dropdown post_setting">
                                    <button class="dots_btn" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <span>&#8411;</span>
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                        <a class="dropdown-item" href="#">Hide Post</a>
                                        <a class="dropdown-item" href="#">Block This Person</a>
                                        <a class="dropdown-item" href="#">Report</a>
                                    </div>
                                </div>
                            </div>
                            <div class="post_body">
                                <div class="post_text">
                                    <p>@if($val->post_content){{$val->post_content}} @endif</p>
                                </div>
                                <div class="post_imgs">
                                    @isset($posts_media[$val->post_id])
                                        @foreach($posts_media[$val->post_id] as $key => $value)
                                            <a class="example-image-link {{ count($posts_media[$val->post_id]) == 1 ? 'mw-100' : '' }}" href="/{{ $value }}" data-lightbox="lightbox-set-{{ $val->post_id }}">
                                                <img class="example-image" src="/{{ $value }}" alt="...">
                                            </a>
                                        @endforeach
                                        @if(count($posts_media[$val->post_id]) > 4)
                                            <span class="more_imgs_caption">{{ count($posts_media[$val->post_id]) - 4 }} More</span>
                                        @endif
                                    @endisset
                                </div>
                                <div class="post_reactions">
                                    <div class="post_likes">
                                        <div class="top_three_recent">
                                            <div><img src="{{ asset('dashboard/img/smiles/like.svg') }}" alt="..."></div>
                                            <div><img src="{{ asset('dashboard/img/smiles/in-love.svg') }}" alt="..."></div>
                                            <div><img src="{{ asset('dashboard/img/smiles/angry.svg') }}" alt="..."></div>
                                        </div>
										@if(isset($likePostUserData[$val->post_id]))
											@foreach($likePostUserData[$val->post_id] as $key => $value)
												<span><a href="javascript:void(0)" id="{{$val->post_id}}" onclick="getLikeDetail(this.id)"><?php 
												
												/*if(($val->num_of_post_like)>3)
												{
													if (strpos($str, 'You') !== false) {
														echo 'You,'.trim(preg_replace("/,+/", ",", str_replace("You","",$value)), ",").',&'.($val->num_of_post_like - 3).' others';
													}
												}
												else 
												{ 
													echo trim('You,'.trim(preg_replace("/,+/", ",", str_replace("You","",$value)), ","), ",");
												}*/
												if( ($val->num_of_post_like) >3)
												{
													$str ='';
													if (strpos($str, 'You') !== false) {
														echo 'You,'.trim(preg_replace("/,+/", ",", str_replace("You","",$value)), ",").',&'.($val->num_of_post_like - 3).' others';
													}
												}
												else if( ($val->num_of_post_love) >3)
												{
													$str ='';
													if (strpos($str, 'You') !== false) {
														echo 'You,'.trim(preg_replace("/,+/", ",", str_replace("You","",$value)), ",").',&'.($val->num_of_post_love - 3).' others';
													}
												}
												else if( ($val->num_of_post_sad) >3)
												{
													$str ='';
													if (strpos($str, 'You') !== false) {
														echo 'You,'.trim(preg_replace("/,+/", ",", str_replace("You","",$value)), ",").',&'.($val->num_of_post_sad - 3).' others';
													}
												}
												else if( ($val->num_of_post_angary) >3)
												{
													$str ='';
													if (strpos($str, 'You') !== false) {
														echo 'You,'.trim(preg_replace("/,+/", ",", str_replace("You","",$value)), ",").',&'.($val->num_of_post_angary - 3).' others';
													}
												}
												else 
												{
													if($value=='You')
													{
														echo trim('You,'.trim(preg_replace("/,+/", ",", str_replace("You","",$value)), ","), ",");
													} 
													else
													{
														echo trim(preg_replace("/,+/", ",", str_replace("You","",$value)), ",");
													}
												}
												?> </a></span>
											@endforeach
										@endisset
                                    </div>
                                    <div class="post_status">
                                        <div class="post_s_type"><span id="{{$val->post_id}}" onclick="getCommentDetail(this.id)">Comments</span><span>{{$val->num_of_post_commnet}}</span></div>
										<div class="post_s_type">Share<span>{{$val->num_of_post_share}}</span></div>
                                    </div>
                                </div>
                                <div class="posting_btn_grp">
                                    <div class="rec_btn">
									
                                        @if(isset($post_likes[$val->post_id]))
                                            <span class="like-btn {{ $post_likes[$val->post_id] == 1 ? 'reacted' : '' }}">
                                        @else
                                            <span class="like-btn">
                                        @endif
                                            <span class="like-btn-text" id="{{$val->post_id}}" onclick="doLikePost(this.id, {{ isset($post_likes[$val->post_id]) ? $post_likes[$val->post_id] : 0 }})"><i class="fa fa-thumbs-up"></i> Like</span>
                                            <ul class="reactions-box">
												@if(isset($post_likes_flagData[$val->post_id]))
												@foreach($post_likes_flagData[$val->post_id] as $key => $value)
													<li class="reaction reaction-like" data-reaction="Like" data-postId="{{$val->post_id}}" data-likeFlag="{{ $value['like_flag'] }}"></li>
													<li class="reaction reaction-love" data-reaction="Love" data-postId="{{$val->post_id}}" data-likeFlag="{{ $value['love_flag'] }}"></li>
													<li class="reaction reaction-sad" data-reaction="Sad" data-postId="{{$val->post_id}}" data-likeFlag="{{ $value['sad_flag'] }}"></li>
													<li class="reaction reaction-angry" data-reaction="Angry" data-postId="{{$val->post_id}}" data-likeFlag="{{ $value['angary_flag'] }}"></li>
												@endforeach
												@else
													<li class="reaction reaction-like" data-reaction="Like" data-postId="{{$val->post_id}}" data-likeFlag="0"></li>
													<li class="reaction reaction-love" data-reaction="Love" data-postId="{{$val->post_id}}" data-likeFlag="0"></li>
													<li class="reaction reaction-sad" data-reaction="Sad" data-postId="{{$val->post_id}}" data-likeFlag="0"></li>
													<li class="reaction reaction-angry" data-reaction="Angry" data-postId="{{$val->post_id}}" data-likeFlag="0"></li>
												@endisset
                                            </ul>
                                        </span>
                                    </div>
                                    <div class="rec_btn"><a data-toggle="collapse" href="#collapseComment_{{$val->post_id}}" role="button" aria-expanded="false" aria-controls="collapseComment"><i class="fa fa-comment"></i> Comment</a></div>
                                    <div class="dropdown">
										<div class="rec_btn" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><a href="javascript:void(0)"><i class="fa fa-share"></i> Share</a></div>
										<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
											<a class="dropdown-item" href="javascript:void(0)">Only me</a>
											<div class="fb-like" data-href="https://developers.facebook.com/docs/plugins/" data-layout="standard" data-action="like" data-size="small" data-show-faces="true" data-share="true"></div>
											<a class="dropdown-item" href="javascript:void(0)">Via Facebook</a>
											<a class="dropdown-item" href="javascript:void(0)">Via LinkedIn</a>
										</div>
									</div>
									
									<!--<div class="dropdown">
										<button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
											Share
										</button>
										<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
											<a class="dropdown-item" href="#">Action</a>
											<a class="dropdown-item" href="#">Another action</a>
											<a class="dropdown-item" href="#">Something else here</a>
										</div>
									</div>-->
                                </div>
                            </div>
                            <div class="post_comment_block">
                                <form id="form_post_comment_{{$val->post_id}}">
                                    <div class="collapse" id="collapseComment_{{$val->post_id}}">
                                        <div>
											@if(isset($post_comment[$val->post_id]))
											@php
											$i=1;
											@endphp
											@foreach($post_comment[$val->post_id] as $key => $value)
											<?php if($i<6) {?>
											<div class="media">
												<div class="poster_img">
												@if($value['image'])
													<img src="{{ env('APP_URL')}}/{{$value['image'] }}" alt="{{ auth()->user()->name }}" />
												@else
													<img src="{{ asset('dist/images/user_icon.png') }}" alt="{{ auth()->user()->name }}" />
												@endif
												</div>
												<div class="media-body">
													<h6 class="mt-0">{{$value['commentBy'] }}</h6>
													<p>{{ $value['comments'] }}</p>
												</div>
											</div>
											<?php } 
											elseif($i>=6) {
												if($i==6){ echo '<span id="show_more"><a href="javascript:void(0)">Show more</a></span>';}		
											?>
											<div class="media show_more" style="display:none;">
												<div class="poster_img">
												@if($value['image'])
													<img src="{{ env('APP_URL')}}/{{$value['image'] }}" alt="{{ auth()->user()->name }}" />
												@else
													<img src="{{ asset('dist/images/user_icon.png') }}" alt="{{ auth()->user()->name }}" />
												@endif
												</div>
												<div class="media-body">
													<h6 class="mt-0">{{$value['commentBy'] }}</h6>
													<p>{{ $value['comments'] }}</p>
												</div>
											</div>
											
											<?php
												if($i==count($post_comment[$val->post_id]))	{										
													echo '<span id="show_less" style="display:none;"><a href="javascript:void(0)">Show Less</a></span>'; 
												}
											}
											?>
											@php
											$i++;
											@endphp
											@endforeach
											@endisset
										</div>
										<div class="post_comment">
                                            <div class="comnt_by">
                                            @if(auth()->user()->image)
                                                <img class="img-fluid" src="{{ env('APP_URL')}}/{{auth()->user()->image }}" alt="{{ auth()->user()->name }}"/>
                                            @else
                                                <img class="img-fluid" src="{{ asset('dist/images/user_icon.png') }}" alt="{{ auth()->user()->name }}"/>
                                            @endif
                                            </div>
                                            <div class="comnt_field">
                                                <input type="hidden" class="form-control" name="post_id" id="post_id" value="{{$val->post_id}}">
                                                <input type="text" class="form-control" name="add_post_comment" id="add_post_comment_{{$val->post_id}}" onkeydown="callComment({{$val->post_id}})" value="" placeholder="write a comment...">
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    @endforeach
				@endif
				
            </div>
        </div>
    </div>

    <div class="rigt_sidebar">
        
    </div>

    <!-- What_in_Mind_popup -->
    <div class="modal fade what_in_mind_popup" id="staticBackdrop" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content create_post">
                <form id="add_post" enctype="multipart/form-data">
					<div class="crtpost_head">
						<h5>Create Post</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<div class="post_cb">
							<div class="post_by">
								<div class="poster_img"><img src="{{ asset('dashboard/img/prf.png') }}" alt="..."></div>
								<div class="poster_info">
									<div class="poster_name"><b><?php echo $name;?></b></div>
									<div class="post_visible_to">
										<select class="custom-select custom-select-sm" name="post_access" id="post_access">
											<option value="1" selected>&#127760; Public</option>
											<option value="2">&#128101; Friends</option>
											<option value="3">&#128100; Only Me</option>
										</select>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="crtpost_body">
						<div class="post_type">
							<textarea placeholder="Whats on your mind, <?php echo $name;?>?" name="post_content" id="emojionearea"  rows="5"></textarea>
							<div id="galeria"></div>
							<div class="add_to_post mt-2">
								<span>Add to your Post</span>
								<div class="add_btns_grp">
									<img src="{{ asset('dashboard/img/smiles/atp_img.svg') }}" alt="...">
										<input type="file" id="adicionafoto" multiple onchange="previewMultiple(event)" name="adicionafoto[]">
										<span id="filename"></span>
									<!-- <a href="#" id="emojionearea"><img src="{{ asset('dashboard/img/smiles/atp_smile.svg') }}" alt="..."></a> -->
								</div>
							</div>
							<button type="submit" name="postBtn" id="postBtn" class="btn btn-secondary w-100 mt-2">Post</button>
						</div>
					</div>
				</form>
                <div class="crtpost_footer">
                    <ul>
                        <li><a href=""><i class="fa fa-plus-circle"></i> Create Status</a></li>
                        <li><a href=""><i class="fa fa-comment"></i> Create Blogs</a></li>
                        <li><a href=""><i class="fa fa-heartbeat"></i> Health Goals</a></li>
                        <li><a href=""><i class="fa fa-play-circle"></i> Go Live</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
	<!--Comment Box Modal start here--->
	<!--<div class="modal fade what_in_mind_popup" id="likeBoxModal" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content create_post">
                <form id="add_post" enctype="multipart/form-data">
					<div class="crtpost_head">
						<h5>Your post like details</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						
					</div>
					<div class="crtpost_body">
						<div class="post_type" id="liked_html_data">
							
						</div>
					</div>
				</form>
                
            </div>
        </div>
    </div>-->
	<!-----End here------>
	<!---Like Detail popups--->
	<div class="modal fade likes_popup" id="likeBackdrop" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="likeBackdropLabel" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered">
		  <div class="modal-content">
			<div class="modal-header">
				<ul class="nav nav-pills" id="pills-tab" role="tablist">
					<li class="nav-item" role="presentation">
					  <a class="nav-link active" id="pills-all-tab" data-toggle="pill" href="#pills-all" role="tab" aria-controls="pills-all" aria-selected="true">All</a>
					</li>
					<li class="nav-item" role="presentation">
					  <a class="nav-link" id="pills-like-tab" data-toggle="pill" href="#pills-like" role="tab" aria-controls="pills-like" aria-selected="false">
						<img src="{{ asset('dashboard/img/smiles/like.svg') }}" alt=""> Like
					  </a>
					</li>
					<li class="nav-item" role="presentation">
					  <a class="nav-link" id="pills-happy-tab" data-toggle="pill" href="#pills-happy" role="tab" aria-controls="pills-happy" aria-selected="false">
						<img src="{{ asset('dashboard/img/smiles/happy.svg') }}" alt=""> Happy
					  </a>
					</li>
					<li class="nav-item" role="presentation">
						<a class="nav-link" id="pills-in-love-tab" data-toggle="pill" href="#pills-in-love" role="tab" aria-controls="pills-in-love" aria-selected="false">
							<img src="{{ asset('dashboard/img/smiles/in-love.svg') }}" alt=""> In Love
						</a>
					</li>
					<li class="nav-item" role="presentation">
						<a class="nav-link" id="pills-angry-tab" data-toggle="pill" href="#pills-angry" role="tab" aria-controls="pills-angry" aria-selected="false">
							<img src="{{ asset('dashboard/img/smiles/angry.svg') }}" alt=""> Angry
						</a>
					</li>
				</ul>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="tab-content" id="pills-tabContent">
					<div class="tab-pane fade show active" id="pills-all" role="tabpanel" aria-labelledby="pills-all-tab">
						<ul class="liker_list" id="all_like_data">
							
						</ul>
					</div>
					<div class="tab-pane fade" id="pills-like" role="tabpanel" aria-labelledby="pills-like-tab">
						<ul class="liker_list" id="thumb_data">
							
						</ul>
					</div>
					<div class="tab-pane fade" id="pills-in-love" role="tabpanel" aria-labelledby="pills-in-love-tab">
						<ul class="liker_list" id="sad_data">
							
						</ul>
					</div>
					<div class="tab-pane fade" id="pills-happy" role="tabpanel" aria-labelledby="pills-happy-tab">
						<ul class="liker_list" id="love_data">
							
						</ul>
					</div>
					<div class="tab-pane fade" id="pills-angry" role="tabpanel" aria-labelledby="pills-angry-tab">
						<ul class="liker_list" id="angry_data">
							
							
						</ul>
					</div>
				</div>
			</div>
		  </div>
		</div>
	  </div>	
	<!----End Here------->
@endsection

@push("scripts")
    <script src="{{ asset('dashboard/js/lightbox.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/emojionearea/3.4.2/emojionearea.min.js" integrity="sha512-hkvXFLlESjeYENO4CNi69z3A1puvONQV5Uh+G4TUDayZxSLyic5Kba9hhuiNLbHqdnKNMk2PxXKm0v7KDnWkYA==" crossorigin="anonymous"></script>
    <script>
        $(document).ready(function() {
            $("#emojionearea").emojioneArea({
                standalone: false,
                pickerPosition: "bottom",
                filtersPosition: "bottom",
                hidePickerOnBlur: true,
                tones: false,
                hideSource: true
            });
			
			$("#show_more").click(function(){
				$("#show_more").hide();
				$(".show_more").show('slide');
				$("#show_less").show();				
			});
			$("#show_less").click(function(){
				$(".show_more").hide('slide');
				$("#show_less").hide();	
				$("#show_more").show();
			});
        });

		/***Upload media at post***/
		function previewMultiple(event){
			var saida = document.getElementById("adicionafoto");
			var quantos = saida.files.length;
			for(i = 0; i < quantos; i++){
				var urls = URL.createObjectURL(event.target.files[i]);
				document.getElementById("galeria").innerHTML += '<img src="'+urls+'">';
			}
		}

		$("#add_post").submit(function(e) {
			e.preventDefault();
			$(".loader").css('display', 'flex');

			var formData = new FormData(this);
			$.ajax({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				data: formData,
				url: "{{ route('social_post') }}",
				type: "POST",
				cache:false,
				contentType: false,
				processData: false,
				success: function (response) {
					$(".loader").css('display', 'none');
					if(response['status'] == 'success'){
						$("#staticBackdrop").modal('hide');
						//successAlert(response['message'],2000,'top-right');
						callProgressBar();
						setTimeout(function(){
							window.location = response['redirect'];
						}, 2000);
					}
					else{
						errorAlert(response['message'],2000,'top-right');
					}
				},
				error: function (data) {
					$(".loader").css('display', 'none');
					let errors = data.responseJSON.errors;
					$.each(errors, function(key, value) {
						errorAlert(value[0],3000,'top-right');
					});
				}
			});
		});

		/*****Add Comment******/
/* 		function callComment(postid)
		{
			var postid = postid;
			$("#add_post_comment_"+postid).unbind('keypress').bind('keypress', function (e) {
				if(e.which == 13) {
					
					$("#form_post_comment_"+postid).submit(function(e) {
						e.preventDefault();
						$(".loader").css('display', 'flex');

						$.ajax({
							headers: {
								'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
							},
							data: $("#form_post_comment_"+postid).serialize(),
							url: "{{ route('add_post_comment') }}",
							type: "POST",
							success: function (response) {
								$(".loader").css('display', 'none');
								if(response['status'] == 'success'){
									successAlert(response['message'],2000,'top-right');
									setTimeout(function(){
										window.location = response['redirect'];
									}, 2000);
								}
								else{
									errorAlert(response['message'],2000,'top-right');
								}
								event.stopPropagation();
							},
							error: function (data) {
								$(".loader").css('display', 'none');
								let errors = data.responseJSON.errors;
								$.each(errors, function(key, value) {
									errorAlert(value[0],3000,'top-right');
								});
							}
						});
						
				});
				}
			});
		} */

		/****Do like on post*****/
		function doLikePost(id, like_flag)
		{
            let likeFlag = (like_flag == 1) ? 0 : 1;  //1= Post alreday liked,0= post need to unliked
            
			$.ajax({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				data: {"post_id":id, "likeFlag": likeFlag, "flagType":"like_flag"},
				url: "{{ route('add_post_like') }}",
				type: "POST",
				success: function (response) {
					$(".loader").css('display', 'none');
					if(response['status'] == 'success'){
						//successAlert(response['message'],2000,'top-right');
						callProgressBar();
						setTimeout(function(){
							window.location = response['redirect'];
						}, 2000);
					}
					else{
						errorAlert(response['message'],2000,'top-right');
					}
				},
				error: function (data) {
					$(".loader").css('display', 'none');
                    console.log(data);
					let errors = data.responseJSON.errors;
					$.each(errors, function(key, value) {
						errorAlert(value[0],3000,'top-right');
					});
				}
			});
		}
		
		/*******Get Comment Details**********/
		function getLikeDetail(id)
		{
			$("#likeBackdrop").modal('show');
			$(".loader").css('display', 'block');
			$.ajax({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				data: {},
				url: "{{ env('APP_URL').'/get_post_like' }}"+'/'+id,
				success: function (response) {
					$(".loader").css('display', 'none');
					var data = jQuery.parseJSON(response); 
					if(data.status == 'success'){
						var htmlData='';
						var htmlData1='';
						var htmlData2='';
						var htmlData3='';
						var htmlData4='';
						$("#all_like_data").html('');
						$("#thumb_data").html('');
						$("#sad_data").html('');
						$("#love_data").html('');
						$("#angry_data").html('');
						
						/**append all type reaction in modal body****/
						$.each( data.all, function( key, value ) {
							var cimage = "{{ env('APP_URL').'/' }}"+value.image;
							var imgPath = (value.flag==1) ? "{{ env('APP_URL').'/dashboard/img/smiles/like.svg' }}" : ((value.flag==2) ? "{{ env('APP_URL').'/dashboard/img/smiles/happy.svg' }}" : ((value.flag==3) ? "{{ env('APP_URL').'/dashboard/img/smiles/in-love.svg' }}" : "{{ env('APP_URL').'/dashboard/img/smiles/angry.svg' }}" ));
							htmlData = htmlData + '<li><div class="lkr_info"><div class="lkr_img_box"><div class="lkr_rection_icon"><img src="'+imgPath+'" alt=""></div><img src="'+cimage+'" alt="'+value.likeBy+'" class="liker_img"></div><span class="liker_name">'+value.likeBy+'</span></div><a href="javascript:void(0)" class="btn btn-outline-info"><i class="fa fa-user-plus"></i> Follow</a></li>';
						});
						
						/**append all thumb type reaction in modal body****/
						$.each( data.thumb, function( key, value ) {
							var cimage = "{{ env('APP_URL').'/' }}"+value.image;
							var imgPath = "{{ asset('dashboard/img/smiles/like.svg') }}";
							htmlData1 = htmlData1 + '<li><div class="lkr_info"><div class="lkr_img_box"><div class="lkr_rection_icon"><img src="'+imgPath+'" alt=""></div><img src="'+cimage+'" alt="'+value.likeBy+'" class="liker_img"></div><span class="liker_name">'+value.likeBy+'</span></div><a href="javascript:void(0)" class="btn btn-outline-info"><i class="fa fa-user-plus"></i> Follow</a></li>';
						});
						
						/**append all sad type reaction in modal body****/
						$.each( data.sad, function( key, value ) {
							var cimage = "{{ env('APP_URL').'/' }}"+value.image;
							var imgPath = "{{ asset('dashboard/img/smiles/in-love.svg') }}";
							htmlData2 = htmlData2 + '<li><div class="lkr_info"><div class="lkr_img_box"><div class="lkr_rection_icon"><img src="'+imgPath+'" alt=""></div><img src="'+cimage+'" alt="'+value.likeBy+'" class="liker_img"></div><span class="liker_name">'+value.likeBy+'</span></div><a href="javascript:void(0)" class="btn btn-outline-info"><i class="fa fa-user-plus"></i> Follow</a></li>';
						});
						
						/**append all love type reaction in modal body****/
						$.each( data.love, function( key, value ) {
							var cimage = "{{ env('APP_URL').'/' }}"+value.image;
							var imgPath = "{{ asset('dashboard/img/smiles/happy.svg') }}";
							htmlData3 = htmlData3 + '<li><div class="lkr_info"><div class="lkr_img_box"><div class="lkr_rection_icon"><img src="'+imgPath+'" alt=""></div><img src="'+cimage+'" alt="'+value.likeBy+'" class="liker_img"></div><span class="liker_name">'+value.likeBy+'</span></div><a href="javascript:void(0)" class="btn btn-outline-info"><i class="fa fa-user-plus"></i> Follow</a></li>';
						});
						
						/**append all angey type reaction in modal body****/
						$.each( data.angry, function( key, value ) {
							var cimage = "{{ env('APP_URL').'/' }}"+value.image;
							var imgPath = "{{ asset('dashboard/img/smiles/angry.svg') }}";
							htmlData4 = htmlData4 + '<li><div class="lkr_info"><div class="lkr_img_box"><div class="lkr_rection_icon"><img src="'+imgPath+'" alt=""></div><img src="'+cimage+'" alt="'+value.likeBy+'" class="liker_img"></div><span class="liker_name">'+value.likeBy+'</span></div><a href="javascript:void(0)" class="btn btn-outline-info"><i class="fa fa-user-plus"></i> Follow</a></li>';
						});
						
						$("#all_like_data").html(htmlData);
						$("#thumb_data").html(htmlData1);
						$("#sad_data").html(htmlData2);
						$("#love_data").html(htmlData3);
						$("#angry_data").html(htmlData4);
					}
					else{
						errorAlert(data.status,2000,'top-right');
					}
				},
				error: function (data) {
					$(".loader").css('display', 'none');
					let errors = data.responseJSON.errors;
					$.each(errors, function(key, value) {
						errorAlert(value[0],3000,'top-right');
					});
				}
			});
		}
		
		$(document).ready(function() {
			$(".reactions-box li").on("click",function(){
				var flagType = $(this).data("reaction");
				flagType = (flagType=="Like") ? "like_flag" : ((flagType=="Love") ? "love_flag" : ((flagType=="Sad") ? "sad_flag" : "angary_flag" ));
				var postId   = $(this).data("postid");
				var reactionFlag = $(this).data("likeflag");
				reactionFlag = (reactionFlag == 1) ? 0 : 1;  //1= Post alreday liked,0= post need to unliked
				
				$.ajax({
					headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					},
					data: {"post_id":postId, "likeFlag": reactionFlag, "flagType":flagType},
					url: "{{ route('add_post_like') }}",
					type: "POST",
					success: function (response) {
						$(".loader").css('display', 'none');
						if(response['status'] == 'success'){
							//successAlert(response['message'],2000,'top-right');
							callProgressBar();
							setTimeout(function(){
								window.location = response['redirect'];
							}, 2000);
						}
						else{
							errorAlert(response['message'],2000,'top-right');
						}
					},
					error: function (data) {
						$(".loader").css('display', 'none');
						console.log(data);
						let errors = data.responseJSON.errors;
						$.each(errors, function(key, value) {
							errorAlert(value[0],3000,'top-right');
						});
					}
				});	
			});
		});
    </script>
	<!--<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.1';
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>-->
@endpush
