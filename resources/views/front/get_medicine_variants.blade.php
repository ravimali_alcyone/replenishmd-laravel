			<div class="container number_uses">

				<div class="num_uses">
					<p>DOSSAGE</p>
					<h5>Do You have any dosage preference for {{$medicine->name}}?</h5>

					<div class="my_dosage dosage_options">
						<p>New to medication? start with:</p>
						
			@if($variants)
				@foreach($variants as $key => $value)
					@if($value->is_popular == 1)
						<div class="dosage_details popular">
							<div class="dosage_select">
								<div class="check_1">
									<input type="radio" id="dosages_{{$value->id}}" name="variants" onclick="dosageSelect(this)" value="{{$value->id}}">
									<label for="dosages_{{$value->id}}"><span>{{$value->variant_name}}</span> </label>
									<p>{{$value->pill_qty}} pills</p>

								</div>
								<div class="drug_p ml-auto">
									<Span>${{$value->price}}</Span>
									<p>per dose</p>
								</div>
							</div>

							<div class="dosage_intro">
								<div>
									<i class="fas fa-lightbulb"></i>
									<p>{{$value->details}}</p>
								</div>
							</div>
						</div>
						
						<div class="dosage_msg">
							<h5>If you know what works for you:</h5>
								<p>If you used the medication before, and it worked without side effects, you should choose the same dose.</p>
						</div>						
					@else
						<div class="dosage_details">
							<div class="dosage_select">
								<div class="check_1">
									<input type="radio" id="dosages_{{$value->id}}" name="variants" onclick="dosageSelect(this)" value="{{$value->id}}">
									<label for="dosages_{{$value->id}}"><span>{{$value->variant_name}}</span> </label>
									<p>{{$value->pill_qty}} pills</p>

								</div>
								<div class="drug_p ml-auto">
									<Span>${{$value->price}}</Span>
									<p>per dose</p>
								</div>
							</div>
						</div>
					@endif
				@endforeach
			@endif						
					</div>
				</div>
			</div>