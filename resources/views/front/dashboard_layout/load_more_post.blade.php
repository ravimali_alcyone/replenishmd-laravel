<div class="mid_wrap">
	<div class="row">
		<div class="col-md-12">
			<div id="fb-root"></div>
			@if($post && count($post)>0)
				@foreach($post as $val)
					<div class="post_wrap">
						<div class="post_head">
							<div class="post_by">
								<div class="poster_img">
									@if(auth()->user()->image)
										<img src="{{ env('APP_URL')}}/{{auth()->user()->image }}" alt="{{ auth()->user()->name }}"/>
									@else
										<img src="{{ asset('dist/images/user_icon.png') }}" alt="{{ auth()->user()->name }}"/>
									@endif
								</div>
								<div class="poster_info">
									<div class="poster_name"><b>{{$val->postBy}}</b></div>
									<div class="post_date"><span><a href="{{env('APP_URL')}}/post/{{$val->param_id}}/0">{{$val->created_at}}</a></span></div>
								</div>
							</div>
							<div class="dropdown post_setting">
								<button class="dots_btn" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									<span>&#8411;</span>
								</button>
								<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
									<a class="dropdown-item" href="#">Hide Post</a>
									<a class="dropdown-item" href="#">Block This Person</a>
									<a class="dropdown-item" href="#">Report</a>
								</div>
							</div>
						</div>
						<div class="post_body">
							<div class="post_text">
								<p>@if($val->post_content){{$val->post_content}} @endif</p>
							</div>
							<div class="post_imgs">
								@isset($posts_media[$val->post_id])
									@foreach($posts_media[$val->post_id] as $key => $value)
										<a class="example-image-link {{ count($posts_media[$val->post_id]) == 1 ? 'mw-100' : '' }}" href="/{{ $value }}" data-lightbox="lightbox-set-{{ $val->post_id }}">
											<img class="example-image" src="/{{ $value }}" alt="...">
										</a>
									@endforeach
									@if(count($posts_media[$val->post_id]) > 4)
										<span class="more_imgs_caption">{{ count($posts_media[$val->post_id]) - 4 }} More</span>
									@endif
								@endisset
							</div>
							<div class="post_reactions">
								<div class="post_likes">
									<div class="top_three_recent">
										<div><img src="{{ asset('dashboard/img/smiles/like.svg') }}" alt="..."></div>
										<div><img src="{{ asset('dashboard/img/smiles/in-love.svg') }}" alt="..."></div>
										<div><img src="{{ asset('dashboard/img/smiles/angry.svg') }}" alt="..."></div>
									</div>
									@if(isset($likePostUserData[$val->post_id]))
										@foreach($likePostUserData[$val->post_id] as $key => $value)
											<span><a href="javascript:void(0)" id="{{$val->post_id}}" onclick="getLikeDetail(this.id)"><?php 
											
											if(($val->num_of_post_like)>3)
											{
												if (strpos($str, 'You') !== false) {
													echo 'You,'.trim(preg_replace("/,+/", ",", str_replace("You","",$value)), ",").',&'.($val->num_of_post_like - 3).' others';
												}
											}
											else 
											{ 
												echo trim('You,'.trim(preg_replace("/,+/", ",", str_replace("You","",$value)), ","), ",");
											}
											?> </a></span>
										@endforeach
									@endisset
								</div>
								<div class="post_status">
									<div class="post_s_type"><span id="{{$val->post_id}}" onclick="getCommentDetail(this.id)">Comments</span><span>{{$val->num_of_post_commnet}}</span></div>
									<div class="post_s_type">Share<span>{{$val->num_of_post_share}}</span></div>
								</div>
							</div>
							<div class="posting_btn_grp">
								<div class="rec_btn">
								
									@if(isset($post_likes[$val->post_id]))
										<span class="like-btn {{ $post_likes[$val->post_id] == 1 ? 'reacted' : '' }}">
									@else
										<span class="like-btn">
									@endif
										<span class="like-btn-text" id="{{$val->post_id}}" onclick="doLikePost(this.id, {{ isset($post_likes[$val->post_id]) ? $post_likes[$val->post_id] : 0 }})"><i class="fa fa-thumbs-up"></i> Like</span>
										<ul class="reactions-box">
											@if(isset($post_likes_flagData[$val->post_id]))
											@foreach($post_likes_flagData[$val->post_id] as $key => $value)
												<li class="reaction reaction-like" data-reaction="Like" data-postId="{{$val->post_id}}" data-likeFlag="{{ $value['like_flag'] }}"></li>
												<li class="reaction reaction-love" data-reaction="Love" data-postId="{{$val->post_id}}" data-likeFlag="{{ $value['love_flag'] }}"></li>
												<li class="reaction reaction-sad" data-reaction="Sad" data-postId="{{$val->post_id}}" data-likeFlag="{{ $value['sad_flag'] }}"></li>
												<li class="reaction reaction-angry" data-reaction="Angry" data-postId="{{$val->post_id}}" data-likeFlag="{{ $value['angary_flag'] }}"></li>
											@endforeach
											@else
												<li class="reaction reaction-like" data-reaction="Like" data-postId="{{$val->post_id}}" data-likeFlag="0"></li>
												<li class="reaction reaction-love" data-reaction="Love" data-postId="{{$val->post_id}}" data-likeFlag="0"></li>
												<li class="reaction reaction-sad" data-reaction="Sad" data-postId="{{$val->post_id}}" data-likeFlag="0"></li>
												<li class="reaction reaction-angry" data-reaction="Angry" data-postId="{{$val->post_id}}" data-likeFlag="0"></li>
											@endisset
										</ul>
									</span>
								</div>
								<div class="rec_btn"><a data-toggle="collapse" href="#collapseComment_{{$val->post_id}}" role="button" aria-expanded="false" aria-controls="collapseComment"><i class="fa fa-comment"></i> Comment</a></div>
								<div class="dropdown">
									<div class="rec_btn" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><a href="javascript:void(0)"><i class="fa fa-share"></i> Share</a></div>
									<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
										<a class="dropdown-item" href="javascript:void(0)">Only me</a>
										<div class="fb-like" data-href="https://developers.facebook.com/docs/plugins/" data-layout="standard" data-action="like" data-size="small" data-show-faces="true" data-share="true"></div>
										<a class="dropdown-item" href="javascript:void(0)">Via Facebook</a>
										<a class="dropdown-item" href="javascript:void(0)">Via LinkedIn</a>
									</div>
								</div>
							</div>
						</div>
						<div class="post_comment_block">
							<form id="form_post_comment_{{$val->post_id}}">
								<div class="collapse" id="collapseComment_{{$val->post_id}}">
									<div>
										@if(isset($post_comment[$val->post_id]))
										@php
										$i=1;
										@endphp
										@foreach($post_comment[$val->post_id] as $key => $value)
										<?php if($i<6) {?>
										<div class="media">
											<div class="poster_img">
											@if($value['image'])
												<img src="{{ env('APP_URL')}}/{{$value['image'] }}" alt="{{ auth()->user()->name }}" />
											@else
												<img src="{{ asset('dist/images/user_icon.png') }}" alt="{{ auth()->user()->name }}" />
											@endif
											</div>
											<div class="media-body">
												<h6 class="mt-0">{{$value['commentBy'] }}</h6>
												<p>{{ $value['comments'] }}</p>
											</div>
										</div>
										<?php } 
										elseif($i>=6) {
											if($i==6){ echo '<span id="show_more"><a href="javascript:void(0)">Show more</a></span>';}		
										?>
										<div class="media show_more" style="display:none;">
											<div class="poster_img">
											@if($value['image'])
												<img src="{{ env('APP_URL')}}/{{$value['image'] }}" alt="{{ auth()->user()->name }}" />
											@else
												<img src="{{ asset('dist/images/user_icon.png') }}" alt="{{ auth()->user()->name }}" />
											@endif
											</div>
											<div class="media-body">
												<h6 class="mt-0">{{$value['commentBy'] }}</h6>
												<p>{{ $value['comments'] }}</p>
											</div>
										</div>
										
										<?php
											if($i==count($post_comment[$val->post_id]))	{										
												echo '<span id="show_less" style="display:none;"><a href="javascript:void(0)">Show Less</a></span>'; 
											}
										}
										?>
										@php
										$i++;
										@endphp
										@endforeach
										@endisset
									</div>
									<div class="post_comment">
										<div class="comnt_by">
										@if(auth()->user()->image)
											<img class="img-fluid" src="{{ env('APP_URL')}}/{{auth()->user()->image }}" alt="{{ auth()->user()->name }}"/>
										@else
											<img class="img-fluid" src="{{ asset('dist/images/user_icon.png') }}" alt="{{ auth()->user()->name }}"/>
										@endif
										</div>
										<div class="comnt_field">
											<input type="hidden" class="form-control" name="post_id" id="post_id" value="{{$val->post_id}}">
											<input type="text" class="form-control" name="add_post_comment" id="add_post_comment_{{$val->post_id}}" onkeydown="callComment({{$val->post_id}})" value="" placeholder="write a comment...">
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
				@endforeach
			@endif
		</div>
	</div>
    </div>


    

