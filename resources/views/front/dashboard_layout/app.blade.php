<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        @if(isset($meta_description))<meta name="description" content="{{$meta_description}}">@endif
        <title>{{ config('app.name') }} @if(isset($meta_title)) - {{$meta_title}} @endif</title>
        <link href="{{asset('assets/images/favicon.png')}}" rel="shortcut icon" type="image/x-icon">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <!-- Google Font -->
        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Lato:wght@400;700&display=swap" rel="stylesheet">
        <!-- Font Awesome -->
        <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
        <!-- Bootstrap CSS -->
        <link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
        <!-- Style.css -->
        <link rel="stylesheet" type="text/css" href="{{asset('dashboard/css/style.css')}}">
        <link rel="stylesheet" href="{{asset('plugins/sweetalert/sweetalert.css')}}" />
        <link href="{{asset('assets/css/jquery-toaster.css')}}" rel="stylesheet" />
        <link href="{{asset('assets/css/owl.carousel.css')}}" rel="stylesheet" />
        <link href="{{asset('dashboard/css/dash_head.css')}}" rel="stylesheet" />
        @yield('stylesheet')
        
        <!-- Emoji -->
        <link rel="stylesheet" type="text/css" href="https://cdn.rawgit.com/mervick/emojionearea/master/dist/emojionearea.min.css">
        

        <!-- Main Dashbord Content Ends-->
        <script src="{{asset('assets/js/jquery.min.js')}}"></script>
		<!-- Blog listing page -->
		<script src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"></script>
		<link rel="stylesheet" href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.min.css" />		
		
        <script src="{{asset('plugins/sweetalert/sweetalert.min.js')}}"></script>
        <script src="{{asset('assets/js/bootstrap.bundle.min.js')}}"></script>

        <!-- Select2 -->
        <link href="{{ asset('assets/css/select2.min.css') }}" rel="stylesheet" />
        <script src="{{ asset('assets/js/select2.min.js') }}"></script>

        <!-- Full Calendar -->
        <script src="{{ asset('dashboard/plugins/fullcalendar/moment.min.js') }}"></script>
        <link rel="stylesheet" href="{{ asset('dashboard/plugins/fullcalendar/fullcalendar.css') }}" />
        <script src="{{ asset('dashboard/plugins/fullcalendar/fullcalendar.js') }}"></script>
        <script src="{{ asset('dashboard/plugins/fullcalendar/popper.min.js') }}"></script>
				
		<!--Datetimepicker-->
		<link href="{{ asset('assets/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet" />
		<script src="{{ asset('assets/js/bootstrap-datetimepicker.min.js') }}"></script>		
        <!---Fb Gallery script----->
        <script src="{{ asset('dashboard/js/fb_gallery.js') }}"></script>
        <script src="{{asset('assets/js/jquery-toaster.min.js')}}"></script>
        <script src="{{asset('assets/js/owl.carousel.js')}}" type="text/javascript"></script>
        <script src="{{asset('assets/js/custom.js')}}"></script>
        <style>
            .dash_top_process {   position: fixed;    top: 0;    width: 100%;    z-index: 1050;    left: 0;    right: 0;}
            .dash_top_process .progress{height: 5px;border-radius:0px;overflow: visible;}
            .dash_top_process .progress-bar{background-color: #ff6c66;box-shadow: 0 3px 6px rgba(255, 108, 102, 0.3);overflow: visible;}

			div#pdNotify_img{box-shadow:0 10px 20px rgba(0,0,0,.16);position:absolute;font-size:12px;line-height:20px;width:300px;background:#ffff;border:solid 1px #f5f5f5;padding:0;border-radius:4px;top:55px;z-index:99;right:0}

			.notice_text{
				position: absolute;
				top: -8px;
				right: 0;
				border-radius: 50%;
				font-size: 10px;
				height: 20px;
				width: 20px;
				text-align: center;
				padding-top: 2px;
			}

		</style>		
        
        @stack('header_scripts')
    </head>
    <body class="pb-70">
		@include('front.dashboard_layout.header')

		<!-- Main Section Side Bar -->
        <section class="main-dashboard mt-5 pt-5">
            <div class="container-fluid">
                <div class="row">
                    <div class="col">
                        <div class="dash_top_process" id="pbar" style="display: none;">
                            <div class="progress">
                              <div class="progress-bar" role="progressbar" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row">
					@if(auth()->user()->is_first_login == 1)
					
					<div class="col-xl-12 col-lg-10 col-md-11 col-sm-12">
						@yield('content')
					</div>
					@else
					<div class="col-xl-2 col-lg-4 col-md-3 col-sm-12">
						<div class="social_postings">
							@if(auth()->user()->is_first_login == 0)
								@include('front.dashboard_layout.sidebar')
							@endif
						</div>
					</div>
					<div class="col-xl-10 col-lg-8 col-md-9 col-sm-12">
						@yield('content')
					</div>
					@endif
                </div>
            </div>
        </section>

		@include('front.dashboard_layout.footer')

		<script>
			$(document).on('click', function() {
				$('.drop_collapse').collapse('hide');
			})
			$(document).ready(function() {
				$('#story_board').owlCarousel({
					loop: false,
					margin: 8,
					responsiveClass: true,
					nav: true,
					autoplay: false,
					responsive: {
						0: {
							items: 3
						},
						400: {
							items: 3,

						},
						600: {
							items: 4,
						},

						800: {
							items: 5,
						},
						900: {
							items: 6,
						},
						992: {
							items: 4,
						},
						1300: {
							items: 6,
						}
					}
				});
			});
		</script>

        @stack("scripts")
	</body>
</html>