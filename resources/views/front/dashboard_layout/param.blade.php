<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        @if(isset($meta_description))<meta name="description" content="{{$meta_description}}">@endif
        <title>{{ config('app.name') }} @if(isset($meta_title)) - {{$meta_title}} @endif</title>
        <link href="{{asset('assets/images/favicon.png')}}" rel="shortcut icon" type="image/x-icon">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <!-- Google Font -->
        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Lato:wght@400;700&display=swap" rel="stylesheet">
        <!-- Font Awesome -->
        <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
        <!-- Bootstrap CSS -->
        <link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
        <!-- Style.css -->
        <link rel="stylesheet" type="text/css" href="{{asset('dashboard/css/style.css')}}">
        <link rel="stylesheet" href="{{asset('plugins/sweetalert/sweetalert.css')}}" />
        <link href="{{asset('assets/css/jquery-toaster.css')}}" rel="stylesheet" />
        <link href="{{asset('assets/css/owl.carousel.css')}}" rel="stylesheet" />
        <link href="{{asset('dashboard/css/dash_head.css')}}" rel="stylesheet" />
        <!-- Emoji -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/emojionearea/3.4.2/emojionearea.css" integrity="sha512-0Nyh7Nf4sn+T48aTb6VFkhJe0FzzcOlqqZMahy/rhZ8Ii5Q9ZXG/1CbunUuEbfgxqsQfWXjnErKZosDSHVKQhQ==" crossorigin="anonymous" />

        <!-- Main Dashbord Content Ends-->
        <script src="{{asset('assets/js/jquery.min.js')}}"></script>
        <script src="{{asset('plugins/sweetalert/sweetalert.min.js')}}"></script>
        <script src="{{asset('assets/js/bootstrap.bundle.min.js')}}"></script>

        <!-- Select2 -->
        <link href="{{ asset('assets/css/select2.min.css') }}" rel="stylesheet" />
        <script src="{{ asset('assets/js/select2.min.js') }}"></script>

        <!-- Full Calendar -->
        <script src="{{ asset('dashboard/plugins/fullcalendar/moment.min.js') }}"></script>
        <link rel="stylesheet" href="{{ asset('dashboard/plugins/fullcalendar/fullcalendar.css') }}" />
        <script src="{{ asset('dashboard/plugins/fullcalendar/fullcalendar.js') }}"></script>
        <script src="{{ asset('dashboard/plugins/fullcalendar/popper.min.js') }}"></script>

        <script src="{{asset('assets/js/jquery-toaster.min.js')}}"></script>
        <script src="{{asset('assets/js/owl.carousel.js')}}" type="text/javascript"></script>
        <script src="{{asset('assets/js/custom.js')}}"></script>
		
    </head>
    <body class="pb-70">
		@include('front.dashboard_layout.header')

		<!-- Main Section Side Bar -->
		<section class="main-dashboard mt-30">
            <div class="container-fluid">
                <div class="row">
                    <div class="col social_postings">
                        <div class="left_side_menu"></div>

						@yield('content')
                    </div>
                </div>
            </div>
        </section>

		@include('front.dashboard_layout.footer')

		<script>
			$(document).on('click', function() {
				$('.drop_collapse').collapse('hide');
			})
			$(document).ready(function() {
				$('#story_board').owlCarousel({
					loop: false,
					margin: 8,
					responsiveClass: true,
					nav: true,
					autoplay: false,
					responsive: {
						0: {
							items: 3
						},
						400: {
							items: 3,

						},
						600: {
							items: 4,
						},

						800: {
							items: 5,
						},
						900: {
							items: 6,
						},
						992: {
							items: 4,
						},
						1300: {
							items: 6,
						}
					}
				});
			});
		</script>
		

        @stack("scripts")
	</body>
</html>