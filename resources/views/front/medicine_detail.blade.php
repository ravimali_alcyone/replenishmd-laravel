@extends('front.layouts.app')

@section('content')

    <!---------------------Main Section--------------------->
	@php
		$images = json_decode($medicine->images,true);
		
		if($images){
			$display_img = $images[0];
		}else{
			$display_img = 'assets/images/default_medicine.jpg';
		}
	@endphp	
    <section class="medicine_intro">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="sildenaficitrate_details  ">
                        <img class="roman" src="{{env('APP_URL')}}/{{$display_img}}" alt="{{$medicine->name}}">

                    </div>
                </div>

                <div class="col-md-6">
                    <div class="medicine_details">
                        <h5><a href="/treatment?g={{$gender}}&t={{$service_detail->id}}">{{$service_detail->name}}</a> / {{$medicine->name}}</h5>
                        <h2> {{$medicine->other_name}}</h2>
							{!! $medicine->description !!}
                        <button class="Visit_online  py-3 px-3">
                                <a  class="start_link" href="{{route('signup')}}">Start online visit</a>
                            </button>
                        <div class="heath_wrapper">
                            <div class="health_details ">
                                <img class="icon_plus " src="{{asset('assets/images/plus.png')}}">
                                <p>
                                    U.S.-licensed healthcare professionals
                                </p>

                            </div>

                            <div class="health_details_1 ">
                                <img class="icon_plus " src="{{asset('assets/images/plus.png')}}">
                                <p>
                                    Licensed pharmacy
                                </p>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!------------------FAQ Section-------------------->
	@if($medicine_faqs && $medicine_faqs->count() > 0)
		<section class="Overview">
			<div class="container">
				<div class="row">
					<div class="col-md-6">
						<div class="Overview_list ">
							<ul>
							@foreach($medicine_faqs as $key => $value)
								<li><a href="#medicine_faq_{{$value->id}}">{{$value->question_data}}</a></li>
							@endforeach
							</ul>
						</div>
					</div>
					<div class="col-md-6">
						<div class="viagra_banner">
						@foreach($medicine_faqs as $key => $value)
							<div id="medicine_faq_{{$value->id}}">
								<h3>{{$value->question_data}}</h3>

									{!! $value->answer_data!!}
							</div>
						@endforeach
						</div>
					</div>

				</div>

			</div>

		</section>
	@endif

@endsection