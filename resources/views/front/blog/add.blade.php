@extends('front.common_layout.app')
	<script src="https://unpkg.com/react@16.8.6/umd/react.production.min.js"></script>
  	<script src="https://unpkg.com/react-dom@16.8.6/umd/react-dom.production.min.js"></script> 
	
		<link rel="stylesheet" href="https://unpkg.com/dropzone/dist/dropzone.css" />
		<link href="https://unpkg.com/cropperjs/dist/cropper.css" rel="stylesheet"/>
		<script src="https://unpkg.com/dropzone"></script>
		<script src="https://unpkg.com/cropperjs"></script>
		
@section('content')
	@php $type = auth()->user()->user_role == 4 ? 'user' : 'provider'; @endphp

  	<script src="{{ asset('vendor/laraberg/js/laraberg.js') }}"></script>
  	<script src="{{ asset('js/admin-app.js') }}" defer></script>
	<link rel="stylesheet" href="{{asset('vendor/laraberg/css/laraberg.css')}}">
	
    <link rel="stylesheet" href="{{asset('css/admin-app.css')}}">  

	<style>

		#coverImage .image_preview .pip img, #headerImage .image_preview .pip img {
			height: 360px;
			width: 100%;
			border: 3px solid black;
		}
		
		.crop-preview {
  			overflow: hidden;
  			width: 160px; 
  			height: 160px;
  			margin: 10px;
  			border: 1px solid red;
		}

		.modal-lg{
  			max-width: 1000px !important;
		}

		.overlay {
		  position: absolute;
		  bottom: 10px;
		  left: 0;
		  right: 0;
		  background-color: rgba(255, 255, 255, 0.5);
		  overflow: hidden;
		  height: 0;
		  transition: .5s ease;
		  width: 100%;
		}

		.image_area:hover .overlay {
		  height: 50%;
		  cursor: pointer;
		}
		
		.select2-container--default .select2-selection--single {
			min-height: 37px;
		}		
		
		.select2-container--default .select2-selection--single .select2-selection__rendered {
			line-height: 32px;
		}		
	</style>
	
	@if(auth()->user()->user_role==3)
		<div class="col-xl-10 col-lg-8 col-md-8 col-sm-12">
	@endif
		<div class="flex flex-col sm:flex-row items-center mt-4 border-b border-gray-200 dark:border-dark-5">
			<h3 class="font-medium text-base mr-auto"><a href="<?php echo route($type.'.blogs'); ?>">Blogs List</a> > @if($blog) {{ 'Edit' }} @else Add New @endif Blog</h3>
		</div>
		
		<div class="col-xl-12 col-lg-12 col-md-12">
			<form id="blogForm">
				<fieldset class="uk-fieldset">
					<div class="laraberg-sidebar">
						<textarea name="excerpt" placeholder="Excerpt" rows="10"></textarea>
					</div>
					<div class="uk-margin">
						<input id="article-title" type="text" class="uk-input uk-form-large {{ $errors->get('title') ? 'uk-form-danger' : '' }}" name="name" placeholder="Title" value="<?php if($blog){ echo $blog->title;}?>" />
					</div>
					<div class="uk-margin">
						<textarea name="description" id="description" hidden><?php if($blog){ echo $blog->blog_content; }?></textarea>
					</div>
				</fieldset>
				
				<div class="row mt-2 ml-1 mr-1 bg-white pd-20-30 hover-effect-box">
					<div class="col-xl-9 col-lg-9 col-md-9">
						<div class="form-row">
						
						  <div class="form-group col-md-6 has-feedback">
							<label>Category</label>
							<div class="mt-2 category_cls">
								<select id="category_id" name="category" class="custom-select select2" onchange="getCategoryTopics(this)" required>
									<option value="">--Select--</option>
									@if($categories && $categories->count() > 0)
										@foreach($categories as $category)
											<option value="{{ $category->id }}" <?php if($blog && $blog->blog_category_id == $category->id){ echo 'selected';}?> >{{ $category->name }}</option>
										@endforeach
									@endif
									<option value="other">Other</option>
								</select>
								
								<div class="input-group d-none"  id="custom_category_id">
									<input type="text" name="custom_category" id="custom_category" value="" class="form-control">
									<div class="input-group-append">
										<button class="btn btn-primary" type="button" onclick="show_cat_option()"><i class="fa fa-refresh" aria-hidden="true"></i></button>
									</div>
								</div>
							</div>
						  </div>
						  
						  <div class="form-group col-md-6 has-feedback">
							<label>Tags (Seprate with comma)</label>
							<div class="mt-2">
							<input type="text" class="form-control" name="tags" id="tags" value="<?php if($blog){ echo $blog->post_tags; }?>"/>
							</div>
						  </div>
						  
						  <div class="form-group col-md-12 has-feedback">
							<label>Topics</label>
							<div class="mt-2" id="topic_opt">							
								<select id="topic_ids" name="topics[]" class="custom-select select2" multiple>
									@if($topics && $topics->count() > 0)
										@php 
											$cat_topics  = $blog && $blog->category_topic_ids != '' ? explode(',' , $blog->category_topic_ids) : array();
										@endphp
										
										@foreach($topics as $topic)
											<option value="{{ $topic->id }}" @if(in_array($topic->id,$cat_topics)) selected @endif >{{ $topic->topic }}</option>
										@endforeach
									@endif
								</select>	
							</div>
							<div class="mt-2 d-none" id="topic_inp">							
								<input type="text" name="custom_topic" id="custom_topic" value="" class="form-control">		
							</div>
						  </div>						  
						  
						  <div class="form-group col-md-6 has-feedback">
							<div class="mt-2">
								<label>Permalink</label>
								<input type="text" name="param_url" class="form-control" placeholder="{{env('APP_URL')}}/blogs/" disabled>
							</div>
						  </div>
						  <div class="form-group col-md-6 has-feedback">
							<div class="mt-2">
								<label>Slug</label>
								<input type="text" name="slug" value="<?php if($blog){ echo $blog->slug;}?>" class="form-control" placeholder="Permalink" id="slug" required>
							</div>
						  </div>			  
						</div>
					</div>

					
					<div class="col-xl-3 col-lg-3 col-md-3 pull-right">

						<div class="col-md-12 mt-3">
							<label><b>Visibility</b></label>
							<div class="mt-1">
								<input type="radio" id="visibility_1" name="visibility" value="public" <?php if(!$blog){ echo 'checked';} ?> class="mr-2" <?php if($blog && $blog->post_visibility==1){ echo "checked"; } ?> ><label for="visibility_1" class="mr-4">Public</label>
								
								<input type="radio" id="visibility_0" name="visibility" value="private" class="mr-2" <?php if($blog && $blog->post_visibility==0){ echo "checked"; } ?> ><label for="visibility_0">Private</label>
							</div>
						</div>
						<div class="col-md-12 mt-3">
							<label><b>Publish</b></label>
							<div class="mt-1">
							
								<input type="radio" id="publish_1" name="publish" value="immediate" <?php if(!$blog){ echo 'checked';} ?> class="mr-2 post_publish" <?php if($blog && $blog->post_publish==1){ echo "checked"; } ?> onchange="setSchedulePublish(this,1);" ><label for="publish_1" class="mr-4">Immediate</label>
								
								<input type="radio" id="publish_0" name="publish" value="draft" class="mr-2 post_publish" <?php if($blog && $blog->post_publish==0){ echo "checked"; } ?> onchange="setSchedulePublish(this,0);"><label for="publish_0" class="mr-4">Draft</label>
								
								<input type="radio" id="publish_2" name="publish" value="schedule" class="mr-2 post_publish" <?php if($blog && $blog->post_publish==2){ echo "checked"; } ?> onchange="setSchedulePublish(this,2);" ><label for="publish_2">Schedule</label>
								
							</div>
							<div id="schedulePostDiv" class="mt-2" style="<?php if($blog && $blog->post_publish == 2){ echo 'display:block'; }else{ echo 'display:none;';} ?>" >
								<label>Schedule Publish at</label>
								<input type="text" id="scheduled_at" name="scheduled_at" value="<?php if($blog && $blog->scheduled_at != NULL){ echo date('d/m/Y H:i:s', strtotime($blog->scheduled_at));}?>" autocomplete="off" data-DateTimePicker="<?php if($blog && $blog->scheduled_at != NULL){ echo date('d/m/Y H:i:s', strtotime($blog->scheduled_at));}?>" class="form-control" <?php if($blog && $blog->post_publish==2){ echo "required"; } ?> >
							</div>
						</div>
						<div class="col-md-12 mt-3">
							<label><b>Allow Comment</b></label>
							<div class="mt-1">
								<input type="checkbox" name="alw_comment" id="alw_comment" value="1" class="form-checkbox" <?php if(!$blog){ echo 'checked';} ?> <?php if($blog && $blog->post_comment==1){ echo "checked"; } ?>>
							</div>
						</div>
					</div>
					
					<div class="col-xl-12 col-lg-12 col-md-12 mt-4">
						<div class="form-row">						

							<div class="form-group col-md-4 has-feedback" id="headerImage">
								<label>Featured(Header) Image</label>
								<input type="file" name="header_image" class="form-control" id="blog_header_image" style="line-height: 16px;">
								<div class="image_preview mt-3">
									@if($blog && $blog->header_image != '')
										
										<input type="hidden" name="old_header_image" value="{{ $blog->header_image }}">
										<span class="pip"><img class="imageThumb" src="/{{ $blog->header_image }}"></span>
									@endif
								</div>
							</div>	
							<div class="form-group col-md-1"></div>
							<div class="form-group col-md-4 has-feedback" id="coverImage">
								<label>Cover Image</label>
								<input type="file" name="image" class="form-control" id="cover_image" style="line-height: 16px;">
								<div class="image_preview mt-3">
									@if($blog && $blog->image != '')
										
										<input type="hidden" name="old_image" value="{{ $blog->image }}">
										<span class="pip"><img class="imageThumb" src="/{{ $blog->image }}"></span>
									@endif
								</div>
							</div>	
							
						</div>							
					</div>
					
				</div>
				<div class="col-xl-12 col-lg-12 col-md-12 mt-12">
					<div class="mt-5">
						<input type="hidden" name="id" value="<?php if($blog){ echo $blog->id;}?>"/>
						<input type="hidden" name="page" value="<?php if($blog){ echo 'edit';} else { echo 'add'; }?>"/>
						<input type="hidden" name="manul_cat_flag" id="manul_cat_flag" value="">
						<button type="submit" id="saveBtn" class="btn btn-primary btn-lg">Save</button>
						<button type="reset" class="btn btn-danger btn-lg ml-2" onclick="location.href = '<?php echo route($type.'.blogs');?>';">Cancel</button>
					</div>

				</div>
			</form>
		</div>
	@if(auth()->user()->user_role==3)
		</div>
	@endif		


	<div class="modal fade" id="cropImageModal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-body">
					<div class="img-container">
						<div class="row">
							<div class="col-md-8">
								<img src="" id="sample_image" />
							</div>
							<div class="col-md-4">
								<div class="crop-preview"></div>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" id="crop" class="btn btn-primary">Crop</button>
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
				</div>
			</div>
		</div>
	</div>	
		
<script>

	$("#scheduled_at").datetimepicker({
		format: 'DD/MM/YYYY HH:mm:ss',
		showClose: true,
		minDate: new Date(),
	});
	
	$('#scheduled_at').data("DateTimePicker").show();

	function setSchedulePublish(e,val){
		if($(e).is(":checked") && val == '2'){ //schedule
			$("#schedulePostDiv").show();
			$("#scheduled_at").attr('required',true);
		}else{
			$("#schedulePostDiv").hide();
			$("#scheduled_at").val('');
			$("#scheduled_at").attr('required',false);
		}
	}
	
var cropImageData = '';
$(document).ready(function(){
	
	var $modal = $('#cropImageModal');
	var image = document.getElementById('sample_image');
	var cropper;
	
	$('#cover_image').change(function(event){
		cropImageData = '';
		$('#coverImage .pip').remove();
		var files = event.target.files;
		var done = function(url){
		
			let x = "<span class='pip'>" +
				"<img class='imageThumb' src=\"" + url + "\"/>" +
				"</span>";
			$("#coverImage .image_preview").append(x);
			$("#coverImage.remove").click(function(){
				$(this).parent("#coverImage .pip").remove();
			});			
			image.src = url;
			$modal.modal('show');
		};

		if(files && files.length > 0)
		{
			reader = new FileReader();
			reader.onload = function(event)
			{
				done(reader.result);
			};
			reader.readAsDataURL(files[0]);
		}
	});

	$modal.on('shown.bs.modal', function() {
		cropper = new Cropper(image, {
			aspectRatio: 1,
			viewMode: 3,
			preview:'.crop-preview'
		});
	}).on('hidden.bs.modal', function(){
		cropper.destroy();
   		cropper = null;
	});

	$('#crop').click(function(){
		canvas = cropper.getCroppedCanvas({
			width:400,
			height:400
		});

		canvas.toBlob(function(blob){
			url = URL.createObjectURL(blob);
			$("#coverImage .image_preview .imageThumb").attr('src',url);
			var reader = new FileReader();
			reader.readAsDataURL(blob);
			reader.onloadend = function(){
				var base64data = reader.result;
				cropImageData = base64data;
				$modal.modal('hide');
			};
		});
	});
	
});

	
	$("#category_id").select2();
	$("#topic_ids").select2({placeholder: "Select Topics"});
	
	function getCategoryTopics(e){
		let cat_id = e.value;
		
		if(cat_id !== 'other')
		{
			$("#manul_cat_flag").val(0);                    //set flag to identify category come from list or manual
			
			if(cat_id !== ''){
				$("#category_id").removeClass("d-none");
				$("#custom_category_id").addClass("d-none");
				$.ajax({
					headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					},
					data: { category_id: cat_id},
					url: "<?php echo route($type.'.blogs.get_topics'); ?>",
					type: "POST",
					success: function (response) {						
						$('#topic_ids').html(response);				  
					},
					error: function (data) {
						$(".loader").css('display', 'none');
						let errors = data.responseJSON.errors;
						
						$.each(errors, function(key, value) {
							errorAlert('Error Occured.',3000,'top-right');				
						});					
			
					}
				});			
			}
			else{
				$("#topic_ids").html('');
				$("#topic_ids").val("");			
			}
		}
		else
		{
			$("#manul_cat_flag").val(1);                        //set flag to identify category come from list or manual
			$("#category_id").closest('.category_cls').find('.select2-selection').addClass("d-none");
			$("#topic_opt").addClass("d-none");
			$("#custom_category_id").removeClass("d-none");
			$("#topic_inp").removeClass("d-none");
		}
	}

	function show_cat_option()
	{
		$("#custom_category_id").addClass("d-none");
		$("#topic_inp").addClass("d-none");
		$("#category_id").closest('.category_cls').find('.select2-selection').removeClass("d-none");
		$("#topic_opt").removeClass("d-none");
		$("#manul_cat_flag").val(0);
	}
	
	window.addEventListener('DOMContentLoaded', () => {
        Laraberg.init('description', { height: '600px', laravelFilemanager: true, sidebar: false })
    })

	$("#blogForm").submit(function(e) {
		e.preventDefault();
		var description = Laraberg.getContent();
		var formData = new FormData(this);
		formData.append('description', description);
		console.log(cropImageData);
		formData.append('crop_cover_image', cropImageData);

		$.ajax({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			url: "<?php echo route($type.'.blogs.store'); ?>",
			data: formData,
			type: "POST",
			cache:false,
			contentType: false,
			processData: false,
			// dataType: 'json',
			success: function (response) {
				callProgressBar();
				//response = JSON.parse(res);
				if(response['status'] == 'success'){
					setTimeout(function(){
					window.location = "<?php echo route($type.'.blogs'); ?>"; }, 1000);
				}
			},
			error: function (data) {
				let errorArr = [];
				let errors = data.responseJSON.errors;
				let emailErr = errors.email[0];
			}
		});
	});

	
	// Header Image reader
	if (window.File && window.FileList && window.FileReader) {
		$("#blog_header_image").on("change", function(e) {
			$('#headerImage .pip').remove();
			var files = e.target.files,
			filesLength = files.length;
			for (var i = 0; i < filesLength; i++) {
				var f = files[i]
				var fileReader = new FileReader();
				fileReader.onload = (function(e) {
					var file = e.target;
					$image = "<span class='pip'>" +
						"<img class='imageThumb' src=\"" + e.target.result + "\" title=\"" + file.name + "\" />" +
						"</span>";
					$("#headerImage .image_preview").append($image);
					$("#headerImage.remove").click(function(){
						$(this).parent("#headerImage .pip").remove();
					});
				});
				fileReader.readAsDataURL(f);
			}
		});
	} 
	else 
	{
		alert("Your browser doesn't support to File API")
	}
	
$("#article-title").bind("keyup change", function(e) {
    let text = $("#article-title").val();
	let slug = convertToSlug(text);
	$("#slug").val(slug);
})	

function convertToSlug(Text)
{
    return Text
        .toLowerCase()
        .replace(/[^\w ]+/g,'')
        .replace(/ +/g,'-')
        ;
}

function changeOption(flag)
{
	if(flag=="img")
	{
		$("#vdo_val").css("display","none");
		$("#yt_val").css("display","none");
		$("#img_val").css("display","block");
	}
	if(flag=="vdo")
	{
		$("#img_val").css("display","none");
		$("#yt_val").css("display","none");
		$("#vdo_val").css("display","block");
	}
	if(flag=="yt")
	{
		$("#img_val").css("display","none");
		$("#vdo_val").css("display","none");
		$("#yt_val").css("display","block");
	}

}

</script>
</html>
@endsection