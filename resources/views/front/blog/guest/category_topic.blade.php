@extends('front.layouts.app')

@section('content')
<section class="blog_cat_top">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <ul class="blog_top_bredcum">
                        <li><a href="javascript:void(0)">RMD Blogs</a></li>
                        <li><?php echo strtoupper($catname->name);?></li>
                    </ul>
                    @if($subscribe>=1)
                    <span><a href="javascript:void(0)" style="color:#39bd9e;"><i class="fa fa-check" aria-hidden="true"></i>&nbsp;Subscribed</a></span>
                    @else
                    <span><a href="javascript:void(0)" style="color:#2C99CB;" data-toggle="modal" data-target="#myModal">Subscribe to this blog</a></span>
                    @endif
                    <div class="blogs_topic_nav_ul">
                        
                        <h6>FILTER BY <?php echo strtoupper($catname->name);?> TOPICS</h6>
                        <ul class="bctt_list">
                            @if($topic && $topic->count() > 0)
                                <li><a class="<?php echo ($topicId=="" || $topicId==0) ? "active" : ""  ?>" href="javascript:void(0)" onclick="javascript:filterCategory(0,{{ $catId }})">All</a></li>        
                                @foreach($topic as $key => $value)
                                    <li><a  class="<?php echo ($topicId==$value->id) ? "active" : ""  ?>" href="javascript:void(0)" onclick="javascript:filterCategory({{ $value->id }},{{ $value->category_id }})">{{ $value->topic }}</a></li>
                                @endforeach
                            @endif
                        </ul>
                        <div class="blogs_topic_nav_sel">
                            <select class="custom-select">
                                <option value="0" <?php echo ($topicId=="" || $topicId==0) ? "selected=selected" : ""  ?> >All</option>
                                @if($topic && $topic->count() > 0)
                                    @foreach($topic as $key => $value)
                                    <option value="{{ $value->id }}" <?php echo ($topicId==$value->id) ? "selected=selected" : ""  ?> onclick="javascript:filterCategory({{ $value->id }},{{ $value->category_id }})">{{ $value->topic }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="content_block clearfix">
        <div class="latest_posts pt-4 pb-4">
            <div class="container mt-4">
                <div class="row">
                    <div class="col-md-8">
                        <div class="rmd_block_title">
                            <h3>Latest Blogs</h3>
                        </div>
                        <div class="row">
                            @if($blog_list_top && $blog_list_top->count() > 0)
                                @foreach($blog_list_top as $key => $value)
                                    <div class="col-md-6">
                                        <div class="post_box">
                                            <div class="post_img"><div class="rmd-post-format-icon"></div><img class="img-fluid" src="{{ env('APP_URL').'/'.$value->image }}" alt=""></div>
                                            <div class="rmd_post_details_inner">
                                                <div class="post-categories"><a href="{{ env('APP_URL')}}/blogs/category/{{ $catId }}"><span
                                                            class="cat-dot" 
                                                            style="background-color: #160600;"></span><span
                                                            class="cat-title">{{ $value->category_name }}</span></a></div>
                                                <h3 class="post-title entry-title"><a href="{{ route('front.blogs.post',$value->slug)}}">{{ $value->title }}</a></h3>
                                                <div class="post-date"><time class="entry-date published updated" datetime="{{ $value->created_at }}">{{ date('F d, Y',strtotime($value->created_at)) }}</time></div>
                                                <div class="post-info-dot"></div>
                                                <div class="post-read-time">5 Mins read</div>
                                            </div>
                                            
                                            <div class="post-excerpt"><?php echo substr(strip_tags($value->blog_content), 0, 150);?>…</div>
                                        </div>
                                    </div>
                                @endforeach            
                            @endif
                            
                            <div class="col-md-12">
                                <div class="text-center mt-5 mb-3 d-flex justify-content-center">
                                    {{ $blog_list_top->links() }}
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="col-md-4 main_sidebar">
                        <ul id="mainSidebar">
                            @if($categories_data && $categories_data->count() > 0)
                            <li>
                                <h2 class="widgettitle">Categories</h2>
                                <ul class="category_list">
                                @foreach($categories_data as $key => $value)
                                    <li>
                                        <div class="post_categories_overlay" style="background-image: url('{{ env('APP_URL').'/'.$value->category_img }}');">
                                            <a href="{{env('APP_URL')}}/blogs/category/{{ $value->id }}">
                                                <div class="cl_pc_item">
                                                    <div class="post-categories">
                                                        <div class="post-category">
                                                            <span class="cat-dot" style="background-color: #994db1;"></span>
                                                            <span class="cat-title">{{ $value->name }}</span>
                                                        </div>
                                                    </div>
                                                    <span class="post-categories-counter">{{ $value->total }} Posts</span>
                                                </div>
                                            </a>
                                        </div>
                                    </li>
                                @endforeach
                                </ul>
                            </li>
                            @endif
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <section class="blog_cat_foot mb-5">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="blogs_topic_nav_ul">
                        <h6>FILTER BY <?php echo strtoupper($catname->name);?> TOPICS</h6>
                        <ul class="bctt_list">
                            @if($topic && $topic->count() > 0)
                                <li><a class="<?php echo ($topicId=="" || $topicId==0) ? "active" : ""  ?>" href="javascript:void(0)" onclick="javascript:filterCategory(0,{{ $catId }})">All</a></li>
                                @foreach($topic as $key => $value)
                                <li><a href="{{ $value->id }}" class="<?php echo ($topicId==$value->id) ? "active" : ""  ?>">{{ $value->topic }}</a></li>
                                @endforeach
                            @endif
                        </ul>
                        <div class="blogs_topic_nav_sel">
                            <select class="custom-select">
                                <option value="0" <?php echo ($topicId=="" || $topicId==0) ? "selected=selected" : ""  ?> >All</option>
                                @if($topic && $topic->count() > 0)
                                    @foreach($topic as $key => $value)
                                    <option value="{{ $value->id }}" <?php echo ($topicId==$value->id) ? "selected=selected" : ""  ?> onclick="javascript:filterCategory({{ $value->id }},{{ $value->category_id }})">{{ $value->topic }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--Blog subscription modal-->
    <div id="myModal" class="modal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content" style="min-width: 600px;">
                <div class="modal-header">
                    <button type="button" class="close" aria-label="Close" onclick="closeBox()">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" id="before_success">
                    <form id="subscriberForm">
                        <div class="row">
                            <div class="col-sm-12" style="text-align:center;font-weight:bold;font-size:22px;margin-bottom:30px;">Please subscribe to get email updates on this blog.</div>

                            <div class="col-sm-9">
                            <input type="text" name="email_subs" id="email_subs" value="" class="form-control">
                            </div>
                            <div class="col-sm-3">
                                <input type="hidden" name="cateId" id="cateId" value="{{$catId}}">
                                <button type="submit" class="btn btn-success" name="subscription" id="subscription">Subscribe</button>
                            </div>
                            
                            <div class="col-sm-12" style="margin: 30px 0px;padding: 0px 50px;text-align:center;font-size: 12px;color:#b1b1b1">
                                By clicking "Subscribe," I agree to the Replenishmd Terms and Conditions and Privacy Policy. I also agree to receive emails from Replenishmd and I understand that I may opt out of Replenishmd subscriptions at any time.
                            </div>
                        </div>
                    </form>	
                </div>
                <div class="modal-body d-none" id="after_success">
                    <form id="subscriberForm">
                        <div class="row">
                            <p class="check-mark" style="margin: 0 auto;">
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 62 62" width="62" height="62">
                                    <g fill="none" fill-rule="evenodd" transform="translate(0.5 1)">
                                        <circle fill="#309a9c" stroke="#309a9c" cx="30.5" cy="30" r="30"></circle>
                                        <path fill="#fff" d="M 44.59 23.397 L 27.96 39.423 c -0.399 0.385 -0.93 0.577 -1.463 0.577 a 2.104 2.104 0 0 1 -1.463 -0.577 l -8.38 -8.013 a 1.96 1.96 0 0 1 0 -2.82 c 0.797 -0.77 2.128 -0.77 2.926 0 l 6.85 6.602 l 15.166 -14.615 a 2.23 2.23 0 0 1 2.993 0 s 0.798 2.051 0 2.82 Z"></path>
                                    </g>
                                </svg>
                            </p>
                            <div class="col-sm-12" style="text-align:center;font-weight:bold;font-size: 32px;margin-bottom: 0px;margin-top: 15px;">Thanks for subscribing!</div>
                            <div class="col-sm-12" style="margin: 30px 0px;padding: 0px 50px;text-align:center;font-size: 20px;color: #000;">
                                You’re now following the Newsletter Blog.<br>Check your email for updates.
                            </div>
                        </div>
                    </form>	
                </div>
                
            </div>
        </div>
    </div>
    <!--End Here--->
    <script>
        
        $("#subscriberForm").submit(function(e) {
			e.preventDefault();
			
			$.ajax({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				data: $('#subscriberForm').serialize(),
				url: "{{ route('blogs.subscribe') }}",
				type: "POST",				
				success: function (response) {
					if(response['status'] == 'success'){
						//$("#myModal").modal('hide');
                        $("#email_subs").val('');
						callProgressBar();
                        $("#before_success").addClass('d-none');
                        $("#after_success").removeClass('d-none');
					}
                    else{
                        errorAlert(response['message'],3000,'top-right');						
                    }
				},
				error: function (data) {
                    let errors = data.responseJSON.errors;
                    $.each(errors, function(key, value) {
                        errorAlert(value[0],3000,'top-right');
                    });					
                }
			});
		});

        function closeBox()
        {
            $("#before_success").removeClass('d-none');
            $("#after_success").addClass('d-none');
            $("#myModal").modal('hide');
        }

        
        function filterCategory(topicId,catId)
        {
            window.location = "{{env('APP_URL')}}/blogs/category/"+catId+"/"+topicId;
            
        }    
    </script>
@endsection

