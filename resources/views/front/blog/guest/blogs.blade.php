@extends('front.blog_layout.app')

@section('content')
<style>
.post-read-time, .post-info-dot{display:none!important;}
</style>
<div class="content_block clearfix">
            <div class="rmd_blog_top_fea pb-5">
                <div class="container-fluid container-content">
					@if($blog_list_top && $blog_list_top->count() > 0)
                    <div class="row no-gutters">
						@foreach($blog_list_top as $key => $value)
							<div class="col-md-3">
								<div class="rmd_post @if($value->post_visibility == 0) format_secret @endif">
									<div class="rmd_post_wrp_inner">
										<div class="rmd-post-image" style="background-image: url('{{ $value->image }}');">
											<div class="rmd-post-format-icon"></div>
										</div>
										<div class="rmd-post-details">
											<div class="rmd_post_details_inner">
												<div class="post-categories"><a href="{{ env('APP_URL')}}/blogs/category/{{ $value->blog_category_id }}"><span
															class="cat-dot" data-style="background-color: #994db1;"
															style="background-color: #994db1;"></span><span
															class="cat-title"> {{ $value->category_name }} </span></a></div>
												<h3 class="post-title entry-title"><a href="{{ route('front.blogs.post',$value->slug)}}">{{ $value->title }}</a></h3>
												<div class="post-author">
													<span class="vcard">
														By <span class="fn"><a href="#" title="Posts by rmd" rel="author">{{ $value->author }}</a></span>
													</span>
												</div>
												<div class="post-info-dot"></div>
												<div class="post-date"><time class="entry-date published updated" datetime="{{ $value->created_at }}">{{ date('F d, Y',strtotime($value->created_at)) }}</time></div>
												<div class="post-info-dot"></div>
												<div class="post-read-time">5 Mins read</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						@endforeach
					</div>
                    @endif					
                </div>
				
            <section class="blog_search">
                <div class="container">
                    <div class="row">
                        <div class="col">
                            <div class="blog_serach_wrapper">
                                <div class="search-input">
                                  <a href="" target="_blank" hidden></a>
                                  <input type="text" placeholder="Type to search topic ..." onkeyup="searchBlog(this);">
                                  <div class="autocom-box">
                                    <!-- here list are inserted from javascript -->
                                  </div>
                                  <div class="icon"><i class="fas fa-search"></i></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
			
				@if($blog_list_featured && $blog_list_featured->count() > 0)				
                <div class="container mt-4">
                    <div class="row">
                        <div class="col">
                            <div class="rmd_block_title">
                                <h3>Featured</h3>
                                <h4>This is sample subtitle blog post section</h4>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container mt-2 featured_posts">
                    <div class="row">
					@foreach($blog_list_featured as $key => $value)
                        <div class="col-md-4">
                            <div class="post_box @if($value->post_visibility == 0) format_secret @endif">
                                <div class="post_img"><div class="rmd-post-format-icon"></div><img class="img-fluid" src="{{ $value->image }}" alt=""></div>
                                <div class="rmd_post_details_inner">
                                    <div class="post-categories"><a href="{{ env('APP_URL')}}/blogs/category/{{ $value->blog_category_id }}"><span
                                                class="cat-dot" data-style="background-color: #994db1;"
                                                style="background-color: #994db1;"></span><span
                                                class="cat-title">{{ $value->category_name }}</span></a></div>
                                    <h3 class="post-title entry-title"><a href="{{ route('front.blogs.post',$value->slug)}}">{{ $value->title }}</a></h3>
                                    <div class="post-date"><time class="entry-date published updated" datetime="{{ $value->created_at }}">{{ date('F d, Y',strtotime($value->created_at)) }}</time></div>
                                    <div class="post-info-dot"></div>
                                    <div class="post-read-time">5 Mins read</div>
                                </div>
                            </div>
                        </div>
					@endforeach					
                    </div>
                </div>
				@endif
            </div>
			
            <div class="editor_choice pt-4 pb-4 bg-light">
                <div class="container mt-4">
                    <div class="row">
                        <div class="col">
                            <div class="rmd_block_title">
                                <h3>Editor's Choice</h3>
                                <h4>Fresh and exclusive</h4>
                            </div>
                        </div>
                    </div>
                    <div class="row">
					@if($blog_list_editor_choice && $blog_list_editor_choice->count() > 0)
                        <div class="col-md-9">
                            <div class="post_box big_post @if($value->post_visibility == 0) format_secret @endif">
                                <div class="post_img"><div class="rmd-post-format-icon"></div><img class="img-fluid" src="{{ $blog_list_editor_choice[0]->image }}" alt=""></div>
                                <div class="rmd_post_details_inner">
                                    <div class="post-categories"><a href="{{ env('APP_URL')}}/blogs/category/{{ $blog_list_editor_choice[0]->blog_category_id }}"><span
                                                class="cat-dot" data-style="background-color: #994db1;"
                                                style="background-color: #994db1;"></span><span
                                                class="cat-title">{{ $blog_list_editor_choice[0]->category_name }}</span></a></div>
                                    <h3 class="post-title entry-title"><a href="{{ route('front.blogs.post',$blog_list_editor_choice[0]->slug)}}">{{ $blog_list_editor_choice[0]->title }}</a></h3>
                                    <div class="post-date"><time class="entry-date published updated" datetime="{{ $value->created_at }}">{{ date('F d, Y',strtotime($blog_list_editor_choice[0]->created_at)) }}</time></div>
                                    <div class="post-info-dot"></div>
                                    <div class="post-read-time">5 Mins read</div>
                                </div>
                            </div>
                        </div>
					@endif
					
					@if($blog_list_editor_choice && $blog_list_editor_choice->count() > 1)	
                        <div class="col-md-3">
						@foreach($blog_list_editor_choice as $key => $value)
							@if($key > 0)
                            <div class="post_box small_post">
                                <div class="post_img"><img class="img-fluid" src="{{ $value->image }}" alt="{{ $value->title }}"></div>
                                <h3 class="post-title entry-title"><a href="{{ route('front.blogs.post',$value->slug)}}">{{ $value->title }}</a></h3>
                            </div>
							@endif
						@endforeach
                        </div>
					@endif
                    </div>
                </div>
            </div>
            <div class="latest_posts pt-4 pb-4">
                <div class="container mt-4">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="rmd_block_title">
                                <h3>Latest</h3>
                                <!--h4>This is sample subtitle blog post section</h4-->
                            </div>
                            <div class="row">
							@if($blog_list_latest_1 && $blog_list_latest_1->count() > 0)
                                <div class="col-md-12">
                                    <div class="rmd_post full_overlay_post @if($value->post_visibility == 0) format_secret @endif mb-5">
                                        <div class="rmd_post_wrp_inner">
                                            <div class="rmd-post-image" style="background-image: url('{{ $blog_list_latest_1[0]->image }}');">
                                                <div class="rmd-post-format-icon"></div>
                                            </div>
                                            <div class="rmd-post-details">
                                                <div class="rmd_post_details_inner">
                                                    <div class="post-categories"><a href="{{ env('APP_URL')}}/blogs/category/{{ $blog_list_latest_1[0]->blog_category_id }}"><span
                                                                class="cat-dot" data-style="background-color: #994db1;"
                                                                style="background-color: #994db1;"></span><span
                                                                class="cat-title">{{ $blog_list_latest_1[0]->category_name }}</span></a></div>
                                                    <h3 class="post-title entry-title"><a href="{{ route('front.blogs.post',$blog_list_latest_1[0]->slug)}}">{{ $blog_list_latest_1[0]->title }}</a></h3>
                                                    <div class="post-author">
                                                        <span class="vcard">
                                                            By <span class="fn"><a href="#" title="Posts by {{ $blog_list_latest_1[0]->author }}" rel="author">{{ $blog_list_latest_1[0]->author }}</a></span>
                                                        </span>
                                                    </div>
                                                    <div class="post-info-dot"></div>
                                                    <div class="post-date"><time class="entry-date published updated" datetime="{{ $blog_list_latest_1[0]->created_at }}">{{ date('F d, Y',strtotime($blog_list_latest_1[0]->created_at)) }}</time></div>
                                                    <div class="post-info-dot"></div>
                                                    <div class="post-read-time">5 Mins read</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
							@endif
							
						@if($blog_list_latest_1 && $blog_list_latest_1->count() > 1)
							@foreach($blog_list_latest_1 as $key => $value)
								@if($key > 0)
                                <div class="col-md-6 @if($value->post_visibility == 0) format_secret @endif">
                                    <div class="post_box">
                                        <div class="post_img"><div class="rmd-post-format-icon"></div><img class="img-fluid" src="{{ $value->image }}" alt="{{ $value->title }}"></div>
                                        <div class="rmd_post_details_inner">
                                            <div class="post-categories"><a href="{{ env('APP_URL')}}/blogs/category/{{ $value->blog_category_id }}"><span
                                                        class="cat-dot" 
                                                        style="background-color: #160600;"></span><span
                                                        class="cat-title">{{ $value->category_name }}</span></a></div>
                                            <h3 class="post-title entry-title"><a href="{{ route('front.blogs.post',$value->slug)}}">{{ $value->title }}</a></h3>
                                            <div class="post-date"><time class="entry-date published updated" datetime="{{ $value->created_at }}">{{ date('F d, Y',strtotime($value->created_at)) }}</time></div>
                                            <div class="post-info-dot"></div>
                                            <div class="post-read-time">5 Mins read</div>
                                        </div>
                                        <div class="post-excerpt"><?php echo substr(strip_tags($value->blog_content), 0, 150);?>…</div>
                                    </div>
                                </div>
								@endif
							@endforeach
						@endif
                                <div class="col-md-12" style="display:none;">
                                    <div class="text-center mt-3 mb-3">
                                        <a class="load_more_btn btn-primary" href="#">Load More</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 main_sidebar">
                            <ul id="mainSidebar">
								@if($blog_list_latest_2 && $blog_list_latest_2->count() > 0)
                                <li>
                                    <h2 class="widgettitle">Latest posts</h2>
                                    <ul class="lastpost_list">
									@foreach($blog_list_latest_2 as $key => $value)
                                        <li>
                                            <div class="post_box">
                                                <div class="post_img"><img class="img-fluid" src="{{ $value->image }}" alt="{{ $value->title }}"></div>
                                                <div class="rmd_post_details_inner">
                                                    <h3 class="post-title entry-title"><a href="{{ route('front.blogs.post',$value->slug)}}">{{ $value->title }}</a></h3>
                                                    <div class="post-date"><time class="entry-date published updated" datetime="{{ $value->created_at }}">{{ date('F d, Y',strtotime($value->created_at)) }}</time></div>
                                                </div>
                                            </div>
                                        </li>
									@endforeach
                                    </ul>
                                </li>
								@endif
								
								@if($blog_list_popular && $blog_list_popular->count() > 0)
                                <li>
                                    <h2 class="widgettitle">Popular</h2>
                                    <ul class="lastpost_list">
									@foreach($blog_list_popular as $key => $value)
                                        <li>
                                            <div class="rmd_post full_overlay_post @if($value->post_visibility == 0) format_secret @endif mb-5">
                                                <div class="rmd_post_wrp_inner">
                                                    <div class="rmd-post-image" style="background-image: url('{{ $value->image }}');">
                                                        <div class="rmd-post-format-icon"></div>
                                                    </div>
                                                    <div class="rmd-post-details">
                                                        <div class="rmd_post_details_inner">
                                                            <div class="post-categories"><a href="{{ env('APP_URL')}}/blogs/category/{{ $value->blog_category_id }}"><span
                                                                        class="cat-dot" data-style="background-color: #994db1;"
                                                                        style="background-color: #994db1;"></span><span
                                                                        class="cat-title">{{ $value->category_name }}</span></a></div>
                                                            <h3 class="post-title entry-title"><a href="{{ route('front.blogs.post',$value->slug)}}">{{ $value->title }}</a></h3>
                                                            
                                                            <div class="post-date"><time class="entry-date published updated" datetime="{{ $value->created_at }}">{{ date('F d, Y',strtotime($value->created_at)) }}</time></div>
                                                            <div class="post-info-dot"></div>
                                                            <div class="post-read-time">5 Mins read</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
									@endforeach
                                    </ul>
                                </li>
								@endif
								
                                <!--li>
                                    <h2 class="widgettitle">Subscribe and Follow</h2>
                                    <ul class="subs_follow">
                                        <li>
                                            <a href="https://facebook.com/" target="_blank" class="sf_facebook">
                                                <i class="fa fa-facebook"></i>
                                                <span class="social-title">Facebook</span>
                                                <span class="social-description">53K Likes</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="https://twitter.com/" target="_blank" class="sf_twitter">
                                                <i class="fa fa-twitter"></i>
                                                <span class="social-title">Twitter</span>
                                                <span class="social-description">53K Followers</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="https://instagram.com/" target="_blank" class="sf_instagram">
                                                <i class="fa fa-instagram"></i>
                                                <span class="social-title">Instagram</span>
                                                <span class="social-description">53K Followers</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="https://youtube.com/" target="_blank" class="sf_yt">
                                                <i class="fa fa-youtube"></i>
                                                <span class="social-title">YouTube</span>
                                                <span class="social-description">53K Followers</span>
                                            </a>
                                        </li>
                                    </ul>
                                </li-->
								@if($categories_data && $categories_data->count() > 0)
                                <li>
                                    <h2 class="widgettitle">Categories</h2>
                                    <ul class="category_list">
									@foreach($categories_data as $key => $value)
                                        <li>
                                            <div class="post_categories_overlay" style="background-image: url('{{ $value->category_img }}');">
                                                <a href="{{env('APP_URL')}}/blogs/category/{{ $value->id }}">
                                                    <div class="cl_pc_item">
                                                        <div class="post-categories">
                                                            <div class="post-category">
                                                                <span class="cat-dot" style="background-color: #994db1;"></span>
                                                                <span class="cat-title">{{ $value->name }}</span>
                                                            </div>
                                                        </div>
                                                        <span class="post-categories-counter">{{ $value->total }} Posts</span>
                                                    </div>
                                                </a>
                                            </div>
                                        </li>
									@endforeach
                                    </ul>
                                </li>
								@endif
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
			@if($blog_list_others && $blog_list_others->count() > 0)
            <div class="bg-light pt-5 pb-5 mb-5">
                <div class="container">
                    <div class="row">
					@foreach($blog_list_others as $key => $value)
                        <div class="col-md-3 col-sm-6">
                            <div class="post_box">
                                <div class="post_img"><div class="rmd-post-format-icon"></div><img class="img-fluid" src="{{ env('APP_URL').'/'.$value->image }}" alt="{{ $value->title }}"></div>
                                <div class="rmd_post_details_inner">
                                    <div class="post-categories"><a href="{{ env('APP_URL')}}/blogs/category/{{ $value->blog_category_id }}"><span
                                                class="cat-dot" 
                                                style="background-color: #160600;"></span><span
                                                class="cat-title">{{ $value->category_name }}</span></a></div>
                                    <h3 class="post-title entry-title"><a href="{{ route('front.blogs.post',$value->slug)}}">{{ $value->title }}</a></h3>
                                    <div class="post-date"><time class="entry-date published updated" datetime="{{ $value->created_at }}">{{ date('F d, Y',strtotime($value->created_at)) }}</time></div>
                                    <div class="post-info-dot"></div>
                                    <div class="post-read-time">5 Mins read</div>
                                </div>
                            </div>
                        </div>
					@endforeach
                    </div>
                </div>
            </div>
			@endif
        </div>

<script>
	function searchBlog(e){
		let search_value = e.value;
		const searchWrapper = document.querySelector(".search-input");
		let html = '';
		
		if(search_value !== '' && search_value.length >= 3){
			$.ajax({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				async:false,
				data: { search_value: search_value},
				url: "<?php echo route('front.blogs.search'); ?>",
				type: "POST",
				// dataType: 'json',
				success: function (response) {
					
					
					if(response['status'] == 'success'){
						let rows = response['data'];
						let base_url = "{{ env('APP_URL') }}/blogs/post";
						for(let i=0; i<rows.length; i++){
							html += '<li><a href="'+base_url+'/'+rows[i].slug+'">'+rows[i].title+'</a></li>';
						}
						 searchWrapper.classList.add("active"); //show autocomplete box
					}else{
						 searchWrapper.classList.remove("active"); //show autocomplete box
					}
					$('.autocom-box').html(html);				  
				},
				error: function (data) {
					searchWrapper.classList.remove("active"); //show autocomplete box
					$.each(errors, function(key, value) {
						errorAlert('Error Occured.',3000,'top-right');				
					});					
		
				}
			});			
		}else{
            $('.autocom-box').html(html);
			searchWrapper.classList.remove("active"); //show autocomplete box
		}
	}
</script>
@endsection