@extends('front.common_layout.app')

@section('content')
@php $type = auth()->user()->user_role == 4 ? 'user' : 'provider'; @endphp
	@if(auth()->user()->user_role==3)
		<div class="col-xl-10 col-lg-8 col-md-8 col-sm-12">
	@endif
		<div class="main-center-data">
			<h3 class="display-username">Blogs</h3>
			<div class="row mt-15">		
				<div class="col-xl-10 col-lg-10 col-md-12 col-sm-12">
					<h5>Filter</h5>
					<div class="filters_wrapper">
						<div class="data-list-filters">
							<label>Category</label>
							<select id="filter_category" class="custom-select select2">
								<option value="">Any</option>
								@if($categories && $categories->count() > 0)
									@foreach($categories as $category)
										<option value="{{ $category->id }}" >{{ $category->name }}</option>
									@endforeach
								@endif
							</select>
						</div>

						<div class="data-list-filters">
							<label>Visibility</label>
							<select id="visibility_status" class="custom-select select2">
								<option value="" class="any">Any</option>
								<option value="1">Public</option>
								<option value="0">Private</option>
							</select>
						</div>

						<div class="data-list-filters">
							<label>Publish</label>
							<select id="publish_status" class="custom-select select2">
								<option value="" class="any">Any</option>
								<option value="1">Immediate</option>
								<option value="0">Draft</option>
							</select>
						</div>

						<div class="data-list-filters">
							<label>Comment</label>
							<select id="comment_status" class="custom-select select2">
								<option value="" class="any">Any</option>
								<option value="1">Allow</option>
								<option value="0">Not-allow</option>
							</select>
						</div>
						<div class="data-list-filters pt-4">
							<div class="mt-3 pt-1">
							<button type="button" class="btn btn-sm btn-secondary" id="filter">Show</button>
							
							<button type="reset" class="btn btn-sm btn-danger ml-2" id="reset">Reset</button>
							</div>
						</div>	
					</div>
				</div>
				
				<div class="col-xl-2 col-lg-2 col-md-12 col-sm-12 pull-right mt-5 pt-2"><a href="{{ url($type.'/blogs/add') }}" class="btn btn-success btn-lg">Add New</a></div>					
			</div>
			
			<div class="row mt-15 bg-white round-crn pd-20-30 hover-effect-box">
				<div class="col-xl-12 col-lg-12 col-md-12">			
					<div id="patients_data">
						<table id="myTable" class="table table-striped table-bordered" style="width:100%">
							<thead>
								<tr>
									<th>S.No.</th>
									<th>Title</th>
									<th>Category</th>
									<th>Visibility</th>
									<th>Publish</th>
									<th>Comment</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody></tbody>
						</table>			
					</div>
				</div>
			</div>

		</div>
	@if(auth()->user()->user_role==3)
		</div>
	@endif		
	
@push('scripts')
<script type="text/javascript">
	var table;
 	$(document).ready(function() {
		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});

		fill_datatable();
		
		function fill_datatable(filter_category = '', visibility_status = '', publish_status = '', comment_status = ''){
			
			table = $('#myTable').DataTable({
				processing: true,
				serverSide: true,
				responsive: true,
				paging: true,
				ordering: false,
				info: false,
				displayLength: 10,
				language: {
					processing: '<img src="/dist/images/loading.gif"/>'
				},				
				lengthMenu: [
				[10, 25, 50, -1],
				[10, 25, 50, "All"]
				],
				ajax:{
					url: "{{ route('user.blogs') }}",
					data:{"filter_category":filter_category, "visibility_status":visibility_status, "publish_status":publish_status, "comment_status":comment_status }
				},
				columns: [
					{data: 'DT_RowIndex', name: 'DT_RowIndex'},
					{data: 'title', name: 'title'},					
					{data: 'blog_category_id', name: 'blog_category_id'},
					{data: 'post_visibility', name: 'post_visibility'},
					{data: 'post_publish', name: 'post_publish'},
					{data: 'post_comment', name: 'post_comment'},
					{data: 'action', name: 'action', orderable: false, searchable: false},
				]
			});
		}

		$('#filter').click(function(){
			var filter_category = $('#filter_category').val();
			var visibility_status = $('#visibility_status').val();
			var publish_status = $('#publish_status').val();
			var comment_status = $('#comment_status').val();
			$('#myTable').DataTable().destroy();
			fill_datatable(filter_category,visibility_status,publish_status,comment_status);
		});

		$('#reset').click(function(){

			$('#filter_category').prop('selectedIndex', 0);
			$('#filter_category').select2();

			$('#visibility_status').prop('selectedIndex', 0);
			$('#visibility_status').select2();

			$('#publish_status').prop('selectedIndex', 0);
			$('#publish_status').select2();
			
			$('#comment_status').prop('selectedIndex', 0);
			$('#comment_status').select2();

			$('#myTable').DataTable().destroy();
			fill_datatable();
		});

		$("#myTable tbody tr").addClass('intro-x');
  	});

    function deleteRow(id) {
		swal({
			title: "Are you sure to delete?",
			text: "",
			icon: 'warning',
			buttons: {
			  cancel: true,
			  delete: 'Yes, Delete It'
			}
		  }).then((isConfirm) => {
			if (!isConfirm) {
				return false;
			} else {
				$.ajax({
					type: "DELETE",
					url: "{{ env('APP_URL').'/user/blogs/destroy' }}"+'/'+id,
					success: function (response) {
						if(response['status'] == 'success'){
						swal({
							title: response['message'],
							icon: 'success'
						});
						setTimeout(function(){
						swal.close();
						}, 1000);
						}
						table.draw();
					},
					error: function (data) {
						console.log('Error:', data);
						swal({
							title: 'Error Occured',
							icon: 'error'
						});
					}
				});
			}
        });
    }

	function changeStatus(id,status,flag) {
		$.ajax({
			url: "{{ url('user/blogs/change_status') }}",
			type: "POST",
			data: {'id': id, 'status':status, 'flag':flag},
			success: function(response) 
			{
				if(response['status'] == 'success')
				{
					swal({
						title: response['message'],
						icon: 'success'
					});
					setTimeout(function(){
					swal.close();
					}, 1000);
				}
				table.draw();
			}
		});
	}
</script>
@endpush

@endsection