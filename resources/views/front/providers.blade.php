@extends('front.layouts.app')

@section('content')

    <section class="inner_banner experts_banner">
        <div class="container">
            <div class="content_wrapper">
                <div class="b_text">

                    <h1>@if(isset($page_data['info_section']->top_main_name)) {{ $page_data['info_section']->top_main_name }} @endif</h1>
                    <p>@if(isset($page_data['info_section']->top_main_content)) {{ $page_data['info_section']->top_main_content }} @endif</p>
                </div>
            </div>
        </div>
        <img src="/assets/images/bottom_curve.svg" alt="bottom_curve">
    </section>

    <section class="contact_head_sec">
        <div class="container">
            <div class="title">@if(isset($page_data['info_section']->main_name)) {{ $page_data['info_section']->main_name }} @endif
                <p>@if(isset($page_data['info_section']->sub_name)) {{ $page_data['info_section']->sub_name }} @endif</p>
            </div>
		@if(!empty($page_data['info_section']->images))
            <div class="member_wrapper">
			@foreach($page_data['info_section']->images as $key=>$value)
                <div class="item_mem">
                    <div class="exprt_box">
                        <div class="exprt_img"><img src="{{env('APP_URL')}}/{{ $value->image }}" class="img-fluid" alt=""></div>
                        <div class="eprt_info">
                            <strong>{{ $value->heading }}</strong><br>
                            <a class="vb_link" href="{{env('APP_URL')}}">View full bio <i class="fas fa-long-arrow-alt-right"></i></a>
                        </div>
                    </div>
                </div>
			@endforeach
            </div>
		@endif
        </div>
    </section>

    <section class="work_join_sec about experts_p">
        <div class="data_wrap">
            <div class="container">
                <div class="content_wrapper">
                    <div class="title">@if(isset($page_data['community']->top_main_name)) {{ $page_data['community']->top_main_name }} @endif
                        <p>@if(isset($page_data['community']->top_main_content)) {{ $page_data['community']->top_main_content }} @endif</p>
                    </div>
                    <div class="boxes_wrapper d_flex_j_center">
                        <div class="box">
                            <span class="fas fa-file-medical icon"></span>
                            <h6 class="title">@if(isset($page_data['community']->content_1)) {{ $page_data['community']->content_1 }} @endif</h6>
                        </div>
                        <div class="box">
                            <span class="medal_icon icon"></span>
                            <h6 class="title">@if(isset($page_data['community']->content_2)) {{ $page_data['community']->content_2 }} @endif</h6>
                        </div>
                        <div class="box">
                            <span class="fas fa-flask icon"></span>
                            <h6 class="title">@if(isset($page_data['community']->content_3)) {{ $page_data['community']->content_3 }} @endif</h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


@endsection
