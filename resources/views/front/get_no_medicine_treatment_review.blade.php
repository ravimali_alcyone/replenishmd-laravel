
	<div class="container number_uses">

		<div class="num_uses">

			<h5>Review treatment and pay</h5>
			<p>Your Shipping frequency</p>
			<div class="review_box">
				<h6>Treatment Details</h6>
				<p>{{ $service->short_info }}</p>
			</div>
			
			<div class="review_box">
				<h6>Payment Type</h6>
				<div class="rec_pay_options">
					<input type="radio" name="md_payment_type" id="one_time_payment" class="form-control" value="0" checked> <label for="one_time_payment">One-time</label>
				</div>
			</div>

			<div class="review_box_1">
				<h6>Your plan</h6>
				<div class="sil_review">
					<div class="sil_details">
						<div class="sil_details_img">
							<img src="{{env('APP_URL')}}/{{$service->image}}" alt="{{$service->name}}">

						</div>

					</div>
					<div class="sil_details_price ml-auto">

						<p><span>${{ $data['amount'] }}</span></p>

					</div>


				</div>

				<div class="sil_review">

					<div class="order_pro">
						<h5>First Order promo</h5>

					</div>

					<div class="order_pro ml-auto">
						<span>-${{ $data['promo_price'] }}</span>

					</div>
				</div>


				<div class="sil_review">

					<div class="order_pro">
						<h5>Healthcare professional review</h5>
						<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum </p>

					</div>

					<div class="order_pro ml-auto">
						<span>free</span>

					</div>
				</div>

				<div class="sil_review">

					<div class="order_pro">
						<h5>2 Day Shipping</h5>

					</div>

					<div class="order_pro ml-auto">
						<span>free</span>

					</div>
				</div>

				<div class="sil_review">

					<div class="order_pro">
						<h5>Total due if prescribed</h5>

					</div>

					<div class="order_pro ml-auto">
						<span>${{ $data['final_amount'] }}</span>

					</div>
				</div>
				

				<div class="sil_review_1">

					<div class="order_pro_v">
						<h5>Due Today</h5>
						<p> <a href="">What am I Changed</a></p>
					</div>

					<div class="order_pro ml-auto">
						<h6>$0</h6>

					</div>
				</div>
			</div>

			<h5>Payments</h5>
			<div class="review_box">
				<div class="check_1">
					<input type="radio" id="payment_card" name="payment_type" onclick="goToPayment(this);" value="payment_card">
					<label for="payment_card"><span>Credit or Debit card</span></label>
					<i class="fas fa-lock"></i>
				</div>

			</div>
			<div class="review_box">
				<div class="check_1">
					<input type="radio" id="payment_paypal" name="payment_type" value="payment_paypal">
					<label for="payment_paypal"><img src="{{asset('assets/images/paypal-logo.svg')}}" alt=""></label>

				</div>

			</div>
			
		</div>
	</div>
	
	