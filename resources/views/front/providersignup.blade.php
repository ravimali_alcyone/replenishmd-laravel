@extends('front.online_visit_layout.app')

@section('content')
	<style>
		.ps_step_four,.ps_step_three,.ps_step_two,.ps_step_five{display:none}
		.pro_sign_header .step_count{width:50px;background-color:#047cb3;border-radius:50%;height:50px;display:flex;justify-content:center;align-items:center;font-size:20px;color:#fff;margin-right:15px}
		.pro_sign_header{display:flex;margin:15px auto 30px auto;justify-content:flex-start;align-items:center}
		.pro_sign_header .step_name{font-size:22px}
		.ps_step_block .btn-primary{border:none}
		.ps_step_block .form-group{text-align:left}
		.ps_step_block .form-group small{display:block;color:#999}
		.ps_step_block .custom-file-input,.ps_step_block .custom-file-label{width:100%;border-radius:0;background-color:#f3f7f8;border:none;font-size:16px;margin-bottom:8px}
		.ps_step_block .form-control{width:100%;border-radius:0;background-color:#f3f7f8;border:none;font-size:16px;margin-bottom:8px}
		.from_btm_btns_grp{display:flex;justify-content:space-between;margin-top:30px;padding-top:10px;border-top:1px solid #dedede;align-items:center}
		.succ_check{width:100px;height:100px;border:2px solid;display:flex;justify-content:center;align-items:center;font-size:30px;color:green;border-radius:50%;margin:25px auto}
		.ps_step_one .from_btm_btns_grp{justify-content:flex-end}
		.ps_step_four .from_btm_btns_grp{justify-content:center}
		.galeria{
            display: flex;
        }
        .galeria img{
            width: 100px;
            height: 100px;
            border-radius: 10px;
            box-shadow: 0 0 8px rgb(0 0 0 / 20%);
            opacity: 85%;
            margin-right: 10px;
            margin-top: 10px;
        }
        
        span.select2.select2-container.select2-container--default {
            width: 100%!important;
        }
        
        .select2-container--default .select2-selection--multiple {
            font-size: 12px;
        }
        
        .sign_in_sec .select2-selection__choice button.select2-selection__choice__remove {
        	width: auto;
        	margin-top: 0;
        }
        
	</style>
	<section class="sign_in_sec_wrapper ">
		<div class="container-fluid" id="grad1">
        <div class="row">
            <div class="col">
			<div class="pro_signup">
				<div class="card shadow sign_in_sec">
                    <div class="row">
                        <div class="col-md-12">
                            <form id="accountInfo">
                                <div id="pstepOne" class="ps_step_block ps_step_one">
                                    <div class="progress" style="height:5px">
                                        <div class="progress-bar bg-success rounded progress-bar-striped progress-bar-animated" role="progressbar" style="width: 0%;" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                                      </div>
                                    <div class="pro_sign_header">
                                        <div class="step_count">01</div>
                                        <div class="step_name">Account Information</div>
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control" type="email" name="email_id" id="email_id" placeholder="Email Id" />
										
                                        <input class="form-control" type="password" name="pwd" id="pwd" placeholder="Password" />
										
                                        <input class="form-control" type="password" name="cpwd" id="cpwd" placeholder="Confirm Password" />
										
                                    </div>
                                    <div class="from_btm_btns_grp">
                                        <button type="submit" id="btnstepTwo" class="btn btn-primary">Continue</button>
                                    </div>
                                </div>
                            </form>
                            <form id="personalInfo">
                                <div id="pstepTwo" class="ps_step_block ps_step_two">
                                    <div class="progress" style="height:5px">
                                        <div class="progress-bar bg-success rounded progress-bar-striped progress-bar-animated" role="progressbar" style="width: 25%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                      </div>
                                    <div class="pro_sign_header">
                                        <div class="step_count">02</div>
                                        <div class="step_name">Personal Information</div>
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control" type="text" name="first_name" id="first_name" placeholder="First Name" />
										<span class="text-danger"><strong class="error" id="first_name-error"></strong></span>
                                        <input class="form-control" type="text" name="last_name" id="last_name" placeholder="Last Name" /> 
										<span class="text-danger"><strong class="error" id="last_name-error"></strong></span>
                                        <input type="text" name="phone_number" class="form-control" maxlength="10" onkeypress="return event.charCode >= 48 && event.charCode <= 57" placeholder="(###) ###-####" id="phone_number" value="">
										<span class="text-danger"><strong class="error" id="phone_number-error"></strong></span>
									</div>
                                    <div class="from_btm_btns_grp">
                                        <button type="button" id="btnstepOne" class="btn btn-primary">Go Back</button>
                                        <button type="submit" id="btnstepThree" class="btn btn-primary">Continue</button>
                                    </div>
                                </div>
                            </form>
                            <form id="professionalInfo" enctype="multipart/form-data">
                                <div id="pstepThree" class="ps_step_block ps_step_three">
                                    <div class="progress" style="height:5px">
                                        <div class="progress-bar bg-success rounded progress-bar-striped progress-bar-animated" role="progressbar" style="width: 50%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                      </div>
                                    <div class="pro_sign_header">
                                        <div class="step_count">03</div>
                                        <div class="step_name">Professional Information</div>
                                    </div>
                                    <div class="form-group">
                                        <label for="">What they treat?</label>
                                        <!--<input type="text" class="form-control" name ="service" id="service" placeholder="">-->
                                        <select id="service" name="service[]" class="custom-select select2" multiple>
                        					<option value="">Select</option>
                        			@if($services)	
                        				@foreach($services as $val)
                        					<option value="{{$val->id}}" >{{$val->name}}</option>
                        				@endforeach
                        			@endif
                        				</select>
										<span class="text-danger"><strong class="error" id="service-error"></strong></span>
                                    </div>
                                    <div class="form-group">
                                        <label for="">Can you please upload your credentials?<small>We want to make sure we offer our Replenishers great service.</small></label>
                                        <small style="color:red;">Only JPEG|PNG|JPG|png|PDF</small>
										<div class="input-group mb-3">
                                            <div class="custom-file">
                                              <!--<input type="file" class="custom-file-input" id="">-->
											  <input class="custom-file-input" type="file" id="cred_img" name="cred_img[]" multiple onchange="previewMultiple(event,1)" value=""/>
                                              <label class="custom-file-label" for="">Upload Credential</label>
                                            </div>
                                        </div>
										<span class="galeria" id="filenameCred">
										
										</span>
                                    </div>
                                    <div class="form-group">
                                        <label for="">Can you please upload a professional picture?<small>This will allow other replenishers to see who they are being serviced by.</small></label>
                                        <small style="color:red;">Only JPEG|PNG|JPG|jpeg|png|jpg</small>
										<div class="input-group mb-3">
                                            <div class="custom-file">
                                              <input class="custom-file-input" type="file" name="profile_img" id="profile_img" value="" onchange="previewMultiple(event,0)"/>
                                              <label class="custom-file-label" for="">Upload Your Photo</label>
                                            </div>
                                        </div>
										<span class="galeria" id="filenamePhoto"></span>
                                    </div>
                                    <div class="from_btm_btns_grp">
                                        <button type="button" id="btnstepTwoPrev" class="btn btn-primary">Go Back</button>
                                        <button type="submit" id="btnstepFour" class="btn btn-primary">Continue</button>
                                    </div>
                                </div>
                            </form>
							<form id="msform">
                                <div id="pstepFour" class="ps_step_block ps_step_four">
                                    <div class="progress" style="height:5px">
                                        <div class="progress-bar bg-success rounded progress-bar-striped progress-bar-animated" role="progressbar" style="width: 75%;" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
                                      </div>
                                    <div class="pro_sign_header">
                                        <div class="step_count">04</div>
                                        <div class="step_name">Email Verification</div>
                                    </div>
									<div class="form-group">
                                        Please check your inbox to verify your email.
									</div>
                                    <div class="form-group">
                                        <input class="form-control" type="password" name="email_v_otp" id="email_v_otp" placeholder="Enter OTP" maxlength="4"/>
									</div>
                                    <div class="from_btm_btns_grp">
									<span><a href="javascript:void(0)" onclick="resendOtp()">Resend OTP</a></span>
                                        <button type="submit" id="btnstepFive" class="btn btn-primary">Verify</button>
                                    </div>
                                </div>
                            </form>
							
							
							
                            <form action="">
                                <div id="pstepFive" class="ps_step_block ps_step_five">
									<div class="progress" style="height:5px">
                                        <div class="progress-bar bg-success rounded progress-bar-striped progress-bar-animated" role="progressbar" style="width: 100%;" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                                      </div>
                                    <div class="sign_success">
                                        <div class="succ_check">&#10004;</div>
                                        <div class="succ_msg">You Have Successfully Signed Up!</div>
                                    </div>
                                    <!--<div class="from_btm_btns_grp">
                                        <button type="button" id="" class="btn btn-primary">Go to Dashboard</button>
                                    </div>-->
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
			</div>
        </div>
    </div>
	</section>	
	<script src="{{asset('assets/js/jquery.mask.js')}}"></script>

	<script>
	
	    $("#service").select2({
			 minimumResultsForSearch: -1,
			 placeholder: "Select a treat",
        
		});
		$("#phone_number").mask("(999) 999-9999");
		
		function previewMultiple(event, flag){
            var imageId = (flag==1) ? "cred_img" : "profile_img";
            var destination = (flag==1) ? "filenameCred" : "filenamePhoto";
            var saida = document.getElementById(imageId);
            var quantos = saida.files.length;
 
            if(flag == 1){
                $("#filenameCred").empty();
            }
 
            for(i = 0; i < quantos; i++){
                // upload pdf
                var urls = URL.createObjectURL(event.target.files[i]);
                var file = event.target.files[i];
                if(file.type == "application/pdf"){
                    var urls = "{{ asset('images/pdf-icon.png') }}" ;
                }
 
                if(flag == 0) {
                    $("#filenamePhoto").empty();
                }
                document.getElementById(destination).innerHTML += '<img src="'+urls+'">';
                // document.getElementById(destination).innerHTML += '<div class="pip inline-block"><img src="'+urls+'"><span class="fa fa-times remove"></span></div>';
            }
        }
		
		$(document).on('click', '.remove', function() {
            $(this).parent(".pip").remove();
        });

		$("#cred_img").on("change", function(e){
			
		});
		
		$(document).ready(function() {
			$("#btnstepOne").on("click", function() {
				$(".ps_step_two").hide();
				$(".ps_step_one").fadeIn();
			});
			
			$("#btnstepTwoPrev").on("click", function() {
				$(".ps_step_three").hide();
				$(".ps_step_two").fadeIn();
			});
			$("#btnstepThreePrev").on("click", function() {
				$(".ps_step_four").hide();
				$(".ps_step_three").fadeIn();
			});
			
		});
			
		
		$("#accountInfo").submit(function(e) {
			e.preventDefault();
			$(".loader").css('display', 'flex');
			$("#accountInfo span.text-danger .error").html('');
			$.ajax({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				data: $('#accountInfo').serialize(),
				url: "{{ route('providersignup_account_info') }}",
				type: "POST",
				success: function (response) {
					if(response['status'] == 'success'){
						$(".loader").css('display', 'none');
						$(".ps_step_one").hide();
						$(".ps_step_two").fadeIn();	
					}
				},
				error: function (data) {
					$(".loader").css('display', 'none');
					let errors = data.responseJSON.errors;
					$.each(errors, function(key, value) {
						errorAlert(value[0],3000,'top-right');
					});	
				}
			});
		});

		$("#personalInfo").submit(function(e) {
			e.preventDefault();
			$(".loader").css('display', 'flex');
			$("#phone_number").unmask();
			$("#personalInfo span.text-danger .error").html('');
			$.ajax({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				data: $('#personalInfo').serialize(),
				url: "{{ route('providersignup_pernl_info') }}",
				type: "POST",
				success: function (response) {
					if(response['status'] == 'success'){
						$(".loader").css('display', 'none');
						$(".ps_step_two").hide();
						$(".ps_step_three").fadeIn();
					}
				},
				error: function (data) {
					$(".loader").css('display', 'none');
					let errors = data.responseJSON.errors;
					$.each(errors, function(key, value) {
						errorAlert(value[0],3000,'top-right');
					});	
				}
			});
		});
		/****Taking a global variable***/
		var gcredential = "";
		$("#professionalInfo").submit(function(e) {
			e.preventDefault();
			$(".loader").css('display', 'flex');
			$("#phone_number").unmask();
			$("#professionalInfo span.text-danger .error").html('');
			
			var name = $("#first_name").val()+' '+$("#last_name").val();
			var email = $("#email_id").val();
			var service = $("#service").val();
			
			var formData = new FormData(this);
			gcredential =  formData;
			formData.append('email', email);
			formData.append('name', name);
			formData.append('service', service);
			
			
			$.ajax({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				data: formData,
				url: "{{ route('providersignup_profs_info') }}",
				type: "POST",
				cache:false,
				contentType: false,
				processData: false,
				success: function (response) {
					if(response['status'] == 'success'){
						$(".loader").css('display', 'none');
						//successAlert(response['message'],2000,'top-right');
						callProgressBar();
						$(".ps_step_three").hide();
						$(".ps_step_four").fadeIn();
					}
				},
				error: function (data) {
					$(".loader").css('display', 'none');
					let errors = data.responseJSON.errors;
					$.each(errors, function(key, value) {
						errorAlert(value[0],3000,'top-right');
					});	
				}
			});
		});
		
		$("#msform").submit(function(e) {
			e.preventDefault();
			$(".loader").css('display', 'flex');
			$("#phone_number").unmask();
			var email_id = $("#email_id").val();
			var passwrd = $("#pwd").val();
			var firstname = $("#first_name").val();
			var lastname = $("#last_name").val();
			var phone = $("#phone_number").val();
			var service = $("#service").val();
			var enteredOtp = $("#email_v_otp").val();
			/**credential image value*/
			
			gcredential.append('email_id', email_id);
			gcredential.append('pwd', passwrd);
			gcredential.append('first_name', firstname);
			gcredential.append('last_name', lastname);
			gcredential.append('phone', phone);
			gcredential.append('enteredOtp', enteredOtp);
			gcredential.append('service', service);
		
			$.ajax({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				data: gcredential,
				url: "{{ route('providersignup_submission') }}",
				type: "POST",
				cache:false,
				contentType: false,
				processData: false,
				success: function (response) {
					if(response['status'] == 'success'){
						$(".loader").css('display', 'none');
						//successAlert(response['message'],2000,'top-right');
						callProgressBar();
						//$(".ps_step_four").hide();
						//$(".ps_step_five").fadeIn();
						setTimeout(function(){
							window.location = response['redirect'];
						}, 2000);
					}
					else{
						$(".loader").css('display', 'none');
						errorAlert(response['message'],2000,'top-right');						
					}
				},
				error: function (data) {
					$(".loader").css('display', 'none');
					let errors = data.responseJSON.errors;
					$.each(errors, function(key, value) {
						errorAlert(value[0],3000,'top-right');
					});	
				}
			});
		});
		
		function resendOtp()
		{
			$(".loader").css('display', 'flex');
			$.ajax({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				data: {},
				url: "{{ route('resend_email_otp') }}",
				type: "POST",
				success: function (response) {
					if(response['status'] == 'success'){
						$(".loader").css('display', 'none');
						//successAlert(response['message'],2000,'top-right');	
						callProgressBar();
					}
					else{
						$(".loader").css('display', 'none');
						errorAlert('Error occured.',3000,'top-right');						
					}
				},
				error: function (data) {
					$(".loader").css('display', 'none');
					let errors = data.responseJSON.errors;
					
					$.each(errors, function(key, value) {
						errorAlert(value[0],3000,'top-right');
					});					
					
				}
			});
		}
		
		

	</script>	
@endsection