@extends('front.online_visit_layout.app')

@section('content')
    <section class="sign_up_sec">
        <div class="sign_up_wrapper">
            <div class="left">
                <div class="owl-carousel clients">
                    <div class="item_box">
                        <div class="tk_b">
                            <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum.</p>
                            <div class="rate_by">
                                <div class="reviewr_img"><img src="./assets/images/c1.png" class="img-fluid" alt=""></div>
                                <div class="reviewer_info">
                                    <ul class="rating">
                                        <li><i class="fas fa-star"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                    </ul>
                                    <div class="reviewer_name">Mariya Brown</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item_box">
                        <div class="tk_b">
                            <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum.</p>
                            <div class="rate_by">
                                <div class="reviewr_img"><img src="./assets/images/c2.png" class="img-fluid" alt=""></div>
                                <div class="reviewer_info">
                                    <ul class="rating">
                                        <li><i class="fas fa-star"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                    </ul>
                                    <div class="reviewer_name">Ethen Haddox</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item_box">
                        <div class="tk_b">
                            <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum.</p>
                            <div class="rate_by">
                                <div class="reviewr_img"><img src="./assets/images/c3.png" class="img-fluid" alt=""></div>
                                <div class="reviewer_info">
                                    <ul class="rating">
                                        <li><i class="fas fa-star"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                    </ul>
                                    <div class="reviewer_name">Calicadoo</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="right">
                <div class="form_wrapper">
                    <h3 class="title">Get started with your online visit for {{$service_detail->name}}</h3>
                    <p class="des">This is an opportunity for you to tell your doctor about your health, medical history, and lifestyle. Your doctor will use this information to evaluate your symptoms and, if appropriate, prescribe medication for treatment.
                    </p>

                    <form id="signupForm">
                        <div class="form-group">
                            <input type="email" name="email" class="form-control" placeholder="Email" id="email" autofocus>
                        </div>
                        <div class="input_wrapper">
                            <div class="form-group">
                                <input type="text" name="first_name" class="form-control" placeholder="First Name" id="first_name">
                            </div>
                            <div class="form-group">
                                <input type="text" name="last_name" class="form-control" placeholder="Last Name" id="last_name">
                            </div>
                        </div>
                        <div class="input_wrapper">
                            <div class="form-group">
                                <input type="password" name="password" class="form-control" placeholder="Password" id="password">
                            </div>
                            <div class="form-group">
                                <input type="password" name="password_confirmation" class="form-control" placeholder="Confirm Password" id="confirm_password">
                            </div>
                        </div>
                        <div class="agree_with_terms_box">
                            <div class="checkbox d-inline-block">
                                <input type="checkbox" id="checkbox2" name="agree" value="1">
                                <label for="checkbox2"><span></span></label>
                            </div>
                            <span>I agree to <a href="{{env('APP_URL')}}/terms-conditions" class="terms">terms</a> and <a href="{{env('APP_URL')}}/privacy-policy">privacy policy</a> and consent to <a href="#">telehealth</a></span>
                        </div>
                        <button type="submit" id="saveBtn" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Processing" class="btn btn-primary">Submit</button>
						
                        <p class="mb-0">Already a member? <a href="{{env('APP_URL')}}/sign_in" class="login_link">Log in and continue</a></p>
                    </form>
                </div>
            </div>
			<!--Modal for email verification-->
			<div id="emailOtpModal" class="modal" tabindex="-1" role="dialog">
				<div class="modal-dialog modal-dialog-centered" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title">Email Verification</h5>
							{{--<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							  <span aria-hidden="true">&times;</span>
							</button>--}}
						</div>
						<form id="emailOtpForm">
							<div class="modal-body">
								<span>Please check your inbox to verify registered email-id.</span>
								<div>&nbsp;</div>
								<div class="form-group has-feedback">
									<label for="old_password">Enter OTP<span class="text-danger">*</span></label>
									<input type="password" placeholder="OTP" class="form-control" name="email_otp" id="email_otp" value="" maxlength="4">	
								</div>
							</div>
							<div class="modal-footer" style="justify-content: space-between;">
								<button type="button" id="resentBtn" class="accept_btn btn btn-outline-primary btn-sm w-auto border border-primary" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Processing" onclick="javascript: resendotp()">Resend OTP</button>
								
								<button type="submit" id="verifyBtn" class="accept_btn btn btn-outline-success btn-sm w-auto border border-success" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Processing">Verify</button>							
							</div>
						</form>
					</div>
				</div>
			</div>
			<!---End here--->
        </div>
    </section>

    <section class="terms_sec_2">
        <div class="content_wrapper shadow">
            <h4 class="heading">Terms and conditions of use</h4>
            <p class="description">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren,
                no sea takimata sanctus est</p>
            <p class="description">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren,
                no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam
                et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>
            <p class="description">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren,
                no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut </p>
            <p class="description">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren,
                no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut </p>
            <p class="description">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam</p>
            <div class="divider"></div>
            <a href="javascript:void(0)" class="close_btn"><span class="fa fas fa-times"></span></a>
        </div>
    </section>
	
	<script>

		$(document).ready(function() {
			if ($(window).width() <= 568) {
				let itemWidth = $(".owl-carousel.clients .owl-item:first-child");
				itemWidth = itemWidth[0].style.width;
				$(".owl-carousel.clients .owl-item").attr("style", "width: " + itemWidth + " !important;");
			}

			// Close terms modal
			$(".terms_sec_2 .close_btn").click(function() {
				$(".terms_sec_2").hide();
			});
			$(".sign_up_sec .terms").click(function() {
				$(".terms_sec_2 ").css('display', 'flex');
			});
		});
		
		$("#signupForm").submit(function(e) {
			e.preventDefault();
			$(".loader").css('display', 'flex');
			$('#saveBtn').html($('#saveBtn').attr('data-loading-text'));
			$('#saveBtn').addClass('disabled');
			
			$.ajax({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				data: $('#signupForm').serialize(),
				url: "{{ route('send_email_otp') }}",
				type: "POST",
				success: function (response) {
					if(response['status'] == 'success'){
						$("#emailOtpModal").modal({
							backdrop: 'static',
							keyboard: false
						});
						$(".loader").css('display', 'none');
						$("#emailOtpForm").trigger('reset');
						$("#emailOtpForm span.text-danger .error").html('');
						$('#saveBtn').html('Submit');
						$('#saveBtn').removeClass('disabled');						
						
					}else{
						errorAlert('Error occured.',3000,'top-right');
						$('#saveBtn').html('Submit');
						$('#saveBtn').removeClass('disabled');	
					}
				},
				error: function (data) {
					$(".loader").css('display', 'none');
					let errors = data.responseJSON.errors;
					
					$.each(errors, function(key, value) {
						errorAlert(value[0],3000,'top-right');
					});
					$('#saveBtn').html('Submit');
					$('#saveBtn').removeClass('disabled');	
				}
			});
		});
	
		$("#emailOtpForm").submit(function(e) {
				e.preventDefault();
				$('#verifyBtn').html($('#verifyBtn').attr('data-loading-text'));
				$('#verifyBtn').addClass('disabled');
				var first_name = $("#first_name").val();
				var last_name  = $("#last_name").val();
				var email = $("#email").val();
				var password = $("#password").val();
				var email_otp = $("#email_otp").val();
				$(".loader").css('display', 'flex');
				$.ajax({
					headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					},
					data: {"first_name":first_name, "last_name":last_name, "email":email, "password":password,"email_otp":email_otp},
					url: "{{ route('signup_submission') }}",
					type: "POST",
					success: function (response) {
						if(response['status'] == 'success'){
							$(".loader").css('display', 'none');
							$("#emailOtpModal").modal('hide');
							//successAlert(response['message'],2000,'top-right');
							$('#verifyBtn').removeClass('disabled');
							$('#verifyBtn').html('Verify');
							callProgressBar();
							setTimeout(function(){
								window.location = response['redirect'];
							}, 2000);	
						}
						else{
							errorAlert(response['message'],3000,'top-right');
							$('#verifyBtn').removeClass('disabled');
						    $('#verifyBtn').html('Verify');	
						}
						
					},
					error: function (data) {
						$('#verifyBtn').removeClass('disabled');
						$('#verifyBtn').html('Verify');
						$(".loader").css('display', 'none');
						let errors = data.responseJSON.errors;
						
						$.each(errors, function(key, value) {
							errorAlert(value[0],3000,'top-right');
						});					
						
					}
				});
			});

			function resendotp()
			{	
				$('#resentBtn').html($('#resentBtn').attr('data-loading-text'));
				$('#resentBtn').addClass('disabled');
				$.ajax({
					headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					},
					data: {},
					url: "{{ route('resend_email_otp') }}",
					type: "POST",
					success: function (response) {
						if(response['status'] == 'success'){
							//successAlert(response['message'],2000,'top-right');
							$('#resentBtn').html('Resend OTP');	
							$('#resentBtn').removeClass('disabled');	
							callProgressBar();
						}
						else{
							errorAlert('Error occured.',3000,'top-right');
							$('#resentBtn').html('Resend OTP');	
							$('#resentBtn').removeClass('disabled');	
						}
					},
					error: function (data) {
						$(".loader").css('display', 'none');
						$('#resentBtn').html('Resend OTP');	
						$('#resentBtn').removeClass('disabled');
						let errors = data.responseJSON.errors;
						
						$.each(errors, function(key, value) {
							errorAlert(value[0],3000,'top-right');
						});					
						
					}
				});
			}
		
	</script>	
@endsection