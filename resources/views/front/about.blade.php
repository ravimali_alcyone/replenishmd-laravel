@extends('front.layouts.app')
@section('content')

	<style>
		.inner_banner.about_banner {
			background-image: url(/{{ $page_data['banner']->image->image_1 ?? '' }});
		}
	</style>
	<section class="inner_banner about_banner">
        <div class="container">
            <div class="content_wrapper">
                <div class="b_text">
                    <h1>@if(isset($page_data['banner']->heading)) {{ $page_data['banner']->heading }}@endif</h1>
                    <p>@if(isset($page_data['banner']->description)) {{ $page_data['banner']->description }}@endif</p>
                </div>
            </div>
        </div>
        <img src="/assets/images/bottom_curve.svg" alt="bottom_curve">
    </section>

    <section class="client_feedback_sec">
        <div class="container">
            <div class="feedback_wrapper">
                <div class="feedback">
                    <span class="fas fa-quote-left"></span>
                    <p>@if(isset($page_data['feedback']->description)) {{ $page_data['feedback']->description }}@endif</p>
                    <img src="/assets/images/feedback_separator.png" alt="feedback_separator">
                    <div class="client">
                        <img src="{{env('APP_URL')}}/{{ $page_data['feedback']->image->image_1 ?? '' }}" alt="client_img">
                        <span>@if(isset($page_data['feedback']->clientfeed)) {{ $page_data['feedback']->clientfeed }}@endif</span>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="about_sec">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-6 order-md-1 order-2">
                    <div class="content_wrapper">
                        <p class="title_sm_red text-uppercase">About US</p>
                        <h1 class="title">@if(isset($page_data['aboutus']->heading)) {{ $page_data['aboutus']->heading }}@endif</h1>
                        <p class="p_md">@if(isset($page_data['aboutus']->description)) {{ $page_data['aboutus']->description }}@endif</p>
                        <ul>
                            @if(!empty($page_data['aboutus']->listdata))
                                @foreach($page_data['aboutus']->listdata as $key => $value)								
                                <li>
                                    <span class="fa fas fa-check"></span>
                                    <p>{{ $value->listitems }}</p>
                                </li>
                                @endforeach
                            @endif
                        </ul>
                        <a href="{{env('APP_URL')}}/{{ $page_data['aboutus']->button_url ?? '' }}" class="btn btn-primary button">@if(isset($page_data['aboutus']->button_text)) {{ $page_data['aboutus']->button_text }}@endif</a>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 order-md-2 order-1">
                    <div class="img_wrapper">
                        <img src="/assets/images/about_img.svg" alt="about_img" class="w-auto">
                    </div>
                </div>
            </div>
            <div class="row expertise_row">
                <div class="col-sm-12 col-md-6">
                    <div class="img_wrapper">
                        <img src="{{env('APP_URL')}}/{{ $page_data['our_expertise']->image->image_1 ?? '' }}" alt="about_expertise" class="w-auto">
                    </div>
                </div>
                <div class="col-sm-12 col-md-6">
                    <div class="content_wrapper">
                        <p class="title_sm_red text-uppercase">Our Expertise</p>
                        <h1 class="title">@if(isset($page_data['our_expertise']->heading)) {{ $page_data['our_expertise']->heading }}@endif</h1>
                        <p class="p_md">@if(isset($page_data['our_expertise']->description)) {{ $page_data['our_expertise']->description }}@endif</p>
                        <ul>
                            @if(!empty($page_data['our_expertise']->listdata))
                                @foreach($page_data['our_expertise']->listdata as $key => $value)								
                                <li>
                                    <span class="fa fas fa-check"></span>
                                    <p>{{ $value->listitems }}</p>
                                </li>
                                @endforeach
                            @endif
                            
                        </ul>
                        <a href="{{env('APP_URL')}}/{{ $page_data['our_expertise']->button_url ?? '' }}" class="btn btn-primary button">@if(isset($page_data['our_expertise']->button_text)) {{ $page_data['our_expertise']->button_text }}@endif</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="work_join_sec about">
        <div class="data_wrap">
            <div class="container">
                <div class="content_wrapper">
                    <div class="title">@if(isset($page_data['ourwork']->top_main_name)) {{ $page_data['ourwork']->top_main_name }}@endif
                        <p>@if(isset($page_data['ourwork']->top_main_name)) {{ $page_data['ourwork']->top_main_name }}@endif</p>
                    </div>
                    <div class="boxes_wrapper d_flex_j_center">
                        <div class="box">
                            <span class="icon"><img src="{{ $page_data['ourwork']->image->image_1 ?? '' }}" class="work_join_icons" alt="dollar"></span>
                            <h6 class="title">@if(isset($page_data['ourwork']->heading_1)) {{ $page_data['ourwork']->heading_1 }}@endif</h6>
                            <p>@if(isset($page_data['ourwork']->content_1)) {{ $page_data['ourwork']->content_1 }}@endif</p>
                        </div>
                        <div class="box">
                            <span class="icon"><img src="{{ $page_data['ourwork']->image->image_2 ?? '' }}" class="work_join_icons" alt="Hand-shake"></span>
                            <h6 class="title">@if(isset($page_data['ourwork']->heading_2)) {{ $page_data['ourwork']->heading_2 }}@endif</h6>
                            <p>@if(isset($page_data['ourwork']->content_2)) {{ $page_data['ourwork']->content_2 }}@endif</p>
                        </div>
                        <div class="box">
                            <span class="icon"><img src="{{ $page_data['ourwork']->image->image_3 ?? '' }}" class="work_join_icons" alt="metro_chart_icon"></span>
                            <h6 class="title">@if(isset($page_data['ourwork']->heading_3)) {{ $page_data['ourwork']->heading_3 }}@endif</h6>
                            <p>@if(isset($page_data['ourwork']->content_3)) {{ $page_data['ourwork']->content_3 }}@endif</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


@endsection