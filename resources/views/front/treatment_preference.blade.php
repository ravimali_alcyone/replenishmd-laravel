@extends('front.online_visit_layout.app')

@section('content')
    <div class="tabs_wrapper welcome basics m_qs">
        <div>
			@if($drugs && $drugs->count() > 0)
				<a href="javascript:void(0);" class="how_it_works">Treatment Preference</a>
			@else
				<a href="javascript:void(0);" class="how_it_works">Other Details</a>
			@endif
            <div class="steps">
                <span class="active ml-0"></span>
                <span class="active"></span>
                <span class="active"></span>
                <span></span>
            </div>
        </div>
        <span id="pageBackBtn" class="back_arrow fa fas fa-arrow-left" onclick="location.href = '{{ route('medical_questions') }}';"></span>
        <span id="blockBackBtn" class="back_arrow fa fas fa-arrow-left" onclick="goPrev()" style="display:none;"></span>		
    </div>

    <div class="main_welcome_wrapper">
	@if($drugs && $drugs->count() > 0)	
        <section id="welcome_section" class="welcome_content_wrapper basic medical_qs">
            <div class="medical_qs_welcome">
                <h3 class="heading">Treatment Preference</h3>
                <p class="description text-center">If you have a preferred treatment, you can let your doctor know here. While your preference will be taken into consideration, your doctor will prescribe the treatment option best suited for you.</p>
                <div class="img_wrapper text-center">
                    <img src="{{asset('assets/images/treatment_preferemce.svg')}}" alt="treatment_preferemce" class="img-fluid">
                </div>
                <button type="button" class="btn btn-primary" id="welc_btn">Continue</button>
            </div>
        </section>

		<section id="medical_uses" style="display:none;">
			<div class="container number_uses">

				<div class="num_uses">
					<p>Number of Uses</p>
					<h5>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</h5>
					<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>

					<div class="select_box">
						<div class="select_details">
							<div class="check_1">
								<input type="radio" id="usage_4" name="quantity" value="4">
								<label for="usage_4">Use <span>4 times</span> per month </label>
							</div>
							<div class="roman_pic">
								<img src="{{asset('assets/images/iop.svg')}}" alt="">

							</div>
						</div>
					</div>

					<div class="select_box">
						<div class="select_details">
							<div class="check_1">
								<input type="radio" id="usage_6" name="quantity" value="6">
								<label for="usage_6">Use <span>6 times</span> per month </label>
							</div>
							<div class="roman_pic">
								<img src="{{asset('assets/images/iop_2.svg')}}" alt="">

							</div>
						</div>
					</div>


					<div class="select_box">
						<div class="select_details">
							<div class="check_1">
								<input type="radio" id="usage_8" name="quantity" value="8">
								<label for="usage_8">Use <span>8 times</span> per month </label>
							</div>
							<div class="roman_pic">
								<img src="{{asset('assets/images/iop_3.svg')}}" alt="">

							</div>
						</div>
					</div>

					<div class="select_box">
						<div class="select_details">
							<div class="check_1">
								<input type="radio" id="usage_10" name="quantity" value="10">
								<label for="usage_10">Use <span>10 times</span> per month </label>
							</div>
							<div class="roman_pic">
								<img src="{{asset('assets/images/iop_4.svg')}}" alt="">

							</div>
						</div>
					</div>
					
				</div>
				
			</div>
		</section>   

        <section id="medical_drugs" style="display:none;">
            <div class="container number_uses">

                <div class="num_uses">
                    <p>Drugs</p>
                    <h5>Your preference will be shared with a physician, who will use their medical judgement to determine the best treatment plan.</h5>
                    <p>Prices listed are from the Ro Pharmacy Network for the recommended starting dose for a healthy patient. Most prescriptions come in non-childproof, single-dose packaging, though you may request childproof packaging for your medications.</p>
			
			
				@foreach($drugs as $key => $value)
                    <div class="select_box">
                        <div class="drug_details">
                            <div class="check_1">
                                <input type="radio" id="drugs_{{$value->id}}" name="drugs" value="{{$value->id}}">
                                <label for="drugs_{{$value->id}}"><span>{{$value->name}}</span></label>
                                <div class="drug_p">
                                    <Span>${{$value->start_price}}</Span>
                                    <p>per dose</p>
                                </div>
                            </div>

                            <div class="roman_pic drugs">
							@php
								$images = json_decode($value->images,true);
								
								if($images){
									$display_img = $images[0];
								}else{
									$display_img = 'assets/images/default_medicine.jpg';
								}
							@endphp
                                <img src="{{env('APP_URL')}}/{{$display_img}}" alt="{{$value->name}}">
                            </div>
                        </div>
                    </div>
				@endforeach

                    <div class="select_box">
                        <a href="javascript:void(0);" onclick="setNoPref(this)">
                            <div class="drug_details">
                                <div class="prefer">
                                    <p>No Preference</p>
                                    <p>Please tell my Doctor I do not have a drug preference</p>
                                </div>

                            </div>
                        </a>
                    </div>
                </div>
			</div>
        </section>
		
		<section id="medical_dosage" style="display:none;">
		
		</section>		

	@endif
	@php $ps = 1; @endphp
	@if(!$user->image || $user->image == '')
		
		<section id="take_photo" @if($drugs && $drugs->count() > 0)	style="display:none;"@endif>
			<div class="container pic">

				<div class="cam_pic">
					<P class="para_1">STEP {{$ps}}</P>
					@php $ps++; @endphp
					<h5>Share a photo of yourself</h5>
					<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
					<p><a href="javascript:void(0)">How does this work?</a></p>

					<div class="face_1" id="imageView">
						<img class="face_img" src="{{asset('assets/images/face.svg')}}" />
					</div>
						<form id="ProfileImageForm">
							<input type="file" name="image" id="user_image" style="visibility:hidden;">

							<button type="submit" id="saveBtn" style="display:none;">Upload</button>
						</form>
					<div class="text-center submitBtn">	
						<button type="button" class="pic_up btn btn-primary" id="photo_submit">Take or Upload Photo</button>
						<button type="button" class="pic_up btn btn-primary" id="photo_submit_skip">Skip</button>
					</div>

				</div>
			</div>
		</section>
	@endif
	
	@if(!$user->id_card_image || $user->id_card_image == '')
		<section id="take_id_card" style="display:none;">
			<div class="container pic">

				<div class="cam_pic">
					<P class="para_1">STEP {{$ps}}</P>
					<h5>Let's add your ID</h5>
					<p>Your doctor or nurse pratitioner will use your photo to verify your identity. This helps us confirm that you're really you.</p>
					<p><a href="javascript:void(0)">How does this work?</a></p>

					<div class="face_1" id="imageView2">
						<img class="face_img" src="{{asset('assets/images/face.svg')}}">
					</div>

						<form id="IDCardImageForm">
							<input type="file" name="image" id="id_card_image" style="visibility:hidden;">

							<button type="submit" id="saveBtn2" style="display:none;">Upload</button>
						</form>
					<div class="text-center submitBtn">	
						<button type="button" class="pic_up btn btn-primary" id="id_card_submit">Take or Upload ID Photo</button>
						<button type="button" class="pic_up btn btn-primary" id="id_card_submit_skip">Skip</button>
					</div>						
					

				</div>
			</div>
		</section>		
	@endif

		<section id="shipping_info" style="display:none;">
			<div class="container ship">

				<div class="ship_info">
					<h5>Shipping Information</h5>
					<div class="shipping_box">
						<div class="ship_img">
							<img src="{{asset('assets/images/Shipping.png')}}" alt="">
						</div>
						<div class="our_phar">
							<h5>Our Pharmacy</h5>
							<div class="sbox_1">
								<i class="fa fa-check" aria-hidden="true"></i>
								<p>FREE 2 Day Shipping</p>
							</div>
							<div class="sbox_1">
								<i class="fa fa-check" aria-hidden="true"></i>
								<p>Discreet Packaging</p>
							</div>
							<div class="sbox_1">
								<i class="fa fa-check" aria-hidden="true"></i>
								<p>Guarenteed Pricing</p>
							</div>
							<div class="sbox_1">
								<i class="fa fa-check" aria-hidden="true"></i>
								<p>Non-childproof, single dose-packaging</p>
							</div>
						</div>

						<div class="ship_d">
							<p><a href="javascript:void(0)">Select different pharmacy</a></p>
						</div>
					</div>
					<h5>Shipping Address</h5>
					<p>Where would you like to have your medication shipped, if prescribed?</p>
					
					<div class="ship_add mb-3">
						<input type="text" name="shipping_address1" placeholder="Street Address" id="shipping_address1" value="" required>
					</div>
					
					<div class="ship_add mb-3">
						<input type="text" name="shipping_address2" placeholder="Address 2" id="shipping_address2" value="">
					</div>

					<div class="ship_add mb-3">
						<input type="text" name="shipping_city" placeholder="City" id="shipping_city" value="" required>
					</div>	

					<div class="ship_add state mb-3">
						<select name="shipping_state" id="shipping_state"  class="custom-select select2" required>
							<option value="">Please Select State</option>
					@if($states)
						@foreach($states as $key => $value)
							<option value="{{ $value->code }}">{{ $value->name }}</option>
						@endforeach
					@endif
						</select>

						<input type="text" name="shipping_zipcode" placeholder="Zipcode" id="shipping_zipcode" value="" maxlength="5"  onkeypress="return event.charCode >= 48 && event.charCode <= 57" value="@if($user->zip_code){{$user->zip_code}}@endif" data-toggle="tooltip" data-placement="right" title="Only Texas zipcodes are allowable." required>
					</div>						

					<button type="button" class="save_b btn btn-primary" id="shipping_submit">Save and Continue</button>
				</div>

			</div>
		</section>

		<section id="treatment_review" style="display:none;">

		</section>
	
    </div>
    
    <script>
	
	var bSec = '';
	var cSec = '';
	var no_preferance = false;
	var quantity;
	var drug;
	var variant;
	var shipping_address;
	
        $(document).ready(function() {
            $("#welc_btn").click(function() {
                $("#welcome_section").hide();
                $("#medical_uses").show();
				bSec = 'welcome_section';
				cSec = 'medical_uses';
				$('#blockBackBtn').show();
				$('#pageBackBtn').hide();				
            });
        });
		
		
		$("#medical_uses .select_box .select_details input[type=radio]").click(function(){
			
			if ($(this).prop("checked")) {
				$("#medical_uses").hide();
				$("#medical_drugs").show();
				bSec = 'medical_uses';
				cSec = 'medical_drugs';
				quantity = $(this).val();
			}
		});
		
		
		$("#medical_drugs .select_box .drug_details input[type=radio]").click(function(){
			
			if ($(this).prop("checked")) {
				
				getVariants($(this).val());
				
				$("#medical_drugs").hide();
				$("#medical_dosage").show();
				bSec = 'medical_drugs';
				cSec = 'medical_dosage';
				no_preferance = false;
				drug = $(this).val();
				
			}
		});

	<?php if($drugs && $drugs->count() > 0){?>
		function dosageSelect(e){
			if ($(e).prop("checked")) {
				$("#medical_dosage").hide();
				bSec = 'medical_dosage';
				variant = $(e).val();
			<?php if(!$user->image || $user->image == ''){?>
				console.log('take_photo');
				$("#take_photo").show();
				cSec = 'take_photo';
			<?php }elseif(!$user->id_card_image || $user->id_card_image == ''){ ?>	
				console.log('take_id_card');
				$("#take_id_card").show();
				cSec = 'take_id_card';
			<?php }else{ ?>
				console.log('shipping_info');
				$("#shipping_info").show();
				cSec = 'shipping_info';				
			<?php }?>
				
			}			
		}
	<?php }else{ ?>	
			<?php if(!$user->image || $user->image == ''){?>
				console.log('take_photo');
				$("#take_photo").show();
				cSec = 'take_photo';
			<?php }elseif(!$user->id_card_image || $user->id_card_image == ''){ ?>	
				console.log('take_id_card');
				$("#take_id_card").show();
				cSec = 'take_id_card';
			<?php }else{ ?>
				console.log('shipping_info');
				$("#shipping_info").show();
				cSec = 'shipping_info';				
			<?php }?>	
	<?php } ?>
				
		$("#photo_submit").click(function(){
			$("#saveBtn").trigger('click');			
		});
		
		
		
		$("#photo_submit_skip").click(function(){

			swal({
				title: "Are you sure to skip?",
				text: "You can be upload photo later.",
				icon: '',
				buttons: {
				  cancel: true,
				  delete: 'Yes, Skip It'
				}
			  }).then((isConfirm) => {
			  if (!isConfirm) {
				return false;
			  } else {
				$("#take_photo").hide();
				$("#take_id_card").show();
				bSec = 'take_photo';
				cSec = 'take_id_card';
			  }
			});
				
		});		
		


		$("#id_card_submit").click(function(){
			$("#saveBtn2").trigger('click');				
		});		



		$("#id_card_submit_skip").click(function(){
			
			swal({
				title: "Are you sure to skip?",
				text: "You can be upload photo later.",
				icon: '',
				buttons: {
				  cancel: true,
				  delete: 'Yes, Skip It'
				}
			  }).then((isConfirm) => {
			  if (!isConfirm) {
				return false;
			  } else {
				$("#take_id_card").hide();
				$("#shipping_info").show();
				bSec = 'take_id_card';
				cSec = 'shipping_info';
			  }
			});			
		
		});
		
		
		$("#shipping_submit").click(function(){
			
			if($("#shipping_address1").val() == ''){ alert('Please enter your street address.'); return false; }
			
			if($("#shipping_city").val() == ''){ alert('Please enter your city.'); return false; }
			
			if($("#shipping_state").val() == ''){ alert('Please select your state.'); return false; }
			
			if($("#shipping_zip").val() == ''){ alert('Please enter your zipcode.'); return false; }
			
			shipping_address = {
				'shipping_address1' : $("#shipping_address1").val(),
				'shipping_address2' : $("#shipping_address2").val(),
				'shipping_city' : $("#shipping_city").val(),
				'shipping_state' : $("#shipping_state").val(),
				'shipping_zipcode' : $("#shipping_zipcode").val(),
				'shipping_country' : 'US',
			};

			getTreatmentReview();
			$("#shipping_info").hide();
			$("#treatment_review").show();
			bSec = 'shipping_info';
			cSec = 'treatment_review';

		});
		
		
		function goPrev(){
			if(bSec != '' && cSec != ''){
				
				console.log('bSec',bSec);
				console.log('cSec',cSec);
						
				$('#'+bSec).show();
				$('#'+cSec).hide();
				
				if(bSec == 'welcome_section' && cSec == 'medical_uses'){
					$('#blockBackBtn').hide();
					$('#pageBackBtn').show();
					cSec = 'welcome_section';
					bSec = '';
				}else{
					$('#blockBackBtn').show();
					$('#pageBackBtn').hide();
					let new_btd = $('#'+bSec).prev('section').css({"display": "none"}).attr('id');
					cSec = bSec;
					bSec = new_btd;
				}
					console.log('new current div',cSec);
					console.log('new back to div',bSec);				
			}
		}
		
		function setNoPref(){
			no_preferance = true;
			$("#medical_drugs").hide();
			$("#take_photo").show();
			bSec = 'medical_drugs';
			cSec = 'take_photo';		
						
		}
		
		function getVariants(id) {
			
			let drug_id = id;
			$(".loader").css('display', 'flex');
			
			$.ajax({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				data: { 'drug_id' : drug_id},
				url: "{{ route('get_medicine_variants') }}",
				type: "POST",
				// dataType: 'json',
				success: function (response) {
					$(".loader").css('display', 'none');
					if(response['status'] == 'success'){
						$('#medical_dosage').html(response['data']);
					}else{
						errorAlert(response['message'],5000,'bottom-left');
					}
				},
				error: function (data) {
					$(".loader").css('display', 'none');
					let errors = data.responseJSON.errors;
					
					$.each(errors, function(key, value) {
						errorAlert(value[0],5000,'bottom-left');
					});					
 					swal({
						title: 'Error Occured.',
						icon: 'error'
					})
				}
			});
		}

		function getTreatmentReview() {
			
			let req_data = {
				'drug_id' : drug,
				'quantity' : quantity,
				'no_preferance' : no_preferance,
				'variant_id' : variant,
				'shipping_address' : shipping_address, //object
			};
					
			$(".loader").css('display', 'flex');
			
			$.ajax({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				data: req_data,
				url: "{{ route('get_treatment_review') }}",
				type: "POST",
				// dataType: 'json',
				success: function (response) {
					$(".loader").css('display', 'none');
					if(response['status'] == 'success'){
						$('#treatment_review').html(response['data']);
					}else{
						errorAlert(response['message'],5000,'bottom-left');
					}
				},
				error: function (data) {
					$(".loader").css('display', 'none');
					let errors = data.responseJSON.errors;
					
					$.each(errors, function(key, value) {
						errorAlert(value[0],5000,'bottom-left');
					});					
 					swal({
						title: 'Error Occured.',
						icon: 'error'
					})
				}
			});
		}
		
		$('#imageView').click(function(){
			$('#user_image').click();
		})
		
		$('#imageView2').click(function(){
			$('#id_card_image').click();
		})		
		
		
		var fileTypes = ['jpg', 'jpeg', 'png'];  //acceptable file types
		
		// Image reader
		if (window.File && window.FileList && window.FileReader) {
			
			//Profile Photo
			$("#user_image").on("change", function(e) {
				var files = e.target.files,
				filesLength = files.length;
				var isSuccess = false;
				for (var i = 0; i < filesLength; i++) {
					var f = files[i];
					var fileReader = new FileReader();
					fileReader.onload = (function(e) {
						var file = e.target;
						var extension = f.name.split('.').pop().toLowerCase();  //file extension from input file
						isSuccess = fileTypes.indexOf(extension) > -1;  //is extension in acceptable types
						
						if(isSuccess){
							let image = "<img class='face_img' src=\"" + e.target.result + "\" title=\"" + f.name + "\"/>";
						
							$("#imageView").html(image);							
						}else{
							alert('Only jpg/png image files are allowable.');
						}

					});
					fileReader.readAsDataURL(f);
				}
			});
			
			

			//ID Card Photo
			$("#id_card_image").on("change", function(e) {
				var files = e.target.files,
				filesLength = files.length;
				var isSuccess = false;
				for (var i = 0; i < filesLength; i++) {
					var f = files[i];
					var fileReader = new FileReader();
					fileReader.onload = (function(e) {
						var file = e.target;
						var extension = f.name.split('.').pop().toLowerCase();  //file extension from input file
						isSuccess = fileTypes.indexOf(extension) > -1;  //is extension in acceptable types
						
						if(isSuccess){
							let image = "<img class='face_img' src=\"" + e.target.result + "\" title=\"" + f.name + "\"/>";
						
							$("#imageView2").html(image);							
						}else{
							alert('Only jpg/png image files are allowable.');
						}

					});
					fileReader.readAsDataURL(f);
				}
			});
			
		} else {
			alert("Your browser doesn't support to File API")
		}
	
		$("#ProfileImageForm").submit(function(e) {
			e.preventDefault();
			$(".loader").css('display', 'flex');
			var formData = new FormData(this);

			$.ajax({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				url: "{{ route('online_visit.profile_image') }}",
				data: formData,
				type: "POST",
				cache:false,
				contentType: false,
				processData: false,
				// dataType: 'json',
				success: function (response) {
					$(".loader").css('display', 'none');
					if(response['status'] == 'success'){
						
						swal({
							title: response['message'],
							icon: 'success'
						});
						$('#ProfileImageForm').trigger("reset");

						setTimeout(function(){
							swal.close();
							$("#take_photo").hide();
							$("#take_id_card").show();
							bSec = 'take_photo';
							cSec = 'take_id_card';							
						}, 3000);
					
					}else{
						swal({
							title: response['message'],
							icon: 'error'
						});
						setTimeout(function(){
							swal.close();							
						}, 3000);						
					}
				},
				error: function (data) {
					$(".loader").css('display', 'none');
				  let Err = 'Error Occured';
				  let errors = data.responseJSON.errors;
				  
				  $.each(errors, function(key, value) {
					  Err = value[0];
				  });
				  
				  swal({
					title: Err,
					icon: 'error'
				  })
				}
			});
		});
	
	
		$("#IDCardImageForm").submit(function(e) {
			e.preventDefault();
			$(".loader").css('display', 'flex');
			var formData = new FormData(this);

			$.ajax({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				url: "{{ route('online_visit.id_card_image') }}",
				data: formData,
				type: "POST",
				cache:false,
				contentType: false,
				processData: false,
				// dataType: 'json',
				success: function (response) {
					$(".loader").css('display', 'none');
					if(response['status'] == 'success'){
						
						swal({
							title: response['message'],
							icon: 'success'
						});
						$('#IDCardImageForm').trigger("reset");

						setTimeout(function(){
							swal.close();
							$("#take_id_card").hide();
							$("#shipping_info").show();
							bSec = 'take_id_card';
							cSec = 'shipping_info';						
						}, 3000);
					
					}else{
						swal({
							title: response['message'],
							icon: 'error'
						});
						setTimeout(function(){
							swal.close();							
						}, 3000);						
					}
				},
				error: function (data) {
					$(".loader").css('display', 'none');
				  let Err = 'Error Occured';
				  let errors = data.responseJSON.errors;
				  
				  $.each(errors, function(key, value) {
					  Err = value[0];
				  });
				  
				  swal({
					title: Err,
					icon: 'error'
				  })
				}
			});
		});	
		function goToPayment(e){
			var setPin = '<?php echo auth()->user()->payment_confirmation_pin; ?>';
			
			let p_type = $('input[name="md_payment_type"]:checked').val();
			
			let m_intveral = (p_type==1) ? $("#recurring_interval").val() : "";
			var purl ="";
			if ($(e).prop("checked")) {
				if($(e).val() == 'payment_card'){
					$(".loader").css('display', 'flex');
					if(setPin=="" || setPin==null)
					{
						purl = (m_intveral !="" ) ? "{{ env('APP_URL') }}/user/set_pin/"+p_type+"/"+m_intveral : "{{ env('APP_URL') }}/user/set_pin/"+p_type ;
						setTimeout(function(){
							window.location = purl;
						}, 500);
					}
					else
					{
						purl = (m_intveral !="" ) ? "{{ env('APP_URL') }}/payment_page/"+p_type+"/"+m_intveral : "{{ env('APP_URL') }}/payment_page/"+p_type ;
						setTimeout(function(){
							window.location = purl;
						}, 500);
					}					
				}
			}
		}

		function show_recurring_options(e){
			
			if(e.value == 1){ //custom
				$('#recurring_options_view').show();
			}else{
				$('#recurring_options_view').hide();
				$('#interval_count').val(1);
				$('.rec_custom_options').hide();
				$('#recurring_payment_1').trigger('click');				
				$('#custom_plan_interval').val('month');				
			}
		}
		
		function show_custom_options(e){
			
			if(e.value == 'custom'){ //custom
				$('.rec_custom_options').show();
			}else{
				$('.rec_custom_options').hide();
				$('#interval_count').val(1);
				$('#custom_plan_interval').val('month');
			}
		}
				
		$('[data-toggle="tooltip"]').tooltip(); 
		
		$("#shipping_state").select2();
    </script>	

@endsection

