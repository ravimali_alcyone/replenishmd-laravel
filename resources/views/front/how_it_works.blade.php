@extends('front.layouts.app')

@section('content')

    <style>
        .inner_banner.hiw_banner {
            background-image: url(/{{ $page_data['banner']->image->image_1 ?? '' }});
        }
    </style>
    <section class="inner_banner hiw_banner">
        <div class="container">
            <div class="content_wrapper">
                <div class="b_text">
                    <h1>@if(isset($page_data['banner']->heading)) {{ $page_data['banner']->heading }}@endif</h1>
                    <p>@if(isset($page_data['banner']->description)) {{ $page_data['banner']->description }}@endif</p>
                </div>
            </div>
        </div>
        <img src="/assets/images/bottom_curve.svg" alt="bottom_curve">
    </section>

    <div class="why_rmd">
        <div class="title">@if(isset($page_data['why_rmd']->top_main_name)) {{ $page_data['why_rmd']->top_main_name }}@endif
            <p>@if(isset($page_data['why_rmd']->top_main_content)) {{ $page_data['why_rmd']->top_main_content }}@endif</p>
        </div>
        <div class="step_wrap">
            <div class="step_block">
                <div class="step_img">
                    <div class="mob_img"><img class="frt_img" src="{{ $page_data['why_rmd']->image->image_1 ?? ''}}" alt=""><img class="phone_mock" src="{{ $page_data['why_rmd']->image->image_1_1 ?? ''}}" alt=""></div>
                </div>
                <div class="step_details">
                    <div class="stp_box">
                        <div class="step_number"><span>Step 01</span></div>
                        <div class="stp_head">@if(isset($page_data['why_rmd']->heading_1)) {{ $page_data['why_rmd']->heading_1 }}@endif</div>
                        <div class="stp_info">@if(isset($page_data['why_rmd']->content_1)) {{ $page_data['why_rmd']->content_1 }}@endif</div>
                    </div>
                </div>
            </div>
            <div class="step_block">
                <div class="step_img">
                    <div class="mob_img"><img class="frt_img" src="{{ $page_data['why_rmd']->image->image_2 ?? ''}}" alt=""><img class="phone_mock" src="{{ $page_data['why_rmd']->image->image_2_1 ?? ''}}" alt=""></div>
                </div>
                <div class="step_details">
                    <div class="stp_box">
                        <div class="step_number"><span>Step 02</span></div>
                        <div class="stp_head">@if(isset($page_data['why_rmd']->heading_2)) {{ $page_data['why_rmd']->heading_2 }}@endif</div>
                        <div class="stp_info">@if(isset($page_data['why_rmd']->content_2)) {{ $page_data['why_rmd']->content_2 }}@endif</div>
                    </div>
                </div>
            </div>
            <div class="step_block">
                <div class="step_img">
                    <div class="mob_img"><img class="frt_img" src="{{ $page_data['why_rmd']->image->image_3 ?? ''}}" alt=""><img class="phone_mock" src="{{ $page_data['why_rmd']->image->image_3_1 ?? ''}}" alt=""></div>
                </div>
                <div class="step_details">
                    <div class="stp_box">
                        <div class="step_number"><span>Step 03</span></div>
                        <div class="stp_head">@if(isset($page_data['why_rmd']->heading_3)) {{ $page_data['why_rmd']->heading_3 }}@endif</div>
                        <div class="stp_info">@if(isset($page_data['why_rmd']->content_3)) {{ $page_data['why_rmd']->content_3 }}@endif</div>
                    </div>
                </div>
            </div>
            <div class="step_block">
                <div class="step_img">
                    <div class="mob_img"><img class="frt_img" src="{{ $page_data['why_rmd']->image->image_4 ?? ''}}" alt=""><img class="phone_mock" src="{{ $page_data['why_rmd']->image->image_4_1 ?? ''}}" alt=""></div>
                </div>
                <div class="step_details">
                    <div class="stp_box">
                        <div class="step_number"><span>Step 04</span></div>
                        <div class="stp_head"> @if(isset($page_data['why_rmd']->heading_4)) {{ $page_data['why_rmd']->heading_4 }}@endif</div>
                        <div class="stp_info">@if(isset($page_data['why_rmd']->content_4)) {{ $page_data['why_rmd']->content_4 }}@endif</div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <section class="work_join_sec">
        <div class="data_wrap">
            <div class="container">
                <div class="content_wrapper">
                    <div class="title"> @if(isset($page_data['joinus']->top_main_name)) {{ $page_data['joinus']->top_main_name }}@endif
                        <p> @if(isset($page_data['joinus']->top_main_content)) {{ $page_data['joinus']->top_main_content }}@endif</p>
                    </div>
                    <div class="boxes_wrapper d_flex_j_center">
                        <div class="box">
                            <span class="icon"><img src="{{ $page_data['joinus']->image->image_1 ?? ''}}" class="patient_logo" alt="patient_logo"></span>
                            <h6 class="title"> @if(isset($page_data['joinus']->heading_1)) {{ $page_data['joinus']->heading_1 }}@endif</h6>
                             <p>@if(isset($page_data['joinus']->content_1)) {{ $page_data['joinus']->content_1 }}@endif</p>
                            <a href="{{URL($page_data['joinus']->link_1_url ?? '')}}" class="blue_btn">@if(isset($page_data['joinus']->link_1_txt)) {{ $page_data['joinus']->link_1_txt }}@endif</a>
                        </div>
                        <div class="box">
                            <span class="icon"><img src="{{ $page_data['joinus']->image->image_1_1 ?? ''}}" class="provider_logo" alt="patient_logo"></span>
                            <h6 class="title">@if(isset($page_data['joinus']->heading_1)) {{ $page_data['joinus']->heading_1 }}@endif</h6>
                            <p>@if(isset($page_data['joinus']->content_2)) {{ $page_data['joinus']->content_2 }}@endif</p>
                            <a href="{{URL($page_data['joinus']->link_2_url ?? '')}}" class="blue_btn">@if(isset($page_data['joinus']->link_2_txt)) {{ $page_data['joinus']->link_2_txt }}@endif</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection