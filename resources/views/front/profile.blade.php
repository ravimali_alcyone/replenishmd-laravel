@extends('front.provider_layout.app')

@section('content')
@push('header_scripts')

<style>
#settingModal .modal-content {
    min-width: 600px;
}

span.select2.select2-container.select2-container--default {
    width: 100%!important;
}

.select2-container--default .select2-selection--multiple {
    font-size: 12px;
}

.op_check .form-check input[type=checkbox] {
    pointer-events: none;
}
.white_box_1 .name_icon {
    width: 100%;
    max-width: 377px;
}

.iframe{
    width: 100%;
	height:500px;
   
}

	.pac-container {
        z-index: 10000 !important;
    }
	.pac-container:after {
		background-image: none !important;
		height: 0px;
	}
	
	.progress_pro{
	display: flex;
    height: 5px;
    overflow: hidden;
    line-height: 0;
    font-size: .75rem;
    background-color: #e9ecef;
    border-radius: 0.25rem;	
	}
	.progress_bar{
		display: flex;
		flex-direction: column;
		justify-content: center;
		overflow: hidden;
		color: #fff;
		text-align: center;
		white-space: nowrap;
		background-color: #2c99cb;
		transition: width .6s ease;
	}
</style>	
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="{{ asset('dashboard/css/lightbox.min.css') }}">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places&key={{ env('Google_Map_Keys')}}" async ></script>
	
<script>
	$(function(){
		$("#dob").datepicker({
			dateFormat: "{{ env('DATE_FORMAT_JQ') }}",
			maxDate: -1,
			changeMonth: true,
			changeYear: true,
			yearRange: "-150:+0",
		});
		
	});
	
</script>
@endpush
	<div class="col-xl-10 col-lg-9 col-md-9 col-sm-12">
		<div class="row">
			<div class="col-xl-9 col-lg-12 col-md-12 col-sm-12">
				<div class="main-center-data">
					<h3 class="display-username">Profile Setup</h3>
					<div class="row">
						<div class="col-xl-4 col-lg-4 col-md-12 col-sm-12">
							<div class="prf_box hover-effect-box">
								<div class="prf_img">
									<div class="img_box" id="img_box">
									@if(auth()->user()->image)			  
									<img id="profileData" src="{{env('APP_URL')}}/{{auth()->user()->image}}" alt="{{auth()->user()->name}}"/> 
									@else
										<img id="profileData" src="{{env('APP_URL')}}/dist/images/user_icon.png" alt="{{auth()->user()->name}}"/> 
									@endif
									</div>
									<form id="changeProfileForm">
										<div class="image-upload">
											<label for="file_input">
												<i class="fa fa-camera" aria-hidden="true"></i>
											</label>
											<input id="file_input" name="file_input" type="file" onchange="changeProfile(event)"/>
										</div>
									</form>
								</div>
								<!--<div class="three_dot">
									<img src="{{asset('dashboard/img/dot.svg')}}">
								</div>-->
								<div class="name_user">
									<h5>{{auth()->user()->name}}</h5>
								</div>
								<!--<span>Lisa conors</span>-->
								<div class="badge_box">
									<div class="nursing_icon">
										<img src="{{asset('dashboard/img/medi.svg')}}">

									</div>
								@if($provider_categories)
									<div class="label_nurse">
									@foreach($provider_categories as $key => $value)
										<div class="blue_box">
											<span>{{ $value }}</span>
										</div>
									@endforeach
									</div>
								@endif

								</div>
								<a href="#">Message</a>
							</div>

							<div class="prf_setup hover-effect-box">
								<div class="setup_details">
									<h5>Complete your profile setup</h5>
									<p>&nbsp;</p>

								</div>
								<?php
									$name = auth()->user()->name;
									$summary = auth()->user()->short_info;
									$gender = auth()->user()->gender;
									$dob = auth()->user()->dob;
									$address = auth()->user()->address;
									$provider_licenses = auth()->user()->provider_licenses;
									$per = 0;
									if($name !="" && $gender !="" && $dob !="0000-00-00") { $per = $per+25; $chk1 ="checked"; }else{ $chk1=""; }
									if($summary !="" ) { $per = $per+25; $chk2 ="checked"; }else{ $chk2=""; }
									if($address !="" ) { $per = $per+25; $chk3 ="checked"; }else{ $chk3=""; }
									if($provider_licenses !="" ) { $per = $per+25; $chk4 ="checked"; }else{ $chk4=""; }
									
								?>
								<div class="progress_pro" >
									<div class="progress_bar" role="progressbar" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100" data-id="<?php echo $per;?>" style="width:<?php echo $per;?>%;">
									</div>
								</div>
								<span><span class="progress_count"><?php echo $per;?></span>% Completed</span>

								<div class="op_check">
									<div class="form-check">
										<input type="checkbox" class="form-check-input" id="exampleCheck1" <?php echo $chk2;?>>
										<label class="form-check-label" for="exampleCheck1">Summary</label>
									</div>
									<div class="form-check">
										<input type="checkbox" class="form-check-input" id="exampleCheck2" <?php echo $chk1;?>>
										<label class="form-check-label" for="exampleCheck2">General Information</label>
									</div>
									<div class="form-check">
										<input type="checkbox" class="form-check-input" id="exampleCheck3" <?php echo $chk3;?>>
										<label class="form-check-label" for="exampleCheck3">Contact Information</label>
									</div>
									<div class="form-check">
										<input type="checkbox" class="form-check-input" id="exampleCheck4" <?php echo $chk4;?>>
										<label class="form-check-label" for="exampleCheck4">Licenses</label>
									</div>

								</div>
								<!--<a href="#">Show More</a>-->
							</div>
						</div>
						<div class="col-xl-8 col-lg-8 col-md-12 col-sm-12">
							<ul class="nav nav-tabs">
								<li class="nav-item">
									<a class="nav-link active" data-toggle="tab" href="#home">Profile</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" data-toggle="tab" href="#menu1">Credentials</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" data-toggle="tab" href="#menu2">Business</a>
								</li>
							</ul>
							<div class="tab-content">
								<div class="tab-pane container active" id="home">
									<div class="summary_box hover-effect-box">
										<h5>Summary</h5>
										<div class="summary_text">{!!(auth()->user()->short_info != null) ? auth()->user()->short_info : "Add your profile summary..."!!}</div>
										<div class="edit_op">
											<a href="javascript:void(0)" data-toggle="modal" data-target="#profileSummaryModal">Edit</a>
										</div>
									</div>

									<div class="summ_box hover-effect-box">
										<h5>General Information</h5>
										<div class="user_personal user_personal_text">
											<div class="white_box">
												<i class="fa fa-calendar" aria-hidden="true"></i>
												<div class="name_icon">
													<p class="user_dob"><?php if(auth()->user()->dob != '0000-00-00'){ echo date(env('DATE_FORMAT_PHP'),strtotime(auth()->user()->dob )); }else{ echo 'mm/dd/yyyy'; } ?></p>
													<span>Date of birth</span>
												</div>
											</div>
											<div class="white_box">
												<i class="fa fa-venus"></i>
												<div class="name_icon">
													<p class="user_gender">{{ucfirst(auth()->user()->gender)}}</p>
													<span>Gender</span>
												</div>
											</div>
											<div class="white_box">
												<i class="fa fa-user"></i>
												<div class="name_icon">
													<p class="user_race">@if($provider_race && $provider_race->count() > 0) {{ $provider_race->name }} @endif </p>
													<span>Race</span>
												</div>
											</div>
										</div>
										<div class="edit_op">
											<a href="javascript:void(0)" data-toggle="modal" data-target="#genralInfoModal">Edit</a>
										</div>
									</div>
									<div class="summ_box hover-effect-box">
										<h5>Contact</h5>
										<div class="user_personal">
											<div class="white_box white_box_flex">
												<i class="fa fa-phone" aria-hidden="true"></i>
												<div class="name_icon">
													<p class="user_phone">{{auth()->user()->phone}}</p>
													<span>Phone no</span>
												</div>
											</div>
											<div class="white_box white_box_flex_1">
												<i class="fa fa-envelope"></i>
												<div class="name_icon">
													<p class="user_email">{{auth()->user()->email}}</p>
													<span>Email</span>
												</div>
											</div>
											
										</div>
										<div class="white_box_1">
												<i class="fa fa-map-marker" aria-hidden="true"></i>
												<div class="name_icon">
													<p class="user_address">{{auth()->user()->address}}</p>
													<span>Address</span>
												</div>
											</div>
										<div class="edit_op">
											<a href="javascript:void(0)" data-toggle="modal" data-target="#contactInfoModal">Edit</a>
										</div>
									</div>
									<div class="summ_box hover-effect-box">
										<h5>What they treat?</h5>
										<div class="white_box_1">
												<i class="fa fa-stethoscope" aria-hidden="true"></i>
												<div class="name_icon services_text">
											@if($services)	
												@foreach($services as $val)										
													@if($provider_services && in_array($val->id, $provider_services))<p>{{ $val->name }}</p> @endif
												@endforeach
											@endif
												</div>
											</div>
										<div class="edit_op">
											<a href="javascript:void(0)" data-toggle="modal" data-target="#treatCatModal">Edit</a>
										</div>
									</div>
								</div>
								<div class="tab-pane container fade" id="menu1">
									<div class="summary_box hover-effect-box">
										<h5>License</h5>
										<div class="row" id="certificate_licence">
										<?php
										if(auth()->user()->provider_licenses != null){
											$img = json_decode(auth()->user()->provider_licenses);
											$len =  count($img);
											for($i=0; $i<$len; $i++)
											{
												if(preg_match("/\.(pdf)$/", $img[$i])){
													?>
													<div class="col-md-4">
														<div class="demo_pic">
															<a class="credentialsPdfModal" href="javascript:void(0)" data-toggle="modal" data-url="{{env('APP_URL')}}/{{$img[$i]}}" data-target="#credentialsPdfModal">
																<img src="{{env('APP_URL')}}/images/pdf-icon.png">
															</a>
														</div>
													</div>								
												<?php
												}else{ 
											
										?>
											<div class="col-md-4">
												<div class="demo_pic">
													<a href="/{{$img[$i]}}" data-lightbox="lightbox-set-img">
														<img src="/{{$img[$i]}}">
													</a>
												</div>
											</div>								
										<?php 
										}
										
										} }?>
										</div>
										<form id="moreCertificateForm">
											<div class="image-upload-cer">
												<label for="file_inputs">
													<span class="certificate">Add more</span>
												</label>
												<input id="file_inputs" name="file_inputs[]" type="file" multiple onchange="addCertificate(event)"/>
											</div>
										</form>
										
									</div>
								</div>
								<div class="tab-pane container fade" id="menu2">
									<div class="summary_box hover-effect-box">
										<h5>License</h5>
										<div class="row">
										<?php
										if(auth()->user()->provider_licenses != null){
											$img = json_decode(auth()->user()->provider_licenses);
											$len =  count($img);
											for($i=0; $i<$len; $i++)
											{
										?>
											<div class="col-md-4">
												<div class="demo_pic">
													<a href="{{$img[$i]}}" data-lightbox="lightbox-set-test">
														<img src="{{env('APP_URL')}}/{{$img[$i]}}">
													</a>
												</div>
											</div>								
										<?php } }?>
										</div>
									</div>
								</div>
								
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-xl-3 col-lg-12 col-md-12 col-sm-12">
				<div class="informative-block bg-white round-crn pd-20-30 settings hover-effect-box">
					<h4 class="font-weight-bold">Settings</h4>
					<div class="stwich-toggle">
						<div class="custom-control custom-switch">
						  <input type="checkbox" class="custom-control-input" id="customSwitch1">
						  <label class="custom-control-label" for="customSwitch1"></label>
						</div>
					</div>
					<h6 class="label-mute mt-15">SMS Notifications</h6>
					<p class="note m-0">By turning on this toggle, I agree to receive texts from RMD and/or {{auth()->user()->name}} to {{auth()->user()->phone}} that might be considered marketing and that may be sent using an auto dialer. I understand that agreeing is not required to purchase.</p>
				</div>
			</div>	
		</div>	
	</div>	
	
	 <!-- Modal -->
	<div id="passModal" class="modal" tabindex="-1" role="dialog">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<h5 class="modal-title">Change Password</h5>
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			  <span aria-hidden="true">&times;</span>
			</button>
		  </div>
		  <form id="changePassForm">
		  <div class="modal-body">

			  <div class="form-group has-feedback">
				<label for="old_password">Old Password</label>
				<input type="password" name="old_password" class="form-control" id="old_password" placeholder="Old Password">
				<span class="text-danger">
					<strong class="error" id="old_password-error"></strong>
				</span>				
			  </div>
			  
			  <div class="form-group has-feedback">
				<label for="new_password">New Password</label>
				<input type="password" name="password" class="form-control" id="new_password" placeholder="New Password">
				<span class="text-danger">
					<strong class="error" id="new_password-error"></strong>
				</span>					
			  </div>

			  <div class="form-group has-feedback">
				<label for="confirm_password">Confirm New Password</label>
				<input type="password" name="password_confirmation" class="form-control" id="confirm_password" placeholder="Confirm New Password">
				<span class="text-danger">
					<strong class="error" id="confirm_password-error"></strong>
				</span>					
			  </div>		  
			
		  </div>
		  <div class="modal-footer">
			<button type="submit" id="changePass_saveBtn" class="accept_btn btn btn-outline-primary btn-sm" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Processing" >Submit</button>
			<button type="button" class="btn btn btn-outline-danger btn-sm" data-dismiss="modal">Cancel</button>
		  </div>
		  </form>
		</div>
	  </div>
	</div>

	
	<!---Edit profile summary-->
	<div id="profileSummaryModal" class="modal" tabindex="-1" role="dialog">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<h5 class="modal-title">Profile Summary</h5>
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			  <span aria-hidden="true">&times;</span>
			</button>
		  </div>
		  <form id="profileSummaryForm">
		  <div class="modal-body">
		  									
			  <div class="form-group has-feedback">
				<textarea name="profile_summary" id="profile_summary" class="input w-full border mt-2" cols="30" rows="4"><?php echo auth()->user()->short_info;?></textarea>
				<span class="text-danger">
					<strong class="error" id="profile_summary-error"></strong>
				</span>				
			  </div>
		  </div>
		  <div class="modal-footer">
			<button type="submit" id="profileSummary_saveBtn" class="accept_btn btn btn-outline-primary btn-sm" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Processing" >Submit</button>
			<button type="button" class="btn btn btn-outline-danger btn-sm" data-dismiss="modal">Cancel</button>
		  </div>
		  </form>
		</div>
	  </div>
	</div>
	<!--End--->

	<!---Edit general info-->
	<div id="genralInfoModal" class="modal" tabindex="-1" role="dialog">
	  <div class="modal-dialog" role="document">
		<div class="modal-content" style="min-width: 600px;">
		  <div class="modal-header">
			<h5 class="modal-title">General Information</h5>
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			  <span aria-hidden="true">&times;</span>
			</button>
		  </div>
		  <form id="generalInfoForm">
		  <div class="modal-body">
		  	<div class="form-row">
				<div class="form-group col-md-6 has-feedback">
					<label for="first_name">First Name <span class="text-danger">*</span></label>
					<input type="text" name="first_name" class="form-control" id="first_name" value="{{auth()->user()->first_name}}" required>
					<span class="text-danger">
						<strong class="error" id="first_name-error"></strong>
					</span>				
			  	</div>
			  	<div class="form-group col-md-6 has-feedback">
					<label for="last_name">Last Name <span class="text-danger">*</span></label>
					<input type="text" name="last_name" class="form-control" id="last_name" value="{{auth()->user()->last_name}}" required>
					<span class="text-danger">
						<strong class="error" id="last_name-error"></strong>
					</span>					
			  	</div>
			</div>
			<div class="form-row">
				<div class="form-group col-md-6 has-feedback">
					<label for="dob">DOB <span class="text-danger">*</span></label>
					<input type="text" name="dob" class="form-control" id="dob" value="<?php if(auth()->user()->dob != '0000-00-00'){ echo date(env('DATE_FORMAT_PHP'),strtotime(auth()->user()->dob )); } ?>" required placeholder="mm/dd/yyyy">
					<span class="text-danger">
						<strong class="error" id="dob-error"></strong>
					</span>					
				</div>	
				<div class="form-group col-md-6 has-feedback">
					<label for="dob">Race <span class="text-danger">*</span></label>
					<select id="provider_race" name="race" class="custom-select select2" required>
				@if($races)	
					@foreach($races as $val)
						<option value="{{$val->id}}" @if(auth()->user()->race && $val->id == auth()->user()->race) selected @endif >{{$val->name}}</option>
					@endforeach
				@endif
					</select>
				</div>				
			</div>
			
			<div class="form-row">
				<div class="form-group col-md-6 has-feedback">
					<label for="gender">Gender <span class="text-danger">*</span></label>
					<div class="form-check-inline">
					  <label class="form-check-label">
						<input type="radio" class="form-check-input" name="gender" value="male" @if(auth()->user()->gender == 'male' && auth()->user()->gender !='') checked @else checked @endif >Male</label>
					</div>
					<div class="form-check-inline">
					  <label class="form-check-label">
						<input type="radio" class="form-check-input" name="gender" value="female" @if(auth()->user()->gender == 'female') checked @endif >Female
					  </label>
					</div>
					
					<span class="text-danger">
						<strong class="error" id="gender-error"></strong>
					</span>					
				</div>				
			</div>
			
			<div class="form-row">
				<div class="form-group col-md-6 has-feedback">
					<label for="first_name">Education </label>
					<input type="text" name="education" class="form-control" id="education" value="{{auth()->user()->education}}" required>
					<span class="text-danger">
						<strong class="error" id="education-error"></strong>
					</span>				
			  	</div>
			  	<div class="form-group col-md-6 has-feedback">
					<label for="last_name">Goal </label>
					<input type="text" name="goal" class="form-control" id="goal" value="{{auth()->user()->goal}}" required>
					<span class="text-danger">
						<strong class="error" id="goal-error"></strong>
					</span>					
			  	</div>
			</div>
			
			<div class="form-row">
				<div class="form-group col-md-6 has-feedback">
					<label for="first_name">Interest </label>
					<input type="text" name="interest" class="form-control" id="interest" value="{{auth()->user()->interest}}" required>
					<span class="text-danger">
						<strong class="error" id="interest-error"></strong>
					</span>				
			  	</div>
			  	<div class="form-group col-md-6 has-feedback">
					<label for="last_name">Relationship Status </label>
					<input type="text" name="rel_status" class="form-control" id="rel_status" value="{{auth()->user()->relation_status}}" required>
					<span class="text-danger">
						<strong class="error" id="rel_status-error"></strong>
					</span>					
			  	</div>
			</div>
			  
		  </div>
		  <div class="modal-footer">
			<button type="submit" id="generalInfo_saveBtn" class="accept_btn btn btn-outline-primary btn-sm" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Processing" >Submit</button>
			<button type="button" class="btn btn btn-outline-danger btn-sm" data-dismiss="modal">Cancel</button>
		  </div>
		  </form>
		</div>
	  </div>
	</div>
	<!--End--->
	
	<!--Edit contact info--->
	<div id="contactInfoModal" class="modal" tabindex="-1" role="dialog">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<h5 class="modal-title">Contact Information</h5>
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			  <span aria-hidden="true">&times;</span>
			</button>
		  </div>
		    <form id="contactForm">
		  		<div class="modal-body">
					<div class="form-group has-feedback">
						<label for="phone_num">Phone <span class="text-danger">*</span></label>
						<input type="text" name="phone_num" class="form-control" id="phone_num" value="{{auth()->user()->phone}}" required>
						<span class="text-danger">
							<strong class="error" id="phone_num-error"></strong>
						</span>				
					</div>
					<div class="form-group has-feedback">
						<label for="email_id">Email <span class="text-danger">*</span></label>
						<input type="email" name="email_id" class="form-control" id="email_id" value="{{auth()->user()->email}}" required>
						<span class="text-danger">
							<strong class="error" id="email_id-error"></strong>
						</span>				
					</div>
					<div class="form-group has-feedback">
						<label for="address">Address</label>
						<!--<textarea name="address" class="form-control" id="address" cols="" rows="3">{{auth()->user()->address}}</textarea>-->
						<input type="text" name="address" class="form-control address_textt" id="address" value="{{auth()->user()->address}}">
						<span class="text-danger">
							<strong class="error" id="address-error"></strong>
						</span>
					</div>
		  		</div>
		  		<div class="modal-footer">
					<button type="submit" id="contact_saveBtn" class="accept_btn btn btn-outline-primary btn-sm" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Processing" >Submit</button>
					<button type="button" class="btn btn btn-outline-danger btn-sm" data-dismiss="modal">Cancel</button>
				</div>
		  	</form>
		</div>
	  </div>
	</div>
	<!---End contact info-->
	
	<!--Edit Setting-->
	<div id="settingModal" class="modal" tabindex="-1" role="dialog">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<h5 class="modal-title">Setting</h5>
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			  <span aria-hidden="true">&times;</span>
			</button>
		  </div>
		  <form id="settingForm">
		  <div class="modal-body">
	@if($office_start_end_time)	
		@php $i = 0; @endphp
		@foreach($office_start_end_time as $val)		  
			<div class="form-row item_wrapper">
			  <div class="form-group col-md-5 has-feedback">
				@if($i == 0)<label for="first_name">Office starting time <span class="text-danger">*</span></label>@endif
					<div class='input-group date mt-2'>
						<input type='text' class="form-control timepicker" name="office_start_time[]" value="{{ $val->st_time }}" />
						<span class="input-group-text" id="basic-addon1">
							<img src="{{asset('assets/images/clock.svg')}}">
						</span>
					</div>		
				<span class="text-danger">
					<strong class="error" id="dob-error"></strong>
				</span>	
			  </div>
			  <div class="form-group col-md-5 has-feedback">
				@if($i == 0)<label for="last_name">Office closing time <span class="text-danger">*</span></label>@endif
					<div class='input-group date mt-2'>
						<input type='text' class="form-control timepicker" name="office_close_time[]" value="{{ $val->en_time }}"/>
						<span class="input-group-text" id="basic-addon1">
							<img src="{{asset('assets/images/clock.svg')}}">
						</span>
					</div>
				<span class="text-danger">
					<strong class="error" id="dob-error"></strong>
				</span>	
			  </div>
			<div class="form-group col-md-1 offset-md-1 has-feedback @if($i == 0) mt-1 @else mt-3 @endif">
				@if($i == 0) <a href="javascript:void(0)" title="Add more" class="add_item"><span style="color:green;"><i class="fa fa-plus-circle fa-2x" aria-hidden="true"></i></span></a>
				@else
				<a href="javascript:void(0)" title="Remove" class="remove_item"><span style="color:red;"><i class="fa fa-minus" aria-hidden="true"></i></span></a>					
				@endif
			
			  </div>
			</div>
			@php $i++; @endphp
			@endforeach
		@else
			<div class="form-row">
			  <div class="form-group col-md-5 has-feedback">
				<label for="first_name">Office starting time <span class="text-danger">*</span></label>
					<div class='input-group date mt-2'>
						<input type='text' class="form-control timepicker" name="office_start_time[]" value="{{ date('h:i A') }}" />
						<span class="input-group-text" id="basic-addon1">
							<img src="{{asset('assets/images/clock.svg')}}">
						</span>
					</div>				
				<span class="text-danger">
					<strong class="error" id="dob-error"></strong>
				</span>	
			  </div>
			  <div class="form-group col-md-5 has-feedback">
				<label for="last_name">Office closing time <span class="text-danger">*</span></label>				
					<div class='input-group date mt-2'>
						<input type='text' class="form-control timepicker" name="office_close_time[]" value="{{ date('h:i A') }}"/>
						<span class="input-group-text" id="basic-addon1">
							<img src="{{asset('assets/images/clock.svg')}}">
						</span>
					</div>
				<span class="text-danger">
					<strong class="error" id="dob-error"></strong>
				</span>	
			  </div>
			  <div class="form-group col-md-1 offset-md-1 has-feedback mt-1">
					<a href="javascript:void(0)" title="Add more" class="add_item"><span style="color:green;"><i class="fa fa-plus-circle fa-2x" aria-hidden="true"></i></span></a>
				</div>
			</div>
		@endif
		
		
			<div class="more_items_wrapper"></div>
			
			<hr>
			
	@if($break_start_end_time)	
		@php $j = 0; @endphp
		@foreach($break_start_end_time as $val)				
			<div class="form-row break_wrapper">
				<div class="form-group col-md-5 has-feedback">
					@if($j == 0)<label for="gender">Break start time</label>@endif
					<div class='input-group date mt-2'>
					<input type='text' class="form-control timepicker" name="break_start_time[]" value="{{ $val->breakSt_time }}"/>
					<span class="input-group-text" id="basic-addon1">
						<img src="{{asset('assets/images/clock.svg')}}">
					</span>
				</div>
				<span class="text-danger">
					<strong class="error" id="dob-error"></strong>
				</span>
				</div>
				<div class="form-group col-md-5 has-feedback">
					@if($j == 0)<label for="email">Break end time</label>@endif
					<div class='input-group date mt-2'>
					<input type='text' class="form-control timepicker" name="break_end_time[]" value="{{ $val->breakEn_time }}"/>
					<span class="input-group-text" id="basic-addon1">
						<img src="{{asset('assets/images/clock.svg')}}">
					</span>
				</div>
				<span class="text-danger">
					<strong class="error" id="dob-error"></strong>
				</span>	
				</div>
				<div class="form-group col-md-1 offset-md-1 has-feedback @if($j == 0) mt-1 @else mt-3 @endif">
					@if($j == 0)<a href="javascript:void(0)" title="Add more" class="add_break"><span style="color:green;"><i class="fa fa-plus-circle fa-2x" aria-hidden="true"></i></span></a>
					@else
					<a href="javascript:void(0)" title="Remove" class="remove_break"><span style="color:red;"><i class="fa fa-minus" aria-hidden="true"></i></span></a>
					@endif
				</div>			  
			</div>
			@php $j++; @endphp
		@endforeach
	@else
			<div class="form-row">
				<div class="form-group col-md-5 has-feedback">
					<label for="gender">Break start time</label>
					<div class='input-group date mt-2'>
					<input type='text' class="form-control timepicker" name="break_start_time[]" value="{{ date('h:i A') }}"/>
					<span class="input-group-text" id="basic-addon1">
						<img src="{{asset('assets/images/clock.svg')}}">
					</span>
				</div>
				<span class="text-danger">
					<strong class="error" id="dob-error"></strong>
				</span>
				</div>
				<div class="form-group col-md-5 has-feedback">
					<label for="gender">Break end time</label>
					<div class='input-group date mt-2'>
					<input type='text' class="form-control timepicker" name="break_end_time[]" value="{{ date('h:i A') }}"/>
					<span class="input-group-text" id="basic-addon1">
						<img src="{{asset('assets/images/clock.svg')}}">
					</span>
				</div>
				<span class="text-danger">
					<strong class="error" id="dob-error"></strong>
				</span>	
				</div>
				<div class="form-group col-md-1 offset-md-1 has-feedback mt-1">
					<a href="javascript:void(0)" title="Add more" class="add_break"><span style="color:green;"><i class="fa fa-plus-circle fa-2x" aria-hidden="true"></i></span></a>
				</div>			  
			</div>		
	@endif
	
			<div class="more_break_wrapper"></div>
			
		  </div>
		  <div class="modal-footer">
			<button type="submit" id="setting_saveBtn" class="accept_btn btn btn-outline-primary btn-sm" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Processing">Add</button>
			<button type="button" class="btn btn-outline-danger btn-sm" data-dismiss="modal">Cancel</button>
		  </div>
		  </form>
		</div>
	  </div>
	</div>
	<!--End here-->
	<!--Treatment Category Modal Start-->
	<div id="treatCatModal" class="modal" tabindex="-1" role="dialog">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<h5 class="modal-title">What they treat?</h5>
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			  <span aria-hidden="true">&times;</span>
			</button>
		  </div>
		  <form id="treatCatForm">
		  <div class="modal-body">			
			  <div class="form-group has-feedback">
			  	<select id="treat_cat" name="treat_cat[]" class="custom-select select2" multiple required>
					<option value="">Select</option>
			@if($services)	
				@foreach($services as $val)
					<option value="{{$val->id}}" @if($provider_services && in_array($val->id, $provider_services)) selected @endif >{{$val->name}}</option>
				@endforeach
			@endif
				</select>
				<span class="text-danger">
					<strong class="error" id="profile_summary-error"></strong>
				</span>				
			  </div>
		  </div>
		  <div class="modal-footer">
			<button type="submit" id="treatCat_saveBtn" class="accept_btn btn btn-outline-primary btn-sm" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Processing" >Submit</button>
			<button type="button" class="btn btn btn-outline-danger btn-sm" data-dismiss="modal">Cancel</button>
		  </div>
		  </form>
		</div>
	  </div>
	</div>								
	<!---End here--->
	
	<!---pdf viewer---->
	<div id="credentialsPdfModal" class="modal" tabindex="-1" role="dialog">
	  <div class="modal-dialog" role="document">
		<div class="modal-content" style="width: 700px;">
		  <div class="modal-header">
			<h5 class="modal-title">PDF Viewer</h5>
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			  <span aria-hidden="true">&times;</span>
			</button>
		  </div>

		  <div class="modal-body">
			<canvas id="the-canvas"></canvas>
		  </div>
		  
		  <div class="modal-footer">
		       <div style="text-align:center;">
				  <button id="prev">Previous</button>
				  <button id="next">Next</button>
				  &nbsp; &nbsp;
				  <span>Page: <span id="page_num"></span> / <span id="page_count"></span></span>
			   </div>
		  </div>
		 
		</div>
	  </div>
	</div>
	<!--End--->

	<!--disabledTimeIntervals: [[moment({ h: 0 }), moment({ h: 8 })], [moment({ h: 18 }), moment({ h: 24 })]]-->
	<script src="{{ asset('dashboard/js/lightbox.min.js') }}"></script>
	<script>
	
	    $(document).ready(function() {
			var serchinput = "address";
			var autocomplete;
			
			autocomplete = new google.maps.places.Autocomplete((document.getElementById(serchinput)), {
				types : ['geocode'],
				//componentRestrictions : {
				//	country: "USA"
				//}
			});
			
			google.maps.event.addListener(autocomplete, 'places_changed', function (){
				var near_place = autocomplete.getPlace();
			});
			
		});
	
		CKEDITOR.replace('profile_summary');
		CKEDITOR.add;
		
		$("#treat_cat").select2({
			 minimumResultsForSearch: -1
		});
		
		$(function () {
			 $('.timepicker').datetimepicker({
				format : 'LT'
			 });
		 });
		 
		function pass_modal(){
			$("#passModal").modal('show');
			$("#changePassForm").trigger('reset');
			$("#changePassForm span.text-danger .error").html('');
		}

		$("#changePassForm").submit(function(e) {
			e.preventDefault();
			
			//loader start
			$('#changePass_saveBtn').html($('#changePass_saveBtn').attr('data-loading-text'));
			
			$("#changePassForm span.text-danger .error").html('');
		
			$.ajax({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				data: $('#changePassForm').serialize(),
				url: "{{ route('provider.change_password') }}",
				type: "POST",
				// dataType: 'json',
				success: function (response) {
					$('#changePass_saveBtn').html('Submit');
					
					if(response['status'] == 'success'){
						//$("#passModal").modal('hide');
						//successAlert(response['message'],2000,'top-right');
						callProgressBar();
						setTimeout(function(){
							//$('#changePassForm').trigger("reset");
							location.reload();
						}, 2000);
					}
				},
				error: function (data) {
					$('#changePass_saveBtn').html('Submit');
					
					if(data.responseJSON.errors) {
						let errors = data.responseJSON.errors;
						if(errors.old_password){
							$( '#old_password-error' ).html( errors.old_password[0] );
						}
						
						if(errors.password){
							$( '#new_password-error' ).html( errors.password[0] );
						}
						
						if(errors.password_confirmation){
							$( '#confirm_password-error' ).html( errors.password_confirmation[0] );
						}
						
					}
				
				}
			});
		});
		
		function setting_modal(){
			$("#settingModal").modal('show');
			$("#settingForm span.text-danger .error").html('');
		}
		
		var currenttime = "{{ date('h:i A') }}";
		/****Add more office time and break time******/
		$('.add_item').click(function() {
			$('.more_items_wrapper').append(
				'<div class="form-row item_wrapper">'+
				  '<div class="form-group col-md-5 has-feedback">'+
					'<div class="input-group date mt-2">'+
						'<input type="text" class="form-control timepicker" name="office_start_time[]" value='+"{{ date('h:i A') }}"+'/>'+
						'<span class="input-group-text" id="basic-addon1">'+
							'<img src="{{asset("assets/images/clock.svg")}}">'+
						'</span>'+
					'</div>'+				
				  '</div>'+
				  '<div class="form-group col-md-5 has-feedback">'+
					'<div class="input-group date mt-2">'+
						'<input type="text" class="form-control timepicker" name="office_close_time[]" value='+"{{ date('h:i A') }}"+'/>'+
						'<span class="input-group-text" id="basic-addon1">'+
							'<img src="{{asset("assets/images/clock.svg")}}">'+
						'</span>'+
					'</div>'+					
				  '</div>'+
				  '<div class="form-group col-md-1 offset-md-1 has-feedback mt-3">'+
					'<a href="javascript:void(0)" title="Remove" class="remove_item"><span style="color:red;"><i class="fa fa-minus" aria-hidden="true"></i></span></a>'+
					'</div>'+
				'</div>'
			);
			$('.timepicker').datetimepicker({ format : 'LT' });
		});

		$(document).on('click', '.remove_item', function() {
			$(this).closest('.item_wrapper').remove();
		});
		
		
		$('.add_break').click(function() {
			$('.more_break_wrapper').append(
				'<div class="form-row break_wrapper">'+
					'<div class="form-group col-md-5 has-feedback">'+
						'<div class="input-group date mt-2">'+
						'<input type="text" class="form-control timepicker" name="break_start_time[]" value='+"{{ date('h:i A') }}"+'/>'+
						'<span class="input-group-text" id="basic-addon1">'+
							'<img src="{{asset("assets/images/clock.svg")}}">'+
						'</span>'+
					'</div>'+
					'</div>'+
					'<div class="form-group col-md-5 has-feedback">'+
						'<div class="input-group date mt-2">'+
						'<input type="text" class="form-control timepicker" name="break_end_time[]" value='+"{{ date('h:i A') }}"+'/>'+
						'<span class="input-group-text" id="basic-addon1">'+
							'<img src="{{asset("assets/images/clock.svg")}}">'+
						'</span>'+
					'</div>'+				
					'</div>'+
					'<div class="form-group col-md-1 offset-md-1 has-feedback mt-3">'+
						'<a href="javascript:void(0)" title="Remove" class="remove_break"><span style="color:red;"><i class="fa fa-minus" aria-hidden="true"></i></span></a>'+
					'</div>'+			  
				'</div>'
			);
			$('.timepicker').datetimepicker({ format : 'LT' });
		});

		$(document).on('click', '.remove_break', function() {
			$(this).closest('.break_wrapper').remove();
		});
		/**********End here**********/
		
		$("#settingForm").submit(function(e) {
			e.preventDefault();
			$('#setting_saveBtn').html($('#setting_saveBtn').attr('data-loading-text'));
			$("#profileForm span.text-danger .error").html('');
			$.ajax({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				data: $('#settingForm').serialize(),
				url: "{{ route('provider.update_setting') }}",
				type: "POST",
				// dataType: 'json',
				success: function (response) {
					$('#setting_saveBtn').html('Update');
					
					if(response['status'] == 'success'){
						//$("#settingModal").modal('hide');
						//successAlert(response['message'],2000,'top-right');
						callProgressBar();
						setTimeout(function(){
							location.reload();
						}, 2000);
					}
				},
				error: function (data) {
					$('#setting_saveBtn').html('Update');
					if(data.responseJSON.errors) {
						let errors = data.responseJSON.errors;
						if(errors.first_name){ $('#first_name-error').html(errors.first_name[0]); }
						if(errors.last_name){ $('#last_name-error').html(errors.last_name[0]); }
						if(errors.phone){ $('#phone-error').html(errors.phone[0]); }
						if(errors.dob){ $('#dob-error').html(errors.dob[0]); }						
					}
				
				}
			});
		});

		/****Add profile summary****/
		$("#profileSummaryForm").submit(function(e) {
			e.preventDefault();
			$('#profileSummary_saveBtn').html($('#profileSummary_saveBtn').attr('data-loading-text'));
			$("#profileSummaryForm span.text-danger .error").html('');

			var profile_summary = CKEDITOR.instances.profile_summary.getData();
			var formData = new FormData(this);
			formData.append('profile_summary', profile_summary);
		
			$.ajax({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				data: formData,
				url: "{{ route('provider.update_summary') }}",
				type: "POST",
				cache:false,
				contentType: false,
				processData: false,
				success: function (response) {
					$('#profileSummary_saveBtn').html('Submit');
					
					if(response['status'] == 'success'){
						
						$("#profileSummaryModal").modal('hide');
						//successAlert(response['message'],2000,'top-right');
						callProgressBar();
						var updatedata = response['user_data'];
						$('.summary_text').html(updatedata['short_info']);
						/*setTimeout(function(){
							location.reload();
						}, 2000);*/
						if($("#exampleCheck1").is(':checked')==false){
							$("#exampleCheck1").prop("checked", true);
							var p_w = parseInt($(".progress_bar").attr('data-id'));
								p_w = p_w+25;
							$(".progress_bar").attr('data-id',p_w)
							$(".progress_bar").width(p_w+'%');
							$(".progress_count").text(p_w);
						}
					}
				},
				error: function (data) {
					$('#profileSummary_saveBtn').html('Submit');
					if(data.responseJSON.errors) {
						if(data.responseJSON.errors) {
							let errors = data.responseJSON.errors;
							if(errors.profile_summary){ $('#profile_summary-error').html(errors.profile_summary[0]); }
						}
					}
				
				}
			});
		});

		/****Edit General Info*****/
		$("#generalInfoForm").submit(function(e) {
			e.preventDefault();
			
 			$('#generalInfo_saveBtn').html($('#generalInfo_saveBtn').attr('data-loading-text'));
			
			$("#generalInfoForm span.text-danger .error").html('');
		
			$.ajax({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				data: $('#generalInfoForm').serialize(),
				url: "{{ route('provider.update_profile') }}",
				type: "POST",
				// dataType: 'json',
				success: function (response) {
					$('#generalInfo_saveBtn').html('Update');
					
					if(response['status'] == 'success'){
						
						$("#genralInfoModal").modal('hide');						
						//successAlert(response['message'],2000,'top-right');
						callProgressBar();
						var updatedata = response['user_data'];
					    $('.user_personal_text').find('.user_dob').html(updatedata['dob']);
					    $('.user_personal_text').find('.user_gender').html(updatedata['gender']);
					    $('.user_personal_text').find('.user_race').html(updatedata['race']);
					    $('.pdh_user_name').text(updatedata['name']);
					    $('.name_user').find('h5').text(updatedata['name']);
						
						/* setTimeout(function(){
							location.reload();
						}, 2000); */
						
						if($("#exampleCheck2").is(':checked')==false){
							if(updatedata['gender'] !='' && updatedata['dob'] !='' && updatedata['name'] !=''){
								$("#exampleCheck2").prop("checked", true);
								var p_w = parseInt($(".progress_bar").attr('data-id'));
									p_w = p_w+25;
								$(".progress_bar").attr('data-id',p_w)
								$(".progress_bar").width(p_w+'%');
								$(".progress_count").text(p_w);
							}
						}
					}
				},
				error: function (data) {
					$('#generalInfo_saveBtn').html('Update');
					if(data.responseJSON.errors) {
						let errors = data.responseJSON.errors;
						if(errors.first_name){ $('#first_name-error').html(errors.first_name[0]); }
						if(errors.last_name){ $('#last_name-error').html(errors.last_name[0]); }
						if(errors.phone){ $('#gender-error').html(errors.gender[0]); }
						if(errors.dob){ $('#dob-error').html(errors.dob[0]); }						
					}
				
				}
			});
		});

		$("#contactForm").submit(function(e) {
			e.preventDefault();
			var phone =$('#phone_num').val();
			if(phone.match(/^(\+{0,})(\d{0,})([(]{1}\d{1,3}[)]{0,}){0,}(\s?\d+|\+\d{2,3}\s{1}\d+|\d+){1}[\s|-]?\d+([\s|-]?\d+){1,2}(\s){0,}$/gm)){
			
			}else{
				
				$('#phone_num-error').html('Please specify a valid phone number.');
				return false;
			}
			
			//loader start
			$('#contact_saveBtn').html($('#contact_saveBtn').attr('data-loading-text'));
			
			$("#contactForm span.text-danger .error").html('');
		
			$.ajax({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				data: $('#contactForm').serialize(),
				url: "{{ route('provider.update_contact') }}",
				type: "POST",
				// dataType: 'json',
				success: function (response) {
					$('#contact_saveBtn').html('Update');
					
					if(response['status'] == 'success'){
						
						$("#contactInfoModal").modal('hide');
						
						callProgressBar();
						var updatedata = response['user_data'];
						$('.user_phone').text(updatedata['phone']);
						$('.user_email').text(updatedata['email']);
						$('.user_address').text(updatedata['address']);
						
						if($("#exampleCheck3").is(':checked')==false){
							if($('#address').val() !=''){
								$("#exampleCheck3").prop("checked", true);
								var p_w = parseInt($(".progress_bar").attr('data-id'));
									p_w = p_w+25;
								$(".progress_bar").attr('data-id',p_w)
								$(".progress_bar").width(p_w+'%');
								$(".progress_count").text(p_w);
							}
						}
					}
				},
				error: function (data) {
					$('#contact_saveBtn').html('Update');
					if(data.responseJSON.errors) {
						let errors = data.responseJSON.errors;
						if(errors.phone_num){ $('#phone_num-error').html(errors.phone_num[0]); }
						if(errors.email_id){ $('#email_id-error').html(errors.email_id[0]); }
						if(errors.address){ $('#address-error').html(errors.address[0]); }			
					}
				
				}
			});
		});


		/*****What they treat form submissin****/
		$("#treatCatForm").submit(function(e) {
			e.preventDefault();
			$('#treatCat_saveBtn').html($('#treatCat_saveBtn').attr('data-loading-text'));
			$("#treatCatForm span.text-danger .error").html('');
			$.ajax({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				data: $('#treatCatForm').serialize(),
				url: "{{ route('provider.updateTreatCat') }}",
				type: "POST",				
				success: function (response) {
					$('#treatCat_saveBtn').html('Update');
					
					if(response['status'] == 'success'){
						$("#treatCatModal").modal('hide');
						//successAlert(response['message'],2000,'top-right');
						callProgressBar();
						var updatedata = response['user_data'];
						$('.services_text').html(updatedata);
						/*setTimeout(function(){
							location.reload();
						}, 2000);*/
					}
				},
				error: function (data) {
					$('#treatCat_saveBtn').html('Update');
					if(data.responseJSON.errors) {
						let errors = data.responseJSON.errors;
						if(errors.first_name){ $('#first_name-error').html(errors.first_name[0]); }
						if(errors.last_name){ $('#last_name-error').html(errors.last_name[0]); }
						if(errors.phone){ $('#phone-error').html(errors.phone[0]); }
						if(errors.dob){ $('#dob-error').html(errors.dob[0]); }						
					}
				
				}
			});
		});


	$('#treat_cat').on("select2:unselecting", function(e){
		var service_id =   e.params.args.data.id;
	  
		if(service_id){
			$.ajax({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				data: {'service_id':service_id},
				url: "{{ route('provider.deleteProviderService') }}",
				type: "POST",				
				success: function (response) {
					
				},
				error: function (data) {

				}
			});
		}
    });
	
	function changeProfile(e)
	{     
		var saida = document.getElementById('file_input');
		var quantos = saida.files.length;
		for(i = 0; i < quantos; i++){
			var urls = URL.createObjectURL(e.target.files[i]);
			document.getElementById('profileData').src=urls;
			document.getElementById('sortProfileData').src=urls;
		}
		var data = new FormData(document.getElementById("changeProfileForm"));
		$.ajax({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			type: "POST",
			url: "{{ route('provider.change_profile_pic') }}",
			data:  data,
			enctype: 'multipart/form-data',
			processData: false,  // tell jQuery not to process the data
			contentType: false,   // tell jQuery not to set contentType
			dataType: "json",
			success: function(response)
			{
				//successAlert(response['message'],2000,'top-right');
				callProgressBar();
			},
			beforeSend: function()
			{
				// some code before request send if required like LOADING....
			}
		});
	}
	
	function addCertificate(event)
	{     
		var saida = document.getElementById('file_inputs');
		var quantos = saida.files.length;
		for(i = 0; i < quantos; i++){
			var urls = URL.createObjectURL(event.target.files[i]);
			
			var urls = URL.createObjectURL(event.target.files[i]);
			var file = event.target.files[i];
			if(file.type == "application/pdf"){
				var urls = "{{ asset('images/pdf-icon.png') }}" ;
			}
			
			if(file.type == "application/vnd.openxmlformats-officedocument.wordprocessingml.document"){
				var urls = "{{ asset('images/doc-icon.png') }}" ;
			}
			
			$("#certificate_licence").append('<div class="col-md-4"><div class="demo_pic"><img src="'+urls+'"></div></div>');
		}
		
		var data = new FormData(document.getElementById("moreCertificateForm"));
		
		$.ajax({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			type: "POST",
			url: "{{ route('provider.add_more_certificate') }}",
			data:  data,
			enctype: 'multipart/form-data',
			processData: false,  // tell jQuery not to process the data
			contentType: false,   // tell jQuery not to set contentType
			dataType: "json",
			success: function(response)
			{
				//successAlert(response['message'],2000,'top-right');
				callProgressBar();
				if($("#exampleCheck4").is(':checked')==false){
					$("#exampleCheck4").prop("checked", true);
					var p_w = parseInt($(".progress_bar").attr('data-id'));
						p_w = p_w+25;
					$(".progress_bar").attr('data-id',p_w)
					$(".progress_bar").width(p_w+'%');
					$(".progress_count").text(p_w);
	
				}
			},
			beforeSend: function()
			{
				// some code before request send if required like LOADING....
			}
		});
	}
	

	</script>	
	<script src="//mozilla.github.io/pdf.js/build/pdf.js"></script>
	<script>
	$(document).on('click', '.credentialsPdfModal', function() {
		
		var url = $(this).attr("data-url");

		// Loaded via <script> tag, create shortcut to access PDF.js exports.
		var pdfjsLib = window['pdfjs-dist/build/pdf'];

		// The workerSrc property shall be specified.
		pdfjsLib.GlobalWorkerOptions.workerSrc = '//mozilla.github.io/pdf.js/build/pdf.worker.js';

		var pdfDoc = null,
			pageNum = 1,
			pageRendering = false,
			pageNumPending = null,
			scale = 1.1,
			canvas = document.getElementById('the-canvas'),
			ctx = canvas.getContext('2d');

		/**
		 * Get page info from document, resize canvas accordingly, and render page.
		 * @param num Page number.
		 */
		function renderPage(num) {
		  pageRendering = true;
		  // Using promise to fetch the page
		  pdfDoc.getPage(num).then(function(page) {
			var viewport = page.getViewport({scale: scale});
			canvas.height = viewport.height;
			canvas.width = viewport.width;

			// Render PDF page into canvas context
			var renderContext = {
			  canvasContext: ctx,
			  viewport: viewport
			};
			var renderTask = page.render(renderContext);

			// Wait for rendering to finish
			renderTask.promise.then(function() {
			  pageRendering = false;
			  if (pageNumPending !== null) {
				// New page rendering is pending
				renderPage(pageNumPending);
				pageNumPending = null;
			  }
			});
		  });

		  // Update page counters
		  document.getElementById('page_num').textContent = num;
		}

		/**
		 * If another page rendering in progress, waits until the rendering is
		 * finised. Otherwise, executes rendering immediately.
		 */
		function queueRenderPage(num) {
		  if (pageRendering) {
			pageNumPending = num;
		  } else {
			renderPage(num);
		  }
		}

		/**
		 * Displays previous page.
		 */
		function onPrevPage() {
		  if (pageNum <= 1) {
			return;
		  }
		  pageNum--;
		  queueRenderPage(pageNum);
		}
		document.getElementById('prev').addEventListener('click', onPrevPage);

		/**
		 * Displays next page.
		 */
		function onNextPage() {
		  if (pageNum >= pdfDoc.numPages) {
			return;
		  }
		  pageNum++;
		  queueRenderPage(pageNum);
		}
		document.getElementById('next').addEventListener('click', onNextPage);

		/**
		 * Asynchronously downloads PDF.
		 */
		pdfjsLib.getDocument(url).promise.then(function(pdfDoc_) {
		  pdfDoc = pdfDoc_;
		  document.getElementById('page_count').textContent = pdfDoc.numPages;

		  // Initial/first page rendering
		  renderPage(pageNum);
		});
		
	});

	</script>
@endsection