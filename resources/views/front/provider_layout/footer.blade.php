            </div>
        </div>
    </section>
<input type="hidden" id="user_id" value="{{ auth()->user()->id }}">
<input type="hidden" id="user_role" value="{{ auth()->user()->user_role }}">
<input type="hidden" id="pusher_api_key" value="{{ env('PUSHER_APP_KEY') }}">
<input type="hidden" id="pusher_channel" value="{{ env('PUSHER_APP_CHANNEL') }}">
<input type="hidden" id="pusher_cluster" value="{{ env('PUSHER_APP_CLUSTER') }}">

@stack('scripts')

<script src="//js.pusher.com/3.1/pusher.min.js"></script>
<!-- pusher notification js-->
<script src="{{asset('dashboard/js/provider_pusher_notification.js')}}"></script>


	</body>
</html>