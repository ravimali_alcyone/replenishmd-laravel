@extends('front.layouts.app')

@section('content')

    <section class="inner_banner banner_with_spike plans_banner">
        <div class="blue_bg_overlay">
            <div class="container">
                <div class="content_wrapper">
                    <div class="b_text text-center">
                        <h1>Privacy Policy</h1>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                    </div>
                </div>
            </div>
            <img src="/assets/images/bottom_curve.svg" alt="bottom_curve">
        </div>
    </section>


    <section class="oac">
        <div class="terms_sec">
            <h1 class="title">ReplenishMD Health Privacy Policy</h1>

            <div class="sub_sec">
                <p class="sub_heading">1. Introduction</p>
                <p>This Privacy Policy describes how ReplenishMD, Inc. collects and uses Personal Data about you through the use of our Website, and through email, text, and other electronic communications between you and ReplenishMD, Inc.</p>
                <p>ReplenishMD, Inc. (“ReplenishMD,” or “we,” “our,” or “us”) respects your privacy, and we are committed to protecting it through our compliance with this policy.</p>
                <p>This Privacy Policy (our “Privacy Policy”) describes the types of information we may collect from you or that you may provide when you visit the website replenishmd.com (our “Website”) and our practices for collecting, using, maintaining,
                    protecting, and disclosing that information.</p>
                <p class="sub_heading mt-5">2. Children Under the Age of 18</p>
                <p>Our Website is not intended for children under the age of 18 and children under the age of 18 are not permitted to use our Website. We will remove any information about a child under the age of 18 if we become aware of it.</p>
                <p>Our Website is not intended for children under 18 years of age. No one under age 18 may provide any information to or through the Website. We do not knowingly collect Personal Data from children under 18. If you are under the age of 18
                    and wish to create an account with ReplenishMD, your parent or legal guardian must create the account, submit your personal information and agree to this Privacy Policy and Terms of Use on your behalf. If you are under 18, do not use or
                    provide any information on our Website or on or through any of its features, including your name, address, telephone number, email address, or any screen name or user name you may use. If we learn we have collected or received Personal
                    Data from a child under 18 without verification of parental consent, we will delete that information. If you believe we might have any information from a child under 18, please contact us at care@replenishmd.com or call us at (833)447-2775.</p>
                <p class="sub_heading mt-5">3. Information We Collect About You and How We Collect It</p>
                <p>We collect different types of information about you, including information that may directly identify you, information that is about you but individually does not personally identify you, and information that we combine with our other
                    users. That includes information that we collect directly from you or through automated collection technologies.</p>
            </div>
        </div>
    </section>
@endsection