@extends('front.layouts.app')

@section('content')

    <section class="inner_banner banner_with_spike plans_banner">
        <div class="blue_bg_overlay">
            <div class="container">
                <div class="content_wrapper">
                    <div class="b_text text-center">
                        <h1>Terms & Conditions</h1>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                    </div>
                </div>
            </div>
            <img src="/assets/images/bottom_curve.svg" alt="bottom_curve">
        </div>
    </section>


    <section class="oac">
        <div class="terms_sec">
            <h1 class="title">ReplenishMD Medical Direct Health Care Program Membership Agreement</h1>

            <div class="sub_sec">
                <p class="sub_heading">INITIAL NOTICES:</p>
                <p>NOT HEALTH INSURANCE. THIS AGREEMENT IS NOT HEALTH INSURANCE AND DOES NOT MEET ANY INDIVIDUAL HEALTH INSURANCE MANDATE THAT MAY BE REQUIRED BY FEDERAL LAW, INCLUDING THE FEDERAL PATIENT PROTECTION AND AFFORDABLE CARE ACT AND COVERS ONLY
                    LIMITED ROUTINE HEALTH CARE SERVICES AS DESIGNATED IN THIS AGREEMENT</p>
                <p>BINDING ARBITRATION. THIS CONTRACT CONTAINS A BINDING ARBITRATION PROVISION WHICH MAY BE ENFORCED BY THE PARTIES</p>
                <p class="mt-5">1. ReplenishMD Program Membership Options and Membership Fees.</p>
                <p>The Program offers different Membership Options, each with varying scope of services and fees. You must select your desired Membership Option from the available list on ReplenishMD’s website at www.parsleyhealth.com/join. The terms
                    of your selected Membership Option, which can be found on the ReplenishMD’s website at http://replenishmd.com/join. Membership Options may change from time to time, and you will receive at least ninety (90) days’ advance
                    notice of such changes. However, you are entitled to the full scope of your Membership Option as it existed as of the effective date of a specific Membership Term for the duration of such Membership Term. For any subsequent Renewal
                    Term, you may accept the revised Membership Options or reject such and terminate your Membership.</p>
                <p>You may pay your Membership Fee in a single sum or make periodic payments per a monthly Membership Fee Payment Schedule. The initial payment must be made before your Membership commences. Once paid, your Membership Fee is non-refundable,
                    except as set forth in the ReplenishMD Refund Policy, available at http://replenishmd.com/faq (“What if I change my mind about my membership?”).</p>
                <p class="mt-5">2. No Emergency Care; Certain Services and Items Excluded.</p>
                <p>If you have an emergency you must dial 911. ReplenishMD does not treat emergencies. ReplenishMD does not offer specialist medical services, medications, or supplements.</p>
                <p class="mt-5">3. No Insurance Accepted; Self-Payment Only.</p>
                <p>The Program is a direct health care service; it is not health insurance. ReplenishMD does not participate with or bill commercial health insurance plans or federal health care programs such as Medicare or Medicaid. ReplenishMD
                    providers may recommend you receive services not offered by ReplenishMD (e.g., specialty services, diagnostic tests), but in no event will ReplenishMD be responsible for any resulting medical bills.</p>
                <p>You are solely responsible for payment of all fees for ReplenishMD’s services. If you do have health insurance, your insurance policy is a contract between you and your insurance company. It is your responsibility to know your benefits,
                    and how they will apply to your benefit payments. ReplenishMD takes no responsibility to understand or be bound by the terms and conditions of such insurance. There is no guarantee your insurance company will make any payment on
                    the cost of the services you have purchased.</p>
            </div>
        </div>
    </section>

@endsection