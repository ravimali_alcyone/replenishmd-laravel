@extends('front.online_visit_layout.app')

@section('content')
    <section class="sign_in_sec_wrapper">
        <div class="sign_in_sec shadow">
            <img src="{{asset('assets/images/rmd_logo_red.svg')}}" alt="rmd_logo_red">
            <h1 class="title">Log in to your RMD platform account to continue.</h1>
            <form id="loginForm">

                <div class="form-group">
                    <input type="email" name="email" class="form-control" placeholder="Email" id="email" required="required" autofocus>
                </div>
                <div class="form-group">
                    <input type="password" name="password" class="form-control" placeholder="Password" id="pwd" required="required">
                </div>
                <button type="submit" class="btn btn-primary">Sign In</button>
            </form>
            <a href="{{ route('forgot_password') }}" class="link">LOST YOUR PASSWORD?</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<a href="{{ route('joinnow') }}" class="link">SIGNUP</a>
        </div>
    </section>	
	
	<script>
		$("#loginForm").submit(function(e) {
			e.preventDefault();
			$(".loader").css('display', 'flex');
			
			$.ajax({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				data: $('#loginForm').serialize(),
				url: "{{ route('signin_submission') }}",
				type: "POST",
				// dataType: 'json',
				success: function (response) {
					$(".loader").css('display', 'none');
					if(response['status'] == 'success'){
						callProgressBar();
						setTimeout(function(){
							if(response['redirect'] != ''){
								let url = response['redirect'];
								window.location = response['redirect'];	
							}
												
						}, 2000);
					}else{
						
						errorAlert(response['message'],3000,'top-right');						
					}
				},
				error: function (data) {
					$(".loader").css('display', 'none');
					let errors = data.responseJSON.errors;
					
					$.each(errors, function(key, value) {
						errorAlert(value[0],3000,'top-right');
					});					
				}
			});
		});		
	</script>	
	
@endsection