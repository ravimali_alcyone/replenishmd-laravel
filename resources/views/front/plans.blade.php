@extends('front.layouts.app')
@section('content')

    <section class="inner_banner banner_with_spike plans_banner">
        <div class="blue_bg_overlay">
            <div class="container">
                <div class="content_wrapper">
                    <div class="b_text text-center">
                        <h1>@if(isset($page_data['banner']->heading)) {{ $page_data['banner']->heading }}@endif</h1>
                        <p>@if(isset($page_data['banner']->description)) {{ $page_data['banner']->description }}@endif</p>
                    </div>
                </div>
            </div>
            <img src="/assets/images/bottom_curve.svg" alt="bottom_curve">
        </div>
    </section>

    <section class="plans_sec">
        <div class="container">
            <h1 class="title">Select Right Plan For You!</h1>
            <div class="plans_wrapper">
		@if($plans)
			@foreach($plans as $key => $value)
                <div class="plan shadow">
                    <div class="head">
                        <h4 class="title">{{ $value['name'] }}</h4>
                        <p class="amount special"><span class="fa fa-"></span> {{ $value['monthly_price'] }}</p>
                        <p class="amount no_special" style="display:none;"><span class="fa fa-"></span> {{ $value['total_price'] }}</p>
                        <p class="short_des special mb-1">{{ $value['monthly_plan_text'] }}</p>
                        <p class="short_des no_special mb-1" style="display:none;">{{ $value['total_plan_text'] }}</p>
						@if($value['is_total'] == 1)
							<a href="javascript:void(0);" onclick="setPlanType(this);" class="plan_criteria special">{{ $value['special'] }}</a>
							<a href="javascript:void(0);" onclick="setPlanType(this);" class="plan_criteria no_special" style="display:none;">I'd rather pay monthly</a>
						@endif
                    </div>
                    <div class="body">
                        <p>{{ $value['detail'] }}</p>
					@if(isset($value['features']) && $value['features'])
                        <div class="points_wrapper">
						@foreach($value['features'] as $k => $val)	
                            <div class="point"><span class="fa fas fa-check"></span>{{ $val['feature_name'] }}</div>
						@endforeach
                        </div>
					@endif
						@if(auth()->user() && auth()->user()->user_role == 4)
							<button class="btn btn-primary">Select</button>
						@else
							<button class="btn btn-primary" onclick="location.href = '{{ route('signup') }}/plans';">Join Now</button>
						@endif
                    </div>
					@if($value['is_most_popular'] == 1)<p class="popular_tag">MOST POPULAR</p>@endif
                </div>
			@endforeach
		@endif
		
            </div>
        </div>
    </section>

    <section class="about_sec">
        <div class="container">
            <div class="row expertise_row">
                <div class="col-sm-12 col-md-6">
                    <div class="img_wrapper">
                        <img src="{{ $page_data['covering_your_care']->image->image_1 ?? '/assets/images/care_1.png' }}" alt="Care_1" class="w-auto">
                    </div>
                </div>
                <div class="col-sm-12 col-md-6">
                    <div class="content_wrapper">
                        <h1 class="title">@if(isset($page_data['covering_your_care']->heading)) {{ $page_data['covering_your_care']->heading }}@endif</h1>
                        <p class="p_md">@if(isset($page_data['covering_your_care']->description)) {{ $page_data['covering_your_care']->description }}@endif</p>
                        <ul>
                        @if(!empty($page_data['covering_your_care']->listdata))
                            @foreach($page_data['covering_your_care']->listdata as $key => $value)								
                            <li>
                                <span class="fa fas fa-check"></span>
                                <p>{{ $value->listitems }}</p>
                            </li>
                            @endforeach
                        @endif
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="oac plans_oac">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="title">@if(isset($page_data['membership_features']->top_main_name)) {{ $page_data['membership_features']->top_main_name }}@endif</div>
                    <div class="oac_list">
                        @if(!empty($page_data['membership_features']->images))
                            @foreach($page_data['membership_features']->images as $key => $value)								
                            <div class="oac_box">
                            <div class="oacb_icon"><img src="{{ $value->image ?? '/assets/images/icon2.svg' }}" class="img-fluid" alt=""></div>
                                <div class="oac_detail">
                                    <h4>{{ $value->heading }}</h4>
                                    <p>{{ $value->description }}</p>
                                </div>
                            </div>
                            @endforeach
                        @endif
                        
                    </div>
                </div>
            </div>
        </div>
    </section>

	<script>
	
		function setPlanType(e){

			if($(e).hasClass('special')){
				$(e).hide();
				$(e).parent().find('.plan_criteria.no_special').show();				
				
				$(e).parent().find('.short_des.special').hide();				
				$(e).parent().find('.short_des.no_special').show();				
				
				$(e).parent().find('.amount.special').hide();				
				$(e).parent().find('.amount.no_special').show();				
								
			}else{
				$(e).hide();
				$(e).parent().find('.plan_criteria.special').show();
				
				$(e).parent().find('.short_des.no_special').hide();						
				$(e).parent().find('.short_des.special').show();

				$(e).parent().find('.amount.no_special').hide();						
				$(e).parent().find('.amount.special').show();				
			}

		}
	</script>
@endsection