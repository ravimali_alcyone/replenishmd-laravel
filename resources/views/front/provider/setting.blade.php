@extends('front.provider_layout.app')

@section('content')
<style>
span.select2.select2-container.select2-container--default {
    width: 100%!important;
}

.select2-container--default .select2-selection--multiple {
    font-size: 12px;
}

.pac-container {
        z-index: 10000 !important;
}
.pac-container:after {
	background-image: none !important;
	height: 0px;
}
</style>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script
      src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places&key={{ env('Google_Map_Keys')}}"
      async
    ></script> 
	<div class="col-xl-6 col-lg-6 col-md-12">
		<div class="main-center-data">
			<h3 class="display-username">Account Setting</h3>
			<div class="informative-block bg-white round-crn pd-20-30 mt-15 hover-effect-box">
				<h4 class="font-weight-bold">Change password</h4>
				<span class="edit-details"  onclick="chat_box('pswrd');"><img src="{{asset('dashboard/img/edit.svg')}}" alt="edit" /></span>
				<table class="table-responsive mt-15">
					<tbody>
						<tr>
							<td class="label-mute">Password</td>
							<td>*********</td>
						</tr>
					</tbody>
				</table>
			</div>
			
			<div class="informative-block bg-white round-crn pd-20-30 mt-15 hover-effect-box">
				<h4 class="font-weight-bold">Preferred location</h4>
				<span class="edit-details" onclick="chat_box('location');"><img src="{{asset('dashboard/img/edit.svg')}}" alt="edit" /></span>
				<table class="table-responsive mt-15">
					<tbody class="provider_location">
						@if($provider_location && $provider_location->count()>0)
						@php $i=0; @endphp
						@foreach($provider_location as $val)
						<tr>
							<td class="label-mute">{{ $val->address_type }}</td>
							<td>{{ $val->address }}</td>
						</tr>
						@php $i++; @endphp
						@endforeach
						@endif
					</tbody>
				</table>
			</div>

			<div class="informative-block bg-white round-crn pd-20-30 mt-15 hover-effect-box">
				<h4 class="font-weight-bold">Office starting/closing time</h4>
				<span class="edit-details" onclick="setting_modal('setting');"><img src="{{asset('dashboard/img/edit.svg')}}" alt="edit" /></span>
				<table class="table-responsive mt-15">
					<tbody class="office_starting">
						<tr>
							<td class="label-mute">Office Time</td>
							<td><?php if(count($office_start_end_time)>0){foreach($office_start_end_time as $val) { echo $val->st_time.' - '.$val->en_time.'<br>'; } }?></td>
						</tr>
						
						<tr>
							<td class="label-mute">Break time</td>
							<td><?php if(count($break_start_end_time)>0){ foreach($break_start_end_time as $val) { echo $val->breakSt_time.' - '.$val->breakEn_time.'<br>'; } }?></td>
						</tr>						
					</tbody>
				</table>
			</div>

			<div class="informative-block bg-white round-crn pd-20-30 mt-15 hover-effect-box">
				<h4 class="font-weight-bold">Weekend Days</h4>
				<span class="edit-details" onclick="chat_box('weekEndDays');"><img src="{{asset('dashboard/img/edit.svg')}}" alt="edit" /></span>
				<table class="table-responsive mt-15">
					<tbody>
						<tr>
							<td class="label-mute">Weekend days</td>
							<td class="weekend_day">{{ $weekend }}</td>
						</tr>
					</tbody>
				</table>
			</div>

		</div>
	</div>
	
	<div class="col-xl-3 col-lg-3 col-md-12">
		<div class="informative-block bg-white round-crn pd-20-30 settings hover-effect-box">
			<h4 class="font-weight-bold">Settings</h4>
			<div class="stwich-toggle">
				<div class="custom-control custom-switch">
				  <input type="checkbox" class="custom-control-input" id="customSwitch1">
				  <label class="custom-control-label" for="customSwitch1"></label>
				</div>
			</div>
			<h6 class="label-mute mt-15">SMS Notifications</h6>
			<p class="note m-0">By turning on this toggle, I agree to receive texts from RMD and/or {{auth()->user()->name}} to {{auth()->user()->phone}} that might be considered marketing and that may be sent using an auto dialer. I understand that agreeing is not required to purchase.</p>
		</div>
	</div>	
	
	
	<div class="chat_pop edit_pop petient chat_pop_pswrd">
		<div class="edit_pop_title">
			<h3>Update Password</h3>
			<a class="cp_close"><i class="fa fa-times"></i></a>
		</div>
        <div class="chat_block">
			<div class="container pt-3">
				<div class="row">
					<form id="changePassForm" class="col-md-12">
						<div class="form-group col-sm-12">
							<label for="old_password">Old Password <span class="text-danger">*</span></label>
							<input type="password" name="old_password" class="form-control" id="old_password" placeholder="Old Password">
							<span class="text-danger">
								<strong class="error" id="old_password-error"></strong>
							</span>
						</div>

						<div class="form-group col-sm-12">
							<label for="new_password">New Password <span class="text-danger">*</span></label>
							<input type="password" name="password" class="form-control" id="new_password" placeholder="New Password">
							<span class="text-danger">
								<strong class="error" id="new_password-error"></strong>
							</span>
						</div>

						<div class="form-group col-sm-12">
							<label for="confirm_password">Confirm New Password <span class="text-danger">*</span></label>
							<input type="password" name="password_confirmation" class="form-control" id="confirm_password" placeholder="Confirm New Password">
							<span class="text-danger">
								<strong class="error" id="confirm_password-error"></strong>
							</span>
						</div>
						
						<div class="form-group col-sm-12">
							<button type="submit" id="saveBtn" class="accept_btn btn btn-outline-primary btn-sm" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Processing" >Submit</button>
							<button type="button" class="btn btn btn-outline-danger btn-sm" onclick="$('.cp_close').trigger('click');">Cancel</button>
						</div>
					</form>
				</div>							
			</div>
    	</div>
	</div>
	
	<div class="chat_pop edit_pop petient chat_pop_location">
		<div class="edit_pop_title">
			<h3>Preferred location</h3>
			<a class="cp_close"><i class="fa fa-times"></i></a>
		</div>
        <div class="chat_block">
			<div class="container pt-3">
				<div class="row">
					<form id="locationForm" class="col-md-12">
						<div class="form-group col-sm-12">
						@if($provider_location && $provider_location->count()>0)
							@php $i=0; $a=1; @endphp
							@foreach($provider_location as $val)
								<div style="border:2px dotted #c8c4d4;padding:10px;border-radius:5px;position: relative;" class="location_box form-group item_wrapper">
									<a href="javascript:void(0)" class="cls_btn remove_item" id="{{$val->id}}">x</a>
									<div class="form-group has-feedback">
										<input type="text" name="add_type[]" class="form-control" id="" value="{{$val->address_type}}">
									</div>
									<div class="has-feedback">
										<input name="address[]" type="text" id="address_{{$a}}" class="form-control address_text" value="{{$val->address}}">
										<div class="address_html">
										</div>
									</div>
									<input type="hidden" name="location_id[]" id="" value="{{$val->id}}">
								</div>
							@php $i++; $a++; @endphp
							@endforeach
						@else
							<div style="border:2px dotted #c8c4d4;padding:10px;border-radius:5px;position: relative;" class="location_box form-group item_wrapper">
								<div class="form-group has-feedback">
									<input type="text" name="add_type[]" class="form-control" id="" placeholder="Address Type">
								</div>
								<div class="has-feedback">
									<input name="address[]" type="text" class="form-control address_text" id="address_1" placeholder="Address">
									<div class="address_html">
									</div>
								</div>
								<input type="hidden" name="location_id[]" id="" value="">
							</div>
						@endif
							<div class="form-group more_location_wrapper"></div>
							<div class="has-feedback d-flex justify-content-end">
								<button type="button" class="btn btn btn-outline-primary btn-sm add_location">Add more</button>
							</div>
						</div>
						<div class="form-group col-sm-12">
							<button type="submit" id="saveBtn" class="accept_btn btn btn-outline-primary btn-sm" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Processing" >Submit</button>
							<button type="button" class="btn btn btn-outline-danger btn-sm" onclick="$('.cp_close').trigger('click');">Cancel</button>
						</div>
					</form>
				</div>							
			</div>
    	</div>
	</div>

	<div class="chat_pop edit_pop petient chat_pop_setting">
		<div class="edit_pop_title">
			<h3>Office starting/closing time</h3>
			<a class="cp_close"><i class="fa fa-times"></i></a>
		</div>
        <div class="chat_block">
			<div class="container pt-3">
				<div class="row">
					<form id="settingForm" class="col-md-12">
						
						<div class="form-group col-sm-12">
							<div class="rmd_office_start_end_time"></div>
						</div>

						<div class="form-group col-sm-12">
							<div class="more_break_wrapper"></div>
						</div>
						
						<div class="form-group col-sm-12">
							<button type="submit" id="saveBtn3" class="accept_btn btn btn-outline-primary btn-sm" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Processing">Add</button>
							<button type="button" class="btn btn-outline-danger btn-sm" onclick="$('.cp_close').trigger('click');">Cancel</button>
						</div>
					</form>
				</div>							
			</div>
    	</div>
	</div>

	<div class="chat_pop edit_pop petient chat_pop_weekEndDays">
		<div class="edit_pop_title">
			<h3>Weekend days setting</h3>
			<a class="cp_close"><i class="fa fa-times"></i></a>
		</div>
        <div class="chat_block">
			<div class="container pt-3">
				<div class="row">
					<form id="weekendForm" class="col-md-12">
						<div class="form-group col-sm-12">
							<label for="old_password">Select day <span class="text-danger">*</span></label>
							<select name="weekend_day[]" class="custom-select select2 form-control" id="weekend_day" multiple="multiple">
							<?php
								
								$weekendd = explode(',',$weekend);
								$day = array("Mon","Tue","Wed","Thu","Fri","Sat","Sun");
								for($i=0; $i<count($day); $i++)
								{
										if(count($weekendd) < count($day))
									{
										$ch = (in_array($day[$i], $weekendd)) ? "selected=selected" : "";
									}else{
										$ch = "";
									}  
									
									echo '<option '.$ch.' value="'.$day[$i].'" >'.$day[$i].'</option>';
								}
							?>
							</select>
						</div>
						
						<div class="form-group col-sm-12">
							<button type="submit" id="saveBtn" class="accept_btn btn btn-outline-primary btn-sm" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Processing" >Submit</button>
		  					<button type="button" class="btn btn btn-outline-danger btn-sm" onclick="$('.cp_close').trigger('click');">Cancel</button>
						</div>
					</form>
				</div>							
			</div>
    	</div>
	</div>
	
	
	








	
	<script type="text/javascript">

		$(document).ready(function(){
			$('.cp_close').click(function(flag){
				var hidden = $('.chat_pop');
				hidden.animate({"right":"-100%"}, "slow").removeClass('visible');
			});
		});

		
		function chat_box(flag)
		{
			var hidden = $('.chat_pop_'+flag);
			if (hidden.hasClass('visible')){
				hidden.animate({"right":"-100%"}, "slow").removeClass('visible');
			} 
			else 
			{
				hidden.animate({"right":"0"}, "slow").addClass('visible');
			}
		}

	    function get_places(serchinput)
		{  
			var autocomplete;
			autocomplete = new google.maps.places.Autocomplete((document.getElementById(serchinput)), {
				types : ['geocode'],
				//componentRestrictions : {
				//	country: "USA"
				//}
			});
			google.maps.event.addListener(autocomplete, 'places_changed', function (){
				var near_place = autocomplete.getPlace();
			});
		}
		  
    		  
		$(function () {
			 $('.timepicker').datetimepicker({
				format : 'LT'
			 });

			 $("#weekend_day").select2({
			 minimumResultsForSearch: -1,
			 placeholder: "Select a day",
		});
		
		    var location_box = $('.location_box').length;
		     
    		  if(location_box > 1){
    		      //console.log(location_box);
    		      for (let i = 1; i <= location_box; i++) {
                       get_places('address_'+i);
                     }
                    
    		  }else{
		        get_places('address_1');
    		  
    		  }
    		 
    		
		 });
		 
		/**********Reset Password*********/	

		$("#changePassForm").submit(function(e) {
			e.preventDefault();
			$('#saveBtn').html($('#saveBtn').attr('data-loading-text'));
			$("#changePassForm span.text-danger .error").html('');
		
			$.ajax({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				data: $('#changePassForm').serialize(),
				url: "{{ route('provider.change_password') }}",
				type: "POST",
				success: function (response) {
					$('#saveBtn').html('Submit');
					if(response['status'] == 'success'){
						callProgressBar();
						$(".cp_close").trigger('click');
						$("input['type=password']").val('');
					}
				},
				error: function (data) {
					$('#saveBtn').html('Submit');
					
					$(".loader").css('display', 'none');
					let errors = data.responseJSON.errors;
					$.each(errors, function(key, value) {
						errorAlert(value[0],3000,'top-right');
					});
				
				}
			});
		});

		/****End Here*****/
	
		
		function setting_modal(flag) 
		{
            $("#settingForm span.text-danger .error").html('');
            //$(".loader").css('display', 'flex');
			
			var hidden = $('.chat_pop_'+flag);
			if (hidden.hasClass('visible')){
				hidden.animate({"right":"-100%"}, "slow").removeClass('visible');
			} 
			else 
			{
				hidden.animate({"right":"0"}, "slow").addClass('visible');
			}
			
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data:{'type':'setting'},
				url: "{{ route('provider.get_timings') }}",
                type: "POST",
                success: function (response) {
            
                    if (response['status'] == 'success') {
                        //$(".loader").css('display', 'none');
                        $("#settingModal").modal('show');
						$('.rmd_office_start_end_time').html(response['rmd_office_start_html']);
						$('.timepicker').datetimepicker({ format : 'LT' });
                    }
                },
                error: function (data) {
                    $(".loader").css('display', 'none');
                    if (data.responseJSON.errors) {
                        let errors = data.responseJSON.errors;
                    }

                }
            });
            
        }
		
		var currenttime = "{{ date('h:i A') }}";
		/****Add more office time and break time******/
		//var list = '<?php echo $provider_location; ?>';
		//var list = '<?php foreach($provider_location as $val){ echo '<option value="'.$val['id'].'">'.$val['address_type'].'</option>'; } ?>';
		
		$(document).on('click', '.add_item', function() { 
		    var newel = $('.loc_opt:last').clone();  // Create clone of location dropdown
			$('.more_items_wrapper').append(
				'<div class="form-row item_wrapper">'+
				  '<div class="form-group col-md-3 has-feedback">'+
					'<div class="input-group date mt-2">'+
						'<input type="text" class="form-control timepicker" name="office_start_time[]" value='+"{{ date('h:i A') }}"+'/>'+
						'<span class="input-group-text" id="basic-addon1">'+
							'<img src="{{asset("assets/images/clock.svg")}}">'+
						'</span>'+
					'</div>'+				
				  '</div>'+
				  '<div class="form-group col-md-3 has-feedback">'+
					'<div class="input-group date mt-2">'+
						'<input type="text" class="form-control timepicker" name="office_close_time[]" value='+"{{ date('h:i A') }}"+'/>'+
						'<span class="input-group-text" id="basic-addon1">'+
							'<img src="{{asset("assets/images/clock.svg")}}">'+
						'</span>'+
					'</div>'+					
				  '</div>'+
				  '<div class="form-group col-md-4 has-feedback">'+			
						'<div class="input-group date mt-2 loc_opt">'+
						'</div>'+
					'<span class="text-danger">'+
						'<strong class="error" id="dob-error"></strong>'+
					'</span>'+	
				'</div>'+
				  '<div class="form-group col-md-1 offset-md-1 has-feedback mt-3">'+
					'<a href="javascript:void(0)" title="Remove" class="remove_item"><span style="color:red;"><i class="fa fa-minus" aria-hidden="true"></i></span></a>'+
					'</div>'+
				'</div>'
			);
			$('.loc_opt:last').append(newel);
			$('.timepicker').datetimepicker({ format : 'LT' });
		});

		$(document).on('click', '.remove_item', function() {
			$(this).closest('.item_wrapper').remove();
		});
		
		
		$(document).on('click', '.add_break', function() {    
			$('.more_break_wrapper').append(
				'<div class="form-row break_wrapper">'+
					'<div class="form-group col-md-5 has-feedback">'+
						'<div class="input-group date mt-2">'+
						'<input type="text" class="form-control timepicker" name="break_start_time[]" value='+"{{ date('h:i A') }}"+'/>'+
						'<span class="input-group-text" id="basic-addon1">'+
							'<img src="{{asset("assets/images/clock.svg")}}">'+
						'</span>'+
					'</div>'+
					'</div>'+
					'<div class="form-group col-md-5 has-feedback">'+
						'<div class="input-group date mt-2">'+
						'<input type="text" class="form-control timepicker" name="break_end_time[]" value='+"{{ date('h:i A') }}"+'/>'+
						'<span class="input-group-text" id="basic-addon1">'+
							'<img src="{{asset("assets/images/clock.svg")}}">'+
						'</span>'+
					'</div>'+				
					'</div>'+
					'<div class="form-group col-md-1 offset-md-1 has-feedback mt-3">'+
						'<a href="javascript:void(0)" title="Remove" class="remove_break"><span style="color:red;"><i class="fa fa-minus" aria-hidden="true"></i></span></a>'+
					'</div>'+			  
				'</div>'
			);
			$('.timepicker').datetimepicker({ format : 'LT' });
		});

		$(document).on('click', '.remove_break', function() {
			$(this).closest('.break_wrapper').remove();
		});


		/****Add more location******/
	    $(document).on('click', '.add_location', function() {    
		    let locationCount = $('.location_box').length + 1;
            var id='address_'+locationCount;
			$('.more_location_wrapper').append(
				'<div style="border:2px dotted #c8c4d4;padding:10px;border-radius: 5px;" class="location_box form-group position-relative item_wrapper">'+
				'<a href="javascript:void(0)" class="cls_btn remove_item">x</a>'+
				'<div class="form-group has-feedback">'+
				'<input type="text" name="add_type[]" class="form-control" placeholder="Add Type">'+
				'</div>'+
				'<div class="has-feedback">'+
				'<input name="address[]" type="text" class="form-control address_text" id="'+id+'" placeholder="Address"><div class="address_html"></div>'+
				'</div>'+
				'<input type="hidden" name="location_id[]" id="" value="">'+
				'</div>'
			);
			get_places(id);
			
		});

		$(document).on('click', '.remove_item', function() {
			var locId = this.id;
			
			$.ajax({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				data: {"id":locId},
				url: "{{ route('provider.delete_location') }}",
				type: "POST",
				success: function (response) {
					if(response['status'] == 'success'){
						$("#settingModal").modal('hide');
						$(this).closest('.item_wrapper').remove();
						callProgressBar();
						//successAlert(response['message'],2000,'top-right');
						setTimeout(function(){
							
							location.reload();
						}, 2000);
					}
				}
			});

			
		});
		/**********End here**********/
		
		$("#settingForm").submit(function(e) {
			e.preventDefault();
			$('#saveBtn3').html($('#saveBtn3').attr('data-loading-text'));
			$("#settingForm span.text-danger .error").html('');
			$.ajax({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				data: $('#settingForm').serialize(),
				url: "{{ route('provider.update_setting') }}",
				type: "POST",
				success: function (response) {
					$('#saveBtn3').html('Update');
					if(response['status'] == 'success'){
						callProgressBar();	
						var office_starting =response['user_data'];
						$('.office_starting').html(office_starting);
						$(".cp_close").trigger('click');
					}
				},
				error: function (data) {
					$(".loader").css('display', 'none');
					$('#saveBtn3').html('Update');
					let errors = data.responseJSON.errors;
					$.each(errors, function(key, value) {
						errorAlert(value[0],3000,'top-right');
					});
				}
			});
		});

		$("#locationForm").submit(function(e) {
			e.preventDefault();
			$('#saveBtn3').html($('#saveBtn3').attr('data-loading-text'));
			$("#locationForm span.text-danger .error").html('');
			$.ajax({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				data: $('#locationForm').serialize(),
				url: "{{ route('provider.add_location') }}",
				type: "POST",
				// dataType: 'json',
				success: function (response) {
					$('#saveBtn3').html('Update');
					if(response['status'] == 'success'){
						callProgressBar();	
						var location_html =response['user_data'];
						var locations_box =response['prefered_locations_box'];
						$('.provider_location').html(location_html);
						$('.prefered_locations_box').html(locations_box);
						$(".cp_close").trigger('click');
					}
				},
				error: function (data) {
					$('#saveBtn3').html('Update');
					if(data.responseJSON.errors) {
						let errors = data.responseJSON.errors;
						$.each(errors, function(key, value) {
    						errorAlert(value[0],3000,'top-right');
    					});
					}
				
				}
			});
		});

		/******Weekend Setting******/
		$("#weekendForm").submit(function(e) {
			e.preventDefault();
			$('#saveBtn3').html($('#saveBtn3').attr('data-loading-text'));
			$("#weekendForm span.text-danger .error").html('');
			$.ajax({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				data: $('#weekendForm').serialize(),
				url: "{{ route('provider.add_weekend') }}",
				type: "POST",
				success: function (response) {
					$('#saveBtn3').html('Update');
					if(response['status'] == 'success'){
						callProgressBar();
						var location_html =response['user_data'];
						$('.weekend_day').html(location_html['weekend_days']);
						$(".cp_close").trigger('click');
					}
				},
				error: function (data) {
					$(".loader").css('display', 'none');
					$('#saveBtn3').html('Update');
					let errors = data.responseJSON.errors;
					$.each(errors, function(key, value) {
						errorAlert(value[0],3000,'top-right');
					});
				}
			});
		});

		
	</script>	
@endsection