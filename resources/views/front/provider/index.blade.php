@extends('front.provider_layout.app')

@section('content')
<div class="col-xl-10 col-lg-9 col-md-8 col-sm-12">
	<div class="row">
		<div class="col-xl-9 col-lg-8 col-md-8 col-sm-12">
			<div class="main-center-data">
				<h3 class="display-username">Hi, {{auth()->user()->name}}</h3>
				<form class="mt-15">
					<div class="form-group show-left-border">
						<input type="text" placeholder="What are your health goals?"/>
					</div>
				</form>
				<div class="row">
					<div class="col-md-4">
						<ul class="m-0 p-0">

							<li>
								<a href="#" class="mt-15">
									<div class="show-section-details">
										<img src="{{asset('dashboard/img/booking.svg')}}" alt="icon"/>
										<h4 class="main-headings">My Booking<br/>Calendar</h4>
									</div>
								</a>
							</li>
							<li>
								<a href="#" class="mt-15">
									<div class="show-section-details">
										<img src="{{asset('dashboard/img/documents.svg')}}" alt="icon"/>
										<h4 class="main-headings">My<br/>Documents</h4>
									</div>
								</a>
							</li>
						</ul>
					</div>
					<div class="col-md-4">
						<ul class="m-0 p-0">
							<li>
									<div class="doctor-outer-block">
										<img src="{{asset('dist/images/user_icon.png')}}" alt="user"/>
										<h4 class="main-headings">Lakhan Shukla</h4>
										<span class="short-description">ED Patient</span>
										<div class="chat-messages mt-15">
											<img src="{{asset('dashboard/img/chat2.svg')}}" alt="chat"/>
											2
										</div>
										<div class="click-action mt-30">
											<a href="#">Chat</a>
										</div>
									</div>
							</li>
							<li>
								<a href="#" class="mt-15">
									<div class="show-section-details">
										<img src="{{asset('dashboard/img/ask2.svg')}}" alt="icon"/>
										<h4 class="main-headings">Quick Q & A</h4>
									</div>
								</a>
							</li>
						</ul>
					</div>
					<div class="col-md-4">
						<ul class="m-0 p-0">
							<li>

							</li>
						</ul>
					</div>
				</div>
				<div class="looking-for-outer mt-15">
					<div class="looking-for1 hover-effect-box round-crn">
						<span>What are you looking for?</span>
						<h4 class="mt-15">Telemedicine Services</h4>
						<div class="see-all mt-15">
							<a href="#">Explore all <img src="{{asset('dashboard/img/arrow-forward.svg')}}" alt="arrow-forward"></a>
						</div>
					</div>
					<div class="looking-for2 pd-20-30 hover-effect-box round-crn">
						<span>What are you looking for?</span>
						<h4 class="mt-15">Concierge Services</h4>
						<div class="see-all mt-15">
							<a href="#">Explore all <img src="{{asset('dashboard/img/arrow-forward.svg')}}" alt="arrow-forward"></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	
		<div class="col-xl-3 col-lg-4 col-md-4 col-sm-12">
		@if($blogs && $blogs->count() > 0)
			<div class="blog-stories">
				<h4 class="blog-heading">Blog stories</h4>
				<ul class="m-0 p-0">
				@foreach($blogs as $key => $value)
					<li class="mt-15">
						<a href="{{ route('front.blogs.post', $value->slug) }}">
							<div class="blog-feature-image">
								<img src="{{ env('APP_URL').'/'.$value->image }}" alt="{{ $value->title}}" />
							</div>
							<div class="blog-extract short-description">
							{{ $value->title }}
							</div>
						</a>
					</li>
				@endforeach	
				</ul>
				<div class="see-all mt-30">
					<a href="{{ route('front.blogs') }}">See all Stories <img src="{{ asset('dashboard/img/arrow-forward.svg') }}" alt="arrow-forward"/></a>
				</div>
			</div>
		@endif	

			<div class="advertisment mt-15">
				<img src="{{asset('dashboard/img/ads.svg')}}" alt="ads" class="img-fluid" />
			</div>
		</div>
	</div>
	
</div>
	
@endsection