@extends('front.provider_layout.app')

@section('content')
@push('header_scripts')
	<style>
	tr td > .fc-day-grid-event{
		/*z-index: -1;*/
		cursor: pointer;
	}

	#myModal .modal-content {
		min-width: 600px;
	}

	#myModal input.form-control.timepicker {
		font-size: 13px;
	}

	#myModal .visit_type {
		padding: 8px;
		border: 1px solid #ccc;
		margin: 1%;
		width: 48%;
		border-radius: 1%;
	}

	.form-check-label {
		margin-bottom: 0;
		font-size: 12px;
	}

	span.select2.select2-container.select2-container--default {
		width: 100%!important;
	}

	.select2-container--default .select2-selection--multiple {
		font-size: 12px;
	}

	<?php if($weekend_days_arr){
			foreach($weekend_days_arr as $k => $w){ ?>
	td.fc-day.fc-widget-content.fc-<?php echo strtolower($w);?>.fc-future.disabled {
		pointer-events: none;
		background-color: ghostwhite;
		color:red;
	}
	<?php  } } ?>

	.add_timeslot_body {
  height: 100%;
}
	.add_timeslot_body form {
  height: 100%;
  display: flex;
  flex-direction: column;
  padding: 15px 0;
  justify-content: space-between;
}
	.add_timeslot_top  {
  height: 690px;
  overflow-y: scroll;
  overflow-x: hidden;
  scrollbar-width: thin;
  height: calc(100% - 90px);
}

	</style>
@endpush		
	<div class="col-xl-10 col-lg-9 col-md-12">
		<div class="main-center-data">
			<h3 class="display-username">Appointment Availability Timeslots</h3>	
			<div class="mt-3 block-area">
				<div id="calendar"></div>
			</div>
			<input type="hidden" id="start_date" value="<?php echo date('Y-m-d');?>">
			<input type="hidden" id="end_date" value="<?php echo date('Y-m-t');?>">
		</div>
	</div>

 <!-- SideBar Modal -->

	<div id="myModal" class="chat_pop edit_pop chat_pop_timeslot">
		<div class="edit_pop_title">
			<h3>Add Availability Time slot :</h3>
			<a class="cp_close normal"><i class="fa fa-times"></i></a>
		</div>
        <div class="chat_block mt-3">
			<div class="add_timeslot_body container">
				
				  <form id="availabilityTimeSlotForm">
					<div id="timeslot-data-div" class="add_timeslot_top">

					</div>
						
					<div class="add_timeslot_foot">
						<div class="row">
						<div class="col-md-12">
							<div class="form-row">
								
								<div class="form-group col-md-2">
									<label>Copy <span class="text-danger">*</span></label>			
								</div>					
								<div class="form-group col-md-10">				
									<div class="form-check form-check-inline" style="display:none;">
									<input class="form-check-input" type="radio" name="setting_filter" id="inlineRadio1" value="1" checked>
									<label class="form-check-label" for="inlineRadio1">This Date</label>
									</div>
									<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="setting_filter" id="inlineRadio2" value="2">
									<label class="form-check-label" for="inlineRadio2">This Week</label>
									</div>
									<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="setting_filter" id="inlineRadio3" value="3">
									<label class="form-check-label" for="inlineRadio3">Next Week</label>
									</div>
									<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="setting_filter" id="inlineRadio4" value="4">
									<label class="form-check-label" for="inlineRadio4">This Month</label>
									</div>	
									<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="setting_filter" id="inlineRadio5" value="5">
									<label class="form-check-label" for="inlineRadio5">Next Month</label>
									</div>
									<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="setting_filter" id="inlineRadio6" value="6" onchange="getMonthDropdown(this)">
									<label class="form-check-label" for="inlineRadio6">Custom</label>
									</div>					
									<span class="text-danger">
										<strong class="error" id="setting_filter-error"></strong>
									</span>				
								</div>			  
							</div>
							
							<div id="customMonthArea" class="form-row" style="display:none;">
								
								<div class="form-group col-md-2">
									<label><small>Select Months</small> </label>			
								</div>	
								
								<div class="form-group col-md-10">
									<select name="custom_months[]" class="custom-select select2" id="custom_month_dropdown" multiple="multiple">
									</select>
								</div>					
							</div>
							
						</div>
						</div>
						<div class="row">
							<div class="col">
							<input type="hidden" id="appointment_date" name="appointment_date">
							<button id="sub_btn" type="submit" class="btn btn-outline-primary btn-sm">Submit</button>
							<button type="button" class="btn btn-outline-danger btn-sm" onclick="$('.cp_close').trigger('click');">Close</button>
							</div>
						</div>
					</div>	

						

				  </form>						
											
			</div>
    	</div>
	</div>
	
 <!-- SideBar Modal -->
 


 <!-- SideBar Modal Single -->

	<div id="myModalSingle" class="chat_pop edit_pop petient chat_pop_appointment">
		<div class="edit_pop_title">
			<h3>Availability Time slot</h3>
			<a class="cp_close normal"><i class="fa fa-times"></i></a>
		</div>
        <div class="chat_block mt-3">
			<div class="container pt-3">
				<div class="row">
					<div class="form-group col-md-12">
						<label for="old_password">Visit Type <span class="text-danger">*</span></label>
						<select name="type_filter" id="type_filter" class="form-control" readonly>
							<option value="">--Select--</option>					
							<option value="2">Virtual Visit</option>
							<option value="3">Concierge Visit</option>
						</select>
						<span class="text-danger">
							<strong class="error" id="type_filter-error"></strong>
						</span>				
					</div>
					<div class="form-group col-md-6">
						<label for="timeSlot1">Start Time <span class="text-danger">*</span></label>
						<div class='input-group date'>
							<input type="text" class="form-control timepicker" id="timeSlot1" autocomplete="off" required>
							<span class="input-group-text" id="basic-addon1">
								<img src="{{asset('assets/images/clock.svg')}}">
							</span>
							<span class="text-danger">
								<strong class="error" id="timeSlot1-error"></strong>
							</span>
						</div>
					</div>
					<div class="form-group col-md-6">
					  <label for="timeSlot2">End Time <span class="text-danger">*</span></label>
					  <div class='input-group date'>
							<input type="text" class="form-control timepicker" id="timeSlot2" autocomplete="off" required>
							<span class="input-group-text" id="basic-addon1">
								<img src="{{asset('assets/images/clock.svg')}}">
							</span>
							<span class="text-danger">
								<strong class="error" id="timeSlot2-error"></strong>
							</span>
						</div>
					</div>
					<div class="form-group col-md-12 location_single" style="display:none;">
						<label>Visit Location <span class="text-danger">*</span></label>
						<textarea id="location_single" class="form-control" cols="30" rows="4" style="width:100%;" placeholder="For all concierge visit timeslots"></textarea>
						<span class="text-danger">
							<strong class="error" id="location_single-error"></strong>
						</span>	
						
					</div>
					
					<div class="form-group col-md-12">	

						<input type="hidden" id="single_appointment_date">
						<input type="hidden" id="timeslot_id">
						<button id="sub_btn" type="button" class="btn btn-outline-primary btn-sm" onclick="saveTime(this);">Save</button>
						<button id="del_btn" type="button" class="btn btn-outline-warning btn-sm" onclick="deleteTimeSlot(this);">Delete</button>
						<button type="button" class="btn btn-outline-danger btn-sm" onclick="$('.cp_close').trigger('click');">Close</button>												
					</div>
						
				</div>							
			</div>
    	</div>
	</div>
	
 <!-- SideBar Modal Single -->


<script>
	$(document).on('change', '#type_filter', function(event){
		let type_filter = $("#type_filter").val();
		if(type_filter==3) { $("#location_div").css("display","block"); }
		else { $("#location_div").css("display","none"); }
	});

	
	var current_time = "{{ date('h:i A') }}";
	var selectedEvent;
	var calendar;
	var calEventID;
	var calEventPrevID;
	var times = [];
	var enable_time = [<?php echo $enable_time;?>]
	var weekend_days = <?php echo $weekend_days;?>

	$(function() { getData(); });	
	
	function getData(){
		$(".loader").css('display', 'flex');
		//destroy 
		if(calendar){
			$("#calendar").fullCalendar('destroy');
		}
		
		//new
		calendar = $('#calendar').fullCalendar({
			
			events: {
				url: "{{ route('provider.availability_timeslots') }}",
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				type: 'POST',
				data: {
					start_date: $("#start_date").val(),
					end_date: $("#end_date").val(),
				},
				error: function() {
					$(".loader").css('display', 'none');									
				}
			},	
			displayEventEnd: true,
			timeFormat: 'h:mm a',
			eventOrder: 'title',
			eventBorderColor: '#1219f2',
			eventTextColor: '#fff',
			eventBackgroundColor: '#64a8fd',
			eventPadding: '2px 5px',
			editable: false,
			locale: 'en',
			
			eventRender: function(event, element) {
				//console.log('event',event);
				$(".loader").css('display', 'none');
				/*if(event.status == 1){
					$(element).css("background-color","#28a745");
				}*/
				if(event.visit_type == 3){
				$(element).css("background-color","#2c99cb");
				}else{
				$(element).css("background-color","#ff6c66");
				}
			},
			
			dayRender: function(date, cell){
				let day = date.format('ddd');

				if($.inArray(day, weekend_days) != -1){
				  $(cell).addClass('disabled');
				}
			},			
			dayClick: function(dateVal, allDay, jsEvent, view) {
				
				
				let dt = moment(dateVal);
				let day = dt.format('ddd');
				
				if($.inArray(day, weekend_days) != -1){
					errorAlert('Error : This is a Weekend Day',2000,'bottom-left');
					return false;
				}
					
				let date = dt.format('Y-MM-DD');
				let date2 = dt.format('MM/DD/YYYY');
				
				$('#myModal .edit_pop_title h3').html('Add Availability Time slot : '+date2);
				$('#appointment_date').val(date);
				
/* 				let today = "{{ date('Y-m-d') }}";
				
				if(date < today){
						return false;
				} */
				
				getTimeSlotByDate(date);				
			},
			eventClick: function(calEvent, jsEvent, view) {	
			
				selectedEvent = $(this);
				getTimeSlotById(calEvent.id);
				
			}			

		});
	}
	
	
	function getTimeSlotByDate(date_val){
		
		$.ajax({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			data: {'date_val':date_val},
			url: "{{ route('provider.getTimeSlotsByDate') }}",
			type: "POST",
			// dataType: 'json',
			success: function (response) {
				
				if(response['status'] == 'success'){
					$("#timeslot-data-div").html(response['data']);
					@include('front.provider.more_add_timeslots')
				}
				
				$('.timepicker').each(function (){
					$(this).datetimepicker({
						showClose: true,
						format : 'LT'
					}).on('dp.show', function (e) { 
						$(this).data("DateTimePicker").enabledHours(enable_time);
					});	
				});
				
				$('input[type=radio][name=setting_filter]').change(function() {
					if (this.value != 6) {
						$('#customMonthArea').hide();	
					}
				});	
				
				//$('#myModal').modal('show');		
				modal_box("timeslot");
			}
		});			
	}
	
	function change_time(e,time){
		
		if($(e).hasClass('active')){
			times = $.grep(times, function(value) {
			  return value != time;
			});
			$(e).removeClass('active');
			console.log(' Remove times',times);			
		}else{
			times.push(time);
			$(e).addClass('active');
			console.log(' Add times',times);			
		}
		
	}
	

	/************* Single Time Slot at once *********/
	
	function getTimeSlotById(id){
		
		$.ajax({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			data: {'id':id},
			url: "{{ route('provider.get_timeslot_by_id') }}",
			type: "POST",
			// dataType: 'json',
			success: function (response) {
				let event_data = response;
				
				//console.log(event_data);
				if(event_data){
					
					let dt = moment(event_data.date_val);
					let date = dt.format('MM/DD/YYYY');
					
					$('#myModalSingle .edit_pop_title h3').html('Availability Time slot : '+date);
					$('#type_filter').val(event_data.visit_type);
					$('#timeSlot1').val(event_data.event_start);
					$('#timeSlot2').val(event_data.event_end);
					
					if(event_data.visit_type == 3){
						$('#location_single').val(event_data.location);
						$('.location_single').show();
					}else{
						$('.location_single').hide();
					}
					$('#single_appointment_date').val(event_data.date_val);
					$('#timeslot_id').val(event_data.id);

					$('.timepicker').each(function (){
						$(this).datetimepicker({
							showClose: true,
							format : 'LT'
						}).on('dp.show', function (e) { 
							$(this).data("DateTimePicker").enabledHours(enable_time);
						});	
					});	
												
					event_dataID = event_data.id;
					
					modal_box("appointment");
					//$('#myModalSingle').modal('show');					
				}
				
			}
		});			
	}
	
	//Update Selected Timeslot
	function saveTime(e){
		
		let id = $("#timeslot_id").val();
		let date_val = $("#single_appointment_date").val();
		
		if(date_val == ''){ 
			return false; 
		}
		
		let s_date = $("#timeSlot1").val();
		let e_date = $("#timeSlot2").val();
		let type_filter = $("#type_filter").val();
		//let location_text = (type_filter==3) ? $("#location").val() : null;
		let location_text = $("#location_single").val();
		
		$(".loader").css('display', 'flex');
		$.ajax({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			data: {'id':id, 'date_val':date_val, 'start_date':s_date, 'end_date':e_date, 'type_filter':type_filter, 'location':location_text},
			url: "{{ route('provider.update_appointment_timeslot_single') }}",
			type: "POST",
			// dataType: 'json',
			success: function (response) {
				$(".loader").css('display', 'none');
				if(response['status'] == 'success'){
					//successAlert(response['message'],2000,'bottom-left');
					callProgressBar();
					var providertime = response['user_data'];
					console.log(providertime);
					$(this).find('.fc-time').html();
					getData();
					setTimeout(function(){
						//$('#myModalSingle').modal('hide');
						$(".cp_close").trigger('click');
						
						//location.reload();							
					}, 2000);
				}else{
					errorAlert(response['message'],3000,'bottom-left');						
				}
			},
			error: function (data) {
				$(".loader").css('display', 'none');
				let errors = data.responseJSON.errors;
				
				$.each(errors, function(key, value) {
					errorAlert('Error Occured.',3000,'bottom-left');
				});					
			}
		});		
	}
	
	//Delete Selected Timeslot
	function deleteTimeSlot(e){
		let id = $("#timeslot_id").val();
		console.log(id);
		
		swal({
			title: "Are you sure to delete this timeslot?",
			text: "",
			icon: '',
			buttons: {
			cancel: true,
			delete: 'Yes'
			}
		}).then((isConfirm) => {
			if (!isConfirm) {
				return false;
			} else {			
				$(".loader").css('display', 'flex');
			
				$.ajax({
					headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					},
					data: {'id':id},
					url: "{{ route('provider.deleteTimeSlot') }}",
					type: "POST",
					// dataType: 'json',
					success: function (response) {
						$(".loader").css('display', 'none');
						
						if(response['status'] == 'success'){
							//successAlert(response['message'],2000,'bottom-left');
							callProgressBar();
							setTimeout(function(){
								selectedEvent.remove();
								//$('#myModalSingle').modal('hide');
								$(".cp_close").trigger('click');
								getData();
							}, 2000);
						}else{
							errorAlert(response['message'],3000,'bottom-left');						
						}
					},
					error: function (data) {
						$(".loader").css('display', 'none');
						errorAlert('Sorry!! you can\'t delete this timeslot.',3000,'bottom-left');						
					}
				});
				
			}
		});	
			
	}
	/*************** Multiple timeslots at once******************/
	
	function getMonthDropdown(e){
		let date = $('#appointment_date').val();
		var months = []
		var monthOptions = '';
		
		var dateStart = moment(date);
		var dateEnd = moment().add(12, 'month');
		
		while (dateEnd.diff(dateStart, 'months') >= 0) {
			let val = dateStart.format('Y-M');
			let label = dateStart.format('MMM Y');
			months.push(dateStart.format('Y-M'))
			dateStart.add(1, 'month');
			monthOptions += '<option value="'+val+'">'+label+'</option>';
		}
		
		
 
		$('#custom_month_dropdown').html(monthOptions);
		$("#custom_month_dropdown").select2({
			 minimumResultsForSearch: -1
		});		
		$('#customMonthArea').show();
	}
	$("#availabilityTimeSlotForm").submit(function(e) {
		e.preventDefault();
		
		$(".loader").css('display', 'flex');
		$.ajax({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			data: $("#availabilityTimeSlotForm").serialize(),
			type: "POST",
			url: "{{ route('provider.add_appointment_timeslots') }}",
			type: "POST",
			// dataType: 'json',			
			success: function (response) {
				$(".loader").css('display', 'none');
				if(response['status'] == 'success'){
					//successAlert(response['message'],2000,'bottom-left');
					callProgressBar();
					getData();
					setTimeout(function(){
						//$('#myModal').modal('hide');
						$(".cp_close").trigger('click');
													
					}, 2000);
				}else{
					errorAlert(response['message'],3000,'bottom-left');						
				}
			},
			error: function (data) {
				$(".loader").css('display', 'none');
				let errors = data.responseJSON.errors;
				
				$.each(errors, function(key, value) {
					errorAlert(value[0],3000,'bottom-left');
				});					
			}
		});		
	});


	@include('front.provider.more_add_timeslots')
	
	$(document).on('click', '.remove_item', function() {
		$(this).closest('.item_wrapper').remove();
	});
				
</script>
@endsection