@extends('front.provider_layout.app')

@section('content')

	<div class="col-xl-10 col-lg-9 col-md-9 col-sm-12">
		<div class="main-center-data">
			<h3 class="display-username">Appointments</h3>
			
			<div class="row mt-3 block-area">
				<div class="col-xl-3 col-lg-3 col-md-3">
					<a class="btn btn-outline-primary" id="add_appointment" href="javascript:void(0)"><i class="fa fa-plus"></i> Add Availability Timeslot</a>			
				</div>			
				
				<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 mt-15">
					<h5>Filter</h5>
					<div class="filters_wrapper">
						<div class="data-list-filters">
							<span class="filter_heading">By Date</span>
							<div class="dates">
								<input type="text" placeholder="Date : From" id="start_date" value="" onchange="getData();">
								<input type="text" placeholder="Date : To" id="end_date" value="" onchange="getData();">
							</div>
							
						</div>
					
						<div class="data-list-filters">
							<span class="filter_heading">By Visit Type</span>
							<select id="by_visit_type" class="custom-select select2" onchange="getData();">
								<option value="">All</option>
								<option value="1">Asynchronous Telemedicine</option>
								<option value="2">Synchronous Telemedicine</option>
								<option value="3">Concierge</option>
							</select>
						</div>

						<div class="data-list-filters">
							<span class="filter_heading">By Service</span>
							<select id="by_service" class="custom-select select2" onchange="getData();">
								<option value="">All</option>
							@if($services && $services->count() > 0)
								@foreach($services as $key => $value)
									<option value="{{ $value->id }}">{{ $value->name }}</option>
								@endforeach
							@endif
							</select>																
						</div>	

						<div class="data-list-filters">
							<span class="filter_heading">By Visit Status</span>
							<select id="by_visit_status" class="custom-select select2" onchange="getData();">
								<option value="">All</option>
								<option value="ACCEPTED">ACCEPTED</option>
								<option value="REVIEWED">REVIEWED</option>
								<option value="SCHEDULED">SCHEDULED</option>
								<option value="COMPLETE">COMPLETE</option>
								<option value="CANCELED">CANCELED</option>
								<option value="REFUND">REFUND</option>
								<option value="RESCHEDULED">RESCHEDULED</option>
							</select>
						</div>					
					</div>
				</div>
				
				<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
					<div id='calendar'></div>
				</div>
			</div>
			
			
		</div>
	</div>

 <!-- SideBar Modal -->

	<div id="myModal" class="chat_pop edit_pop petient chat_pop_appointment">
		<div class="edit_pop_title">
			<h3>Appointment Detail</h3>
			<a class="cp_close normal"><i class="fa fa-times"></i></a>
		</div>
        <div class="chat_block">
			<div class="container pt-3">
				<div class="row">
					<div class="form-group col-sm-12">
						<div class="details">
							<span class="detail-heading">Appointment at : </span>
							<span class="appointment-time"></span>
						</div>
					</div>
					<div class="form-group col-sm-12">
						<div class="remarks">
							<textarea id="remarks" class="form-control" placeholder="Remarks(optional)" cols="30" rows="4" style="width:100%;"></textarea>
						</div>
					</div>
					
					<div class="form-group col-sm-12">							
						<button type="button" class="accept_btn btn btn-primary btn-sm" data-id="" onclick="change_appointment_status(this,'accept');" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Processing" title="Accept">Accept</button>
						<button type="button" class="decline_btn btn btn-danger btn-sm" data-id="" onclick="change_appointment_status(this,'decline');" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Processing" title="Decline">Decline</button>
						<button type="button" class="btn btn-outline-primary btn-sm" onclick="$('.cp_close').trigger('click');">Close</button>							
					</div>
						
				</div>							
			</div>
    	</div>
	</div>
	
 <!-- SideBar Modal -->



@push('scripts')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>   
<script>
	
	$(function(){
		$("#start_date").datepicker({
			dateFormat: "{{ env('DATE_FORMAT_JQ') }}",
			changeMonth: true,
			changeYear: true,
			yearRange: "-150:+0",
		});
		
		$("#end_date").datepicker({
			dateFormat: "{{ env('DATE_FORMAT_JQ') }}",
			changeMonth: true,
			changeYear: true,
			yearRange: "-150:+0",
		});
		
	});
	
	$("#by_visit_type, #by_service, #by_visit_status").select2({
		 minimumResultsForSearch: -1
	});
		
	var selectedEvent;
	var calendar;
	var calEventID;
	var calEventPrevID;

	$(function() {
	  	getData();
	});
 
	function getData(){
		$(".loader").css('display', 'flex');
		//destroy 
		if(calendar){
			$("#calendar").fullCalendar('destroy');
		}
		
		var start_date = $("#start_date").val();
		var end_date = $("#end_date").val();
	
		var new_start_date = moment(start_date, "MM/DD/YYYY").format("YYYY-MM-DD");
		var new_end_date = moment(end_date, "MM/DD/YYYY").format("YYYY-MM-DD");
		
		if(start_date==''){
			new_start_date="{{date('Y-m-01')}}";
		}
		if(end_date==''){
			new_end_date="{{date('Y-m-t')}}";
		}
		
		//new
		calendar = $('#calendar').fullCalendar({
			
			events: {
				url: "{{ route('provider.appointments') }}",
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				type: 'POST',
				data: {
					start_date: new_start_date,
					end_date: new_end_date,
					visit_type: $("#by_visit_type").val(),
					visit_status: $("#by_visit_status").val(),
					service: $("#by_service").val(),
				},
				error: function() {
					$(".loader").css('display', 'none');				
				}
			},			
			displayEventEnd: false,
			timeFormat: 'h:mm a',
			eventOrder: 'title',
			eventBorderColor: '#ddd',
			eventTextColor: '#fff',
			eventBackgroundColor: '#ffc107',
			editable: false,
			locale: 'en',
			
			eventRender: function(event, element) {
				//console.log('event',event);
				$(".loader").css('display', 'none');
					$(element).css("padding","5px");
					$(element).css("font-size","13px");				
				if(event.status == 1){
					$(element).css("background-color","#28a745");
				}
			},
					
			eventClick: function(calEvent, jsEvent, view) {	
			
				selectedEvent = $(this);
				getAppointmentData(calEvent.id);
			}				

		});
		
		// $('#calendar').fullCalendar('destroy');
		//$('#calendar').fullCalendar('render');
	}
	
	function change_appointment_status (e,status){
		let event_id = $(e).attr('data-id');
		let remarks = $("#myModal #remarks").val();
		
		//loader start
		$(e).html($(e).attr('data-loading-text'));
				
		$.ajax({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			data: {'event_id':event_id, 'status':status, 'remarks':remarks},
			url: "{{ route('provider.change_appointment_status') }}",
			type: "POST",
			// dataType: 'json',
			success: function (response) {
				//loader end
				$(e).html($(e).attr('title'));
				
				if(response['status'] == 'success'){
					//$('#myModal').modal('hide');
					$(".cp_close").trigger('click');
					//successAlert(response['message'],2000,'top-right');
					callProgressBar();
					calEventPrevID = event_id;
					$('#myModal .accept_btn').hide();
					$('#myModal .decline_btn').hide();
					
					if(status == 'decline'){
						selectedEvent.remove();
					}else{
						selectedEvent.css("background-color", "#28a745");
					}
				}else{
					errorAlert('Error occured.',3000,'top-right');						
				}
			},
			error: function (data) {
				//loader end
				$(e).html($(e).attr('title'));
				let errors = data.responseJSON.errors;
				
				$.each(errors, function(key, value) {
					errorAlert('Error Occured.',3000,'top-right');
				});					
			}
		});		
	}
  
	function getAppointmentData(id){
		
		$.ajax({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			data: {'id':id},
			url: "{{ route('provider.get_appointment_by_id') }}",
			type: "POST",
			// dataType: 'json',
			success: function (response) {
				let event_data = response;
				
				console.log('title',event_data.title);
				if(event_data){
					
					let dt = moment(event_data.start);
					let date = dt.format('MM/DD/YYYY h:mm A');
					
					let dt2 = moment(event_data.end);
					let date2 = dt2.format('h:mm A');
					
					$('#myModal .edit_pop_title h3').html(event_data.title);
					$('#myModal span.appointment-time').html(date+"-"+date2);
					$('#myModal .remarks textarea').val(event_data.remarks);
					//$('#myModal .modal-body span.appointment-detail').html(event_data.service+' ('+event_data.visit_type+')');
						
					event_dataID = event_data.id;
					
					if(event_data.status == 0){
						$('#myModal .accept_btn').attr('data-id',event_data.id);
						$('#myModal .decline_btn').attr('data-id',event_data.id);
						
						if(calEventID != calEventPrevID){
							$('#myModal .accept_btn').show();
							$('#myModal .decline_btn').show();							
						}
					
					}else{
						$('#myModal .accept_btn').hide();
						$('#myModal .decline_btn').hide();						
					}
					modal_box("appointment");
					//$('#myModal').modal('show');					
				}
				
			}
		});			
	}
	

	$(document).on('click','#add_appointment',function(e){    
		e.preventDefault();
		
		$('#verifyBtn').html($('#verifyBtn').attr('data-loading-text'));
	    $('#verifyBtn').addClass('disabled');
	    //$(".loader").css('display', 'flex');
		var id="{{ auth()->user()->id }}";
		$.ajax({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			data: {'id':id },
			url: "{{ route('provider.check_appointment') }}",
			type: "POST",
			success: function (response) {
				if(response['status'] == 'success'){
						window.location = response['redirect'];
				}
				else{
					errorAlert("Provider can't add appointment without an office start time and end time.",3000,'top-right');						
				}
				$('#verifyBtn').removeClass('disabled');
				$('#verifyBtn').html('Verify');
				//$(".loader").css('display', 'none');
			},
			error: function (data) {
				//$(".loader").css('display', 'none');
				$('#verifyBtn').removeClass('disabled');
				$('#verifyBtn').html('Verify');
				let errors = data.responseJSON.errors;
				
				$.each(errors, function(key, value) {
					errorAlert(value[0],3000,'top-right');
				});					
				
			}
		});
	});
  
</script>
@endpush

@endsection