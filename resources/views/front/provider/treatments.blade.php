@extends('front.dashboard_layout.app')

@section('content')
<style>
.chat_pop{width:100%;max-width:600px;z-index:2;position:fixed;background:#f1f8f9;color:#000;right:-100%;bottom:0;height:calc(100vh - 80px);box-shadow:0 10px 30px 0 rgba(0,0,0,.16);z-index: 10;}
.cp_close{width:40px;height:48px;font-size:14px;position:absolute;left:0;top:0;display:flex;justify-content:center;align-items:center;cursor:pointer;border-right:1px solid #dedede}

iframe.chat_frame {
    height: calc(100vh - 80px);
    width: 100%;
    border: none;
}
.chat_pop {
    z-index: 1031;
	top:80px;
}
@media screen and (max-width: 962px){
.chat_pop {
    z-index: 1031;
	top :0;
}
iframe.chat_frame {
    height: 100vh ;
    width: 100%;
    border: none;
}
}
</style>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script>
	$(function(){
		$("#start_date").datepicker({
			dateFormat: "mm/dd/yy",
			changeMonth: true,
			changeYear: true,
			yearRange: "-150:+0",
		});
		
		$("#end_date").datepicker({
			dateFormat: "mm/dd/yy",
			changeMonth: true,
			changeYear: true,
			yearRange: "-150:+0",
		});
	});
</script>
    <div class="chat_pop petient">
		<a class="cp_close"><i class="fa fa-times"></i></a>
        <div class="chat_block">
			
        </div>
    </div>
	
 <!-- Modal -->
<div id="myModal" class="modal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="min-width: 600px;">
      <div class="modal-header">
        <h5 class="modal-title">Video/Audio Call</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
		
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-primary btn-sm" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
	
	<div>
		<div class="main-center-data">
			<h3 class="display-username">My Treatments</h3>
			<div class="row mt-15">
				<div class="col-xl-12 col-lg-12 col-md-12">
					<h5>Filter</h5>
					<div class="filters_wrapper">
						<div class="data-list-filters">
							<span class="filter_heading">By Date</span>
							<div class="dates">
								<input type="text" placeholder="Date : From" id="start_date" value="" onchange="getData();">
								<input type="text" placeholder="Date : To" id="end_date" value="" onchange="getData();">
							</div>
							
						</div>
					
						<div class="data-list-filters">
							<span class="filter_heading">By Visit Type</span>
							<select id="by_visit_type" class="custom-select select2" onchange="getData();">
								<option value="">All</option>
								<option value="1">Asynchronous Telemedicine</option>
								<option value="2">Synchronous Telemedicine</option>
								<option value="3">Concierge</option>
							</select>
						</div>

						<div class="data-list-filters">
							<span class="filter_heading">By Service</span>
							<select id="by_service" class="custom-select select2" onchange="getData();">
								<option value="">All</option>
							@if($services && $services->count() > 0)
								@foreach($services as $key => $value)
									<option value="{{ $value->id }}">{{ $value->name }}</option>
								@endforeach
							@endif
							</select>																
						</div>	

						<div class="data-list-filters">
							<span class="filter_heading">By Visit Status</span>
							<select id="by_visit_status" class="custom-select select2" onchange="getData();">
								<option value="">All</option>
								<option value="PENDING ACCEPTANCE">PENDING</option>
								<option value="ACCEPTED">ACCEPTED</option>
								<option value="REVIEWED">REVIEWED</option>
								<option value="SCHEDULED">SCHEDULED</option>
								<option value="COMPLETE">COMPLETE</option>
								<option value="CANCELED">CANCELED</option>
								<option value="REFUND">REFUND</option>
								<option value="RESCHEDULED">RESCHEDULED</option>
							</select>
						</div>					
					</div>
				</div>
			</div>
			<div id="patients_data" class="row">
			</div>

		</div>
	</div>

	 <!-- Modal -->
	<div id="feedbackModal" class="modal" tabindex="-1" role="dialog">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<h5 class="modal-title">Feedback and Rating to Provider</h5>
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			  <span aria-hidden="true">&times;</span>
			</button>
		  </div>
		  <form id="feedbackForm">
		  <div class="modal-body">

			  <div class="form-group has-feedback">
				<label for="feedback">Feedback</label>
				<textarea name="feedback" class="form-control" id="feedback" placeholder="Feedback"></textarea>
				<span class="text-danger">
					<strong class="error" id="feedback-error"></strong>
				</span>				
			  </div>
			  
			  <div class="form-group has-feedback">
				<label class="rating_label" for="rating">Star Rating</label>
				<fieldset class="rate">
				
					<input type="radio" id="rating10" name="rating" value="5" />
					<label for="rating10" title="5"></label>
					
					<input type="radio" id="rating9" name="rating" value="4.5" />
					<label class="half" for="rating9" title="4.5"></label>
					
					<input type="radio" id="rating8" name="rating" value="4" />
					<label for="rating8" title="4"></label>
					
					<input type="radio" id="rating7" name="rating" value="3.5" />
					<label class="half" for="rating7" title="3.5"></label>
					
					<input type="radio" id="rating6" name="rating" value="3" />
					<label for="rating6" title="3"></label>
					
					<input type="radio" id="rating5" name="rating" value="2.5" />
					<label class="half" for="rating5" title="2.5"></label>
					
					<input type="radio" id="rating4" name="rating" value="2" />
					<label for="rating4" title="2"></label>
					
					<input type="radio" id="rating3" name="rating" value="1.5" />
					<label class="half" for="rating3" title="1.5"></label>
					
					<input type="radio" id="rating2" name="rating" value="1" />
					<label for="rating2" title="1"></label>
					
					<input type="radio" id="rating1" name="rating" value="0.5" />
					<label class="half" for="rating1" title="0.5"></label>		
					
				</fieldset>
				<span class="text-danger">
					<strong class="error" id="rating-error"></strong>
				</span>					
			  </div>
		  
			
		  </div>
		  <div class="modal-footer">
			<input type="hidden" name="provider_id" value="" id="feedback_provider_id">
			<input type="hidden" name="service_id" value="" id="feedback_service_id">		  
			<input type="hidden" name="online_visit_id" value="" id="feedback_online_visit_id">		  
			<button type="submit" id="saveBtn" class="accept_btn btn btn-primary" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Processing" >Submit</button>
			<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
		  </div>
		  </form>
		</div>
	  </div>
	</div>
	
<script>

	var selFdBtn;
	$("#by_visit_type, #by_service, #by_visit_status").select2({
		 minimumResultsForSearch: -1
	});
	
	$(function() {
	  	getData('');
	});
	
	function getData(page){
		
		var start_date = $("#start_date").val();
		var end_date = $("#end_date").val();
	
		var new_start_date = moment(start_date, "MM/DD/YYYY").format("YYYY-MM-DD");
		var new_end_date = moment(end_date, "MM/DD/YYYY").format("YYYY-MM-DD");
		
		if(start_date==''){
			new_start_date='';
		}
		if(end_date==''){
			new_end_date='';
		}
		
		if($("#start_date").val() !='' &&  $("#end_date").val() != ''){
			
			if($("#start_date").val() > $("#end_date").val()){
				errorAlert('Start date must not be greater than End date.',2000,'top-right');
				return false;
			}
		
		}
		
		var req_data = {
			'start_date': new_start_date,
			'end_date': new_end_date,
			'visit_type': $("#by_visit_type").val(),
			'visit_status': $("#by_visit_status").val(),
			'service': $("#by_service").val()
		};
		
		$(".loader").css('display', 'flex');
		
		var url;
		
		if(page){
			url = "{{ route('user.treatments') }}?page="+page;
		}else{
			url = "{{ route('user.treatments') }}";
		}
		
		$.ajax({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			data: req_data,
			url: url,
			type: "POST",
			// dataType: 'json',
			success: function (response) {
				$(".loader").css('display', 'none');
				if(response['status'] == 'success'){
					$('#patients_data').html(response['data']);
				}else{
					errorAlert(response['message'],2000,'top-right');
					$('#patients_data').html(response['data']);
				}
			},
			error: function (data) {
				$(".loader").css('display', 'none');
				let errors = data.responseJSON.errors;
				
				$.each(errors, function(key, value) {
					errorAlert(value[0],3000,'top-right');					
				});					
	
			}
		});		
	}
	
	function video_call(e,online_visit_id,provider_id,paywall_feature=''){
		$(".loader").css('display', 'flex');
		$.ajax({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			data: {'online_visit_id':online_visit_id,'provider_id':provider_id,'paywall_feature':paywall_feature},
			url: "{{ route('user.check_video_call') }}",
			type: "POST",
			// dataType: 'json',
			success: function (response) {
				
				if(response['status'] == 'success'){
					//console.log('url',response['redirect']);
					 setTimeout(function(){
						$(".loader").css('display', 'none');
						if(response['type'] == 'payment'){
							window.location = response['redirect']; //payment page
						}else{
							video_box(response['redirect'],paywall_feature);
							//window.open(response['redirect'],'_blank'); //video call page
						}
						
						
					}, 500);
				}else{
					$(".loader").css('display', 'none');
					errorAlert(response['message'],3000,'top-right');						
				}
			}
		});		
	}
	
	$(document).on('click', '.custom-pagination ul.pagination li.page-item a.page-link', function(event){
		event.preventDefault(); 
		var page = $(this).attr('href').split('page=')[1];
	  
		getData(page);//call the function
	 });	
	 
 
	function feedback_modal(e,pid,sid,oid){
		$("#feedbackForm").trigger('reset');
		$("#feedback_provider_id").val(pid);
		$("#feedback_service_id").val(sid);
		$("#feedback_online_visit_id").val(oid);
		$("#feedbackForm span.text-danger .error").html('');
		$("#feedbackModal").modal('show');
		selFdBtn = $(e);
	}
	
	$("#feedbackForm").submit(function(e) {
	e.preventDefault();
	
	//loader start
	$('#saveBtn').html($('#saveBtn').attr('data-loading-text'));
	
	$("#feedbackForm span.text-danger .error").html('');

	$.ajax({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		},
		data: $('#feedbackForm').serialize(),
		url: "{{ route('user.provider_feedback') }}",
		type: "POST",
		// dataType: 'json',
		success: function (response) {
			$('#saveBtn').html('Submit');
			if(response['status'] == 'success'){
				
				$("#feedbackModal").modal('hide');
				selFdBtn.remove();
				
				//successAlert(response['message'],2000,'top-right');
				callProgressBar();
				setTimeout(function(){
					$('#feedbackForm').trigger("reset");
					//location.reload();
				}, 2000);
			}
		},
		error: function (data) {
			$('#saveBtn').html('Submit');
			
			if(data.responseJSON.errors) {
				let errors = data.responseJSON.errors;
				if(errors.feedback){
					$( '#feedback-error' ).html( errors.feedback[0] );
				}
				
				if(errors.rating){
					$( '#rating-error' ).html( errors.rating[0] );
				}
				
			}
		
		}
	});
});

	//Chat Box
	$(document).ready(function(){
		$('.cp_close').click(function(){
			var hidden = $('.chat_pop');
			hidden.animate({"right":"-100%"}, "slow").removeClass('visible');
		});
	});

	
	function chat_box(userid){
		let url = "{{ env('APP_URL') }}/mychat/"+userid;
		let html_val = "<iframe class='chat_frame' src='"+url+"' ></iframe>";
		
		$('.chat_block').html(html_val);
		
		var hidden = $('.chat_pop');
		if (hidden.hasClass('visible')){
			hidden.animate({"right":"-100%"}, "slow").removeClass('visible');
		} else {
			hidden.animate({"right":"0"}, "slow").addClass('visible');
		}
	}
	
	function video_box(url,paywall_feature=''){
		let html_val = "<iframe src='"+url+"' style='width:100%;height:100vh; border:none;'></iframe>";
		if(paywall_feature=='chat_feature'){
			$('#myModal .modal-title').text('Chat feature');
		}
		
		$('#myModal .modal-body').html(html_val);
		
		$('#myModal').modal('show');
		
/* 		var hidden = $('.chat_pop');
		if (hidden.hasClass('visible')){
			hidden.animate({"right":"-100%"}, "slow").removeClass('visible');
		} else {
			hidden.animate({"right":"0"}, "slow").addClass('visible');
		} */
	}	
	
	
</script>	
@endsection