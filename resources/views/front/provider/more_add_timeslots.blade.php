/****Add more time slots for virtual visit******/
$('.v_add_item').click(function() {
	$('.v_more_items_wrapper').append(	
		'<div class="form-row v_item_wrapper">'+
		  '<div class="form-group col-md-5 has-feedback">'+
			'<div class="input-group date mt-2">'+
				'<input type="text" class="form-control timepicker" name="slots_data[virtual][start_time][]" value="'+current_time+'"/>'+
				'<span class="input-group-text" id="basic-addon1">'+
					'<img src="{{asset("assets/images/clock.svg")}}">'+
				'</span>'+
			'</div>'+				
		  '</div>'+
		  '<div class="form-group col-md-5 has-feedback">'+
			'<div class="input-group date mt-2">'+
				'<input type="text" class="form-control timepicker" name="slots_data[virtual][end_time][]" value="'+current_time+'"/>'+
				'<span class="input-group-text" id="basic-addon1">'+
					'<img src="{{asset("assets/images/clock.svg")}}">'+
				'</span>'+
			'</div>'+					
		  '</div>'+
		  '<div class="form-group col-md-2 has-feedback text-center mt-3">'+
		  '<input type="hidden" name="slots_data[virtual][id][]" value="">'+
			'<a href="javascript:void(0)" title="Remove" class="v_remove_item"><span style="color:red;"><i class="fa fa-minus-circle" aria-hidden="true"></i></span></a>'+
			'</div>'+
		'</div>'
	);
	
	$('.timepicker').each(function (){
		$(this).datetimepicker({
			showClose: true,
			format : 'LT'
		}).on('dp.show', function (e) { 
			$(this).data("DateTimePicker").enabledHours(enable_time);
		});	
	});
});	

$(document).on('click', '.v_remove_item', function() {
	$(this).closest('.v_item_wrapper').remove();
});	

/****Add more time slots for concierge visit******/
$('.add_item').click(function() {
	$('.more_items_wrapper').append(	
		'<div class="form-row item_wrapper">'+
		  '<div class="form-group col-md-5 has-feedback">'+
			'<div class="input-group date mt-2">'+
				'<input type="text" class="form-control timepicker" name="slots_data[concierge][start_time][]" value="'+current_time+'"/>'+
				'<span class="input-group-text" id="basic-addon1">'+
					'<img src="{{asset("assets/images/clock.svg")}}">'+
				'</span>'+
			'</div>'+				
		  '</div>'+
		  '<div class="form-group col-md-5 has-feedback">'+
			'<div class="input-group date mt-2">'+
				'<input type="text" class="form-control timepicker" name="slots_data[concierge][end_time][]" value="'+current_time+'"/>'+
				'<span class="input-group-text" id="basic-addon1">'+
					'<img src="{{asset("assets/images/clock.svg")}}">'+
				'</span>'+
			'</div>'+					
		  '</div>'+
		  '<div class="form-group col-md-2 has-feedback text-center mt-3">'+
		  '<input type="hidden" name="slots_data[concierge][id][]" value="">'+
			'<a href="javascript:void(0)" title="Remove" class="remove_item"><span style="color:red;"><i class="fa fa-minus-circle" aria-hidden="true"></i></span></a>'+
			'</div>'+
		'</div>'
	);

	$('.timepicker').each(function (){
		$(this).datetimepicker({
			showClose: true,
			format : 'LT'
		}).on('dp.show', function (e) { 
			$(this).data("DateTimePicker").enabledHours(enable_time);
		});	
	});

});	

$(document).on('click', '.remove_item', function() {
	$(this).closest('.item_wrapper').remove();
});