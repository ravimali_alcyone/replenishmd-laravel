@extends('front.provider_layout.app')

@section('content')
<!-- Style.css -->
<style>
.intro_step_ques_box{text-align:center}
.ques_box .carousel{height:100vh;display:flex;flex-direction:column;align-items:center;justify-content:center;padding:50px 0}
.ques_box .carousel-indicators{top:50px;bottom:auto}
.intro_step_ques_box .carousel-indicators li{opacity:1;background-color:#ddd}
.intro_step_ques_box .carousel-indicators li.active{opacity:1;background-color:#ff6c66}
.carousel-item .content{height:100%}
.intro_ques_box{text-align:center;padding-top:10%;margin:0 auto;display:flex;justify-content:flex-start;flex-direction:column;align-items:center;height:100%}
.intro_ques_box>div{width:100%}
.qus_wel .intro_stps_btn_grp{justify-content:center}
.intro_stps_btn_grp{display:flex;justify-content:center;width:100%;max-width:400px;align-items:center;margin:25px auto;flex-wrap:wrap}
.intro_stps_btn_grp .btn{height:50px;display:flex;justify-content:center;align-items:center;width:100%;max-width:160px;border-radius:4px;margin:10px}
.intro_stps_btn_grp .btn:hover{box-shadow:1px 0 4px rgba(0,2,4,.055),0 7px 18px rgba(1,1,1,.05)}
.intro_stps_btn_grp .con_btn{background-color:#2c99cb;color:#fff}
.intro_stps_btn_grp .con_btn:hover{background-color:#ff6c66;border-color:#eb5752}
.intro_stps_btn_grp .skip_btn{border-color:#2c99cb;color:#2c99cb}
.intro_stps_btn_grp .skip_btn:hover{background-color:#fff;border-color:#f1f8f9;color:#333}
.intro_gra_img{margin-bottom:50px}
.intro_ques_box h5{margin:15px auto;line-height:1.5;font-weight:700}
.intro_step_wrap .intro_modal{background-color:rgba(255,255,255,.98)}
.intro_step_wrap .intro_modal .modal-dialog{height:100vh;margin:0 auto;padding:0;display:flex;justify-content:center;flex-direction:column;max-width:inherit}
.intro_step_wrap .intro_modal .modal-content{background-color:transparent;border:none;height:100vh;padding:15px;position:relative}
.intro_step_wrap .intro_modal .intro_model_wrap{max-width:500px;margin:40px auto 15px auto;text-align:left;width:100%}
.intro_model_wrap .form-control{background-color:#fff;border-color:#dedede;height:50px}
.intro_model_wrap textarea.form-control{height:auto}
.intro_model_wrap .form-control:focus{box-shadow:1px 0 4px rgba(0,2,4,.055),0 7px 18px rgba(1,1,1,.05)}
.intro_model_wrap .form-group{margin-bottom:.75rem}
.intro_model_wrap label{display:inline-block;margin-bottom:.25rem;font-size:14px;color:#555}
.intro_model_wrap .modal-title{margin-bottom:1.5rem;line-height:1.5;font-size:1.5rem;font-weight:700}
.intro_model_wrap .intro_stps_btn_grp{display:flex;justify-content:flex-start;max-width:initial;align-items:center;flex-wrap:wrap;padding:0;margin:0 .5rem;border-top:none}
.intro_modal button.close{position:absolute;right:15px;top:15px;width:40px;height:40px;border-radius:50%;border:none;background-color:#eee}
.intro_address_box{box-shadow:0 0 20px 0 rgba(50,56,66,.1);padding:15px 20px;background-color:#fff;border-radius:4px}
.intro_ques_box .form-control{background-color:#fff;border-color:#dedede;height:50px}
.intro_ques_box .intro_loc{background-image:url('/assets/images/map_marker.svg');background-repeat:no-repeat;text-indent:25px;background-position:11px}
.intro_ques_box textarea.form-control{height:auto}
.intro_ques_box .form-control:focus{box-shadow:1px 0 4px rgba(0,2,4,.055),0 7px 18px rgba(1,1,1,.05)}
.intro_ques_box .form-group{margin:0 auto .75rem auto;max-width:480px}
.intro_step_wrap #settingModal .modal-dialog{max-width:inherit;margin:0 auto}
.intro_step_wrap #settingModal .intro_model_wrap{max-width:670px}
.days_select{margin:0;padding:0}
.days_select li{display:flex;width:45px;height:45px;border-radius:4px;background-color:#fff;border:1px solid #dedede;justify-content:center;align-items:center}
.days_select li:hover{background-color:#ff6c66;border-color:#ff6c66;color:#fff;box-shadow:1px 0 4px rgba(0,2,4,.055),0 7px 18px rgba(1,1,1,.05);cursor:pointer}
.days_select{display:flex;justify-content:center;flex-wrap:wrap;gap:4px;margin-top:5px}
.weekend_days{text-align:left;max-width:340px;margin:0 auto}
.select_weeke_day{background-color: #ff6c66 !important;border-color: #ff6c66 !important;color: #fff;box-shadow: 1px 0 4px rgb(0 2 4 / 6%), 0 7px 18px rgb(1 1 1 / 5%);cursor: pointer;}
#carouselExampleIndicators{height:100% !important;}
.page_before_dashboard{width:100% !important;}
.custom-navbar{display: none;}
@media screen and ( max-width: 767px) {
    .intro_ques_box{ margin: 0 15px; justify-content: center;}
}
@media screen and ( max-width: 400px) {
    .intro_stps_btn_grp .btn{ margin: 4px;}
    .intro_model_wrap .modal-body{ padding: 0;}
    .intro_model_wrap .intro_stps_btn_grp{ margin: 0 auto;}
}
@media screen and ( max-width: 400px) {
    .intro_stps_btn_grp .btn{ margin: 4px 0; max-width: inherit;}
}

	.pac-container {
        z-index: 10000 !important;
    }
	.pac-container:after {
		background-image: none !important;
		height: 0px;
	}
</style>

<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script
      src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places&key={{ env('Google_Map_Keys')}}"
      async
    ></script> 
    <!-- Main Section Side Bar -->
<?php //print_r($office_start_end_time); die(); ?>
    <section class="page_before_dashboard">
        <div class="container-fluid">
            <div class="row">
                <div class="col">
                    <div class="intro_step_wrap">
                        <div class="intro_step_ques_box ">
                            <div class="ques_slider">
                                <div class="ques_box">
                                    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel"
                                        data-interval="false">
                                        <ol class="carousel-indicators">
                                            <li data-target="#carouselExampleIndicators" data-slide-to="0"
                                                class="active"></li>
                                            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                                            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                                            <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
                                        </ol>
                                        <div class="carousel-inner">
                                            <div class="carousel-item active">
                                                <div class="content">
                                                    <div class="intro_ques_box">
                                                        <div class="qus_wel">
                                                            <a class="rmd_logo" href="javascript:void(0)">
                                                                <img src="{{asset('assets/images/rmd_logo_red.svg')}}"
                                                                    class="logo logo_red" alt="ReplenishMD" />
                                                            </a>
    
                                                            <h5>Welcomes you, {{auth()->user()->name}}.</h5>
                                                            <div class="intro_stps_btn_grp">
                                                                <a class="btn con_btn" href="#carouselExampleIndicators"
                                                                role="button" data-slide="next">
                                                                <span>Let's Get Started!</span></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="carousel-item">
                                                <div class="content">
                                                    <div class="intro_ques_box">
                                                        <div class="qus_one">
                                                            <div class="intro_gra_img"><img src="{{asset('assets/images/gra2.svg')}}" alt=""></div>
                                                            <h5 >Hi, {{auth()->user()->name}},<br/> Do you have a preferred location for concierge visits ?</h5>
                                                            <div class="form-group">
                                                                <input type="text" class="form-control intro_loc" placeholder="Location" data-toggle="modal" data-target="#preferedLocationModal">
                                                            </div>
                                                            <div class="intro_stps_btn_grp">
                                                                <button type="button" class="con_btn btn" data-toggle="modal" data-target="#preferedLocationModal" >Continue</button>
                                                                <a class="btn skip_btn" href="#carouselExampleIndicators"
                                                                role="button" data-slide="next">
                                                                <span>Skip</span></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="carousel-item">
                                                <div class="content">
                                                    <div class="intro_ques_box">
                                                        <div class="qus_one">
                                                            <div class="intro_gra_img"><img src="{{asset('assets/images/gra3.svg')}}" alt=""></div>
                                                            <h5 >Set your office open & closing times.</h5>
                                                            <div class="intro_stps_btn_grp">
                                                                <button type="button" class="con_btn btn" onclick="setting_modal(this);">Add Timings</button>
                                                                <a class="btn skip_btn" href="#carouselExampleIndicators"
                                                                role="button" data-slide="next">
                                                                <span>Skip</span></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="carousel-item">
                                                <div class="content">
                                                    <div class="intro_ques_box">
                                                        <div class="qus_one">
                                                            <div class="intro_gra_img"><img src="{{asset('assets/images/gra4.svg')}}" alt=""></div>
                                                            <h5>Set your typical days off.</h5>
															<form id="weekendForm">
																<div class="weekend_days">
																	<strong>Select days</strong>
																	
																	<ul class="days_select">
																		<li data-week="Sun" class="week_day">S <input name="weekend_day[]" type="hidden" value=""></li>
																		
																		<li data-week="Mon" class="week_day">M <input name="weekend_day[]" type="hidden" value=""></li>
																		
																		<li data-week="Tue" class="week_day">T<input name="weekend_day[]" type="hidden" value=""></li>
																		
																		<li data-week="Wed" class="week_day">W<input name="weekend_day[]" type="hidden" value=""></li>
																		
																		<li data-week="Thu" class="week_day">T<input name="weekend_day[]" type="hidden" value=""></li>
																		
																		<li data-week="Fri" class="week_day">F<input name="weekend_day[]" type="hidden" value=""></li>
																		
																		<li data-week="Sat" class="week_day">S<input name="weekend_day[]" type="hidden" value=""></li>
																	</ul>
																</div>
																<div class="intro_stps_btn_grp">
																	<button type="submit" class="con_btn btn">Save</button>
																	<button type="button" class="skip_btn skip_btn_last btn">Skip</button>
																</div>
															</form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--Modal-->
                            <div id="passModal" class="intro_modal modal" tabindex="-1" role="dialog">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                       <div class="intro_model_wrap">
                                        <form id="changePassForm">
                                            <div class="modal-body">
                                                <h5 class="modal-title">Change Password</h5>
                                                <div class="form-group has-feedback">
                                                    <label for="old_password">Old Password <span
                                                            class="text-danger">*</span></label>
                                                    <input type="password" name="old_password" class="form-control"
                                                        id="old_password" >
                                                    <span class="text-danger">
                                                        <strong class="error" id="old_password-error"></strong>
                                                    </span>
                                                </div>

                                                <div class="form-group has-feedback">
                                                    <label for="new_password">New Password <span
                                                            class="text-danger">*</span></label>
                                                    <input type="password" name="password" class="form-control"
                                                        id="new_password" >
                                                    <span class="text-danger">
                                                        <strong class="error" id="new_password-error"></strong>
                                                    </span>
                                                </div>

                                                <div class="form-group has-feedback">
                                                    <label for="confirm_password">Confirm New Password <span
                                                            class="text-danger">*</span></label>
                                                    <input type="password" name="password_confirmation"
                                                        class="form-control" id="confirm_password">
                                                    <span class="text-danger">
                                                        <strong class="error" id="confirm_password-error"></strong>
                                                    </span>
                                                </div>

                                            </div>
                                            <div class="modal-footer intro_stps_btn_grp">
                                                <button type="submit" id="saveBtn"
                                                    class="con_btn btn"
                                                    data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Processing">Save</button>
                                                <button type="button" class="btn skip_btn"
                                                    data-dismiss="modal">Cancel</button>
                                            </div>
                                        </form>
                                       </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!--Edit Setting-->
                        <div id="settingModal" class="intro_modal modal" tabindex="-1" role="dialog">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                   <div class="intro_model_wrap">
                                    
                                    <form id="settingForm">
                                        <div class="modal-body">
                                            <h5 class="modal-title">Set Timings</h5>
                                            <div class="rmd_office_start_end_time">
							
											</div>
                                            <div class="more_break_wrapper"></div>

                                        </div>
                                        <div class="modal-footer intro_stps_btn_grp">
                                            <button type="submit" id="saveBtn3"
                                                class="con_btn btn"
                                                data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Processing">Save</button>
                                            <button type="button" class="btn skip_btn"
                                                data-dismiss="modal">Cancel</button>
                                        </div>
                                    </form>
                                   </div>
                                </div>
                            </div>
                        </div>
                        <!--End here-->

                        <!---Preferred location--->
                        <div id="preferedLocationModal" class="intro_modal modal" tabindex="-1" role="dialog">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                   <div class="intro_model_wrap">
                                    
                                    <form id="locationForm">
                                        <div class="modal-body">
                                            <h5 class="modal-title">Preferred location</h5>
											@if($provider_location && $provider_location->count()>0)
												@php $i=0; $a=1; @endphp
												@foreach($provider_location as $val)
													<div style="border:2px dotted #c8c4d4;padding:10px;border-radius: 5px;" class="form-group position-relative item_wrapper location_box">
														<a href="javascript:void(0)" class="cls_btn remove_item" id="{{$val->id}}">x</a>
														<div class="form-group has-feedback">
															<label>Address Type</label>
															<input type="text" name="add_type[]" class="form-control" value="{{$val->address_type}}">
														</div>
														<div class="has-feedback">
															<label for="Address">Address</label>
															<input name="address[]" id="address_{{$a}}" value="{{$val->address}}" class="form-control">
														</div>
														<input type="hidden" name="location_id[]" id="" value="{{$val->id}}">
													</div>
													
												@php $i++; $a++; @endphp
												@endforeach
											@else
												<div class="intro_address_box item_wrapper location_box">
													<div class="form-group has-feedback">
														<label>Address Type</label>
														<input type="text" name="add_type[]" class="form-control" id="">
													</div>
													<div class="has-feedback">
														<label for="Address">Address</label>
														<!--<textarea name="address[]" rows="2" id="address_textt" class="form-control"></textarea>-->
														<input type="text" name="address[]" rows="2" id="address_1" class="form-control">
													</div>
													<input type="hidden" name="location_id[]" id="" value="">
												</div>
											@endif
                                            <div class="form-group more_location_wrapper"></div>
                                            <div class="has-feedback">
                                                <button type="button"
                                                    class="btn btn-outline-secondary btn-sm add_location">Add more</button>
                                            </div>
                                        </div>
                                        
                                        <div class="modal-footer intro_stps_btn_grp">
                                            <button type="submit" id="saveBtn"
                                                class="con_btn btn"
                                                data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Processing">Save</button>
                                            <button type="button" class="btn skip_btn"
                                                data-dismiss="modal">Cancel</button>
                                        </div>
                                    </form>
                                   </div>
                                </div>
                            </div>
                        </div>
                        <!----End here------>

                        <!---Weekend day setting--->
                        <div id="weekendModal" class="modal" tabindex="-1" role="dialog">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title">Weekend days setting</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <form id="weekendForm_old">
                                        <div class="modal-body">
                                            <div class="form-group has-feedback">
                                                <label for="old_password">Select day <span
                                                        class="text-danger">*</span></label>
                                                <select name="weekend_day[]" class="custom-select select2 form-control"
                                                    id="weekend_day" multiple="multiple">
                                                    <option value="Mon">Mon</option>
                                                    <option value="Tue">Tue</option>
                                                    <option value="Wed">Wed</option>
                                                    <option value="Thu">Thu</option>
                                                    <option value="Fri">Fri</option>
                                                    <option value="Sat">Sat</option>
                                                    <option value="Sun">Sun</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="submit" id="saveBtn"
                                                class="accept_btn btn btn-outline-primary btn-sm"
                                                data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Processing">Submit</button>
                                            <button type="button" class="btn btn btn-outline-danger btn-sm"
                                                data-dismiss="modal">Cancel</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!----End here---->

    <script>
    
    		    function get_places(serchinput){  
        			
        			var autocomplete;
        			
        			autocomplete = new google.maps.places.Autocomplete((document.getElementById(serchinput)), {
        				types : ['geocode'],
        				//componentRestrictions : {
        				//	country: "USA"
        				//}
        			});
        			
        			google.maps.event.addListener(autocomplete, 'places_changed', function (){
        				var near_place = autocomplete.getPlace();
        			});
    			
    		    }
    		  
    		
            $(document).ready(function() {
                
                  var location_box = $('.location_box').length;
		          get_places('address_1');
		          
        		  if(location_box > 1){
        		      //console.log(location_box);
        		      for (let i = 1; i <= location_box; i++) {
                           get_places('address_'+i);
                         }
                        
        		  }
            		
            	$(document).on("click",".week_day",function() {
            		if($(this).hasClass('select_weeke_day')){
            			$(this).removeClass('select_weeke_day');
            			$(this).children('input').val('');
            		}else{
            			
            			$(this).addClass('select_weeke_day');
            			$(this).children('input').val($(this).attr('data-week'));
            		}
            	});
            
            		
            });
                            $(function () {
                                $('.timepicker').datetimepicker({
                                    format: 'LT'
                                });

                                $("#weekend_day").select2({
                                    minimumResultsForSearch: -1
                                });
                            });

                            /**********Reset Password*********/
                            function pass_modal() {
                                $("#passModal").modal('show');
                                $("#changePassForm").trigger('reset');
                                $("#changePassForm span.text-danger .error").html('');
                            }

                            $("#changePassForm").submit(function (e) {
                                e.preventDefault();
                                $('#saveBtn').html($('#saveBtn').attr('data-loading-text'));
                                $("#changePassForm span.text-danger .error").html('');

                                $.ajax({
                                    headers: {
                                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                    },
                                    data: $('#changePassForm').serialize(),
									url: "{{ route('provider.change_password') }}",
                                    type: "POST",
                                    success: function (response) {
                                        $('#saveBtn').html('Submit');
                                        if (response['status'] == 'success') {
											
                                            $("#passModal").modal('hide');
                                            //successAlert(response['message'], 2000, 'top-right');
                                            $('#changePassForm').trigger("reset");
                                        }
                                    },
                                    error: function (data) {
                                        $('#saveBtn').html('Submit');

                                        if (data.responseJSON.errors) {
                                            let errors = data.responseJSON.errors;
                                            if (errors.old_password) {
                                                $('#old_password-error').html(errors.old_password[0]);
                                            }

                                            if (errors.password) {
                                                $('#new_password-error').html(errors.password[0]);
                                            }

                                            if (errors.password_confirmation) {
                                                $('#confirm_password-error').html(errors.password_confirmation[0]);
                                            }

                                        }

                                    }
                                });
                            });

                            /****End Here*****/


                            function setting_modal() {
                                
                                $("#settingForm span.text-danger .error").html('');
                                $(".loader").css('display', 'flex');
                                $.ajax({
                                    headers: {
                                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                    },
                                    data:{'type':'preference'},
									url: "{{ route('provider.get_timings') }}",
                                    type: "POST",
                                    success: function (response) {
                                
                                        if (response['status'] == 'success') {
                                            $(".loader").css('display', 'none');
                                            $("#settingModal").modal('show');
											$('.rmd_office_start_end_time').html(response['rmd_office_start_html']);
											$('.timepicker').datetimepicker({ format: 'LT' });
                                        }
                                    },
                                    error: function (data) {
                                        $(".loader").css('display', 'none');
                                        if (data.responseJSON.errors) {
                                            let errors = data.responseJSON.errors;
                                        }

                                    }
                                });
                                
                            }

                            var currenttime = "{{ date('h:i A') }}";
                            /****Add more office time and break time******/
							var list='';
                            
							$(document).on('click', '.add_item', function () {
								var newel = $('.loc_opt:last').clone();  // Create clone of location dropdown
								//console.log(newel);
								$('.more_items_wrapper').append(
                                    '<div class="form-row item_wrapper">' +
                                    '<div class="form-group col-md-3 has-feedback">' +
                                    '<div class="input-group date mt-2">' +
                                    '<input type="text" class="form-control timepicker" name="office_start_time[]" value=' + "{{ date('h:i A') }}" + '/>' +
                                    '<span class="input-group-text" id="basic-addon1">' +
                                    '<img src="{{asset("assets/images/clock.svg")}}">' +
                                    '</span>' +
                                    '</div>' +
                                    '</div>' +
                                    '<div class="form-group col-md-3 has-feedback">' +
                                    '<div class="input-group date mt-2">' +
                                    '<input type="text" class="form-control timepicker" name="office_close_time[]" value=' + "{{ date('h:i A') }}" + '/>' +
                                    '<span class="input-group-text" id="basic-addon1">' +
                                    '<img src="{{asset("assets/images/clock.svg")}}">' +
                                    '</span>' +
                                    '</div>' +
                                    '</div>' +
                                    '<div class="form-group col-md-4 has-feedback">' +
                                    '<div class="input-group date mt-2 loc_opt">'+
                                    '</div>' +
                                    '<span class="text-danger">' +
                                    '<strong class="error" id="dob-error"></strong>' +
                                    '</span>' +
                                    '</div>' +
                                    '<div class="form-group col-md-1 offset-md-1 has-feedback mt-3">' +
                                    '<a href="javascript:void(0)" title="Remove" class="remove_item"><span style="color:red;"><i class="fa fa-minus" aria-hidden="true"></i></span></a>' +
                                    '</div>' +
                                    '</div>'
                                );
								$('.loc_opt:last').append(newel);
                                $('.timepicker').datetimepicker({ format: 'LT' });
                            });

                            $(document).on('click', '.remove_item', function () {
                                $(this).closest('.item_wrapper').remove();
                            });


                            $(document).on('click', '.add_break', function () {    
                                $('.more_break_wrapper').append(
                                    '<div class="form-row break_wrapper">' +
                                    '<div class="form-group col-md-5 has-feedback">' +
                                    '<div class="input-group date mt-2">' +
                                    '<input type="text" class="form-control timepicker" name="break_start_time[]" value=' + "{{ date('h:i A') }}" + '/>' +
                                    '<span class="input-group-text" id="basic-addon1">' +
                                    '<img src="{{asset("assets/images/clock.svg")}}">' +
                                    '</span>' +
                                    '</div>' +
                                    '</div>' +
                                    '<div class="form-group col-md-5 has-feedback">' +
                                    '<div class="input-group date mt-2">' +
                                    '<input type="text" class="form-control timepicker" name="break_end_time[]" value=' + "{{ date('h:i A') }}" + '/>' +
                                    '<span class="input-group-text" id="basic-addon1">' +
                                    '<img src="{{asset("assets/images/clock.svg")}}">' +
                                    '</span>' +
                                    '</div>' +
                                    '</div>' +
                                    '<div class="form-group col-md-1 offset-md-1 has-feedback mt-3">' +
                                    '<a href="javascript:void(0)" title="Remove" class="remove_break"><span style="color:red;"><i class="fa fa-minus" aria-hidden="true"></i></span></a>' +
                                    '</div>' +
                                    '</div>'
                                );
                                $('.timepicker').datetimepicker({ format: 'LT' });
                            });

                            $(document).on('click', '.remove_break', function () {
                                $(this).closest('.break_wrapper').remove();
                            });


                            /****Add more location******/
                            $('.add_location').click(function () {
                                
                                let locationCount = $('.location_box').length + 1;
                                var id='address_'+locationCount;
                                
                                //console.log(id);
                                $('.more_location_wrapper').append(
                                    '<div style="border:2px dotted #c8c4d4;padding:10px;border-radius: 5px;" class="form-group position-relative item_wrapper location_box">' +
                                    '<a href="javascript:void(0)" class="cls_btn remove_item">x</a>' +
                                    '<div class="form-group has-feedback">' +
                                    '<input type="text" name="add_type[]" class="form-control" placeholder="Add Type">' +
                                    '</div>' +
                                    '<div class="has-feedback">' +
                                    '<input name="address[]" type="text" class="form-control" id="'+id+'" placeholder="Address">' +
                                    '</div>' +
                                    '<input type="hidden" name="location_id[]" id="" value="">' +
                                    '</div>'
                                );
                                get_places(id);

                            });

                            $(document).on('click', '.remove_item', function () {
                                var locId = this.id;

                                $.ajax({
                                    headers: {
                                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                    },
                                    data: { "id": locId },
                                    url: "{{ route('provider.delete_location') }}",
                                    type: "POST",
                                    success: function (response) {
                                        if (response['status'] == 'success') {
                                            $("#settingModal").modal('hide');
                                            $(this).closest('.item_wrapper').remove();
											callProgressBar();
											
                                        }
                                    }
                                });


                            });
                            /**********End here**********/

                            $("#settingForm").submit(function (e) {
                                e.preventDefault();
                                $('#saveBtn3').html($('#saveBtn3').attr('data-loading-text'));
                                $("#settingForm span.text-danger .error").html('');
                                $.ajax({
                                    headers: {
                                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                    },
                                    data: $('#settingForm').serialize(),
                                    url: "{{ route('provider.update_setting') }}",
                                    type: "POST",
                                    success: function (response) {
										callProgressBar(); 
                                        $('#saveBtn3').html('Update');
                                        if (response['status'] == 'success') {
                                            $("#settingModal").modal('hide');
                                            $("ol.carousel-indicators li").eq(2).removeClass("active");
											$("ol.carousel-indicators li").eq(3).addClass("active").trigger("click");
                                        }
                                    },
                                    error: function (data) {
                                        $('#saveBtn3').html('Update');
                                        if (data.responseJSON.errors) {
                                            let errors = data.responseJSON.errors;
                                        }
                                    }
                                });
                            });

                            $("#locationForm").submit(function (e) {
                                e.preventDefault();
                                $('#saveBtn3').html($('#saveBtn3').attr('data-loading-text'));
                                $("#locationForm span.text-danger .error").html('');
                                $.ajax({
                                    headers: {
                                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                    },
                                    data: $('#locationForm').serialize(),
                                    url: "{{ route('provider.add_location') }}",
                                    type: "POST",
                                    // dataType: 'json',
                                    success: function (response) {
                                        $('#saveBtn3').html('Update');
                                        if (response['status'] == 'success') {
											callProgressBar();
                                            $("#preferedLocationModal").modal('hide');
											$("#wthData").html(response['location_option']);
											var list = response['location_option'];
											var list='';
											$("ol.carousel-indicators li").eq(1).removeClass("active");
											$("ol.carousel-indicators li").eq(2).addClass("active").trigger("click");
											
                                        }
                                    },
                                    error: function (data) {
                                        $('#saveBtn3').html('Update');
                                        if (data.responseJSON.errors) {
                                            let errors = data.responseJSON.errors;
                                            
                                        }

                                    }
                                });
                            });

                            /******Weekend Setting******/
                            $("#weekendForm").submit(function (e) {
                                e.preventDefault();
                                $('#saveBtn3').html($('#saveBtn3').attr('data-loading-text'));
                                $("#weekendForm span.text-danger .error").html('');
                                $.ajax({
                                    headers: {
                                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                    },
                                    data: $('#weekendForm').serialize(),
                                    url: "{{ route('provider.add_weekend') }}",
                                    type: "POST",
                                    success: function (response) {
                                        $('#saveBtn3').html('Update');
                                        if (response['status'] == 'success') {
                                            $("#weekendModal").modal('hide');
                                            //successAlert(response['message'], 2000, 'top-right');
											callProgressBar();
                                            $("#weekendDays").text(response['newdata']);
                                            setTimeout(function () {
                                                window.location.href = "/provider/profile";
                                            }, 5000);
                                        }
                                    },
                                    error: function (data) {
                                        $('#saveBtn3').html('Update');
                                        if (data.responseJSON.errors) {
                                            let errors = data.responseJSON.errors;
                                        }
                                    }
                                });
                            });

                            $(".skip_btn_last").click(function (e) {
                                $.ajax({
                                    headers: {
                                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                    },
                                    data: { "id": "" },
                                    url: "{{ route('provider.update_login_status') }}",
                                    type: "POST",
                                    success: function (response) {
                                        if (response['status'] == 'success') {
                                            setTimeout(function () {
                                                window.location.href = "/provider/profile";
                                            }, 2000);
                                        }
                                    }
                                });
                            });



                            var sliderFlag = 0;
                            $('#con_btn').click(function () {
								alert("helllo");
                                $("#carouselExampleIndicators .carousel-control-next").trigger('click');
                                sliderFlag++;
                                if (sliderFlag == 4) {
                                    $("#con_btn").attr('disabled', true);
                                }
                                else {
                                    $("#carouselExampleIndicators ol li").click(function () {
                                        var indx = $(this).attr("data-slide-to");
                                        sliderFlag = indx;
                                        $("#con_btn").attr('disabled', false);
                                    });
                                }
                            });

                            $("#carouselExampleIndicators ol li").click(function () {
                                var indx = $(this).attr("data-slide-to");
                                if (indx == 4) {
                                    $("#con_btn").attr('disabled', true);
                                }
                            });


                        </script>

                    </div>
                </div>
            </div>
        </div>
        </div>
    </section>
@endsection