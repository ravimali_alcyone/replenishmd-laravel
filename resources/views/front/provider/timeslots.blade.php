
<div class="row">
	<div class="col-md-12">
		<div id="accordion">
		  <div class="card">
			<div class="card-header" id="headingOne">
			  <h5 class="mb-0">
				<button class="btn btn-link btn-block collapsed text-left" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne" type="button">
				  Virtual Visit
				</button>
			  </h5>
			</div>
			<div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion" style="">
			  <div class="card-body">
		@if($virtual_timeslots && $virtual_timeslots->count() > 0)
			@php $x =0; @endphp
			@foreach($virtual_timeslots as $val)
				@if($x == 0)
				<div class="form-row">
				  <div class="form-group col-md-5 has-feedback">
					<label>Start Time <span class="text-danger">*</span></label>
						<div class="input-group date mt-2">
							<input type="text" class="form-control timepicker" name="slots_data[virtual][start_time][]" value="{{ $val->event_start }}">
							<span class="input-group-text">
								<img src="{{asset('assets/images/clock.svg')}}">
							</span>
						</div>				
				  </div>
				  <div class="form-group col-md-5 has-feedback">
					<label>End Time <span class="text-danger">*</span></label>				
						<div class="input-group date mt-2">
							<input type="text" class="form-control timepicker" name="slots_data[virtual][end_time][]" value="{{ $val->event_end }}">
							<span class="input-group-text" id="basic-addon1">
								<img src="{{asset('assets/images/clock.svg')}}">
							</span>
						</div>	
				  </div>
				  <div class="form-group col-md-2 has-feedback text-center">
					<input type="hidden" name="slots_data[virtual][id][]" value="">
					<a href="javascript:void(0)" title="Add more" class="v_add_item"><span style="color:green;"><i class="fa fa-plus-circle fa-2x" aria-hidden="true"></i></span></a>
				  </div>
				</div>
				@endif
				@php $x++; @endphp
			@endforeach
		@else
				<div class="form-row">
				  <div class="form-group col-md-5 has-feedback">
					<label>Start Time <span class="text-danger">*</span></label>
						<div class="input-group date mt-2">
							<input type="text" class="form-control timepicker" name="slots_data[virtual][start_time][]" value="{{ date('h:i A') }}">
							<span class="input-group-text">
								<img src="{{asset('assets/images/clock.svg')}}">
							</span>
						</div>				
				  </div>
				  <div class="form-group col-md-5 has-feedback">
					<label>End Time <span class="text-danger">*</span></label>				
						<div class="input-group date mt-2">
							<input type="text" class="form-control timepicker" name="slots_data[virtual][end_time][]" value="{{ date('h:i A') }}">
							<span class="input-group-text" id="basic-addon1">
								<img src="{{asset('assets/images/clock.svg')}}">
							</span>
						</div>	
				  </div>
				  <div class="form-group col-md-2 has-feedback text-center">
					<input type="hidden" name="slots_data[virtual][id][]" value="">
					<a href="javascript:void(0)" title="Add more" class="v_add_item"><span style="color:green;"><i class="fa fa-plus-circle fa-2x" aria-hidden="true"></i></span></a>
				  </div>
				</div>			
		@endif
				<div class="v_more_items_wrapper">
		@if($virtual_timeslots && $virtual_timeslots->count() > 0)
			@php $x =0; @endphp
			@foreach($virtual_timeslots as $val)
				@if($x != 0)
				<div class="form-row v_item_wrapper">
					<div class="form-group col-md-5 has-feedback">
						<div class="input-group date mt-2">
							<input type="text" class="form-control timepicker" name="slots_data[virtual][start_time][]" value="{{ $val->event_start }}">
							<span class="input-group-text">
								<img src="{{asset('assets/images/clock.svg')}}">
							</span>
						</div>
					</div>
					<div class="form-group col-md-5 has-feedback">
						<div class="input-group date mt-2">
							<input type="text" class="form-control timepicker" name="slots_data[virtual][end_time][]" value="{{ $val->event_end }} ">
							<span class="input-group-text">
								<img src="{{asset('assets/images/clock.svg')}}">
							</span>
						</div>
					</div>
					<div class="form-group col-md-2 has-feedback text-center mt-3">
						<input type="hidden" name="slots_data[virtual][id][]" value="{{ $val->id }}">
						<a href="javascript:void(0)" title="Remove" class="v_remove_item">
							<span style="color:red;">
								<i class="fa fa-minus-circle" aria-hidden="true"></i>
							</span>
						</a>
					</div>
				</div>
				@endif
				@php $x++; @endphp
			@endforeach
		@endif					
				</div>    
			  </div>
			</div>
		  </div>
		</div>
	</div>	
</div>

<div class="row mt-3">
	<div class="col-md-12">
		<div id="accordion2">
		  <div class="card">
			<div class="card-header" id="headingTwo">
			  <h5 class="mb-0">
				<button class="btn btn-link btn-block collapsed text-left" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo" type="button">
				  Concierge Visit
				</button>
			  </h5>
			</div>
			<div id="collapseTwo" class="collapse show" aria-labelledby="headingTwo" data-parent="#accordion2" style="">
			  <div class="card-body">
		@if($concierge_timeslots && $concierge_timeslots->count() > 0)
			@php $x =0; @endphp
			@foreach($concierge_timeslots as $val)
				@if($x == 0)
				<div class="form-row">
				  <div class="form-group col-md-5 has-feedback">
					<label>Start Time <span class="text-danger">*</span></label>
						<div class="input-group date mt-2">
							<input type="text" class="form-control timepicker" name="slots_data[concierge][start_time][]" value="{{ $val->event_start }}">
							<span class="input-group-text">
								<img src="{{asset('assets/images/clock.svg')}}">
							</span>
						</div>				
				  </div>
				  <div class="form-group col-md-5 has-feedback">
					<label>End Time <span class="text-danger">*</span></label>				
						<div class="input-group date mt-2">
							<input type="text" class="form-control timepicker" name="slots_data[concierge][end_time][]" value="{{ $val->event_end }}">
							<span class="input-group-text">
								<img src="{{asset('assets/images/clock.svg')}}">
							</span>
						</div>	
				  </div>
				  <div class="form-group col-md-2 has-feedback text-center">
					<input type="hidden" name="slots_data[concierge][id][]" value="">
					<a href="javascript:void(0)" title="Add more" class="add_item"><span style="color:green;"><i class="fa fa-plus-circle fa-2x" aria-hidden="true"></i></span></a>
				  </div>
				</div>
				@endif
				@php $x++; @endphp
			@endforeach
		@else
				<div class="form-row">
				  <div class="form-group col-md-5 has-feedback">
					<label>Start Time <span class="text-danger">*</span></label>
						<div class="input-group date mt-2">
							<input type="text" class="form-control timepicker" name="slots_data[concierge][start_time][]" value="{{ date('h:i A') }}">
							<span class="input-group-text">
								<img src="{{asset('assets/images/clock.svg')}}">
							</span>
						</div>				
				  </div>
				  <div class="form-group col-md-5 has-feedback">
					<label>End Time <span class="text-danger">*</span></label>				
						<div class="input-group date mt-2">
							<input type="text" class="form-control timepicker" name="slots_data[concierge][end_time][]" value="{{ date('h:i A') }}">
							<span class="input-group-text">
								<img src="{{asset('assets/images/clock.svg')}}">
							</span>
						</div>	
				  </div>
				  <div class="form-group col-md-2 has-feedback text-center">
					<input type="hidden" name="slots_data[concierge][id][]" value="">
					<a href="javascript:void(0)" title="Add more" class="add_item"><span style="color:green;"><i class="fa fa-plus-circle fa-2x" aria-hidden="true"></i></span></a>
				  </div>
				</div>			
		@endif
				<div class="more_items_wrapper">
		@if($concierge_timeslots && $concierge_timeslots->count() > 0)
			@php $x =0; @endphp
			@foreach($concierge_timeslots as $val)
				@if($x != 0)
				<div class="form-row item_wrapper">
					<div class="form-group col-md-5 has-feedback">
						<div class="input-group date mt-2">
							<input type="text" class="form-control timepicker" name="slots_data[concierge][start_time][]" value="{{ $val->event_start }}">
							<span class="input-group-text">
								<img src="{{asset('assets/images/clock.svg')}}">
							</span>
						</div>
					</div>
					<div class="form-group col-md-5 has-feedback">
						<div class="input-group date mt-2">
							<input type="text" class="form-control timepicker" name="slots_data[concierge][end_time][]" value="{{ $val->event_end }} ">
							<span class="input-group-text">
								<img src="{{asset('assets/images/clock.svg')}}">
							</span>
						</div>
					</div>
					<div class="form-group col-md-2 has-feedback text-center mt-3">
						<input type="hidden" name="slots_data[concierge][id][]" value="{{ $val->id }}">
						<a href="javascript:void(0)" title="Remove" class="remove_item">
							<span style="color:red;">
								<i class="fa fa-minus-circle" aria-hidden="true"></i>
							</span>
						</a>
					</div>
				</div>
				@endif
				@php $x++; @endphp
			@endforeach
		@endif					
				</div> 
				<div class="form-row" style="display:none;">
					<div class="form-group col-md-12 has-feedback">
						<label>Visit Location <span class="text-danger">*</span></label>
						<textarea name="location" id="location" class="form-control" col="40" rows="3" placeholder="For all concierge visit timeslots" disabled></textarea>
						<span class="text-danger">
							<strong class="error" id="location-error"></strong>
						</span>									
					</div>
				</div>				
			  </div>
			</div>
		  </div>
		</div>
	</div>	
</div>