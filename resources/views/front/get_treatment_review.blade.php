	@php
		$images = json_decode($medicine->images,true);
		
		if($images){
			$display_img = $images[0];
		}else{
			$display_img = 'assets/images/default_medicine.jpg';
		}
	@endphp
	<style>
/* 		.rec_pay_options {
			display: inline-block;
			width: 30%;
		} */
		.rec_custom_options {
			font-size: 12px;
			margin-top: 10px;
			width: 100%;
		}		
		.rec_custom_options input#interval_count {
			width: 50px;
			padding-left: 2px;
		}		
		.rec_custom_options select#custom_plan_interval {
			height: 22px;
		}		
	</style>
	<div class="container number_uses">

		<div class="num_uses">

			<h5>Review treatment and pay</h5>
			<p>Your Shipping frequency</p>
		@if($variant->is_recurring == 1 || $variant->is_recurring == 2)			
			<div class="review_box">
				<h6>Payment Type</h6>
				<div class="rec_pay_options">
					<input type="radio" name="md_payment_type" id="one_time_payment" class="form-control" value="0" onclick="show_recurring_options(this);" checked> <label for="one_time_payment">One-time</label>
				</div>
				<div class="rec_pay_options">
					<input type="radio" name="md_payment_type" id="recurring_payment" class="form-control" value="1" onclick="show_recurring_options(this);" > <label for="recurring_payment">Recurring</label>
				</div>	
			</div>
			<div class="review_box" id="recurring_options_view" style="display:none;">
				<h6>Recurring Options</h6>
				@if($variant->plan_interval == 'month')
					<div class="rec_pay_options">
					<!--input type="radio" name="recurring_payment_type" id="recurring_payment_1" class="form-control" onclick="show_custom_options(this);" value="month"--> <label for="recurring_payment_1">Monthly</label> <small > (Receive a 1 month supply and get billed every month. Cancel your plan at anytime.)</small>
				</div>
				@elseif($variant->plan_interval == 'week')
				<div class="rec_pay_options">
					<!--input type="radio" name="recurring_payment_type" id="recurring_payment_2" class="form-control" value="week"--> <label for="recurring_payment_2" onclick="show_custom_options(this);">Weekly</label> <small > (Receive a 1 week supply and get billed every month. Cancel your plan at anytime.)</small>
				</div>
				@elseif($variant->plan_interval == 'day')
				<div class="rec_pay_options">
					<!--input type="radio" name="recurring_payment_type" id="recurring_payment_3" class="form-control" onclick="show_custom_options(this);" value="day"--> <label for="recurring_payment_3">Daily</label> <small > (Receive a 1 day supply and get billed every month. Cancel your plan at anytime.)</small>
				</div>
				@elseif($variant->plan_interval == 'year')
				<div class="rec_pay_options">
					<!--input type="radio" name="recurring_payment_type" id="recurring_payment_4" class="form-control" onclick="show_custom_options(this);" value="year"--> <label for="recurring_payment_4">Yearly</label> <small > (Receive a 1 year supply and get billed every month. Cancel your plan at anytime.)</small>
				</div>
				@elseif($variant->plan_interval == 'custom')
				<div class="rec_pay_options">
					<!--input type="radio" name="recurring_payment_type" id="recurring_payment_5" class="form-control" onclick="show_custom_options(this);" value="custom"--> <label for="recurring_payment_5">Custom</label> <small > (Receive a custom time supply and get billed every month. Cancel your plan at anytime.)</small>
				</div>
				@endif
				
				@if($variant->plan_interval == 'custom')
				<div class="rec_custom_options">
					<label>Every</label>
					<!--input type="number" name="interval_count" id="interval_count" class="" value="{{ $variant->interval_count }}" min="1" readonly-->
					<span> {{ $variant->interval_count }} </span>
					<!--select name="custom_plan_interval" id="custom_plan_interval" class="">
						@if($variant->custom_plan_interval == 'month')<!--option value="month"-->Months<!--/option-->@endif
						@if($variant->custom_plan_interval == 'week')<!--option value="week"-->Weeks<!--/option-->@endif
						@if($variant->custom_plan_interval == 'day')<!--option value="day"-->Days<!--/option-->@endif
					</select-->
				</div>
				@endif
				<div class="rec_pay_options">
					 <label for="recurring_payment_5">Total interval</label> <input type="number" name="recurring_interval" id="recurring_interval" class="form-control" value="1" required>
				</div>
			</div>
		@else
			<div class="review_box" style="display:none;">
				<h6>Payment Type</h6>
				<div class="rec_pay_options">
					<input type="radio" name="md_payment_type" id="one_time_payment" class="form-control" value="0" checked> <label for="one_time_payment">One-time</label>
				</div>
			</div>			
		@endif

			<div class="review_box_1">
				<h6>Your plan</h6>
				<div class="sil_review">
					<div class="sil_details">
						<div class="sil_details_img">
							<img src="{{env('APP_URL')}}/{{$display_img}}" alt="{{$medicine->name}}">

						</div>
						<div class="sil_details_intro">
							<h5>{{$medicine->name}}</h5>
							<p>{{$variant->variant_name}}</p>
							<p>{{$data['quantity']}} x (${{$variant->price}} Ea.)</p>
							<!--p>${{$variant->price}}</p-->
							<!--p>{{ $data['months'] }} Month Supply</p-->
							<p style="display:none;">Edit</p>
						</div>

					</div>
					<div class="sil_details_price ml-auto">

						<!--p><span>${{ $data['amount'] }}</span>/{{ $data['months'] }}mon</p-->
						<p><span>${{ $data['amount'] }}</span></p>

					</div>


				</div>

				<!--div class="sil_review">

					<div class="order_pro">
						<h5>First Order promo</h5>

					</div>

					<div class="order_pro ml-auto">
						<span>-${{ @$data['promo_price'] }}</span>

					</div>
				</div-->


				<div class="sil_review">

					<div class="order_pro">
						<h5>Healthcare professional review</h5>
						<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum </p>

					</div>

					<div class="order_pro ml-auto">
						<span>free</span>

					</div>
				</div>

				<div class="sil_review">

					<div class="order_pro">
						<h5>2 Day Shipping</h5>

					</div>

					<div class="order_pro ml-auto">
						<span>free</span>

					</div>
				</div>

				<div class="sil_review">

					<div class="order_pro">
						<h5>Total due if prescribed</h5>

					</div>

					<div class="order_pro ml-auto">
						<span>${{ $data['final_amount'] }}</span>

					</div>
				</div>
				

				<div class="sil_review_1">

					<div class="order_pro_v">
						<h5>Due Today</h5>
						<p> <a href="javascript:void(0)">What am I Changed</a></p>
					</div>

					<div class="order_pro ml-auto">
						<h6>@if($variant->is_recurring == 0) ${{ $data['final_amount'] }} @else $0 @endif</h6>

					</div>
				</div>
			</div>

			<h5>Payments</h5>
			<div class="review_box">
				<div class="check_1">
					<input type="radio" id="payment_card" name="payment_type" onclick="goToPayment(this);" value="payment_card">
					<label for="payment_card"><span>Credit or Debit card</span></label>
					<i class="fas fa-lock"></i>
				</div>

			</div>
			<div class="review_box">
				<div class="check_1">
					<input type="radio" id="payment_paypal" name="payment_type" value="payment_paypal">
					<label for="payment_paypal"><img src="{{asset('assets/images/paypal-logo.svg')}}" alt=""></label>

				</div>

			</div>
			
		</div>
	</div>
	
	