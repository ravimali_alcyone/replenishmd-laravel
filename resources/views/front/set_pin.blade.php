@extends('front.online_visit_layout.app')

@section('css')

    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

@section('content')

<style>
.modal-backdrop.show {
    opacity: 0.5;
    z-index: 0;
}

.modal-open .modal{
    z-index: 1;
}

.payment_image{

	width:33%;

	float:left;

}
li.saved_card {
    border: 1px solid #dedede;
    padding:10px 15px;
    display: flex;
    flex-wrap: wrap;
    justify-content: space-between;
    align-items: center;
    border-radius: 4px;
	box-shadow: 0px 0px 20px 0px rgba(50, 56, 66, 0.1); margin-bottom:10px;
}
.card_info {
    display: flex;
    width: calc(100% - 40px);
    justify-content: space-between;
    align-items: center;
    flex-wrap: wrap;
}
.cc_cvv_input input {
    width: 70px;
    text-align: center;
    letter-spacing: 5px;
}
.cc_card_info {
    display: flex;flex-wrap: wrap;font-size: 13px;
}
.cc_card_info .cc_holder_name { width:100%;
    text-transform: uppercase;
    font-size: 14px;
    font-weight: 600;
    margin-bottom: 5px;
}
.cc_card_info .cc_number{ margin-right:8px;}
.new_cc_add{
    background-color: #f5f5f5;
    border-radius: 4px;
    padding: 15px;
    border: 1px solid #eee;
}
.pin_box{
	display:flex;
	gap:10px;
	align-items:center;
}
.pin_box input{ max-width:100px; height:42px;}
</style>

    <div class="flex items-center mt-5">

        <div class="md:w-1/2 md:mx-auto">



            @if (session('status'))

                <div class="text-sm border border-t-8 rounded text-green-700 border-green-600 bg-green-100 px-3 py-4 mb-4" role="alert">

                    {{ session('status') }}

                </div>

            @endif

			<div class="flex flex-col break-words bg-white border border-2 rounded shadow-md">
				<div class="font-semibold bg-gray-200 text-gray-700 py-3 px-6 mb-0 text-right">
					<span class="px-6">Please set your one time payment confirmation pin before doing first payment.</span>
				</div>
                <div class="w-full p-6">
					@if(session('error_message'))
						<div role="alert" class="mb-4">
							<div class="bg-red-500 text-white font-bold rounded-t px-4 py-2">Payment Failed</div>
								<div class="border border-t-0 border-red-400 rounded-b bg-red-100 px-4 py-3 text-red-700">
									<p>{{ session('error_message') }}</p>
								</div>
							</div>
						</div>
					@endif
					<div class="mx-auto clearfix">
						<form id="setPinForm" class="new_cc_add">
							@csrf
							<div class="row">
								<div class="col-md-12">
									<div class="CVV">
										<label for="cvv">Payment Pin</label>
										<div class="pin_box">
											<input type="password" class=" appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="payment_pin" name="payment_pin" value="" maxlength="4" placeholder="****">
											<span id="cvv-error" class="error text-red"></span>
											<input type="hidden" name="p_type" id="p_type" value="{{ $p_type }}">
											<input type="hidden" name="m_intveral" id="m_intveral" value="{{ $interval }}">
											<button type="submit" class="inline-block align-middle text-center select-none border font-bold whitespace-no-wrap py-2 px-4 rounded text-base leading-normal no-underline text-gray-100 bg-red-500 hover:bg-red-600" id="saveBtn_pin">Set Pin</button>
										</div>
									</div>
								</div>
							</div>    
						</form>
					</div>
                </div>
            </div>

        </div>

    </div>

	

	



<script src="{{asset('assets/js/jquery.mask.js')}}"></script>

<script>
	
	$("#setPinForm").submit(function(e) {
		e.preventDefault();
		$(".loader").css('display', 'flex');
		$('#saveBtn_pin').html($('#saveBtn_pin').attr('data-loading-text'));
		$("#setPinForm span.text-danger .error").html('');
		
		$.ajax({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			data: $('#setPinForm').serialize(),
			url: "{{ route('user.set_payment_pin') }}",
			type: "POST",
			success: function (response) {
				$('#saveBtn_pin').html('Submit');
				$(".loader").css('display', 'none');
				if(response['status'] == 'success'){
					callProgressBar();
					$('#setPinForm').trigger("reset");
					var p_type = $("#p_type").val();
					var m_intveral = $("#m_intveral").val();
					purl = (m_intveral !="" ) ? "{{ env('APP_URL') }}/payment_page/"+p_type+"/"+m_intveral : "{{ env('APP_URL') }}/payment_page/"+p_type ;
					setTimeout(function(){
						window.location = purl;
					}, 500);
					
					/*setTimeout(function(){
						$('#setPinForm').trigger("reset");
						window.location=response['redirect'];
					}, 2000);*/
				}
			},
			error: function (data) {
				$('#saveBtn_pin').html('Submit');
				$(".loader").css('display', 'none');
				if(data.responseJSON.errors) {
					let errors = data.responseJSON.errors;
					if(errors.payment_pin){
						$( '#payment_pin-error' ).html( errors.payment_pin[0] );
					}
					
				}
			
			}
		});
	});



</script>

    



@endsection



