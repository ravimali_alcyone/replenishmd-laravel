@extends('front.online_visit_layout.app')

@section('content')
	<style>
	    .modal-backdrop.show {
            opacity: 0.5;
            z-index: 0;
        }
        
        .modal-open .modal{
            z-index: 1;
        }
	</style>
	<section class="sign_in_sec_wrapper">
		<div class="sign_in_sec shadow">
			<img src="{{asset('assets/images/rmd_logo_red.svg')}}" alt="rmd_logo_red">
			<h1 class="title">Sign up to your RMD platform account to continue.</h1>


			<form id="signupForm">
				<div class="form-group">
					<input type="email" name="email" class="form-control" placeholder="Email" id="email" autofocus>
				</div>
				<div class="input_wrapper">
					<div class="form-group">
						<input type="text" name="first_name" class="form-control" placeholder="First Name" id="first_name">
					</div>
					<div class="form-group">
						<input type="text" name="last_name" class="form-control" placeholder="Last Name" id="last_name">
					</div>
				</div>
				<div class="input_wrapper">
					<div class="form-group">
						<input type="password" name="password" class="form-control" placeholder="Password" id="password">
					</div>
					<div class="form-group">
						<input type="password" name="password_confirmation" class="form-control" placeholder="Confirm Password" id="confirm_password">
					</div>
				</div>
				<div class="agree_with_terms_box">
					<div class="checkbox d-inline-block">
						<input type="checkbox" id="checkbox2" name="agree" value="1">
						<label for="checkbox2"><span></span></label>
					</div>
					<span>I agree to <a href="{{env('APP_URL')}}/terms-conditions" class="terms">terms</a> and <a href="{{env('APP_URL')}}/privacy-policy">privacy policy</a> and consent to <a href="#">telehealth</a></span>
				</div>
				<!--<button type="submit" id="saveBtn" class="btn btn-primary">Submit</button>-->
				<button type="submit" id="saveBtn" class="btn btn-primary" >Submit</button>
				<p class="mb-0">Already a member? <a href="{{env('APP_URL')}}/sign_in" class="login_link link">Log in and continue</a></p>
			</form>
		</div>
		
		
	<div id="emailOtpModal" class="modal" tabindex="-1" role="dialog">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
			  <div class="modal-header">
				<h5 class="modal-title">Email Verification</h5>
				<!--<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				  <span aria-hidden="true">&times;</span>
				</button>-->
			  </div>
			  <form id="emailOtpForm">
			  <div class="modal-body">
					<span>Please check your inbox to verify registered email-id.</span>
					<div>&nbsp;</div>
					<div class="form-group has-feedback">
						<label for="old_password">Enter OTP<span class="text-danger">*</span></label>
						<input type="password" placeholder="OTP" class="form-control" name="email_otp" id="email_otp" value="">	
					</div>
			  </div>
			  <div class="modal-footer justify-content-between">
			  <button type="button" onclick="resendOtp()" class="accept_btn btn btn-outline-primary btn-sm resend_otp" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Processing">
			      Resend OTP</button>
				<button type="submit" id="verifyBtn" class="accept_btn btn btn-outline-primary btn-sm" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Processing">Verify</button>
			  </div>
			  </form>
			</div>
		  </div>
		</div>
	</section>		
	
	<script>
		
		$("#signupForm").submit(function(e) {
			e.preventDefault();
			$(".loader").css('display', 'flex');
			
			$.ajax({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				data: $('#signupForm').serialize(),
				url: "{{ route('send_email_otp') }}",
				type: "POST",
				success: function (response) {
					$(".loader").css('display', 'none');
											
					if(response['status'] == 'success'){
						$("#emailOtpModal").modal({
							backdrop: 'static',
							keyboard: false
						});
						$("#emailOtpForm").trigger('reset');
						$("#emailOtpForm span.text-danger .error").html('');	
						
					}else{
						errorAlert('Error occured.',3000,'top-right');						
					}
				},
				error: function (data) {
					$(".loader").css('display', 'none');
					let errors = data.responseJSON.errors;
					
					$.each(errors, function(key, value) {
						errorAlert(value[0],3000,'top-right');
					});	
				}
			});
		});
		
		$("#emailOtpForm").submit(function(e) {
			e.preventDefault();
			var first_name = $("#first_name").val();
			var last_name  = $("#last_name").val();
			var email = $("#email").val();
			var password = $("#password").val();
			var email_otp = $("#email_otp").val();
			
			$('#verifyBtn').html($('#verifyBtn').attr('data-loading-text'));
		    $('#verifyBtn').addClass('disabled');
		    $(".loader").css('display', 'flex');
			
			$.ajax({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				data: {"first_name":first_name, "last_name":last_name, "email":email, "password":password,"email_otp":email_otp},
				url: "{{ route('signup_submission') }}",
				type: "POST",
				success: function (response) {
					if(response['status'] == 'success'){
						$("#emailOtpModal").modal('hide');
						//successAlert(response['message'],2000,'top-right');
						callProgressBar();
						setTimeout(function(){
							window.location = response['redirect'];
						}, 2000);	
					}
					else{
						errorAlert(response['message'],3000,'top-right');						
					}
					$('#verifyBtn').removeClass('disabled');
					$('#verifyBtn').html('Verify');
					$(".loader").css('display', 'none');
				},
				error: function (data) {
					$(".loader").css('display', 'none');
					$('#verifyBtn').removeClass('disabled');
					$('#verifyBtn').html('Verify');
					let errors = data.responseJSON.errors;
					
					$.each(errors, function(key, value) {
						errorAlert(value[0],3000,'top-right');
					});					
					
				}
			});
		});
		
		function resendOtp()
		{
		    $('.resend_otp').html($('.resend_otp').attr('data-loading-text'));
		    $('.resend_otp').addClass('disabled');
		    //$(".loader").css('display', 'flex');
			$.ajax({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				data: {},
				url: "{{ route('resend_email_otp') }}",
				type: "POST",
				success: function (response) {
				   
					if(response['status'] == 'success'){
						//successAlert(response['message'],2000,'top-right');
						callProgressBar();
						//$(".loader").css('display', 'none');
					}
					else{
						errorAlert('Error occured.',3000,'top-right');
						//$(".loader").css('display', 'none');
					}
					$('.resend_otp').removeClass('disabled');
					$('.resend_otp').html('Resend OTP');
				},
				error: function (data) {
					//$(".loader").css('display', 'none');
					$('.resend_otp').removeClass('disabled');
					$('.resend_otp').html('Resend OTP');
					let errors = data.responseJSON.errors;
					
					$.each(errors, function(key, value) {
						errorAlert(value[0],3000,'top-right');
					});					
					
				}
			});
		}
			
	</script>	
@endsection