@extends('front.common_layout.app')
@section('content')
@php $type = auth()->user()->user_role == 4 ? 'user' : 'provider'; @endphp
	@if(auth()->user()->user_role==3)
		<div class="col-xl-10 col-lg-8 col-md-8 col-sm-12">
	@endif
		<div class="main-center-data">
			<h3 class="display-username">Forums</h3>
			<div class="row mt-15">		
				<div class="col-xl-10 col-lg-10 col-md-12 col-sm-12">
					
					<div class="filters_wrapper">
						<div class="data-list-filters">
							<label>Status</label>
							<select id="publish_status" class="custom-select select2">
								<option value="" class="any">Any</option>
								<option value="1">Active</option>
								<option value="0">Inactive</option>
							</select>
						</div>
						<div class="data-list-filters">
							<label>Access Type</label>
							<select id="pay_status" class="custom-select select2">
								<option value="" class="any">Any</option>
								<option value="1">Paid</option>
								<option value="0">Unpaid</option>
							</select>
						</div>
						<div class="data-list-filters">
							<label>Access Payment Type</label>
							<select id="access_payment_type" class="custom-select select2">
								<option value="" class="any">Any</option>
								<option value="1">One Time</option>
								<option value="0">Monthly</option>
							</select>
						</div>
						<div class="data-list-filters">
							<label>Posting Type</label>
							<select id="post_status" class="custom-select select2">
								<option value="" class="any">Any</option>
								<option value="1">Paid</option>
								<option value="0">Unpaid</option>
							</select>
						</div>
						<div class="data-list-filters">
							<label>Posting Payment Type</label>
							<select id="posting_payment_type" class="custom-select select2">
								<option value="" class="any">Any</option>
								<option value="1">One Time</option>
								<option value="0">Monthly</option>
							</select>
						</div>
						<div class="data-list-filters">
							<label>Type</label>
							<select id="ver_status" class="custom-select select2">
								<option value="" class="any">Any</option>
								<option value="1">Verified</option>
								<option value="0">Pendding</option>
							</select>
						</div>
						<div class="data-list-filters pt-4">
							<div class="mt-3 pt-1">
							<button type="button" class="btn btn-sm btn-secondary" id="filter">Show</button>
							
							<button type="reset" class="btn btn-sm btn-danger ml-2" id="reset">Reset</button>
							</div>
						</div>	
					</div>
				</div>
				<?php $data = json_decode($setting[0]->value); ?>
				@if($data->payable==1 && $forum_payment_flag==0)
					<div class="col-xl-1 col-lg1 col-md-12 col-sm-12 pull-right mt-5 pt-2"><a href="javascript:void(0)" onclick="chat_box('pswrd')"  class="btn btn-success btn-sm">Create forum</a></div>
				@else
					<div class="col-xl-1 col-lg-1 col-md-12 col-sm-12 pull-right mt-5 pt-2"><a href="{{ url($type.'/forums/addForum/0') }}" class="btn btn-success btn-sm">Create forum</a></div>
					<div class="col-xl-1 col-lg-1 col-md-12 col-sm-12 pull-right mt-5 pt-2"><a href="{{ url($type.'/forums/post') }}" class="btn btn-success btn-sm">Forum Post</a></div>										
				@endif
				
			</div>
			
			<div class="row mt-15 bg-white round-crn pd-20-30 hover-effect-box">
				<div class="col-xl-12 col-lg-12 col-md-12">			
					<div id="patients_data">
						<table id="myTable" class="table table-striped table-bordered" style="width:100%">
							<thead>
								<tr class="intro-x">
									<th class="border-b-2">S.No.</th>
									<th class="border-b-2">Forum Subject</th>
									<th class="border-b-2">Status</th>
									<th class="border-b-2">Is Access Paid?</th>
									<th class="border-b-2">Is Posting Paid?</th>
									<th class="border-b-2">Action</th>
								</tr>
							</thead>
							<tbody></tbody>
						</table>			
					</div>
				</div>
			</div>

		</div>

		<div class="chat_pop edit_pop petient chat_pop_pswrd">
			<div class="edit_pop_title">
				<h3>Paywall Payment</h3>
				<a class="cp_close"><i class="fa fa-times"></i></a>
			</div>
			<div class="chat_block">
				{!! $paywall !!}	
			</div>
		</div>


	@if(auth()->user()->user_role==3)
		</div>
	@endif		
	
@push('scripts')
<script type="text/javascript">
	
	$(document).ready(function(){
		$('.cp_close').click(function(flag){
			var hidden = $('.chat_pop');
			hidden.animate({"right":"-100%"}, "slow").removeClass('visible');
		});
	});

	function chat_box(flag)
	{
		var hidden = $('.chat_pop_'+flag);
		if (hidden.hasClass('visible')){
			hidden.animate({"right":"-100%"}, "slow").removeClass('visible');
		} else {
			hidden.animate({"right":"0"}, "slow").addClass('visible');
		}
	}


	var table;
 	$(document).ready(function() {
		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});
		fill_datatable();
		function fill_datatable(publish_status = '', pay_status='', access_payment_type='', post_status='', posting_payment_type='', ver_status=''){
			table = $('#myTable').DataTable({
				processing: true,
				serverSide: true,
				responsive: true,
				paging: true,
				ordering: false,
				info: false,
				displayLength: 10,
				language: {
					processing: '<img src="/dist/images/loading.gif"/>'
				},				
				lengthMenu: [
				[10, 25, 50, -1],
				[10, 25, 50, "All"]
				],
				ajax:{
					url: "{{ route('user.forums') }}",
					data:{"publish_status":publish_status,"pay_status":pay_status,"access_payment_type":access_payment_type,"post_status":post_status,"posting_payment_type":posting_payment_type,"ver_status":ver_status }
				},
				columns: [
					{data: 'DT_RowIndex', name: 'DT_RowIndex'},
					{data: 'forum_subject', name: 'forum_subject'},								
					{data: 'status', name: 'status'},
					{data: 'is_paid', name: 'is_paid'},
					{data: 'is_posting_paid', name: 'is_posting_paid'},
					{data: 'action', name: 'action', orderable: false, searchable: false},
				]
			});
		}

		$('#filter').click(function(){		
			var publish_status = $('#publish_status').val();			
			var pay_status = $('#pay_status').val();
			var access_payment_type = $('#access_payment_type').val();
			var post_status = $('#post_status').val();
			var posting_payment_type = $('#posting_payment_type').val();
			var ver_status = $('#ver_status').val();
			$('#myTable').DataTable().destroy();
			fill_datatable(publish_status,pay_status,access_payment_type,post_status,posting_payment_type,ver_status);
		});

		$('#reset').click(function(){

			$('#publish_status').prop('selectedIndex', 0);
			$('#publish_status').select2();			
			
			$('#pay_status').prop('selectedIndex', 0);
			$('#pay_status').select2();
			$('#access_payment_type').prop('selectedIndex', 0);
			$('#access_payment_type').select2();
			$('#post_status').prop('selectedIndex', 0);
			$('#post_status').select2();
			$('#posting_payment_type').prop('selectedIndex', 0);
			$('#posting_payment_type').select2();
			$('#ver_status').prop('selectedIndex', 0);
			$('#ver_status').select2();

			$('#myTable').DataTable().destroy();
			fill_datatable();
		});

		$("#myTable tbody tr").addClass('intro-x');
  	});

    function deleteRow(id) {
		swal({
			title: "Are you sure to delete?",
			text: "",
			icon: 'warning',
			buttons: {
			  cancel: true,
			  delete: 'Yes, Delete It'
			}
		  }).then((isConfirm) => {
			if (!isConfirm) {
				return false;
			} else {
				$.ajax({
					type: "DELETE",
					url: "{{ env('APP_URL').'/user/forums/destroy' }}"+'/'+id,
					success: function (response) {
						if(response['status'] == 'success'){
						swal({
							title: response['message'],
							icon: 'success'
						});
						setTimeout(function(){
						swal.close();
						}, 1000);
						}
						table.draw();
					},
					error: function (data) {
						console.log('Error:', data);
						swal({
							title: 'Error Occured',
							icon: 'error'
						});
					}
				});
			}
        });
    }

	function changeStatus(id,status,flag) {
		$.ajax({
			url: "{{ url('user/forums/change_status') }}",
			type: "POST",
			data: {'id': id, 'status':status, 'flag':flag},
			success: function(response) 
			{
				if(response['status'] == 'success')
				{
					swal({
						title: response['message'],
						icon: 'success'
					});
					setTimeout(function(){
					swal.close();
					}, 1000);
				}
				table.draw();
			}
		});
	}
</script>
@endpush

@endsection