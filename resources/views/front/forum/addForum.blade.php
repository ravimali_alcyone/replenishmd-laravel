@extends('front.common_layout.app')
@section('content')
	@php $type = auth()->user()->user_role == 4 ? 'user' : 'provider'; @endphp

	<style>
		.select2-container--default .select2-selection--single {
			min-height: 37px;
		}		
		
		.select2-container--default .select2-selection--single .select2-selection__rendered {
			line-height: 32px;
		}		
	</style>
	
	@if(auth()->user()->user_role==3)
		<div class="col-xl-10 col-lg-8 col-md-8 col-sm-12">
	@endif
		<div class="flex flex-col sm:flex-row items-center mt-4 border-b border-gray-200 dark:border-dark-5">
			<h3 class="font-medium text-base mr-auto"><a href="<?php echo route($type.'.forums'); ?>">Froum List</a> > @if($forum_topic) {{ 'Edit' }} @else Add New @endif Froum</h3>
		</div>
		<div class=" mt-15 bg-white round-crn pd-20-30 hover-effect-box">
			<form id="forumForm">
				<div class="row">
					<div class="col-sm-10 pl-0">
						<div class="row">
							<div class="form-group col-sm-12">
								<label for="email">Subject</label>
								<input type="text" name="title" value="<?php if($forum_topic){ echo $forum_topic->forum_subject;}?>" class="form-control border mt-2" placeholder="Title" id="title" >
							</div>

							<div class="form-group col-sm-12">
								<label for="email">Slug</label>
								<input type="text" name="slug" value="<?php if($forum_topic){ echo $forum_topic->slug;}?>" class="form-control border mt-2" placeholder="Slug" id="slug" >
							</div>

							<div class="form-group col-sm-12">
								<label for="email">Short info</label>
								<textarea name="short_info" id="short_info" col="30" rows="4" class="form-control"><?php if($forum_topic){ echo $forum_topic->short_info;}?></textarea>
							</div>
						</div>
					</div>
					<div class="col-md-2  pt-3" style="background-color: #f7fafc;">
						<div class="intro-y col-span-12">
							<div class="grid grid-cols-12  gap-6">
								<div class="col-span-12 border-bottom">
									<label><strong>Status</strong></label>
									<div>
										<input type="radio" id="status_1" name="status" value="1" <?php if(!$forum_topic){ echo 'checked';} ?> class="mr-3" <?php if($forum_topic && $forum_topic->status==1){ echo "checked"; } ?> ><label for="status_1" class="mr-4">Active</label>
										<input type="radio" id="status_0" name="status" value="0" class="mr-4" <?php if($forum_topic && $forum_topic->status==0){ echo "checked"; } ?> ><label for="status_0">Inactive</label>
									</div>
								</div>
								<div class="col-span-12 mt-1 border-bottom">
									<label><strong>Is Access Paid?</strong></label>
									<div>
										<input type="radio" id="is_paid_1" name="is_paid" value="1" <?php if(!$forum_topic){ echo 'checked';} ?> class="mr-3" <?php if($forum_topic && $forum_topic->is_paid==1){ echo "checked"; } ?> ><label for="is_paid_1" class="mr-4">Yes</label>
										<input type="radio" id="is_paid_0" name="is_paid" value="0" class="mr-4" <?php if($forum_topic && $forum_topic->is_paid==0){ echo "checked"; } ?> ><label for="is_paid_0">No</label>
									</div>
									<div>
										<input type="radio" id="access_payment_type_1" name="access_payment_type" value="1" <?php if(!$forum_topic){ echo 'checked';} ?> class="mr-3" <?php if($forum_topic && $forum_topic->access_payment_type==1){ echo "checked"; } ?> ><label for="is_paid_1" class="mr-4">One time</label>
										<input type="radio" id="access_payment_type_0" name="access_payment_type" value="0" class="mr-4" <?php if($forum_topic && $forum_topic->access_payment_type==0){ echo "checked"; } ?> ><label for="is_paid_0">Monthly</label>
									</div>
									<div class="mb-2">
										<input type="text" name="access_amount" id="access_amount" value="<?php if($forum_topic){ echo $forum_topic->access_amount; } ?>" class="form-control">
									</div>
								</div>
								<div class="col-span-12 mt-1">
									<label><strong>Is Posting Paid?</strong></label>
									<div>
										<input type="radio" id="is_posting_1" name="is_posting" value="1" <?php if(!$forum_topic){ echo 'checked';} ?> class="mr-3" <?php if($forum_topic && $forum_topic->is_posting_paid==1){ echo "checked"; } ?> ><label for="is_paid_1" class="mr-4">Yes</label>
										<input type="radio" id="is_posting_0" name="is_posting" value="0" class="mr-4" <?php if($forum_topic && $forum_topic->is_posting_paid==0){ echo "checked"; } ?> ><label for="is_paid_0">No</label>
									</div>
									<div>
										<input type="radio" id="posting_payment_type_1" name="posting_payment_type" value="1" <?php if(!$forum_topic){ echo 'checked';} ?> class="mr-3" <?php if($forum_topic && $forum_topic->posting_payment_type==1){ echo "checked"; } ?> ><label for="is_paid_1" class="mr-4">One time</label>
										<input type="radio" id="posting_payment_type_0" name="posting_payment_type" value="0" class="mr-4" <?php if($forum_topic && $forum_topic->posting_payment_type==0){ echo "checked"; } ?> ><label for="is_paid_0">Monthly</label>
									</div>
									<div>
										<input type="text" name="posting_amount" id="posting_amount" value="<?php if($forum_topic){ echo $forum_topic->posting_amount; } ?>" class="form-control">
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="form-group mt-12">
					<div class="intro-y col-span-12 lg:col-span-12">
						<input type="hidden" name="id" value="<?php if($forum_topic){ echo $forum_topic->id;}?>"/>
						<input type="hidden" name="page" value="<?php if($forum_topic){ echo 'edit';} else { echo 'add'; }?>"/>
						<button type="submit" id="saveBtn" class="btn btn-primary">Save</button>
						<button type="reset" class="btn btn-danger ml-2" onclick="location.href = '{{ route('user.forums') }}';">Cancel</button>
					</div>
				</div>
			</form>
		</div>
	@if(auth()->user()->user_role==3)
		</div>
	@endif		

	<script type="text/javascript">
		$("#title").bind("keyup change", function(e) 
		{
			let text = $("#title").val();
			let slug = convertToSlug(text);
			$("#slug").val(slug);
		})	

		function convertToSlug(Text)
		{
			return Text
				.toLowerCase()
				.replace(/[^\w ]+/g,'')
				.replace(/ +/g,'-')
				;
		}

		$("#forumForm").submit(function(e) {
			e.preventDefault();
			var formData = new FormData(this);
			$.ajax({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				url: "{{ route('user.forums.storeForum') }}",
				data: formData,
				type: "POST",
				cache:false,
				contentType: false,
				processData: false,
				success: function (response) {
					callProgressBar();
					if(response['status'] == 'success'){
						$('#forumTopicForm').trigger("reset");
						setTimeout(function(){
						window.location = "{{ route('user.forums') }}"; }, 1000);
					}
				},
					error: function (data) {
						let errors = data.responseJSON.errors;
						
						$.each(errors, function(key, value) {
							errorAlert(value[0],3000,'top-right');
						});					
						
					}
			});
		});

</script>
</html>
@endsection