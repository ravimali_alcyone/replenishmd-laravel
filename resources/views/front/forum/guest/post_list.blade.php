@php
	use App\Library\Services\CommonService;
	$common = new CommonService();
@endphp

@extends('front.layouts.app')

@section('content')
@push('header_scripts')
 <link href="{{asset('assets/css/forum.css')}}" rel="stylesheet" type="text/css" />
@endpush
   <section class="forum_page_name">
        <h1>Forum</h1>
    </section>
    <div class="forum_wrap">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <ul class="nav forum_tabs mb-3" id="pills-tab" role="tablist">
                        <li class="nav-item" role="presentation">
                            <a class="nav-link active" id="pills-category-tab" data-toggle="pill" href="#pills-category" role="tab" aria-controls="pills-category" aria-selected="true">All Post</a>
                        </li>
                        <li class="nav-item" role="presentation">
                            <a class="nav-link" id="pills-latest-forum-tab" data-toggle="pill" href="#pills-latest-forum" role="tab" aria-controls="pills-latest-forum" aria-selected="false">Latest Post</a>
                        </li>
                        <li class="nav-item" role="presentation">
                            <a class="nav-link" id="pills-popular-tab" data-toggle="pill" href="#pills-popular-forum" role="tab" aria-controls="pills-popular" aria-selected="false">Popular Post</a>
                        </li>
                        <li class="nav-item ml-auto" role="presentation">	
                            <a href="{{ route('joinnow') }}" class="btn-primary">Create Post</a>	
                        </li>
                    </ul>
                    <div class="tab-content pb-4" id="pills-tabContent">
                        <div class="tab-pane fade show active" id="pills-category" role="tabpanel" aria-labelledby="pills-category-tab">
                        <div class="topics_table">
							@if($all_topics)
                                <table class="table table-hover table-responsive-md">
                                    <thead>
                                        <tr>
                                            <th scope="col">Topic</th>
                                            <th scope="col">Category</th>
                                            <th scope="col">Users</th>
                                            <th scope="col">Replies</th>
                                            <th scope="col">Activity</th>
                                        </tr>
                                    </thead>
                                    <tbody>
									@foreach($all_topics as $k => $v)
                                        <tr>
                                            <th scope="row"><b><a href="{{ url('view-article') }}/{{ $v['topic']['slug'] }}" style="text-decoration:none;">{{ $v['topic']['title'] }}</a></b></th>
                                            <td>{{ $v['topic']['forum_category'] }}</td>
                                            <td>
												@if($v['comment_user'] && isset($v['comment_user_data']))
                                                <ul class="users_list">
													@foreach($v['comment_user_data'] as $c => $img)
                                                    <li><img src="https://i.pravatar.cc/50?img=1" alt=""></li>                                                   
													@endforeach
                                                </ul>
												@endif
                                            </td>
                                            <td>@if($v['comment']) {{ count($v['comment']) }} @endif</td>
                                            <td>@if($v['comment_time']) {{ $common->get_time_ago(strtotime($v['comment_time'][0]))}}  @endif</td>
                                        </tr>
									@endforeach
                                    </tbody>
                                </table>
								@endif
                            </div>
                        </div>
                        <div class="tab-pane fade" id="pills-latest-forum" role="tabpanel" aria-labelledby="pills-latest-forum-tab">
                            <div class="topics_table">
							@if($latest_topics)
                                <table class="table table-hover table-responsive-md">
                                    <thead>
                                        <tr>
                                            <th scope="col">Topic</th>
                                            <th scope="col">Category</th>
                                            <th scope="col">Users</th>
                                            <th scope="col">Replies</th>
                                            <th scope="col">Activity</th>
                                        </tr>
                                    </thead>
                                    <tbody>
									@foreach($latest_topics as $k => $v)
                                        <tr>
                                            <th scope="row"><b><a href="{{ url('view-article') }}/{{ $v['topic']['topic_id'] }}" style="text-decoration:none;">{{ $v['topic']['title'] }}</a></b></th>
                                            <td>{{ $v['topic']['forum_category'] }}</td>
                                            <td>
												@if($v['comment_user'] && isset($v['comment_user_data']))
                                                <ul class="users_list">
													@foreach($v['comment_user_data'] as $c => $img)
                                                    <li><img src="https://i.pravatar.cc/50?img=1" alt=""></li>                                                   
													@endforeach
                                                </ul>
												@endif
                                            </td>
                                            <td>@if($v['comment']) {{ count($v['comment']) }} @endif</td>
                                            <td>@if($v['comment_time']) {{ $common->get_time_ago(strtotime($v['comment_time'][0]))}}  @endif</td>
                                        </tr>
									@endforeach
                                    </tbody>
                                </table>
								@endif
                            </div>
                        </div>
						
                        <div class="tab-pane fade" id="pills-popular-forum" role="tabpanel" aria-labelledby="pills-popular-forum-tab">
                            <div class="topics_table">
							@if($popular_topics)
                                <table class="table table-hover table-responsive-md">
                                    <thead>
                                        <tr>
                                            <th scope="col">Topic</th>
                                            <th scope="col">Category</th>
                                            <th scope="col">Users</th>
                                            <th scope="col">Replies</th>
                                            <th scope="col">Activity</th>
                                        </tr>
                                    </thead>
                                    <tbody>
									@foreach($popular_topics as $k => $v)
                                        <tr>
                                            <th scope="row"><b><a href="{{ url('view-article') }}/{{ $v['topic']['topic_id'] }}" style="text-decoration:none;">{{ $v['topic']['title'] }}</a></b></th>
                                            <td>{{ $v['topic']['forum_category'] }}</td>
                                            <td>
												@if($v['comment_user'] && isset($v['comment_user_data']))
                                                <ul class="users_list">
													@foreach($v['comment_user_data'] as $c => $img)
                                                    <li><img src="https://i.pravatar.cc/50?img=1" alt=""></li>
                                                    <!--li><img src="https://i.pravatar.cc/50?img=2" alt=""></li>
                                                    <li><img src="https://i.pravatar.cc/50?img=3" alt=""></li>
                                                    <li><img src="https://i.pravatar.cc/50?img=4" alt=""></li>
                                                    <li><span>+2</span></li-->
													@endforeach
                                                </ul>
												@endif
                                            </td>
                                            <td>@if($v['comment']) {{ count($v['comment']) }} @endif</td>
                                            <td>@if($v['comment_time']) {{ $common->get_time_ago(strtotime($v['comment_time'][0]))}}  @endif</td>
                                        </tr>
									@endforeach
                                    </tbody>
                                </table>
								@endif
                            </div>
                        </div>						
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection