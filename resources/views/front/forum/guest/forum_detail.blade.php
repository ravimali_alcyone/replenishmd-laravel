@extends('front.blog_layout.app')
@section('content')

@push('header_scripts')

		<meta name="keywords" content="{{ $blog->post_tags }}" />	
		<meta name="robots" content="max-image-preview:large">	
		
		<!-- Open Graph data, Facebook, LinkenIn etc-->
		<meta property="og:title" content="{{ $blog->title }}" /> 
		<meta property="og:description" content="{{substr(strip_tags($blog->blog_content), 0, 100)}} ..." />
		<meta property="og:image" content="{{ env('APP_URL')."/".$blog->header_image }}" />
		<meta property="og:image:secure_url" content="{{ env('APP_URL')."/".$blog->header_image }}" />
		<meta property="og:image:alt" content="{{ $blog->title }}" />
		<meta property="og:url" content="{{ url()->full()}}" />
		<meta property="og:site_name" content="{{env('APP_NAME')}}" /> 
		<meta property="og:type" content="article" />
		<meta property="og:locale" content="en_US" /> 
		<meta property="article:author" content="{{ $blog->author }}" /> 
		<meta property="article:publisher" content="{{env('APP_URL')}}" />	
		
		<!-- Twitter Card data -->
		<meta name="twitter:card" content="summary">
		<!--meta name="twitter:site" content="@publisher_handle"-->
		<meta name="twitter:title" content="{{ $blog->title }}">
		<meta name="twitter:description" content="{{substr(strip_tags($blog->blog_content), 0, 100)}} ...">
		<!--meta name="twitter:creator" content="@author_handle"-->
		<meta name="twitter:image" content="{{ env('APP_URL')."/".$blog->header_image }}">
		
		
    <style>
        .comment_box {
            padding: 60px 0;
            position: relative;
        }
        .comment_box .content_wrapper>.img_wrapper {
            width: 70px;
            height: 70px;
            border-radius: 50%;
            overflow: hidden;
            margin-right: 30px;
        }
        .comment_box .content_wrapper {
            display: flex;
            flex-wrap: wrap;
            justify-content: flex-start;
            align-items: center;
            border-bottom: 1px solid #E5E5E5;
            padding: 30px 0;
        }
        .comment_box .content_wrapper .text_wrapper {
            width: calc(100% - 100px);
        }
        .comment_box .content_wrapper p {
            margin-top: 16px;
        }
        .comment_box .content_wrapper .name{
            color: #2c99cb;
            font-weight: 600;
            display: inline-block;
            margin-right: 20px;
        }
        .comment_box .form_box {
            margin-top: 48px;
        }
        .comment_box .form_box .title {
            font-size: 30px;
        }
        .comment_box .fa {
            display: inline-flex;
            width: 36px;
            height: 36px;
            border: 1px solid #ff6c66;
            border-radius: 50%;
            align-items: center;
            justify-content: center;
            font-size: 18px;
            margin-right: 6px;
        }
        .comment_box .heading_wrapper {
            display: flex;
            flex-wrap: wrap;
            justify-content: space-between;
            align-items: center;
        }
		
		html {
			scroll-behavior: auto!important;
		}	
    </style>
@endpush
	
    <div class="content_block clearfix">
        <section class="banner_sec" style="background: url('{{ env('APP_URL')."/".$blog->header_image }}'); background-repeat: no-repeat; background-size: cover;">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-md-12 col-lg-12">
                        <div class="content_wrapper">
                            <div>
                                <div class="post-categories"><a href="{{ env('APP_URL')}}/blogs/category/{{ $blog->blog_category_id }}"><span class="cat-dot" style="background-color: #994DB1;"></span><span class="cat-title">{{ $blog->category_name }}</span></a></div>
                                <h2 class="heading">{{ $blog->title }}</h2>
                                <div class="custom_beradcrumb">
                                    <span>By {{ $blog->author }}</span>
                                    <span>{{ date('F d, Y',strtotime($blog->created_at)) }}</span>
                                    <!--span>5 Mins read</span-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>


		<section class="detail_sec">
			<div class="uk-section">
				<div class="uk-container">					
						{!! $blog->lb_content ?? '' !!}
				</div>
			</div>
		</section>

		
	@if($similar_blogs && $similar_blogs->count() > 0)
        <div class="bg-light pt-5 pb-5">
            <div class="container">
                <div class="row">
				@foreach($similar_blogs as $key => $value)
                    <div class="col-md-3 col-sm-6">
                        <div class="post_box">
                            <div class="post_img">
                                <div class="rmd-post-format-icon"></div><img class="img-fluid" src="{{ env('APP_URL')."/".$value->image }}" alt="{{ $value->title }}"></div>
                            <div class="rmd_post_details_inner">
                                <div class="post-categories"><a href="#"><span
                                            class="cat-dot"
                                            style="background-color: #160600;"></span><span
                                            class="cat-title">{{ $value->category_name }}</span></a></div>
                                <h3 class="post-title entry-title"><a href="{{ route('front.blogs.post',$value->slug)}}">{{ $value->title }}</a></h3>
                                <div class="post-date"><time class="entry-date published updated" datetime="{{ $value->created_at }}">{{ date('F d, Y',strtotime($value->created_at)) }}</time></div>
                                <!--div class="post-info-dot"></div>
                                <div class="post-read-time">5 Mins read</div-->
                            </div>
                        </div>
                    </div>
				@endforeach
                </div>
            </div>
        </div>
	@endif
	
	@if($blog->post_comment == '1')		
    	<section class="comment_box" id="comment-section">
    	    <div class="container">
    	        <div class="row">
    	            <div class="col-sm-12">
    	                <div class="heading_wrapper">
    	                    @if($comments)<h4 class="title text-left mb-0" id="comment-count">{{$comments->count()}} Comments</h4>@endif
    	                    @if(!$comments)<h4 class="title text-left mb-0" id="comment-count">0 Comments</h4>@endif
							
							@php 
								$url = env('APP_URL').'/blogs/post/'.$blog->slug;
							@endphp
    	                    <ul class="social_links">
                                <li>
									<a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u={{$url}}"><i class="fab fa-facebook-f" title="Share it on Facebook"></i></a>
								</li>
                                <li>
									<a target="_blank" href="https://twitter.com/share?url={{$url}}" title="Share it on Twitter"><i class="fab fa-twitter"></i></a>									
								</li>
                                <li>
									<a target="_blank" href="https://www.linkedin.com/shareArticle?mini=true&amp;url={{$url}}&amp;title={{$blog->title}}&amp;summary={{env('APP_URL')}}&amp;source=ReplenishMD" title="Share it on Linkedin"><i class="fab fa-linkedin"></i></a>
								</li>														
                            </ul>
    	                </div>
						
				@if($comments && $comments->count() > 0)
					<div id="blog-comments">
					@foreach($comments as $key => $value)						
    	                <div class="content_wrapper">
	                        <div class="img_wrapper">
	                            <img src="/{{$value->author_image}}" />
	                        </div>
	                        <div class="text_wrapper">
	                            <div>
									@if(auth()->user() && auth()->user()->user_role == '3' && $value->author_role != 1)
										<a href="{{env('APP_URL')}}/provider/view/{{$value->author_id}}" class="name">{{ $value->author_name }}</a>
									@elseif(auth()->user() && auth()->user()->user_role == '4' && $value->author_role != 1)
										<a href="{{env('APP_URL')}}/user/view/{{$value->author_id}}" class="name">{{ $value->author_name }}</a>
									@else 
										<a href="javascript:void(0);" class="name">{{ $value->author_name }}</a>
									@endif
									<span class="date">{{ date('F d, Y h:i a',strtotime($value->created_at)) }}</span></div>
	                            <p>{{ $value->comments }}</p>
	                        </div>
    	                </div>
					@endforeach
					</div>
				@endif
					
					@if(auth()->user())
    	                <div class="form_box">
	                        <h4 class="title text-left">Leave a Comment</h4>
	                        <form id="blogComment">
                                <div class="form-group">
                                    <label for="exampleFormControlTextarea1">Comment</label>
									<input type="hidden" name="blog_id" value="{{$blog->id}}"/>
                                    <textarea name="comments" class="form-control" id="exampleFormControlTextarea1" rows="4" required></textarea>
                                </div>
                                <button id="sbBtn" type="submit" class="btn btn-primary border-0">Submit</button>
                            </form>
    	                </div>
					@endif
    	            </div>
    	        </div>
    	    </div>
    	</section>
	@endif

	<section class="work_join_sec"></section>		
    </div>

@if($section && $section!= '')
<script>
	$(document).ready(function () {
		$('html, body').animate({
				scrollTop: $("#{{$section}}").offset().top
			});
		
	});
</script>
@endif
<script>
						
	$("#blogComment").submit(function(e) {
		e.preventDefault();
		$("#sbBtn").attr('disable','true');
		
		$.ajax({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			url: "{{ route('front.blogs.comments')}}",
			data: $("#blogComment").serialize(),
			type: "POST",
			cache:false,
			success: function (response) {
				if(response['status'] == 'success'){
					$("#exampleFormControlTextarea1").val('');
					callProgressBar();
				setTimeout(function(){
					location.href = "{{ env('APP_URL').'/blogs/post/'.$blog->slug }}?section=comment-section";
				}, 500);					
				}
			},
			error: function (data) {
				console.log(data);
			}
		});
	});
	
	function callProgressBar(){
		$("#pbar").css('display', 'block');
		var $progress = $('.progress');
		var $progressBar = $('.progress-bar');
		setTimeout(function() {
			$progressBar.css('width', '10%');
			setTimeout(function() {
				$progressBar.css('width', '30%');
					setTimeout(function() {
						$progressBar.css('width', '60%');
						setTimeout(function() {
							$progressBar.css('width', '100%');
							setTimeout(function() {
								$progressBar.css('width', '0');
								$("#pbar").css('display', 'none');
							}, 500); // WAIT 5 milliseconds
						}, 1500); // WAIT 2 seconds
				}, 1500); // WAIT 2 seconds
			}, 1000); // WAIT 1 seconds
		}, 1000); // WAIT 1 second
	}	

</script>

@endsection