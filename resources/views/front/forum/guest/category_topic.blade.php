@extends('front.layouts.app')
@section('content')
<?php
use App\Library\Services\CommonService;
$common = new CommonService();
?>
<style>
    .topic_card{
    border: 1px solid #605c5c1f;
    min-height: 200px;
    border-radius: 10px;
    background: #fff;
    box-shadow: 0px 7px 29px #605c5c1f;
    }
    .topic_foot{ border-radius:0 0px 10px 10px}
    .topic_foot .form-group{ margin-bottom:0}
    .com_head {
        box-shadow: 1px 1px 1px 1px #605c5c1f;
        align-items: center;
    }
    .profile {
        display: flex;
    }
    img.circle-profil-img {
        width: 80px;
        border-radius: 10px;
        height: 80px;
    }
    .consult_btn {
        background: #fd6b65;
        border-radius: 25px;
        padding: 9px 10px;
        color: #fff;
    }
    .consult_call_btn {
        background: #ededed;
        border-radius: 20px;
        padding: 8px 18px;
        color: #fff;
    }
    
    .advertisement
    {
        background: #ededed;
        min-height: 400px;
        border-radius: 10px;
    }
    .text_box
    {
        background: #ededed;
        padding: 45px 20px 30px 20px;
    }
    .title_card{
    border: 1px solid #605c5c1f;
    min-height: 160px;
    border-radius: 10px;
    background: #fff;
    box-shadow: 0px 7px 29px #605c5c1f;

}

i.far.fa-user {
    border-radius: 50%;
    background-color:#05caf11c;
    width: 35px;
    height: 35px;
    padding: 10px;
}
img.user_small_pic {
    border-radius: 50%;
    background-color:#05caf11c;
    width: 35px;
    height: 35px;
    padding: 0px; object-fit:cover; margin-right:5px;
}

.read_link a {
    color: #05caf1;
    font-size: 15px;
    line-height: 2.2;
}
a.card-link {
    color: #6c6b6b;
    font-size: 17px;
}
.title h3 {
    text-align: left;
    font-size: 20px;
    padding: 20px;
}
p.card-text {
    color: #6c6b6b;
    font-size: 15px;
    line-height: 28px;
}
.title_sec b{
   
    font-size: 15px;
    color: #201e1ecf;
}
.topic_head_3 h6{
    margin: 0px;
}
/* right side bar css */
.frame_box {
    border: 1px solid #605c5c1f;
    min-height: 200px;
    border-radius: 10px;
    background: #fff;
    box-shadow: 0px 7px 29px #605c5c1f;
}
.frame_box h5 {
    font-size: 23px;
    color: black;
    font-weight: 500;
    line-height: 2.1;
    margin-bottom: 10px;
    align-items: center;
    width: 100%;
}
.pro_wrraper {
    width: 100%;
   
}
.profile {
    display: flex;
    margin-bottom: 10px;
}
.detail b {
    font-size: 17px;
    color: #201e1ecf;
    line-height: 1.2;
    
}
.detail p {
    font-size: 15px;
    color: #1817179e;
    font-weight: 600;
}   
.round_btn{
        text-align: right!important;
        margin-right: 8px;
        display: flex;
    justify-content: end;
    margin-left: 93px;
    }


    .btn-primary {
        color: #fff;
        background-color: var(--primary_color);
        height: 40px;
        padding: 0 10px!important;
       
        justify-content: center;
        align-items: center;
        font-size: 1rem;
        font-weight: normal;
        border-radius: 25px;
        width: 183px;
        transition: all ease-in-out .3s;
        box-shadow: 0 6px 12px rgb(0 0 0 / 16%);
    }
    .topic_foot .form-group{
        margin-top: 20px;
    }
</style>
<section class="blog_cat_top">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="blog_top_bredcum">
                    <li>Your Feed</li>
                </ul>
                
            </div>
        </div>
    </div>
</section>
<?php
//echo '<pre>';
//print_r($post_detail); die;
?>
<div class="content_block clearfix">
        <div class="latest_posts pt-4 pb-4">
            <div class="container mt-4">
                <div class="row ">
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="d-flex justify-content-between mb-2">
                                <h6><i class="far fa-user"></i>&nbsp;&nbsp;&nbsp;<strong>{{$post_detail['name']}}</strong>&nbsp;&nbsp;&nbsp;&nbsp;{{$post_detail['city']}}</h6>
                                @if($post_like==0)
                                    <a href="javascript:void(0)" class="ml-auto" onclick="doLike(this.id)" id="{{$post_detail['id']}}" title="Like It?"><i class="far fa-heart text-danger"></i></a>
                                @else
                                    <a href="javascript:void(0)" class="ml-auto"  id="{{$post_detail['id']}}" title="Liked"><i class="fas fa-heart text-danger"></i></a>
                                @endif    
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="topic_card">
                                <div class="topic_head p-3">
                                    <strong>{{$post_detail['title']}}</strong>
                                    <span class="topic_date text-sm-left d-block mt-2">{{ date_format(date_create($post_detail['created_at']),"d M Y H:i") }} | {{ $common->get_time_ago(strtotime($post_detail['created_at']))}} post</span>
                                </div>
                                <div class="topic_body p-3">
                                    {!! $post_detail['description'] !!}
                                    <!--<div class="help_btn mt-5"><a href="javascript:void(0)" class="consult_btn">Helpful</a></div>-->
                                </div>
                                <div class="d-flex justify-content-end">
                                    <?php 
                                        $userId = (isset(auth()->user()->id)) ? auth()->user()->id : 0 ;
                                        if($userId==0) { $backUrl = request()->segment(1).'/'.request()->segment(2); session()->put('post_comment_check', $backUrl); }
                                    ?>
                                    <button name="comment_post" id="comment_post" class="btn-primary mr-5 mb-4" style="border:none;" onclick="doComment(<?php echo $userId;?>)">Reply</button>
                                </div>
                                <form id="post_comment" class="d-none">
                                    <div class="topic_foot bg-light border-top pt-4 pl-5 pr-5 pb-5">
                                        <div class="form-group">
                                            <lable>Comment</lable>
                                            <textarea name="comment" id="comment" col="30" rows="3" class="form-control"></textarea>
                                        </div>
                                        <div class="form-group d-flex justify-content-end">
                                            <input type="hidden" name="post_id" id="post_id" value="{{$post_detail['id']}}">
                                            <button name="comment_post" id="comment_post" class="btn-primary" style="border:none;">Submit</button>
                                        </div>
                                    </div>
                                </form>
                                
                            </div>
                            <!-- my code start -->
                            <div class="title">
                                <h3>Related Reviews</h3>
                            </div>
                            @if($comment && !empty($comment))
                                @foreach($comment as $val)
                                <div class="title_card mb-3">
                                    <div class="topic_head_3 pt-3 pr-3 pl-3 d-flex justify-content-between align-items-center">
                                        <h6>
                                        @if($val->image!="")
                                            <img class="user_small_pic" src="/{{$val->image}}">
                                            @else
                                            <i class="far fa-user"></i>
                                            @endif
                                            
                                            <strong>{{$val->name}}</strong>
                                        </h6>
                                        <span class="topic_date text-sm-right">{{ date_format(date_create($val->created_at),"d M Y H:i") }} | {{ $common->get_time_ago(strtotime($val->created_at))}} post</span>
                                    </div>
                                    <div class="title_sec p-3">
                                        <p class="card-text">{{$val->comment_content}}
                                        </p>
                                        <!-- <div class="read_link">
                                            <a href="#" class="card-link_1">Read More</a>
                                        </div> -->
                                        <!--<span style="font-size:16px;color:#ff6c66;" id="last_count">20</span> <span style="font-size:16px;color:#ff6c66;"><a href="#" id="{{$val->id}}" onclcik="doLike(this.id,$('#last_count').text())">Likes</a></span>-->
                                    </div>
                                </div>
                                @endforeach
                            @endif
                        </div>
                    </div>
                </div>
                <div class="col-md-4 main_sidebar">
                    <div class="col-sm-12 frame_box p-4">
                        <h5>Treatment Provider</h5>
                        <div class="pro_wrraper">
                            <div class="profile">
                                <div class="image-box mt-1">
                                    <img src="/{{$post_detail['image']}}"  class="circle-profil-img" />
                                </div>
                                <div class="detail ml-4">
                                    <p><b>{{$post_detail['name']}}</b></p>
                                    <p>.......</p>
                                </div>
                            </div>
                            <div class="text-right round_btn">
                                <a href="javascript:void(0)" class=" btn-primary"> Get a Consultation</a>
                                <a href="javascript:void(0)" class="consult_call_btn ml-2"> <i
                                        class="fas fa-phone-alt"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 advertisement mt-3 p-5">
                        <p class="">Advertisement</p>
                    </div>
                </div>

                </div>
            </div>
        </div>
    </div>
    <section class="blog_cat_foot">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="blogs_topic_nav_ul">
                        <h6>FILTER BY DIABETES TOPICS</h6>
                        <ul class="bctt_list">
                            <li><a class="active" href="#">All</a></li>
                            <li><a href="#">Diagnosis</a></li>
                            <li><a href="#">Healthy Eating</a></li>
                            <li><a href="#">Lifestyle</a></li>
                            <li><a href="#">Living with Diabetes</a></li>
                            <li><a href="#">Research</a></li>
                            <li><a href="#">Seasonal</a></li>
                            <li><a href="#">Testing</a></li>
                        </ul>
                        <div class="blogs_topic_nav_sel">
                            <select class="custom-select">
                              <option value="all">All</option>
                              <option value="diabetes-diagnosis">Diagnosis</option>
                              <option value="healthy-eating">Healthy Eating</option>
                              <option value="diabetes-lifestyle">Lifestyle</option>
                              <option value="living-with-diabetes">Living with Diabetes</option>
                              <option value="diabetes-research">Research</option>
                              <option value="diabetes-seasonal">Seasonal</option>
                              <option value="testing">Testing</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    <script>
        
        $("#post_comment").submit(function(e) {
			e.preventDefault();
			
			$.ajax({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				data: $('#post_comment').serialize(),
				url: "{{ route('post.comment') }}",
				type: "POST",				
				success: function (response) {
					if(response['status'] == 'success'){
                        callProgressBar(); 
                        $("#comment").val('');                       
					}
                    else{
                        errorAlert(response['message'],3000,'top-right');						
                    }
				},
				error: function (data) {
                    let errors = data.responseJSON.errors;
                    $.each(errors, function(key, value) {
                        errorAlert(value[0],3000,'top-right');
                    });					
                }
			});
		});

        function doLike(postId)
        {
            $.ajax({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				data: {"post_id":postId},
				url: "{{ route('forums.post.like') }}",
				type: "POST",				
				success: function (response) {
					if(response['status'] == 'success'){
                        callProgressBar(); 
                        //$("#comment").val('');                       
					}
                    else{
                        errorAlert(response['message'],3000,'top-right');						
                    }
				},
				error: function (data) {
                    let errors = data.responseJSON.errors;
                    $.each(errors, function(key, value) {
                        errorAlert(value[0],3000,'top-right');
                    });					
                }
			});
        }

        function doComment(userId)
        {
            if(userId==0)
            {
                window.location = "{{ route('sign_in') }}";
            }
            else
            {
                $("#comment_post").hide();
                $("#post_comment").removeClass('d-none');
            }
        }
    
    </script>
@endsection

