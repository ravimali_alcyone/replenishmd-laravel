@extends('front.common_layout.app')
@section('content')
@php $type = auth()->user()->user_role == 4 ? 'user' : 'provider'; @endphp
	@if(auth()->user()->user_role==3)
		<div class="col-xl-10 col-lg-8 col-md-8 col-sm-12">
	@endif
		<div class="flex flex-col sm:flex-row items-center mt-4 border-b border-gray-200 dark:border-dark-5">
			<h3><a href="<?php echo route($type.'.forums'); ?>">Froum list</a> > Post list</h3>
		</div>
		<div class="main-center-data">
			<div class="row mt-15">		
				<div class="col-xl-10 col-lg-10 col-md-12 col-sm-12">
					
					<div class="filters_wrapper">
						<div class="data-list-filters">
							<label>Category</label>
							<select id="filter_category" class="custom-select select2">
								<option value="">Any</option>
								@if($categories && $categories->count() > 0)
									@foreach($categories as $category)
										<option value="{{ $category->id }}" >{{ $category->forum_subject }}</option>
									@endforeach
								@endif
							</select>
						</div>

						<div class="data-list-filters">
							<label>Status</label>
							<select id="publish_status" class="custom-select select2">
								<option value="" class="any">Any</option>
								<option value="1">Active</option>
								<option value="0">Inactive</option>
							</select>
						</div>

						<div class="data-list-filters">
							<label>Comment</label>
							<select id="comment_status" class="custom-select select2">
								<option value="" class="any">Any</option>
								<option value="1">Allow</option>
								<option value="0">Not-allow</option>
							</select>
						</div>

						<!--<div class="data-list-filters">
							<label>Topic Type</label>
							<select id="pay_status" class="custom-select select2">
								<option value="" class="any">Any</option>
								<option value="1">Paid</option>
								<option value="0">Unpaid</option>
							</select>
						</div>-->
						<div class="data-list-filters pt-4">
							<div class="mt-3 pt-1">
							<button type="button" class="btn btn-sm btn-secondary" id="filter">Show</button>
							
							<button type="reset" class="btn btn-sm btn-danger ml-2" id="reset">Reset</button>
							</div>
						</div>	
					</div>
				</div>
				
				<div class="col-xl-1 col-lg-2 col-md-12 col-sm-12 pull-right mt-5 pt-2"><a href="{{ url($type.'/forums/add/0') }}" class="btn btn-success btn-sm">Add New</a></div>					
			</div>
			
			<div class="row mt-15 bg-white round-crn pd-20-30 hover-effect-box">
				<div class="col-xl-12 col-lg-12 col-md-12">			
					<div id="patients_data">
						<table id="myTable" class="table table-striped table-bordered" style="width:100%">
							<thead>
								<tr class="intro-x">
									<th class="border-b-2">S.No.</th>
									<th class="border-b-2">Forum Subject</th>
									<th class="border-b-2">Post Heading</th>
									<th class="border-b-2">Author</th>
									<th class="border-b-2">Status</th>
									<th class="border-b-2">Comment</th>
									<!--<th class="border-b-2">Paid</th>-->
									<th class="border-b-2">Action</th>
								</tr>
							</thead>
							<tbody></tbody>
						</table>			
					</div>
				</div>
			</div>

		</div>
	@if(auth()->user()->user_role==3)
		</div>
	@endif		
	
@push('scripts')
<script type="text/javascript">
	var table;
 	$(document).ready(function() {
		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});

		fill_datatable();
		
		function fill_datatable(filter_category = '', publish_status = '', comment_status = ''){
			
			table = $('#myTable').DataTable({
				processing: true,
				serverSide: true,
				responsive: true,
				paging: true,
				ordering: false,
				info: false,
				displayLength: 10,
				language: {
					processing: '<img src="/dist/images/loading.gif"/>'
				},				
				lengthMenu: [
				[10, 25, 50, -1],
				[10, 25, 50, "All"]
				],
				ajax:{
					url: "{{ route('user.forums.post') }}",
					data:{"filter_category":filter_category,"publish_status":publish_status, "comment_status":comment_status }
				},
				columns: [
					{data: 'DT_RowIndex', name: 'DT_RowIndex'},
					{data: 'forum_subject', name: 'forum_subject'},
					{data: 'title', name: 'title'},
					{data: 'author', name: 'author'},
					{data: 'status', name: 'status'},
					{data: 'comment_on', name: 'comment_on'},
					{data: 'action', name: 'action', orderable: false, searchable: false},
				]
			});
		}

		$('#filter').click(function(){
			var filter_category = $('#filter_category').val();
			var publish_status = $('#publish_status').val();
			var comment_status = $('#comment_status').val();
			
			$('#myTable').DataTable().destroy();
			fill_datatable(filter_category,publish_status,comment_status);
		});

		$('#reset').click(function(){

			$('#filter_category').prop('selectedIndex', 0);
			$('#filter_category').select2();

			$('#publish_status').prop('selectedIndex', 0);
			$('#publish_status').select2();
			
			$('#comment_status').prop('selectedIndex', 0);
			$('#comment_status').select2();

			$('#myTable').DataTable().destroy();
			fill_datatable();
		});

		$("#myTable tbody tr").addClass('intro-x');
  	});

    function deleteRow(id) {
		swal({
			title: "Are you sure to delete?",
			text: "",
			icon: 'warning',
			buttons: {
			  cancel: true,
			  delete: 'Yes, Delete It'
			}
		  }).then((isConfirm) => {
			if (!isConfirm) {
				return false;
			} else {
				$.ajax({
					type: "DELETE",
					url: "{{ env('APP_URL').'/user/forums/destroy' }}"+'/'+id,
					success: function (response) {
						if(response['status'] == 'success'){
						swal({
							title: response['message'],
							icon: 'success'
						});
						setTimeout(function(){
						swal.close();
						}, 1000);
						}
						table.draw();
					},
					error: function (data) {
						console.log('Error:', data);
						swal({
							title: 'Error Occured',
							icon: 'error'
						});
					}
				});
			}
        });
    }

	function changeStatus(id,status,flag) {
		$.ajax({
			url: "{{ url('user/forums/change_status') }}",
			type: "POST",
			data: {'id': id, 'status':status, 'flag':flag},
			success: function(response) 
			{
				if(response['status'] == 'success')
				{
					swal({
						title: response['message'],
						icon: 'success'
					});
					setTimeout(function(){
					swal.close();
					}, 1000);
				}
				table.draw();
			}
		});
	}
</script>
@endpush

@endsection