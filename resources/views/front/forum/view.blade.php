@extends('front.common_layout.app')
@section('content')
	<script src="https://unpkg.com/react@16.8.6/umd/react.production.min.js"></script>
  	<script src="https://unpkg.com/react-dom@16.8.6/umd/react-dom.production.min.js"></script> 

  	<script src="{{ asset('vendor/laraberg/js/laraberg.js') }}"></script>
  	<script src="{{ asset('js/admin-app.js') }}" defer></script>
	<link rel="stylesheet" href="{{asset('vendor/laraberg/css/laraberg.css')}}">
	
    <link rel="stylesheet" href="{{asset('css/admin-app.css')}}">  
	<style type="">
    .blog_by {
        display: flex;
        justify-content: space-between;
        align-items: center;
        padding: 15px 0;
    }
    .bloger_name {
        font-weight: bold;
    }
    .blog_desc {
        border-bottom: 1px solid #dedede;
        margin-bottom: 10px;
        padding-bottom: 15px;
    }
    .blog_info {
        padding: 15px 0;
        border-bottom: 1px solid #dedede;
        margin-bottom: 10px;
    }
    .blog_preview {
        width: 100%;
        max-width: 800px;
        margin: 10px auto;
    }
    .grid-cols-12 {
        grid-template-columns: repeat(12, minmax(0, 1fr));
    }

    .gap-6 {
        grid-gap: 1.5rem;
        gap: 1.5rem;
    }
    .grid {
        display: grid;
    }
    .col-span-12 {
        grid-column: span 12 / span 12;
    }
    .p-3 {
        padding: 0.75rem;
    }
    .box {
        box-shadow: 0px 3px 20px #0000000b;
        --bg-opacity: 1;
        background-color: #fff;
        background-color: rgba(255, 255, 255, var(--bg-opacity));
        border-radius: 0.375rem;
        position: relative;
    }
    .blog_preview .blog_title {
        font-size: 30px;
        text-transform: capitalize;
        font-weight: 600;
    }
    .pl-3 {
        padding-left: 0.75rem;
    }
    .pr-3 {
        padding-right: 0.75rem;
    }

    .p-1 {
        padding: 0.25rem;
    }
    .mb-2 {
        margin-bottom: 0.5rem;
    }
    .inline-block {
        display: inline-block;
    }
    .rounded-lg {
        border-radius: 0.5rem;
    }
    .bg-gray-200 {
        --bg-opacity: 1;
        background-color: #edf2f7;
        background-color: rgba(237, 242, 247, var(--bg-opacity));
    }
    </style>
    <!-- BEGIN: Content -->
   
	<div>
        <div class="col-xl-12 col-lg-12 col-md-12">
			<h3 class="font-medium text-base mr-auto"><a href="{{ url()->previous() }}">Blogs List</a> > Post Details</h3>
		</div>
        
        <!-- END: Profile Info -->
        <div class="tab-content mt-5">
            <div class="tab-content__pane active" id="blog_detail_page">
                <div class="grid grid-cols-12 gap-6">

                    <div class="col-span-12 box p-3">
                    <div class="blog_preview">
                        <div class="blog_title">{{ $blog->title ?? ''}}</div>
                        <div class="blog_info">
                            <div class="blog_cat rounded-lg bg-gray-200 p-1 pl-3 pr-3 mb-2 inline-block"><b>Category Name -</b> {!! ucfirst($category) ?? '' !!}</div>
                            <div class="tags">
                            <b>Tags -</b> {{ $blog->post_tags ?? ''}}
                            </div>
                        </div>
                        <div class="blog_desc">
                                {!! $blog->blog_content ?? '' !!}
                        </div>
                        <div class="blog_by">
                            <div class="bloger_name">{!! ucfirst($author_name) ?? '' !!}</div>
                            <div class="bloger_time">{!! $blog->created_at ?? '' !!}</div>
                        </div>
                    </div>
                    </div>
                </div>

                <div class="col-xl-2 col-lg-2 col-md-2">
                    <div class="">
                        <div class="relative flex items-center">
                            <button type="button" class="btn btn-primary mt-3" onclick="javascript:location.href = '{{ url()->previous() }}'">Back</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END: Content -->
    </div>

    <script>
        $(document).ready(function() {
            $("#blog_info").find('table').addClass('table table-report table-report--bordered sub_admin_table');

            $(".blog_thumbnail_section img").click(function() {
                $(".blog_main_img img").attr('src', $(this).attr('src')).attr('data-zoom', $(this).attr('src'));
            });
            $(".blog_main_img img").mouseover(function() {
                $(this).css('opacity', '0');
            });
            $(".blog_main_img img").mouseout(function() {
                $(this).css('opacity', '1');
            });
        });
    </script>
@endsection