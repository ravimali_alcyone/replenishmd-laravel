@extends('front.common_layout.app')
@section('content')
	@php $type = auth()->user()->user_role == 4 ? 'user' : 'provider'; @endphp

	<style>
		.select2-container--default .select2-selection--single {
			min-height: 37px;
		}		
		
		.select2-container--default .select2-selection--single .select2-selection__rendered {
			line-height: 32px;
		}		
	</style>
	
	@if(auth()->user()->user_role==3)
		<div class="col-xl-10 col-lg-8 col-md-8 col-sm-12">
	@endif
		<div class="flex flex-col sm:flex-row items-center mt-4 border-b border-gray-200 dark:border-dark-5">
			<h3 class="font-medium text-base mr-auto"><a href="<?php echo route($type.'.forums.post'); ?>">Froum List</a> > @if($forum_topic) {{ 'Edit' }} @else Add New @endif Post</h3>
		</div>
		<div class=" mt-15 bg-white round-crn pd-20-30 hover-effect-box">
		<form id="forumTopicForm">
				<div class="row">
					<div class="col-sm-10 pl-0">
						<div class="row">
							<div class="form-group col-sm-12">
								<label for="email">Title</label>
								<input type="text" name="title" value="<?php if($forum_topic){ echo $forum_topic->title;}?>" class="form-control border mt-2" placeholder="Title" id="title" required>
							</div>

							<div class="form-group col-sm-12">
								<label for="email">Slug</label>
								<input type="text" name="slug" value="<?php if($forum_topic){ echo $forum_topic->slug;}?>" class="form-control border mt-2" placeholder="Slug" id="slug" required>
							</div>
						</div>
						<div class="row">
							<div class="form-group col-sm-6">
								<label for="email">Forum</label>
								<select id="category_id" name="category" class="select2 form-control" required>
									<option value="">--Select--</option>
									@if($categories && $categories->count() > 0)
										@foreach($categories as $category)
											<option value="{{ $category->id }}" <?php if($forum_topic && $forum_topic->forum_category_id == $category->id){ echo 'selected';}?> >{{ $category->forum_subject }}</option>
										@endforeach
									@endif
									
								</select>
							</div>
						</div>
						<div class="row">
							<div class="form-group col-sm-12">
								<label for="email">Description</label>
								<textarea name="description" id="description" class="form-control border mt-2"><?php if($forum_topic){ echo $forum_topic->description; }?></textarea>
							</div>
						</div>
						
					</div>
					<div class="col-md-2  pt-3" style="background-color: #f7fafc;">
						<div class="intro-y col-span-12">
							<div class="grid grid-cols-12  gap-6">
								<div class="col-span-12">
									<label>Status</label>
									<div class="mt-2">
										<input type="radio" id="status_1" name="status" value="1" <?php if(!$forum_topic){ echo 'checked';} ?> class="mr-3" <?php if($forum_topic && $forum_topic->status==1){ echo "checked"; } ?> ><label for="status_1" class="mr-4">Active</label>
										<input type="radio" id="status_0" name="status" value="0" class="mr-4" <?php if($forum_topic && $forum_topic->status==0){ echo "checked"; } ?> ><label for="status_0">Inactive</label>
									</div>
								</div>
								<!--<div class="col-span-12">
									<label>Paid?</label>
									<div class="mt-2">
										<input type="radio" id="is_paid_1" name="is_paid" value="1" <?php if(!$forum_topic){ echo 'checked';} ?> class="mr-3" <?php if($forum_topic && $forum_topic->is_paid==1){ echo "checked"; } ?> ><label for="is_paid_1" class="mr-4">Yes</label>
										<input type="radio" id="is_paid_0" name="is_paid" value="0" class="mr-4" <?php if($forum_topic && $forum_topic->is_paid==0){ echo "checked"; } ?> ><label for="is_paid_0">No</label>
										
									</div>
								</div>-->
								<div class="col-span-12">
									<label>Allow Comment</label>
									<div class="mt-2">
										<input type="checkbox" name="comment_on" id="comment_on" value="1" class="form-checkbox" <?php if($forum_topic && $forum_topic->comment_on == 1){ echo "checked"; } ?>>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="form-group mt-12">
					<div class="intro-y col-span-12 lg:col-span-12">
						<input type="hidden" name="id" value="<?php if($forum_topic){ echo $forum_topic->id;}?>"/>
						<input type="hidden" name="page" value="<?php if($forum_topic){ echo 'edit';} else { echo 'add'; }?>"/>
						<input type="hidden" name="manul_cat_flag" id="manul_cat_flag" value="">
						<button type="submit" id="saveBtn" class="btn btn-primary">Save</button>
						<button type="reset" class="btn btn-danger ml-2" onclick="location.href = '{{ route('user.forums') }}';">Cancel</button>
					</div>
				</div>
			</form>
		</div>
	@if(auth()->user()->user_role==3)
		</div>
	@endif		


	<div class="modal fade" id="cropImageModal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-body">
					<div class="img-container">
						<div class="row">
							<div class="col-md-8">
								<img src="" id="sample_image" />
							</div>
							<div class="col-md-4">
								<div class="crop-preview"></div>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" id="crop" class="btn btn-primary">Crop</button>
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
				</div>
			</div>
		</div>
	</div>	
	<script src="{{ asset('plugins/ckeditor/ckeditor.js') }}"></script>	
	<script type="text/javascript">
	CKEDITOR.replace('description',{
			height: '400px',
	});
	CKEDITOR.add;

	$("#category_id").select2();
	$("#topic_ids").select2({placeholder: "Select Topics"});
	
	$("#title").bind("keyup change", function(e) {
		let text = $("#title").val();
		let slug = convertToSlug(text);
		$("#slug").val(slug);
	})	

	function convertToSlug(Text)
	{
		return Text
			.toLowerCase()
			.replace(/[^\w ]+/g,'')
			.replace(/ +/g,'-')
			;
	}

	$("#forumTopicForm").submit(function(e) {
		e.preventDefault();
		var description = CKEDITOR.instances.description.getData();

		var formData = new FormData(this);
		formData.append('description', description);
		
		$.ajax({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			url: "{{ route('user.forums.store') }}",
			data: formData,
			type: "POST",
			cache:false,
			contentType: false,
			processData: false,
			success: function (response) {
				callProgressBar();
				if(response['status'] == 'success'){
					$('#forumTopicForm').trigger("reset");
					setTimeout(function(){
					window.location = "{{ route('user.forums.post') }}"; }, 1000);
				}
			},
			error: function (data) {
				let errorArr = [];
				let errors = data.responseJSON.errors;
				let emailErr = errors.email[0];
			}
		});
	});

</script>
</html>
@endsection