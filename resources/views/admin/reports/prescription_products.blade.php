@extends('layouts.app')
@section('content')
	<style>
		div#myTable_processing {
			position: absolute;
			top: 0;
			left: 0;
			width: 104%;
			height: 108%;
		}
	</style>
	<div class="intro-y flex flex-col sm:flex-row items-center mt-8">
		<h2 class="text-lg font-medium mr-auto">{{ $heading }}</h2>
	</div>

	<div class="grid grid-cols-12 gap-6 mt-5">

		<div class="intro-y col-span-12 items-center">
			<form id="tableFilters">
			
				<div class="col-span-4">
					<label>Service/Treatment</label>
					<select id="filter1" class="select2 w-full">
						<option value="">Any</option>
						@if($services && $services->count() > 0)
							@foreach($services as $service)
								<option value="{{ $service->id }}" >{{ $service->name }}</option>
							@endforeach
						@endif
					</select>
				</div>
				
				<div class="col-span-4">
					<label>Prescription Product</label>
					<select id="filter2" class="select2 w-full">
						<option value="">Any</option>
						@if($medicines && $medicines->count() > 0)
							@foreach($medicines as $medicine)
								<option value="{{ $medicine->id }}" >{{ $medicine->name }}</option>
							@endforeach
						@endif
					</select>
				</div>
				
				<div class="w-auto mt-3 mt-md-0">
					<button type="button" class="button w-24 mr-1 mb-2 btn_dark ml-2" id="filter">Filter</button>
					<button type="reset" class="button w-24 mr-1 mb-2 btn_red float-right ml-2" id="reset">Reset</button>
				</div>
			</form>
		</div>
		
		<div class="intro-y datatable-wrapper box p-5 col-span-12 overflow-auto lg:overflow-hidden" id="patients_table">
			<table id="myTable" class="table table-report table-report--bordered display w-full sub_admin_table dataTable mt-0">
				<thead>
					<tr class="intro-x">
						<th class="border-b-2">S.No.</th>
						<th class="border-b-2">Variant</th>
						<th class="border-b-2">Medicine</th>
						<th class="border-b-2">Service</th>
						<th class="border-b-2">Price</th>
						<th class="border-b-2">Qty</th>
					</tr>
				</thead>
				<tbody></tbody>
			</table>
		</div>
	</div>


	<script type="text/javascript">
		var table;
		var doc_heading = "{{ $doc_heading }}";
		var doc_heading2 = "{{ $doc_heading2 }}";
		
		$(document).ready(function() {
			$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			});
			
			fill_datatable();
			function fill_datatable(filter1 = '', filter2 = ''){
				
				table = $('#myTable').DataTable({
					dom: 'Bfrtip',
					buttons: [
						{
							extend: 'collection',
							text: 'Export',
							autoClose: true,						
							buttons: [
								{
									extend: 'excelHtml5',
									className: 'green',
									autoFilter: true,
									sheetName: 'Exported data',
									title: doc_heading,
									messageTop: doc_heading2,
									key: {
										shiftKey: true,
										key: 'e'
									}						
								},					
								{
									extend: 'pdfHtml5',
									className: 'red',
									title: doc_heading,
									messageTop: doc_heading2,
									key: {
										shiftKey: true,
										key: 'p'
									}						
								}					
							],
						},						
						'pageLength'												
					],
					select: true,
					processing: true,
					serverSide: true,
					responsive: true,
					paging: true,
					ordering: false,
					info: false,
					displayLength: 10,
					language: {
						processing: '<img src="/dist/images/loading.gif"/>'
					},				
					lengthMenu: [
						[10, 25, 50, -1],
						[10, 25, 50, "All"]
					], 
					
					ajax:{
						url: "{{ route('admin.reports.prescription_products') }}",
						data:{filter1:filter1, filter2:filter2}
					},
					columns: [
						{data: 'DT_RowIndex', name: 'DT_RowIndex'},
						{data: 'variant_name', name: 'variant_name'},
						{data: 'medicine_name', name: 'medicine_name'},
						{data: 'service', name: 'service'},
						{data: 'variant_price', name: 'variant_price'},
						{data: 'variant_qty', name: 'variant_qty'}

					]
				});
				
				table.on('page.dt', function() {
				  $('html, body').animate({
					scrollTop: $(".dataTables_wrapper").offset().top
				   }, 'slow');
				});
			}
			
			
			$('#filter').click(function(){
				var filter1 = $('#filter1').val();
				var filter2 = $('#filter2').val();

				if(filter1 != '' ||  filter2 != '') {
					$('#myTable').DataTable().destroy();
					fill_datatable(filter1, filter2);
				}
			});

			$('#reset').click(function(){
				$(".select2-selection__rendered").attr('title', 'Any').html('Any');
				$('#myTable').DataTable().destroy();
				fill_datatable();
			});
		

			$("#myTable tbody tr").addClass('intro-x');
		});

	</script>
@endsection