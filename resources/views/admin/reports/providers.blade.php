@extends('layouts.app')
@section('content')
	<style>
		div#myTable_processing {
			position: absolute;
			top: 0;
			left: 0;
			width: 104%;
			height: 108%;
		}
	</style>
	<div class="intro-y flex flex-col sm:flex-row items-center mt-8">
		<h2 class="text-lg font-medium mr-auto">{{ $heading }}</h2>
	</div>

	<div class="grid grid-cols-12 gap-6 mt-5">

		<div class="intro-y col-span-12 items-center">
			<form id="tableFilters">
			
				<div class="col-span-4 ml-2">
					<label>Start Date</table>
					<input type="date" value="" id="filter4" class="input w-full border" max="{{ date('Y-m-d') }}">
				</div>	
				
				<div class="col-span-4 ml-2">
					<label>End Date</table>
					<input type="date" value="" id="filter5" class="input w-full border" max="{{ date('Y-m-d') }}">
				</div>	
				
				<div class="col-span-4 ml-2">
					<label>Category</label>
					<select id="filter1" class="select2 w-full">
						<option value="">Any</option>
						@if($categories && $categories->count() > 0)
							@foreach($categories as $category)
								<option value="{{ $category->id }}">{{ $category->name }}</option>
							@endforeach
						@endif
					</select>
				</div>
				
				<div class="col-span-4 ml-2">
					<label>Service/Treatment</label>
					<select id="filter3" class="select2 w-full">
						<option value="">Any</option>
						@if($services && $services->count() > 0)
							@foreach($services as $service)
								<option value="{{ $service->id }}" >{{ $service->name }}</option>
							@endforeach
						@endif
					</select>
				</div>				

				<div class="col-span-4 ml-2">
					<label>Status</label>
					<select id="filter2" class="select2 w-full">
						<option value="" class="any">Any</option>
						<option value="1">Active</option>
						<option value="0">Blocked</option>
					</select>
				</div>
				
				<div class="w-auto mt-3 mt-md-0">
					<button type="button" class="button w-24 mr-1 mb-2 btn_dark ml-2" id="filter">Filter</button>
					<button type="reset" class="button w-24 mr-1 mb-2 btn_red float-right ml-2" id="reset">Reset</button>
				</div>
			</form>
		</div>

		<div class="intro-y datatable-wrapper box p-5 col-span-12 overflow-auto lg:overflow-hidden" id="patients_table">
			<table id="myTable" class="table table-report table-report--bordered display w-full sub_admin_table dataTable mt-0">
				<thead>
					<tr class="intro-x">
						<th class="border-b-2">S.No.</th>
						<th class="border-b-2">Name</th>
						<th class="border-b-2">Gender</th>
						<th class="border-b-2">Phone</th>
						<th class="border-b-2">Categories</th>
						<th class="border-b-2">Services</th>
						<th class="border-b-2">Registered At</th>
					</tr>
				</thead>
				<tbody></tbody>
			</table>
		</div>
	</div>


	<script type="text/javascript">
		var table;
		var doc_heading = "{{ $doc_heading }}";
		var doc_heading2 = "{{ $doc_heading2 }}";
		var request_data;
		
		$(document).ready(function() {
			$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			});
			
			fill_datatable();
			
			function fill_datatable(request_data){
				
				table = $('#myTable').DataTable({
					dom: 'Bfrtip',
					buttons: [
						{
							extend: 'collection',
							text: 'Export',
							autoClose: true,						
							buttons: [
								{
									extend: 'excelHtml5',
									className: 'green',
									autoFilter: true,
									sheetName: 'Exported data',
									title: doc_heading,
									messageTop: doc_heading2,
									key: {
										shiftKey: true,
										key: 'e'
									}						
								},					
								{
									extend: 'pdfHtml5',
									className: 'red',
									title: doc_heading,
									messageTop: doc_heading2,
									key: {
										shiftKey: true,
										key: 'p'
									}						
								}					
							],
						},						
						'pageLength'												
					],
					select: true,
					processing: true,
					serverSide: true,
					responsive: true,
					paging: true,
					ordering: false,
					info: false,
					displayLength: 10,
					language: {
						processing: '<img src="/dist/images/loading.gif"/>'
					},				
					lengthMenu: [
						[10, 25, 50, -1],
						[10, 25, 50, "All"]
					], 
					
					ajax:{
						url: "{{ route('admin.reports.providers') }}",
						data:request_data
					},
					columns: [
						{data: 'DT_RowIndex', name: 'DT_RowIndex'},
						{data: 'name', name: 'name'},
						{data: 'gender', name: 'gender'},
						{data: 'phone', name: 'phone'},
						{data: 'category', name: 'category'},
						{data: 'service', name: 'service'},
						{data: 'created_at', name: 'created_at'},

					]
				});
				
				table.on('page.dt', function() {
				  $('html, body').animate({
					scrollTop: $(".dataTables_wrapper").offset().top
				   }, 'slow');
				});
			}
			
			$('#filter').click(function(){
				var filter1 = $('#filter1').val();
				var filter2 = $('#filter2').val();
				var filter3 = $('#filter3').val();
				var filter4 = $('#filter4').val();
				var filter5 = $('#filter5').val();

				if(filter4 != '' && filter5 != ''){
					let date1 = new Date(filter4);
					let date2 = new Date(filter5);
					
					if(date1 > date2){
						alert('Start date must not be greater than End date.');
						return false;
					}
				}
				
				if(filter1 != '' ||  filter2 != '' ||  filter3 != '' ||  filter4 != '' ||  filter5 != '') {
					
					request_data = {filter1:filter1, filter2:filter2, filter3:filter3, filter4:filter4, filter5:filter5};
					
					$('#myTable').DataTable().destroy();
					fill_datatable(request_data);
				}
			});

			$('#reset').click(function(){
				$(".select2-selection__rendered").attr('title', 'Any').html('Any');
				$('#myTable').DataTable().destroy();
				fill_datatable();
			});
		

			$("#myTable tbody tr").addClass('intro-x');
		});

	</script>
@endsection