@extends('layouts.app')
@section('content')
	<div class="intro-y box mt-5">
		<div class="flex flex-col sm:flex-row items-center p-5 border-b border-gray-200 dark:border-dark-5">
			<h2 class="font-medium text-base mr-auto">@if($faq) {{ 'Edit' }} @else Add New @endif FAQ</h2>
		</div>
		<div class="intro-y datatable-wrapper box p-5 col-span-12 overflow-auto lg:overflow-hidden">
			<form id="FaqForm">
			<input type="hidden" name="id" value="<?php if($faq){ echo $faq->id;}?>"/>
				<div class="grid grid-cols-12 gap-6 mt-5">
					<div class="intro-y col-span-12 lg:col-span-6">
						<label>Category</label>
						<div class="mt-2">
							<select id="category_id" name="category_id" class="select2 w-full">
								<option value="">--Select--</option>
								@if($categories && $categories->count() > 0)
									@foreach($categories as $category)
										<option value="{{ $category->id }}" @if($faq && $faq->category_id == $category->id) {{ 'selected' }} @endif >{{ $category->name }}</option>
									@endforeach
								@endif
							</select>
						</div>
					</div>
				</div>

				<p class="mt-5">Question/Answers</p>
				
					@if($faq && $faq->faq_data != '')
						<section class="more_items_wrapper">
						@php
							$faq_rows = json_decode($faq->faq_data, true);
							$flag = 1;
						@endphp

						@if($faq_rows)
							@foreach($faq_rows as $row)
								<div class="grid grid-cols-12 gap-6 mt-2 filds_wrapper">
									<div class="intro-y col-span-12 lg:col-span-4">
										<input type="text" name="faq_data[question][]" class="input w-full border" placeholder="Question" value="{{ $row['question'] }}">
									</div>
									<div class="intro-y col-span-12 lg:col-span-4">
										<input type="text" name="faq_data[answer][]" class="input w-full border" placeholder="Answer" value="{{ $row['answer'] }}">
									</div>
									<div class="intro-y col-span-12 lg:col-span-4">
										@if($flag == 1)
											<button type="button" class="button button--md w-24 mr-1 mb-2 btn_blue add_item">Add More</button>
										@else
											<button type="button" class="button button--md w-24 mr-1 mb-2 btn_red remove_item">Remove</button>
										@endif
									</div>
								</div>
								@php
									$flag++;
								@endphp
							@endforeach
						@endif
					</section>
					@else
						<div class="grid grid-cols-12 gap-6 mt-2">
							<div class="intro-y col-span-12 lg:col-span-4">
								<input type="text" name="faq_data[question][]" class="input w-full border" placeholder="Question">
							</div>
							<div class="intro-y col-span-12 lg:col-span-4">
								<input type="text" name="faq_data[answer][]" class="input w-full border" placeholder="Answer">
							</div>
							<div class="intro-y col-span-12 lg:col-span-4">
								<button type="button" class="button button--md w-24 mr-1 mb-2 btn_blue add_item">Add More</button>
							</div>
						</div>
					<section class="more_items_wrapper"></section>
					
					@endif

				 <div class="mt-4">
					<label>Active Status</label>
					<div class="mt-2">
						<input type="checkbox" name="status" class="input input--switch border" @if($faq && $faq->status == 1) checked @endif>
					</div>
				</div> 
				<div class="grid grid-cols-12 gap-6 mt-12">
					<div class="intro-y col-span-12 lg:col-span-12">
						<button type="submit" id="saveBtn" class="button button--md w-24 mr-1 mb-2 btn_blue">Save</button>
						<button type="reset" class="button button--md w-24 mr-1 mb-2 btn_red" onclick="location.href = '{{ route('admin.faqs') }}';">Cancel</button>
					</div>
				</div>
			</form>
		</div>
	</div>

	<script type="text/javascript">
		$(document).ready(function() {

			$("#FaqForm").submit(function(e) {
				e.preventDefault();
				$.ajax({
					headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					},
					data: $('#FaqForm').serialize(),
					url: "{{ route('admin.faqs.store') }}",
					type: "POST",
					// dataType: 'json',
					success: function (response) {
						//response = JSON.parse(res);
						if(response['status'] == 'success'){
							swal({
								title: response['message'],
								icon: 'success'
							});
							$('#FaqForm').trigger("reset");
							setTimeout(function(){
								swal.close();
								window.location = "{{ route('admin.faqs') }}";
							}, 1000);
						}
					},
					error: function (data) {
						//console.log('Error:', data);
						swal({
							title: 'Error Occured.',
							icon: 'error'
						})
						$('#saveBtn').html('Save');
					}
				});
			});

			$('.add_item').click(function() {
				$('.more_items_wrapper').append(
					'<div class="grid grid-cols-12 gap-6 mt-2 filds_wrapper">' +
						'<div class="intro-y col-span-12 lg:col-span-4">' +
							'<input type="text" name="faq_data[question][]" class="input w-full border" placeholder="Question">' +
						'</div>' +
						'<div class="intro-y col-span-12 lg:col-span-4">' +
							'<input type="text" name="faq_data[answer][]" class="input w-full border" placeholder="Answer">' +
						'</div>' +
						'<div class="intro-y col-span-12 lg:col-span-4">' +
							'<button type="button" class="button button--md w-24 mr-1 mb-2 btn_red remove_item">Remove</button>' +
						'</div>' +
					'</div>'
				);
			});

			$(document).on('click', '.more_items_wrapper .remove_item', function() {
				// alert('dfd');
				$(this).closest('.filds_wrapper').remove();
			});
		});

	</script>
</html>
@endsection