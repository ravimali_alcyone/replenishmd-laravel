
@extends('layouts.app')
@section('content')

    <!-- BEGIN: Content -->
    <div class="content">
        <div class="intro-y flex items-center mt-8">
            <h2 class="text-lg font-medium mr-auto">Profile</h2>
        </div>
        <!-- BEGIN: Profile Info -->
        <div class="intro-y box px-5 pt-5 mt-5">
            <div class="flex flex-col lg:flex-row border-b border-gray-200 dark:border-dark-5 pb-5 -mx-5">
                <div class="flex flex-1 px-5 items-center justify-center lg:justify-start">
				
				
                    <div class="w-20 h-40 sm:w-40 sm:h-40 flex-none lg:w-40 lg:h-40 image-fit relative profile2-text mb-3">
                        @if($user->image != null)
                            <img alt="" class="rounded-full" src="{{env('APP_URL')}}/{{$user->image}}">
                        @else
                            <img alt="" class="rounded-full" src="{{env('APP_URL')}}/dist/images/user_icon.png">
                        @endif
                    </div>
                    <div class="w-20 h-40 sm:w-40 sm:h-40 flex-none lg:w-40 lg:h-40 image-fit relative profile2-input mb-3" style="display:none;">	
						<form id="ProfileImageForm">
							<input type="file" name="image" class="input w-30 border mt-2" id="user_image">
							<div class="images_preview w-20 mt-3 mb-3">
							</div>
							<button type="submit" id="saveBtn3" class="button button--sm w-10 mr-1 mb-2 btn_blue"><i data-feather="check"></i></button>
							<button type="button" id="profile2_close" class="button button--sm w-10 mr-1 mb-2 btn_red"><i data-feather="x"></i></button>
						</form>
                    </div>					
					<button type="button" id="profile2" class="button button--sm px-2 mr-1 mb-2 border text-gray-700 dark:bg-dark-5 dark:text-gray-300" title="Chane Image" ><i data-feather="edit"></i></button>
                    <div class="ml-5">
                        <div class="w-24 sm:w-40 truncate sm:whitespace-normal font-medium text-lg">@if($user->name != null) {{ucfirst($user->name)}} @endif</div>
                    </div>
                </div>
                <div class="flex mt-6 lg:mt-0 items-center lg:items-start flex-1 flex-col justify-center text-gray-600 dark:text-gray-300 px-5 border-l border-r border-gray-200 dark:border-dark-5 border-t lg:border-t-0 pt-5 lg:pt-0">
				
					<button type="button" id="profile1" class="button button--sm px-2 mr-1 mb-2 border text-gray-700 dark:bg-dark-5 dark:text-gray-300" title="Edit" ><i data-feather="edit"></i></button>
					
					<div class="profile1-text">
						<div class="truncate sm:whitespace-normal flex items-center email-text"> <i data-feather="mail" class="w-4 h-4 mr-2"></i> @if($user->email != null) <span>{{$user->email}} </span> @endif </div>
						<div class="truncate sm:whitespace-normal flex items-center mt-3 phone-text"> <i data-feather="phone-call" class="w-4 h-4 mr-2"></i> @if($user->phone != null) <span> {{$user->phone}} </span> @endif </div>
						<div class="sm:whitespace-normal flex items-center mt-3 address-text"> <i data-feather="home" class="w-4 h-4 mr-2"></i> @if($user->address != null) <span> {{$user->address}} </span> @endif </div>
					</div>
					
					<div class="profile1-input" style="display:none;">
						<form id="ContactDetails">
						<label>Email</label><input type="text" class="input w-full border mt-1 mb-2 validate" id="email" name="email" value="@if($user->email != null) {{$user->email}} @endif">
						<label>Phone</label><input type="text" class="input w-full border mt-1 mb-2 validate" id="phone" name="phone" value="@if($user->phone != null) {{$user->phone}} @endif">
						<label>Address</label><input type="text" class="input w-full border mt-1 mb-2 validate" id="address" name="address" value="@if($user->address != null) {{$user->address}} @endif">
						<div id="review-location">
						</div>
							<button type="submit" id="saveBtn2" class="button button--sm w-10 mr-1 mb-2 btn_blue"><i data-feather="check"></i></button>
							<button type="button" id="profile1_close" class="button button--sm w-10 mr-1 mb-2 btn_red"><i data-feather="x"></i></button>
						</form>
					</div>
                </div>
            </div>
			<div class="nav-tabs flex flex-col sm:flex-row justify-center lg:justify-start">
				<a data-toggle="tab" data-target="#profile" href="javascript:;" class="py-4 sm:mr-8 flex items-center active"> <i class="w-4 h-4 mr-2" data-feather="user"></i> Profile </a>
				<a data-toggle="tab" data-target="#change-password" href="javascript:;" class="py-4 sm:mr-8 flex items-center"> <i class="w-4 h-4 mr-2" data-feather="lock"></i> Change Password </a>
				<a data-toggle="tab" data-target="#settings" href="javascript:;" class="py-4 sm:mr-8 flex items-center"> <i class="w-4 h-4 mr-2" data-feather="settings"></i> Settings </a>
			</div>				
        </div>
        <!-- END: Profile Info -->
	
        <div class="tab-content mt-5">
            <div class="tab-content__pane active" id="profile">
                <div class="grid grid-cols-12 gap-6">
                    <!-- BEGIN: Daily Sales -->
                    <div class="intro-y box col-span-12 lg:col-span-6">
                        <div class="p-5">
                            <div class="relative flex items-center">
                                <div class="mr-auto">
                                    <a href="#" class="font-medium">Gender</a>
                                </div>
                                <div class="font-medium text-gray-600">{{ucfirst($user->gender) ?? ''}}</div>
                            </div>
                            <div class="relative flex items-center mt-5">
                                <div class="mr-auto">
                                    <a href="#" class="font-medium">Date of Birth</a>
                                </div>
                                <div class="font-medium text-gray-600">@if($user->dob != null) {{ucfirst($user->dob)}} @endif</div>
                            </div>
                            <div class="relative flex items-center mt-5">
                                <div class="mr-auto">
                                    <a href="#" class="font-medium">Age</a>
                                </div>
                                <div class="font-medium text-gray-600">@if($user->age != null) {{ucfirst($user->age)}} @endif</div>
                            </div>
                            <div class="relative flex items-center mt-5">
                                <div class="mr-auto">
                                    <a href="#" class="font-medium">Status</a>
                                </div>
                                <div class="font-medium text-gray-600">@if($user->status == 1) Active @else Blocked @endif</div>
                            </div>
                        </div>
                    </div>

                    <div class="intro-y box col-span-12 lg:col-span-6">
                        <div class="p-5">
                            <div class="relative flex items-center">
                                <div class="mr-auto">
                                    <a href="#" class="font-medium">RMD ID</a>
                                </div>
                                <div class="font-medium text-gray-600">@if($user->rmd_ad_id != null) {{$user->rmd_ad_id}} @endif</div>
                            </div>
                            <div class="relative flex items-center mt-5">
                                <div class="mr-auto">
                                    <a href="#" class="font-medium">Google Adsense ID</a>
                                </div>
                                <div class="font-medium text-gray-600">@if($user->google_adsense_id != null) {{$user->google_adsense_id}} @endif</div>
                            </div>
                            <div class="relative flex items-center mt-5">
                                <div class="mr-auto">
                                    <a href="#" class="font-medium">Bing Ads ID</a>
                                </div>
                                <div class="font-medium text-gray-600">@if($user->bing_ads_id != null) {{$user->bing_ads_id}} @endif</div>
                            </div>
                            <div class="relative flex items-center mt-5">
                                <div class="mr-auto">
                                    <a href="#" class="font-medium">Amazon ID</a>
                                </div>
                                <div class="font-medium text-gray-600">@if($user->amazon_id != null) {{$user->amazon_id}} @endif</div>
                            </div>

                        </div>
                    </div>

                    <!-- END: Daily Sales -->
                </div>
            </div>
            <div class="tab-content__pane" id="change-password">
                <div class="grid grid-cols-12 gap-6">
                    <!-- BEGIN: Daily Sales -->
                    <div class="intro-y box col-span-12 lg:col-span-6 myForm">
                        <div class="p-5">
							<form id="ChangePasswordForm">
								<div class="relative flex items-center">
									<div class="mr-auto">
										<a href="#" class="font-medium">Old Password</a>
									</div>
									<div class="font-medium text-gray-600">
										<input type="password" class="input w-full border mt-2 validate" id="old_password" name="old_password" required>
									</div>
								</div>
								<div class="relative flex items-center mt-5">
									<div class="mr-auto">
										<a href="#" class="font-medium">Password</a>
									</div>
									<div class="font-medium text-gray-600">
										<input type="password" class="input w-full border mt-2 validate" id="password" name="password" required>
									</div>
								</div>
								<div class="relative flex items-center mt-5">
									<div class="mr-auto">
										<a href="#" class="font-medium">Confirm Password</a>
									</div>
									<div class="font-medium text-gray-600">
										<input type="password" class="input w-full border mt-2 validate" id="password_confirmation" name="password_confirmation" required>
									</div>
								</div>
								<div class="relative flex items-center mt-5">
									<div class="mr-auto">
										<button type="submit" id="saveBtn" class="button button--md w-24 mr-1 mb-2 btn_blue">Submit</button>
									</div>

								</div>
							</form>	
                        </div>
                    </div>
                    <!-- END: Daily Sales -->
                </div>
            </div>
			<div class="tab-content__pane" id="settings">
                <div class="grid grid-cols-12 gap-6">
				<?php 
					/*foreach($setting as $value)
					{
						echo $value->value; die;
					}*/
				?>
					@php $i=1; @endphp
					@if(!empty($setting))
					@foreach($setting as $value)
                    
					@if($value->function_name=="forum")
					<!-- Forum Setting -->
                    <div class="intro-y box col-span-12 lg:col-span-6">
                        <div class="p-5">
							<div class="relative grid mt-5">
								<div class="grid grid-cols-4 gap-4 forum_text">
									<div class="text-left">
										<a href="#" class="font-medium text-gray-600">Is Forum Payable ?</a>
									</div>
									<div class="text-center">
										<?php $data = json_decode($value->value);?>
										<span><?php echo ($data->price!=0) ? $data->price : 0;?> $</span>
									</div>
									<div class="text-center">
										<span><?php echo ($data->package==1) ? "One Time Package" : "Monthly"; ?></span>
									</div>
									<div class="text-right">
										@if($data->payable==1)
										<input type="checkbox" name="status" class="input input--switch border" title="Payable" checked="" onchange="changeFlag('forum',1)">
										@else
										<input type="checkbox" name="status" class="input input--switch border" title="Free" onchange="changeFlag('forum',0)">
										@endif
										&nbsp;&nbsp;<button type="button" id="payableForm" class="button button--sm px-2 mr-1 mb-2 border text-gray-700 dark:bg-dark-5 dark:text-gray-300" title="Edit"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-edit"><path d="M11 4H4a2 2 0 0 0-2 2v14a2 2 0 0 0 2 2h14a2 2 0 0 0 2-2v-7"></path><path d="M18.5 2.5a2.121 2.121 0 0 1 3 3L12 15l-4 1 1-4 9.5-9.5z"></path></svg></button>	
									</div>
									
								</div>
								<div class="forum_input" style="display:none;">
									<div class="grid grid-cols-12 gap-6 mt-5">
										<div class="intro-y col-span-12 lg:col-span-12 myForm">
											<div class="intro-y box p-5">
												<form id="forumPayable">
													<input type="hidden" name="id" id="id" value="{{$value->id}}">
													<div class="mt-4">
														<label class="active"><strong>Is Forum Payable?</strong></label>
														<div class="flex mt-3">
															<div class="form-check form-check-inline">
																<input class="form-check-input form-check-input rounded-full h-4 w-4 border border-gray-300 bg-white checked:bg-blue-600 checked:border-blue-600 focus:outline-none transition duration-200 mt-1 align-top bg-no-repeat bg-center bg-contain float-left mr-2 cursor-pointer" type="radio" name="payable" id="inlineRadio1" value="1" <?php if($data->payable==1) echo "checked=checked";?> >
																<label class="form-check-label inline-block text-gray-800" for="inlineRadio10">Yes</label>
															</div>
															<div class="form-check form-check-inline ml-5">
																<input class="form-check-input form-check-input rounded-full h-4 w-4 border border-gray-300 bg-white checked:bg-blue-600 checked:border-blue-600 focus:outline-none transition duration-200 mt-1 align-top bg-no-repeat bg-center bg-contain float-left mr-2 cursor-pointer" type="radio" name="payable" id="inlineRadio2" value="0" <?php if($data->payable==0) echo "checked=checked";?> >
																<label class="form-check-label inline-block text-gray-800" for="inlineRadio20">No</label>
															</div>
														</div>
													</div>
													<div class="mt-4">
														<label class="active"><strong>Package</strong></label>
														<div class="flex mt-5">
															<div class="form-check form-check-inline">
																<input class="form-check-input form-check-input rounded-full h-4 w-4 border border-gray-300 bg-white checked:bg-blue-600 checked:border-blue-600 focus:outline-none transition duration-200 mt-1 align-top bg-no-repeat bg-center bg-contain float-left mr-2 cursor-pointer" type="radio" name="package" id="inlineRadio11" value="1" <?php if($data->package==1) echo "checked=checked";?> >
																<label class="form-check-label inline-block text-gray-800" for="inlineRadio10">One Time</label>
															</div>
															<div class="form-check form-check-inline ml-5">
																<input class="form-check-input form-check-input rounded-full h-4 w-4 border border-gray-300 bg-white checked:bg-blue-600 checked:border-blue-600 focus:outline-none transition duration-200 mt-1 align-top bg-no-repeat bg-center bg-contain float-left mr-2 cursor-pointer" type="radio" name="package" id="inlineRadio21" value="0" <?php if($data->package==0) echo "checked=checked";?> >
																<label class="form-check-label inline-block text-gray-800" for="inlineRadio20">Monthly</label>
															</div>
														</div>
													</div>
													
													<div class="mt-4">
														<label><strong>Price</strong></label>
														<div class="mt-2">
														<input type="text" class="input w-full border mt-2" placeholder="Price" id="price" name="price" value="{{$data->price}}" required="">
														</div>
													</div>
													<div class="mt-4">
														<button type="submit" id="saveBtn2" class="button button--sm w-10 mr-1 mb-2 btn_blue"><i data-feather="check"></i></button>
														<button type="button" id="forum_close" class="button button--sm w-10 mr-1 mb-2 btn_red"><i data-feather="x"></i></button>
													</div>	
												</form>	
											</div>
										</div>
									</div>
								</div>
                            </div>
                        </div>
                    </div>
                    <!-- END: Forum setting -->
					@endif

					@if($value->function_name=="chat_paywall")
					<!-- Chat paywall feature Setting -->
                    <div class="intro-y box col-span-12 lg:col-span-6">
                        <div class="p-5">
							<div class="relative grid mt-5">
								
								<div class="grid grid-cols-4 gap-4 forum_text_chat">
									<div class="text-left">
										<a href="#" class="font-medium text-gray-600">Chat Paywall</a>
									</div>
									<?php $data = json_decode($value->value);?>
									<div class="text-center">
										<?php echo ($data->price!=0) ? $data->price : 0;?> $
									</div>
									<div class="text-center">
										&nbsp;
									</div>
									<div class="text-right">
										<button type="button" id="chatPaywallForm" class="button button--sm px-2 mr-1 mb-2 border text-gray-700 dark:bg-dark-5 dark:text-gray-300" title="Edit"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-edit"><path d="M11 4H4a2 2 0 0 0-2 2v14a2 2 0 0 0 2 2h14a2 2 0 0 0 2-2v-7"></path><path d="M18.5 2.5a2.121 2.121 0 0 1 3 3L12 15l-4 1 1-4 9.5-9.5z"></path></svg></button>	
									</div>
									
								</div>
								<div class="forum_input_chat" style="display:none;">
									<div class="grid grid-cols-12 gap-6 mt-5">
										<div class="intro-y col-span-12 lg:col-span-12 myForm">
											<div class="intro-y box p-5">
												<form id="chat_paywall_form">
													<input type="hidden" name="paywall_id" id="paywall_id" value="{{$value->id}}">
													<div class="mt-4">
														<label><strong>Price</strong></label>
														<div class="mt-2">
														<input type="text" class="input w-full border mt-2" placeholder="Price" id="paywall_price" name="paywall_price" value="<?php echo ($data->price!=0) ? $data->price : 0;?>" required="">
														</div>
													</div>
													<div class="mt-4">
														<button type="submit" id="saveBtn2" class="button button--sm w-10 mr-1 mb-2 btn_blue"><i data-feather="check"></i></button>
														<button type="button" id="chatPaywallForm_close" class="button button--sm w-10 mr-1 mb-2 btn_red"><i data-feather="x"></i></button>
													</div>	
												</form>	
											</div>
										</div>
									</div>
								</div>
                            </div>
                        </div>
                    </div>
                    <!-- END: Chat paywall feature Setting -->
					@endif

					@php $i+=1; @endphp	
					@endforeach
					@endif
                </div>
            </div>			
        </div>
    </div>
    <!-- END: Content -->
	
<script src="{{ asset('js/jquery.min.js')}}"></script>
<script src="{{ asset('plugins/jquery-validation/jquery.validate.min.js')}}"></script>

<script type="text/javascript">

  if ($(".myForm").length > 0) {
    $("#ChangePasswordForm").validate({
      rules: {
        old_password: {
          required: true
        },
        password: {
          required: true
        },
        password_confirmation: {
          required: true
        }
      },
      errorElement: 'div',
	  submitHandler: function(form) {
		// do other things for a valid form
		$.ajax({
		  headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		  },
		  data: $('#ChangePasswordForm').serialize(),
		  url: "{{ route('admin.change_password') }}",
		  type: "POST",
		 // dataType: 'json',
		  success: function (response) {
				if(response['status'] == 'success'){
				  swal({
					title: response['message'],
					icon: 'success'
				  });
				  $('#ChangePasswordForm').trigger("reset");
				  setTimeout(function(){
					swal.close();
				  }, 1000);
				}else if(response['status'] == 'error'){
				  swal({
					title: response['message'],
					icon: 'error'
				  })
				  $('#saveBtn').html('Submit');					
				}


		  },
		  error: function (data) {
			  let errorArr = [];
			  let errors = data.responseJSON.errors;
			  let passwordErr = errors.password[0];
			  swal({
				title: passwordErr,
				icon: 'error'
			  })
			  $('#saveBtn').html('Submit');
		  }
		});
	  }
    });

  }


	$('#profile1').click(function(){
		$('.profile1-input').show();
		$('.profile1-text').hide();
	});

	$('#profile1_close').click(function(){
		$('.profile1-input').hide();
		$('.profile1-text').show();
	});

	$('#payableForm').click(function(){
		$('.forum_input').show();
		$('.forum_text').hide();
	});

	$('#forum_close').click(function(){
		$('.forum_input').hide();
		$('.forum_text').show();
	});

	$('#chatPaywallForm').click(function(){
		$('.forum_input_chat').show();
		$('.forum_text_chat').hide();
	});

	$('#chatPaywallForm_close').click(function(){
		$('.forum_input_chat').hide();
		$('.forum_text_chat').show();
	});

	

	$('#ContactDetails').submit(function(e){
		e.preventDefault();
		var phone = $('#phone').val();
		if(phone.match(/^(\+{0,})(\d{0,})([(]{1}\d{1,3}[)]{0,}){0,}(\s?\d+|\+\d{2,3}\s{1}\d+|\d+){1}[\s|-]?\d+([\s|-]?\d+){1,2}(\s){0,}$/gm)){
			
		}else{
			swal({
					title: 'Please specify a valid phone number.',
					icon: 'error'
				  });
			return false;
		}
		
		$.ajax({
		  headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		  },
		  data: $('#ContactDetails').serialize(),
		  url: "{{ route('admin.contact_details') }}",
		  type: "POST",
		 // dataType: 'json',
		  success: function (response) {
				if(response['status'] == 'success'){
					
					$('.email-text span').text($('#email').val());
					$('.phone-text span').text($('#phone').val());
					$('.address-text span').text($('#address').val());
				  swal({
					title: response['message'],
					icon: 'success'
				  });
				  $('#profile1_close').trigger("click");
				  setTimeout(function(){
					swal.close();
				  }, 1000);
				}else if(response['status'] == 'error'){
				  swal({
					title: response['message'],
					icon: 'error'
				  })
						
				}


		  },
		  error: function (data) {
			  let Err = 'Error Occured';
			  let errors = data.responseJSON.errors;
			  
			  $.each(errors, function(key, value) {
				  Err = value[0];
			  });
			  
			  swal({
				title: Err,
				icon: 'error'
			  })
		  }
		});	
	});


	$('#profile2').click(function(){
		$('.profile2-input').show();
		$('.profile2-text').hide();
	});

	$('#profile2_close').click(function(){
		$('.profile2-input').hide();
		$('.profile2-text').show();
	});


	// Image reader
	if (window.File && window.FileList && window.FileReader) {
		$("#user_image").on("change", function(e) {
			$('.pip').remove();
			var files = e.target.files,
			filesLength = files.length;
			for (var i = 0; i < filesLength; i++) {
				var f = files[i]
				var fileReader = new FileReader();
				fileReader.onload = (function(e) {
					var file = e.target;

					$images = "<span class='pip'>" +
						"<img class='imageThumb' src=\"" + e.target.result + "\" title=\"" + file.name + "\" width='80' height='80'/>" +
						"</span>";
					$(".images_preview").append($images);
					// "<br/><span class=\"remove\">Remove image</span>" +
					$(".remove").click(function(){
						$(this).parent(".pip").remove();
					});
				});
				fileReader.readAsDataURL(f);
			}
		});
	} else {
		alert("Your browser doesn't support to File API")
	}
	
	$("#ProfileImageForm").submit(function(e) {
		e.preventDefault();

		var formData = new FormData(this);

		$.ajax({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			url: "{{ route('admin.profile_image') }}",
			data: formData,
			type: "POST",
			cache:false,
			contentType: false,
			processData: false,
			// dataType: 'json',
			success: function (response) {
				//response = JSON.parse(res);
				if(response['status'] == 'success'){
					swal({
						title: response['message'],
						icon: 'success'
					});
					$('#ProfileImageForm').trigger("reset");
					setTimeout(function(){
					swal.close();
					location.reload(); }, 1000);
				}
			},
			error: function (data) {
			  let Err = 'Error Occured';
			  let errors = data.responseJSON.errors;
			  
			  $.each(errors, function(key, value) {
				  Err = value[0];
			  });
			  
			  swal({
				title: Err,
				icon: 'error'
			  })
			}
		});
	});	
	
	$(document).on('change', '#address', function() {
		if ($(this).val().length >= 3) {
		  
		  var api_key ="AIzaSyAtwRPqMHXd8mlGIRxGL5EwpAlQAPUu0p4";
		  $("#review-location").html('<iframe width="100%" frameborder="0" style="border:0;margin-top: 10px;" src="https://www.google.com/maps/embed/v1/place?key='+api_key+'&q=' + $("#address").val() + '&language=en"></iframe>')
		}else{
		  $("#review-location").html('')
		}
	});

	function changeFlag(flag1,flag2)
	{
		swal({
			title: "Are you sure to change?",
			text: "",
			icon: 'warning',
			buttons: {
			  cancel: true,
			  delete: 'Yes, Change It'
			}
		  }).then((isConfirm) => {
			if (!isConfirm) {
				return false;
			} else {
				$.ajax({
					headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					},
					type: "POST",
					url: "{{ env('APP_URL').'/admin/setting/update' }}"+'/'+flag1+'/'+flag2,
					success: function (response) {
						if(response['status'] == 'success'){
						swal({
							title: response['message'],
							icon: 'success'
						});
						setTimeout(function(){
						swal.close();
						}, 1000);
						}
						table.draw();
					},
					error: function (data) {
						console.log('Error:', data);
						swal({
							title: 'Error Occured',
							icon: 'error'
						});
					}
				});
			}
        });
	}


	$("#forumPayable").submit(function(e) {
		e.preventDefault();
		var payable = $("input[name='payable']:checked").val();
		var package = $("input[name='package']:checked").val();
		var price = $("#price").val();
		var id = $("#id").val();

		$.ajax({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			data: {"payable":payable,"package":package,"price":price,"id":id},
			url: "{{ route('admin.forum.setting') }}",
			type: "POST",
			dataType: 'json',
			success: function (response) {
				if(response['status'] == 'success'){
					$('#categoryTopicForm').trigger("reset");
					successAlert(response['message'],2000,'top-right');	
					setTimeout(function(){
					window.location = "{{ route('admin.profile') }}"; }, 1000);
				}
				else{
					errorAlert('Error occured.',3000,'top-right');						
				}
			},
			error: function (data) {
				$(".loader").css('display', 'none');
				let errors = data.responseJSON.errors;
				
				$.each(errors, function(key, value) {
					errorAlert(value[0],3000,'top-right');
				});	
			}
		});
	});

	$("#chat_paywall_form").submit(function(e) {
		e.preventDefault();
		var price = $("#paywall_price").val();
		var id = $("#paywall_id").val();

		$.ajax({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			data: {"price":price,"id":id},
			url: "{{ route('admin.chat_paywall.setting') }}",
			type: "POST",
			dataType: 'json',
			success: function (response) {
				if(response['status'] == 'success'){
					successAlert(response['message'],2000,'top-right');	
					setTimeout(function(){
					window.location = "{{ route('admin.profile') }}"; }, 1000);
				}
				else{
					errorAlert('Error occured.',3000,'top-right');						
				}
			},
			error: function (data) {
				$(".loader").css('display', 'none');
				let errors = data.responseJSON.errors;
				
				$.each(errors, function(key, value) {
					errorAlert(value[0],3000,'top-right');
				});	
			}
		});
	});

</script>	
@endsection