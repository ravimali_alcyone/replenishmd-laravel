@extends('layouts.app')
@section('content')
	<div class="intro-y box mt-5">
		<div class="flex flex-col sm:flex-row items-center p-5 border-b border-gray-200 dark:border-dark-5">
			<h2 class="font-medium text-base mr-auto">@if($product) {{ 'Edit' }} @else Add New @endif Product</h2>
		</div>
		<div class="intro-y datatable-wrapper box p-5 col-span-12 overflow-auto lg:overflow-hidden">
			<form id="productForm">
				<div class="grid grid-cols-12 gap-6 mt-5">
					<div class="intro-y col-span-12 lg:col-span-6">
						<div>
							<label>Name</label>
							<input type="text" name="name" value="<?php if($product){ echo $product->name;}?>" class="input w-full border mt-2" placeholder="Name" id="name" required>
						</div>
					</div>
					<div class="intro-y col-span-12 lg:col-span-6">
						<div>
							<label>Manufacturer Name</label>
							<input type="text" name="manufacturer_name" value="<?php if($product){ echo $product->manufacturer_name; }?>" class="input w-full border mt-2" placeholder="Manufacturer Name" id="manufacturer_name" required>
						</div>
					</div>
					<div class="intro-y col-span-12 lg:col-span-4">
						<label>Category</label>
						<div class="mt-2">
							<select id="category_id" name="category" class="select2 w-full" onchange="getSubCategories(this)" required>
								<option value="">--Select--</option>
								@if($categories && $categories->count() > 0)
									@foreach($categories as $category)
										<option value="{{ $category->id }}" <?php if($product && $product->category == $category->id){ echo 'selected';}?> >{{ $category->name }}</option>
									@endforeach
								@endif
							</select>
						</div>
					</div>

					<div class="intro-y col-span-12 lg:col-span-4">
						<label>Sub Category</label>
						<div class="mt-2">
							<select id="sub_category_id" name="sub_category" class="select2 w-full" onchange="getSubSubCategories(this)">
								<option value="">--Select--</option>
								@if($sub_categories && $sub_categories->count() > 0)
									@foreach($sub_categories as $sub_category)
										<option value="{{ $sub_category->id }}" <?php if($product && $product->sub_category == $sub_category->id){ echo 'selected';}?> >{{ $sub_category->name }}</option>
									@endforeach
								@endif
							</select>
						</div>
					</div>
					<div class="intro-y col-span-12 lg:col-span-4">
						<label>Sub Sub Category</label>
						<div class="mt-2">
							<select id="sub_sub_category" name="sub_category" class="select2 w-full">
								<option value="">--Select--</option>
								@if($sub_categories && $sub_categories->count() > 0)
									@foreach($sub_categories as $sub_category)
										<option value="{{ $sub_category->id }}" <?php if($product && $product->sub_category == $sub_category->id){ echo 'selected';}?> >{{ $sub_category->name }}</option>
									@endforeach
								@endif
							</select>
						</div>
					</div>
					<div class="intro-y col-span-12 lg:col-span-4">
						<label>Price Type</label>
						<div class="mt-2">
							<select id="price_type" name="price_type" class="select2 w-full">
								<option value="dose" <?php if($product && $product->price_type == 'dose'){ echo 'selected'; }?>>Dose</option>
								<option value="subscription" <?php if($product && $product->price_type == 'subscription'){ echo 'selected'; }?>>Subscription</option>
							</select>
						</div>
					</div>
					<div class="intro-y col-span-12 lg:col-span-4">
						<div>
							<label>Price Value</label>
							<input type="number" name="price_value" value="<?php if($product){ echo $product->price_value;	}?>" class="input w-full border mt-2" placeholder="Price Type" id="price_value" required>
						</div>
					</div>
					<div class="intro-y col-span-12 lg:col-span-4">
						<div>
							<label>Discount Price</label>
							<input type="number" name="discount_price" value="<?php if($product){ echo $product->discount_price; }?>" class="input w-full border mt-2" placeholder="Discount Price" id="discount_price" required>
						</div>
					</div>
					<div class="intro-y col-span-12 lg:col-span-4">
						<div>
							<label>Stock Quantity</label>
							<input type="number" name="stock_qty" value="<?php if($product){ echo $product->stock_qty; }?>" class="input w-full border mt-2" placeholder="Stock Quantity" id="stock_qty" required>
						</div>
					</div>

					<div class="intro-y col-span-12 lg:col-span-4">
						<label>Stock Status</label>
						<div class="mt-2">
							<select id="stock_status" name="stock_status" class="select2 w-full">
								<option value="in" <?php if($product && $product->stock_status == 'in'){ echo 'selected'; }?>>In</option>
								<option value="out" <?php if($product && $product->stock_status == 'out'){ echo 'selected'; }?>>Out</option>
							</select>
						</div>
					</div>
					<div class="intro-y col-span-12 lg:col-span-4">
						<label>Direct Purchase</label>
						<div class="mt-2">
							<select id="is_direct_purchase" name="is_direct_purchase" class="select2 w-full">
								<option value="1" <?php if($product && $product->is_direct_purchase == 1){ echo 'selected'; }?>>Yes</option>
								<option value="0" <?php if($product && $product->is_direct_purchase == 0){ echo 'selected'; }?>>No</option>
							</select>
						</div>
					</div>
					<div class="intro-y col-span-12 lg:col-span-12">
						<label>Description</label>
						<!-- <textarea name="description2" id="" cols="30" rows="10"></textarea> -->
						<textarea name="description" id="description" class="input w-full border mt-2" cols="30" rows="4"><?php if($product){ echo $product->description; }?></textarea>
					</div>
					<div class="intro-y col-span-12 lg:col-span-12">
						<label>Details</label>
						<textarea name="details" id="details" class="input w-full border mt-2" cols="30" rows="4"><?php if($product){ echo $product->details; }?></textarea>
					</div>
					<div class="intro-y col-span-12 lg:col-span-12">
						<label>Product Info</label>
						<textarea name="product_info" id="product_info" class="input w-full border mt-2" cols="30" rows="4"><?php if($product){ echo $product->product_info; }?></textarea>
					</div>
					<div class="intro-y col-span-12 lg:col-span-12">
						<label>Safety Info</label>
						<textarea name="safety_info" id="safety_info" class="input w-full border mt-2" cols="30" rows="4"><?php if($product){ echo $product->safety_info; }?></textarea>
					</div>
					<div class="intro-y col-span-12 lg:col-span-12">
						<label>Associate Problems</label>
						<textarea name="associate_problems" id="associate_problems" class="input w-full border mt-2" cols="30" rows="4"><?php if($product){ echo $product->associate_problems; }?></textarea>
					</div>
					<div class="intro-y col-span-12 lg:col-span-6">
						<label>Image</label>
						<input type="file" name="images[]" class="input w-full border mt-2" id="product_images" multiple="multiple">
						<!-- <img src="/<?php // if($product){ echo json_decode($product->images);}?>" alt="" width="80" height="80" class="mt-3"> -->
						<div class="images_preview mt-3">
							@if($product && json_decode($product->images))
								@foreach(json_decode($product->images) as $key => $value)
									<input type="hidden" name="old_image[]" value="{{ $value }}">
									<span class="pip"><img class="imageThumb" src="/{{ $value }}"></span>
								@endforeach
							@endif
						</div>
					</div>
					<div class="intro-y col-span-12 lg:col-span-4">
						<label>Active Status</label>
						<div class="mt-2">
							<input type="checkbox" name="status" class="input input--switch border" <?php if($product && $product->status == "1"){ echo 'checked';}?>>
						</div>
					</div>
				</div>
				<div class="grid grid-cols-12 gap-6 mt-12">
					<div class="intro-y col-span-12 lg:col-span-12">
						<input type="hidden" name="id" value="<?php if($product){ echo $product->id;}?>"/>
						<input type="hidden" name="page" value="<?php if($product){ echo 'edit';} else { echo 'add'; }?>"/>
						<button type="submit" id="saveBtn" class="button button--md w-24 mr-1 mb-2 btn_blue">Save</button>
						<button type="reset" class="button button--md w-24 mr-1 mb-2 btn_red" onclick="location.href = '{{ route('admin.products') }}';">Cancel</button>
					</div>
				</div>
			</form>
		</div>
	</div>

	<script type="text/javascript">
		CKEDITOR.replace( 'description' );
		CKEDITOR.add;
		CKEDITOR.replace( 'details' );
		CKEDITOR.add;
		CKEDITOR.replace( 'product_info' );
		CKEDITOR.add;
		CKEDITOR.replace( 'safety_info' );
		CKEDITOR.add;
		CKEDITOR.replace( 'associate_problems' );
		CKEDITOR.add;

	$("#productForm").submit(function(e) {
		e.preventDefault();
		var description = CKEDITOR.instances.description.getData();
		var details = CKEDITOR.instances.details.getData();
		var product_info = CKEDITOR.instances.product_info.getData();
		var safety_info = CKEDITOR.instances.safety_info.getData();
		var associate_problems = CKEDITOR.instances.associate_problems.getData();

		var formData = new FormData(this);
		formData.append('description', description);
		formData.append('details', details);
		formData.append('product_info', product_info);
		formData.append('safety_info', safety_info);
		formData.append('associate_problems', associate_problems);

		$.ajax({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			// data: $('#productForm').serialize(),
			url: "{{ route('admin.products.store') }}",
			data: formData,
			type: "POST",
			cache:false,
			contentType: false,
			processData: false,
			// dataType: 'json',
			success: function (response) {
				//response = JSON.parse(res);
				if(response['status'] == 'success'){
					swal({
						title: response['message'],
						icon: 'success'
					});
					$('#productForm').trigger("reset");
					setTimeout(function(){
					swal.close();
					window.location = "{{ route('admin.products') }}"; }, 1000);
				}
			},
			error: function (data) {
				let errorArr = [];
				let errors = data.responseJSON.errors;
				let emailErr = errors.email[0];
				// $.each(errors, function(key, value) {

				// });
				//console.log('Error:', data);
				// swal({
				// 	title: emailErr,
				// 	icon: 'error'
				// })
				// $('#saveBtn').html('Save');
			}
		});
	});

	function getSubCategories(e){
		let category_id = e.value;
		$.ajax({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			data: {'category_id':category_id},
			url: "{{ route('admin.products.get_sub_category') }}",
			type: "POST",
			success: function (response) {
				$("#sub_category_id").html(response);
				// $("#sub_category_id").formSelect();
			},
			error: function (data) {
				console.log('error',data);
			}
		});
	}

	function getSubSubCategories(e){
		let category_id = e.value;
		$.ajax({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			data: {'category_id': category_id},
			url: "{{ route('admin.products.get_sub_sub_category') }}",
			type: "POST",
			success: function (response) {
				$("#get_sub_sub_category").html(response);
				// $("#sub_category_id").formSelect();
			},
			error: function (data) {
				console.log('error',data);
			}
		});
	}

	// Image reader
	if (window.File && window.FileList && window.FileReader) {
		$("#product_images").on("change", function(e) {
			$('.pip').remove();
			var files = e.target.files,
			filesLength = files.length;
			for (var i = 0; i < filesLength; i++) {
				var f = files[i]
				var fileReader = new FileReader();
				fileReader.onload = (function(e) {
					var file = e.target;
					// $("<span class=\"pip\">" +
					// "<img class='imageThumb' src=\"" + e.target.result + "\" title=\"" + file.name + "\" width='80' height='80'/>" +
					// "</span>").insertAfter("#product_images");

					$images = "<span class='pip'>" +
						"<img class='imageThumb' src=\"" + e.target.result + "\" title=\"" + file.name + "\" width='80' height='80'/>" +
						"</span>";
					$(".images_preview").append($images);
					// "<br/><span class=\"remove\">Remove image</span>" +
					$(".remove").click(function(){
						$(this).parent(".pip").remove();
					});
				});
				fileReader.readAsDataURL(f);
			}
		});
	} else {
		alert("Your browser doesn't support to File API")
	}
</script>
</html>
@endsection