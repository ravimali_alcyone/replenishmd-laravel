@extends('layouts.app')
@section('content')
	<div class="intro-y flex flex-col sm:flex-row items-center mt-8">
		<h2 class="text-lg font-medium mr-auto">Sub Admins</h2>
		<div class="w-full sm:w-auto flex mt-4 sm:mt-0">
			<div class="intro-y col-span-12 flex flex-wrap sm:flex-no-wrap items-center mt-2">
				<a href="{{ route('admin.subadmins.add') }}" class="button w-32 mr-2 mb-2 flex items-center justify-center btn_white text-white"> Add New </a>
			</div>
		</div>
	</div>

	<div class="grid grid-cols-12 gap-6 mt-5">
		<div class="intro-y col-span-12 items-center">
			<form id="tableFilters">

				<div class="col-span-4 ml-2">
					<label>Status</label>
					<select id="filter_status" class="select2 w-full">
						<option value="" class="any">Any</option>
						<option value="1">Active</option>
						<option value="0">Blocked</option>
					</select>
				</div>
				<div class="w-auto mt-3 mt-md-0">
					<button type="button" class="button w-24 mr-1 mb-2 btn_dark text-white ml-2" id="filter">Show</button>
					<button type="reset" class="button w-24 mr-1 mb-2 btn_red text-white float-right ml-2" id="reset">Reset</button>
				</div>
			</form>
		</div>

		<div class="intro-y datatable-wrapper box p-5 col-span-12 overflow-auto lg:overflow-hidden">
			<table id="myTable" class="table table-report table-report--bordered display w-full sub_admin_table dataTable">
				<thead>
					<tr>
						<th class="border-b-2">S.No.</th>
						<th class="border-b-2">Name</th>
						<th class="border-b-2">Email</th>
						<th class="border-b-2">Privileges</th>
						<th class="border-b-2">Status</th>
						<th class="border-b-2">Actions</th>
					</tr>
				</thead>
				<tbody></tbody>
			</table>
		</div>
	</div>

<script src="{{ asset('js/jquery.min.js')}}"></script>

<script type="text/javascript">

var table;
$(document).ready(function(){
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});
	fill_datatable();

    function fill_datatable(filter_category = '', filter_status = ''){
		table = $('#myTable').DataTable({
			processing: true,
			serverSide: true,
			responsive: true,
			paging: true,
			ordering: false,
			info: false,
			displayLength: 10,
			language: {
				processing: '<img src="/dist/images/loading.gif"/>'
			},			
			lengthMenu: [
			  [10, 25, 50, -1],
			  [10, 25, 50, "All"]
			],
			//ajax: "{{ route('admin.subadmins') }}",
			ajax:{
				url: "{{ route('admin.subadmins') }}",
				data:{filter_category:filter_category, filter_status:filter_status}
			},
			columns: [
				{data: 'DT_RowIndex', name: 'DT_RowIndex'},
				{data: 'name', name: 'name'},
				{data: 'email', name: 'email'},
				{data: 'privileges', name: 'privileges'},
				{data: 'status', name: 'status'},
				{data: 'action', name: 'action', orderable: false, searchable: false},
			]
		});
		
		table.on('page.dt', function() {
		  $('html, body').animate({
			scrollTop: $(".dataTables_wrapper").offset().top
		   }, 'slow');
		});
    }

    $('#filter').click(function(){
        var filter_category = $('#filter_category').val();
        var filter_status = $('#filter_status').val();

        if(filter_category != '' ||  filter_status != '') {
            $('#myTable').DataTable().destroy();
            fill_datatable(filter_category, filter_status);
        }
	});

    $('#reset').click(function(){
		$(".select2-selection__rendered").attr('title', 'Any').html('Any');
        $('#myTable').DataTable().destroy();
        fill_datatable();
    });
  });



    function deleteRow(id) {

		swal({
			title: "Are you sure to delete?",
			text: "",
			icon: 'warning',
			buttons: {
			  cancel: true,
			  delete: {text:'Yes, Delete It',className:'btn_red'}
			}
		  }).then((isConfirm) => {
          if (!isConfirm) {
            return false;
          } else {
			$.ajax({
				type: "DELETE",
				url: "{{ env('APP_URL').'/admin/subadmins/destroy' }}"+'/'+id,
				success: function (response) {

					if(response['status'] == 'success'){
					  swal({
						title: response['message'],
						icon: 'success'
					  });
					  setTimeout(function(){
					  swal.close();
					  }, 1000);
					}
					table.draw();
				},
				error: function (data) {
					console.log('Error:', data);
					swal({
						title: 'Error Occured',
						icon: 'error'
					});
				}
			});
          }
        });
    }

		function changeStatus(id) {
			$.ajax({
				url: "{{ url('admin/subadmins/change_status') }}",
				type: "POST",
				data: {'id': id},
				success: function(response) {
					console.log(response);
				}
			});
		}

</script>
</html>
@endsection