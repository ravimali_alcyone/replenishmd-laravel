
@extends('layouts.app')
@section('content')
    <!-- BEGIN: Content -->
    <div class="content">
        <div class="intro-y flex items-center mt-8">
            <h2 class="text-lg font-medium mr-auto">Treatment Medicine Detail</h2>
        </div>

        <!-- END: Profile Info -->
        <div class="tab-content mt-5">
            <div class="tab-content__pane active" id="product_detail_page">
                <div class="grid grid-cols-12 gap-6">
                    <!-- BEGIN: Daily Sales -->
                    <div class="intro-y box col-span-12 lg:col-span-6">
                        <div class="p-5">
                            <div class="product_main_img">
                                @if(isset($images))
                                    <div class="image-zoom relative">
                                        <img src="/{{ $images[0] }}" data-zoom="/{{ $images[0] }}" alt="" class="w-full">
                                    </div>
                                @endif
                            </div>

                            <div class="product_thumbnail_section mt-3">
                                @if(isset($images))
                                    @foreach($images as $key => $value)
                                        <img src="/{{ $value }}" alt="">
                                    @endforeach
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="intro-y box col-span-12 lg:col-span-6">
                        <div class="p-5">
                            <h2 class="text-3xl text-gray-700 dark:text-gray-600">{{ $treatment_medicine->name ?? ''}}</h2>
                            <p class="mt-2">{!! $treatment_medicine->description ?? '' !!}</p>

                            <p class="text-gray-700 dark:text-gray-600 leading-none mt-10"><b>Treatment/Service</b> - {!! ucfirst($service) ?? '' !!}</p>
                            <p class="text-gray-700 dark:text-gray-600 leading-none mt-4"><b>Start Price</b> - $ {!! $treatment_medicine->start_price ?? '' !!}</p>
                        </div>
                    </div>


                <div class="intro-y col-span-12 lg:col-span-4 mt-5">
                    <div class="">
                        <div class="relative flex items-center">
                            <button type="button" class="button w-24 mr-1 mb-2 btn_dark ml-2" onclick="javascript:location.href = '/admin/treatment_medicines'">Back</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END: Content -->
    </div>

    <script>
        $(document).ready(function() {

            $(".product_thumbnail_section img").click(function() {
                $(".product_main_img img").attr('src', $(this).attr('src')).attr('data-zoom', $(this).attr('src'));
            });
            $(".product_main_img img").mouseover(function() {
                $(this).css('opacity', '0');
            });
            $(".product_main_img img").mouseout(function() {
                $(this).css('opacity', '1');
            });
        });
    </script>
@endsection