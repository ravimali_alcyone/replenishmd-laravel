@extends('layouts.app')
@section('content')
<style>
.customInterval .input {
    width: 75px;
    display: inline-block;
    padding: 5px;
    line-height: 16px;
    font-size: 12px;
}
.plan_type {
    width: 110px;
    display: inline-block;
}
.plan_interval {
    width: 100px;
    display: inline-block;
}
.custom_interval_count {
    width: 180px;
    display: inline-block;
}
</style>
<?php $x = 1;?>

	<div class="intro-y box mt-5">
		<div class="flex flex-col sm:flex-row items-center p-5 border-b border-gray-200 dark:border-dark-5">
			<h2 class="font-medium text-base mr-auto">@if($treatment_medicine) {{ 'Edit' }} @else Add New @endif Treatment Medicine</h2>
		</div>
		<div class="intro-y datatable-wrapper box p-5 col-span-12 overflow-auto lg:overflow-hidden">
			<form id="treatment_medicineForm">
				<div class="grid grid-cols-12 gap-6 mt-5">
					<div class="intro-y col-span-12 lg:col-span-6">
						<div>
							<label>Name</label>
							<input type="text" name="name" value="<?php if($treatment_medicine){ echo $treatment_medicine->name;}?>" class="input w-full border mt-2" placeholder="Name" id="name" required>
						</div>
					</div>
					<div class="intro-y col-span-12 lg:col-span-6">
						<div>
							<label>Other Name</label>
							<input type="text" name="other_name" value="<?php if($treatment_medicine){ echo $treatment_medicine->other_name; }?>" class="input w-full border mt-2" placeholder="Other Name" id="other_name">
						</div>
					</div>
					<div class="intro-y col-span-12 lg:col-span-6">
						<div>
							<label>Chemical Name</label>
							<input type="text" name="chemical_name" value="<?php if($treatment_medicine){ echo $treatment_medicine->chemical_name; }?>" class="input w-full border mt-2" placeholder="Chemical Name" id="chemical_name" >
						</div>
					</div>					
					<div class="intro-y col-span-12 lg:col-span-4">
						<label>Treatment/Service</label>
						<div class="mt-2">
							<select id="service_id" name="service_id" class="select2 w-full" required>
								<option value="">--Select--</option>
								@if($services && $services->count() > 0)
									@foreach($services as $service)
										<option value="{{ $service->id }}" <?php if($treatment_medicine && $treatment_medicine->service_id == $service->id){ echo 'selected';}?> >{{ $service->name }}</option>
									@endforeach
								@endif
							</select>
						</div>
					</div>

					<div class="intro-y col-span-12 lg:col-span-4">
						<div>
							<label>Starting Price($)</label>
							<input type="number" name="start_price" value="<?php if($treatment_medicine){ echo $treatment_medicine->start_price;	}?>" class="input w-full border mt-2" placeholder="Starting Price" id="start_price" required>
						</div>
					</div>
										
					<div class="intro-y col-span-12 lg:col-span-12">
						<label>Description</label>
						<textarea name="description" id="description" class="input w-full border mt-2" cols="50" rows="4"><?php if($treatment_medicine){ echo $treatment_medicine->description; }?></textarea>
					</div>

					<div class="intro-y col-span-12 lg:col-span-6">
						<label>Display Images</label>
						<input type="file" name="images[]" class="input w-full border mt-2" id="treatment_medicine_images" multiple="multiple" accept="image/*">
						<div class="images_preview mt-3">
							@if($treatment_medicine && json_decode($treatment_medicine->images))
								@foreach(json_decode($treatment_medicine->images) as $key => $value)
									<input type="hidden" name="old_image[]" value="{{ $value }}">
									<span class="pip"><img class="imageThumb" src="/{{ $value }}"></span>
								@endforeach
							@endif
						</div>
					</div>
										
					<div class="intro-y col-span-12 lg:col-span-12 variants_area">
						<p class="mt-5"><strong>Medicine Variants</strong></p>
						
					@if(isset($variant_rows)&& $variant_rows && $variant_rows->count() > 0)
						<section class="more_variants_wrapper">
						@foreach($variant_rows as $vrow)
							<div class="grid grid-cols-12 gap-6 mt-5 filds_wrapper">
							
								<div class="intro-y col-span-12 lg:col-span-2">
									<label>Variant Name</label>
									<input type="text" name="variants[name][]" value="{{ $vrow->variant_name }}" class="input w-full border mt-2" placeholder="Variant Name" required>
								</div>	

								<div class="intro-y col-span-12 lg:col-span-1">
									<label>Variant Price($)</label>
									<input type="number" name="variants[price][]" value="{{ $vrow->price }}" class="input w-full border mt-2" placeholder="Price" required min="0">
								</div>

								<div class="intro-y col-span-12 lg:col-span-1">
									<label>Qty</label>
									<input type="number" name="variants[qty][]" value="{{ $vrow->pill_qty }}" class="input w-full border mt-2" placeholder="Variant Qty" required min="1">
								</div>	

								<div class="intro-y col-span-12 lg:col-span-1">
									<label>Popular?</label>
									<br>
									<br>
									<input type="checkbox" name="variants[is_popular][]" class="input input--switch border" @if($vrow->is_popular == 1)checked @endif>
								</div>							
								
								<div class="intro-y col-span-12 lg:col-span-4">
									<div class="plan_type mr-2">
										<label>Payment Type</label>
										<select name="variants[is_recurring][]" class="input w-full border mt-2 is_recurring">
											<option value="0" @if($vrow->is_recurring == 0) selected @endif >On time</option>
											<option value="1" @if($vrow->is_recurring == 1) selected @endif >Recurring</option>
											<option value="2" @if($vrow->is_recurring == 2) selected @endif >Both</option>
										</select>
									</div>
									
									<div class="plan_interval" @if($vrow->is_recurring == 0) style="display:none;" @endif>
										<label>Interval</label>
										<select name="variants[plan_interval][]" class="input w-full border mt-2 change_interval">
											<option value="month" @if($vrow->plan_interval == 'month') selected @endif >Month</option>
											<option value="week" @if($vrow->plan_interval == 'week') selected @endif >Week</option>
											<option value="year" @if($vrow->plan_interval == 'year') selected @endif >Year</option>
											<option value="day" @if($vrow->plan_interval == 'day') selected @endif >Day</option>
											<option value="custom" @if($vrow->plan_interval == 'custom') selected @endif >Custom</option>
										</select>
									</div>
									
									<div class="custom_interval_count text-center" @if(($vrow->is_recurring == 1 || $vrow->is_recurring == 2) && $vrow->plan_interval == 'custom' ) @else style="display:none;" @endif>
										<label>Custom</label>
										<div class="customInterval mt-2">
											<input type="number" name="variants[interval_count][]" class="input border" min="1" value="1">
											<select name="variants[custom_plan_interval][]" class="input border">
												<option value="month" @if($vrow->custom_plan_interval == 'month') selected @endif >Months</option>
												<option value="week" @if($vrow->custom_plan_interval == 'week') selected @endif >Weeks</option>
												<option value="day" @if($vrow->custom_plan_interval == 'day') selected @endif >Days</option>
											</select>
										</div>
									</div>								
								</div>								
								
								<div class="intro-y col-span-12 lg:col-span-2">
									<label>Variant Detail</label>
									<textarea name="variants[detail][]" class="input w-full border mt-2" cols="50" rows="2">{{ $vrow->details }}</textarea>
								</div>	

								<div class="intro-y col-span-12 lg:col-span-1">
									<br><br>
									<input type="hidden" name="variants[id][]" value="{{ $vrow->id }}">
									<button type="button" class="button button--md w-10 btn_red remove_variant" title="Delete Variant" data-id="{{ $vrow->id }}">X</button>
								</div>								
							</div>
						@endforeach
						</section>
					@else
						<section class="more_variants_wrapper"></section>					
					@endif	
						
						<div class="intro-y col-span-12 lg:col-span-12">							
							<button type="button" class="button button--md w-36 mr-1 mb-2 btn_blue add_variant">Add New Variant</button>
						</div>	
					
					</div>
						
					<div class="intro-y col-span-12 lg:col-span-12 medicine_faq_area">
						<p class="mt-5"><strong>FAQ : Question/Answers</strong></p>
				
						@if($faq_rows && $faq_rows->count() > 0)
						<section class="more_items_wrapper">
							
							@foreach($faq_rows as $row)
								<div class="grid grid-cols-12 gap-6 mt-2 filds_wrapper">
									<div class="intro-y col-span-12 lg:col-span-11 mt-5">
										<input type="text" name="faq_data[question][]" class="input w-full border" placeholder="Question" value="{{ $row->question_data }}">
									</div>
									<div class="intro-y col-span-12 lg:col-span-1 mt-5">
										<button type="button" class="button button--md w-10 btn_red remove_item" style="float:right;"title="Delete FAQ">X</button>
									</div>									
									<div class="intro-y col-span-12 lg:col-span-12">
										<textarea  name="faq_data[answer][]" class="myanswer input w-full border" placeholder="Answer" cols="50" rows="4" id="faq_answer{{$x}}">{{ $row->answer_data }}</textarea>
									</div>
								</div>
								@php
									$x++;
								@endphp
							@endforeach						
						</section>
					@else
						<section class="more_items_wrapper"></section>					
					@endif	
					</div>
					<div class="intro-y col-span-12 lg:col-span-12">							
						<button type="button" class="button button--md w-36 mr-1 mb-2 btn_blue add_item">Add New FAQ</button>
					</div>						
					
				</div>
				
				<div class="intro-y col-span-12 lg:col-span-4">
					<label>Active Status</label>
					<div class="mt-2">
						<input type="checkbox" name="status" class="input input--switch border" <?php if($treatment_medicine && $treatment_medicine->status == "1"){ echo 'checked';}?>>
					</div>
				</div>
					
				<div class="grid grid-cols-12 gap-6 mt-12">
					<div class="intro-y col-span-12 lg:col-span-12">
						<input type="hidden" name="id" value="<?php if($treatment_medicine){ echo $treatment_medicine->id;}?>"/>
						<input type="hidden" name="page" value="<?php if($treatment_medicine){ echo 'edit';} else { echo 'add'; }?>"/>
						<button type="submit" id="saveBtn" class="button button--md w-24 mr-1 mb-2 btn_blue">Save</button>
						<button type="reset" class="button button--md w-24 mr-1 mb-2 btn_red" onclick="location.href = '{{ route('admin.treatment_medicines') }}';">Cancel</button>
					</div>
				</div>
			</form>
		</div>
	</div>

	<script type="text/javascript">
		CKEDITOR.replace( 'description' );
		var count = <?php echo $x;?>;
		var faq_rows = <?php echo $faq_rows ? $faq_rows->count() : 0;?>;
		
		var vcount = 0;	
		var variant_rows;

		if(faq_rows > 0){
			for(var i=1; i<=faq_rows; i++){
			
				CKEDITOR.replace('faq_answer'+i, {
					height: 600,
					filebrowserUploadUrl: "{{route('image-upload', ['_token' => csrf_token() ])}}",
					filebrowserImageUploadUrl: "{{route('image-upload', ['_token' => csrf_token() ])}}",
					filebrowserUploadMethod: 'form'
				});
				
			}
			CKEDITOR.add;
		}
		


	$("#treatment_medicineForm").submit(function(e) {
		e.preventDefault();
		
	    var start_price = $('#start_price').val();
		if(start_price <= 0){
			$("#start_price").focus();
			swal({
				 	title: 'Please enter a valid starting price.',
				 	icon: 'error'
				 })
			return false;
		}
		
		$('textarea.myanswer').each(function () {
		   var $textarea = $(this);
		   //console.log($textarea.attr('id'));
		   var ans_val = CKEDITOR.instances[$textarea.attr('id')].getData();
		   //console.log(ans_val);
		   $textarea.val(ans_val);
		   //console.log('textAreaVal=',$textarea.val());
		});
			
		var description = CKEDITOR.instances.description.getData();		
		var formData = new FormData(this);
		formData.append('description', description);
		$(".loader").css('display', 'flex');
		
		$.ajax({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			url: "{{ route('admin.treatment_medicines.store') }}",
			data: formData,
			type: "POST",
			cache:false,
			contentType: false,
			processData: false,
			// dataType: 'json',
			success: function (response) {
				$(".loader").css('display', 'none');
				
				if(response['status'] == 'success'){
					swal({
						title: response['message'],
						icon: 'success'
					});
					$('#treatment_medicineForm').trigger("reset");
					setTimeout(function(){
					swal.close();
					window.location = "{{ route('admin.treatment_medicines') }}"; }, 1000);
				}
			},
			error: function (data) {
				let errorArr = [];
				let errors = data.responseJSON.errors;
				let emailErr = errors.email[0];
				// $.each(errors, function(key, value) {

				// });
				//console.log('Error:', data);
				// swal({
				// 	title: emailErr,
				// 	icon: 'error'
				// })
				// $('#saveBtn').html('Save');
			}
		});
	});


	// Image reader
	if (window.File && window.FileList && window.FileReader) {
		$("#treatment_medicine_images").on("change", function(e) {
			$('.pip').remove();
			var files = e.target.files,
			filesLength = files.length;
			for (var i = 0; i < filesLength; i++) {
				var f = files[i]
				var file = this.files[i];
				var fileType = file["type"];
				
				var validImageTypes = ["image/gif", "image/jpeg", "image/png"];
				if ($.inArray(fileType, validImageTypes) < 0) {
					$('#treatment_medicine_images').val('');
				}else{
					
					var fileReader = new FileReader();
					fileReader.onload = (function(e) {
						var file = e.target;
						$images = "<span class='pip'>" +
							"<img class='imageThumb' src=\"" + e.target.result + "\" title=\"" + file.name + "\" width='80' height='80'/>" +
							"</span>";
						$(".images_preview").html($images);
						$(".remove").click(function(){
							$(this).parent(".pip").remove();
						});
					});
					fileReader.readAsDataURL(f);
					
				}
				
			}
		});
	} else {
		alert("Your browser doesn't support to File API")
	}
	
	$(document).ready(function() {
			
		$('.add_item').click(function() {
			$('.more_items_wrapper').append(
				'<div class="grid grid-cols-12 gap-6 mt-2 filds_wrapper">' +
					'<div class="intro-y col-span-12 lg:col-span-11 mt-5">' +
						'<input type="text" name="faq_data[question][]" class="input w-full border" placeholder="Question">' +
					'</div>' +
					'<div class="intro-y col-span-12 lg:col-span-1 mt-5">' +
						'<button type="button" class="button button--md w-10 btn_red remove_item" style="float:right;" title="Delete FAQ">X</button>' +
					'</div>' +					
					'<div class="intro-y col-span-12 lg:col-span-12">' +
						'<textarea name="faq_data[answer][]" class="myanswer input w-full border" placeholder="Answer" cols="50" rows="4" id="faq_answer'+count+'"></textarea>' +
					'</div>' +
				'</div>'
			);
			
			CKEDITOR.replace('faq_answer'+count, {
				height: 600,
				filebrowserUploadUrl: "{{route('image-upload', ['_token' => csrf_token() ])}}",
				filebrowserImageUploadUrl: "{{route('image-upload', ['_token' => csrf_token() ])}}",
				filebrowserUploadMethod: 'form'
			});
			
			CKEDITOR.add;			
			count++;
		});

		$(document).on('click', '.more_items_wrapper .remove_item', function() {
			$(this).closest('.filds_wrapper').remove();
			count--;
		});
		
		
		$('.add_variant').click(function() {
			$('.more_variants_wrapper').append(
				'<div class="grid grid-cols-12 gap-6 mt-5 filds_wrapper">' +
					'<div class="intro-y col-span-12 lg:col-span-2">' +
						'<label>Variant Name</label><input type="text" name="variants[name][]" value="" class="input w-full border mt-2" placeholder="Variant Name" required>' +
					'</div>' +
					'<div class="intro-y col-span-12 lg:col-span-1">' +
						'<label>Variant Price($)</label><input type="number" name="variants[price][]" value="" class="input w-full border mt-2" placeholder="Price" required min="0">' +
					'</div>' +					
					'<div class="intro-y col-span-12 lg:col-span-1">' +
						'<label>Qty</label><input type="number" name="variants[qty][]" value="1" class="input w-full border mt-2" placeholder="Variant Qty" required min="1">' +
					'</div>' + 
					'<div class="intro-y col-span-12 lg:col-span-1">' +
						'<label>Popular?</label><br><br><input type="checkbox" name="variants[is_popular][]" class="input input--switch border">' +
					'</div>' +
					
					'<div class="intro-y col-span-12 lg:col-span-4">' +
						'<div class="plan_type mr-2"><label>Payment Type</label><select name="variants[is_recurring][]" class="input w-full border mt-2 is_recurring"><option value="0">On time</option><option value="1">Recurring</option><option value="2">Both</option></select></div>' +
						'<div class="plan_interval" style="display:none;"><label>Interval</label><select name="variants[plan_interval][]" class="input w-full border mt-2 change_interval"><option value="month">Month</option><option value="week">Week</option><option value="year">Year</option><option value="day">Day</option><option value="custom">Custom</option></select></div>' +
						'<div class="custom_interval_count text-center" style="display:none;"><label>Custom</label><div class="customInterval mt-2"><input type="number" name="variants[interval_count][]" class="input border" min="1" value="1"><select name="variants[custom_plan_interval][]" class="input border"><option value="month">Months</option><option value="week">Weeks</option><option value="day">Days</option></select></div></div>' +
					'</div>' +	
					
					'<div class="intro-y col-span-12 lg:col-span-2">' +
						'<label>Variant Detail</label><textarea name="variants[detail][]" class="input w-full border mt-2" cols="50" rows="2"></textarea>' +
					'</div>' +
					
					'<div class="intro-y col-span-12 lg:col-span-1">' +
						'<br><br><input type="hidden" name="variants[id][]" value=""><button type="button" class="button button--md w-10 btn_red remove_variant" title="Delete Variant" data-id="">X</button>' +
					'</div>' +					
				'</div>'
			);			
			vcount++;
		});

		$(document).on('click', '.more_variants_wrapper .remove_variant', function() {
			let v_id = $(this).attr('data-id');
			
			if(v_id && v_id!= ''){
				deleteRow($(this),v_id);
			}else{
				$(this).closest('.filds_wrapper').remove();
				vcount--;
			}
			
			
			
		});	

		//if Recurring/On time
		$(document).on('change', '.more_variants_wrapper .is_recurring', function() {
			
		if($(this).val() == 1 || $(this).val() == 2 ){
				$(this).parent().parent().find('.plan_interval').show();
			}else{
				$(this).parent().parent().find('.plan_interval').hide();
				$(this).parent().parent().find('.plan_interval select').val('month');
				$(this).parent().parent().find('.custom_interval_count').hide();
			}
		});
		
		//if Recurring/On time
		$(document).on('change', '.more_variants_wrapper .change_interval', function() {
			
		if($(this).val() == 'custom' ){
				$(this).parent().parent().find('.custom_interval_count').show();
			}else{
				$(this).parent().parent().find('.custom_interval_count').hide();
			}
		});		
			
	});		


		function deleteRow(element,id) {
			swal({
				title: "Are you sure to delete?",
				text: "",
				icon: 'warning',
				buttons: {
				cancel: true,
				delete: 'Yes, Delete It'
				}
			}).then((isConfirm) => {
				if (!isConfirm) {
					return false;
				} else {
					$(".loader").css('display', 'flex');
					$.ajax({
						type: "DELETE",
						headers: {
							'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
						},						
						url: "{{ env('APP_URL').'/admin/treatment_medicines/variant_destroy' }}"+'/'+id,
						success: function (response) {
							$(".loader").css('display', 'none');
							
							if(response['status'] == 'success'){
								swal({
									title: response['message'],
									icon: 'success'
								});
								setTimeout(function(){
									element.closest('.filds_wrapper').remove();
									vcount--;
									swal.close();
								}, 1000);
							}else{
								swal({
									title: response['message'],
									icon: 'error'
								});
								setTimeout(function(){
									swal.close();
								}, 1000);								
							}
							
						},
						error: function (data) {
							console.log('Error:', data);
							swal({
								title: 'Error Occured',
								icon: 'error'
							});
							setTimeout(function(){
								swal.close();
							}, 1000);							
						}
					});
				}
			});
		}	
		

</script>
</html>
@endsection