@extends('layouts.app')
@section('content')
	<div class="intro-y box mt-5">
		<div class="flex flex-col sm:flex-row items-center p-5 border-b border-gray-200 dark:border-dark-5">
			<h2 class="font-medium text-base mr-auto">@if($service) {{ 'Edit' }} @else Add New @endif Service/Treatment</h2>
		</div>
		<div class="intro-y datatable-wrapper box p-5 col-span-12 overflow-auto lg:overflow-hidden">
			<form id="ServiceForm">					
				<div class="grid grid-cols-12 gap-6 mt-5">
					<div class="intro-y col-span-12 lg:col-span-4">
						<div>
							<label>Name</label>
							<input type="text" name="name" value="<?php if($service){ echo $service->name;}?>" class="input w-full border mt-2" placeholder="Name" id="name" required>
						</div>
					</div>					
					<div class="intro-y col-span-12 lg:col-span-4">
						<label>Visit Type</label>
						<div class="mt-2">
							<select id="visit_type" name="visit_type" class="select2 w-full" required>
								<option value="1" <?php if($service && $service->visit_type == '1'){ echo 'selected'; }?>>Asynchronous Telemedicine</option>
								<option value="2" <?php if($service && $service->visit_type == '2'){ echo 'selected'; }?>>Synchronous Telemedicine</option>
								<option value="3" <?php if($service && $service->visit_type == '3'){ echo 'selected'; }?>>Concierge</option>
							</select>
						</div>
					</div>
					<div class="intro-y col-span-12 lg:col-span-4">
						<label>Male/Female</label>
						<div class="mt-2">
							<select id="available_for" name="available_for" class="select2 w-full" required>
								<option value="male" <?php if($service && $service->available_for == 'male'){ echo 'selected'; }?>>Male</option>
								<option value="female" <?php if($service && $service->available_for == 'female'){ echo 'selected'; }?>>Female</option>
								<option value="both" <?php if($service && $service->available_for == 'both'){ echo 'selected'; }?>>Both</option>
							</select>
						</div>
					</div>					
					<div class="intro-y col-span-12 lg:col-span-12">
						<label>Short Info</label>
						<textarea name="short_info" class="input w-full border mt-2" cols="20" rows="3" required><?php if($service){ echo $service->short_info; }?></textarea>
					</div>
					<div class="intro-y col-span-12 lg:col-span-12">
						<label>Description</label>
						<textarea name="description" id="description" class="input w-full border mt-2" cols="30" rows="4"><?php if($service){ echo $service->description; }?></textarea>
					</div>
					<div class="intro-y col-span-12 lg:col-span-6">
						<label>Display Image</label>
						<input type="file" name="image" class="input w-full border mt-2" id="service_image" accept="image/*">
						<div class="images_preview mt-3">
							@if($service && $service->image != '')
								<input type="hidden" name="old_image" value="{{ $service->image }}">
								<span class="pip"><img class="imageThumb" src="/{{ $service->image }}"></span>
							@endif
						</div>
					</div>					
				</div>				

					
				<div class="mt-4">
					<label>Active Status</label>
					<div class="mt-2">
						<input type="checkbox" name="status" class="input input--switch border" @if($service && $service->status == 1) checked @endif>
					</div>
				</div>
				<div class="grid grid-cols-12 gap-6 mt-12">
					<div class="intro-y col-span-12 lg:col-span-12">
						<input type="hidden" name="id" value="<?php if($service){ echo $service->id;}?>"/>
						<input type="hidden" name="page" value="<?php if($service){ echo 'edit';} else { echo 'add'; }?>"/>			
						<button type="submit" id="saveBtn" class="button button--md w-24 mr-1 mb-2 btn_blue">Save</button>
						<button type="reset" class="button button--md w-24 mr-1 mb-2 btn_red" onclick="location.href = '{{ route('admin.services') }}';">Cancel</button>
					</div>
				</div>
			</form>
		</div>
	</div>

	<script type="text/javascript">
	
		CKEDITOR.replace( 'description' );
		CKEDITOR.add;
		
		$(document).ready(function() {

			$("#ServiceForm").submit(function(e) {
				e.preventDefault();
				var description = CKEDITOR.instances.description.getData();
		
				var formData = new FormData(this);
				formData.append('description', description);
		
				$.ajax({
					headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					},
					//data: $('#ServiceForm').serialize(),
					url: "{{ route('admin.services.store') }}",					
					data: formData,
					type: "POST",
					cache:false,
					contentType: false,
					processData: false,
					// dataType: 'json',					
					success: function (response) {
						//response = JSON.parse(res);
						if(response['status'] == 'success'){
							swal({
								title: response['message'],
								icon: 'success'
							});
							$('#ServiceForm').trigger("reset");
							setTimeout(function(){
								swal.close();
								window.location = "{{ route('admin.services') }}";
							}, 1000);
						}
					},
					error: function (data) {
						//console.log('Error:', data);
						swal({
							title: 'Error Occured.',
							icon: 'error'
						})
						$('#saveBtn').html('Save');
					}
				});
			});

		});

	// Image reader
	if (window.File && window.FileList && window.FileReader) {
		$("#service_image").on("change", function(e) {
			$('.pip').remove();
			var files = e.target.files,
			filesLength = files.length;
			for (var i = 0; i < filesLength; i++) {
				var f = files[i]
				var file = this.files[i];
				var fileType = file["type"];
				console.log(fileType);
				var validImageTypes = ["image/gif", "image/jpeg", "image/png"];
				if ($.inArray(fileType, validImageTypes) < 0) {
					$('#service_image').val('');
				}else{
					
					var fileReader = new FileReader();
					fileReader.onload = (function(e) {
						var file = e.target;
						$images = "<span class='pip'>" +
							"<img class='imageThumb' src=\"" + e.target.result + "\" title=\"" + file.name + "\" width='80' height='80'/>" +
							"</span>";
						$(".images_preview").html($images);
						$(".remove").click(function(){
							$(this).parent(".pip").remove();
						});
					});
					fileReader.readAsDataURL(f);
					
				}
			}
		});
	} else {
		alert("Your browser doesn't support to File API")
	}
	</script>
</html>
@endsection