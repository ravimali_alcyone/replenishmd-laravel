@extends('layouts.app')
@section('content')
	<link rel="stylesheet" href="{{asset('css/questions.css')}}" />
	<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.1/css/all.css">	
	<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
	<script type="text/javascript" src="https://cdn.jsdelivr.net/gh/RubaXa/Sortable/Sortable.min.js"></script>
	<style>
		#demo1 li { list-style:none;}
		#demo1 li:hover { cursor: all-scroll;}
		.add_cond_ques { display:none;}
	</style>	
    <section class="vfq_wrap">
		<form id="VisitFieldForm">
			<div id="vf_qus" class="vs_wrapper">
				<div class="vs_page_title"><h2>Visit Form Questions Pool - <b>{{$visit_form->name}}</b></h2></div>
				
				
				<ul class="list-group" id="demo1">			
	@if($visit_form)
		@if($visit_questions && $visit_questions->count() > 0)
			@foreach($visit_questions as $key => $row)

				@if($row->question_type == 'header_text')
					<li class="list-group-item parent" data-type="{{$row->question_type}}" data-id="{{$row->id}}">
						<div class="q_box">
							<div class="q_actions">
								<div class="act_btn_grp">
									<a href="javascript:void(0)" class="act_btn trash removeQ"><i class="fa fa-trash"></i></a>
									<a href="javascript:void(0)" class="act_btn up_dwn" onclick="copyInputType(6)"><i class="fa fa-copy"></i></a>
								</div>
							</div>
							<div class="qb_title"><b>Header Text:</b></div>
							<div class="qb_area">
								<div class="qb_input">
									<input type="text" class="q_field" placeholder="Type a title" value="{{ $row->question_text}}">
									<input type="hidden" class="q_field_id" value="{{ $row->id}}">
								</div>
							</div>
						</div>
					</li>
				@endif
				
				@if($row->question_type == 'short_text')
					<li class="list-group-item parent" data-type="{{$row->question_type}}" data-id="{{$row->id}}">
						<div class="q_box">
							<div class="q_actions">
								<div class="act_btn_grp">
									<a href="javascript:void(0)" class="act_btn trash removeQ"><i class="fa fa-trash"></i></a>
									<a href="javascript:void(0)" class="act_btn up_dwn" onclick="copyInputType(1)"><i class="fa fa-copy"></i></a>
								</div>
							</div>
							<div class="qb_title"><b>Short Text Question:</b></div>
							<div class="qb_area">
								<div class="qb_input">
									<input type="text" class="q_field" placeholder="Type a title" value="{{ $row->question_text}}">
									<input type="hidden" class="q_field_id" value="{{ $row->id}}">
								</div>
							</div>
						</div>
					</li>			
				@endif

				@if($row->question_type == 'long_text')
					<li class="list-group-item parent" data-type="{{$row->question_type}}" data-id="{{$row->id}}">
						<div class="q_box">
							<div class="q_actions">
								<div class="act_btn_grp">
									<a href="javascript:void(0)" class="act_btn trash removeQ"><i class="fa fa-trash"></i></a>
									<a href="javascript:void(0)" class="act_btn up_dwn" onclick="copyInputType(2)"><i class="fa fa-copy"></i></a>
								</div>
							</div>
							<div class="qb_title"><b>Long Text Question:</b></div>
							<div class="qb_area">
								<div class="qb_input">
									<textarea class="qtxt_field q_field" rows="5" placeholder="">{{ $row->question_text}}</textarea>
									<input type="hidden" class="q_field_id" value="{{ $row->id}}">
								</div>
							</div>
						</div>
					</li>			
				@endif

				@if($row->question_type == 'yes_no')
					<li class="list-group-item parent" data-type="{{$row->question_type}}" data-id="{{$row->id}}">
						<div class="q_box">
							<div class="q_actions">
								<div class="act_btn_grp">
									<a href="javascript:void(0)" class="act_btn trash removeQ"><i class="fa fa-trash"></i></a>
									<a href="javascript:void(0)" class="act_btn up_dwn" onclick="copyInputType(3)"><i class="fa fa-copy"></i></a>
								</div>
							</div>
							<div class="qb_title"><b>Yes/No Question:</b></div>
							
							<div class="qb_area">
								<div class="qb_input">
									<input type="text" class="q_field" placeholder="Type a question" value="{{ $row->question_text}}">
									<input type="hidden" class="q_field_id" value="{{ $row->id }}">
								</div>							
								<div class="qb_child_input_wrapper">
							@php $optionsArray = App\Http\Controllers\admin\VisitFormController::getOptions($row->visit_form_id,$row->id); @endphp
							@if($optionsArray && $optionsArray->count() > 0)
								@php $x=0; @endphp
								@foreach($optionsArray as $op)								
									<div class="qb_child_input parent_option">
										<div class="input_box">
											<input type="text" class="q_field" placeholder="@if($x==0)Yes @else No @endif" value="{{$op->option_text}}">
											<input type="hidden" class="q_field_id" value="{{ $op->id }}">
										</div>
										<div class="check_box">
											<input type="checkbox" class="form-check-input check_cond_box" id="exampleCheck{{$op->id}}" @if($op->is_conditional == 1) checked @endif>
											<label class="form-check-label" for="exampleCheck{{$op->id}}">Is conditional?</label>
										</div>
										<div class="con_add_qus @if($op->is_conditional != 1) add_cond_ques @endif">
											<a href="javascript:void(0)" class="act_btn trash vf_add_btn_drp"><i class="fa fa-plus"></i></a>
										</div>
										
										@if($op->is_conditional == 1)
											@php $child_questions = App\Http\Controllers\admin\VisitFormController::get_child_question($row->visit_form_id,$op->id); @endphp
											
											@if($child_questions)
												{!! $child_questions !!}
											@endif
										@endif
										
									</div>
									@php $x++; @endphp
								@endforeach
							@endif
								</div>
							</div>	
						</div>
					</li>			
				@endif	

				@if($row->question_type == 'single')
					<li class="list-group-item parent" data-type="{{$row->question_type}}" data-id="{{$row->id}}">
						<div class="q_box">
							<div class="q_actions">
								<div class="act_btn_grp">
									<a href="javascript:void(0)" class="act_btn trash removeQ"><i class="fa fa-trash"></i></a>
									<a href="javascript:void(0)" class="act_btn up_dwn" onclick="copyInputType(4)"><i class="fa fa-copy"></i></a>
								</div>
							</div>
							<div class="qb_title"><b>Single Choice Question:</b></div>
							<div class="qb_area">
								<div class="qb_input">
									<input type="text" class="q_field" placeholder="Type a question" value="{{ $row->question_text }}">
									<input type="hidden" class="q_field_id" value="{{ $row->id }}">
								</div>
								<div class="qb_child_input_wrapper">
							@php $optionsArray = App\Http\Controllers\admin\VisitFormController::getOptions($row->visit_form_id,$row->id); @endphp
							@if($optionsArray && $optionsArray->count() > 0)
								@php $x=0; @endphp
								@foreach($optionsArray as $op)								
									<div class="qb_child_input parent_option">
										<div class="q_actions">
											<a href="javascript:void(0)" class="act_btn trash del_btn"><i class="fa fa-trash"></i></a>
										</div>
										<div class="input_box">
											<input type="text" class="q_field" placeholder="Type option here..." value="{{$op->option_text}}">
											<input type="hidden" class="q_field_id" value="{{ $op->id }}">
										</div>
										<div class="check_box">
											<input type="checkbox" class="form-check-input check_cond_box" id="exampleCheck{{$op->id}}" @if($op->is_conditional == 1) checked @endif>
											<label class="form-check-label" for="exampleCheck{{$op->id}}">Is conditional?</label>
										</div>
										<div class="con_add_qus @if($op->is_conditional != 1) add_cond_ques @endif">
											<a href="javascript:void(0)" class="act_btn trash vf_add_btn_drp"><i class="fa fa-plus"></i></a>
										</div>
										
										@if($op->is_conditional == 1)
											@php $child_questions = App\Http\Controllers\admin\VisitFormController::get_child_question($row->visit_form_id,$op->id); @endphp
											
											@if($child_questions)
												{!! $child_questions !!}
											@endif
										@endif
										
									</div>
									@php $x++; @endphp
								@endforeach
							@endif
								</div>
								<div class="add_new_ans"><a href="javascript:void(0)" class="add_btn"><i class="fa fa-plus"></i></a></div>
							</div>	
						</div>
					</li>			
				@endif	

				@if($row->question_type == 'multiple')
					<li class="list-group-item parent" data-type="{{$row->question_type}}" data-id="{{$row->id}}">
						<div class="q_box">
							<div class="q_actions">
								<div class="act_btn_grp">
									<a href="javascript:void(0)" class="act_btn trash removeQ"><i class="fa fa-trash"></i></a>
									<a href="javascript:void(0)" class="act_btn up_dwn" onclick="copyInputType(5)"><i class="fa fa-copy"></i></a>
								</div>
							</div>
							<div class="qb_title"><b>Multiple Choice Question:</b></div>
							<div class="qb_area">
								<div class="qb_input">
									<input type="text" class="q_field" placeholder="Type a question" value="{{ $row->question_text }}">
									<input type="hidden" class="q_field_id" value="{{ $row->id }}">
								</div>
								<div class="qb_child_input_wrapper">
							@php $optionsArray = App\Http\Controllers\admin\VisitFormController::getOptions($row->visit_form_id,$row->id); @endphp
							@if($optionsArray && $optionsArray->count() > 0)
								@php $x=0; @endphp
								@foreach($optionsArray as $op)								
									<div class="qb_child_input parent_option">
										<div class="q_actions">
											<a href="javascript:void(0)" class="act_btn trash del_btn"><i class="fa fa-trash"></i></a>
										</div>
										<div class="input_box">
											<input type="text" class="q_field" placeholder="Type option here..." value="{{$op->option_text}}">
											<input type="hidden" class="q_field_id" value="{{ $op->id }}">
										</div>
										<div class="check_box">
											<input type="checkbox" class="form-check-input check_cond_box" id="exampleCheck{{$op->id}}" @if($op->is_conditional == 1) checked @endif>
											<label class="form-check-label" for="exampleCheck{{$op->id}}">Is conditional?</label>
										</div>
										<div class="con_add_qus @if($op->is_conditional != 1) add_cond_ques @endif">
											<a href="javascript:void(0)" class="act_btn trash vf_add_btn_drp"><i class="fa fa-plus"></i></a>
										</div>
										
										@if($op->is_conditional == 1)
											@php $child_questions = App\Http\Controllers\admin\VisitFormController::get_child_question($row->visit_form_id,$op->id); @endphp
											
											@if($child_questions)
												{!! $child_questions !!}
											@endif
										@endif
										
									</div>
									@php $x++; @endphp
								@endforeach
							@endif									
								</div>
								<div class="add_new_ans"><a href="javascript:void(0)" class="add_btn"><i class="fa fa-plus"></i></a></div>
							</div>	
						</div>
					</li>			
				@endif				
			
			@endforeach
		@endif
	@endif


				</ul>
				
				
				
				
				<div id="qus_drop_open" class="qus_selection_box"  style="display: none;">
					<ul class="qsb">
				@if($form_field_types && $form_field_types->count() > 0)
					@foreach($form_field_types as $field)
						<li><a href="javascript:void(0)" onclick="copyInputType({{ $field->id }})"><i class="{{ $field->icon }}"></i>{{ $field->field_type_name }}</a></li>
					@endforeach
				@endif
					</ul>
				</div>			
				<div class="add_new_que"><a id="vf_add_btn" href="javascript:void(0)" class="add_btn_list"><i class="fa fa-plus"></i> &nbsp;Add New Question</a></div>
			</div>
		<!--button type="submit" class="add_btn_list">Save</button-->
		
				<div class="grid grid-cols-12 gap-6 mt-6" id="FormActionBtn">
					<div class="intro-y col-span-12 lg:col-span-12 pt-5 border-t border-gray-200 dark:border-dark-5" style="text-align: center;">
						<input type="hidden" name="formid" id="formid" value="<?php if($visit_form){ echo $visit_form->id;}?>"/>
						<button type="submit" id="saveBtn" class="button button--md w-24 mr-1 mb-2 btn_blue" <?php if($visit_questions && $visit_questions->count() > 0){}else{ echo 'disabled';}?>>Save</button>
						<button type="reset" class="button button--md w-24 mr-1 mb-2 btn_red" onclick="location.href = '{{env('APP_URL')}}<?php if($visit_form){ echo '/admin/visit_forms/add/'.$visit_form->id;}?>'">Cancel</button>
					</div>
				</div>
				
		</form>
    </section>
    
    <script>
        $(document).ready(function(){
			
			$( "#vf_add_btn" ).click(function() {
				$( "#qus_drop_open" ).slideToggle( "slow", function() {
					//window.scrollBy(0, 200);
				});
			});
			
			$(document).on("click", ".vf_add_btn_drp", function() {
				if($(this).find("#qus_drop_down").length == 1) {
					$(this).closest(".vf_add_btn_drp").find("#qus_drop_down").slideToggle( "slow");
				} else {
					$(this).append(add_child_ques);
					$(this).closest(".vf_add_btn_drp").find("#qus_drop_down").slideToggle( "slow");
				}
			});
			
			$(document).on("click", ".check_cond_box", function() {
				if(this.checked)
				{
					$(this).parent().parent().find(".con_add_qus").removeClass("add_cond_ques");
				}
				else{
					$(this).closest(".qb_child_input").find(".con_add_qus").addClass("add_cond_ques");
				}
			});
			
        });


		var add_child_ques = '<div id="qus_drop_down" class="qus_selection_box"  style="display: none;">'+
		

									'<ul class="qsb">'+							
									@if($form_field_types && $form_field_types->count() > 0)
										@foreach($form_field_types as $field)							
											'<li><a href="javascript:void(0)" data-id="{{$field->id}}" class="create_child_input"><i class="{{$field->icon}}"></i>{{$field->field_type_name}}</a></li>'+
										@endforeach
									@endif
										'</ul>'+

									'</div>';
			
			var inputText = '<li class="list-group-item" data-type="short_text"><div class="q_box"><div class="q_actions"><div class="act_btn_grp"><a href="javascript:void(0)" class="act_btn trash removeQ"><i class="fa fa-trash"></i></a><a href="javascript:void(0)" class="act_btn up_dwn" onclick="copyInputType(1)"><i class="fa fa-copy"></i></a></div></div><div class="qb_title"><b>Short Text Question:</b></div><div class="qb_area"><div class="qb_input"><input type="text" class="q_field" placeholder="Type a question" value=""><input type="hidden" class="q_field_id" value=""></div></div></div></li>';
			
			var inputLongText = '<li class="list-group-item" data-type="long_text">'+
									'<div class="q_box">'+
										'<div class="q_actions">'+
											'<div class="act_btn_grp">'+
												'<a href="javascript:void(0)" class="act_btn trash removeQ"><i class="fa fa-trash"></i></a>'+
												'<a href="javascript:void(0)" class="act_btn up_dwn" onclick="copyInputType(2)"><i class="fa fa-copy"></i></a>'+
											'</div>'+
										'</div>'+
										'<div class="qb_title"><b>Long Text Question:</b></div>'+
										'<div class="qb_area">'+
											'<div class="qb_input"><textarea class="qtxt_field q_field" rows="5" placeholder=""></textarea></div>'+
										'</div>'+
									'</div>'+
								'</li>';
			
			var inputYesNo = '<li class="list-group-item" data-type="yes_no">'+				
								'<div class="q_box">'+
									'<div class="q_actions">'+
										'<div class="act_btn_grp">'+
										'<a href="javascript:void(0)" class="act_btn trash removeQ"><i class="fa fa-trash"></i></a>'+
										'<a href="javascript:void(0)" class="act_btn up_dwn" onclick="copyInputType()"><i class="fa fa-copy"></i></a>'+
									'</div>'+
									'</div>'+
									'<div class="qb_title"><b>Yes/No Question:</b></div>'+
									'<div class="qb_area">'+
										'<div class="qb_input"><input type="text" class="q_field" placeholder="Type a question" value=""><input type="hidden" class="q_field_id" value=""></div>'+
										'<div class="qb_child_input_wrapper">'+
											'<div class="qb_child_input">'+
												'<div class="input_box"><input type="text" class="q_field" placeholder="Yes" value=""><input type="hidden" class="q_field_id" value=""></div>'+
												'<div class="check_box">'+
													'<input type="checkbox" class="form-check-input check_cond_box" id="exampleCheck1">'+
													'<label class="form-check-label" for="exampleCheck1">Is conditional?</label>'+
												'</div>'+
												'<div class="con_add_qus add_cond_ques">'+
													'<a href="javascript:void(0)" class="act_btn trash vf_add_btn_drp"><i class="fa fa-plus"></i></a>'+
												'</div>'+
											'</div>'+
											'<div class="qb_child_input">'+
												'<div class="input_box"><input type="text" class="q_field" placeholder="No" value=""><input type="hidden" class="q_field_id" value=""></div>'+
												'<div class="check_box">'+
													'<input type="checkbox" class="form-check-input check_cond_box" id="exampleCheck1">'+
													'<label class="form-check-label" for="exampleCheck1">Is conditional?</label>'+
												'</div>'+
												'<div class="con_add_qus add_cond_ques">'+
													'<a href="javascript:void(0)" class="act_btn trash vf_add_btn_drp"><i class="fa fa-plus"></i></a>'+
												'</div>'+
											'</div>'+
										'</div>'+
									'</div>'+
								'</div>'+
							'</li>';
			
			var inputSingleQuestion = '<li class="list-group-item" data-type="single">'+
										'<div class="q_box">'+
											'<div class="q_actions">'+
												'<div class="act_btn_grp"> '+                       
												'<a href="javascript:void(0)" class="act_btn trash removeQ"><i class="fa fa-trash"></i></a>'+
												'<a href="javascript:void(0)" class="act_btn up_dwn" onclick="copyInputType(4)"><i class="fa fa-copy"></i></a>'+
											'</div>'+
											'</div>'+
											'<div class="qb_title"><b>Single Choice Question:</b></div>'+
											'<div class="qb_area">'+
												'<div class="qb_input"><input type="text" class="q_field" placeholder="Type a question" value=""><input type="hidden" class="q_field_id" value=""></div>'+
												'<div class="qb_child_input_wrapper">'+
													'<div class="qb_child_input">'+
														'<div class="q_actions">'+
															'<a href="javascript:void(0)" class="act_btn trash del_btn"><i class="fa fa-trash"></i></a>'+
														'</div>'+
														'<div class="input_box"><input type="text" class="q_field" placeholder="Type option here..." value=""><input type="hidden" class="q_field_id" value=""></div>'+
														'<div class="check_box">'+
															'<input type="checkbox" class="form-check-input check_cond_box" id="exampleCheck1">'+
															'<label class="form-check-label" for="exampleCheck1">Is conditional?</label>'+
														'</div>'+
														'<div class="con_add_qus add_cond_ques">'+
															'<a href="javascript:void(0)" class="act_btn trash vf_add_btn_drp"><i class="fa fa-plus"></i></a>'+
														'</div>'+
													'</div>'+
													'<div class="qb_child_input">'+
														'<div class="q_actions">'+
															'<a href="javascript:void(0)" class="act_btn trash del_btn"><i class="fa fa-trash"></i></a>'+
														'</div>'+
														'<div class="input_box"><input type="text" class="q_field" placeholder="Type option here..." value=""><input type="hidden" class="q_field_id" value=""></div>'+
														'<div class="check_box">'+
															'<input type="checkbox" class="form-check-input check_cond_box" id="exampleCheck1">'+
															'<label class="form-check-label" for="exampleCheck1">Is conditional?</label>'+
														'</div>'+
														'<div class="con_add_qus add_cond_ques">'+
															'<a href="javascript:void(0)" class="act_btn trash vf_add_btn_drp"><i class="fa fa-plus"></i></a>'+
														'</div>'+
													'</div>'+
													'<div class="qb_child_input">'+
														'<div class="q_actions">'+
															'<a href="javascript:void(0)" class="act_btn trash del_btn"><i class="fa fa-trash"></i></a>'+
														'</div>'+
														'<div class="input_box"><input type="text" class="q_field" placeholder="Type option here..." value=""><input type="hidden" class="q_field_id" value=""></div>'+
														'<div class="check_box">'+
															'<input type="checkbox" class="form-check-input check_cond_box" id="exampleCheck1">'+
															'<label class="form-check-label" for="exampleCheck1">Is conditional?</label>'+
														'</div>'+
														'<div class="con_add_qus add_cond_ques">'+
															'<a href="javascript:void(0)" class="act_btn trash vf_add_btn_drp"><i class="fa fa-plus"></i></a>'+
														'</div>'+
													'</div>'+
												'</div>'+
												'<div class="add_new_ans"><a href="javascript:void(0)" class="add_btn"><i class="fa fa-plus"></i></a></div>'+
											'</div>'+
										'</div>'+
									'</li>';
							
			var inputMultiQuestion = '<li class="list-group-item" data-type="multiple">'+
										'<div class="q_box">'+
											'<div class="q_actions">'+
												'<div class="act_btn_grp">'+                        
												'<a href="javascript:void(0)" class="act_btn trash removeQ"><i class="fa fa-trash"></i></a>'+
												'<a href="javascript:void(0)" class="act_btn up_dwn" onclick="copyInputType()"><i class="fa fa-copy"></i></a>'+
											'</div>'+
											'</div>'+
											'<div class="qb_title"><b>Multiple Choice Question:</b></div>'+
											'<div class="qb_area">'+
												'<div class="qb_input"><input type="text" class="q_field" placeholder="Type a question" value=""><input type="hidden" class="q_field_id" value=""></div>'+
												'<div class="qb_child_input_wrapper">'+
												'<div class="qb_child_input">'+
													'<div class="q_actions">'+
														'<a href="javascript:void(0)" class="act_btn trash del_btn"><i class="fa fa-trash"></i></a>'+
													'</div>'+
													'<div class="input_box"><input type="text" class="q_field" placeholder="Type option here..." value=""><input type="hidden" class="q_field_id" value=""></div>'+
													'<div class="check_box">'+
														'<input type="checkbox" class="form-check-input check_cond_box" id="exampleCheck1">'+
														'<label class="form-check-label" for="exampleCheck1">Is conditional?</label>'+
													'</div>'+
													'<div class="con_add_qus add_cond_ques">'+
														'<a href="javascript:void(0)" class="act_btn trash vf_add_btn_drp"><i class="fa fa-plus"></i></a>'+
													'</div>'+
												'</div>'+
												'<div class="qb_child_input">'+
													'<div class="q_actions">'+
														'<a href="javascript:void(0)" class="act_btn trash del_btn"><i class="fa fa-trash"></i></a>'+
													'</div>'+
													'<div class="input_box"><input type="text" class="q_field" placeholder="Type option here..." value=""><input type="hidden" class="q_field_id" value=""></div>'+
													'<div class="check_box">'+
														'<input type="checkbox" class="form-check-input check_cond_box" id="exampleCheck1">'+
														'<label class="form-check-label" for="exampleCheck1">Is conditional?</label>'+
													'</div>'+
													'<div class="con_add_qus add_cond_ques">'+
														'<a href="javascript:void(0)" class="act_btn trash vf_add_btn_drp"><i class="fa fa-plus"></i></a>'+
													'</div>'+
												'</div>'+
												'</div>'+
												'<div class="add_new_ans"><a href="javascript:void(0)" class="add_btn"><i class="fa fa-plus"></i></a></div>'+
											'</div>'+
										'</div>'+
									'</li>';
							
		var headerText = '<li class="list-group-item" data-type="header_text"><div class="q_box"><div class="q_actions"><div class="act_btn_grp"><a href="javascript:void(0)" class="act_btn trash removeQ"><i class="fa fa-trash"></i></a><a href="javascript:void(0)" class="act_btn up_dwn" onclick="copyInputType(6)"><i class="fa fa-copy"></i></a></div></div><div class="qb_title"><b>Header Text:</b></div><div class="qb_area"><div class="qb_input"><input type="text" class="q_field" placeholder="Type a title" value=""><input type="hidden" class="q_field_id" value=""></div></div></div></li>';
	
		Sortable.create(demo1, {
		  animation: 100,
		  group: 'list-1',
		  draggable: '.list-group-item',
		  handle: '.list-group-item',
		  sort: true,
		  filter: '.sortable-disabled',
		  chosenClass: 'active'
		});

			
		$(document).ready(function() {
			$(document).on("click", ".add_btn", function() {
				let last = $(this).parent().parent().find('.qb_child_input_wrapper:eq(0)').find(".qb_child_input:last-child");
				let optionHtml = last[0].outerHTML;
				let currentParent = $(this).parent().parent().find(".qb_child_input_wrapper:eq(0)");
				currentParent.append(optionHtml);
				$(this).parent().parent().find(".qb_child_input_wrapper:eq(0)").children('.qb_child_input:last-child').find('input').val('');
			});
		}); 

		/******Remove option*******/
		$(document).ready(function(){
			$(document).on("click", ".removeQ", function() {
				let row_id = $(this).closest('.list-group-item').attr('data-id');
				
				if (typeof row_id !== typeof undefined && row_id !== false) {
					let el = $(this).closest('.list-group-item');
					deleteQuestion(el,row_id);
					//$(this).closest('.list-group-item').remove();
					
				}else{
					$(this).closest('.list-group-item').remove();
				}
				
				if($('ul.list-group').find('.parent.list-group-item').length == 0){ 
					$("#saveBtn").prop('disabled',true);
				}
				
			});
		});
		
		/******Remove option*******/
		$(document).ready(function(){
			$(document).on("click", ".del_btn", function() {
				$(this).closest('.qb_child_input').remove();
			});
		});
		
		/***Insert child question**/
		$(document).ready(function(){
			$(document).on("click", ".create_child_input", function(){
				var flag = $(this).data("id");
				var inputType = (flag==1) ? inputText : ((flag==2) ? inputLongText : ((flag==3) ? inputYesNo : ((flag==4) ? inputSingleQuestion : ((flag==5)  ? inputMultiQuestion : headerText))));
				$(this).closest(".qb_child_input").append('<ul class="child" style="margin-top: 2%;">'+inputType+'</ul>');				
			});
		});
		
		function copyInputType(flag){			
			var inputType = (flag==1) ? inputText : ((flag==2) ? inputLongText : ((flag==3) ? inputYesNo : ((flag==4) ? inputSingleQuestion : ((flag==5)  ? inputMultiQuestion : headerText))));
			
			$("#demo1").append(inputType);
			
			let last = $("#demo1 li.list-group-item").last();
			last.addClass('parent');			
			last.find('.qb_child_input').addClass('parent_option');
			
			$("#saveBtn").prop('disabled',false);
		}
		
		function getChildren(data){
			var result = [];
			var elements = $(data).children('.child').children('li.list-group-item');
			
			if(elements && elements.length > 0){
				for(var x = 0; x < elements.length; x++){
					var options = $(elements[x]).find('.qb_area:eq(0)').find('.qb_child_input_wrapper:eq(0)').children('.qb_child_input');
					var data_type = $(elements[x]).attr('data-type');							
					//child_options
					var opArr = [];		
					
					if(options && options.length > 0){
						for(var y = 0; y < options.length; y++){							
							//check for children
							var is_cond = 0;
							var children = getChildren(options[y]); //recursive function call
							
							if(children && children.length > 0){ is_cond = 1;}							
							
							let val = $(options[y]).find('.input_box .q_field').val(); //option value
							let id = $(options[y]).find('.input_box .q_field_id').val(); //hidden value
							let data = {'o_label': val, 'op_id': id, 'is_cond': is_cond, 'child_array': children};
							opArr.push(data);
						}
					}
					let val = $(elements[x]).find('.qb_input .q_field').val(); //label value
					let id = $(elements[x]).find('.qb_input .q_field_id').val(); //hidden value
					let data = {'q_label':val, 'q_id': id, 'type': data_type, 'options' : opArr};
					result.push(data);				
				}
			}
			return result;
		}
		
		function getFormData(){
			var form_data = [];
			//main questions
			var elements = $('ul.list-group').find('.parent.list-group-item');
			
			if(elements && elements.length > 0){
				for(var i = 0; i < elements.length; i++){	
					//Check for options
					var opArr = [];
					var options = $(elements[i]).find('.qb_child_input.parent_option'); //option of main div
					var data_type = $(elements[i]).attr('data-type');
					
					if(options && options.length > 0){
						for(var j = 0; j < options.length; j++){
							var is_cond = 0;						
							//Check for children
							var children = getChildren(options[j]);
							
							if(children && children.length > 0){ is_cond = 1;}
					
							let val = $(options[j]).find('.input_box .q_field').val(); //option value
							let id = $(options[j]).find('.input_box .q_field_id').val(); //hidden value
							let data = {'o_label': val, 'op_id': id, 'is_cond': is_cond, 'child_array': children};
							opArr.push(data);
						}
					}
					
					let val = $(elements[i]).find('.qb_input .q_field').val(); //label value
					let id = $(elements[i]).find('.qb_input .q_field_id').val(); //hidden value
					let data = {'q_label':val, 'q_id': id, 'type': data_type, 'options': opArr};
					form_data.push(data);
				}
			}
			return form_data;
		}
		
		$("#VisitFieldForm").submit(function(e) {
			e.preventDefault();
			
			var formData = getFormData();
			var formid = $("#formid").val();
			
			$.ajax({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				data: {'formid':formid, 'visit_form_data':formData},
				url: "{{ route('admin.visit_form_questions.store') }}",
				type: "POST",
				// dataType: 'json',
				success: function (response) {
					
					//response = JSON.parse(res);
					if(response['status'] == 'success'){
						swal({
							title: response['message'],
							icon: 'success'
						});

						setTimeout(function(){
							swal.close();
							//$('#VisitFieldForm').trigger("reset");
							window.location = "{{ route('admin.visit_forms') }}";
						}, 500);
					}
				},
				error: function (data) {
					swal({
						title: 'Error Occured.',
						icon: 'error'
					})
					$('#saveBtn').html('Save');
				}
			});
		});
		
		function deleteQuestion(el,id) {
			swal({
				title: "Are you sure to delete this question?",
				text: "",
				icon: 'warning',
				buttons: {
				cancel: true,
				delete: {text:'Yes, Delete It',className:'btn_red'}				
				}
			}).then((isConfirm) => {
				if (!isConfirm) {
					return false;
				} else {
					$.ajax({
						headers: {
							'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
						},						
						type: "DELETE",
						url: "{{ env('APP_URL').'/admin/visit_forms/destroy_question' }}"+'/'+id,
						success: function (response) {

							if(response['status'] == 'success'){
								swal({
									title: response['message'],
									icon: 'success'
								});
								setTimeout(function(){
									swal.close();
									el.remove();
								}, 500);
							}
							//
						},
						error: function (data) {
							swal({
								title: 'Error Occured',
								icon: 'error'
							});
						}
					});
				}
			});
		}	

		function deleteOption(el,id) {
			swal({
				title: "Are you sure to delete this option?",
				text: "",
				icon: 'warning',
				buttons: {
				cancel: true,
				delete: {text:'Yes, Delete It',className:'btn_red'}
				}
			}).then((isConfirm) => {
				if (!isConfirm) {
					return false;
				} else {
					$.ajax({
						type: "DELETE",
						url: "{{ env('APP_URL').'/admin/visit_forms/destroy_option' }}"+'/'+id,
						success: function (response) {

							if(response['status'] == 'success'){
								swal({
									title: response['message'],
									icon: 'success'
								});
								setTimeout(function(){
									swal.close();
									el.remove();
								}, 500);
							}
							//
						},
						error: function (data) {
							console.log('Error:', data);
							swal({
								title: 'Error Occured',
								icon: 'error'
							});
						}
					});
				}
			});
		}		
	</script>
@endsection