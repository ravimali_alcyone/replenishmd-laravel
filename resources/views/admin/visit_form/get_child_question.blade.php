<ul class="child" style="margin-top: 2%;">			
	@if($child_questions && $child_questions->count() > 0)
		@foreach($child_questions as $key => $row)

			@if($row->question_type == 'header_text')
				<li class="list-group-item" data-type="{{$row->question_type}}" data-id="{{$row->id}}">
					<div class="q_box">
						<div class="q_actions">
							<div class="act_btn_grp">
								<a href="javascript:void(0)" class="act_btn trash removeQ"><i class="fa fa-trash"></i></a>
								<a href="javascript:void(0)" class="act_btn up_dwn" onclick="copyInputType(6)"><i class="fa fa-copy"></i></a>
							</div>
						</div>
						<div class="qb_title"><b>Header Text:</b></div>
						<div class="qb_area">
							<div class="qb_input">
								<input type="text" class="q_field" placeholder="Type a title" value="{{ $row->question_text}}">
								<input type="hidden" class="q_field_id" value="{{ $row->id}}">
							</div>
						</div>
					</div>
				</li>
			@endif
			
			@if($row->question_type == 'short_text')
				<li class="list-group-item" data-type="{{$row->question_type}}" data-id="{{$row->id}}">
					<div class="q_box">
						<div class="q_actions">
							<div class="act_btn_grp">
								<a href="javascript:void(0)" class="act_btn trash removeQ"><i class="fa fa-trash"></i></a>
								<a href="javascript:void(0)" class="act_btn up_dwn" onclick="copyInputType(1)"><i class="fa fa-copy"></i></a>
							</div>
						</div>
						<div class="qb_title"><b>Short Text Question:</b></div>
						<div class="qb_area">
							<div class="qb_input">
								<input type="text" class="q_field" placeholder="Type a title" value="{{ $row->question_text}}">
								<input type="hidden" class="q_field_id" value="{{ $row->id}}">
							</div>
						</div>
					</div>
				</li>			
			@endif

			@if($row->question_type == 'long_text')
				<li class="list-group-item" data-type="{{$row->question_type}}" data-id="{{$row->id}}">
					<div class="q_box">
						<div class="q_actions">
							<div class="act_btn_grp">
								<a href="javascript:void(0)" class="act_btn trash removeQ"><i class="fa fa-trash"></i></a>
								<a href="javascript:void(0)" class="act_btn up_dwn" onclick="copyInputType(2)"><i class="fa fa-copy"></i></a>
							</div>
						</div>
						<div class="qb_title"><b>Long Text Question:</b></div>
						<div class="qb_area">
							<div class="qb_input">
								<textarea class="qtxt_field q_field" rows="5" placeholder="">{{ $row->question_text}}</textarea>
								<input type="hidden" class="q_field_id" value="{{ $row->id}}">
							</div>
						</div>
					</div>
				</li>			
			@endif

			@if($row->question_type == 'yes_no')
				<li class="list-group-item" data-type="{{$row->question_type}}" data-id="{{$row->id}}">
					<div class="q_box">
						<div class="q_actions">
							<div class="act_btn_grp">
								<a href="javascript:void(0)" class="act_btn trash removeQ"><i class="fa fa-trash"></i></a>
								<a href="javascript:void(0)" class="act_btn up_dwn" onclick="copyInputType(3)"><i class="fa fa-copy"></i></a>
							</div>
						</div>
						<div class="qb_title"><b>Yes/No Question:</b></div>
						
						<div class="qb_area">
							<div class="qb_input">
								<input type="text" class="q_field" placeholder="Type a question" value="{{ $row->question_text}}">
								<input type="hidden" class="q_field_id" value="{{ $row->id }}">
							</div>							
							<div class="qb_child_input_wrapper">
						@php $optionsArray = App\Http\Controllers\admin\VisitFormController::getOptions($row->visit_form_id,$row->id); @endphp
						@if($optionsArray && $optionsArray->count() > 0)
							@php $x=0; @endphp
							@foreach($optionsArray as $op)								
								<div class="qb_child_input">
									<div class="input_box">
										<input type="text" class="q_field" placeholder="@if($x==0)Yes @else No @endif" value="{{$op->option_text}}">
										<input type="hidden" class="q_field_id" value="{{ $op->id }}">
									</div>
									<div class="check_box">
										<input type="checkbox" class="form-check-input check_cond_box" id="exampleCheck{{$op->id}}" @if($op->is_conditional == 1) checked @endif>
										<label class="form-check-label" for="exampleCheck{{$op->id}}">Is conditional?</label>
									</div>
									<div class="con_add_qus @if($op->is_conditional != 1) add_cond_ques @endif">
										<a href="javascript:void(0)" class="act_btn trash vf_add_btn_drp"><i class="fa fa-plus"></i></a>
									</div>
									
									@if($op->is_conditional == 1)
										@php $child_questions = App\Http\Controllers\admin\VisitFormController::get_child_question($row->visit_form_id,$op->id); @endphp
										
										@if($child_questions)
											{!! $child_questions !!}
										@endif
									@endif
									
								</div>
								@php $x++; @endphp
							@endforeach
						@endif
							</div>
						</div>	
					</div>
				</li>			
			@endif	

			@if($row->question_type == 'single')
				<li class="list-group-item" data-type="{{$row->question_type}}" data-id="{{$row->id}}">
					<div class="q_box">
						<div class="q_actions">
							<div class="act_btn_grp">
								<a href="javascript:void(0)" class="act_btn trash removeQ"><i class="fa fa-trash"></i></a>
								<a href="javascript:void(0)" class="act_btn up_dwn" onclick="copyInputType(4)"><i class="fa fa-copy"></i></a>
							</div>
						</div>
						<div class="qb_title"><b>Single Choice Question:</b></div>
						<div class="qb_area">
							<div class="qb_input">
								<input type="text" class="q_field" placeholder="Type a question" value="{{ $row->question_text }}">
								<input type="hidden" class="q_field_id" value="{{ $row->id }}">
							</div>
							<div class="qb_child_input_wrapper">
						@php $optionsArray = App\Http\Controllers\admin\VisitFormController::getOptions($row->visit_form_id,$row->id); @endphp
						@if($optionsArray && $optionsArray->count() > 0)
							@php $x=0; @endphp
							@foreach($optionsArray as $op)								
								<div class="qb_child_input">
									<div class="q_actions">
										<a href="javascript:void(0)" class="act_btn trash del_btn"><i class="fa fa-trash"></i></a>
									</div>
									<div class="input_box">
										<input type="text" class="q_field" placeholder="Type option here..." value="{{$op->option_text}}">
										<input type="hidden" class="q_field_id" value="{{ $op->id }}">
									</div>
									<div class="check_box">
										<input type="checkbox" class="form-check-input check_cond_box" id="exampleCheck{{$op->id}}" @if($op->is_conditional == 1) checked @endif>
										<label class="form-check-label" for="exampleCheck{{$op->id}}">Is conditional?</label>
									</div>
									<div class="con_add_qus @if($op->is_conditional != 1) add_cond_ques @endif">
										<a href="javascript:void(0)" class="act_btn trash vf_add_btn_drp"><i class="fa fa-plus"></i></a>
									</div>
									
									@if($op->is_conditional == 1)
										@php $child_questions = App\Http\Controllers\admin\VisitFormController::get_child_question($row->visit_form_id,$op->id); @endphp
										
										@if($child_questions)
											{!! $child_questions !!}
										@endif
									@endif
									
								</div>
								@php $x++; @endphp
							@endforeach
						@endif
							</div>
							<div class="add_new_ans"><a href="javascript:void(0)" class="add_btn"><i class="fa fa-plus"></i></a></div>
						</div>	
					</div>
				</li>			
			@endif	

			@if($row->question_type == 'multiple')
				<li class="list-group-item" data-type="{{$row->question_type}}" data-id="{{$row->id}}">
					<div class="q_box">
						<div class="q_actions">
							<div class="act_btn_grp">
								<a href="javascript:void(0)" class="act_btn trash removeQ"><i class="fa fa-trash"></i></a>
								<a href="javascript:void(0)" class="act_btn up_dwn" onclick="copyInputType(5)"><i class="fa fa-copy"></i></a>
							</div>
						</div>
						<div class="qb_title"><b>Multiple Choice Question:</b></div>
						<div class="qb_area">
							<div class="qb_input">
								<input type="text" class="q_field" placeholder="Type a question" value="{{ $row->question_text }}">
								<input type="hidden" class="q_field_id" value="{{ $row->id }}">
							</div>
							<div class="qb_child_input_wrapper">
						@php $optionsArray = App\Http\Controllers\admin\VisitFormController::getOptions($row->visit_form_id,$row->id); @endphp
						@if($optionsArray && $optionsArray->count() > 0)
							@php $x=0; @endphp
							@foreach($optionsArray as $op)								
								<div class="qb_child_input">
									<div class="q_actions">
										<a href="javascript:void(0)" class="act_btn trash del_btn"><i class="fa fa-trash"></i></a>
									</div>
									<div class="input_box">
										<input type="text" class="q_field" placeholder="Type option here..." value="{{$op->option_text}}">
										<input type="hidden" class="q_field_id" value="{{ $op->id }}">
									</div>
									<div class="check_box">
										<input type="checkbox" class="form-check-input check_cond_box" id="exampleCheck{{$op->id}}" @if($op->is_conditional == 1) checked @endif>
										<label class="form-check-label" for="exampleCheck{{$op->id}}">Is conditional?</label>
									</div>
									<div class="con_add_qus @if($op->is_conditional != 1) add_cond_ques @endif">
										<a href="javascript:void(0)" class="act_btn trash vf_add_btn_drp"><i class="fa fa-plus"></i></a>
									</div>
									
									@if($op->is_conditional == 1)
										@php $child_questions = App\Http\Controllers\admin\VisitFormController::get_child_question($row->visit_form_id,$op->id); @endphp
										
										@if($child_questions)
											{!! $child_questions !!}
										@endif
									@endif
									
								</div>
								@php $x++; @endphp
							@endforeach
						@endif									
							</div>
							<div class="add_new_ans"><a href="javascript:void(0)" class="add_btn"><i class="fa fa-plus"></i></a></div>
						</div>	
					</div>
				</li>			
			@endif
		@endforeach
	@endif
</ul>
