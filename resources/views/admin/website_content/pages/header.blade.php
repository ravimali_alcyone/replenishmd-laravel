@extends('layouts.app')
@section('content')
	<div class="intro-y box mt-5">
		<div class="flex flex-col sm:flex-row items-center p-5 border-b border-gray-200 dark:border-dark-5">
			<h2 class="font-medium text-base mr-auto">Header ({{ strtoupper($language) }})</h2>

			<ul class="lang_nav">
				<li>
					<a href="{{ URL('/admin/en/'.$page_name) }}" class="<?php if($language == 'en'){ echo 'active'; } ?>">English</a>
				</li>
				<!--li>
					<a href="{{ URL('/admin/fr/'.$page_name) }}" class="<?php //if($language == 'fr'){ echo 'active'; } ?>">Français</a>
				</li-->
				<li>
					<a href="{{ URL('/admin/nl/'.$page_name) }}" class="<?php if($language == 'nl'){ echo 'active'; } ?>">Nederlands</a>
				</li>
			</ul>
		</div>
		<div class="intro-y datatable-wrapper box p-5 col-span-12 overflow-auto lg:overflow-hidden">
			<div class="rounded-md flex items-center px-5 py-4 mb-2 bg-theme-14 text-theme-10 success_message">Data saved successfully!</div>
			<form id="headerForm">
				<div class="grid grid-cols-12 gap-6 mt-5 logo_wrapper">
					<div class="intro-y col-span-12 lg:col-span-12 mt-5">
						<div>
							<label>Language</label>
							<input type="text" name="language" id="language" class="input w-full border mt-2" value="{{ isset($section_content->language) ? $section_content->language : '' }}">
						</div>
					</div>				
					<div class="intro-y col-span-12 lg:col-span-12">
						<div>
							<label>Logo</label>
							<input type="file" name="image_1" id="logo" class="input w-full border mt-2 background_img" value="{{ !empty($section_content->logo->image_1) ? $section_content->logo->image_1 : '' }}">
							<input type="hidden" name="existing_image[image_1]" value="{{ !empty($section_content->logo->image_1) ? $section_content->logo->image_1 : '' }}">
							<input type="hidden" name="old_image[]" value="{{ !empty($section_content->logo->image_1) ? $section_content->logo->image_1 : '' }}">
						</div>
						<div class="images_preview">
							@if(!empty($section_content->logo->image_1))
								<span class="pip">
									<img class="imageThumb" src="/{{ $section_content->logo->image_1 }}">
								</span>
							@endif
						</div>
					</div>
				</div>

				<div class="intro-y col-span-12 lg:col-span-12 mt-5">
					<h2 class="text-lg font-medium mr-auto">Menu's</h2>
					<button type="button" class="button button--sm w-24 mr-1 mb-2 bg-theme-1 text-white float-right add_new_menu_btn">Add More</button>
				</div>

				@if(empty($section_content->menus))
					<div class="menu_section mt-3">
						<div class="intro-y col-span-12 lg:col-span-6">
							<div>
								<label>Menu Name</label>
								<input type="text" name="menu_names[]" value="" class="input w-full border mt-2" placeholder="Menu Name">
							</div>
						</div>
						<div class="intro-y col-span-12 lg:col-span-6 mt-5">
							<div>
								<label>Menu Link</label>
								<input type="text" name="menu_links[]" value="" class="input w-full border mt-2" placeholder="Menu Link">
							</div>
						</div>
					</div>
				@endif
				<div id="menus_wrapper">
					@if(!empty($section_content->menus))
						@foreach($section_content->menus as $key => $value)
							<div class="menu_section mt-3">
								<div class="text-right"><a href="javascript:;" class="remove_section_btn">X</a></div>
								<div class="intro-y col-span-12 lg:col-span-6">
									<div>
										<label>Menu Name</label>
										<input type="text" name="menu_names[]" value="{{ $value->name }}" class="input w-full border mt-2" placeholder="Menu Name">
									</div>
								</div>
								<div class="intro-y col-span-12 lg:col-span-6 mt-5">
									<div>
										<label>Menu Link</label>
										<input type="text" name="menu_links[]" value="{{ $value->link }}" class="input w-full border mt-2" placeholder="Menu Link">
									</div>
								</div>
							</div>
						@endforeach
					@endif
				</div>

				<div class="intro-y col-span-12 lg:col-span-12 mt-5">
					<h2 class="text-lg font-medium mr-auto">Sub Menu's</h2>
					<button type="button" class="button button--sm w-24 mr-1 mb-2 bg-theme-1 text-white float-right add_new_sub_menu_btn">Add More</button>
				</div>
				@if(empty($section_content->sub_menus))
					<div class="menu_section mt-3">
						<div class="intro-y col-span-12 lg:col-span-6">
							<div>
								<label>Parent Menu Name</label>
								<input type="text" name="parent_menu_names[]" value="" class="input w-full border mt-2" placeholder="Menu Name">
							</div>
						</div>
						<div class="intro-y col-span-12 lg:col-span-6 mt-5">
							<div>
								<label>Sub Menu Name</label>
								<input type="text" name="sub_menu_names[]" value="" class="input w-full border mt-2" placeholder="Menu Name">
							</div>
						</div>
						<div class="intro-y col-span-12 lg:col-span-6 mt-5">
							<div>
								<label>Sub Menu Link</label>
								<input type="text" name="sub_menu_links[]" value="" class="input w-full border mt-2" placeholder="Menu Link">
							</div>
						</div>
					</div>
				@endif
				<div id="sub_menus_wrapper">
					@if(!empty($section_content->sub_menus))
						@foreach($section_content->sub_menus as $key => $value)
							<div class="sub_menu_section mt-3">
								<div class="text-right"><a href="javascript:;" class="remove_sub_menu_section_btn">X</a></div>
								<div class="intro-y col-span-12 lg:col-span-6">
									<div>
										<label>Parent Menu Name</label>
										<input type="text" name="parent_menu_names[]" value="{{ $value->parent_name }}" class="input w-full border mt-2" placeholder="Menu Name">
									</div>
								</div>
								<div class="intro-y col-span-12 lg:col-span-6 mt-5">
									<div>
										<label>Sub Menu Name</label>
										<input type="text" name="sub_menu_names[]" value="{{ $value->sub_menu_name }}" class="input w-full border mt-2" placeholder="Menu Name">
									</div>
								</div>
								<div class="intro-y col-span-12 lg:col-span-6 mt-5">
									<div>
										<label>Sub Menu Link</label>
										<input type="text" name="sub_menu_links[]" value="{{ $value->sub_menu_link }}" class="input w-full border mt-2" placeholder="Menu Link">
									</div>
								</div>
							</div>
						@endforeach
					@endif
				</div>

				<div class="grid grid-cols-12 gap-6 mt-12">
					<div class="intro-y col-span-12 lg:col-span-12 pt-5 border-t border-gray-200 dark:border-dark-5">
						<input type="hidden" name="lang" class="lang" value="{{ $language }}">
						<input type="hidden" name="sectionName" value="header" class="sectionName">
						<input type="hidden" name="pageName" value="{{ $page_name }}" class="pageName">
						<button type="submit" id="saveBtn" class="button button--md w-24 mr-1 mb-2 bg-theme-9 text-white">Save</button>
						<button type="reset" class="button button--md w-24 mr-1 mb-2 bg-theme-6 text-white">Cancel</button>
					</div>
				</div>
			</form>
		</div>
	</div>

	<script type="text/javascript">
		$(document).ready(function() {
			// Image Reader
			if (window.File && window.FileList && window.FileReader) {
				$("#logo").on("change", function(e) {
					$('.pip').remove();
					var files = e.target.files,
					filesLength = files.length;
					for (var i = 0; i < filesLength; i++) {
						var f = files[i]
						var fileReader = new FileReader();
						fileReader.onload = (function(e) {
							var file = e.target;
							let html = "<span class=\"pip\">" +
							"<img class=\"imageThumb\" src=\"" + e.target.result + "\" title=\"" + file.name + "\"/>" +
							"</span>";

							$(".images_preview").append(html);
							$(".remove").click(function(){
								$(this).parent(".pip").remove();
							});
						});
						fileReader.readAsDataURL(f);
					}
				});
			} else {
				alert("Your browser doesn't support to File API")
			}

			// Add new menu
			$(".add_new_menu_btn").click(function() {
				let html = '<div class="menu_section mt-3">'+
								'<div class="text-right"><a href="javascript:;" class="remove_section_btn">X</a></div>'+
								'<div class="intro-y col-span-12 lg:col-span-6">'+
									'<div>'+
										'<label>Menu Name</label>'+
										'<input type="text" name="menu_names[]" value="" class="input w-full border mt-2" placeholder="Menu Name">'+
									'</div>'+
								'</div>'+
								'<div class="intro-y col-span-12 lg:col-span-6 mt-5">'+
									'<div>'+
										'<label>Menu Link</label>'+
										'<input type="text" name="menu_links[]" value="" class="input w-full border mt-2" placeholder="Menu Link">'+
									'</div>'+
								'</div>'+
							'</div>';
				$("#menus_wrapper").append(html);
			});

			// Remove menu section
			$(document).on("click", ".remove_section_btn", function() {
				if (confirm('Are you sure?')) {
					$(this).closest('.menu_section').remove();
				}
			});

			// Add new sub-menu
			$(".add_new_sub_menu_btn").click(function() {
				let html = '<div class="sub_menu_section mt-3">'+
								'<div class="text-right"><a href="javascript:;" class="remove_sub_menu_section_btn">X</a></div>'+
								'<div class="intro-y col-span-12 lg:col-span-6">'+
									'<div>'+
										'<label>Parent Menu Name</label>'+
										'<input type="text" name="parent_menu_names[]" value="" class="input w-full border mt-2" placeholder="Menu Name">'+
									'</div>'+
								'</div>'+
								'<div class="intro-y col-span-12 lg:col-span-6 mt-5">'+
									'<div>'+
										'<label>Sub Menu Name</label>'+
										'<input type="text" name="sub_menu_names[]" value="" class="input w-full border mt-2" placeholder="Menu Name">'+
									'</div>'+
								'</div>'+
								'<div class="intro-y col-span-12 lg:col-span-6 mt-5">'+
									'<div>'+
										'<label>Sub Menu Link</label>'+
										'<input type="text" name="sub_menu_links[]" value="" class="input w-full border mt-2" placeholder="Menu Link">'+
									'</div>'+
								'</div>'+
							'</div>';
				$("#sub_menus_wrapper").append(html);
			});

			// Remove sub-menu section
			$(document).on("click", ".remove_sub_menu_section_btn", function() {
				if (confirm('Are you sure?')) {
					$(this).closest('.sub_menu_section').remove();
				}
			});

			// Submission process
			$('#headerForm').submit(function(e) {
				e.preventDefault();
				var menu_names = $('[name="menu_names[]"]').map(function(){return $(this).val();}).get();
				var menu_links = $('[name="menu_links[]"]').map(function(){return $(this).val();}).get();
				var parent_menu_names = $('[name="parent_menu_names[]"]').map(function(){return $(this).val();}).get();
				var sub_menu_names = $('[name="sub_menu_names[]"]').map(function(){return $(this).val();}).get();
				var sub_menu_links = $('[name="sub_menu_links[]"]').map(function(){return $(this).val();}).get();

				var multiple_menu = [];
				$.each(menu_names, function(key,value){
				    var menu = { name : menu_names[key], link : menu_links[key] };
				    multiple_menu.push(menu);
				});

				var sub_menus = [];
				$.each(parent_menu_names, function(key,value){
				    var sub_menu = {
						parent_name: parent_menu_names[key],
						sub_menu_name: sub_menu_names[key],
						sub_menu_link: sub_menu_links[key]
					};
				    sub_menus.push(sub_menu);
				});

				$.ajax({
					url: "{{ url('/admin/FilesUpload') }}",
					headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					},
					type: 'POST',
					data: new FormData(this),
					contentType: false,
					processData:false,
					success: function(result) {
					var logo = result;
					var content = { language: $("#language").val(), logo: logo, menus: multiple_menu, sub_menus: sub_menus };
						$.ajax({
							url: "{{ url('admin/insertAndUpdateSection') }}",
							headers: {
								'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
							},
							type: 'POST',
							datatype: 'json',
							data: {
								_token: $('input[name=_token]').val(),
								section_name: $('.sectionName').val(),
								page_name: $('.pageName').val(),
								section_content : content,
								language: $('.lang').val(),
							},
							success: function(result) {
								if(result.msg == true) {
									$('html, body').animate({ scrollTop: 0 }, 'slow');
									$(".success_message").slideDown();
									$(".success_message").delay(3000).slideUp();
								}
							}
						});
					}
				});
			});
		});
	</script>
@endsection