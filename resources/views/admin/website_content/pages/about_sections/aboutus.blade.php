@extends('layouts.app')
@section('content')
@php
	$i=1;
	@endphp
	<div class="intro-y box mt-5">
		<div class="flex flex-col sm:flex-row items-center p-5 border-b border-gray-200 dark:border-dark-5">
			<h2 class="font-medium text-base mr-auto">About > AboutUs ({{ strtoupper($language) }})</h2>

			<ul class="lang_nav">
				<li>
					<a href="{{ URL('/admin/en/'.$page_name.'/banner') }}" class="<?php if($language == 'en'){ echo 'active'; } ?>">English</a>
				</li>
				<!-- <li>
					<a href="{{ URL('/admin/fr/'.$page_name) }}" class="<?php //if($language == 'fr'){ echo 'active'; } ?>">Français</a>
				</li> -->
				<li>
					<a href="{{ URL('/admin/nl/'.$page_name.'/banner') }}" class="<?php if($language == 'nl'){ echo 'active'; } ?>">Nederlands</a>
				</li>
			</ul>
		</div>
		<div class="intro-y datatable-wrapper box p-5 col-span-12 overflow-auto lg:overflow-hidden">
			<div class="rounded-md flex items-center px-5 py-4 mb-2 bg-theme-14 text-theme-10 success_message">Data saved successfully!</div>
			<form id="bannerForm">
				<div class="intro-y col-span-12 lg:col-span-12 mt-5">
					<h2 class="text-lg font-medium mr-auto">AboutUs Section</h2>
                </div>	
                <div class="intro-y col-span-12 lg:col-span-6">
                    <div>
                        <label>Heading</label>
                        <input type="text" name="heading" value="{{ isset($section_content->heading) ? $section_content->heading : '' }}" class="input w-full border mt-2" placeholder="Heading">
                    </div>
                </div>
                <div class="intro-y col-span-12 lg:col-span-6 mt-5">
                    <div>
                        <label>Description</label>
                        <textarea name="description" id="description" cols="30" rows="4" class="input w-full border mt-2" placeholder="Description">{{ isset($section_content->description) ? $section_content->description : '' }}</textarea>
                    </div>
                </div>

                @if(empty($section_content->aboutus))
                    <div class="menu_section mt-3">
                        <h2 class="text-lg font-medium mr-auto">List Items</h2>
                        <div class="intro-y col-span-12 lg:col-span-6 mt-5">
                            <div>
                                <button type="button" class="button button--sm w-24 mr-1 mb-2 bg-theme-1 text-white float-right add_new_menu_btn add_new_slide_btn">Add More</button> 
                
                                <label>List Item</label>
                                <input type="text" name="listitems[]" value="" class="input w-full border mt-2" placeholder="listitems">
                            </div>
                        </div>
                        
                    </div>
				@endif

                <div class="menu_section mt-3">
                    <div id="slides_wrapper">
                        @if(!empty($section_content->listdata))
                            @foreach($section_content->listdata as $key => $value)
                            <div class="menu_section mt-3">
                                <div class="text-right"><a href="javascript:;" class="remove_section_btn">X</a></div>
                                <div class="intro-y col-span-12 lg:col-span-6 mt-5">
                                    <div>
                                        <label>List Item</label>
                                        <input type="text" name="listitems[]" value="{{ $value->listitems }}" class="input w-full border mt-2" placeholder="Description">
                                    </div>
                                </div>
                            </div>
                            @php
                            $i++;
                            @endphp							
                            @endforeach
                        @endif
                    </div>
                </div>

                <div class="intro-y col-span-12 lg:col-span-6 mt-5">
                    <div>
                        <label>Button Name</label>
                        <input type="text" name="link_names" value="Get Started Now!" class="input aboutus_button w-full border mt-2" placeholder="Link Name">
                    </div>
                </div>
                <div class="intro-y col-span-12 lg:col-span-6 mt-5">
                    <div>
                        <label>Button Link</label>
                        <input type="text" name="links" value="#" class="input aboutus_button_link w-full border mt-2" placeholder="Link">
                    </div>
                </div>

				<div class="grid grid-cols-12 gap-6 mt-12">
					<div class="intro-y col-span-12 lg:col-span-12 pt-5 border-t border-gray-200 dark:border-dark-5">
						<input type="hidden" name="lang" class="lang" value="{{ $language }}">
						<input type="hidden" name="sectionName" value="aboutus" class="sectionName">
						<input type="hidden" name="pageName" value="{{ $page_name }}" class="pageName">
						<button type="submit" id="saveBtn" class="button button--md w-24 mr-1 mb-2 bg-theme-9 text-white">Save</button>
						<button type="reset" class="button button--md w-24 mr-1 mb-2 bg-theme-6 text-white" onclick="location.href = '{{ env('APP_URL') }}admin/{{$language}}/{{$page_name}}';">Cancel</button>
					</div>
				</div>
			</form>
		</div>
	</div>

	<script type="text/javascript">
		$(document).ready(function() {
			
			// Add new menu
			$(".add_new_slide_btn").click(function() {
                var x = <?php echo $i;?>; 
                var rand = x++;
                let html = '<div class="menu_section mt-3">'+
                                '<div class="text-right"><a href="javascript:;" class="remove_section_btn">X</a></div>'+
                                '<div class="intro-y col-span-12 lg:col-span-6 mt-5">'+
                                    '<div>'+
                                        '<label>List Item</label>'+
                                        '<input type="text" name="listitems[]" value="" class="input w-full border mt-2" placeholder="listitems">'+
                                    '</div>'+
                                '</div>'+
                            '</div>';
				$("#slides_wrapper").append(html);
			});

			// Remove menu section
			$(document).on("click", ".remove_section_btn", function() {
				if (confirm('Are you sure?')) {
					$(this).closest('.menu_section').remove();
				}
			});

            $(".success_message").slideUp();
			// Submission process
			$('#bannerForm').submit(function(e) {
				e.preventDefault();
                var listitems = $('[name="listitems[]"]').map(function(){return $(this).val();}).get();
                var listdata = [];

                $.ajax({
                    url: "{{ url('/admin/FilesUpload') }}",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type: 'POST',
                    data: new FormData(this),
                    contentType: false,
                    processData:false,
                    success: function(result) {
                        $.each(listitems, function(key,value){
                            var list_item = {
                                listitems : listitems[key]
                            };
                            listdata.push(list_item);
                        });
                        
                        var content = {
                            heading: $("input[name='heading']").val(),
                            description: $("#description").val(),
                            listdata: listdata,
                            button_text: $(".aboutus_button").val(),
                            button_url: $(".aboutus_button_link").val(),
                        };

                        $.ajax({
                            url: "{{ url('admin/insertAndUpdateSection') }}",
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            type: 'POST',
                            datatype: 'json',
                            data: {
                                _token: $('input[name=_token]').val(),
                                section_name: $('.sectionName').val(),
                                page_name: $('.pageName').val(),
                                section_content : content,
                                language: $('.lang').val(),
                            },
                            success: function(result) {
                                if(result.msg == true) {
                                    $('html, body').animate({ scrollTop: 0 }, 'slow');
                                    $(".success_message").slideDown();
                                    $(".success_message").delay(3000).slideUp();
                                }
                            }
                        });
                    }
                });
			});
		});
	</script>
@endsection