@extends('layouts.app')
@section('content')
	<div class="intro-y box mt-5">
		<div class="flex flex-col sm:flex-row items-center p-5 border-b border-gray-200 dark:border-dark-5">
			<h2 class="font-medium text-base mr-auto">Plans > Banner ({{ strtoupper($language) }})</h2>

			<ul class="lang_nav">
				<li>
					<a href="{{ URL('/admin/en/'.$page_name.'/banner') }}" class="<?php if($language == 'en'){ echo 'active'; } ?>">English</a>
				</li>
				<!-- <li>
					<a href="{{ URL('/admin/fr/'.$page_name) }}" class="<?php //if($language == 'fr'){ echo 'active'; } ?>">Français</a>
				</li> -->
				<li>
					<a href="{{ URL('/admin/nl/'.$page_name.'/banner') }}" class="<?php if($language == 'nl'){ echo 'active'; } ?>">Nederlands</a>
				</li>
			</ul>
		</div>
		<div class="intro-y datatable-wrapper box p-5 col-span-12 overflow-auto lg:overflow-hidden">
			<div class="rounded-md flex items-center px-5 py-4 mb-2 bg-theme-14 text-theme-10 success_message">Data saved successfully!</div>
			<form id="bannerForm">
				<div class="intro-y col-span-12 lg:col-span-12 mt-5">
					<h2 class="text-lg font-medium mr-auto">Banner Section</h2>
				</div>

                <div class="menu_section mt-3">
                    <div class="intro-y col-span-12 lg:col-span-6">
                        <div>
                            <label>Heading</label>
                            <input type="text" name="heading" value="{{ isset($section_content->heading) ? $section_content->heading : '' }}" class="input w-full border mt-2" placeholder="Heading">
                        </div>
                    </div>
                    <div class="intro-y col-span-12 lg:col-span-6 mt-5">
                        <div>
                            <label>Description</label>
                            <textarea name="description" id="description" cols="30" rows="4" class="input w-full border mt-2" placeholder="Description">{{ isset($section_content->description) ? $section_content->description : '' }}</textarea>
                        </div>
                    </div>
                    
                </div>

				<div class="grid grid-cols-12 gap-6 mt-12">
					<div class="intro-y col-span-12 lg:col-span-12 pt-5 border-t border-gray-200 dark:border-dark-5">
						<input type="hidden" name="lang" class="lang" value="{{ $language }}">
						<input type="hidden" name="sectionName" value="banner" class="sectionName">
						<input type="hidden" name="pageName" value="{{ $page_name }}" class="pageName">
						<button type="submit" id="saveBtn" class="button button--md w-24 mr-1 mb-2 bg-theme-9 text-white">Save</button>
						<button type="reset" class="button button--md w-24 mr-1 mb-2 bg-theme-6 text-white" onclick="location.href = '{{ env('APP_URL') }}admin/{{$language}}/{{$page_name}}';">Cancel</button>
					</div>
				</div>
			</form>
		</div>
	</div>

	<script type="text/javascript">
		$(document).ready(function() {
			

            $(".success_message").slideUp();

			// Submission process
			$('#bannerForm').submit(function(e) {
				e.preventDefault();

                
                        var content = {
                            heading: $("input[name='heading']").val(),
                            description: $("#description").val()
                        };

                        $.ajax({
                            url: "{{ url('admin/insertAndUpdateSection') }}",
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            type: 'POST',
                            datatype: 'json',
                            data: {
                                _token: $('input[name=_token]').val(),
                                section_name: $('.sectionName').val(),
                                page_name: $('.pageName').val(),
                                section_content : content,
                                language: $('.lang').val(),
                            },
                            success: function(result) {
                                if(result.msg == true) {
                                    $('html, body').animate({ scrollTop: 0 }, 'slow');
                                    $(".success_message").slideDown();
                                    $(".success_message").delay(3000).slideUp();
                                }
                            }
                        });
			});
		});
	</script>
@endsection