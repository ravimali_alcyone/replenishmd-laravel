@extends('layouts.app')
@section('content')
@php
	$i=1;
	@endphp
	<div class="intro-y box mt-5">
		<div class="flex flex-col sm:flex-row items-center p-5 border-b border-gray-200 dark:border-dark-5">
			<h2 class="font-medium text-base mr-auto">How It Work > JoinUs ({{ strtoupper($language) }})</h2>

			<ul class="lang_nav">
				<li>
					<a href="{{ URL('/admin/en/'.$page_name.'/banner') }}" class="<?php if($language == 'en'){ echo 'active'; } ?>">English</a>
				</li>
				<!-- <li>
					<a href="{{ URL('/admin/fr/'.$page_name) }}" class="<?php //if($language == 'fr'){ echo 'active'; } ?>">Français</a>
				</li> -->
				<li>
					<a href="{{ URL('/admin/nl/'.$page_name.'/banner') }}" class="<?php if($language == 'nl'){ echo 'active'; } ?>">Nederlands</a>
				</li>
			</ul>
		</div>
		<div class="intro-y datatable-wrapper box p-5 col-span-12 overflow-auto lg:overflow-hidden">
			<div class="rounded-md flex items-center px-5 py-4 mb-2 bg-theme-14 text-theme-10 success_message">Data saved successfully!</div>
			<form id="ourwork">
				
                    <div class="menu_section mt-3">
                        <div class="intro-y col-span-12 lg:col-span-6">
                            <div>
                                <label>Main Heading</label>
                                <input type="text" name="top_main_name" value="{{ isset($section_content->top_main_name) ? $section_content->top_main_name : '' }}" class="input w-full border mt-2" placeholder="">
                            </div>
                        </div>
                        <div class="intro-y col-span-12 lg:col-span-6 mt-5">
                            <div>
                                <label>Content</label>
                                <input type="text" name="top_main_content" value="{{ isset($section_content->top_main_content) ? $section_content->top_main_content : '' }}" class="input w-full border mt-2" placeholder="">
                            </div>
                        </div>						
                    </div>

                    <div class="intro-y datatable-wrapper box p-5 mt-3 col-span-6">
                        <div class="menu_section mt-3">
                            <div class="intro-y col-span-12 lg:col-span-6">
                            <h2 class="text-lg font-medium mr-auto">Patient Register Card</h2>
                                <div>
                                    <label>Heading</label>
                                    <input type="text" name="heading_1" value="{{ isset($section_content->heading_1) ? $section_content->heading_1 : '' }}" class="input w-full border mt-2" placeholder="">
                                </div>
                            </div>
                            <div class="intro-y col-span-12 lg:col-span-6 mt-5">
                                <div>
                                    <label>Content</label>
                                    <input type="text" name="content_1" value="{{ isset($section_content->content_1) ? $section_content->content_1 : '' }}" class="input w-full border mt-2" placeholder="">
                                </div>
                            </div>
                            <div class="intro-y col-span-12 lg:col-span-6 mt-5">
                                <div>
                                    <label>Botton-Text</label>
                                    <input type="text" name="link_1_txt" value="{{ isset($section_content->link_1_txt) ? $section_content->link_1_txt : '' }}" class="input w-full border mt-2" placeholder="">
                                </div>
                            </div>	
                            <div class="intro-y col-span-12 lg:col-span-6 mt-5">
                                <div>
                                    <label>Botton-Url</label>
                                    <input type="text" name="link_1_url" value="{{ isset($section_content->link_1_url) ? $section_content->link_1_url : '' }}" class="input w-full border mt-2" placeholder="">
                                </div>
                            </div>
                            <div class="intro-y col-span-12 lg:col-span-6 mt-5">
                                <label>Logo Image</label>
                                <input type="file" name="image_1" id="logo" class="slider_images input w-full border mt-2 background_img" value="{{ !empty($section_content->image->image_1) ? $section_content->image->image_1 : '' }}">
                                <input type="hidden" name="existing_image[image_1]" value="{{ !empty($section_content->image->image_1) ? $section_content->image->image_1 : '' }}">
                                <input type="hidden" name="old_image[]" value="{{ !empty($section_content->image->image_1) ? $section_content->image->image_1 : '' }}">
                            </div>
                            <div class="images_preview mb-4">
                                @if(isset($section_content->image->image_1))
                                    <img src="/{{ $section_content->image->image_1 }}" alt="">
                                @endif
                            </div>						
                        </div>
                    </div>

                    <div class="intro-y datatable-wrapper box mt-3 p-5 col-span-6">
                        <div class="menu_section mt-3">
                            <div class="intro-y col-span-12 lg:col-span-6">
                            <h2 class="text-lg font-medium mr-auto">Provider Register Card</h2>
                                <div>
                                    <label>Heading</label>
                                    <input type="text" name="heading_2" value="{{ isset($section_content->heading_2) ? $section_content->heading_2 : '' }}" class="input w-full border mt-2" placeholder="">
                                </div>
                            </div>
                            <div class="intro-y col-span-12 lg:col-span-6 mt-5">
                                <div>
                                    <label>Content</label>
                                    <input type="text" name="content_2" value="{{ isset($section_content->content_2) ? $section_content->content_2 : '' }}" class="input w-full border mt-2" placeholder="">
                                </div>
                            </div>
                            <div class="intro-y col-span-12 lg:col-span-6 mt-5">
                                <div>
                                    <label>Botton-Text</label>
                                    <input type="text" name="link_2_txt" value="{{ isset($section_content->link_2_txt) ? $section_content->link_2_txt : '' }}" class="input w-full border mt-2" placeholder="">
                                </div>
                            </div>	
                            <div class="intro-y col-span-12 lg:col-span-6 mt-5">
                                <div>
                                    <label>Botton-Url</label>
                                    <input type="text" name="link_2_url" value="{{ isset($section_content->link_2_url) ? $section_content->link_2_url : '' }}" class="input w-full border mt-2" placeholder="">
                                </div>
                            </div>
                            <div class="intro-y col-span-12 lg:col-span-6 mt-5">
                                <label>Logo Image</label>
                                <input type="file" name="image_1_1" id="logo" class="slider_images input w-full border mt-2 background_img" value="{{ !empty($section_content->image->image_1_1) ? $section_content->image->image_1_1 : '' }}">
                                <input type="hidden" name="existing_image[image_1_1]" value="{{ !empty($section_content->image->image_1_1) ? $section_content->image->image_1_1 : '' }}">
                                <input type="hidden" name="old_image[]" value="{{ !empty($section_content->image->image_1_1) ? $section_content->image->image_1_1 : '' }}">
                            </div>
                            <div class="images_preview mb-4">
                                @if(isset($section_content->image->image_1_1))
                                    <img src="/{{ $section_content->image->image_1_1 }}" alt="">
                                @endif
                            </div>							
                        </div>
                    </div>

                <div class="grid grid-cols-12 gap-6 mt-12">
					<div class="intro-y col-span-12 lg:col-span-12 pt-5 border-t border-gray-200 dark:border-dark-5 form_btn_wrapper">
						<input type="hidden" name="sectionName" value="joinus" class="sectionName">
						<input type="hidden" name="pageName" value="{{ $page_name }}" class="pageName">
						<button type="submit" id="saveBtn" class="button button--md w-24 mr-1 mb-2 bg-theme-9 text-white">Save</button>
						<button type="reset" class="button button--md w-24 mr-1 mb-2 bg-theme-6 text-white" onclick="location.href = '{{ env('APP_URL') }}/admin/content/{{$page_name}}';">Cancel</button>
					</div>
				</div>
			</form>
		</div>
	</div>

	<script type="text/javascript">
		$(document).ready(function() {
			// Image Reader
			if (window.File && window.FileList && window.FileReader) {
				$(".slider_images").on("change", function(e) {
					$('.pip').remove();
					var files = e.target.files,
					filesLength = files.length;
					for (var i = 0; i < filesLength; i++) {
						var f = files[i]
						var fileReader = new FileReader();
						fileReader.onload = (function(e) {
							var file = e.target;
							let html = "<span class=\"pip\">" +
							"<img class=\"imageThumb\" src=\"" + e.target.result + "\" title=\"" + file.name + "\"/>" +
							"</span>";

							$(".images_preview").append(html);
							$(".remove").click(function(){
								$(this).parent(".pip").remove();
							});
						});
						fileReader.readAsDataURL(f);
					}
				});
			} else {
				alert("Your browser doesn't support to File API")
			}
            $(".success_message").slideUp();
            // Submission process
			$('#ourwork').submit(function(e) {
				e.preventDefault();
                var top_main_name = $('[name="top_main_name"]').val();
                var top_main_content = $('[name="top_main_content"]').val();
				var heading_1 = $('[name="heading_1"]').val();
				var content_1 = $('[name="content_1"]').val();
                var link_1_txt = $('[name="link_1_txt"]').val();
                var link_1_url = $('[name="link_1_url"]').val();
				var heading_2 = $('[name="heading_2"]').val();
				var content_2 = $('[name="content_2"]').val();
                var link_2_txt = $('[name="link_2_txt"]').val();
                var link_2_url = $('[name="link_2_url"]').val();

                $.ajax({
                    url: "{{ url('/admin/FilesUpload') }}",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type: 'POST',
                    data: new FormData(this),
                    contentType: false,
                    processData:false,
                    success: function(result) {
                       
                        var content = {
                            top_main_name: top_main_name,
                            top_main_content: top_main_content,
                            heading_1: heading_1, 
                            content_1: content_1,
                            link_1_txt: link_1_txt,
                            link_1_url: link_1_url,
                            heading_2: heading_2,
                            content_2: content_2,
                            link_2_txt: link_2_txt,
                            link_2_url: link_2_url,
                            image: result
                        };

                        $.ajax({
                            url: "{{ url('admin/insertAndUpdateSection') }}",
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            type: 'POST',
                            datatype: 'json',
                            data: {
                                _token: $('input[name=_token]').val(),
                                section_name: $('.sectionName').val(),
                                page_name: $('.pageName').val(),
                                section_content : content,
                                language: $('.lang').val(),
                            },
                            success: function(result) {
                                if(result.msg == true) {
                                    $('html, body').animate({ scrollTop: 0 }, 'slow');
                                    $(".success_message").slideDown();
                                    $(".success_message").delay(3000).slideUp();
                                }
                            }
                        });
                    }
                });
			});
		});
	</script>
@endsection