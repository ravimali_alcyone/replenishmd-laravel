@extends('layouts.app')
@section('content')
	<div class="intro-y box mt-5">
		<div class="flex flex-col sm:flex-row items-center p-5 border-b border-gray-200 dark:border-dark-5">
			<h2 class="font-medium text-base mr-auto">Header ({{ strtoupper($language) }})</h2>

			<ul class="lang_nav">
				<li>
					<a href="{{ URL('/admin/en/'.$page_name) }}" class="<?php if($language == 'en'){ echo 'active'; } ?>">English</a>
				</li>
				<!--li>
					<a href="{{ URL('/admin/fr/'.$page_name) }}" class="<?php //if($language == 'fr'){ echo 'active'; } ?>">Français</a>
				</li-->
				<li>
					<a href="{{ URL('/admin/nl/'.$page_name) }}" class="<?php if($language == 'nl'){ echo 'active'; } ?>">Nederlands</a>
				</li>
			</ul>
		</div>
		<div class="intro-y datatable-wrapper box p-5 col-span-12 overflow-auto lg:overflow-hidden">
			<div class="rounded-md flex items-center px-5 py-4 mb-2 bg-theme-14 text-theme-10 success_message">Data saved successfully!</div>
			<form id="headerForm">
				<div class="grid grid-cols-12 gap-6 mt-5 logo_wrapper">
					<div class="intro-y col-span-12 lg:col-span-12">
						<div>
							<label>Logo</label>
							<input type="file" name="image_1" id="logo" class="input w-full border mt-2 background_img" value="{{ !empty($section_content->logo->image_1) ? $section_content->logo->image_1 : '' }}">
							<input type="hidden" name="existing_image[image_1]" value="{{ !empty($section_content->logo->image_1) ? $section_content->logo->image_1 : '' }}">
							<input type="hidden" name="old_image[]" value="{{ !empty($section_content->logo->image_1) ? $section_content->logo->image_1 : '' }}">
						</div>
						<div class="images_preview">
							@if(!empty($section_content->logo->image_1))
								<span class="pip">
									<img class="imageThumb" src="/{{ $section_content->logo->image_1 }}">
								</span>
							@endif
						</div>
					</div>
				</div>

				<div class="intro-y col-span-12 lg:col-span-12 mt-5">
					<h2 class="text-lg font-medium mr-auto">Links</h2>
					<!-- <button type="button" class="button button--sm w-24 mr-1 mb-2 bg-theme-1 text-white float-right add_new_menu_btn">Add More</button> -->
				</div>
                <div class="menu_section mt-3">
                    <div class="intro-y col-span-12 lg:col-span-6">
                        <div>
                            <label>Menu-1 Name</label>
                            <input type="text" name="menu_1_name" value="{{ !empty($section_content->menu_1_name) ? $section_content->menu_1_name : '' }}" class="input w-full border mt-2" placeholder="Menu Name">
                        </div>
                    </div>
                    <div class="intro-y col-span-12 lg:col-span-6 mt-5">
                        <div>
                            <label>Menu-1 Link</label>
                            <input type="text" name="menu_1_link" value="{{ !empty($section_content->menu_1_link) ? $section_content->menu_1_link : '' }}" class="input w-full border mt-2" placeholder="Menu Link">
                        </div>
                    </div>
                </div>
                <div class="menu_section mt-3">
                    <div class="intro-y col-span-12 lg:col-span-6">
                        <div>
                            <label>Menu-2 Name</label>
                            <input type="text" name="menu_2_name" value="{{ !empty($section_content->menu_2_name) ? $section_content->menu_2_name : '' }}" class="input w-full border mt-2" placeholder="Menu Name">
                        </div>
                    </div>
                    <div class="intro-y col-span-12 lg:col-span-6 mt-5">
                        <div>
                            <label>Menu-2 Link</label>
                            <input type="text" name="menu_2_link" value="{{ !empty($section_content->menu_2_link) ? $section_content->menu_2_link : '' }}" class="input w-full border mt-2" placeholder="Menu Link">
                        </div>
                    </div>
                </div>
                <div class="menu_section mt-3">
                    <div class="intro-y col-span-12 lg:col-span-6">
                        <div>
                            <label>Menu-3 Name</label>
                            <input type="text" name="menu_3_name" value="{{ !empty($section_content->menu_3_name) ? $section_content->menu_3_name : '' }}" class="input w-full border mt-2" placeholder="Menu Name">
                        </div>
                    </div>
                    <div class="intro-y col-span-12 lg:col-span-6 mt-5">
                        <div>
                            <label>Menu-3 Link</label>
                            <input type="text" name="menu_3_link" value="{{ !empty($section_content->menu_3_link) ? $section_content->menu_3_link : '' }}" class="input w-full border mt-2" placeholder="Menu Link">
                        </div>
                    </div>
                </div>
                <div class="menu_section mt-3">
                    <div class="intro-y col-span-12 lg:col-span-6">
                        <div>
                            <label>Menu-4 Name</label>
                            <input type="text" name="menu_4_name" value="{{ !empty($section_content->menu_4_name) ? $section_content->menu_4_name : '' }}" class="input w-full border mt-2" placeholder="Menu Name">
                        </div>
                    </div>
                    <div class="intro-y col-span-12 lg:col-span-6 mt-5">
                        <div>
                            <label>Menu-4 Link</label>
                            <input type="text" name="menu_4_link" value="{{ !empty($section_content->menu_4_link) ? $section_content->menu_4_link : '' }}" class="input w-full border mt-2" placeholder="Menu Link">
                        </div>
                    </div>
                </div>
                <div class="menu_section mt-3">
                    <div class="intro-y col-span-12 lg:col-span-6">
                        <div>
                            <label>Menu-5 Name</label>
                            <input type="text" name="menu_5_name" value="{{ !empty($section_content->menu_5_name) ? $section_content->menu_5_name : '' }}" class="input w-full border mt-2" placeholder="Menu Name">
                        </div>
                    </div>
                    <div class="intro-y col-span-12 lg:col-span-6 mt-5">
                        <div>
                            <label>Menu-5 Link</label>
                            <input type="text" name="menu_5_link" value="{{ !empty($section_content->menu_5_link) ? $section_content->menu_5_link : '' }}" class="input w-full border mt-2" placeholder="Menu Link">
                        </div>
                    </div>
                </div>

                <div class="intro-y col-span-12 lg:col-span-12 mt-5">
					<h2 class="text-lg font-medium mr-auto">Intresting</h2>
				</div>
                <div class="menu_section mt-3">
                    <div class="intro-y col-span-12 lg:col-span-6">
                        <div>
                            <label>Menu-1 Name</label>
                            <input type="text" name="intrest_menu_1_name" value="{{ !empty($section_content->intrest_menu_1_name) ? $section_content->intrest_menu_1_name : '' }}" class="input w-full border mt-2" placeholder="Menu Name">
                        </div>
                    </div>
                    <div class="intro-y col-span-12 lg:col-span-6 mt-5">
                        <div>
                            <label>Menu-1 Link</label>
                            <input type="text" name="intrest_menu_1_link" value="{{ !empty($section_content->intrest_menu_1_link) ? $section_content->intrest_menu_1_link : '' }}" class="input w-full border mt-2" placeholder="Menu Link">
                        </div>
                    </div>
                </div>
                <div class="menu_section mt-3">
                    <div class="intro-y col-span-12 lg:col-span-6">
                        <div>
                            <label>Menu-2 Name</label>
                            <input type="text" name="intrest_menu_2_name" value="{{ !empty($section_content->intrest_menu_2_name) ? $section_content->intrest_menu_2_name : '' }}" class="input w-full border mt-2" placeholder="Menu Name">
                        </div>
                    </div>
                    <div class="intro-y col-span-12 lg:col-span-6 mt-5">
                        <div>
                            <label>Menu-2 Link</label>
                            <input type="text" name="intrest_menu_2_link" value="{{ !empty($section_content->intrest_menu_2_link) ? $section_content->intrest_menu_2_link : '' }}" class="input w-full border mt-2" placeholder="Menu Link">
                        </div>
                    </div>
                </div>
                <div class="menu_section mt-3">
                    <div class="intro-y col-span-12 lg:col-span-6">
                        <div>
                            <label>Menu-3 Name</label>
                            <input type="text" name="intrest_menu_3_name" value="{{ !empty($section_content->intrest_menu_3_name) ? $section_content->intrest_menu_3_name : '' }}" class="input w-full border mt-2" placeholder="Menu Name">
                        </div>
                    </div>
                    <div class="intro-y col-span-12 lg:col-span-6 mt-5">
                        <div>
                            <label>Menu-3 Link</label>
                            <input type="text" name="intrest_menu_3_link" value="{{ !empty($section_content->intrest_menu_3_link) ? $section_content->intrest_menu_3_link : '' }}" class="input w-full border mt-2" placeholder="Menu Link">
                        </div>
                    </div>
                </div>
                <div class="menu_section mt-3">
                    <div class="intro-y col-span-12 lg:col-span-6">
                        <div>
                            <label>Menu-4 Name</label>
                            <input type="text" name="intrest_menu_4_name" value="{{ !empty($section_content->intrest_menu_4_name) ? $section_content->intrest_menu_4_name : '' }}" class="input w-full border mt-2" placeholder="Menu Name">
                        </div>
                    </div>
                    <div class="intro-y col-span-12 lg:col-span-6 mt-5">
                        <div>
                            <label>Menu-4 Link</label>
                            <input type="text" name="intrest_menu_4_link" value="{{ !empty($section_content->intrest_menu_4_link) ? $section_content->intrest_menu_4_link : '' }}" class="input w-full border mt-2" placeholder="Menu Link">
                        </div>
                    </div>
                </div>
                <div class="menu_section mt-3">
                    <div class="intro-y col-span-12 lg:col-span-6">
                        <div>
                            <label>Menu-5 Name</label>
                            <input type="text" name="intrest_menu_5_name" value="{{ !empty($section_content->intrest_menu_5_name) ? $section_content->intrest_menu_5_name : '' }}" class="input w-full border mt-2" placeholder="Menu Name">
                        </div>
                    </div>
                    <div class="intro-y col-span-12 lg:col-span-6 mt-5">
                        <div>
                            <label>Menu-5 Link</label>
                            <input type="text" name="intrest_menu_5_link" value="{{ !empty($section_content->intrest_menu_5_link) ? $section_content->intrest_menu_5_link : '' }}" class="input w-full border mt-2" placeholder="Menu Link">
                        </div>
                    </div>
                </div>

                <div class="intro-y col-span-12 lg:col-span-12 mt-5">
					<h2 class="text-lg font-medium mr-auto">Company</h2>
				</div>
                <div class="menu_section mt-3">
                    <div class="intro-y col-span-12 lg:col-span-6">
                        <div>
                            <label>Menu-1 Name</label>
                            <input type="text" name="company_menu_1_name" value="{{ !empty($section_content->company_menu_1_name) ? $section_content->company_menu_1_name : '' }}" class="input w-full border mt-2" placeholder="Menu Name">
                        </div>
                    </div>
                    <div class="intro-y col-span-12 lg:col-span-6 mt-5">
                        <div>
                            <label>Menu-1 Link</label>
                            <input type="text" name="company_menu_1_link" value="{{ !empty($section_content->company_menu_1_link) ? $section_content->company_menu_1_link : '' }}" class="input w-full border mt-2" placeholder="Menu Link">
                        </div>
                    </div>
                </div>
                <div class="menu_section mt-3">
                    <div class="intro-y col-span-12 lg:col-span-6">
                        <div>
                            <label>Menu-2 Name</label>
                            <input type="text" name="company_menu_2_name" value="{{ !empty($section_content->company_menu_2_name) ? $section_content->company_menu_2_name : '' }}" class="input w-full border mt-2" placeholder="Menu Name">
                        </div>
                    </div>
                    <div class="intro-y col-span-12 lg:col-span-6 mt-5">
                        <div>
                            <label>Menu-2 Link</label>
                            <input type="text" name="company_menu_2_link" value="{{ !empty($section_content->company_menu_2_link) ? $section_content->company_menu_2_link : '' }}" class="input w-full border mt-2" placeholder="Menu Link">
                        </div>
                    </div>
                </div>
                <div class="menu_section mt-3">
                    <div class="intro-y col-span-12 lg:col-span-6">
                        <div>
                            <label>Menu-3 Name</label>
                            <input type="text" name="company_menu_3_name" value="{{ !empty($section_content->company_menu_3_name) ? $section_content->company_menu_3_name : '' }}" class="input w-full border mt-2" placeholder="Menu Name">
                        </div>
                    </div>
                    <div class="intro-y col-span-12 lg:col-span-6 mt-5">
                        <div>
                            <label>Menu-3 Link</label>
                            <input type="text" name="company_menu_3_link" value="{{ !empty($section_content->company_menu_3_link) ? $section_content->company_menu_3_link : '' }}" class="input w-full border mt-2" placeholder="Menu Link">
                        </div>
                    </div>
                </div>
                <div class="menu_section mt-3">
                    <div class="intro-y col-span-12 lg:col-span-6">
                        <div>
                            <label>Menu-4 Name</label>
                            <input type="text" name="company_menu_4_name" value="{{ !empty($section_content->company_menu_4_name) ? $section_content->company_menu_4_name : '' }}" class="input w-full border mt-2" placeholder="Menu Name">
                        </div>
                    </div>
                    <div class="intro-y col-span-12 lg:col-span-6 mt-5">
                        <div>
                            <label>Menu-4 Link</label>
                            <input type="text" name="company_menu_4_link" value="{{ !empty($section_content->company_menu_4_link) ? $section_content->company_menu_4_link : '' }}" class="input w-full border mt-2" placeholder="Menu Link">
                        </div>
                    </div>
                </div>

                <div class="intro-y col-span-12 lg:col-span-12 mt-5">
					<h2 class="text-lg font-medium mr-auto">Contact</h2>
				</div>
                <div class="menu_section mt-3">
                    <div class="intro-y col-span-12 lg:col-span-6">
                        <div>
                            <label>Address</label>
                            <input type="text" name="address" value="{{ !empty($section_content->address) ? $section_content->address : '' }}" class="input w-full border mt-2" placeholder="Address">
                        </div>
                    </div>
                    <div class="intro-y col-span-12 lg:col-span-6 mt-5">
                        <div>
                            <label>Phone</label>
                            <input type="text" name="phone" value="{{ !empty($section_content->phone) ? $section_content->phone : '' }}" class="input w-full border mt-2" placeholder="Phone">
                        </div>
                    </div>
                    <div class="intro-y col-span-12 lg:col-span-6 mt-5">
                        <div>
                            <label>Email</label>
                            <input type="email" name="email" value="{{ !empty($section_content->email) ? $section_content->email : '' }}" class="input w-full border mt-2" placeholder="Email">
                        </div>
                    </div>
                    <div class="intro-y col-span-12 lg:col-span-6 mt-5">
                        <div>
                            <label>Facebook Link</label>
                            <input type="text" name="fb_link" value="{{ !empty($section_content->fb_link) ? $section_content->fb_link : '' }}" class="input w-full border mt-2" placeholder="Facebook Link">
                        </div>
                    </div>
                    <div class="intro-y col-span-12 lg:col-span-6 mt-5">
                        <div>
                            <label>Instagram Link</label>
                            <input type="text" name="insta_link" value="{{ !empty($section_content->insta_link) ? $section_content->insta_link : '' }}" class="input w-full border mt-2" placeholder="Instagram Link">
                        </div>
                    </div>
                    <div class="intro-y col-span-12 lg:col-span-6 mt-5">
                        <div>
                            <label>Linkedin Link</label>
                            <input type="text" name="in_link" value="{{ !empty($section_content->in_link) ? $section_content->in_link : '' }}" class="input w-full border mt-2" placeholder="Linkedin Link">
                        </div>
                    </div>
                    <div class="intro-y col-span-12 lg:col-span-6 mt-5">
                        <div>
                            <label>Youtube Link</label>
                            <input type="text" name="youtube_link" value="{{ !empty($section_content->youtube_link) ? $section_content->youtube_link : '' }}" class="input w-full border mt-2" placeholder="Youtube Link">
                        </div>
                    </div>
                    <div class="intro-y col-span-12 lg:col-span-6 mt-5">
                        <div>
                            <label>Copyright</label>
                            <input type="text" name="copyright" value="{{ !empty($section_content->copyright) ? $section_content->copyright : '' }}" class="input w-full border mt-2" placeholder="Copyright">
                        </div>
                    </div>
                </div>

				<div class="grid grid-cols-12 gap-6 mt-12">
					<div class="intro-y col-span-12 lg:col-span-12 pt-5 border-t border-gray-200 dark:border-dark-5">
						<input type="hidden" name="lang" class="lang" value="{{ $language }}">
						<input type="hidden" name="sectionName" value="footer" class="sectionName">
						<input type="hidden" name="pageName" value="{{ $page_name }}" class="pageName">
						<button type="submit" id="saveBtn" class="button button--md w-24 mr-1 mb-2 bg-theme-9 text-white">Save</button>
						<button type="reset" class="button button--md w-24 mr-1 mb-2 bg-theme-6 text-white">Cancel</button>
					</div>
				</div>
			</form>
		</div>
	</div>

	<script type="text/javascript">
		$(document).ready(function() {
			// Image Reader
			if (window.File && window.FileList && window.FileReader) {
				$("#logo").on("change", function(e) {
					$('.pip').remove();
					var files = e.target.files,
					filesLength = files.length;
					for (var i = 0; i < filesLength; i++) {
						var f = files[i]
						var fileReader = new FileReader();
						fileReader.onload = (function(e) {
							var file = e.target;
							let html = "<span class=\"pip\">" +
							"<img class=\"imageThumb\" src=\"" + e.target.result + "\" title=\"" + file.name + "\"/>" +
							"</span>";

							$(".images_preview").append(html);
							$(".remove").click(function(){
								$(this).parent(".pip").remove();
							});
						});
						fileReader.readAsDataURL(f);
					}
				});
			} else {
				alert("Your browser doesn't support to File API")
			}

			// Submission process
			$('#headerForm').submit(function(e) {
				e.preventDefault();

				$.ajax({
					url: "{{ url('/admin/FilesUpload') }}",
					headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					},
					type: 'POST',
					data: new FormData(this),
					contentType: false,
					processData:false,
					success: function(result) {
                        var logo = result;
                        var content = {
                            logo: logo,
                            menu_1_name: $("input[name='menu_1_name']").val(),
                            menu_1_link: $("input[name='menu_1_link']").val(),
                            menu_2_name: $("input[name='menu_2_name']").val(),
                            menu_2_link: $("input[name='menu_2_link']").val(),
                            menu_3_name: $("input[name='menu_3_name']").val(),
                            menu_3_link: $("input[name='menu_3_link']").val(),
                            menu_4_name: $("input[name='menu_4_name']").val(),
                            menu_4_link: $("input[name='menu_4_link']").val(),
                            menu_5_name: $("input[name='menu_5_name']").val(),
                            menu_5_link: $("input[name='menu_5_link']").val(),
                            intrest_menu_1_name: $("input[name='intrest_menu_1_name']").val(),
                            intrest_menu_1_link: $("input[name='intrest_menu_1_link']").val(),
                            intrest_menu_2_name: $("input[name='intrest_menu_2_name']").val(),
                            intrest_menu_2_link: $("input[name='intrest_menu_2_link']").val(),
                            intrest_menu_3_name: $("input[name='intrest_menu_3_name']").val(),
                            intrest_menu_3_link: $("input[name='intrest_menu_3_link']").val(),
                            intrest_menu_4_name: $("input[name='intrest_menu_4_name']").val(),
                            intrest_menu_4_link: $("input[name='intrest_menu_4_link']").val(),
                            intrest_menu_5_name: $("input[name='intrest_menu_5_name']").val(),
                            intrest_menu_5_link: $("input[name='intrest_menu_5_link']").val(),
                            company_menu_1_name: $("input[name='company_menu_1_name']").val(),
                            company_menu_1_link: $("input[name='company_menu_1_link']").val(),
                            company_menu_2_name: $("input[name='company_menu_2_name']").val(),
                            company_menu_2_link: $("input[name='company_menu_2_link']").val(),
                            company_menu_3_name: $("input[name='company_menu_3_name']").val(),
                            company_menu_3_link: $("input[name='company_menu_3_link']").val(),
                            company_menu_4_name: $("input[name='company_menu_4_name']").val(),
                            company_menu_4_link: $("input[name='company_menu_4_link']").val(),
                            address: $("input[name='address']").val(),
                            phone: $("input[name='phone']").val(),
                            email: $("input[name='email']").val(),
                            fb_link: $("input[name='fb_link']").val(),
                            insta_link: $("input[name='insta_link']").val(),
                            in_link: $("input[name='in_link']").val(),
                            youtube_link: $("input[name='youtube_link']").val(),
                            copyright: $("input[name='copyright']").val()
                        };
						$.ajax({
							url: "{{ url('admin/insertAndUpdateSection') }}",
							headers: {
								'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
							},
							type: 'POST',
							datatype: 'json',
							data: {
								_token: $('input[name=_token]').val(),
								section_name: $('.sectionName').val(),
								page_name: $('.pageName').val(),
								section_content : content,
								language: $('.lang').val(),
							},
							success: function(result) {
								if(result.msg == true) {
									$('html, body').animate({ scrollTop: 0 }, 'slow');
									$(".success_message").slideDown();
									$(".success_message").delay(3000).slideUp();
								}
							}
						});
					}
				});
			});
		});
	</script>
@endsection