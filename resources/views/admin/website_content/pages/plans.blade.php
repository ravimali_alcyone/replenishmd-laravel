@extends('layouts.app')
@section('content')
	<div class="intro-y box mt-5">
		<div class="flex flex-col sm:flex-row items-center p-5 border-b border-gray-200 dark:border-dark-5">
			<h2 class="font-medium text-base mr-auto">Plans > Sections</h2>
		</div>

        <div class="col-span-12 sm:col-span-6 xl:col-span-3 intro-y p-5">
            <div class="home_page_sections_wrapper">
                <a href="{{ env('APP_URL') }}/admin/content/{{$page_name}}/banner" class="section">
                    <h3 class="font-medium text-base mr-auto">Banner</h3>
                </a>
                <a href="{{ env('APP_URL') }}/admin/content/{{$page_name}}/covering_your_care" class="section">
                    <h3 class="font-medium text-base mr-auto">Covering Your Care</h3>
                </a>
                <a href="{{ env('APP_URL') }}/admin/content/{{$page_name}}/membership_features" class="section">
                    <h3 class="font-medium text-base mr-auto">Membership Plans</h3>
                </a>
            </div>
        </div>
	</div>
@endsection