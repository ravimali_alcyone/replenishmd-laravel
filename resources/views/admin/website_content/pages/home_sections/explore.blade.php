@extends('layouts.app')
@section('content')
	<div class="intro-y box mt-5">
		<div class="flex flex-col sm:flex-row items-center p-5 border-b border-gray-200 dark:border-dark-5">
			<h2 class="font-medium text-base mr-auto">Header > Explore </h2>

			
        </div>

		<div class="intro-y datatable-wrapper box p-5 col-span-12 overflow-auto lg:overflow-hidden">
			<div class="rounded-md flex items-center px-5 py-4 mb-2 bg-theme-14 text-theme-10 success_message hidden">Data saved successfully!</div>
			<form id="bannerForm">
				<div class="intro-y col-span-12 lg:col-span-12 mt-5">
					<h2 class="text-lg font-medium mr-auto">Explore</h2>
                </div>

                <div class="intro-y col-span-12 lg:col-span-6 mt-5">
                    <div>
                        <label>Heading</label>
                        <input type="text" name="heading" value="{{ isset($section_content->heading) ? $section_content->heading : '' }}" class="input w-full border mt-2 heading" placeholder="Heading">
                    </div>
                </div>
                <div class="intro-y col-span-12 lg:col-span-6 mt-5">
                    <div>
                        <label>Description</label>
                        <textarea name="description" id="description" cols="30" rows="4" class="input w-full border mt-2" placeholder="Description">{{ isset($section_content->description) ? $section_content->description : '' }}</textarea>
                    </div>
                </div>
                <div class="intro-y col-span-12 lg:col-span-6 mt-5">
                    <div>
                        <label>Link Name</label>
                        <input type="text" name="link_name" value="{{ isset($section_content->link_name) ? $section_content->link_name : '' }}" class="input w-full border mt-2" placeholder="Link Name">
                    </div>
                </div>
                <div class="intro-y col-span-12 lg:col-span-6 mt-5">
                    <div>
                        <label>Link</label>
                        <input type="text" name="link" value="{{ isset($section_content->link) ? $section_content->link : '' }}" class="input w-full border mt-2" placeholder="Link">
                    </div>
                </div>
                <div class="intro-y col-span-12 lg:col-span-6 mt-5">
                    <label>logoes:</label>
                    <input type="file" name="images[]" class="background_img slider_images input w-full border mt-2" multiple="multiple" accept="image/*">
                </div>
                <div class="images_preview mb-4 d-flex">
                    @if(!empty($section_content->logoes))
                        @foreach($section_content->logoes as $key => $value)
                            <input type="hidden" name="old_image[]" value="{{ $value }}">
                            <img src="/{{ $value }}" alt="">
                        @endforeach
                    @endif
                </div>

				<div class="grid grid-cols-12 gap-6 mt-12">
					<div class="intro-y col-span-12 lg:col-span-12 pt-5 border-t border-gray-200 dark:border-dark-5">
						<input type="hidden" name="lang" class="lang" value="{{ $language }}">
						<input type="hidden" name="sectionName" value="explore" class="sectionName">
						<input type="hidden" name="pageName" value="{{ $page_name }}" class="pageName">
						<button type="submit" id="saveBtn" class="button button--md w-24 mr-1 mb-2 bg-theme-9 text-white">Save</button>
						<button type="reset" class="button button--md w-24 mr-1 mb-2 bg-theme-6 text-white" onclick="location.href = '{{ env('APP_URL') }}/admin/content/{{$page_name}}';">Cancel</button>
					</div>
				</div>
			</form>
		</div>
	</div>

	<script type="text/javascript">
		$(document).ready(function() {
			// Image Reader
			if (window.File && window.FileList && window.FileReader) {
				$(".slider_images").on("change", function(e) {
					$('.pip').remove();
					var files = e.target.files,
					filesLength = files.length;
					for (var i = 0; i < filesLength; i++) {
						var f = files[i]
						var fileReader = new FileReader();
						fileReader.onload = (function(e) {
							var file = e.target;
							let html = "<span class=\"pip\">" +
							"<img class=\"imageThumb\" src=\"" + e.target.result + "\" title=\"" + file.name + "\"/>" +
							"</span>";

							$(".images_preview").append(html);
							$(".remove").click(function(){
								$(this).parent(".pip").remove();
							});
						});
						fileReader.readAsDataURL(f);
					}
				});
			} else {
				alert("Your browser doesn't support to File API")
			}

			// Submission process
			$('#bannerForm').submit(function(e) {
				e.preventDefault();
				var heading = $('.heading').val();
				if(heading == ''){
					$('.heading').focus();
					swal({
							title: 'Some required fields are missing.',
							icon: 'error'
						})
					return false;
				}
                $.ajax({
                    url: "{{ url('/admin/MultipalFilesUpload') }}",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type: 'POST',
                    data: new FormData(this),
                    contentType: false,
                    processData:false,
                    success: function(result) {
                        var content = {
                            heading: $('input[name=heading]').val(),
                            description: $('#description').val(),
                            link_name: $('input[name=link_name]').val(),
                            link: $('input[name=link]').val(),
                            logoes: result
                        };

                        $.ajax({
                            url: "{{ url('admin/insertAndUpdateSection') }}",
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            type: 'POST',
                            datatype: 'json',
                            data: {
                                _token: $('input[name=_token]').val(),
                                section_name: $('.sectionName').val(),
                                page_name: $('.pageName').val(),
                                section_content : content,
                                language: $('.lang').val(),
                            },
                            success: function(result) {
                                if(result.msg == true) {
									swal({
										title: 'Data saved successfully!.',
										icon: 'success'
									});
									
									setTimeout(function(){
										swal.close();
										window.location = "{{ env('APP_URL') }}/admin/content/{{ $page_name }}";
									}, 1000);
								}
                            }
                        });
                    }
                });
			});
		});
	</script>
@endsection