@extends('layouts.app')
@section('content')
    @php
    	$i=1;
	@endphp
	<div class="intro-y box mt-5">
		<div class="flex flex-col sm:flex-row items-center p-5 border-b border-gray-200 dark:border-dark-5">
			<h2 class="font-medium text-base mr-auto">Home > Community</h2>
		</div>
		<div class="intro-y datatable-wrapper box p-5 col-span-12 overflow-auto lg:overflow-hidden">
			<form id="community_sectionForm">
				
                    <div class="menu_section mt-3">
                        <div class="intro-y col-span-12 lg:col-span-6">
                            <div>
                                <label>Main Heading</label>
                                <input type="text" name="top_main_name" value="{{ isset($section_content->top_main_name) ? $section_content->top_main_name : '' }}" class="input w-full border mt-2" placeholder="">
                            </div>
                        </div>
                        <div class="intro-y col-span-12 lg:col-span-6 mt-5">
                            <div>
                                <label>Content</label>
                                <input type="text" name="top_main_content" value="{{ isset($section_content->top_main_content) ? $section_content->top_main_content : '' }}" class="input w-full border mt-2" placeholder="">
                            </div>
                        </div>						
                    </div>

				
				    <div class="menu_section mt-3">
                        <div class="intro-y col-span-12 lg:col-span-6">
                            <div>
                                <label>Heading-1</label>
                                <input type="text" name="heading_1" value="{{ isset($section_content->heading_1) ? $section_content->heading_1 : '' }}" class="input w-full border mt-2" placeholder="">
                            </div>
                        </div>
                        <div class="intro-y col-span-12 lg:col-span-6 mt-5">
                            <div>
                                <label>Content-1</label>
                                <input type="text" name="content_1" value="{{ isset($section_content->content_1) ? $section_content->content_1 : '' }}" class="input w-full border mt-2" placeholder="">
                            </div>
                        </div>						
                    </div>

                    <div class="menu_section mt-3">
                        <div class="intro-y col-span-12 lg:col-span-6">
                            <div>
                                <label>Heading-2</label>
                                <input type="text" name="heading_2" value="{{ isset($section_content->heading_2) ? $section_content->heading_2 : '' }}" class="input w-full border mt-2" placeholder="">
                            </div>
                        </div>
                        <div class="intro-y col-span-12 lg:col-span-6 mt-5">
                            <div>
                                <label>Content-2</label>
                                <input type="text" name="content_2" value="{{ isset($section_content->content_2) ? $section_content->content_2 : '' }}" class="input w-full border mt-2" placeholder="">
                            </div>
                        </div>						
                    </div>

                    <div class="menu_section mt-3">
                        <div class="intro-y col-span-12 lg:col-span-6">
                            <div>
                                <label>Heading-3</label>
                                <input type="text" name="heading_3" value="{{ isset($section_content->heading_3) ? $section_content->heading_3 : '' }}" class="input w-full border mt-2" placeholder="">
                            </div>
                        </div>
                        <div class="intro-y col-span-12 lg:col-span-6 mt-5">
                            <div>
                                <label>Content-3</label>
                                <input type="text" name="content_3" value="{{ isset($section_content->content_3) ? $section_content->content_3 : '' }}" class="input w-full border mt-2" placeholder="">
                            </div>
                        </div>						
                    </div>

                    <div class="menu_section mt-3">
                        <div class="intro-y col-span-12 lg:col-span-6">
                            <div>
                                <label>Heading-4</label>
                                <input type="text" name="heading_4" value="{{ isset($section_content->heading_4) ? $section_content->heading_4 : '' }}" class="input w-full border mt-2" placeholder="">
                            </div>
                        </div>
                        <div class="intro-y col-span-12 lg:col-span-6 mt-5">
                            <div>
                                <label>Content-4</label>
                                <input type="text" name="content_4" value="{{ isset($section_content->content_4) ? $section_content->content_4 : '' }}" class="input w-full border mt-2" placeholder="">
                            </div>
                        </div>						
                    </div>

				<div class="grid grid-cols-12 gap-6 mt-12">
					<div class="intro-y col-span-12 lg:col-span-12 pt-5 border-t border-gray-200 dark:border-dark-5 form_btn_wrapper">
						<input type="hidden" name="sectionName" value="community" class="sectionName">
						<input type="hidden" name="pageName" value="{{ $page_name }}" class="pageName">
						<button type="submit" id="saveBtn" class="button button--md w-24 mr-1 mb-2 bg-theme-9 text-white">Save</button>
						<button type="reset" class="button button--md w-24 mr-1 mb-2 bg-theme-6 text-white" onclick="location.href = '{{ env('APP_URL') }}/admin/content/{{$page_name}}';">Cancel</button>
					</div>
				</div>
			</form>
		</div>
	</div>

	<script type="text/javascript">
	        // Submission process
			$('#community_sectionForm').submit(function(e) {
				e.preventDefault();
                var top_main_name = $('[name="top_main_name"]').val();
                var top_main_content = $('[name="top_main_content"]').val();
				var heading_1 = $('[name="heading_1"]').val();
				var content_1 = $('[name="content_1"]').val();
				var heading_2 = $('[name="heading_2"]').val();
				var content_2 = $('[name="content_2"]').val();
                var heading_3 = $('[name="heading_3"]').val();
				var content_3 = $('[name="content_3"]').val();
                var heading_4 = $('[name="heading_4"]').val();
				var content_4 = $('[name="content_4"]').val();
                
                var content = { top_main_name: top_main_name,top_main_content: top_main_content, heading_1: heading_1, content_1: content_1, heading_2: heading_2, content_2: content_2,heading_3: heading_3, content_3: content_3,heading_4: heading_4, content_4: content_4 };
                    $.ajax({
                        url: "{{ url('admin/insertAndUpdateSection') }}",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        type: 'POST',
                        datatype: 'json',
                        data: {
                            _token: $('input[name=_token]').val(),
                            section_name: $('.sectionName').val(),
                            page_name: $('.pageName').val(),
                            section_content : content,
                        },
                        success: function(result) {
                            if(result.msg == true) {
                                swal({
                                    title: 'Data saved successfully!',
                                    icon: 'success'
                                });	

                                setTimeout(function(){										
                                    swal.close();
                                }, 1000);									
                            }
                        }
                    });
			});

           
	</script>
@endsection