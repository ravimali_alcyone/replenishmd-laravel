@extends('layouts.app')
@section('content')
	@php
	$i=1;
	@endphp
	<div class="intro-y box mt-5">
		<div class="flex flex-col sm:flex-row items-center p-5 border-b border-gray-200 dark:border-dark-5">
			<h2 class="font-medium text-base mr-auto">Header > Features ({{ strtoupper($language) }})</h2>

			<ul class="lang_nav">
				<li>
					<a href="{{ URL('/admin/en/'.$page_name.'/features') }}" class="<?php if($language == 'en'){ echo 'active'; } ?>">English</a>
				</li>
				<!-- <li>
					<a href="{{ URL('/admin/fr/'.$page_name) }}" class="<?php //if($language == 'fr'){ echo 'active'; } ?>">Français</a>
				</li> -->
				<li>
					<a href="{{ URL('/admin/nl/'.$page_name.'/features') }}" class="<?php if($language == 'nl'){ echo 'active'; } ?>">Nederlands</a>
				</li>
			</ul>
		</div>
		<div class="intro-y datatable-wrapper box p-5 col-span-12 overflow-auto lg:overflow-hidden">
			<div class="rounded-md flex items-center px-5 py-4 mb-2 bg-theme-14 text-theme-10 success_message">Data saved successfully!</div>
			<form id="bannerForm">
				<div class="intro-y col-span-12 lg:col-span-12 mt-5">
					<h2 class="text-lg font-medium mr-auto">Features</h2>
					<button type="button" class="button button--sm w-24 mr-1 mb-2 bg-theme-1 text-white float-right add_new_menu_btn add_new_slide_btn">Add More</button>
                </div>

                @if(empty($section_content->features))
                    <div class="menu_section mt-3">
                        <div class="intro-y col-span-12 lg:col-span-6">
                            <div>
                                <label>Heading</label>
                                <input type="text" name="headings[]" value="" class="input w-full border mt-2" placeholder="Heading">
                            </div>
                        </div>
                        <div class="intro-y col-span-12 lg:col-span-6 mt-5">
                            <div>
                                <label>Description</label>
                                <input type="text" name="descriptions[]" value="" class="input w-full border mt-2" placeholder="Description">
                            </div>
                        </div>
                        <div class="intro-y col-span-12 lg:col-span-6 mt-5">
                            <label>Image:</label>
                            <input type="file" name="images[]" class="background_img slider_images input w-full border mt-2" onchange="imageReader(event, 'preview_<?php echo $i;?>')">
                            <input type="hidden" name="old_image[]" value="">
                        </div>
                        <div class="images_preview mypreview preview_<?php echo $i;?> mb-4"></div>
                    </div>
				@endif

                <div id="slides_wrapper">
                    @if(!empty($section_content->features))
                        @foreach($section_content->features as $key => $value)
                        <div class="menu_section mt-3">
                            <div class="text-right"><a href="javascript:;" class="remove_section_btn">X</a></div>
                            <div class="intro-y col-span-12 lg:col-span-6">
                                <div>
                                    <label>Heading</label>
                                    <input type="text" name="headings[]" value="{{ $value->heading }}" class="input w-full border mt-2" placeholder="Heading">
                                </div>
                            </div>
                            <div class="intro-y col-span-12 lg:col-span-6 mt-5">
                                <div>
                                    <label>Description</label>
                                    <input type="text" name="descriptions[]" value="{{ $value->description }}" class="input w-full border mt-2" placeholder="Description">
                                </div>
                            </div>
                            <div class="intro-y col-span-12 lg:col-span-6 mt-5">
                                <label>Image:</label>
                                <input type="file" name="images[]" class="background_img slider_images input w-full border mt-2" onchange="imageReader(event, 'preview_<?php echo $i;?>')">
                                <input type="hidden" name="old_image[]" value="{{ !empty($value->image) ? $value->image : '' }}">
                            </div>
                            <div class="images_preview mypreview preview_<?php echo $i;?> mb-4">
                                @if(!empty($value->image))
                                    <img src="/{{ $value->image }}" alt="" class="imageThumb">
                                @endif
                            </div>
                        </div>
						@php
						$i++;
						@endphp							
                        @endforeach
                    @endif
                </div>

				<div class="grid grid-cols-12 gap-6 mt-12">
					<div class="intro-y col-span-12 lg:col-span-12 pt-5 border-t border-gray-200 dark:border-dark-5">
						<input type="hidden" name="lang" class="lang" value="{{ $language }}">
						<input type="hidden" name="sectionName" value="features" class="sectionName">
						<input type="hidden" name="pageName" value="{{ $page_name }}" class="pageName">
						<button type="submit" id="saveBtn" class="button button--md w-24 mr-1 mb-2 bg-theme-9 text-white">Save</button>
						<button type="reset" class="button button--md w-24 mr-1 mb-2 bg-theme-6 text-white" onclick="location.href = '{{ env('APP_URL') }}admin/{{$language}}/{{$page_name}}';">Cancel</button>
					</div>
				</div>
			</form>
		</div>
	</div>

	<script type="text/javascript">
	
            // Image Reader
            function imageReader(e, preview_class) {
            if (window.File && window.FileList && window.FileReader) {
                var files = e.target.files,
                filesLength = files.length;
                for (var i = 0; i < filesLength; i++) {
                    var f = files[i]
                    var fileReader = new FileReader();
                    fileReader.onload = (function(e) {
                        var file = e.target;
                        let html = "<span class=\"pip\">" +
                        "<img class=\"imageThumb\" src=\"" + e.target.result + "\" title=\"" + file.name + "\"/>" +
                        "</span>";
                        $('.' + preview_class).empty().append(html);
                    });
                    fileReader.readAsDataURL(f);
                }
            } else {
                alert("Your browser doesn't support to File API")
            }
        }
		
		$(document).ready(function() {

			// Add new menu
			$(".add_new_slide_btn").click(function() {
                var x = <?php echo $i;?>; 
                var rand = x++;
                var cl = "'preview_"+rand+"'";					
				let html = '<div class="menu_section mt-3">'+
                                '<div class="text-right"><a href="javascript:;" class="remove_section_btn">X</a></div>'+
                                '<div class="intro-y col-span-12 lg:col-span-6">'+
                                    '<div>'+
                                        '<label>Heading</label>'+
                                        '<input type="text" name="headings[]" value="" class="input w-full border mt-2" placeholder="Heading">'+
                                    '</div>'+
                                '</div>'+
                                '<div class="intro-y col-span-12 lg:col-span-6 mt-5">'+
                                    '<div>'+
                                        '<label>Description</label>'+
                                        '<input type="text" name="descriptions[]" value="" class="input w-full border mt-2" placeholder="Description">'+
                                    '</div>'+
                                '</div>'+
                                '<div class="intro-y col-span-12 lg:col-span-6 mt-5">'+
                                    '<label>Image:</label>'+
                                    '<input type="file" name="images[]" class="background_img slider_images input w-full border mt-2" onchange="imageReader(event, '+cl+')">'+
                                    '<input type="hidden" name="old_image[]" value="">'+
                                '</div>'+
                                '<div class="images_preview mypreview preview_'+rand+' mb-4"></div>'+
                            '</div>';
				$("#slides_wrapper").append(html);
			});

			// Remove menu section
			$(document).on("click", ".remove_section_btn", function() {
				if (confirm('Are you sure?')) {
					$(this).closest('.menu_section').remove();
				}
			});

			// Submission process
			$('#bannerForm').submit(function(e) {
				e.preventDefault();
				var headings = $('[name="headings[]"]').map(function(){return $(this).val();}).get();
                var descriptions = $('[name="descriptions[]"]').map(function(){return $(this).val();}).get();
                var old_images = $('[name="old_image[]"]').map(function(){return $(this).val();}).get();
				var features = [];

                $.ajax({
                    url: "{{ url('/admin/multiSectionImageUpload') }}",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type: 'POST',
                    data: new FormData(this),
                    contentType: false,
                    processData:false,
                    success: function(result) {
                        $.each(headings, function(key,value){
                            var feature = {
                                heading : headings[key],
                                description : descriptions[key],
                                image : result[key]
                            };
                            features.push(feature);
                        });

                        var content = { features: features };
                        $.ajax({
                            url: "{{ url('admin/insertAndUpdateSection') }}",
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            type: 'POST',
                            datatype: 'json',
                            data: {
                                _token: $('input[name=_token]').val(),
                                section_name: $('.sectionName').val(),
                                page_name: $('.pageName').val(),
                                section_content : content,
                                language: $('.lang').val(),
                            },
                            success: function(result) {
                                if(result.msg == true) {
									$('html, body').animate({ scrollTop: 0 }, 'slow');
									$(".success_message").slideDown();
									$(".success_message").delay(3000).slideUp();
								}
                            }
                        });
                    }
                });
			});
		});
	</script>
@endsection