@extends('layouts.app')

@section('content')

<div class="intro-y flex flex-col sm:flex-row items-center mt-8">
	<h2 class="text-lg font-medium mr-auto">@if(isset($title)){{ $title }} @endif List</h2>
	<div class="w-full sm:w-auto flex mt-4 sm:mt-0">
		<div class="intro-y col-span-12 flex flex-wrap sm:flex-no-wrap items-center mt-2">
			<a href="{{ url('admin/forums/post/add') }}" class="button w-32 mr-2 mb-2 flex items-center justify-center btn_white">Add New</a>
		</div>
	</div>
</div>

<div class="grid grid-cols-12 gap-6 mt-5">
	<div class="intro-y col-span-12 items-center">
		<form id="tableFilters">
			<div class="col-span-3">
				<label>Moderator</label>
				<select id="author" name="" class="select2 w-full">
					<option value="">Any</option>
					@if($users && $users->count() > 0)
						@foreach($users as $users)
							<option value="{{ $users->id }}" >{{ $users->name }}</option>
						@endforeach
					@endif
				</select>
			</div>

			<div class="col-span-3">
				<label>Forum Category</label>
				<select id="filter_category" class="select2 w-full">
					<option value="">Any</option>
					@if($categories && $categories->count() > 0)
						@foreach($categories as $category)
							<option value="{{ $category->id }}" >{{ $category->forum_subject }}</option>
						@endforeach
					@endif
				</select>
			</div>

			<div class="col-span-3 ml-2">
				<label>Status</label>
				<select id="publish_status" class="select2 w-full">
					<option value="" class="any">Any</option>
					<option value="1">Active</option>
					<option value="0">Inactive</option>
				</select>
			</div>

			<div class="col-span-3 ml-2">
				<label>Comment</label>
				<select id="comment_status" class="select2 w-full">
					<option value="" class="any">Any</option>
					<option value="1">Allow</option>
					<option value="0">Not-allow</option>
				</select>
			</div>

			<div class="w-auto mt-3 mt-md-0">
				<button type="button" class="button w-24 mr-1 mb-2 btn_dark ml-2" id="filter">Show</button>
				<button type="reset" class="button w-24 mr-1 mb-2 btn_red float-right ml-2" id="reset">Reset</button>
			</div>
		</form>
	</div>

	<div class="intro-y datatable-wrapper box p-5 col-span-12 overflow-auto lg:overflow-hidden" id="patients_table">
		<table id="myTable" class="table table-report table-report--bordered display w-full sub_admin_table dataTable mt-0">
			<thead>
				<tr class="intro-x">
					<th class="border-b-2">S.No.</th>
					<th class="border-b-2">Forum Subject</th>
					<th class="border-b-2">Post Heading</th>
					<th class="border-b-2">Author</th>					
					<th class="border-b-2">Status</th>
					<th class="border-b-2">Comment</th>
					<th class="border-b-2">Action</th>
				</tr>
			</thead>
			<tbody></tbody>
		</table>
	</div>
</div>

<script type="text/javascript">
	var table;
 	$(document).ready(function() {
		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});

		fill_datatable();
		function fill_datatable(author='', filter_category = '', publish_status = '', comment_status = ''){
			
			table = $('#myTable').DataTable({
				processing: true,
				serverSide: true,
				responsive: true,
				paging: true,
				ordering: false,
				info: false,
				displayLength: 10,
				language: {
					processing: '<img src="/dist/images/loading.gif"/>'
				},				
				lengthMenu: [
				[10, 25, 50, -1],
				[10, 25, 50, "All"]
				],
				ajax:{
					url: "{{ route('admin.forums.posts') }}",
					data:{"author":author, "filter_category":filter_category,"publish_status":publish_status, "comment_status":comment_status }
				},
				columns: [
					{data: 'DT_RowIndex', name: 'DT_RowIndex'},
					{data: 'forum_subject', name: 'forum_subject'},	
					{data: 'title', name: 'title'},								
					{data: 'author', name: 'author'},
					{data: 'status', name: 'status'},
					{data: 'comment_on', name: 'comment_on'},
					{data: 'action', name: 'action', orderable: false, searchable: false},
				]
			});
		}

		$('#filter').click(function(){
			var author = $('#author').val();
			var filter_category = $('#filter_category').val();
			var publish_status = $('#publish_status').val();
			var comment_status = $('#comment_status').val();
			$('#myTable').DataTable().destroy();
			fill_datatable(author,filter_category,publish_status,comment_status);
		});

		$('#reset').click(function(){
			$(".select2-selection__rendered").attr('title', 'Any').html('Any');
			$('#myTable').DataTable().destroy();
			fill_datatable();
		});

		$("#myTable tbody tr").addClass('intro-x');
  	});

    function deleteRow(id) {
		swal({
			title: "Are you sure to delete?",
			text: "",
			icon: 'warning',
			buttons: {
			  cancel: true,
			  delete: 'Yes, Delete It'
			}
		  }).then((isConfirm) => {
			if (!isConfirm) {
				return false;
			} else {
				$.ajax({
					type: "DELETE",
					url: "{{ env('APP_URL').'/admin/forums/post/destroy' }}"+'/'+id,
					success: function (response) {
						if(response['status'] == 'success'){
						swal({
							title: response['message'],
							icon: 'success'
						});
						setTimeout(function(){
						swal.close();
						}, 1000);
						}
						table.draw();
					},
					error: function (data) {
						console.log('Error:', data);
						swal({
							title: 'Error Occured',
							icon: 'error'
						});
					}
				});
			}
        });
    }

	function changeStatus(id,status,flag) {
		$.ajax({
			url: "{{ route('admin.forums.post.status') }}",
			type: "POST",
			data: {'id': id, 'status':status, 'flag':flag},
			success: function(response) 
			{
				if(response['status'] == 'success')
				{
					swal({
						title: response['message'],
						icon: 'success'
					});
					setTimeout(function(){
					swal.close();
					}, 1000);
				}
				table.draw();
			}
		});
	}
	
	function changeCommentStatus(id,comment_on,flag) {
		$.ajax({
			url: "{{ route('admin.forums.topics.comment_on') }}",
			type: "POST",
			data: {'id': id, 'comment_on':comment_on, 'flag':flag},
			success: function(response) 
			{
				if(response['status'] == 'success')
				{
					swal({
						title: response['message'],
						icon: 'success'
					});
					setTimeout(function(){
					swal.close();
					}, 1000);
				}
				table.draw();
			}
		});
	}

	function changePaidStatus(id,comment_on) {
		$.ajax({
			url: "{{ route('admin.forums.topics.is_paid') }}",
			type: "POST",
			data: {'id': id, 'comment_on':comment_on},
			success: function(response) 
			{
				if(response['status'] == 'success')
				{
					swal({
						title: response['message'],
						icon: 'success'
					});
					setTimeout(function(){
					swal.close();
					}, 1000);
				}
				table.draw();
			}
		});
	}
	
	function verifyRow(id) {
		swal({
			title: "Are you sure to verify?",
			text: "",
			icon: 'info',
			buttons: {
			  cancel: true,
			  delete: 'Yes, Verify'
			}
		  }).then((isConfirm) => {
			if (!isConfirm) {
				return false;
			} else {
				$.ajax({
					type: "DELETE",
					url: "{{ env('APP_URL').'/admin/forums/topics/verify' }}"+'/'+id,
					success: function (response) {
						if(response['status'] == 'success'){
						swal({
							title: response['message'],
							icon: 'success'
						});
						setTimeout(function(){
							
							swal.close();						
						}, 1000);
						}
						table.draw();
					},
					error: function (data) {
						console.log('Error:', data);
						swal({
							title: 'Error Occured',
							icon: 'error'
						});
					}
				});
			}
        });
    }
</script>
</html>
@endsection