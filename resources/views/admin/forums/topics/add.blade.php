@extends('layouts.app')
@section('content')

	<div class="intro-y box mt-5">
		<div class="flex flex-col sm:flex-row items-center p-5 border-b border-gray-200 dark:border-dark-5">
			<h2 class="font-medium text-base mr-auto"><a href="{{env('APP_URL').'/admin/forums/posts'}}">@if(isset($title)){{ $title }} @endif List</a> > @if($forum_topic) {{ 'Edit' }} @else Add New @endif Post</h2>
		</div>
		<div class="intro-y datatable-wrapper box p-5 col-span-12 overflow-auto lg:overflow-hidden">
			<form id="forumTopicForm">
				
				<div class="grid grid-cols-12 gap-6 mt-5">
					<div class="col-span-10">
						<div class="intro-y col-span-6 lg:col-span-6 mt-4 mb-4">
							<div>
								<label>Title</label>
								<input type="text" name="title" value="<?php if($forum_topic){ echo $forum_topic->title;}?>" class="input w-full border mt-2" placeholder="Title" id="title" required>
							</div>
						</div>
						
						<div class="intro-y col-span-6 lg:col-span-6 mt-4 mb-4">
							<div>
								<label>Slug</label>
								<input type="text" name="slug" value="<?php if($forum_topic){ echo $forum_topic->slug;}?>" class="input w-full border mt-2" placeholder="Slug" id="slug" required>
							</div>
						</div>
						
						<div class="intro-y col-span-12">
							<div class="grid grid-cols-12  gap-6">
								<div class="col-span-6">
									<label>Forum</label>
									<div class="mt-2">
									<select id="category_id" name="category" class="select2 w-full" required>
										<option value="">--Select--</option>
										@if($categories && $categories->count() > 0)
											@foreach($categories as $category)
												<option value="{{ $category->id }}" <?php if($forum_topic && $forum_topic->forum_category_id == $category->id){ echo 'selected';}?> >{{ $category->forum_subject }}</option>
											@endforeach
										@endif
									</select>
									</div>
								</div>
							</div>
						</div>

						<div class="intro-y col-span-12 lg:col-span-12 mt-4">
							<label>Description</label>
							<textarea name="description" id="description" class="input w-full border mt-2"><?php if($forum_topic){ echo $forum_topic->description; }?></textarea>
						</div>
					</div>
					<div class="col-span-2 p-3 bg-gray-100">
						<div class="intro-y col-span-12">
							<div class="grid grid-cols-12  gap-6">
								<div class="col-span-12">
									<label>Status</label>
									<div class="mt-2">
										<input type="radio" id="status_1" name="status" value="1" <?php if(!$forum_topic){ echo 'checked';} ?> class="mr-3" <?php if($forum_topic && $forum_topic->status==1){ echo "checked"; } ?> ><label for="status_1" class="mr-4">Active</label>
										<input type="radio" id="status_0" name="status" value="0" class="mr-4" <?php if($forum_topic && $forum_topic->status==0){ echo "checked"; } ?> ><label for="status_0">Inactive</label>
									</div>
								</div>
								<div class="col-span-12">
									<label>Allow Comment</label>
									<div class="mt-2">
										<input type="checkbox" name="comment_on" id="comment_on" value="1" class="form-checkbox" <?php if($forum_topic && $forum_topic->comment_on == 1){ echo "checked"; } ?>>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="grid grid-cols-12 gap-6 mt-12">
					<div class="intro-y col-span-12 lg:col-span-12">
						<input type="hidden" name="id" value="<?php if($forum_topic){ echo $forum_topic->id;}?>"/>
						<input type="hidden" name="page" value="<?php if($forum_topic){ echo 'edit';} else { echo 'add'; }?>"/>
						<button type="submit" id="saveBtn" class="button button--md w-24 mr-1 mb-2 btn_blue">Save</button>
						<button type="reset" class="button button--md w-24 mr-1 mb-2 btn_red" onclick="location.href = '{{ route('admin.forums.posts') }}';">Cancel</button>
					</div>
				</div>
			</form>
		</div>
	</div>

<script type="text/javascript">

	CKEDITOR.replace('description',{
			height: '400px',
		});
		CKEDITOR.add;


	$("#forumTopicForm").submit(function(e) {
		e.preventDefault();
		var description = CKEDITOR.instances.description.getData();

		var formData = new FormData(this);
		formData.append('description', description);

		$.ajax({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			// data: $('#forumTopicForm').serialize(),
			url: "{{ route('admin.forums.topics.store') }}",
			data: formData,
			type: "POST",
			cache:false,
			contentType: false,
			processData: false,
			// dataType: 'json',
			success: function (response) {
				//response = JSON.parse(res);
				if(response['status'] == 'success'){
					swal({
						title: response['message'],
						icon: 'success'
					});
					$('#forumTopicForm').trigger("reset");
					setTimeout(function(){
					swal.close();
					window.location = "{{ route('admin.forums.posts') }}"; }, 1000);
				}
			},
			error: function (data) {
				let errorArr = [];
				let errors = data.responseJSON.errors;
				let emailErr = errors.email[0];
				
			}
		});
	});
	
$("#title").bind("keyup change", function(e) {
    let text = $("#title").val();
	let slug = convertToSlug(text);
	$("#slug").val(slug);
})	

function convertToSlug(Text){
    return Text
        .toLowerCase()
        .replace(/[^\w ]+/g,'')
        .replace(/ +/g,'-')
        ;
}


</script>
</html>
@endsection