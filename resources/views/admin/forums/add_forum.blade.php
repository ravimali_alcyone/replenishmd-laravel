@extends('layouts.app')
@section('content')

	<div class="intro-y box mt-5">
		<div class="flex flex-col sm:flex-row items-center p-5 border-b border-gray-200 dark:border-dark-5">
			<h2 class="font-medium text-base mr-auto"><a href="{{env('APP_URL').'/admin/forums'}}">@if(isset($title)){{ $title }} @endif List</a> > @if($forum_topic) {{ 'Edit' }} @else Create New @endif Forum</h2>
		</div>
		<div class="intro-y datatable-wrapper box p-5 col-span-12 overflow-auto lg:overflow-hidden">
			<form id="forumTopicForm">
				
				<div class="grid grid-cols-12 gap-6 mt-5">
					<div class="col-span-10">
						<div class="intro-y col-span-6 lg:col-span-6 mt-4 mb-4">
							<div>
								<label>Subject</label>
								<input type="text" name="forum_subject" value="<?php if($forum_topic){ echo $forum_topic->forum_subject;}?>" class="input w-full border mt-2" placeholder="Forum Subject" id="forum_subject" required>
							</div>
						</div>
						
						<div class="intro-y col-span-6 lg:col-span-6 mt-4 mb-4">
							<div>
								<label>Slug</label>
								<input type="text" name="slug" value="<?php if($forum_topic){ echo $forum_topic->slug;}?>" class="input w-full border mt-2" placeholder="Slug" id="slug" required>
							</div>
						</div>
						
						

						<div class="intro-y col-span-12 lg:col-span-12 mt-4">
							<label>Description</label>
							<textarea name="short_info" id="short_info" col="30" rows="4" class="input w-full border mt-2"><?php if($forum_topic){ echo $forum_topic->short_info; }?></textarea>
						</div>
					</div>
					<div class="col-span-2 p-3 bg-gray-100">
						<div class="intro-y col-span-12">
						<div class="intro-y col-span-12">
							<div class="grid grid-cols-12  gap-6">
								<div class="col-span-12 border-bottom">
									<label><strong>Status</strong></label>
									<div>
										<input type="radio" id="status_1" name="status" value="1" <?php if(!$forum_topic){ echo 'checked';} ?> class="mr-3" <?php if($forum_topic && $forum_topic->status==1){ echo "checked"; } ?> ><label for="status_1" class="mr-4">Active</label>
										<input type="radio" id="status_0" name="status" value="0" class="mr-4" <?php if($forum_topic && $forum_topic->status==0){ echo "checked"; } ?> ><label for="status_0">Inactive</label>
									</div>
								</div>
								<div class="col-span-12 mt-1 border-bottom">
									<label><strong>Is Access Paid?</strong></label>
									<div>
										<input type="radio" id="is_paid_1" name="is_paid" value="1" <?php if(!$forum_topic){ echo 'checked';} ?> class="mr-3" <?php if($forum_topic && $forum_topic->is_paid==1){ echo "checked"; } ?> ><label for="is_paid_1" class="mr-4">Yes</label>
										<input type="radio" id="is_paid_0" name="is_paid" value="0" class="mr-4" <?php if($forum_topic && $forum_topic->is_paid==0){ echo "checked"; } ?> ><label for="is_paid_0">No</label>
									</div>
									<div>
										<input type="radio" id="access_payment_type_1" name="access_payment_type" value="1" <?php if(!$forum_topic){ echo 'checked';} ?> class="mr-3" <?php if($forum_topic && $forum_topic->access_payment_type==1){ echo "checked"; } ?> ><label for="is_paid_1" class="mr-4">One time</label>
										<input type="radio" id="access_payment_type_0" name="access_payment_type" value="0" class="mr-4" <?php if($forum_topic && $forum_topic->access_payment_type==0){ echo "checked"; } ?> ><label for="is_paid_0">Monthly</label>
									</div>
									<div class="mb-2">
										<input type="text" name="access_amount" id="access_amount" value="<?php if($forum_topic){ echo $forum_topic->access_amount; } ?>" class="form-control">
									</div>
								</div>
								<div class="col-span-12 mt-1">
									<label><strong>Is Posting Paid?</strong></label>
									<div>
										<input type="radio" id="is_posting_1" name="is_posting" value="1" <?php if(!$forum_topic){ echo 'checked';} ?> class="mr-3" <?php if($forum_topic && $forum_topic->is_posting_paid==1){ echo "checked"; } ?> ><label for="is_paid_1" class="mr-4">Yes</label>
										<input type="radio" id="is_posting_0" name="is_posting" value="0" class="mr-4" <?php if($forum_topic && $forum_topic->is_posting_paid==0){ echo "checked"; } ?> ><label for="is_paid_0">No</label>
									</div>
									<div>
										<input type="radio" id="posting_payment_type_1" name="posting_payment_type" value="1" <?php if(!$forum_topic){ echo 'checked';} ?> class="mr-3" <?php if($forum_topic && $forum_topic->posting_payment_type==1){ echo "checked"; } ?> ><label for="is_paid_1" class="mr-4">One time</label>
										<input type="radio" id="posting_payment_type_0" name="posting_payment_type" value="0" class="mr-4" <?php if($forum_topic && $forum_topic->posting_payment_type==0){ echo "checked"; } ?> ><label for="is_paid_0">Monthly</label>
									</div>
									<div>
										<input type="text" name="posting_amount" id="posting_amount" value="<?php if($forum_topic){ echo $forum_topic->posting_amount; } ?>" class="form-control">
									</div>
								</div>
							</div>
						</div>
						</div>
					</div>
				</div>
				<div class="grid grid-cols-12 gap-6 mt-12">
					<div class="intro-y col-span-12 lg:col-span-12">
						<input type="hidden" name="id" value="<?php if($forum_topic){ echo $forum_topic->id;}?>"/>
						<input type="hidden" name="page" value="<?php if($forum_topic){ echo 'edit';} else { echo 'add'; }?>"/>
						<button type="submit" id="saveBtn" class="button button--md w-24 mr-1 mb-2 btn_blue">Save</button>
						<button type="reset" class="button button--md w-24 mr-1 mb-2 btn_red" onclick="location.href = '{{ route('admin.forums') }}';">Cancel</button>
					</div>
				</div>
			</form>
		</div>
	</div>

<script type="text/javascript">

	$("#forumTopicForm").submit(function(e) {
		e.preventDefault();
		var formData = new FormData(this);
		$.ajax({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			url: "{{ route('admin.forums.store') }}",
			data: formData,
			type: "POST",
			cache:false,
			contentType: false,
			processData: false,
			// dataType: 'json',
			success: function (response) {
				//response = JSON.parse(res);
				if(response['status'] == 'success'){
					swal({
						title: response['message'],
						icon: 'success'
					});
					$('#forumTopicForm').trigger("reset");
					setTimeout(function(){
					swal.close();
					window.location = "{{ route('admin.forums') }}"; }, 1000);
				}
			},
			error: function (data) {
				let errorArr = [];
				let errors = data.responseJSON.errors;
				let emailErr = errors.email[0];				
			}
		});
	});
	
$("#forum_subject").bind("keyup change", function(e) {
    let text = $("#forum_subject").val();
	let slug = convertToSlug(text);
	$("#slug").val(slug);
})	

function convertToSlug(Text){
    return Text
        .toLowerCase()
        .replace(/[^\w ]+/g,'')
        .replace(/ +/g,'-')
        ;
}


</script>
</html>
@endsection