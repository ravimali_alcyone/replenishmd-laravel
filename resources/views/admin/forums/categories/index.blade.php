@extends('layouts.app')

@section('content')

<div class="intro-y flex flex-col sm:flex-row items-center mt-8">
	<h2 class="text-lg font-medium mr-auto">@if(isset($title)){{ $title }} @endif List</h2>
	<div class="w-full sm:w-auto flex mt-4 sm:mt-0">
		<div class="intro-y col-span-12 flex flex-wrap sm:flex-no-wrap items-center mt-2">
			<a href="{{ url('admin/forums/addForum/0') }}" class="button w-32 mr-2 mb-2 flex items-center justify-center btn_white">Create Forum</a>
			<a href="{{ url('admin/forums/posts') }}" class="button w-32 mr-2 mb-2 flex items-center justify-center btn_white">Forum Post</a>
		</div>
	</div>
</div>

<div class="grid grid-cols-12 gap-6 mt-5">
	<div class="intro-y col-span-10 items-center">
		<form id="tableFilters">
			<div class="col-span-3">
				<label>Moderator</label>
				<select id="author" name="" class="select2 w-full">
					<option value="">Any</option>
					@if($users && $users->count() > 0)
						@foreach($users as $users)
							<option value="{{ $users->id }}" >{{ $users->name }}</option>
						@endforeach
					@endif
				</select>
			</div>

			<div class="col-span-3 ml-2">
				<label>Forum Status</label>
				<select id="vstatus" class="select2 w-full">
					<option value="" class="any">Any</option>
					<option value="1">Verified</option>
					<option value="0">Not verified</option>
				</select>
			</div>
			<div class="col-span-3 w-auto mt-3 mt-md-0">
				<button type="button" class="button w-24 mr-1 mb-2 btn_dark ml-2" id="filter">Show</button>
				<button type="reset" class="button w-24 mr-1 mb-2 btn_red float-right ml-2" id="reset">Reset</button>
			</div>
			
			
			

		</form>
	</div>
											

	<div class="intro-y datatable-wrapper box p-5 col-span-12 overflow-auto lg:overflow-hidden" id="patients_table">
		<table id="myTable" class="table table-report table-report--bordered display w-full sub_admin_table dataTable mt-0">
			<thead>
				<tr class="intro-x">
					<th class="border-b-2">S.No.</th>
					<th class="border-b-2">Forum Subject</th>
					<th class="border-b-2">Moderator</th>					
					<th class="border-b-2">Number Of Post </th>
					<th class="border-b-2">Action</th>
				</tr>
			</thead>
			<tbody></tbody>
		</table>
	</div>
</div>

<script type="text/javascript">
	var table;
 	$(document).ready(function() {
		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});

		fill_datatable();
		function fill_datatable(author='', vstatus = ''){
			
			table = $('#myTable').DataTable({
				processing: true,
				serverSide: true,
				responsive: true,
				paging: true,
				ordering: false,
				info: false,
				displayLength: 10,
				language: {
					processing: '<img src="/dist/images/loading.gif"/>'
				},				
				lengthMenu: [
				[10, 25, 50, -1],
				[10, 25, 50, "All"]
				],
				ajax:{
					url: "{{ route('admin.forums.categories.list') }}",
					data:{"author":author,"vstatus":vstatus }
				},
				columns: [
					{data: 'DT_RowIndex', name: 'DT_RowIndex'},
					{data: 'title', name: 'title'},	
					{data: 'moderator', name: 'moderator'},
					{data: 'no_of_post', name: 'no_of_post'},
					{data: 'action', name: 'action', orderable: false, searchable: false},
				]
			});
		}

		$('#filter').click(function(){
			var author = $('#author').val();
			var vstatus = $('#vstatus').val();
			$('#myTable').DataTable().destroy();
			fill_datatable(author,vstatus);
		});

		$('#reset').click(function(){
			$(".select2-selection__rendered").attr('title', 'Any').html('Any');
			$('#myTable').DataTable().destroy();
			fill_datatable();
		});

		$("#myTable tbody tr").addClass('intro-x');
  	});

    function deleteRow(id) {
		swal({
			title: "Are you sure to delete?",
			text: "",
			icon: 'warning',
			buttons: {
			  cancel: true,
			  delete: 'Yes, Delete It'
			}
		  }).then((isConfirm) => {
			if (!isConfirm) {
				return false;
			} else {
				$.ajax({
					type: "DELETE",
					url: "{{ env('APP_URL').'/admin/forums/categories/destroy' }}"+'/'+id,
					success: function (response) {
						if(response['status'] == 'success'){
						swal({
							title: response['message'],
							icon: 'success'
						});
						setTimeout(function(){
						swal.close();
						}, 1000);
						}
						table.draw();
					},
					error: function (data) {
						console.log('Error:', data);
						swal({
							title: 'Error Occured',
							icon: 'error'
						});
					}
				});
			}
        });
    }

	function verifyRow(id) {
		swal({
			title: "Are you sure to verify?",
			text: "",
			icon: 'info',
			buttons: {
			  cancel: true,
			  delete: 'Yes, Verify'
			}
		  }).then((isConfirm) => {
			if (!isConfirm) {
				return false;
			} else {
				$.ajax({
					type: "DELETE",
					url: "{{ env('APP_URL').'/admin/forum/category/verify' }}"+'/'+id,
					success: function (response) {
						if(response['status'] == 'success'){
						swal({
							title: response['message'],
							icon: 'success'
						});
						setTimeout(function(){
							
							swal.close();						
						}, 1000);
						}
						table.draw();
					},
					error: function (data) {
						console.log('Error:', data);
						swal({
							title: 'Error Occured',
							icon: 'error'
						});
					}
				});
			}
        });
    }

	function changeStatus(id,status,flag) {
		$.ajax({
			url: "{{ url('admin/forums/categories/change_status') }}",
			type: "POST",
			data: {'id': id, 'status':status, 'flag':flag},
			success: function(response) 
			{
				if(response['status'] == 'success')
				{
					swal({
						title: response['message'],
						icon: 'success'
					});
					setTimeout(function(){
					swal.close();
					}, 1000);
				}
				table.draw();
			}
		});
	}
</script>
</html>
@endsection