@extends('layouts.app')
@section('content')
	<div class="intro-y box mt-5">
		<div class="flex flex-col sm:flex-row items-center p-5 border-b border-gray-200 dark:border-dark-5">
			<h2 class="font-medium text-base mr-auto"><a href="{{env('APP_URL').'/admin/blogs/blog_post_type'}}">Blog Post Type List</a> > @if(isset($catData)) {{ 'Edit' }} @else Add @endif Post Type</h2>
		</div>
		<div class="intro-y datatable-wrapper box p-5 col-span-12 overflow-auto lg:overflow-hidden">
			<form id="postTypeForm">
				<div class="grid grid-cols-12 gap-6 mt-5">
					<div class="intro-y col-span-12 lg:col-span-12">
						<div>
							<label>Post Type</label>
							<input type="text" name="post_type" value="<?php if(isset($catData)){ echo $catData->type_name; }?>" class="input w-full border mt-2" placeholder="Title" id="post_type" >
						</div>
					</div>
				</div>
				<div class="grid grid-cols-12 gap-6 mt-12">
					<div class="intro-y col-span-12 lg:col-span-12">
						<input type="hidden" name="id" id="id" value="{{$id}}"/>
						<input type="hidden" name="page" id="page" value="{{$funcType}}"/>
						<button type="submit" id="saveBtn" class="button button--md w-24 mr-1 mb-2 btn_blue">Save</button>
						<button type="reset" class="button button--md w-24 mr-1 mb-2 btn_red" onclick="location.href = '{{ route('admin.blogs.blog_post_type') }}';">Cancel</button>
					</div>
				</div>
			</form>
		</div>
	</div>

	<script type="text/javascript">
		$("#postTypeForm").submit(function(e) {
			e.preventDefault();
			
			$.ajax({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				data: {"post_type":$("#post_type").val(),"id":$("#id").val(),"page":$("#page").val()},
				url: "{{ route('admin.blogs.store_post_type') }}",
				type: "POST",
				dataType: 'json',
				success: function (response) {
					if(response['status'] == 'success'){
						$('#postTypeForm').trigger("reset");
						successAlert(response['message'],2000,'top-right');	
						setTimeout(function(){
						window.location = "{{ route('admin.blogs.blog_post_type') }}"; }, 1000);
					}
					else{
						errorAlert('Error occured.',3000,'top-right');						
					}
				},
				error: function (data) {
					$(".loader").css('display', 'none');
					let errors = data.responseJSON.errors;
					
					$.each(errors, function(key, value) {
						errorAlert(value[0],3000,'top-right');
					});	
				}
			});
		});
	
</script>
</html>
@endsection