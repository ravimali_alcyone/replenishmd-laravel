@extends('layouts.app')
@section('content')
	<div class="intro-y box mt-5">
		<div class="flex flex-col sm:flex-row items-center p-5 border-b border-gray-200 dark:border-dark-5">
			<h2 class="font-medium text-base mr-auto"><a href="{{env('APP_URL').'/admin/forums/forum_topic'}}">Forum Topic</a> > @if(isset($forumData)) {{ 'Edit' }} @else Add @endif Topic</h2>
		</div>
		<div class="intro-y datatable-wrapper box p-5 col-span-12 overflow-auto lg:overflow-hidden">
			<form id="forumsTopicForm">
				<div class="grid grid-cols-6 gap-6 mt-5">
					<div class="intro-y col-span-12 lg:col-span-12">
						<div>
							<label>Forum name</label>
							<select name="forum_name" id="forum_name" class="select2 w-full">
								@if($forums && $forums->count() > 0)
									@foreach($forums as $val)
										<option value="{{ $val->id }}" <?php if(isset($forumData)){ if($forumData && $forumData->forum_id == $val->id){ echo 'selected';} }?> >{{ $val->title }}</option>
									@endforeach
								@endif
							</select>
						</div>
					</div>					
				</div>
				<div class="grid grid-cols-6 gap-6 mt-5">
					<div class="intro-y col-span-12 lg:col-span-12">
						<div>
							<label>Topic</label>
							<input type="text" name="topic" required value="{{ $forumData->topic ?? '' }}" class="input w-full border mt-2" placeholder="Topic" id="topic" >
						</div>
					</div>
				</div>
				<div class="grid grid-cols-6 gap-6 mt-5">
					<div class="intro-y col-span-12 lg:col-span-12">
						<div>
							<label>Description</label>
							<textarea id="description" class="input w-full border mt-2" name="description" rows="4" cols="8" placeholder="Description">{{ $forumData->description ??''}}</textarea>
							
						</div>
					</div>
				</div>
				<div class="grid grid-cols-12 gap-6 mt-5">
					<div class="intro-y col-span-6 lg:col-span-6">
						<div>
							<label>Permalink</label>
							<input type="text" class="input w-full border mt-2" disabled placeholder="{{env('APP_URL')}}/forums/{{ $forumData->forum_slug ?? 'forum-topic'}}">
						</div>
					</div>
					<div class="intro-y col-span-6 lg:col-span-6">
						<div>
							<label>Slug</label>
							<input type="text" name="slug" value="{{ $forumData->slug ?? '' }}" class="input w-full border mt-2" placeholder="Permalink" id="slug" required>
						</div>
					</div>
				</div>
				<div class="grid grid-cols-12 gap-6 mt-12">
					<div class="intro-y col-span-12 lg:col-span-12">
						<input type="hidden" name="id" id="id" value="{{$id}}"/>
						<input type="hidden" name="page" id="page" value="{{$funcType}}"/>
						<button type="submit" id="saveBtn" class="button button--md w-24 mr-1 mb-2 btn_blue">Save</button>
						<button type="reset" class="button button--md w-24 mr-1 mb-2 btn_red" onclick="location.href = '{{ route('admin.forums.forum_topic') }}';">Cancel</button>
					</div>
				</div>
			</form>
		</div>
	</div>

	<script type="text/javascript">
	

	$("#forumsTopicForm").submit(function(e) {
		e.preventDefault();
		$.ajax({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			data: {"forum_id":$("#forum_name").val(),"topic":$("#topic").val(),"id":$("#id").val(),"page":$("#page").val(), "description":$("#description").val(), "slug":$("#slug").val()},
			url: "{{ route('admin.forums.store_topic') }}",
			type: "POST",
			dataType: 'json',
			success: function (response) {
				if(response['status'] == 'success'){
					$('#forumsTopicForm').trigger("reset");
					//successAlert(response['message'],2000,'top-right');	
					swal({
						title: response['message'],
						icon: 'success'
					});
					setTimeout(function(){
					swal.close();
					window.location = "{{ route('admin.forums.forum_topic') }}"; }, 1000);
				}
				else{
					errorAlert('Error occured.',3000,'top-right');						
				}
			},
			error: function (data) {
				$(".loader").css('display', 'none');
				let errors = data.responseJSON.errors;
				
				$.each(errors, function(key, value) {
					errorAlert(value[0],3000,'top-right');
				});	
			}
		});
	});
	
	$("#topic").bind("keyup change", function(e) {
		let text = $("#topic").val();
		let slug = convertToSlug(text);
		$("#slug").val(slug);
	})	

	function convertToSlug(Text)
	{
		return Text
			.toLowerCase()
			.replace(/[^\w ]+/g,'')
			.replace(/ +/g,'-')
			;
	}

	
</script>
</html>
@endsection