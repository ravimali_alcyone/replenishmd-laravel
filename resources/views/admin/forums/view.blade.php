
@extends('layouts.app')
@section('content')
    <!-- BEGIN: Content -->
   
    <div class="content">
        <div class="flex flex-col sm:flex-row items-center p-5 border-b border-gray-200 dark:border-dark-5">
			<h2 class="font-medium text-base mr-auto"><a href="{{env('APP_URL').'/admin/blogs'}}">Blogs List</a> > Post Details</h2>
		</div>
        
        <!-- END: Profile Info -->
        <div class="tab-content mt-5">
            <div class="tab-content__pane active" id="blog_detail_page">
                <div class="grid grid-cols-12 gap-6">

                    <div class="col-span-12 box p-3">
                    <div class="blog_preview">
                        <div class="blog_title">{{ $blog->blog_title ?? ''}}</div>
                        <div class="blog_info">
                            <div class="blog_cat rounded-lg bg-gray-200 p-1 pl-3 pr-3 mb-2 inline-block"><b>Category Name -</b> {!! ucfirst($category) ?? '' !!}</div>
                            <div class="tags">
                            <b>Tags -</b> {{ $blog->post_tags ?? ''}}
                            </div>
                        </div>
                        <div class="blog_desc">
                                {!! $blog->blog_content ?? '' !!}
                        </div>
                        <div class="blog_by">
                            <div class="bloger_name">{!! ucfirst($author_name) ?? '' !!}</div>
                            <div class="bloger_time">{!! $blog->created_at ?? '' !!}</div>
                        </div>
                    </div>
                    </div>
                </div>

                <div class="intro-y col-span-12 lg:col-span-4 mt-5">
                    <div class="">
                        <div class="relative flex items-center">
                            <button type="button" class="button w-24 mr-1 mb-2 btn_dark ml-2" onclick="javascript:location.href = '/admin/blogs'">Back</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END: Content -->
    </div>

    <script>
        $(document).ready(function() {
            $("#blog_info").find('table').addClass('table table-report table-report--bordered sub_admin_table');

            $(".blog_thumbnail_section img").click(function() {
                $(".blog_main_img img").attr('src', $(this).attr('src')).attr('data-zoom', $(this).attr('src'));
            });
            $(".blog_main_img img").mouseover(function() {
                $(this).css('opacity', '0');
            });
            $(".blog_main_img img").mouseout(function() {
                $(this).css('opacity', '1');
            });
        });
    </script>
@endsection