
@extends('layouts.app')
@section('content')
    <!-- BEGIN: Content -->
    <style>
        #certificate_licence .image_wrapper {
            margin-right: 8px;
            max-width: 120px;
            margin-right: 12px;
        }
        #certificate_licence .image_wrapper img{
            width: 100px;
            height: 80px;
            border: 1px solid #ff776d;
        }
        #certificate_licence {
            display: flex;
            flex-wrap: wrap;
            justify-content: start;
            margin-top: 18px;
            flex-direction: s;
        }
        
        .modal-header {
            display: -ms-flexbox;
            display: flex;
            -ms-flex-align: start;
            align-items: flex-start;
            -ms-flex-pack: justify;
            justify-content: space-between;
            padding: 1rem 1rem;
            color: #fff;
            border-top-left-radius: calc(.3rem - 1px);
            border-top-right-radius: calc(.3rem - 1px);
            background-color: #ff6c66;
        }
        
        .modal-content {
            position: relative;
            display: -ms-flexbox;
            display: flex;
            -ms-flex-direction: column;
            flex-direction: column;
            width: 100%;
            pointer-events: auto;
            background-color: #fff;
            background-clip: padding-box;
            border: 1px solid rgba(0, 0, 0, .2);
            border-radius: .3rem;
            outline: 0;
        }
        
        .modal-dialog {
            position: relative;
            width: auto;
            margin: .5rem;
            pointer-events: none;
        }
        
        .close {
            float: right;
            font-size: 1.5rem;
            font-weight: 700;
            line-height: 1;
            color: #000;
            text-shadow: 0 1px 0 #fff;
            opacity: .5;
        }
        .modal-open .modal {
            overflow-x: hidden;
            overflow-y: auto;
        }
        
        .modal-body {
            position: relative;
            -ms-flex: 1 1 auto;
            flex: 1 1 auto;
            padding: 1rem;
        }
        
        .modal-footer {
            display: -ms-flexbox;
            display: flex;
            -ms-flex-wrap: wrap;
            flex-wrap: wrap;
            -ms-flex-align: center;
            align-items: center;
            -ms-flex-pack: end;
            justify-content: flex-end;
            padding: .75rem;
            border-top: 1px solid #dee2e6;
            border-bottom-right-radius: calc(.3rem - 1px);
            border-bottom-left-radius: calc(.3rem - 1px);
        }
        
    </style>
	@php
	$provider_licenses = json_decode($provider->provider_licenses,true);
	@endphp
    <div class="content">
        <div class="intro-y flex items-center mt-8">
            <h2 class="text-lg font-medium mr-auto">Provider Detail</h2>
        </div>
        <!-- BEGIN: Profile Info -->
        <div class="intro-y box px-5 pt-5 mt-5">
            <div class="flex flex-col lg:flex-row border-b border-gray-200 dark:border-dark-5 pb-5 -mx-5">
                <div class="flex flex-1 px-5 items-center justify-center lg:justify-start">
                    <div class="w-20 h-20 sm:w-24 sm:h-24 flex-none lg:w-32 lg:h-32 image-fit relative">
                        @if($provider->image != null)
                            <img alt="" class="rounded-full" src="/{{$provider->image}}">
                        @else
                            <img alt="" class="rounded-full" src="/dist/images/user_icon.png">
                        @endif
                    </div>
                    <div class="ml-5">
                        <div class="w-24 sm:w-40 truncate sm:whitespace-normal font-medium text-lg">@if($provider->name != null) {{ucfirst($provider->name)}} @endif</div>
                        <div class="text-gray-600 flex">
                            @if($categories != null)
                                @php
                                    $i = 1;
                                    $count = count($categories);
                                @endphp
                                @foreach($categories as $key => $value)
                                    <a href="{{ url('admin/providers') }}/{{$value->id}}" class="custome_email_link">{{ucfirst($value->name)}}<a/>
                                    @if($count != $i)
                                        ,&nbsp;
                                    @endif
                                    @php
                                        $i += 1
                                    @endphp
                                @endforeach
                            @endif
                        </div>
                    </div>
                </div>
                <div class="flex mt-6 lg:mt-0 items-center lg:items-start flex-1 flex-col justify-center text-gray-600 dark:text-gray-300 px-5 border-l border-r border-gray-200 dark:border-dark-5 border-t lg:border-t-0 pt-5 lg:pt-0">
                    <div class="truncate sm:whitespace-normal flex items-center"><a href="mailto:{{$provider->email}}" class="custome_email_link"><i data-feather="mail" class="w-4 h-4 mr-2"></i> @if($provider->email != null) {{$provider->email}} @endif</a> </div>
                    <div class="truncate sm:whitespace-normal flex items-center mt-3"><a href="tel:{{$provider->phone}}" class="custome_email_link"><i data-feather="phone-call" class="w-4 h-4 mr-2"></i> @if($provider->phone != null) {{$provider->phone}} @endif</a> </div>
                    <div class="sm:whitespace-normal flex items-center mt-3"><a href="http://maps.google.com/?q={{ $provider->address }}" target="_blank" class="custome_email_link"> <i data-feather="home" class="w-4 h-4 mr-2"></i> @if($provider->address != null) {{$provider->address}} @endif</a> </div>
                </div>
            </div>
        </div>
        <!-- END: Profile Info -->

        <div class="tab-content mt-5">
            <div class="tab-content__pane active" id="profile">
                <div class="grid grid-cols-12 gap-6">
                    <!-- BEGIN: Daily Sales -->
                    <div class="intro-y box col-span-12 lg:col-span-4">
                        <div class="p-5">
                            <div class="relative flex items-center">
                                <div class="mr-auto">
                                    <span class="font-medium">Gender</span>
                                </div>
                                <div class="font-medium text-gray-600">{{ucfirst($provider->gender) ?? ''}}</div>
                            </div>
                            <div class="relative flex items-center mt-5">
                                <div class="mr-auto">
                                    <span class="font-medium">Date of Birth</span>
                                </div>
                                <div class="font-medium text-gray-600">@if($provider->dob != null) {{ucfirst($provider->dob)}} @endif</div>
                            </div>
                            <div class="relative flex items-center mt-5">
                                <div class="mr-auto">
                                    <span class="font-medium">Age</span>
                                </div>
                                <div class="font-medium text-gray-600">@if($provider->age != null) {{ucfirst($provider->age)}} @endif</div>
                            </div>
                            <div class="relative flex items-start mt-5">
                                <div class="mr-auto">
                                    <span class="font-medium">Practice Address</span>
                                </div>
                                <div class="font-medium text-gray-600 ml-8 text-right">@if($provider->practice_address != null) {{ucfirst($provider->practice_address)}} @endif</div>
                            </div>
                            {{-- <div class="relative flex items-start mt-5">
                                <div class="mr-auto">
                                    <span class="font-medium">Categories</span>
                                </div>
                                <div class="font-medium text-gray-600 ml-8 text-right">@if($categories) {{$categories}} @endif</div>
                            </div> --}}

                        </div>
                    </div>

                    <div class="intro-y box col-span-12 lg:col-span-4">
                        <div class="p-5">
                            <div class="relative flex items-center">
                                <div class="mr-auto">
                                    <span class="font-medium">RMD ID</span>
                                </div>
                                <div class="font-medium text-gray-600">@if($provider->rmd_ad_id != null) {{$provider->rmd_ad_id}} @endif</div>
                            </div>
                            <div class="relative flex items-center mt-5">
                                <div class="mr-auto">
                                    <span class="font-medium">Google Adsense ID</span>
                                </div>
                                <div class="font-medium text-gray-600">@if($provider->google_adsense_id != null) {{$provider->google_adsense_id}} @endif</div>
                            </div>
                            <div class="relative flex items-center mt-5">
                                <div class="mr-auto">
                                    <span class="font-medium">Amazon ID</span>
                                </div>
                                <div class="font-medium text-gray-600">@if($provider->amazon_id != null) {{$provider->amazon_id}} @endif</div>
                            </div>
                            <div class="relative flex items-center mt-5">
                                <div class="mr-auto">
                                    <span class="font-medium">Status</span>
                                </div>
                                <div class="font-medium text-gray-600">@if($provider->status == 1) Active @else Blocked @endif</div>
                            </div>
                        </div>
                    </div>

                    <div class="intro-y box col-span-12 lg:col-span-4">
                        <div class="p-5">
                            <div class="relative flex items-center mt-5">
                                <div class="mr-auto">
                                    <span class="font-medium">Provider Licenses</span>
                                </div>
                                <div class="font-medium text-gray-600">
                                    @if($provider->provider_licenses != null)
                                        <div class="text-center">
                                            <a href="javascript:;" data-toggle="modal" data-target="#slick-modal-preview" class="button inline-block btn_gray">Show Licenses</a>
                                        </div>
                                        <div class="modal" id="slick-modal-preview">
                                            <span class="close_btn"></span>
                                            <div class="modal__content modal__content--xl relative">
                                                <div class="p-5">
                                                    <div class="license_slider">
                                                        <div class="type_filter_wrapper">
                                                            <label for="" class="d-block">License </label>
                                                        
                                                        </div>

                                                        <a data-dismiss="modal" href="javascript:;" class="absolute close right-0 top-0 mt-3 mr-3"> <i data-feather="x" class="close_icon w-8 h-8 text-gray-500"></i> </a>
                                                        @php
                                                            $i = 1;
                                                        @endphp
                                                        @if($provider_licenses)
                                                            {{-- @foreach($provider_licenses as $key => $value)
                                                                <div class="h-screen px-2">
                                                                    <div class="h-full image-fit rounded-md overflow-hidden">
                                                                        <img src="/{{$value}}" />
                                                                    </div>
                                                                </div>
                                                            @endforeach --}}
                                                    <div class="tab-pane container fade active show" id="menu1">
                                                        <div class="summary_box hover-effect-box">
                                                            <div class="row" id="certificate_licence">
                                                                <?php
																if($provider_licenses != null){
																	$img = $provider_licenses;
																	$len =  count($img);
																	for($i=0; $i<$len; $i++)
																	{
																		if(preg_match("/\.(pdf)$/", $img[$i])){
																			?>
																			<div class="image_wrapper">
																				<div class="demo_pic">
																					<a class="credentialsPdfModal" href="javascript:void(0)" data-toggle="modal" data-url="{{env('APP_URL')}}/{{$img[$i]}}" data-target="#credentialsPdfModal">
																						<img src="{{env('APP_URL')}}/images/pdf-icon.png">
																					</a>
																				</div>
																			</div>								
																		<?php
																		}else{ 
																	
																?>
																	<div class="image_wrapper">
																		<div class="demo_pic">
																			<a href="/{{$img[$i]}}" data-lightbox="lightbox-set-img">
																				<img src="/{{$img[$i]}}">
																			</a>
																		</div>
																	</div>								
																<?php 
																}
																
																} }?>
                                                            </div>
                                                        </div>
                                                            </div>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
								</div>
                            </div>
                            <div class="relative flex items-start mt-5">
                                <div class="mr-auto">
                                    <span class="font-medium">NPI</span>
                                </div>
                                <div class="font-medium text-gray-600 ml-8 text-right">@if($provider->npi != null) {{ucfirst($provider->npi)}} @endif</div>
                            </div>
                            <div class="relative flex items-start mt-5">
                                <div class="mr-auto">
                                    <span class="font-medium">DEA No.</span>
                                </div>
                                <div class="font-medium text-gray-600 ml-8 text-right">@if($provider->dea != null) {{ucfirst($provider->dea)}} @endif</div>
                            </div>
                        </div>
                    </div>
                    <div class="intro-y col-span-12 lg:col-span-4">
                        <div class="">
                            <div class="relative flex items-center">
                                <button type="button" class="button w-24 mr-1 mb-2 btn_dark ml-2" onclick="javascript:location.href = '/admin/providers'">Back</button>
                            </div>
                        </div>
                    </div>
                    <!-- END: Daily Sales -->
                </div>
            </div>
        </div>
    </div>
    <!-- END: Content -->
    
    <!---pdf viewer---->
	<div id="credentialsPdfModal" class="modal" tabindex="-1" role="dialog">
	  <div class="modal-dialog" role="document">
		<div class="modal-content mx-auto" style="width: 700px;">
		  <div class="modal-header">
			<h5 class="modal-title">PDF Viewer</h5>
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			  <span aria-hidden="true">&times;</span>
			</button>
		  </div>

		  <div class="modal-body">
			<canvas id="the-canvas"></canvas>
		  </div>
		  
		  <div class="modal-footer">
		       <div style="text-align:center;">
				  <button class="button btn_gray" id="prev">Previous</button>
				  <button class="button btn_gray" id="next">Next</button>
				  &nbsp; &nbsp;
				  <span>Page: <span id="page_num"></span> / <span id="page_count"></span></span>
			   </div>
		  </div>
		 
		</div>
	  </div>
	</div>
	<!--End--->
@endsection

@section('script')
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src="{{ asset('/src/js/lightslider.js') }}"></script>
<script>
    $(document).ready(function() {
        $("#content-slider").lightSlider({
            loop:true,
            keyPress:true
        });
        $('#image-gallery, #image-gallery2, #image-gallery3').lightSlider({
            gallery:true,
            item:1,
            thumbItem:9,
            slideMargin: 0,
            speed:500,
            auto: false,
            loop:true,
            onSliderLoad: function() {
                $('#image-gallery, #image-gallery2, #image-gallery3').removeClass('cS-hidden');
            }
        });

        $(".l_type_c_slider").closest(".lSSlideOuter").hide();
    });

    // Filter for license
    function license_type_filter(e) {
        let val = $(e).val();
        // alert(val);
        let html = '<ul id="image-gallery2" class="gallery list-unstyled cS-hidden">';
        let data = new Array();
        <?php
            if ($provider_licenses && !empty($provider_licenses)) {
                foreach($provider_licenses as $key => $value) {
        ?>
            data.push('<?php echo $value; ?>');
        <?php } } ?>
        // console.log(data);
        if (data != null) {
            let x = 0;
            $.each(data, function(key, value) {
                if (val == "l_type_a") {
                    if (x <= 2) {
                        html += '<li data-thumb="/'+ value +'" class="l_type_a">'+
                                    '<img src="/'+ value +'" class="l_type_a w-full" />'+
                                '</li>';
                    }
                }

                if(val == "l_type_c") {
                    if (x > 2 && x <= 5) {
                        html += '<li data-thumb="/'+ value +'" class="l_type_a">'+
                                    '<img src="/'+ value +'" class="l_type_a w-full" />'+
                                '</li>';
                    }
                }

                if(val == "l_type_g") {
                    if (x >= 4 && x <= 6) {
                        html += '<li data-thumb="/'+ value +'" class="l_type_a">'+
                                    '<img src="/'+ value +'" class="l_type_a w-full" />'+
                                '</li>';
                    }
                }
                x += 1;
            });
        }
        // console.log(html);
        $(".carousel_wrapper").empty();
        setTimeout(() => {
            $(".carousel_wrapper").append(html);
            $("#image-gallery2").lightSlider({
                gallery: true,
                item: 1,
                thumbItem: 9,
                slideMargin: 0,
                speed: 500,
                auto: false,
                loop: true,
                onSliderLoad: function() {
                    $("#image-gallery2").removeClass('cS-hidden');
                }
            });
        }, 100);
    }
</script>

<script src="//mozilla.github.io/pdf.js/build/pdf.js"></script>
	<script>
	$(document).on('click', '.credentialsPdfModal', function() {
		
		var url = $(this).attr("data-url");

		// Loaded via <script> tag, create shortcut to access PDF.js exports.
		var pdfjsLib = window['pdfjs-dist/build/pdf'];

		// The workerSrc property shall be specified.
		pdfjsLib.GlobalWorkerOptions.workerSrc = '//mozilla.github.io/pdf.js/build/pdf.worker.js';

		var pdfDoc = null,
			pageNum = 1,
			pageRendering = false,
			pageNumPending = null,
			scale = 1.1,
			canvas = document.getElementById('the-canvas'),
			ctx = canvas.getContext('2d');

		/**
		 * Get page info from document, resize canvas accordingly, and render page.
		 * @param num Page number.
		 */
		function renderPage(num) {
		  pageRendering = true;
		  // Using promise to fetch the page
		  pdfDoc.getPage(num).then(function(page) {
			var viewport = page.getViewport({scale: scale});
			canvas.height = viewport.height;
			canvas.width = viewport.width;

			// Render PDF page into canvas context
			var renderContext = {
			  canvasContext: ctx,
			  viewport: viewport
			};
			var renderTask = page.render(renderContext);

			// Wait for rendering to finish
			renderTask.promise.then(function() {
			  pageRendering = false;
			  if (pageNumPending !== null) {
				// New page rendering is pending
				renderPage(pageNumPending);
				pageNumPending = null;
			  }
			});
		  });

		  // Update page counters
		  document.getElementById('page_num').textContent = num;
		}

		/**
		 * If another page rendering in progress, waits until the rendering is
		 * finised. Otherwise, executes rendering immediately.
		 */
		function queueRenderPage(num) {
		  if (pageRendering) {
			pageNumPending = num;
		  } else {
			renderPage(num);
		  }
		}

		/**
		 * Displays previous page.
		 */
		function onPrevPage() {
		  if (pageNum <= 1) {
			return;
		  }
		  pageNum--;
		  queueRenderPage(pageNum);
		}
		document.getElementById('prev').addEventListener('click', onPrevPage);

		/**
		 * Displays next page.
		 */
		function onNextPage() {
		  if (pageNum >= pdfDoc.numPages) {
			return;
		  }
		  pageNum++;
		  queueRenderPage(pageNum);
		}
		document.getElementById('next').addEventListener('click', onNextPage);

		/**
		 * Asynchronously downloads PDF.
		 */
		pdfjsLib.getDocument(url).promise.then(function(pdfDoc_) {
		  pdfDoc = pdfDoc_;
		  document.getElementById('page_count').textContent = pdfDoc.numPages;

		  // Initial/first page rendering
		  renderPage(pageNum);
		});
		
	});

	</script>
@endsection