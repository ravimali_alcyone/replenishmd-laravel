@extends('layouts.app')

@section('content')

    <!-- BEGIN: Page Main-->
	<div id="main">
		<div class="row">
			<div class="content-wrapper-before gradient-45deg-indigo-purple"></div>
			<div class="col s12">
				<div class="breadcrumbs-dark pb-0 pt-4" id="breadcrumbs-wrapper">
					<!-- Search for small screen-->
					<div class="container">
						<div class="row">
							<div class="col s10 m6 l6">
								<h5 class="breadcrumbs-title mt-0 mb-0"><span><?php if($user) echo 'Edit'; else echo 'Add New';?> Doctor</span></h5>
							</div>
						</div>
					</div>
				</div>

				<div class="container">
					<div class="section users-edit">
						<div class="card">
							<div class="card-content">
								<div class="row">
									<div class="col s12" id="information">
										<form id="DoctorForm">
											<div class="row">
												<input type="hidden" name="id" value="<?php if($user){ echo $user->id;}?>"/>

												<div class="col s12 input-field">
													<input id="name" name="name" type="text" class="validate" data-error=".errorTxt1" value="<?php if($user){ echo $user->name;}?>" required>
													<label for="name">Name</label>
													<small class="errorTxt1"></small>
												</div>

												<div class="col s12 input-field">
													<input id="email" name="email" type="email" class="validate" data-error=".errorTxt2" value="<?php if($user){ echo $user->email;}?>" required>
													<label for="email">Email</label>
													<small class="errorTxt2"></small>
												</div>

												<div class="col s12 input-field">
													<select id="category" data-error=".errorTxt3" name="category">
													<option value="1" <?php if($user && $user->category == "1"){ echo 'selected';}?> >Cardiologist</option>
													<option value="2" <?php if($user && $user->category == "2"){ echo 'selected';}?>>Audiologist</option>
													<option value="3" <?php if($user && $user->category == "3"){ echo 'selected';}?>>Psychiatrists</option>
													<option value="4" <?php if($user && $user->category == "4"){ echo 'selected';}?>>Gynaecologist</option>
													</select>
													<label>Category</label>
													<small class="errorTxt3"></small>
												</div>

												<div class="col s12 input-field">
													<div class="switch">
														<label>
														Blocked
														<input type="checkbox" name="status" data-error=".errorTxt4" <?php if($user && $user->status == "1"){ echo 'checked';}?>>
														<span class="lever"></span>
														Active
														</label>
													</div>
													<small class="errorTxt4"></small>
												</div>

												<div class="col s12 display-flex justify-content-end mt-1">
													<button id="saveBtn" type="submit" class="btn indigo">
													Save</button>
													<a href="{{ route('admin.providers') }}" class="btn btn-light">Cancel</a>
												</div>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

<script type="text/javascript">


  if ($(".users-edit").length > 0) {
    $("#DoctorForm").validate({
      rules: {
        name: {
          required: true
        },
        email: {
          required: true
        },
        privileges: {
          required: true
        }
      },
      errorElement: 'div',
	  submitHandler: function(form) {
		// do other things for a valid form
		$.ajax({
		  headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		  },
		  data: $('#DoctorForm').serialize(),
		  url: "{{ route('admin.providers.store') }}",
		  type: "POST",
		 // dataType: 'json',
		  success: function (response) {
				//response = JSON.parse(res);
				if(response['status'] == 'success'){

				  swal({
					title: response['message'],
					icon: 'success'
				  });
				  $('#DoctorForm').trigger("reset");
				  setTimeout(function(){
				  swal.close();
				  window.location = "{{ route('admin.providers') }}"; }, 1000);
				}


		  },
		  error: function (data) {
			  let errorArr = [];
			  let errors = data.responseJSON.errors;
			  let emailErr = errors.email[0];
/* 			  $.each(errors, function(key, value) {

			  }) */;
			  //console.log('Error:', data);
			  swal({
				title: emailErr,
				icon: 'error'
			  })
			  $('#saveBtn').html('Save');
		  }
		});
	  }
    });

  }


</script>
</html>
@endsection