
@extends('layouts.app')
@section('content')
    <!-- BEGIN: Content -->
    <div class="content">
        <div class="intro-y flex items-center mt-8">
            <h2 class="text-lg font-medium mr-auto">Patient Detail</h2>
        </div>
        <!-- BEGIN: Profile Info -->
        <div class="intro-y box px-5 pt-5 mt-5">
            <div class="flex flex-col lg:flex-row border-b border-gray-200 dark:border-dark-5 pb-5 -mx-5">
                <div class="flex flex-1 px-5 items-center justify-center lg:justify-start">
                    <div class="w-20 h-20 sm:w-24 sm:h-24 flex-none lg:w-32 lg:h-32 image-fit relative">
                        @if($patient->image != null)
                            <img alt="" class="rounded-full" src="/uploads/users/{{$patient->image}}">
                        @else
                            <img alt="" class="rounded-full" src="/dist/images/user_icon.png">
                        @endif
                    </div>
                    <div class="ml-5">
                        <div class="w-24 sm:w-40 truncate sm:whitespace-normal font-medium text-lg">@if($patient->name != null) {{ucfirst($patient->name)}} @endif</div>
                        <!-- <div class="text-gray-600">Software Engineer</div> -->
                    </div>
                </div>
                <div class="flex mt-6 lg:mt-0 items-center lg:items-start flex-1 flex-col justify-center text-gray-600 dark:text-gray-300 px-5 border-l border-r border-gray-200 dark:border-dark-5 border-t lg:border-t-0 pt-5 lg:pt-0">
                    <div class="truncate sm:whitespace-normal flex items-center"><a href="mailto:{{$patient->email}}" class="custome_email_link"> <i data-feather="mail" class="w-4 h-4 mr-2"></i> @if($patient->email != null) {{$patient->email}} @endif</a> </div>
                    <div class="truncate sm:whitespace-normal flex items-center mt-3"><a href="tel:{{$patient->phone}}" class="custome_email_link"> <i data-feather="phone-call" class="w-4 h-4 mr-2"></i> @if($patient->phone != null) {{$patient->phone}} @endif</a> </div>
                    <div class="sm:whitespace-normal flex items-center mt-3"><a href="http://maps.google.com/?q={{ $patient->address }}" target="_blank" class="custome_email_link"> <i data-feather="home" class="w-4 h-4 mr-2"></i> @if($patient->address != null) {{$patient->address}} @endif</a> </div>
                </div>
                {{-- <div class="mt-6 lg:mt-0 flex-1 flex items-center justify-center px-5 border-t lg:border-0 border-gray-200 dark:border-dark-5 pt-5 lg:pt-0">
                    @if($patient->rmd_ad_id != null && $patient->rmd_ad_id != '')
                        <div class="text-center rounded-md w-20 py-3">
                            <div class="font-semibold text-theme-1 dark:text-theme-10 text-lg">{{$patient->rmd_ad_id}}</div>
                            <div class="text-gray-600">RMD ID</div>
                        </div>
                    @endif

                    @if($patient->google_adsense_id != null && $patient->google_adsense_id != '')
                        <div class="text-center rounded-md w-20 py-3">
                            <div class="font-semibold text-theme-1 dark:text-theme-10 text-lg">{{$patient->google_adsense_id}}</div>
                            <div class="text-gray-600">Google ID</div>
                        </div>
                    @endif

                    @if($patient->amazon_id != null && $patient->amazon_id != '')
                        <div class="text-center rounded-md w-20 py-3">
                            <div class="font-semibold text-theme-1 dark:text-theme-10 text-lg">{{$patient->amazon_id}}</div>
                            <div class="text-gray-600">Amazon ID</div>
                        </div>
                    @endif
                </div> --}}
            </div>
        </div>
        <!-- END: Profile Info -->
        <div class="tab-content mt-5">
            <div class="tab-content__pane active" id="profile">
                <div class="grid grid-cols-12 gap-6">
                    <!-- BEGIN: Daily Sales -->
                    <div class="intro-y box col-span-12 lg:col-span-4">
                        <div class="p-5">
                            <div class="relative flex items-center">
                                <div class="mr-auto">
                                    <span class="font-medium">First Name</span>
                                </div>
                                <div class="font-medium text-gray-600">@if($patient->email != null) {{ucfirst($patient->first_name)}} @endif</div>
                            </div>
                            <div class="relative flex items-center mt-5">
                                <div class="mr-auto">
                                    <span class="font-medium">Last Name</span>
                                </div>
                                <div class="font-medium text-gray-600">@if($patient->email != null) {{ucfirst($patient->last_name)}} @endif</div>
                            </div>
                            <div class="relative flex items-center mt-5">
                                <div class="mr-auto">
                                    <span class="font-medium">Gender</span>
                                </div>
                                <div class="font-medium text-gray-600">@if($patient->gender != null) {{ucfirst($patient->gender)}} @endif</div>
                            </div>
                            <div class="relative flex items-center mt-5">
                                <div class="mr-auto">
                                    <span class="font-medium">Date of Birth</span>
                                </div>
                                <div class="font-medium text-gray-600">@if($patient->dob != null) {{ucfirst($patient->dob)}} @endif</div>
                            </div>
                            <div class="relative flex items-center mt-5">
                                <div class="mr-auto">
                                    <span class="font-medium">Age</span>
                                </div>
                                <div class="font-medium text-gray-600">@if($patient->age != null) {{ucfirst($patient->age)}} @endif</div>
                            </div>
                            <div class="relative flex items-center mt-5">
                                <div class="mr-auto">
                                    <span class="font-medium">Insurance</span>
                                </div>
                                <div class="font-medium text-gray-600">@if($patient->insurance != null) {{ucfirst($patient->insurance)}} @endif</div>
                            </div>
                        </div>
                    </div>

                    <div class="intro-y box col-span-12 lg:col-span-4">
                        <div class="p-5">
                            <div class="relative flex items-center">
                                <div class="mr-auto">
                                    <span class="font-medium">RMD ID</span>
                                </div>
                                <div class="font-medium text-gray-600">@if($patient->rmd_ad_id != null) {{$patient->rmd_ad_id}} @endif</div>
                            </div>
                            <div class="relative flex items-center mt-5">
                                <div class="mr-auto">
                                    <span class="font-medium">Google Adsense ID</span>
                                </div>
                                <div class="font-medium text-gray-600">@if($patient->google_adsense_id != null) {{$patient->google_adsense_id}} @endif</div>
                            </div>
                            <div class="relative flex items-center mt-5">
                                <div class="mr-auto">
                                    <span class="font-medium">Bing Ads ID</span>
                                </div>
                                <div class="font-medium text-gray-600">@if($patient->bing_ads_id != null) {{$patient->bing_ads_id}} @endif</div>
                            </div>
                            <div class="relative flex items-center mt-5">
                                <div class="mr-auto">
                                    <span class="font-medium">Amazon ID</span>
                                </div>
                                <div class="font-medium text-gray-600">@if($patient->amazon_id != null) {{$patient->amazon_id}} @endif</div>
                            </div>
                            <div class="relative flex items-center mt-5">
                                <div class="mr-auto">
                                    <span class="font-medium">Household Income</span>
                                </div>
                                <div class="font-medium text-gray-600">@if($patient->household_income != null) ${{$patient->household_income}} @else $0 @endif</div>
                            </div>
                            <div class="relative flex items-start mt-5">
                                <div class="mr-auto">
                                    <span class="font-medium">Monthly Maintenance Fund</span>
                                </div>
                                <div class="font-medium text-gray-600 ml-8 text-right">@if($patient->dmm_fund != null) ${{$patient->dmm_fund}} @else $0 @endif</div>
                            </div>
                        </div>
                    </div>

                    <div class="intro-y box col-span-12 lg:col-span-4">
                        <div class="p-5">
                        <div class="relative flex items-center">
                                <div class="mr-auto">
                                    <span class="font-medium">Status</span>
                                </div>
                                <div class="font-medium text-gray-600">@if($patient->status == 1) Active @else Blocked @endif</div>
                            </div>
                            <div class="relative flex items-center mt-5">
                                <div class="mr-auto">
                                    <span class="font-medium">Smoker</span>
                                </div>
                                <div class="font-medium text-gray-600">@if($patient->smokers != null) {{ucfirst($patient->smokers)}} @endif</div>
                            </div>
                            <div class="relative flex items-start mt-5">
                                <div class="mr-auto">
                                    <span class="font-medium">Diagnosis</span>
                                </div>
                                <div class="font-medium text-gray-600 ml-8 text-right">@if($diagnosis != null && $diagnosis != '') {{ucfirst($diagnosis)}} @endif</div>
                            </div>
                            <div class="relative flex items-start mt-5">
                                <div class="mr-auto">
                                    <span class="font-medium">Allergies</span>
                                </div>
                                <div class="font-medium text-gray-600 ml-8 text-right">@if($patient->allergies != null) {{ucfirst($patient->allergies)}} @endif</div>
                            </div>
                            <div class="relative flex items-start mt-5">
                                <div class="mr-auto">
                                    <span class="font-medium">Interests</span>
                                </div>
                                <div class="font-medium text-gray-600 ml-8 text-right"></div>
                            </div>
                        </div>
                    </div>

                    <div class="intro-y col-span-12 lg:col-span-4">
                        <div class="">
                            <div class="relative flex items-center">
                                <button type="button" class="button w-24 mr-1 mb-2 btn_dark ml-2" onclick="javascript:location.href = '/admin/patients'">Back</button>
                            </div>
                        </div>
                    </div>
                    <!-- END: Daily Sales -->
                </div>
            </div>
        </div>
    </div>
    <!-- END: Content -->
@endsection