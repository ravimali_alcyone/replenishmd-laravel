@extends('layouts.app')

@section('content')
<div class="intro-y flex flex-col sm:flex-row items-center mt-8">
            @if($page == 'asynchronous_visits')
				<h2 class="text-lg font-medium mr-auto">Asynchronous Telemedicine Visits</h2>
			@elseif($page == 'synchronous_visits')
				<h2 class="text-lg font-medium mr-auto">Synchronous Telemedicine Visits</h2>
			@elseif($page == 'concierge_visits')
				<h2 class="text-lg font-medium mr-auto">Concierge Visits</h2>				
			@endif

</div>

<div class="grid grid-cols-12 gap-6 mt-5">
	@if($services && $services->count()>0)
	<div class="intro-y col-span-12 items-center">
		<div class="col-span-12 ml-2">
			<button class="button w-30 inline-block mr-5 mb-2 border border-theme-1 text-theme-1 dark:border-theme-10 dark:text-theme-10 services active" id="service_all" onclick="setServiceType(this,'');">All</button>
			@foreach($services as $service)
			<button class="button w-30 inline-block mr-1 mb-2 border border-theme-1 text-theme-1 dark:border-theme-10 dark:text-theme-10 services" id="service_{{$service->id}}" onclick="setServiceType(this,{{$service->id}});">{{$service->name}}</button>
			@endforeach
		</div>
	</div>
	@endif

	<div class="intro-y datatable-wrapper box p-5 col-span-12 overflow-auto lg:overflow-hidden" id="patients_table">
		<table id="myTable" class="table table-report table-report--bordered display w-full sub_admin_table dataTable mt-0">
			<thead>
				<tr class="intro-x">
					<th class="border-b-2">S.No.</th>
					<th class="border-b-2">Patient Name</th>
					<th class="border-b-2">Treatment Looking for</th>
					<th class="border-b-2">Provider</th>
					<th class="border-b-2">Visit Status</th>
					<th class="border-b-2">Requested at</th>
					<th class="border-b-2">Action</th>
				</tr>
			</thead>
			<tbody></tbody>
		</table>
	</div>
	<!-- loader  -->
	<div class="custom_loader">
		<img class="ajax_loader" src="/dist/images/loading.gif"/>
	</div>
</div>

<script type="text/javascript">
	var table;
	var url;
	var service_id = '';

 	$(document).ready(function() {
		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});

		fill_datatable();
		$("#myTable tbody tr").addClass('intro-x');
  	});

	function fill_datatable(service_id = ''){
		<?php if($page == 'asynchronous_visits'){?>
			url = "{{ route('admin.asynchronous_visits') }}";
		<?php }elseif($page == 'synchronous_visits'){?>
			url = "{{ route('admin.synchronous_visits') }}";
		<?php }elseif($page == 'concierge_visits'){?>
			url = "{{ route('admin.concierge_visits') }}";			
		<?php } ?>

		table = $('#myTable').DataTable({
			processing: true,
			serverSide: true,
			responsive: true,
			paging: true,
			ordering: false,
			info: false,
			displayLength: 10,
			language: {
				processing: '<img src="/dist/images/loading.gif"/>'
			},
			initComplete:function( settings, json){
				$(".select2").select2({
					minimumResultsForSearch: -1
				});
			},
			lengthMenu: [
			[10, 25, 50, -1],
			[10, 25, 50, "All"]
			],

			ajax:{
				url: url,
				data:{service_id:service_id}
			},
			columns: [
				{data: 'DT_RowIndex', name: 'DT_RowIndex'},
				{data: 'patient_name', name: 'patient_name'},
				{data: 'treatment', name: 'treatment'},
				{data: 'provider_name', name: 'provider_name'},
				{data: 'visit_status', name: 'visit_status'},
				{data: 'datetime', name: 'datetime'},
				{data: 'action', name: 'action', orderable: false, searchable: false},
			]
		});
		
		table.on('page.dt', function() {
		  $('html, body').animate({
			scrollTop: $(".dataTables_wrapper").offset().top
		   }, 'slow');
		});
	}

	function setServiceType(e, id) {

		if($(e).hasClass('active')){
			$(e).removeClass('active');
		}else{
			$(e).parent().find('button.services').removeClass('active');
			$(e).addClass('active');
		}

		service_id = id;
		$('#myTable').DataTable().destroy();
		fill_datatable(service_id);
	}

/*     function deleteRow(id) {
		swal({
			title: "Are you sure to delete?",
			text: "",
			icon: 'warning',
			buttons: {
			  cancel: true,
			  delete: 'Yes, Delete It'
			}
		  }).then((isConfirm) => {
			if (!isConfirm) {
				return false;
			} else {
				$.ajax({
					type: "DELETE",
					url: "{{ env('APP_URL').'/admin/online_visits/destroy' }}"+'/'+id,
					success: function (response) {
						if(response['status'] == 'success'){
						swal({
							title: response['message'],
							icon: 'success'
						});
						setTimeout(function(){
						swal.close();
						}, 1000);
						}
						table.draw();
					},
					error: function (data) {
						console.log('Error:', data);
						swal({
							title: 'Error Occured',
							icon: 'error'
						});
					}
				});
			}
        });
    } */

/* 	function changeStatus(id, value) {
		$.ajax({
			url: "{{ url('admin/online_visits/change_status') }}",
			type: "POST",
			data: {
				'id': id,
				'visit_status': value
			},
			beforeSend:function() {
				$(".custom_loader").css('display', 'flex');
			},
			success: function(response) {
				setTimeout(() => {
					if (response.status == 'success') {
						$(".custom_loader").hide();
					}
				}, 1000);
			}
		});
	} */

	// $(document).ready(function() {
	// 	$(".select2").select2();
  	// });
	

</script>
</html>
@endsection