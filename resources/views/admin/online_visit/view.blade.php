@extends('layouts.app')
@section('content')
    <!-- BEGIN: Content -->
    <div class="content">
        <div class="intro-y flex items-center mt-8">
            @if($online_visit->visit_type == 1)<h2 class="text-lg font-medium mr-auto">Asynchronous Telemedicine Visit Detail</h2>
			@elseif($online_visit->visit_type == 2)<h2 class="text-lg font-medium mr-auto">Synchronous Telemedicine Visit Detail</h2>
			@elseif($online_visit->visit_type == 3)<h2 class="text-lg font-medium mr-auto">Concierge Visit Detail</h2>
			@endif
        </div>
        <!-- BEGIN: Profile Info -->
        <div class="intro-y box px-5 pt-5 mt-5">
            <div class="flex flex-col lg:flex-row border-b border-gray-200 dark:border-dark-5 pb-5 -mx-5">
                <div class="flex flex-1 px-5 items-center justify-center lg:justify-start">
                    <div class="w-20 h-20 sm:w-24 sm:h-24 flex-none lg:w-32 lg:h-32 image-fit relative">
                        @if($online_visit->image != null)
                            <img alt="" class="rounded-full" src="{{ env('APP_URL')}}/{{$online_visit->image }}">
                        @else
                            <img alt="" class="rounded-full" src="{{ asset('dist/images/user_icon.png') }}">
                        @endif
                    </div>
                    <div class="ml-5">
                        <div class="w-24 sm:w-40 truncate sm:whitespace-normal font-medium text-lg">@if($online_visit->patient_name != null) {{ucfirst($online_visit->patient_name)}} @endif</div>
                        <div class="text-gray-600">(Patient)</div>
                    </div>
                </div>
                <div class="flex mt-6 lg:mt-0 items-center lg:items-start flex-1 flex-col justify-center text-gray-600 dark:text-gray-300 px-5 border-l border-r border-gray-200 dark:border-dark-5 border-t lg:border-t-0 pt-5 lg:pt-0">
                    <div class="truncate sm:whitespace-normal flex items-center"> <i data-feather="mail" class="w-4 h-4 mr-2"></i> @if($online_visit->email != null) {{$online_visit->email}} @endif </div>
                    <div class="truncate sm:whitespace-normal flex items-center mt-3"> <i data-feather="phone-call" class="w-4 h-4 mr-2"></i> @if($online_visit->phone != null) {{$online_visit->phone}} @endif </div>
                    <div class="sm:whitespace-normal flex items-center mt-3"> <i data-feather="home" class="w-4 h-4 mr-2"></i> @if($online_visit->address != null) {{$online_visit->address}} @endif </div>
                </div>
            </div>
        </div>
        <!-- END: Profile Info -->
        <div class="tab-content mt-5">
            <div class="tab-content__pane active" id="profile">
                <div class="grid grid-cols-12 gap-6">
                    <!-- BEGIN: Daily Sales -->
                    <div class="intro-y box col-span-12 lg:col-span-4">
                        <div class="p-5">
                            <div class="relative flex items-center">
                                <div class="mr-auto">
                                    <a href="" class="font-medium">Patient Name</a>
                                </div>
                                <div class="font-medium text-gray-600">@if($online_visit->patient_name != null) <a href="{{env('APP_URL')}}/admin/patients/view/{{$online_visit->patient_id}}" target="_blank">{{ucfirst($online_visit->patient_name)}}</a> @endif</div>
                            </div>						
                            <div class="relative flex items-center mt-5">
                                <div class="mr-auto">
                                    <a href="" class="font-medium">Gender</a>
                                </div>
                                <div class="font-medium text-gray-600">@if($online_visit->gender != null) {{ucfirst($online_visit->gender)}} @endif</div>
                            </div>
                            <div class="relative flex items-center mt-5">
                                <div class="mr-auto">
                                    <a href="" class="font-medium">Date of Birth</a>
                                </div>
                                <div class="font-medium text-gray-600">@if($online_visit->dob != null) {{ucfirst($online_visit->dob)}} @endif</div>
                            </div>
                            <div class="relative flex items-center mt-5">
                                <div class="mr-auto">
                                    <a href="" class="font-medium">Age</a>
                                </div>
                                <div class="font-medium text-gray-600">
								<?php 
								if($online_visit->age != null){
									echo ucfirst($online_visit->age); 
								}else{ 
								   $dob=ucfirst($online_visit->dob);
								   $diff = (date('Y') - date('Y',strtotime($dob)));
								   echo $diff;
								} ?></div>
                            </div>
                        </div>
                    </div>

                    <div class="intro-y box col-span-12 lg:col-span-8">
                        <div class="p-5">
                            <div class="relative flex items-center">
                                <div class="mr-auto">
                                    <a href="" class="font-medium">Treatment Looking For</a>
                                </div>
                                <div class="font-medium text-gray-600">@if($online_visit->service_name != null) {{$online_visit->service_name}} @endif</div>
                            </div>
                            <div class="relative flex items-center mt-5">
                                <div class="mr-auto">
                                    <a href="" class="font-medium">Visit Type</a>
                                </div>
                                <div class="font-medium text-gray-600">
									@if($online_visit->visit_type == 1) Asynchronous Telemedicine 
									@elseif($online_visit->visit_type == 2) Synchronous Telemedicine
									@elseif($online_visit->visit_type == 3) Concierge
									@endif
								</div>
                            </div>
                            <div class="relative flex items-center mt-5">
                                <div class="mr-auto">
                                    <a href="" class="font-medium">Provider Assigned</a>
                                </div>
                                <div class="font-medium text-gray-600">@if($online_visit->provider_name != null) <a href="{{env('APP_URL')}}/admin/providers/view/{{$online_visit->provider_id}}" target="_blank"> {{$online_visit->provider_name}}</a> @endif</div>
                            </div>
                            <div class="relative flex items-center mt-5">
                                <div class="mr-auto">
                                    <a href="" class="font-medium">Visit Status</a>
                                </div>
                                <div class="font-medium text-gray-600">
									<span>{{ $online_visit->visit_status }}</span>
								</div>
                            </div>
                            <div class="relative flex items-center mt-5">
                                <div class="mr-auto">
                                    <a href="" class="font-medium">Requested at</a>
                                </div>
                                <div class="font-medium text-gray-600">@if($online_visit->created_at != null) {{date(env('DATE_FORMAT_PHP_H'),strtotime($online_visit->created_at))}} @endif</div>
                            </div>
                        </div>
                    </div>
                    <!-- END: Daily Sales -->
                </div>
            </div>
			<?php if($status_timeline && $status_timeline->count() > 0){?>
			<!-- BEGIN: Latest Tasks -->
			<div class="intro-y box mt-5">
				<div class="flex items-center px-5 py-5 sm:py-0 border-b border-gray-200 dark:border-dark-5">
					<h2 class="font-medium text-base mr-auto p-2">
						Status Timeline
					</h2>
				</div>
				
				<div class="p-5">
					<div class="tab-content">
						<div class="tab-content__pane active" id="latest-tasks-new">
						<?php foreach($status_timeline as $row){?>
							<div class="flex items-center mt-5">
								<div class="border-l-2 border-theme-1 pl-4">
									{{ $row->new_visit_status }}
									<div class="text-gray-600">{{ date(env('DATE_FORMAT_PHP_HA'),strtotime($row->created_at)) }}</div>
								</div>
								<input class="input input--switch ml-auto border" type="checkbox">
							</div>
						<?php } ?>
						</div>
					</div>
				</div>
			</div>
			<!-- END: Latest Tasks -->			
			<?php } ?>
        </div>
		<div class="intro-y col-span-12 lg:col-span-4 mt-5">
			<div class="">
				<div class="relative flex items-center">
            @if($online_visit->visit_type == 1)
				<button type="button" class="button w-24 mr-1 mb-2 btn_dark" onclick="javascript:location.href = '/admin/asynchronous_visits'">Back</button>
			@elseif($online_visit->visit_type == 2)
				<button type="button" class="button w-24 mr-1 mb-2 btn_dark" onclick="javascript:location.href = '/admin/synchronous_visits'">Back</button>
			@elseif($online_visit->visit_type == 3)
				<button type="button" class="button w-24 mr-1 mb-2 btn_dark" onclick="javascript:location.href = '/admin/concierge_visits'">Back</button>				
			@endif				
					
				</div>
			</div>
		</div>
					
		<!-- loader  -->
		<div class="custom_loader">
			<img class="ajax_loader" src="/dist/images/loading.gif"/>
		</div>			
    </div>
    <!-- END: Content -->
	
<script>
/* 	function changeStatus(id, value) {
		$.ajax({
			url: "{{ url('admin/online_visits/change_status') }}",
			type: "POST",
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},			
			data: {
				'id': id,
				'visit_status': value
			},
			beforeSend:function() {
				$(".custom_loader").css('display', 'flex');
			},
			success: function(response) {
				setTimeout(() => {
					if (response.status == 'success') {
						$(".custom_loader").hide();
					}
				}, 1000);
			}
		});
	} */
</script>	
@endsection