@extends('layouts.app')
   
@section('content')

	<div class="grid grid-cols-12 gap-6">
		<div class="col-span-12 xxl:col-span-12 grid grid-cols-12 gap-6">
			<!-- BEGIN: General Report -->
			<div class="col-span-12 mt-8">
				<div class="intro-y flex items-center h-10">
					<h2 class="text-lg font-medium truncate mr-5">
						General Report
					</h2>
					<a href="" class="ml-auto flex text-theme-1 dark:text-theme-10"> <i data-feather="refresh-ccw" class="w-4 h-4 mr-3"></i> Reload Data </a>
				</div>
				<div class="grid grid-cols-12 gap-6 mt-5">
					<div class="col-span-12 sm:col-span-6 xl:col-span-2 intro-y">
						<div class="report-box zoom-in">
							<a href="{{ URL('admin/patients') }}" >
								<div class="box p-5">
									<div class="flex">
										<i data-feather="users" class="report-box__icon text-theme-9"></i> 
										<div class="ml-auto">
											<!-- <div class="report-box__indicator bg-theme-9 tooltip cursor-pointer" title="22% Higher than last month"> 22% <i data-feather="chevron-up" class="w-4 h-4"></i> </div> -->
										</div>
									</div>
									<div class="text-3xl font-bold leading-8 mt-6">{{ count($total_patients) }}</div>
									<div class="text-base text-gray-600 mt-1">Total Patients</div>
								</div>
							</a>
						</div>
					</div>				
					<div class="col-span-12 sm:col-span-6 xl:col-span-2 intro-y">
						<div class="report-box zoom-in">
							<a href="{{ URL('admin/reports/prescription_products') }}" >
								<div class="box p-5">
									<div class="flex">
										<i data-feather="shopping-cart" class="report-box__icon text-theme-10"></i> 
										<div class="ml-auto">
											<!-- <div class="report-box__indicator bg-theme-9 tooltip cursor-pointer" title="33% Higher than last month"> 33% <i data-feather="chevron-up" class="w-4 h-4"></i> </div> -->
										</div>
									</div>
									<div class="text-3xl font-bold leading-8 mt-6">{{ count($total_sales) }}</div>
									<div class="text-base text-gray-600 mt-1">Item Sales</div>
								</div>
							</a>
						</div>
					</div>
					<div class="col-span-12 sm:col-span-6 xl:col-span-2 intro-y">
						<div class="report-box zoom-in">
							<div class="box p-5">
								<div class="flex">
									<i data-feather="credit-card" class="report-box__icon text-theme-11"></i> 
									<div class="ml-auto">
										<!-- <div class="report-box__indicator bg-theme-6 tooltip cursor-pointer" title="2% Lower than last month"> 2% <i data-feather="chevron-down" class="w-4 h-4"></i> </div> -->
									</div>
								</div>
								<div class="text-3xl font-bold leading-8 mt-6">3.521</div>
								<div class="text-base text-gray-600 mt-1">New Orders</div>
							</div>
						</div>
					</div>
					<div class="col-span-12 sm:col-span-6 xl:col-span-2 intro-y">
						<div class="report-box zoom-in">
							<a href="{{ URL('admin/treatment_medicines') }}" >
								<div class="box p-5">
									<div class="flex">
										<i data-feather="monitor" class="report-box__icon text-theme-12"></i> 
										<div class="ml-auto">
											<!-- <div class="report-box__indicator bg-theme-9 tooltip cursor-pointer" title="12% Higher than last month"> 12% <i data-feather="chevron-up" class="w-4 h-4"></i> </div> -->
										</div>
									</div>
									<div class="text-3xl font-bold leading-8 mt-6"> {{ count($total_products) }} </div>
									<div class="text-base text-gray-600 mt-1">Total Products</div>
								</div>
							</a>
						</div>
					</div>
					<div class="col-span-12 sm:col-span-6 xl:col-span-2 intro-y">
						<div class="report-box zoom-in">
							<div class="box p-5">
								<div class="flex">
									<i data-feather="user" class="report-box__icon text-theme-9"></i> 
									<div class="ml-auto">
										<!-- <div class="report-box__indicator bg-theme-9 tooltip cursor-pointer" title="22% Higher than last month"> 22% <i data-feather="chevron-up" class="w-4 h-4"></i> </div> -->
									</div>
								</div>
								<div class="text-3xl font-bold leading-8 mt-6">152.000</div>
								<div class="text-base text-gray-600 mt-1">Unique Visitor</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- END: General Report -->
		</div>
		
	</div>

@endsection