@extends('layouts.app')

@section('content')
<div class="intro-y flex flex-col sm:flex-row items-center mt-8">
	<h2 class="text-lg font-medium mr-auto">Category List</h2>
	<div class="w-full sm:w-auto flex mt-4 sm:mt-0">
		<div class="intro-y col-span-12 flex flex-wrap sm:flex-no-wrap items-center mt-2">
			<a href="{{ url('admin/blogs/add_post_type/0') }}" class="button w-32 mr-2 mb-2 flex items-center justify-center btn_white">Add New</a>
		</div>
	</div>
</div>

<div class="grid grid-cols-12 gap-6 mt-5">
	

	<div class="intro-y datatable-wrapper box p-5 col-span-12 overflow-auto lg:overflow-hidden" id="patients_table">
		<table id="myTable" class="table table-report table-report--bordered display w-full sub_admin_table dataTable mt-0">
			<thead>
				<tr class="intro-x">
					<th class="border-b-2">S.No.</th>
					<th class="border-b-2">Type</th>
					<th class="border-b-2">Status</th>
					<th class="border-b-2">Action</th>
				</tr>
			</thead>
			<tbody></tbody>
		</table>
	</div>
</div>

<script type="text/javascript">
var table;
$(document).ready(function() {
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});

	fill_datatable();
	function fill_datatable(filter_category = '', publish_status = ''){
		
		table = $('#myTable').DataTable({
			processing: true,
			serverSide: true,
			responsive: true,
			paging: true,
			ordering: false,
			info: false,
			displayLength: 10,
			language: {
				processing: '<img src="/dist/images/loading.gif"/>'
			},				
			lengthMenu: [
			[10, 25, 50, -1],
			[10, 25, 50, "All"]
			],
			ajax:{
				url: "{{ route('admin.blogs.blog_post_type') }}",
				data:{"filter_category":'',"publish_status":'' }
			},
			columns: [
				{data: 'DT_RowIndex', name: 'DT_RowIndex'},
				{data: 'type', name: 'type'},						
				{data: 'status', name: 'status'},
				{data: 'action', name: 'action', orderable: false, searchable: false},
			]
		});
	}

	$('#filter').click(function(){
		
		$('#myTable').DataTable().destroy();
		fill_datatable();
	});

	$('#reset').click(function(){
		$(".select2-selection__rendered").attr('title', 'Any').html('Any');
		$('#myTable').DataTable().destroy();
		fill_datatable();
	});

	$("#myTable tbody tr").addClass('intro-x');
});

/*************************************************************************************************/

 function deleteRow(id) {
		swal({
			title: "Are you sure to delete?",
			text: "",
			icon: 'warning',
			buttons: {
			  cancel: true,
			  delete: 'Yes, Delete It'
			}
		  }).then((isConfirm) => {
			if (!isConfirm) {
				return false;
			} else {
				$.ajax({
					headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					},
					type: "DELETE",
					url: "{{ env('APP_URL').'/admin/category/destroy' }}"+'/'+id,
					success: function (response) {
						if(response['status'] == 'success'){
						swal({
							title: response['message'],
							icon: 'success'
						});
						setTimeout(function(){
						swal.close();
						}, 1000);
						}
						table.draw();
					},
					error: function (data) {
						console.log('Error:', data);
						swal({
							title: 'Error Occured',
							icon: 'error'
						});
					}
				});
			}
        });
    }

	function changeStatus(id) {
		$.ajax({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			url: "{{ url('admin/blogs/change_category_status') }}",
			type: "POST",
			data: {'id': id},
			success: function(response) {
				console.log(response);
			}
		});
	}
</script>
</html>
@endsection