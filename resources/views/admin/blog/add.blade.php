@extends('layouts.app')
@section('content')
	<style>
	.daterangepicker.ltr.single.opensright.show-calendar{
		display:none!important;
	}
	#coverImage .image_preview .pip img, #headerImage .image_preview .pip img {
		height: 360px;
		width: 100%;
		border: 3px solid black;
	}	
	
	.crop-preview {
		overflow: hidden;
		width: 160px; 
		height: 160px;
		margin: 10px;
		border: 1px solid red;
	}

	.modal-lg{
		max-width: 1000px !important;
	}

	.overlay {
	  position: absolute;
	  bottom: 10px;
	  left: 0;
	  right: 0;
	  background-color: rgba(255, 255, 255, 0.5);
	  overflow: hidden;
	  height: 0;
	  transition: .5s ease;
	  width: 100%;
	}

	.image_area:hover .overlay {
	  height: 50%;
	  cursor: pointer;
	}	
		
	 .list-unstyled {
            padding-left: 0;
            list-style: none;
        }
     table {
            border-collapse: collapse;
        }
     .dropdown-menu {
            position: absolute;
            top: 100%;
            left: 0;
            z-index: 1000;
            display: none;
            float: left;
            min-width: 10rem;
            padding: 0.5rem 0;
            margin: 0.125rem 0 0;
            font-size: 1rem;
            color: #212529;
            text-align: left;
            list-style: none;
            background-color: #fff;
            background-clip: padding-box;
            border: 1px solid rgba(0, 0, 0, .15);
            border-radius: 0.25rem;
        }
     .interface-interface-skeleton__sidebar {
            z-index: 1 !important;        
    }
	</style>
	<!-- Font Awesome -->
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
	<!-- Laraberg Editor -->
	<script src="https://unpkg.com/react@16.8.6/umd/react.production.min.js"></script>
  	<script src="https://unpkg.com/react-dom@16.8.6/umd/react-dom.production.min.js"></script> 
  	<script src="{{ asset('vendor/laraberg/js/laraberg.js') }}"></script>
  	<script src="{{ asset('js/admin-app.js') }}" defer></script>
	<link rel="stylesheet" href="{{asset('vendor/laraberg/css/laraberg.css')}}">
    <link rel="stylesheet" href="{{asset('css/admin-app.css')}}">
	
	<!-- Crop image -->
	<link rel="stylesheet" href="https://unpkg.com/dropzone/dist/dropzone.css" />
	<link href="https://unpkg.com/cropperjs/dist/cropper.css" rel="stylesheet"/>
	<script src="https://unpkg.com/dropzone"></script>
	<script src="https://unpkg.com/cropperjs"></script>	

	<div class="intro-y box mt-5">
		<div class="flex flex-col sm:flex-row items-center p-5 border-b border-gray-200 dark:border-dark-5">
			<h2 class="font-medium text-base mr-auto"><a href="{{env('APP_URL').'/admin/blogs'}}">Blogs List</a> > @if($blog) {{ 'Edit' }} @else Add New @endif Blog</h2>
		</div>
		<div class="intro-y datatable-wrapper box p-5 col-span-12 overflow-auto lg:overflow-hidden">
			<form id="blogForm">
				<fieldset class="uk-fieldset">
					<div class="laraberg-sidebar">
						<textarea name="excerpt" placeholder="Excerpt" rows="10"></textarea>
					</div>
					<div class="uk-margin">
						<input id="article-title" type="text" class="uk-input uk-form-large {{ $errors->get('title') ? 'uk-form-danger' : '' }}" name="name" placeholder="Title" value="<?php if($blog){ echo $blog->title;}?>" />
					</div>
					<div class="uk-margin">
						<textarea name="description" id="description" hidden><?php if($blog){ echo $blog->blog_content; }?></textarea>
					</div>
				</fieldset>
				
				<div class="grid grid-cols-12 gap-6 mt-5">
					<div class="col-span-9">
						<div class="intro-y col-span-12">
							<div class="grid grid-cols-12 gap-6">
							
								<div class="col-span-6">
									<label>Category</label>
									<div class="mt-2">
									<select id="category_id" name="category" class="select2 w-full" onchange="getCategoryTopics(this)" required>
										<option value="">--Select--</option>
										@if($categories && $categories->count() > 0)
											@foreach($categories as $category)
												<option value="{{ $category->id }}" <?php if($blog && $blog->blog_category_id == $category->id){ echo 'selected';}?> >{{ $category->name }}</option>
											@endforeach
										@endif
									</select>
									</div>
								</div>
								
								<div class="col-span-6">
									<label>Tags (Seprate with comma)</label>
									<div class="mt-2">
									<input type="text" class="input w-full border" name="tags" id="tags" value="<?php if($blog){ echo $blog->post_tags; }?>"/>
									</div>
								</div>								
																
							</div>
						</div>


						<div class="intro-y col-span-12 mt-5">
							<div class="grid grid-cols-12 gap-6">							
								<div class="col-span-12">
									<label>Topics</label>
									<div class="mt-2">
									<select id="topic_ids" name="topics[]" class="select2 w-full" multiple>
										@if($topics && $topics->count() > 0)
											@php 
												$cat_topics  = $blog && $blog->category_topic_ids != '' ? explode(',' , $blog->category_topic_ids) : array();
											@endphp
											
											@foreach($topics as $topic)
												<option value="{{ $topic->id }}" @if(in_array($topic->id,$cat_topics)) selected @endif >{{ $topic->topic }}</option>
											@endforeach
										@endif
									</select>
									</div>
								</div>
							</div>
						</div>

						
						<div class="intro-y col-span-12 mt-5">
							<div class="grid grid-cols-12 gap-6">							
								<div class="col-span-6">
									<label>Permalink</label>
									<input type="text" name="param_url" class="input w-full border mt-2" placeholder="{{env('APP_URL')}}/blogs/" disabled>
								</div>
								
								<div class="col-span-6 mt-5">
									<input type="text" name="slug" value="<?php if($blog){ echo $blog->slug;}?>" class="input w-full border mt-2" placeholder="Permalink" id="slug" required>
								</div>
							</div>
						</div>
						
						<div class="intro-y col-span-12 mt-5">
							<div class="grid grid-cols-12 gap-6">	

								<div class="col-span-4" id="headerImage">
									<label>Featured(Header) Image</label>
									<input type="file" name="header_image" class="input w-full border mt-2" id="blog_header_image" >
									<div class="image_preview mt-3">
										@if($blog && $blog->header_image != '')
											
											<input type="hidden" name="old_header_image" value="{{ $blog->header_image }}">
											<span class="pip"><img class="imageThumb" src="/{{ $blog->header_image }}"></span>
										@endif
									</div>
								</div>	
								
								<div class="col-span-1"></div>
								
								<div class="col-span-4" id="coverImage">
									<label>Cover Image</label>
									<input type="file" name="image" class="input w-full border mt-2" id="cover_image" >
									<div class="image_preview mt-3">
										@if($blog && $blog->image != '')
											
											<input type="hidden" name="old_image" value="{{ $blog->image }}">
											<span class="pip"><img class="imageThumb" src="/{{ $blog->image }}"></span>
										@endif
									</div>
								</div>	
						
							</div>							
						</div>							
					</div>
					
					<div class="col-span-3 p-3 bg-gray-100">
						<div class="intro-y col-span-12">
							<div class="grid grid-cols-12  gap-6">
								<div class="col-span-12">
									<label>Visibility</label>
									<div class="mt-2">
										<input type="radio" id="" name="visibility" value="public" <?php if(!$blog){ echo 'checked';} ?> class="mr-3" <?php if($blog && $blog->post_visibility==1){ echo "checked"; } ?> ><label for="img" class="mr-4">Public</label>
										<input type="radio" id="" name="visibility" value="private" class="mr-4" <?php if($blog && $blog->post_visibility==0){ echo "checked"; } ?> ><label for="img">Private</label>
									</div>
								</div>
								<div class="col-span-12">
									<label>Publish</label>
									<div class="mt-2">
										<input type="radio" id="publish_1" name="publish" value="immediate" <?php if(!$blog){ echo 'checked';} ?> class="mr-2" <?php if($blog && $blog->post_publish==1){ echo "checked"; } ?> onchange="setSchedulePublish(this,1);" ><label for="publish_1" class="mr-4">Immediate</label>
										
										<input type="radio" id="publish_0" name="publish" value="draft" class="mr-2" <?php if($blog && $blog->post_publish==0){ echo "checked"; } ?> onchange="setSchedulePublish(this,0);"><label for="publish_0" class="mr-4">Draft</label>
										
										<input type="radio" id="publish_2" name="publish" value="schedule" class="mr-2" <?php if($blog && $blog->post_publish==2){ echo "checked"; } ?> onchange="setSchedulePublish(this,2);" ><label for="publish_2">Schedule</label>
										
									</div>
									<div id="schedulePostDiv" class="mt-2" style="<?php if($blog && $blog->post_publish == 2){ echo 'display:block'; }else{ echo 'display:none;';} ?>" >
										<label>Schedule Publish at</label>
										<input type="text" id="scheduled_at" name="scheduled_at" value="<?php if($blog && $blog->scheduled_at != NULL){ echo date('d/m/Y H:i:s', strtotime($blog->scheduled_at));}?>" autocomplete="off" data-DateTimePicker="<?php if($blog && $blog->scheduled_at != NULL){ echo date('d/m/Y H:i:s', strtotime($blog->scheduled_at));}?>" class="input w-full border mt-2"  <?php if($blog && $blog->post_publish==2){ echo "required"; } ?> >
									</div>									
								</div>
								<div class="col-span-12">
									<label>Allow Comment</label>
									<div class="mt-2">
										<input type="checkbox" name="alw_comment" id="alw_comment" value="1" class="form-checkbox" <?php if(!$blog){ echo 'checked';} ?> <?php if($blog && $blog->post_comment==1){ echo "checked"; } ?>>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="grid grid-cols-12 gap-6 mt-12">
					<div class="intro-y col-span-12 lg:col-span-12">
						<input type="hidden" name="id" value="<?php if($blog){ echo $blog->id;}?>"/>
						<input type="hidden" name="page" value="<?php if($blog){ echo 'edit';} else { echo 'add'; }?>"/>
						<button type="submit" id="saveBtn" class="button button--md w-24 mr-1 mb-2 btn_blue">Save</button>
						<button type="reset" class="button button--md w-24 mr-1 mb-2 btn_red" onclick="location.href = '{{ route('admin.blogs') }}';">Cancel</button>
					</div>
				</div>
			</form>
		</div>
	</div>

 
 <div class="modal" id="cropImageModal">
     <div class="modal__content relative modal__content--xl"> <a data-dismiss="modal" href="javascript:;" class="absolute right-0 top-0 mt-3 mr-3"> <i data-feather="x" class="w-8 h-8 text-gray-500"></i> </a>
         <div class="p-5 text-center">
		 
			<div class="intro-y col-span-12">
				<div class="grid grid-cols-12 gap-6">			 
					<div class="col-span-8">
						<img src="" id="sample_image" />
					</div>
					<div class="col-span-4">
						<div class="crop-preview"></div>
					</div>
				</div>
			</div>	

			
         </div>
         <div class="px-5 pb-8 text-center"> 
			<button id="crop" type="button" class="button w-24 bg-theme-3 text-white">Crop</button> 
			<button id="closebtn" type="button" data-dismiss="modal" class="button w-24 bg-theme-1 text-white">Cancel</button> 
		</div>
     </div>
 </div>
 
<script type="text/javascript">

	$("#scheduled_at").datetimepicker({
		format: 'DD/MM/YYYY HH:mm:ss',
		showClose: true,
		minDate: new Date(),
	});
	
	$('#scheduled_at').data("DateTimePicker").show();

	function setSchedulePublish(e,val){
		if($(e).is(":checked") && val == '2'){ //schedule
			$("#schedulePostDiv").show();
			$("#scheduled_at").attr('required',true);
		}else{
			$("#schedulePostDiv").hide();
			$("#scheduled_at").val('');
			$("#scheduled_at").attr('required',false);
		}
	}


	var cropImageData = '';
	
$(document).ready(function(){
	
	var $modal = $('#cropImageModal');
	var image = document.getElementById('sample_image');
	var cropper;
	
	$('#cover_image').change(function(event){
		cropImageData = '';
		$('#coverImage .pip').remove();
		var files = event.target.files;

		var done = function(url){
		
			let x = "<span class='pip'>" +
				"<img class='imageThumb' src=\"" + url + "\"/>" +
				"</span>";
			$("#coverImage .image_preview").append(x);
			$("#coverImage.remove").click(function(){
				$(this).parent("#coverImage .pip").remove();
			});			
			
			if(cropper){
				cropper.destroy();
				cropper = null;
			}
		
			image.src = url;
			$modal.modal('show');
			
			cropper = new Cropper(image, {
				aspectRatio: 1,
				viewMode: 3,
				preview:'.crop-preview'
			});			
		};

		if(files && files.length > 0)
		{
			reader = new FileReader();
			reader.onload = function(event)
			{
				done(reader.result);
			};
			reader.readAsDataURL(files[0]);
		}
	});

	$('#crop').click(function(){
		canvas = cropper.getCroppedCanvas({
			width:400,
			height:400
		});

		canvas.toBlob(function(blob){
			url = URL.createObjectURL(blob);
			$("#coverImage .image_preview .imageThumb").attr('src',url);
			var reader = new FileReader();
			reader.readAsDataURL(blob);
			reader.onloadend = function(){
				var base64data = reader.result;
				cropImageData = base64data;
				$modal.modal('hide');
			};
		});
	});
	
});



	function getCategoryTopics(e){
		let cat_id = e.value;
		
		if(cat_id !== ''){
			$.ajax({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				data: { category_id: cat_id},
				url: "{{ route('admin.blogs.get_topics') }}",
				type: "POST",
				// dataType: 'json',
				success: function (response) {						
					$('#topic_ids').html(response);				  
				},
				error: function (data) {
					$(".loader").css('display', 'none');
					let errors = data.responseJSON.errors;
					
					$.each(errors, function(key, value) {
						errorAlert('Error Occured.',3000,'top-right');				
					});					
		
				}
			});			
		}else{
            //$("#topic_ids").html('<option value="">Select Topic</option>');
            $("#topic_ids").html('');
            $("#topic_ids").val("");			
		}
	}
	
	window.addEventListener('DOMContentLoaded', () => {
        Laraberg.init('description', { height: '600px', laravelFilemanager: true, sidebar: false })
    })

	$("#blogForm").submit(function(e) {
		e.preventDefault();
	
		var description = Laraberg.getContent();

		var formData = new FormData(this);
		formData.append('description', description);
		formData.append('crop_cover_image', cropImageData);

		$.ajax({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			// data: $('#blogForm').serialize(),
			url: "{{ route('admin.blogs.store') }}",
			data: formData,
			type: "POST",
			cache:false,
			contentType: false,
			processData: false,
			// dataType: 'json',
			success: function (response) {
				//response = JSON.parse(res);
				if(response['status'] == 'success'){
					swal({
						title: response['message'],
						icon: 'success'
					});
					$('#blogForm').trigger("reset");
					setTimeout(function(){
					swal.close();
					window.location = "{{ route('admin.blogs') }}"; }, 1000);
				}
			},
			error: function (data) {
				let errorArr = [];
				let errors = data.responseJSON.errors;
				let emailErr = errors.email[0];				
			}
		});
	});
	
	// Image reader
	if (window.File && window.FileList && window.FileReader) {
		$("#blog_header_image").on("change", function(e) {
			$('#headerImage .pip').remove();
			var files = e.target.files,
			filesLength = files.length;
			for (var i = 0; i < filesLength; i++) {
				var f = files[i]
				var fileReader = new FileReader();
				fileReader.onload = (function(e) {
					var file = e.target;
					$image = "<span class='pip'>" +
						"<img class='imageThumb' src=\"" + e.target.result + "\" title=\"" + file.name + "\" />" +
						"</span>";
					$("#headerImage .image_preview").append($image);
					$("#headerImage.remove").click(function(){
						$(this).parent("#headerImage .pip").remove();
					});
				});
				fileReader.readAsDataURL(f);
			}
		});
	} else {
		alert("Your browser doesn't support to File API")
	}
	
$("#article-title").bind("keyup change", function(e) {
    let text = $("#article-title").val();
	let slug = convertToSlug(text);
	$("#slug").val(slug);
})	

function convertToSlug(Text)
{
    return Text
        .toLowerCase()
        .replace(/[^\w ]+/g,'')
        .replace(/ +/g,'-')
        ;
}

function changeOption(flag)
{
	if(flag=="img")
	{
		$("#vdo_val").css("display","none");
		$("#yt_val").css("display","none");
		$("#img_val").css("display","block");
	}
	if(flag=="vdo")
	{
		$("#img_val").css("display","none");
		$("#yt_val").css("display","none");
		$("#vdo_val").css("display","block");
	}
	if(flag=="yt")
	{
		$("#img_val").css("display","none");
		$("#vdo_val").css("display","none");
		$("#yt_val").css("display","block");
	}

}

</script>
</html>
@endsection