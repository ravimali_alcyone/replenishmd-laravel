@extends('layouts.app')
@section('content')
	<div class="intro-y box mt-5">
		<div class="flex flex-col sm:flex-row items-center p-5 border-b border-gray-200 dark:border-dark-5">
			<h2 class="font-medium text-base mr-auto"><a href="{{env('APP_URL').'/admin/blogs/blog_category'}}">Category List</a> > @if(isset($catData)) {{ 'Edit' }} @else Add @endif Category</h2>
		</div>
		<div class="intro-y datatable-wrapper box p-5 col-span-12 overflow-auto lg:overflow-hidden">
			<form id="blogForm">
				<div class="grid grid-cols-12 gap-6 mt-5">
					<div class="intro-y col-span-12 lg:col-span-12">
						<label class="">Category Image</label>
						<div class="mt-1 flex items-center">
							<span class="inline-block h-12 w-12 overflow-hidden bg-gray-100 mr-5" style="width: 5rem !important; height:5rem !important">
								<?php if(isset($catData)){ 
									echo '<img id="profileData" src="'.env('APP_URL').'/'.$catData->category_img.'" alt="Category Image"/>';
								 }
								 else
								 {
									 echo '<img id="profileData" src="'.env('APP_URL').'/dist/images/user_icon.png" alt="Category Image"/>';
								 }
								 ?>
							</span>
							<input id="file_input" name="file_input" type="file" onchange="changeProfile(event)"/>
						</div> 
					</div>
					<div class="intro-y col-span-12 lg:col-span-12">
						<div>
							<label>Category name</label>
							<input type="text" name="category_name" value="<?php if(isset($catData)){ echo $catData->name; }?>" class="input w-full border mt-2" placeholder="Title" id="category_name" required>
						</div>
					</div>
				</div>
				<div class="grid grid-cols-12 gap-6 mt-12">
					<div class="intro-y col-span-12 lg:col-span-12">
						<input type="hidden" name="id" id="id" value="{{$id}}"/>
						<input type="hidden" name="old_file" id="old_file" value="<?php if(isset($catData)){ echo $catData->category_img; }?>"/>
						<input type="hidden" name="page" id="page" value="{{$funcType}}"/>
						<button type="submit" id="saveBtn" class="button button--md w-24 mr-1 mb-2 btn_blue">Save</button>
						<button type="reset" class="button button--md w-24 mr-1 mb-2 btn_red" onclick="location.href = '{{ route('admin.blogs.blog_category') }}';">Cancel</button>
					</div>
				</div>
			</form>
		</div>
	</div>

	<script type="text/javascript">
	function changeProfile(e)
	{     
		var saida = document.getElementById('file_input');
		var quantos = saida.files.length;
		for(i = 0; i < quantos; i++){
			var urls = URL.createObjectURL(e.target.files[i]);
			document.getElementById('profileData').src=urls;
		}
	}

	$("#blogForm").submit(function(e) {
		e.preventDefault();
		//var formData = new FormData(this);
		var formData = new FormData(document.getElementById("blogForm"));
		console.log(formData);
		/*$.ajax({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			data: {"category_name":$("#category_name").val(),"id":$("#id").val(),"page":$("#page").val()},
			url: "{{ route('admin.blogs.store_category') }}",
			type: "POST",
			dataType: 'json',
			success: function (response) {
				if(response['status'] == 'success'){
					swal({
						title: response['message'],
						icon: 'success'
					});
					$('#blogForm').trigger("reset");
					setTimeout(function(){
					swal.close();
					window.location = "{{ route('admin.blogs.blog_category') }}"; }, 1000);
				}
			},
			error: function (data) {
				let errorArr = [];
				let errors = data.responseJSON.errors;
				let emailErr = errors.email[0];
			}
		});*/
		$.ajax({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			data: formData,
			url: "{{ route('admin.blogs.store_category') }}",
			type: "POST",
			enctype: 'multipart/form-data',
			processData: false,  // tell jQuery not to process the data
			contentType: false,   // tell jQuery not to set contentType
			dataType: "json",
			success: function (response) {
				if(response['status'] == 'success'){
					swal({
						title: response['message'],
						icon: 'success'
					});
					$('#blogForm').trigger("reset");
					setTimeout(function(){
					swal.close();
					window.location = "{{ route('admin.blogs.blog_category') }}"; }, 1000);
				}
			},
			error: function (data) {
				let errorArr = [];
				let errors = data.responseJSON.errors;
				let emailErr = errors.email[0];
			}
		});
	});

	
	
	$("#name").bind("keyup change", function(e) {
		let text = $("#name").val();
		let slug = convertToSlug(text);
		$("#slug").val(slug);
	})	

	function convertToSlug(Text)
	{
		return Text
			.toLowerCase()
			.replace(/[^\w ]+/g,'')
			.replace(/ +/g,'-')
			;
	}
	
</script>
</html>
@endsection