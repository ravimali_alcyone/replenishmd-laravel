@extends('layouts.app')

@section('content')

<div class="intro-y flex flex-col sm:flex-row items-center mt-8">
	<h2 class="text-lg font-medium mr-auto">Blogs List</h2>
	<div class="w-full sm:w-auto flex mt-4 sm:mt-0">
		<div class="intro-y col-span-12 flex flex-wrap sm:flex-no-wrap items-center mt-2">
			<a href="{{ url('admin/blogs/add') }}" class="button w-32 mr-2 mb-2 flex items-center justify-center btn_white">Add New</a>
		</div>
	</div>
</div>

<div class="grid grid-cols-12 gap-6 mt-5">
	<div class="intro-y col-span-12 items-center">
		<form id="tableFilters">
			<div class="col-span-3">
				<label>Author</label>
				<select id="author" name="" class="select2 w-full">
					<option value="">Any</option>
					@if($users && $users->count() > 0)
						@foreach($users as $users)
							<option value="{{ $users->id }}" >{{ $users->name }}</option>
						@endforeach
					@endif
				</select>
			</div>

			<div class="col-span-3">
				<label>Category</label>
				<select id="filter_category" class="select2 w-full">
					<option value="">Any</option>
					@if($categories && $categories->count() > 0)
						@foreach($categories as $category)
							<option value="{{ $category->id }}" >{{ $category->name }}</option>
						@endforeach
					@endif
				</select>
			</div>

			<div class="col-span-3 ml-2">
				<label>Visibility</label>
				<select id="visibility_status" class="select2 w-full">
					<option value="" class="any">Any</option>
					<option value="1">Public</option>
					<option value="0">Private</option>
				</select>
			</div>

			<div class="col-span-3 ml-2">
				<label>Publish</label>
				<select id="publish_status" class="select2 w-full">
					<option value="" class="any">Any</option>
					<option value="1">Immediate</option>
					<option value="0">Draft</option>
				</select>
			</div>

			<div class="col-span-3 ml-2">
				<label>Comment</label>
				<select id="comment_status" class="select2 w-full">
					<option value="" class="any">Any</option>
					<option value="1">Allow</option>
					<option value="0">Not-allow</option>
				</select>
			</div>
			<div class="w-auto mt-3 mt-md-0">
				<button type="button" class="button w-24 mr-1 mb-2 btn_dark ml-2" id="filter">Show</button>
				<button type="reset" class="button w-24 mr-1 mb-2 btn_red float-right ml-2" id="reset">Reset</button>
			</div>
		</form>
	</div>

	<div class="intro-y datatable-wrapper box p-5 col-span-12 overflow-auto lg:overflow-hidden" id="patients_table">
		<table id="myTable" class="table table-report table-report--bordered display w-full sub_admin_table dataTable mt-0">
			<thead>
				<tr class="intro-x">
					<th class="border-b-2">S.No.</th>
					<th class="border-b-2">Title</th>
					<th class="border-b-2">Author</th>
					<th class="border-b-2">Category</th>
					<th class="border-b-2">Visibility</th>
					<th class="border-b-2">Publish</th>
					<th class="border-b-2">Comment</th>
					<th class="border-b-2">Action</th>
				</tr>
			</thead>
			<tbody></tbody>
		</table>
	</div>
</div>

<script type="text/javascript">
	var table;
 	$(document).ready(function() {
		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});

		fill_datatable();
		function fill_datatable(author='', filter_category = '', visibility_status = '', publish_status = '', comment_status = ''){
			
			table = $('#myTable').DataTable({
				processing: true,
				serverSide: true,
				responsive: true,
				paging: true,
				ordering: false,
				info: false,
				displayLength: 10,
				language: {
					processing: '<img src="/dist/images/loading.gif"/>'
				},				
				lengthMenu: [
				[10, 25, 50, -1],
				[10, 25, 50, "All"]
				],
				//ajax: "{{ route('admin.blogs') }}",
				ajax:{
					url: "{{ route('admin.blogs') }}",
					data:{"name":author, "filter_category":filter_category, "visibility_status":visibility_status, "publish_status":publish_status, "comment_status":comment_status }
				},
				columns: [
					{data: 'DT_RowIndex', name: 'DT_RowIndex'},
					{data: 'title', name: 'title'},					
					{data: 'name', name: 'name'},					
					{data: 'blog_category_id', name: 'blog_category_id'},
					{data: 'post_visibility', name: 'post_visibility'},
					{data: 'post_publish', name: 'post_publish'},
					{data: 'post_comment', name: 'post_comment'},
					{data: 'action', name: 'action', orderable: false, searchable: false},
				]
			});
		}

		$('#filter').click(function(){
			var author = $('#author').val();
			var filter_category = $('#filter_category').val();
			var visibility_status = $('#visibility_status').val();
			var publish_status = $('#publish_status').val();
			var comment_status = $('#comment_status').val();
			$('#myTable').DataTable().destroy();
			fill_datatable(author,filter_category,visibility_status,publish_status,comment_status);
		});

		$('#reset').click(function(){
			$('#author').prop('selectedIndex', 0);
			$('#author').select2();

			$('#filter_category').prop('selectedIndex', 0);
			$('#filter_category').select2();

			$('#visibility_status').prop('selectedIndex', 0);
			$('#visibility_status').select2();

			$('#publish_status').prop('selectedIndex', 0);
			$('#publish_status').select2();
			
			$('#comment_status').prop('selectedIndex', 0);
			$('#comment_status').select2();

			$('#myTable').DataTable().destroy();
			fill_datatable();
		});

		$("#myTable tbody tr").addClass('intro-x');
  	});

    function deleteRow(id) {
		swal({
			title: "Are you sure to delete?",
			text: "",
			icon: 'warning',
			buttons: {
			  cancel: true,
			  delete: 'Yes, Delete It'
			}
		  }).then((isConfirm) => {
			if (!isConfirm) {
				return false;
			} else {
				$.ajax({
					type: "DELETE",
					url: "{{ env('APP_URL').'/admin/blogs/destroy' }}"+'/'+id,
					success: function (response) {
						if(response['status'] == 'success'){
						swal({
							title: response['message'],
							icon: 'success'
						});
						setTimeout(function(){
						swal.close();
						}, 1000);
						}
						table.draw();
					},
					error: function (data) {
						console.log('Error:', data);
						swal({
							title: 'Error Occured',
							icon: 'error'
						});
					}
				});
			}
        });
    }

	function changeStatus(id,status,flag) {
		$.ajax({
			url: "{{ url('admin/blogs/change_status') }}",
			type: "POST",
			data: {'id': id, 'status':status, 'flag':flag},
			success: function(response) 
			{
				if(response['status'] == 'success')
				{
					swal({
						title: response['message'],
						icon: 'success'
					});
					setTimeout(function(){
					swal.close();
					}, 1000);
				}
				table.draw();
			}
		});
	}

	function verifyRow(id) {
		swal({
			title: "Are you sure to verify?",
			text: "",
			icon: 'info',
			buttons: {
			  cancel: true,
			  delete: 'Yes, Verify'
			}
		  }).then((isConfirm) => {
			if (!isConfirm) {
				return false;
			} else {
				$.ajax({
					type: "DELETE",
					url: "{{ env('APP_URL').'/admin/blog_post/verify' }}"+'/'+id,
					success: function (response) {
						if(response['status'] == 'success'){
						swal({
							title: response['message'],
							icon: 'success'
						});
						setTimeout(function(){
							window.location = "{{env('APP_URL')}}/admin/blogs";
							swal.close();						
						}, 1000);
						}
						table.draw();
					},
					error: function (data) {
						console.log('Error:', data);
						swal({
							title: 'Error Occured',
							icon: 'error'
						});
					}
				});
			}
        });
    }

</script>
</html>
@endsection