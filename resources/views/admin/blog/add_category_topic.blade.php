@extends('layouts.app')
@section('content')
	<div class="intro-y box mt-5">
		<div class="flex flex-col sm:flex-row items-center p-5 border-b border-gray-200 dark:border-dark-5">
			<h2 class="font-medium text-base mr-auto"><a href="{{env('APP_URL').'/admin/blogs/category_topic'}}">Category Topic</a> > @if(isset($catData)) {{ 'Edit' }} @else Add @endif Category Topic</h2>
		</div>
		<div class="intro-y datatable-wrapper box p-5 col-span-12 overflow-auto lg:overflow-hidden">
			<form id="categoryTopicForm">
				<div class="grid grid-cols-6 gap-6 mt-5">
					<div class="intro-y col-span-12 lg:col-span-12">
						<div>
							<label>Category name</label>
							<select name="category_name" id="category_name" class="select2 w-full">
								@if($category && $category->count() > 0)
									@foreach($category as $val)
										<option value="{{ $val->id }}" <?php if(isset($catData)){ if($catData && $catData->category_id == $val->id){ echo 'selected';} }?> >{{ $val->name }}</option>
									@endforeach
								@endif
							</select>
						</div>
					</div>					
				</div>
				<div class="grid grid-cols-6 gap-6 mt-5">
					<div class="intro-y col-span-12 lg:col-span-12">
						<div>
							<label>Topic</label>
							<input type="text" name="topic" value="<?php if(isset($catData)){ echo $catData->topic; }?>" class="input w-full border mt-2" placeholder="Topic" id="topic" >
						</div>
					</div>
				</div>
				<div class="grid grid-cols-12 gap-6 mt-12">
					<div class="intro-y col-span-12 lg:col-span-12">
						<input type="hidden" name="id" id="id" value="{{$id}}"/>
						<input type="hidden" name="page" id="page" value="{{$funcType}}"/>
						<button type="submit" id="saveBtn" class="button button--md w-24 mr-1 mb-2 btn_blue">Save</button>
						<button type="reset" class="button button--md w-24 mr-1 mb-2 btn_red" onclick="location.href = '{{ route('admin.blogs.category_topic') }}';">Cancel</button>
					</div>
				</div>
			</form>
		</div>
	</div>

	<script type="text/javascript">
	

	$("#categoryTopicForm").submit(function(e) {
		e.preventDefault();
		
		$.ajax({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			data: {"category_id":$("#category_name").val(),"topic":$("#topic").val(),"id":$("#id").val(),"page":$("#page").val()},
			url: "{{ route('admin.blogs.store_category_topic') }}",
			type: "POST",
			dataType: 'json',
			success: function (response) {
				if(response['status'] == 'success'){
					$('#categoryTopicForm').trigger("reset");
					successAlert(response['message'],2000,'top-right');	
					setTimeout(function(){
					window.location = "{{ route('admin.blogs.category_topic') }}"; }, 1000);
				}
				else{
					errorAlert('Error occured.',3000,'top-right');						
				}
			},
			error: function (data) {
				$(".loader").css('display', 'none');
				let errors = data.responseJSON.errors;
				
				$.each(errors, function(key, value) {
					errorAlert(value[0],3000,'top-right');
				});	
			}
		});
	});

	
</script>
</html>
@endsection